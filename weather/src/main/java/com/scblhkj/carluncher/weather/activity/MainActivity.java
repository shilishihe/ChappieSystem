package com.scblhkj.carluncher.weather.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.jude.utils.JUtils;
import com.scblhkj.carluncher.AmapInfoInterface;
import com.scblhkj.carluncher.domain.RemoteAmapInfoBean;
import com.scblhkj.carluncher.weather.R;
import com.scblhkj.carluncher.weather.common.Constants;
import com.scblhkj.carluncher.weather.domain.JuheWeatherBean;
import com.scblhkj.carluncher.weather.utils.GsonUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.List;

import okhttp3.Request;

/**
 * 天气主界面
 */
public class MainActivity extends BaseActivity {

    @butterknife.Bind(R.id.tv_today)
    TextView tvToday;
    @butterknife.Bind(R.id.im_now_weather)
    ImageView imNowWeather;
    @butterknife.Bind(R.id.tv_pm25)
    TextView tvPm25;
    @butterknife.Bind(R.id.tv_sdi)
    TextView tvSdi;
    @butterknife.Bind(R.id.tv_todayTemp)
    TextView tvTodayTemp;
    @butterknife.Bind(R.id.tv_location)
    TextView tvLocation;
    @butterknife.Bind(R.id.tv_uv)
    TextView tvUv;
    @butterknife.Bind(R.id.tv_uvv)
    TextView tvUvv;
    @butterknife.Bind(R.id.tv_washing_index)
    TextView tvWashingIndex;
    @butterknife.Bind(R.id.tv_washing_indexv)
    TextView tvWashingIndexv;
    @butterknife.Bind(R.id.tv_week1)
    TextView tvWeek1;
    @butterknife.Bind(R.id.tv_day_weather1)
    TextView tvDayWeather1;
    @butterknife.Bind(R.id.im_day_weather1)
    ImageView imDayWeather1;
    @butterknife.Bind(R.id.tv_day_temp1)
    TextView tvDayTemp1;
    @butterknife.Bind(R.id.im_night_weather1)
    ImageView imNightWeather1;
    @butterknife.Bind(R.id.tv_night_weather1)
    TextView tvNightWeather1;
    @butterknife.Bind(R.id.tv_night_temp1)
    TextView tvNightTemp1;
    @butterknife.Bind(R.id.tv_week2)
    TextView tvWeek2;
    @butterknife.Bind(R.id.tv_day_weather2)
    TextView tvDayWeather2;
    @butterknife.Bind(R.id.im_day_weather2)
    ImageView imDayWeather2;
    @butterknife.Bind(R.id.tv_day_temp2)
    TextView tvDayTemp2;
    @butterknife.Bind(R.id.im_night_weather2)
    ImageView imNightWeather2;
    @butterknife.Bind(R.id.tv_night_weather2)
    TextView tvNightWeather2;
    @butterknife.Bind(R.id.tv_night_temp2)
    TextView tvNightTemp2;
    @butterknife.Bind(R.id.tv_week3)
    TextView tvWeek3;
    @butterknife.Bind(R.id.tv_day_weather3)
    TextView tvDayWeather3;
    @butterknife.Bind(R.id.im_day_weather3)
    ImageView imDayWeather3;
    @butterknife.Bind(R.id.tv_day_temp3)
    TextView tvDayTemp3;
    @butterknife.Bind(R.id.im_night_weather3)
    ImageView imNightWeather3;
    @butterknife.Bind(R.id.tv_night_weather3)
    TextView tvNightWeather3;
    @butterknife.Bind(R.id.tv_night_temp3)
    TextView tvNightTemp3;
    @butterknife.Bind(R.id.tv_week4)
    TextView tvWeek4;
    @butterknife.Bind(R.id.tv_day_weather4)
    TextView tvDayWeather4;
    @butterknife.Bind(R.id.im_day_weather4)
    ImageView imDayWeather4;
    @butterknife.Bind(R.id.tv_day_temp4)
    TextView tvDayTemp4;
    @butterknife.Bind(R.id.im_night_weather4)
    ImageView imNightWeather4;
    @butterknife.Bind(R.id.tv_night_weather4)
    TextView tvNightWeather4;
    @butterknife.Bind(R.id.tv_night_temp4)
    TextView tvNightTemp4;
    @butterknife.Bind(R.id.tv_week5)
    TextView tvWeek5;
    @butterknife.Bind(R.id.tv_day_weather5)
    TextView tvDayWeather5;
    @butterknife.Bind(R.id.im_day_weather5)
    ImageView imDayWeather5;
    @butterknife.Bind(R.id.tv_day_temp5)
    TextView tvDayTemp5;
    @butterknife.Bind(R.id.im_night_weather5)
    ImageView imNightWeather5;
    @butterknife.Bind(R.id.tv_night_weather5)
    TextView tvNightWeather5;
    @butterknife.Bind(R.id.tv_night_temp5)
    TextView tvNightTemp5;
    @butterknife.Bind(R.id.tv_week6)
    TextView tvWeek6;
    @butterknife.Bind(R.id.tv_day_weather6)
    TextView tvDayWeather6;
    @butterknife.Bind(R.id.im_day_weather6)
    ImageView imDayWeather6;
    @butterknife.Bind(R.id.tv_day_temp6)
    TextView tvDayTemp6;
    @butterknife.Bind(R.id.im_night_weather6)
    ImageView imNightWeather6;
    @butterknife.Bind(R.id.tv_night_weather6)
    TextView tvNightWeather6;
    @butterknife.Bind(R.id.tv_night_temp6)
    TextView tvNightTemp6;
    private AmapInfoInterface amapInfoInterface;
    private RemoteAmapInfoBean remoteAmapInfoBean;
    private static final String TAG = "天气界面";

    private String city;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        butterknife.ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    protected void initView() {
    }

    @Override
    protected void initData() {
        bindRemoteService();
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {
            amapInfoInterface = AmapInfoInterface.Stub.asInterface(service);
            try {
                remoteAmapInfoBean = amapInfoInterface.AmapInfoInterfaceCallBack();
                if (remoteAmapInfoBean != null) {
                    city = remoteAmapInfoBean.getCity();
                    if (!TextUtils.isEmpty(city)) {
                        queryWeather(city);
                    } else {
                        JUtils.Log(TAG, "城市名为空");
                    }
                } else {
                    Log.e(TAG, "remoteAmapInfoBean = null");
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            amapInfoInterface = null;
        }

    };


    private void bindRemoteService() {
        Intent intent = new Intent("com.scblhkj.carluncher.service.RouteRecordService");
        intent.setPackage("com.scblhkj.carluncher");
        boolean success = bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        if (success) {
            Log.e(TAG, "远程服务绑定成功");
        } else {
            Log.e(TAG, "远程服务绑定失败");
        }
    }

    private void unBindRemoteService() {
        unbindService(mConnection);
        mConnection = null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unBindRemoteService();
    }


    /**
     * 查询天气
     */
    private void queryWeather(String cityName) {
        OkHttpUtils.get().url(Constants.JUHE_WEATHER_HOST + cityName).build().execute(new StringCallback() {
            @Override
            public void onError(Request request, Exception e) {
                Log.e(TAG, "查询天气出现异常");
            }

            @Override
            public void onResponse(String response) {
                if (!TextUtils.isEmpty(response)) {
                    JuheWeatherBean bean = GsonUtil.jsonToBean(response, JuheWeatherBean.class);
                    fillData2View(bean);
                }


            }
        });
    }

    /**
     * 将天气数据填充到控件上
     *
     * @param bean
     */
    private void fillData2View(JuheWeatherBean bean) {
        JuheWeatherBean.Data data = bean.result.data;
        JuheWeatherBean.Life life = data.life;
        JuheWeatherBean.PM pm25 = data.pm25;
        JuheWeatherBean.RealTime realtime = data.realtime;
        List<JuheWeatherBean.Weather> weathers = data.weather;

        String realWeather = realtime.weather.info;  // 当前的天气
        setWeatherImgIcon(imNowWeather, realWeather);
        tvPm25.setText("PM2.5:" + pm25.pm25.pm25);  // 设置PM25值
        tvSdi.setText(pm25.pm25.quality); // 污染指数
        tvTodayTemp.setText(realtime.weather.temperature); // 当前温度
        tvLocation.setText(city); // 当前城市
        tvUvv.setText(life.info.ziwaixian.get(0)); // 紫外线强弱
        tvWashingIndexv.setText(life.info.xiche.get(0)); // 洗车指数
        // 天气预报第一天
        tvWeek1.setText("周" + weathers.get(0).week); // 星期
        tvDayWeather1.setText(weathers.get(0).info.day.get(1)); // 天气的文字描述
        setWeatherImgIcon(imDayWeather1, weathers.get(0).info.day.get(1)); // 天气的图片描述
        String dayTemp1 = weathers.get(0).info.night.get(2) + "/" + weathers.get(0).info.day.get(2) + "℃";
        tvDayTemp1.setText(dayTemp1); // 当天白天的最低最高温度
        setWeatherImgIcon(imNightWeather1, weathers.get(0).info.night.get(1)); // 当天晚上天气的图片描述
        tvNightWeather1.setText(weathers.get(0).info.night.get(1));  // 当天晚上天气的文字描述
        String nightTemp1 = weathers.get(0).info.night.get(0) + "/" + weathers.get(0).info.night.get(2) + "℃";
        tvNightTemp1.setText(nightTemp1);
        // 天气预报第二天
        tvWeek2.setText("周" + weathers.get(1).week); // 星期
        tvDayWeather2.setText(weathers.get(1).info.day.get(1)); // 天气的文字描述
        setWeatherImgIcon(imDayWeather2, weathers.get(1).info.day.get(1)); // 天气的图片描述
        String dayTemp2 = weathers.get(1).info.night.get(2) + "/" + weathers.get(1).info.day.get(2) + "℃";
        tvDayTemp2.setText(dayTemp2); // 当天白天的最低最高温度
        setWeatherImgIcon(imNightWeather2, weathers.get(0).info.night.get(1)); // 当天晚上天气的图片描述
        tvNightWeather2.setText(weathers.get(1).info.night.get(1));  // 当天晚上天气的文字描述
        String nightTemp2 = weathers.get(1).info.night.get(0) + "/" + weathers.get(1).info.night.get(2) + "℃";
        tvNightTemp2.setText(nightTemp2);
        // 天气预报第三天
        tvWeek3.setText("周" + weathers.get(2).week); // 星期
        tvDayWeather3.setText(weathers.get(2).info.day.get(1)); // 天气的文字描述
        setWeatherImgIcon(imDayWeather3, weathers.get(2).info.day.get(1)); // 天气的图片描述
        String dayTemp3 = weathers.get(2).info.night.get(2) + "/" + weathers.get(2).info.day.get(2) + "℃";
        tvDayTemp3.setText(dayTemp3); // 当天白天的最低最高温度
        setWeatherImgIcon(imNightWeather3, weathers.get(2).info.night.get(1)); // 当天晚上天气的图片描述
        tvNightWeather3.setText(weathers.get(2).info.night.get(1));  // 当天晚上天气的文字描述
        String nightTemp3 = weathers.get(2).info.night.get(0) + "/" + weathers.get(2).info.night.get(2) + "℃";
        tvNightTemp3.setText(nightTemp3);
        // 天气预报第四天
        tvWeek4.setText("周" + weathers.get(3).week); // 星期
        tvDayWeather4.setText(weathers.get(3).info.day.get(1)); // 天气的文字描述
        setWeatherImgIcon(imDayWeather4, weathers.get(3).info.day.get(1)); // 天气的图片描述
        String dayTemp4 = weathers.get(3).info.night.get(2) + "/" + weathers.get(3).info.day.get(2) + "℃";
        tvDayTemp4.setText(dayTemp4); // 当天白天的最低最高温度
        setWeatherImgIcon(imNightWeather4, weathers.get(3).info.night.get(1)); // 当天晚上天气的图片描述
        tvNightWeather4.setText(weathers.get(3).info.night.get(1));  // 当天晚上天气的文字描述
        String nightTemp4 = weathers.get(3).info.night.get(0) + "/" + weathers.get(3).info.night.get(2) + "℃";
        tvNightTemp4.setText(nightTemp4);
        // 第五天天气预报
        tvWeek5.setText("周" + weathers.get(4).week); // 星期
        tvDayWeather5.setText(weathers.get(4).info.day.get(1)); // 天气的文字描述
        setWeatherImgIcon(imDayWeather5, weathers.get(4).info.day.get(1)); // 天气的图片描述
        String dayTemp5 = weathers.get(4).info.night.get(2) + "/" + weathers.get(4).info.day.get(2) + "℃";
        tvDayTemp5.setText(dayTemp5); // 当天白天的最低最高温度
        setWeatherImgIcon(imNightWeather5, weathers.get(4).info.night.get(1)); // 当天晚上天气的图片描述
        tvNightWeather5.setText(weathers.get(4).info.night.get(1));  // 当天晚上天气的文字描述
        String nightTemp5 = weathers.get(4).info.night.get(0) + "/" + weathers.get(4).info.night.get(2) + "℃";
        tvNightTemp5.setText(nightTemp5);
        // 第六天天气预报
        tvWeek6.setText("周" + weathers.get(5).week); // 星期
        tvDayWeather6.setText(weathers.get(5).info.day.get(1)); // 天气的文字描述
        setWeatherImgIcon(imDayWeather6, weathers.get(5).info.day.get(1)); // 天气的图片描述
        String dayTemp6 = weathers.get(5).info.night.get(2) + "/" + weathers.get(5).info.day.get(2) + "℃";
        tvDayTemp6.setText(dayTemp6); // 当天白天的最低最高温度
        setWeatherImgIcon(imNightWeather6, weathers.get(5).info.night.get(1)); // 当天晚上天气的图片描述
        tvNightWeather6.setText(weathers.get(5).info.night.get(1));  // 当天晚上天气的文字描述
        String nightTemp6 = weathers.get(5).info.night.get(0) + "/" + weathers.get(5).info.night.get(2) + "℃";
        tvNightTemp6.setText(nightTemp6);
    }


    /**
     * 给天气图标切换图片
     *
     * @param image
     * @param weather
     */
    private void setWeatherImgIcon(ImageView image, String weather) {
        switch (weather) {
            case Constants.SUNNY: {
                image.setBackgroundResource(R.mipmap.ic_day_sunny);
                break;
            }
            case Constants.CLOUDY: {
                image.setBackgroundResource(R.mipmap.ic_day_cloudy);
                break;
            }
            case Constants.YIN: {
                image.setBackgroundResource(R.mipmap.ic_day_yin);
                break;
            }
            case Constants.SHOWER: {
                image.setBackgroundResource(R.mipmap.ic_day_shower);
                break;
            }
            case Constants.THUNDER_SHOWER: {
                image.setBackgroundResource(R.mipmap.ic_day_thunder_shower);
                break;
            }
            case Constants.THUNDER_SHOWER_CLIP_HAIL: {
                image.setBackgroundResource(R.mipmap.ic_day_snow_shower);
                break;
            }
            case Constants.SLEET: {
                image.setBackgroundResource(R.mipmap.ic_day_sleet);
                break;
            }
            case Constants.LIGHT_RAIN: {
                image.setBackgroundResource(R.mipmap.ic_day_light_rain);
                break;
            }
            case Constants.MODERATE_RAIN: {
                image.setBackgroundResource(R.mipmap.ic_day_moderate_rain);
                break;
            }
            case Constants.HEAVY_RAIN: {
                image.setBackgroundResource(R.mipmap.ic_day_heavy_rain);
                break;
            }
            case Constants.HEAVY_RAINS: {
                image.setBackgroundResource(R.mipmap.ic_day_heavy_rains);
                break;
            }
            case Constants.TORRENTIAL_RAIN: {
                image.setBackgroundResource(R.mipmap.ic_day_torrential_rain);
                break;
            }
            case Constants.RAINSTORM: {
                image.setBackgroundResource(R.mipmap.ic_day_rainstorm);
                break;
            }
            case Constants.SNOW_SHOWER: {
                image.setBackgroundResource(R.mipmap.ic_day_snow_shower);
                break;
            }
            case Constants.LIGHT_SNOW: {
                image.setBackgroundResource(R.mipmap.ic_day_light_snow);
                break;
            }
            case Constants.MODERATE_SNOW: {
                image.setBackgroundResource(R.mipmap.ic_day_moderate_snow);
                break;
            }
            case Constants.HEAVY_SNOW: {
                image.setBackgroundResource(R.mipmap.ic_day_heavy_snow);
                break;
            }
            case Constants.BLIZZARD: {
                image.setBackgroundResource(R.mipmap.ic_day_blizzard);
                break;
            }
            case Constants.FOG: {
                image.setBackgroundResource(R.mipmap.ic_day_fog);
                break;
            }
            case Constants.FREEZING_RAIN: {
                image.setBackgroundResource(R.mipmap.ic_day_freezing_rain);
                break;
            }
            case Constants.DUST_STORMS: {
                image.setBackgroundResource(R.mipmap.ic_day_dust_storms);
                break;
            }
            case Constants.LIGHT_RAIN_MODERATE_RAIN: {
                image.setBackgroundResource(R.mipmap.ic_day_light_rain_moderate_rain);
                break;
            }
            case Constants.HEAVY_RAIN_HEAVY_RAIN: {
                image.setBackgroundResource(R.mipmap.ic_day_heavy_rain_heavy_rain);
                break;
            }
            case Constants.HEAVY_RAIN_HEAVY_RAINS: {
                image.setBackgroundResource(R.mipmap.ic_day_heavy_rain_heavy_rains);
                break;
            }
            case Constants.HEAVY_RAINS_TORRENTIAL_RAIN: {
                image.setBackgroundResource(R.mipmap.ic_day_heavy_rains_torrential_rain);
                break;
            }
            case Constants.TORRENTIAL_RAIN_RAINSTORM: {
                image.setBackgroundResource(R.mipmap.ic_day_torrential_rain_rainstorm);
                break;
            }
            case Constants.LIGHT_SNOW_MODERATE_SNOW: {
                image.setBackgroundResource(R.mipmap.ic_day_light_snow_moderate_snow);
                break;
            }
            case Constants.MODERATE_SNOW_HEAVY_SNOW: {
                image.setBackgroundResource(R.mipmap.ic_day_moderate_snow_heavy_snow);
                break;
            }
            case Constants.HEAVY_SNOW_BLIZZARD: {
                image.setBackgroundResource(R.mipmap.ic_day_heavy_snow_blizzard);
                break;
            }
            case Constants.FLY_ASH: {
                image.setBackgroundResource(R.mipmap.ic_fly_ash);
                break;
            }
            case Constants.MICROMETEOROLOGY: {
                image.setBackgroundResource(R.mipmap.ic_day_micrometeorology);
                break;
            }
            case Constants.STRONG_SANDSTORMS: {
                image.setBackgroundResource(R.mipmap.ic_day_strong_sandstorms);
                break;
            }
            case Constants.HAZE: {
                image.setBackgroundResource(R.mipmap.ic_day_haze);
                break;
            }
            default: {
                image.setBackgroundResource(R.mipmap.ic_weather_null);
                break;
            }
        }
    }
}

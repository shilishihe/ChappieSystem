package com.scblhkj.carluncher.weather.app;

import android.app.Application;

import com.jude.utils.BuildConfig;
import com.jude.utils.JUtils;

/**
 * Created by Leo on 2016/1/20.
 */
public class LeoWeatherApplication extends Application {



    @Override
    public void onCreate() {
        super.onCreate();
        JUtils.initialize(this);
        JUtils.setDebug(BuildConfig.DEBUG, "DefaultTag");
    }



}

package com.scblhkj.carluncher.weather.common;

/**
 * Created by Leo on 2016/1/22.
 */
public interface Constants {
    /********************
     * 天气
     *****************************/
    String SUNNY = "晴"; // 需要区分晚上
    String CLOUDY = "多云"; // 需要区分晚上
    String YIN = "阴";
    String SHOWER = "阵雨";  // 需要区分晚上
    String THUNDER_SHOWER = "雷阵雨";
    String THUNDER_SHOWER_CLIP_HAIL = "雷阵雨并伴有冰雹";
    String SLEET = "雨夹雪";
    String LIGHT_RAIN = "小雨";
    String MODERATE_RAIN = "中雨";
    String HEAVY_RAIN = "大雨";
    String HEAVY_RAINS = "暴雨";
    String TORRENTIAL_RAIN = "大暴雨";
    String RAINSTORM = "特大暴雨";
    String SNOW_SHOWER = "阵雪";  // 需要区分晚上
    String LIGHT_SNOW = "小雪";
    String MODERATE_SNOW = "中雪";
    String HEAVY_SNOW = "大雪";
    String BLIZZARD = "暴雪";
    String FOG = "雾";
    String FREEZING_RAIN = "冻雨";
    String DUST_STORMS = "沙尘暴";
    String LIGHT_RAIN_MODERATE_RAIN = "小雨-中雨";
    String HEAVY_RAIN_HEAVY_RAIN = "中雨-大雨";
    String HEAVY_RAIN_HEAVY_RAINS = "大雨-暴雨";
    String HEAVY_RAINS_TORRENTIAL_RAIN = "暴雨-大暴雨";
    String TORRENTIAL_RAIN_RAINSTORM = "大暴雨-特大暴雨";
    String LIGHT_SNOW_MODERATE_SNOW = "小雪-中雪";
    String MODERATE_SNOW_HEAVY_SNOW = "中雪-大雪";
    String HEAVY_SNOW_BLIZZARD = "大雪-暴雪";
    String FLY_ASH = "浮尘";
    String MICROMETEOROLOGY = "扬沙";
    String STRONG_SANDSTORMS = "强沙尘暴";
    String HAZE = "霾";
    String SQUALL = "飑";
    String TORNADO = "龙卷风";
    String WEAK_BLOWING_SNOW = "弱高吹雪";
    String LIGHT_HAZE = "轻霾";

    // 聚合天气接口
    String JUHE_WEATHER_KEY = "6805e417318a246a9b7d8f12fddcc184";
    // 接口地址
    // http://op.juhe.cn/onebox/weather/query?cityname=%E6%B8%A9%E5%B7%9E&key=您申请的KEY
    String JUHE_WEATHER_HOST = "http://op.juhe.cn/onebox/weather/query?&key=" + JUHE_WEATHER_KEY + "" + "&cityname=";
}

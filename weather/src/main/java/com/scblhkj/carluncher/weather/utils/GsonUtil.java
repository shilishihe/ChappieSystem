package com.scblhkj.carluncher.weather.utils;

import com.google.gson.Gson;

import java.util.List;

/**
 * Gson解析Json的工具类
 *
 * @author HP
 */
public class GsonUtil {

    public static <T> T jsonToBean(String jsonResult, Class<T> clz) {
        Gson gson = new Gson();
        T t = gson.fromJson(jsonResult, clz);
        return t;
    }

    public static <T> String listBean2Json(List<T> beans) {
        Gson gson = new Gson();
        return gson.toJson(beans);
    }

}

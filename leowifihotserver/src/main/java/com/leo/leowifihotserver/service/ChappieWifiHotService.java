package com.leo.leowifihotserver.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.leo.leowifihotserver.thread.ListenThread;

/**
 * Created by Leo on 2016/3/30.
 */
public class ChappieWifiHotService extends Service {

    private ListenThread listenThread;

    private static final String TAG = "查派WIFI后是服务";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        listenThread = new ListenThread(this);
        listenThread.start();
        Log.e(TAG, "查派Wifi热点服务启动");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }
}

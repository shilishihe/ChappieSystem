package com.leo.leowifihotserver.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

/**
 * Created by Leo on 2016/3/7.
 * Wifi热点通信服务
 */
public class WifiHotSocketService extends Service {

    private WifiHotSocketThread wifiHotSocketThread;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        wifiHotSocketThread = new WifiHotSocketThread();
        wifiHotSocketThread.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        wifiHotSocketThread.release();
    }

    class WifiHotSocketThread extends Thread {

        private Socket mSocket;
        private ServerSocket mServerSocket;
        private boolean isRun;

        public WifiHotSocketThread() {
        }

        public void release() {
            isRun = false;
        }

        @Override
        public void run() {
            super.run();
            try {
                mServerSocket = new ServerSocket(34353);
                mSocket = mServerSocket.accept();
                if (mSocket != null && !mSocket.isClosed()) {
                    InputStream is = mSocket.getInputStream();
                    byte[] buffer = new byte[1024 * 2];
                    int length = 0;
                    while (!mSocket.isClosed() && !mSocket.isInputShutdown() && isRun && ((length = is.read(buffer)) != -1)) {
                        if (length > 0) {
                            String message = new String(Arrays.copyOf(buffer, length));
                            String[] tempMessage = message.split("\n"); // 按照换行符分离
                            Log.e(TAG, "Wifi热点客户端发送过来的指令：" + tempMessage);
                        }
                    }
                    is.close();
                }else{
                    Toast.makeText(WifiHotSocketService.this,"Wifi服务端socket创建失败",Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(WifiHotSocketService.this,"Exception e Wifi服务端socket创建失败",Toast.LENGTH_LONG).show();
            }
        }
    }

    public static final String TAG = "Wifi热点通信服务";

}

package com.leo.leowifihotserver.utils;

import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Leo on 2016/3/28.
 */
public class CommonUtil {

    private static final String TAG = "CommonUtil";
    public static String PIC_FILE_BASE = "/storage/sdcard1/ChappieLauncher/picture/";
    public static String VIDEO_FILE_BASE = "/storage/sdcard1/ChappieLauncher/video/";

    /**
     * 生成的是20150308
     *
     * @return
     */
    public static String getCurrentData() {
        SimpleDateFormat nowDayFormat = new SimpleDateFormat("yyyyMMdd");
        String nowDay = nowDayFormat.format(new Date());
        return nowDay;
    }

    /**
     * 获取指定目录下的文件信息
     *
     * @param path 路径(图片/视频)
     * @return 文件加_文件名 集合
     */
    public static List<String> getAllFileInfo(String path) {
        // 文件加_文件名
        List<String> infoList = new ArrayList<String>();
        File file = new File(path);
        if (file.exists() && file.isDirectory()) {
            File dirs[] = file.listFiles();
            for (int i = 0; i < dirs.length; i++) {
                String dirName = dirs[i].getName();
                File files[] = dirs[i].listFiles();
                for (int j = 0; j < files.length; j++) {
                    infoList.add(dirName + "," + files[j].getName());  // 考虑到文件名中会出现"_"所以改用","
                }
            }
        }
        return infoList;
    }

    /**
     * @param start  开始位置
     * @param offset 偏移量
     * @return
     */
    public static String getAllFileInfoString(int type, int start, int offset) {

        int end = start + offset;
        StringBuilder resultSB = new StringBuilder();
        String dataStr = "";
        String cacheDir = "";
        List<String> fileInfoList = null;

        if (type == 0) {
            fileInfoList = getAllFileInfo(PIC_FILE_BASE);
        } else if (type == 1) {
            fileInfoList = getAllFileInfo(VIDEO_FILE_BASE);
        }
        if (fileInfoList == null || fileInfoList.size() == 0) {
            Log.e(TAG, "文件列表为空");
            return "";
        }

        for (String ss : fileInfoList) {
            Log.e(TAG, ss);
        }

        if (start >= fileInfoList.size()) {
            start = 0;
        }
        if (end >= fileInfoList.size()) {
            end = fileInfoList.size();
        }

        Map<String, String> tempInfoMap = new LinkedHashMap<String, String>();
        String fileNames = "";
        cacheDir = fileInfoList.get(start).split(",")[0];
        Log.e(TAG, "start = " + start + " end=" + end);
        for (int j = start; j < end; j++) {
            String fileInfo = fileInfoList.get(j);
            String dfs[] = fileInfo.split(",");
            String tempDir = dfs[0];   // 当前的文件夹名称
            String tempFile = dfs[1];  // 当前的文件名称
            if (!cacheDir.equals(tempDir)) {
                tempInfoMap.put(cacheDir, fileNames);
                fileNames = "";
                cacheDir = tempDir;
            }
            fileNames += tempFile + ":";
        }
        tempInfoMap.put(cacheDir, fileNames);


        StringBuilder dataSB = new StringBuilder();  // 数据
        for (Map.Entry<String, String> entry : tempInfoMap.entrySet()) {
            String dirName = entry.getKey();
            String fs = entry.getValue();

            String fss[] = fs.split(":");
            dataSB.append(dirName);
            for (String f : fss) {

                if (f.length() >= 4) {
                    String fn = "";
                    f = f.substring(0, f.length() - 4);
                    if (!f.contains("lock_") && !f.contains("even_")) {
                        fn = getFixedLenString(f, 11);
                    } else {
                        fn = f;
                    }
                    dataSB.append(fn);
                } else {
                    Log.e(TAG, "文件名长度不够四位");
                }
                String thumStr = "";
                if (type == 0) {
                    thumStr = ThumbnailUtil.getImageThumbnailBase64String(PIC_FILE_BASE + dirName + "/" + f + ".jpg");
                } else if (type == 1) {
                    thumStr = ThumbnailUtil.getVideoThumbnailBase64String(VIDEO_FILE_BASE + dirName + "/" + f + ".mp4");
                }
                dataSB.append(getFixedLenString(Integer.toHexString(thumStr.length()), 4) + thumStr);
            }
            dataSB.append(",");
        }

        dataStr = dataSB.toString().substring(0, dataSB.toString().length() - 1);  // 数据
        String dirL = Integer.toHexString(tempInfoMap.size()); // 文件夹总数(4位十六进制)
        String fileL = Integer.toHexString(fileInfoList.size()); // 文件总数(八位十六进制)
        String dataL = Integer.toHexString(dataStr.length()); // 数据长度(八位十六进制)
        dirL = getFixedLenString(dirL, 4);
        fileL = getFixedLenString(fileL, 8);
        dataL = getFixedLenString(dataL, 8);

        if (type == 0) {
            resultSB.append("FDP" + dirL + fileL + "jpg" + dataL + dataStr);
        } else if (type == 1) {
            resultSB.append("FDV" + dirL + fileL + "mp4" + dataL + dataStr);
        }

        Log.e(TAG, resultSB.toString());

        return resultSB.toString();
    }


    /**
     * 指定位数在字符串前面补0
     *
     * @param str
     * @param len
     * @return
     */
    public static String getFixedLenString(String str, int len) {
        StringBuilder sb = new StringBuilder();
        if (str == null || str.length() == 0) {
            str = "";
        }
        if (str.length() == len) {
            return str;
        }
        for (int i = 0; i < len - str.length(); i++) {
            sb.append('0');
        }
        sb.append(str);
        return sb.toString();
    }
}

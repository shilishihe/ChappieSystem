package com.leo.leowifihotserver.thread;

import android.util.Log;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class SendMsgDataThread extends Thread {

    private Socket socket;
    private String address;
    private String content;


    public SendMsgDataThread(String address, String content) {
        this.address = address;
        this.content = content;
    }

    @Override
    public void run() {
        try {
            socket = new Socket(address, 12345);// 发送到本机下某个Ip的端口上
            if (socket != null) {
                try {
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                    out.println(content + "\n"); // 发送数据
                    out.flush();
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (UnknownHostException e) {
            Log.d("aaa", "SendMsgDataThread.init() has UnknownHostException" + e.getMessage());
        } catch (IOException e) {
            Log.d("aaa", "SendMsgDataThread.init().IOException:" + e.getMessage());
        }
    }
}

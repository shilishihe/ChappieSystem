package com.leo.leowifihotserver.thread;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.leo.leowifihotserver.utils.CommonUtil;
import com.leo.leowifihotserver.utils.FileToolkit;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 收到数据的回调方法
 *
 * @author Leo
 */
public class ListenThread extends Thread {

    private static final String TAG = "ListenThread";
    private ServerSocket serverSocket = null;
    private Context context;
    private Socket socket;
    private BroadcastReceiver broadcastReceiver;
    private Map<String, DataOutputStream> cacheMap = new LinkedHashMap<String, DataOutputStream>();

    public ListenThread(Context context) {
        try {
            serverSocket = new ServerSocket(12345);// 监听本机的12345端口
            this.context = context;
            initBroadCast();
            //    registerBroadCastReceiver();
        } catch (IOException e) {
            Log.d(TAG, "ListenThread ServerSocket init() has exception");
        }
    }

    private DataOutputStream dos;

    @Override
    public void run() {
        while (true) {
            try {
                socket = serverSocket.accept();// 等待消息
                socket.setSoTimeout(1000 * 3600);
                socket.getKeepAlive();
                Log.e(TAG, "执行到这里");
                if (socket != null && !socket.isClosed()) {
                    dos = new DataOutputStream(socket.getOutputStream());
                    String clientIP = socket.getInetAddress().getHostAddress();
                    if (cacheMap.size() > 0) {
                        for (String cacheIP : cacheMap.keySet()) {
                            Log.e(TAG, "clientIP = " + clientIP + " cacheip = " + cacheIP);
                            if (!clientIP.equals(cacheIP)) {
                                cacheMap.put(clientIP, dos);
                                Log.e(TAG, "新的客户端上线" + " ip = " + socket.getInetAddress().getHostAddress() + " port = " + socket.getPort());
                            } else {
                                Log.e(TAG, "该地址的客户已经连接");
                            }
                        }
                    } else {
                        cacheMap.put(clientIP, dos);
                        Log.e(TAG, "新的客户端上线" + " ip = " + socket.getInetAddress().getHostAddress() + " port = " + socket.getPort());
                    }
                    InputStream is = socket.getInputStream();
                    byte[] buffer = new byte[1024 * 2];
                    int length = 0;
                    if (!socket.isClosed() && !socket.isInputShutdown() && ((length = is.read(buffer)) != -1)) {
                        if (length > 0) {
                            String msgData = new String(Arrays.copyOf(buffer, length));
                            Log.e(TAG, "接收到客户端发过来的数据是:" + msgData);
                            dos.write("heartbeat".getBytes());
                            dos.flush();
                            if (msgData.equals("CP")) {
                                registerBroadCastReceiver();
                                sendBoradCast2C();
                            } else if (msgData.equals("FVS")) {
                                registerBroadCastReceiver();
                                Intent intent = new Intent();
                                intent.setAction("com.scblhkj.cp.FV_START");
                                intent.addCategory(Intent.CATEGORY_DEFAULT);
                                context.sendBroadcast(intent);
                            } else if (msgData.equals("FVE")) {
                                registerBroadCastReceiver();
                                Intent intent = new Intent();
                                intent.setAction("com.scblhkj.cp.FV_END");
                                intent.addCategory(Intent.CATEGORY_DEFAULT);
                                context.sendBroadcast(intent);
                            } else if (msgData.contains("FP")) {
                                if (msgData.length() == 21) {  // 请求单张图片
                                    String picDir = msgData.substring(2, 10);
                                    String picName = msgData.substring(10, msgData.length());
                                    if (picName.contains("even_")) {  // 事件图片
                                        // 不需要做处理
                                    } else {
                                        // 非事件图片做截取
                                        picName = picName.substring(5, picName.length());
                                    }
                                    Log.e(TAG, "图片文件目录是" + picDir + "图片文件名称是" + picName);
                                    new SendFileDataThread(dos, "/storage/sdcard1/ChappieLauncher/picture/" + picDir + "/" + picName + ".jpg", "FP").start();
                                } else if (msgData.contains("FPD")) {   // 请求删除指定的图片集合
                                    List<String> cacheList = new ArrayList<String>();
                                    int dataL = Integer.parseInt(msgData.substring(3, 11), 16);  // 数据长度
                                    String data = msgData.substring(11, msgData.length());
                                    for (int i = 0; i < dataL / 19; i++) {
                                        cacheList.add(data.substring(i * 19, i * 19 + 19));
                                    }
                                    for (String df : cacheList) {
                                        String fileDir = df.substring(0, 8);
                                        String tempFileName = df.substring(8, df.length());
                                        String fileName = "";
                                        if (tempFileName.contains("even_")) {
                                            fileName = tempFileName;
                                        } else {
                                            fileName = tempFileName.substring(5, tempFileName.length());
                                        }
                                        FileToolkit.deleteFile("/storage/sdcard1/ChappieLauncher/picture/" + fileDir + "/" + fileName + ".jpg");
                                    }
                                    dos.writeBytes("FPD1");
                                    dos.flush();
                                }
                            } else if (msgData.contains("FV")) {
                                if (msgData.length() == 21) {   // 请求指定的视频下载
                                    String videoDir = msgData.substring(2, 10);  // 文件路径
                                    String videoName = msgData.substring(10, msgData.length());  // 视频名称
                                    if (videoName.contains("lock_")) {
                                        // 不做处理
                                    } else {
                                        // 非事件视频做截取
                                        videoName = videoName.substring(5, videoName.length());
                                    }
                                    Log.e(TAG, "要下载的视频文件的目录是 " + videoDir + " 视频文件的名称是 " + videoName);
                                    new SendFileDataThread(dos, "/storage/sdcard1/ChappieLauncher/video/" + videoDir + "/" + videoName + ".3gp", "FV").start();
                                } else if (msgData.contains("FVD")) {   // 请求删除指定的图片集合
                                    List<String> cacheList = new ArrayList<String>();
                                    int dataL = Integer.parseInt(msgData.substring(3, 11), 16);  // 数据长度
                                    String data = msgData.substring(11, msgData.length());
                                    for (int i = 0; i < dataL / 19; i++) {
                                        cacheList.add(data.substring(i * 19, i * 19 + 19));
                                    }
                                    for (String df : cacheList) {
                                        String fileDir = df.substring(0, 8);
                                        String tempFileName = df.substring(8, df.length());
                                        String fileName = "";
                                        if (tempFileName.contains("lock_")) {
                                            fileName = tempFileName;
                                        } else {
                                            fileName = tempFileName.substring(5, tempFileName.length());
                                        }
                                        FileToolkit.deleteFile("/storage/sdcard1/ChappieLauncher/video/" + fileDir + "/" + fileName + ".3gp");
                                    }
                                    dos.writeBytes("FVD1");
                                    dos.flush();
                                }
                            } else if (msgData.contains("FDP")) {
                                if (msgData.length() == 9) {   // 返回分页所有图片
                                    int start = Integer.parseInt(msgData.substring(3, 7), 16);
                                    int offset = Integer.parseInt(msgData.substring(7, msgData.length()));
                                    dos.writeBytes(CommonUtil.getAllFileInfoString(0, start, offset));
                                    dos.flush();
                                }
                            } else if (msgData.contains("FDV")) {
                                if (msgData.length() == 9) {    // 返回分页所有视频
                                    int start = Integer.parseInt(msgData.substring(3, 7), 16);
                                    int offset = Integer.parseInt(msgData.substring(7, msgData.length()));
                                    dos.writeBytes(CommonUtil.getAllFileInfoString(1, start, offset));
                                    dos.flush();
                                }
                            }
                        }
                    }
                } else {
                    Log.e(TAG, "客户端Socket异常");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 发送广播指令去拍照
     */
    private void sendBoradCast2C() {
        Intent intent = new Intent();
        intent.setAction("com.android.chappie.ACTION_WIFI_C_PIC");
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        context.sendBroadcast(intent);
    }

    /**
     *
     */
    private void initBroadCast() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case "com.scblhkj.chappie.hddvr.SEND_CAR_COLLISION_PIC": {
                        String filePath = intent.getStringExtra("com.scblhkj.chappie.hddvr.SEND_CAR_COLLISION_PIC_PATH");
                        for (DataOutputStream os : cacheMap.values()) {
                            new SendFileDataThread(os, filePath, "FPE").start();// 消息发送方启动线程发送消息
                        }
                        Log.e(TAG, "向客户端发送了震动检测的图片");
                        break;
                    }
                    case "com.scblhkj.chappie.hddvr.SEND_CAR_CMD_WIFI_PIC": {
                        String filePath = intent.getStringExtra("com.scblhkj.chappie.hddvr.SEND_CAR_CMD_WIFI_PIC_PATH");
                        for (DataOutputStream os : cacheMap.values()) {
                            new SendFileDataThread(os, filePath, "CP").start();// 消息发送方启动线程发送消息
                        }
                        //  unRister(); 注销广播接受者这里需要注意下
                        break;
                    }
                    case "com.scblhkj.cp.FINISH_VIDEO_FILE": {
                        String videoFilePath = intent.getStringExtra("com.scblhkj.cp.FINISH_VIDEO_FILE_VALUE");   // 获取到了手动录制视频的文件路径
                        Log.e(TAG, "接收到了手动录制完成后发送过来的视频路径" + videoFilePath);
                        new SendFileDataThread(dos, videoFilePath, "FV").start();
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        };
    }

    /**
     * 注册广播接收者
     */
    private void registerBroadCastReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        intentFilter.addAction("com.scblhkj.chappie.hddvr.SEND_CAR_CMD_WIFI_PIC");
        intentFilter.addAction("com.scblhkj.chappie.hddvr.SEND_CAR_COLLISION_PIC");
        intentFilter.addAction("com.scblhkj.cp.FINISH_VIDEO_FILE");  // 手动视频录制完成之后的广播,携带的广播数据是录制完成视频的路径
        context.registerReceiver(broadcastReceiver, intentFilter);
        Log.e(TAG, "注册了广播接受者");
    }


    private void unRister() {
        if (broadcastReceiver != null) {
            context.unregisterReceiver(broadcastReceiver);
        }
    }


}

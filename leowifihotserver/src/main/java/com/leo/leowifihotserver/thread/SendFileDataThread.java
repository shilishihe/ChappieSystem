package com.leo.leowifihotserver.thread;

import android.util.Log;

import com.leo.leowifihotserver.utils.CommonUtil;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.Socket;

/**
 * Created by Leo on 2016/3/24.
 * Wifi文件传输的线程
 */
public class SendFileDataThread extends Thread {

    private static final String TAG = "SendFileDataThread";
    private Socket socket;
    private String filePath;
    private int length = 0;
    private int sumL = 0;
    private byte[] sendBytes = null;
    private FileInputStream fis = null;
    private DataOutputStream dos;
    private String cmd;

    public SendFileDataThread(DataOutputStream dos, String filePath, String cmd) {
        this.filePath = filePath;
        this.dos = dos;
        this.cmd = cmd;
    }

    @Override
    public void run() {
        try {
            try {
                File file = new File(filePath);
                Log.e(TAG, "待发送的文件路径是" + filePath);
                Log.e(TAG,"fileLength = " + file.length());
                String fileLHex = Long.toHexString(file.length());
                fileLHex = CommonUtil.getFixedLenString(fileLHex, 8);
                String fileName = file.getName();
                fileName = CommonUtil.getFixedLenString(fileName, 15);
                String headData = cmd + CommonUtil.getCurrentData() + fileName + fileLHex;
                Log.e(TAG, "头长度为" + headData.length() + "头内容" + headData);
                dos.write(headData.getBytes());
                dos.flush();
                fis = new FileInputStream(file);
                Log.e(TAG,"**fileLength = " + file.length());
                sendBytes = new byte[1024 * 4];
                while ((length = fis.read(sendBytes, 0, sendBytes.length)) > 0) {
                    sumL += length;
                    dos.write(sendBytes, 0, length);
                    dos.flush();
                //    Log.e(TAG, "文件已经发送了" + sumL + "B");
                }
                Log.e(TAG,"while执行完成");
                Log.e(TAG,"***fileLength = " + file.length());
                if (file.length() == sumL) {
                    Log.e(TAG,"sumL =" + sumL);
                    Log.e(TAG, "fileLength = " + file.length() +"文件发送成功");
                } else {
                    Log.e(TAG, "文件发送过程中出现了丢包,还有" + (file.length() - sumL) + "个B没传过去");
                }
            } finally {
                if (fis != null)
                    fis.close();
            }
        } catch (Exception e) {
            Log.e(TAG, "SendFileDataThread中出现异常");
            e.printStackTrace();
        }
        super.run();
    }
}

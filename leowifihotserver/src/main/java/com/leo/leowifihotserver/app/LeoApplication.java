package com.leo.leowifihotserver.app;

import android.app.Application;

import com.leo.leowifihotserver.utils.ToastUtil;

import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Leo on 2016/1/29.
 */
public class LeoApplication extends Application {

    private static LeoApplication app;

    public static Map<String, Socket> connClien;

    public LeoApplication() {
    }

    public static LeoApplication getApp() {
        if (null == app) {
            app = new LeoApplication();
        }
        return app;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        connClien = new LinkedHashMap<String, Socket>();
        ToastUtil.init(this);
    }

}

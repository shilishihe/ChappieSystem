package com.leo.leowifihotserver.constant;

/**
 * Created by Leo on 2016/3/29.
 * 单例测试类
 * 兼顾线程安全和效率的写法
 */
public class SingletonDemo {

    private static volatile SingletonDemo singleton = null;

    private SingletonDemo() {
    }

    public static SingletonDemo getSingleton() {
        if (singleton == null) {
            synchronized (SingletonDemo.class) {
                if (singleton == null) {
                    singleton = new SingletonDemo();
                }
            }
        }
        return singleton;
    }

}

package com.leo.leowifihotserver.constant;

/**
 * Created by Leo on 2016/1/29.
 */
public interface Constans {
    // wifi状态
    int WIFI_CONNECTING = 0;
    int WIFI_CONNECTED = 1;
    int WIFI_CONNECT_FAILED = 2;
    String SSID = "LEO_SSID";
    String HOT_PASSWORD = "LEO_MIME";
    int WIFICIPHER_NOPASS = 1;
    int WIFICIPHER_WEP = 2;
    int WIFICIPHER_WPA = 3;
    String INT_SERVER_FAIL = "INTSERVER_FAIL";
    String INT_SERVER_SUCCESS = "INTSERVER_SUCCESS";
    String INT_CLIENT_FAIL = "INTCLIENT_FAIL";
    String INT_CLIENT_SUCCESS = "INTCLIENT_SUCCESS";
    String CONNECT_SUCESS = "connect_success";
    String CONNECT_FAIL = "connect_fail";
    // 数据传输命令
    /**
     * FTP服务
     */
    String USERNAME = "username";
    String PASSWORD = "password";
    String PORTNUM = "portNum";
    String CHROOTDIR = "chrootDir";
    String ACCEPT_WIFI = "allowWifi";
    String ACCEPT_NET = "allowNet";
    String STAY_AWAKE = "stayAwake";
    /**
     * MessageWhate
     */
    int RECEIVE_DATA_YES = 0XFF;
    int RECEIVE_DATA_NO = 0XFE;


    int PIC = 0XAA;
    int VIDEO = 0XAB;

}

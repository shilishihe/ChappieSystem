package com.leo.leowifihotserver.activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.leo.leowifihotserver.R;
import com.leo.leowifihotserver.constant.Constans;
import com.leo.leowifihotserver.service.ChappieWifiHotService;
import com.leo.leowifihotserver.service.LocalService;
import com.leo.leowifihotserver.swiftp_2.FTPServerService;
import com.leo.leowifihotserver.thread.SendMsgDataThread;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * wifi热点服务端demo
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MainActivityServer";
    private WifiManager wifi = null;
    private Button openheat = null;
    private Button getphone = null;
    private Button startsocket = null;
    private boolean flag = false;
    private LocalService myservice;
    private ServiceConnection sc = null;
    private TelephonyManager tm;
    private BroadcastReceiver broadcastReceiver;
    private Button sendTestVideo;  // 发送测试视频
    private boolean isFTPServerStart; // FTP服务是否开启

    private void initBroadCast() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case "com.scblhkj.chappie.hddvr.SEND_CAR_COLLISION_PIC": {
                        String filePath = intent.getStringExtra("com.scblhkj.chappie.hddvr.SEND_CAR_COLLISION_PIC_PATH");
                        ArrayList<String> connectedIP = getConnectedIP();
                        for (String ip : connectedIP) {
                            if (ip.contains(".")) {
                                //new SendFileDataThread(ip, 12346, filePath).start();// 消息发送方启动线程发送消息
                            }
                        }
                        Log.e("info", "接收到事件图片的地址广播，图片路径为" + filePath);
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        };
    }


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constans.RECEIVE_DATA_YES: {

                    break;
                }
                case Constans.RECEIVE_DATA_NO: {
                    break;
                }
                default: {
                    break;
                }
            }
            super.handleMessage(msg);
        }
    };

    /**
     * 注册广播接收者
     */
    private void registerBroadCastReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        intentFilter.addAction("com.scblhkj.chappie.hddvr.SEND_CAR_COLLISION_PIC");
        registerReceiver(broadcastReceiver, intentFilter);
    }

    /**
     * 将设备的IMIE码存入到SP中
     */
    public String getDeviceIMEI() {
        tm = ((TelephonyManager) getSystemService(TELEPHONY_SERVICE));
        Log.e(TAG, tm.getDeviceId());
        return tm.getDeviceId();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initData();
        initBroadCast();
        registerBroadCastReceiver();
       /* new Thread(new Runnable() {
            @Override
            public void run() {
                Log.e(TAG, CommonUtil.getAllPicInfoString(0, 20));
            }
        }).start();*/

    }

    private void initView() {
        openheat = (Button) findViewById(R.id.openheat);
        getphone = (Button) findViewById(R.id.getphone);
        startsocket = (Button) findViewById(R.id.startsocket);
        sendTestVideo = (Button) findViewById(R.id.sendTestVideo);
    }

    private void initData() {
        wifi = (WifiManager) getSystemService(WIFI_SERVICE);
        sc = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                LocalService.LocalBinder binder = (LocalService.LocalBinder) service; // 通过IBinder获取Service
                myservice = binder.getService();
                myservice.startWaitDataThread(MainActivity.this);// 完成绑定后打开另外一条线程等待消息接收
            }
        };
        openheat.setOnClickListener(this);
        getphone.setOnClickListener(this);
        startsocket.setOnClickListener(this);
        sendTestVideo.setOnClickListener(this);
        connection();// 绑定等待发送方消息的service
        //    startChappieWifiHostService();
    }


    private void startChappieWifiHostService() {
        Intent intent = new Intent(this, ChappieWifiHotService.class);
        startService(intent);
    }

    private void connection() {
        Intent intent = new Intent("com.leo.local.service");
        intent = new Intent(getExplicitIntent(this, intent));
        bindService(intent, sc, this.BIND_AUTO_CREATE);
    }

    /**
     * 将隐式意图转成显示意图
     *
     * @param context
     * @param implicitIntent
     * @return
     */
    public static Intent getExplicitIntent(Context context, Intent implicitIntent) {
        // Retrieve all services that can match the given intent
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfo = pm.queryIntentServices(implicitIntent, 0);
        // Make sure only one match was found
        if (resolveInfo == null || resolveInfo.size() != 1) {
            return null;
        }
        // Get component info and create ComponentName
        ResolveInfo serviceInfo = resolveInfo.get(0);
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        // Create a new intent. Use the old one for extras and such reuse
        Intent explicitIntent = new Intent(implicitIntent);
        // Set the component to be explicit
        explicitIntent.setComponent(component);
        return explicitIntent;
    }

    /**
     * 开启Wifi热点
     */
    private Boolean setWifiApEnabled(Boolean enable) {
        if (enable) { // disable WiFi in any case
            // wifi和热点不能同时打开，所以打开热点的时候需要关闭wifi
            wifi.setWifiEnabled(false);
        }
        try {
            // 热点的配置类
            WifiConfiguration apConfig = new WifiConfiguration();
            // 配置热点的名称(可以在名字后面加点随机数什么的)
            apConfig.SSID = "Chappie_" + getDeviceIMEI();
            String PWSS = "12345678";
            // 配置热点的密码
            apConfig.preSharedKey = PWSS;
            apConfig.hiddenSSID = false;
            apConfig.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
            apConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            apConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            apConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            apConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            apConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            apConfig.status = WifiConfiguration.Status.ENABLED;
            // 通过反射调用设置热点
            Method method = wifi.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, Boolean.TYPE);
            // 返回热点打开状态
            return (Boolean) method.invoke(wifi, apConfig, enable);
        } catch (Exception e) {
            return false;
        }
    }


    private void warnIfNoExternalStorage() {
        String storageState = Environment.getExternalStorageState();
        if (!storageState.equals(Environment.MEDIA_MOUNTED)) {
            Toast toast = Toast.makeText(this, R.string.storage_warning, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.openheat: {
                flag = !flag;
                setWifiApEnabled(flag);// 开启热点
                //        startWifiFtpServer();
                break;
            }
            case R.id.getphone: {
                ArrayList<String> connectedIP = getConnectedIP();
                StringBuilder resultList = new StringBuilder();
                for (String ip : connectedIP) {
                    resultList.append(ip);
                    resultList.append("\n");
                }
                Log.e(TAG, "连接到手机上的Ip是：" + resultList.toString());
                break;
            }
            case R.id.startsocket: {
                ArrayList<String> connectedIP = getConnectedIP();
                for (String ip : connectedIP) {
                    if (ip.contains(".")) {
                        new SendMsgDataThread(ip, "ssssssssssssssssssssssss").start();// 消息发送方启动线程发送消息
                    }
                }
                break;
            }
            case R.id.sendTestVideo: {   // 发送测试视频到客户端
                ArrayList<String> connectedIP = getConnectedIP();
                for (String ip : connectedIP) {
                    if (ip.contains(".")) {
                        // new SendFileDataThread(ip, 12347, "/storage/sdcard1/ChappieLauncher/video/2016-03-24/lock_18-03-56.3gp").start();// 消息发送方启动线程发送消息
                    }
                }
                break;
            }
            default: {
                break;
            }
        }
    }

    /**
     * 获取到连接到热点手机的客户端IP
     *
     * @return
     */
    private ArrayList<String> getConnectedIP() {// 获取连接到本机热点上的手机ip
        ArrayList<String> connectedIP = new ArrayList<String>();
        try {
            BufferedReader br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                String[] splitted = line.split(" +");
                if (splitted != null && splitted.length >= 4) {
                    String ip = splitted[0];
                    connectedIP.add(ip);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connectedIP;
    }

    @Override
    public void onDestroy() {
        unbindService(sc);
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
        super.onDestroy();
    }

    /**
     * 开启FTP服务
     */
    private void startWifiFtpServer() {
        Intent intent = new Intent(this, FTPServerService.class);
        if (!isFTPServerStart) {
            if (!FTPServerService.isRunning()) {
                warnIfNoExternalStorage();
                startService(intent);
                Log.e(TAG, "启动了Wifi FTP 服务");
                Toast.makeText(this, "启动了Wifi FTP 服务", Toast.LENGTH_SHORT).show();
                isFTPServerStart = true;
            }
        } else {
            stopService(intent);
            Log.e(TAG, "停止Wifi FTP 服务");
            Toast.makeText(this, "停止Wifi FTP 服务", Toast.LENGTH_SHORT).show();
            isFTPServerStart = false;
        }
    }


}

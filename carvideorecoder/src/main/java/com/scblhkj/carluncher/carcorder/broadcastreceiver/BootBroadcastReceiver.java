package com.scblhkj.carluncher.carcorder.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.scblhkj.carluncher.carcorder.service.DriveVideoRecordService;

/**
 * Created by Leo on 2016/2/24.
 * 监听开机广播
 */
public class BootBroadcastReceiver extends BroadcastReceiver {

    static final String ACTION = "android.intent.action.BOOT_COMPLETED";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION)) {
            startDriveRecordService(context);
            startCarCollisionDetectionServoce(context);
        }
    }

    /**
     * 启动行车记录服务
     */
    private void startDriveRecordService(Context context) {
        Intent intent = new Intent(context, DriveVideoRecordService.class);
        context.startService(intent);
    }

    /**
     * 开启碰撞检测服务
     */
    private void startCarCollisionDetectionServoce(Context context) {
        Intent intent = new Intent();
        intent.setAction("com.scblhkj.carluncher.carcorder.service.CarCollisionDetectionService");
        intent.setPackage("com.scblhkj.carluncher.carcorder");  //Android5.0后service不能采用隐式启动，故此处加上包名
        context.startService(intent);
    }

}

package com.scblhkj.carluncher.carcorder.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.scblhkj.carluncher.carcorder.R;
import com.scblhkj.carluncher.carcorder.app.MyApplication;
import com.scblhkj.carluncher.carcorder.common.Constant;
import com.scblhkj.carluncher.carcorder.widget.RecordView;


/**
 * 行车记录仪录制的后台服务
 */
public class DriveVideoRecordService extends Service implements RecordView.OnRecordViewInitSuccessListener, View.OnClickListener {

    private static final String TAG = "视频录制服务";
    private WindowManager.LayoutParams params;
    private View sView;
    private WindowManager windowManager;
    private RecordView ll_video;
    private BroadcastReceiver controlRecordReceiver;
    public static final String MAX_WINDOW = "DRIVEVIDEORECORDSERVICE.MAX_WINDOW";
    public static final String MIN_WINDOW = "DRIVEVIDEORECORDSERVICE.MIN_WINDOW";
    public static final String HALF_WINDOW = "DRIVEVIDEORECORDSERVICE.HALF_WINDOW";
    public static final String TAKE_MAIN_PICTRUE = "DRIVEVIDEORECORDSERVICE.TAKE_MAIN_PICTRUE";   // 主摄像头拍照
    public static final String RESTART_MAIN_RECORD = "DRIVEVIDEORECORDSERVICE.RESTART_MAIN_RECORD"; // 重新开始主摄像头的录制
    public static final String TAKE_SECOND_PICTRUE = "DRIVEVIDEORECORDSERVICE.TAKE_SECOND_PICTRUE";   // 副摄像头拍照
    public static final String RESTART_SECOND_RECORD = "DRIVEVIDEORECORDSERVICE.RESTART_SECOND_RECORD"; // 重新开始副像头的录制
    public static final String RESTART_BACK_VIEW = "DRIVEVIDEORECORDSERVICE.RESTART_BACK_VIEW";  // 切换到到车后视


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        initControlRecordReceiver();
        registerControlRecordReceiver();
        smallWindowManager();
        maxWindowManager();
        Log.e(TAG, "录制服务启动");
        super.onCreate();
    }

    /**
     * 初始化行车记录控制广播接收者
     */
    private void initControlRecordReceiver() {
        controlRecordReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch (action) {
                    case MAX_WINDOW: {
                        maxWindowManager();
                        break;
                    }
                    case MIN_WINDOW: {
                        smallWindowManager();
                        break;
                    }
                    case HALF_WINDOW: {
                        halfWindowManager();
                        break;
                    }
                    case TAKE_MAIN_PICTRUE: {
                        takeMainPicture();
                        break;
                    }
                    case RESTART_MAIN_RECORD: {
                        restartSecondMainVideo();
                        break;
                    }
                    case RESTART_SECOND_RECORD: {
                        restartSecondRecordVideo();
                        break;
                    }
                    case TAKE_SECOND_PICTRUE: {
                        takeSecondPictrue();
                        break;
                    }
                    case RESTART_BACK_VIEW: {
                        // 进入倒车后视模式
                        ll_video.switch2BackView();
                        break;
                    }
                    case Constant.CAR_BACK_CUSTOM_KEY_EVENT: {
                        String flag = intent.getStringExtra("key_flag");
                        switch (flag) {
                            case Constant.CARBACKA: {
                                maxWindowManager();
                                // 进入倒车后视模式
                                ll_video.switch2BackView();
                                break;
                            }
                            case Constant.CARBACKB: {
                                // 退出倒车后视模式
                                smallWindowManager();
                                break;
                            }

                        }
                    }
                }
            }
        };
    }


    /**
     * 放大至窗口长度一半的界面
     */
    private void halfWindowManager() {
        params.x = 0;
        params.y = 0;
        //设置悬浮窗口长宽数据
        params.width = MyApplication.SCREEN_WIDTH / 2;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        windowManager.updateViewLayout(sView, params);
        if (sView != null) {
            sView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        }
    }


    /**
     * 注册视频录制广播接收者
     */
    private void registerControlRecordReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        intentFilter.addAction(MAX_WINDOW);
        intentFilter.addAction(MIN_WINDOW);
        intentFilter.addAction(HALF_WINDOW);
        intentFilter.addAction(TAKE_MAIN_PICTRUE);
        intentFilter.addAction(RESTART_MAIN_RECORD);
        intentFilter.addAction(TAKE_SECOND_PICTRUE);
        intentFilter.addAction(RESTART_SECOND_RECORD);
        intentFilter.addAction(RESTART_BACK_VIEW);
        intentFilter.addAction(Constant.CAR_BACK_CUSTOM_KEY_EVENT); // 倒车广播
        registerReceiver(controlRecordReceiver, intentFilter);
    }


    private void unRegisterControlRecordReceiver() {
        if (controlRecordReceiver != null) {
            unregisterReceiver(controlRecordReceiver);
        }
    }

    @Override
    public void onDestroy() {
        unRegisterControlRecordReceiver();
        super.onDestroy();
    }


    @Override
    public void OnRecordViewInitSuccess() {
        ll_video.record(0);
        Log.i(TAG, "视频记录控件初始化成功");
    }

    /**
     * 初始化WindowManager
     */
    private void maxWindowManager() {
        Log.e(TAG, "放大录制窗口");
        // 以屏幕左上角为原点，设置x、y初始值，相对于gravity
        params.x = 0;
        params.y = 0;
        //设置悬浮窗口长宽数据
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        windowManager.updateViewLayout(sView, params);
        if (sView != null) {
            sView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        }
    }

    /**
     * 隐藏
     */
    private void smallWindowManager() {
        if (windowManager == null && params == null) {
            windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            params = new WindowManager.LayoutParams();
            params.type = WindowManager.LayoutParams.TYPE_PHONE;  //这种模式下可以使录制窗口在屏幕最顶端
            params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE; //标志窗口不获取焦点，使录制窗口之外的其他地方可点击
            //设置图片格式，效果为背景透明
            params.format = PixelFormat.RGBA_8888;
            //调整悬浮窗显示的停靠位置为左侧置顶
            params.gravity = Gravity.LEFT | Gravity.TOP;
            // 以屏幕左上角为原点，设置x、y初始值，相对于gravity
            params.x = 0;
            params.y = 0;
            //设置悬浮窗口长宽数据
            params.width = 2;
            params.height = 2;
            sView = View.inflate(this, R.layout.fragment_drive_record, null);
            windowManager.addView(sView, params);
            sView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            ll_video = (RecordView) sView.findViewById(R.id.ll_video);

            im_switchCamera = (ImageView) sView.findViewById(R.id.im_switchCamera);
            im_tp = (ImageView) sView.findViewById(R.id.im_tp);
            im_moreFunction = (ImageView) sView.findViewById(R.id.im_moreFunction);
            rl_choiceCamera = (RelativeLayout) sView.findViewById(R.id.rl_choiceCamera);
            ll_mainCamera = (LinearLayout) sView.findViewById(R.id.ll_mainCamera);
            ll_secondCamera = (LinearLayout) sView.findViewById(R.id.ll_secondCamera);
            ll_backCamera = (LinearLayout) sView.findViewById(R.id.ll_backCamera);
            ll_video.setOnRecordViewInitSuccessListener(this);

            im_switchCamera.setOnClickListener(this);
            im_tp.setOnClickListener(this);
            im_moreFunction.setOnClickListener(this);
            ll_mainCamera.setOnClickListener(this);
            ll_secondCamera.setOnClickListener(this);
            ll_backCamera.setOnClickListener(this);
            rl_choiceCamera.setOnClickListener(this);
        } else {
            // 以屏幕左上角为原点，设置x、y初始值，相对于gravity
            params.x = 0;
            params.y = 0;
            params.width = 2;
            params.height = 2;
            windowManager.updateViewLayout(sView, params);
            if (sView != null) {
                sView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            }
        }
    }

    private ImageView im_switchCamera;  // 切换摄像头
    private ImageView im_tp; // 拍照
    private ImageView im_moreFunction; // 更多
    private RelativeLayout rl_choiceCamera; // 点击更多弹出的选择摄像头的布局
    private LinearLayout ll_mainCamera;   // 主摄像头
    private LinearLayout ll_secondCamera;  // 副摄像头
    private LinearLayout ll_backCamera;  // 倒车摄像头

    /**
     * 主摄像头拍摄照片
     */
    public void takeMainPicture() {
        if (ll_video != null) {
            ll_video.takeMainPicture(); // 住摄像头拍照
        }
    }

    /**
     * 副摄像头拍摄照片
     */
    public void takeSecondPictrue() {
        if (ll_video != null) {
            ll_video.takeSecondPicture(); // 副摄像头拍照
        }
    }


    /**
     * 重启副摄像头录像
     */
    public void restartSecondRecordVideo() {
        if (ll_video != null) {
            ll_video.restartSecondRecordVideo();
        }
    }


    /**
     * 重启主摄像头录像
     */
    public void restartSecondMainVideo() {
        if (ll_video != null) {
            ll_video.restartMainVideo();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.im_switchCamera: {  // 切换摄像头
                if (Constant.CAMERA_MAIN == ll_video.currentCamera) {  // 当前为主摄像
                    ll_video.restartSecondRecordVideo();  // 开始副摄像头录制
                } else if (Constant.CAMERA_CECOND == ll_video.currentCamera) {
                    ll_video.restartMainVideo(); // 开始主摄像头录制
                }
                break;
            }
            case R.id.im_tp: {  // 拍照
                if (Constant.CAMERA_MAIN == ll_video.currentCamera) {
                    ll_video.takeMainPicture();
                } else if (Constant.CAMERA_CECOND == ll_video.currentCamera) {
                    ll_video.takeSecondPicture();
                }
                break;
            }
            case R.id.im_moreFunction: { // 更多
                rl_choiceCamera.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.ll_mainCamera: {  // 切换到主摄像头
                ll_video.restartMainVideo();  // 开始主摄像头录制
                rl_choiceCamera.setVisibility(View.GONE);
                break;
            }
            case R.id.ll_secondCamera: { // 切换到副摄像头
                ll_video.restartSecondRecordVideo();
                rl_choiceCamera.setVisibility(View.GONE);
                break;
            }
            case R.id.ll_backCamera: { // 切换到倒车后视
                ll_video.switch2BackView();
                rl_choiceCamera.setVisibility(View.GONE);
            }
            case R.id.rl_choiceCamera: {
                rl_choiceCamera.setVisibility(View.GONE);
            }
        }
    }
}

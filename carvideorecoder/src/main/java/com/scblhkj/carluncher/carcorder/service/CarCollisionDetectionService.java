package com.scblhkj.carluncher.carcorder.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.scblhkj.carluncher.carcorder.common.Constant;
import com.scblhkj.carluncher.carcorder.utils.SpUtil;
import com.scblhkj.carluncher.carcorder.utils.ToastUtil;
import com.scblhkj.carluncher.carcorder.widget.RecordView;


/**
 * 检测汽车碰撞的后台服务
 */
public class CarCollisionDetectionService extends Service {

    private static final int START_CRASH = 100;
    // 延时屏蔽三秒之内的重复碰撞检测
    private static final int START_CRASH_DELAY = 3000;
    // 上次监测的时间
    private long lastUpdateTime = 0;
    // 传感器类对象
    private SensorManager sm;
    // 加速度传感器
    private Sensor acceleromererSensor;
    private float lastX = 0;
    private float lastY = 0;
    private float lastZ = 0;
    // 是否正在监测碰撞
    private boolean isCrash = false;
    // 震动检测到的次数
    private int crashCount = 0;
    // 震动级别
    private int collisionLevel;
    // 服务已关闭
    private boolean shutdownService;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case START_CRASH:
                    isCrash = false;
                    break;

                default:
                    break;
            }
            super.handleMessage(msg);
        }
    };

    public static final String TEMP_COLLISON_TIME = "TEMP_COLLISON_TIME";
    private BroadcastReceiver receiver;


    private void initBroadCastReceiver() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch (action) {
                    case Constant.ACTION_REMOTE_COLLISION_SERVICE: {
                        String level = intent.getStringExtra(Constant.ACTION_REMOTE_COLLISION_SERVICE_VALUE);
                        collisionLevel = Integer.parseInt(level);
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        };
    }

    /**
     * 注册广播接收者
     */
    private void registerBroadCastReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        intentFilter.addAction(Constant.ACTION_REMOTE_COLLISION_SERVICE);
        intentFilter.addAction(Constant.CAR_BACK_CUSTOM_KEY_EVENT); // 倒车广播
        registerReceiver(receiver, intentFilter);
    }

    /**
     * 注销广播接收者
     */
    private void unRegisterBroadCastReceiver() {
        if (receiver != null) {
            unregisterReceiver(receiver);
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
        shutdownService = false;
        Log.e(TAG, "震动检测服务启动");
        // 获取传感器管理器类
        sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        // 获取加速度传感器类
        acceleromererSensor = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        // 注册监听器
        sm.registerListener(sensorEventListener, acceleromererSensor, SensorManager.SENSOR_DELAY_UI);
        initBroadCastReceiver();
        registerBroadCastReceiver();
        initcollisionLevel();
    }

    /**
     * 初始化碰撞监测的值
     */
    private void initcollisionLevel() {
        String level = SpUtil.getString2SP(Constant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_VIBRATION_MONITORING_LEVEL);
        if (0 == Integer.parseInt(level)) {
            collisionLevel = 10;
        } else {
            collisionLevel = Integer.parseInt(level);
        }
    }

    public CarCollisionDetectionService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        unRegisterBroadCastReceiver();
        shutdownService = true;
        Log.e(TAG, "关闭了震动监测服务");
        super.onDestroy();
    }

    /**
     * 加速度传感器监听类
     */
    private SensorEventListener sensorEventListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {
            if (shutdownService) {
                float x = event.values[SensorManager.DATA_X];
                float y = event.values[SensorManager.DATA_Y];
                float z = event.values[SensorManager.DATA_Z];
                // 现在检测时间
                long currentUpdateTime = System.currentTimeMillis();
                // 两次检测的时间间隔
                long timeInterval = currentUpdateTime - lastUpdateTime;
                // 现在的时间变成last时间
                lastUpdateTime = currentUpdateTime;
                // 获得x,y,z的变化值
                final float deltaX = x - lastX;
                final float deltaY = y - lastY;
                final float deltaZ = z - lastZ;
                // 将现在的坐标变成last坐标
                lastX = x;
                lastY = y;
                lastZ = z;
                // 速度值
                final double speed = Math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ);
                if (speed > collisionLevel) {  // 震动监测的告警级别
                    if (!isCrash) {
                        isCrash = true;
                        sendCarCollisionBroacast();
                    }
                    Message msg = Message.obtain();
                    msg.what = START_CRASH;
                    handler.sendMessageDelayed(msg, START_CRASH_DELAY);
                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };


    private static final String TAG = "震动监测服务";

    /**
     * 发送汽车发送碰撞的时间戳
     */
    private void sendCarCollisionBroacast() {
        if (crashCount == 0) {
            Log.i(TAG, "屏蔽第一次震动临界值");
        } else {
            Intent intent = new Intent();
            intent.setAction(RecordView.CAR_COLLISION_ACTION);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            Bundle bundle = new Bundle();
            bundle.putLong(CarCollisionDetectionService.TEMP_COLLISON_TIME, System.currentTimeMillis());
            intent.putExtras(bundle);
            sendBroadcast(intent);
            ToastUtil.showToast("检测到汽车震动了,并发送了震动的广播消息给了录制服务");
        }
        crashCount++;
    }

}

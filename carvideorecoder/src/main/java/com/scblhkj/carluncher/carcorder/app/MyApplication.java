package com.scblhkj.carluncher.carcorder.app;

import android.app.Activity;
import android.app.Application;

import com.scblhkj.carluncher.carcorder.common.UnCeHandler;
import com.scblhkj.carluncher.carcorder.utils.MeasureScreenUtil;
import com.scblhkj.carluncher.carcorder.utils.RemoteSpUtil;
import com.scblhkj.carluncher.carcorder.utils.SpUtil;
import com.scblhkj.carluncher.carcorder.utils.ToastUtil;

import java.util.ArrayList;

/**
 * Created by Leo on 2016/2/24.
 */
public class MyApplication extends Application {
    public static int SCREEN_WIDTH = 0;
    public static int SCREEN_HEIGHT = 0;
    public static boolean isDebug = false;
    ArrayList<Activity> list = new ArrayList<Activity>();
    @Override
    public void onCreate() {
        super.onCreate();
        ToastUtil.init(this);
        SpUtil.init(this);
        RemoteSpUtil.init(this);
        MeasureScreenUtil.init(this);
        MeasureScreenUtil.getScreenSize();
        MyApplication.SCREEN_WIDTH = MeasureScreenUtil.width;
        MyApplication.SCREEN_HEIGHT = MeasureScreenUtil.height;
        UnCeHandler catchExcep = new UnCeHandler(this);
        Thread.setDefaultUncaughtExceptionHandler(catchExcep);
    }

    /**
     * 关闭Activity列表中的所有Activity*/
    public void finishActivity(){
        for (Activity activity : list) {
            if (null != activity) {
                activity.finish();
            }
        }
        //杀死该应用进程
        android.os.Process.killProcess(android.os.Process.myPid());
    }
}

package com.scblhkj.carluncher.carcorder.utils;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;

import java.io.File;

/**
 * Created by Leo on 2016/1/22.
 */
public class StorageCapacityUtil {

    private Context context;

    public StorageCapacityUtil(Context context) {
        this.context = context;
    }

    /**
     * 获取手机内存可用空间大小
     *
     * @return
     */
    public long getSDCardAvailableSpace() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File sdcardDir = Environment.getExternalStorageDirectory();
            StatFs sf = new StatFs(sdcardDir.getPath());
            long blockSize = sf.getBlockSize();
            long availCount = sf.getAvailableBlocks();
            return availCount * blockSize;
        }
        return 0;
    }

    /**
     * 获取手机内存中视频文件的大小
     *
     * @return
     */
    public long getSDCardVideoSize() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File file = new File(Environment.getExternalStorageDirectory(), "ChappieLauncher/" + "video");
            return FileUtil.getDirSize(file);
        }
        return 0;
    }

    /**
     * 获取手机内存中图片文件的大小
     *
     * @return
     */
    public long getSDCardPicSize() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File file = new File(Environment.getExternalStorageDirectory(), "ChappieLauncher/" + "picture");
            return (long) FileUtil.getDirSize(file);
        }
        return 0;
    }

    /**
     * 获取SD卡其他空间
     *
     * @return
     */
    public long getSDOtherSize() {
        return getSDTotalSize() - getSDCardVideoSize() - getSDCardPicSize();
    }

    /**
     * 获取手机内存中总内存
     *
     * @return
     */
    public long getSDTotalSize() {
        File path = Environment.getExternalStorageDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return blockSize * totalBlocks / 1024 / 1024 / 1024;
    }

}

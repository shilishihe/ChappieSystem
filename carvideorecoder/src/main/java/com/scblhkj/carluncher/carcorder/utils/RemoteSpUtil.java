package com.scblhkj.carluncher.carcorder.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;

import com.scblhkj.carluncher.carcorder.common.Constant;

/**
 * Created by Leo on 2016/2/25.
 * 远程SPUtils
 * 通过SP和桌面应用贡共享数据
 */
public class RemoteSpUtil {

    private static Context remoteContext;
    private static SharedPreferences remoteSP;
    private static SharedPreferences.Editor ed;

    public static void init(Context context) {
        try {
            remoteContext = context.createPackageContext(Constant.LAUCHER_PACKAGE, Context.CONTEXT_IGNORE_SECURITY);
            remoteSP = remoteContext.getSharedPreferences(Constant.LAUCHER_SP_CONFIG, Constant.REMOTE_SP_MODE);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据指定的key获取值
     *
     * @param key
     * @return
     */
    public static Boolean getBoolean2SP(String key) {
        return remoteSP.getBoolean(key, false);
    }

    public static String getString2SP(String key) {
        return remoteSP.getString(key, "0");
    }


    public static void saveInt2SP(String key, int i) {
        ed.putInt(key, i);
        ed.commit();
    }

    private static String TAG = "SpUtil";

    public static int getInt2SP(String key) {
        return remoteSP.getInt(key, 0);
    }

    /**
     * 存储key value
     *
     * @param key
     * @param value
     */
    public static void saveString2SP(String key, String value) {
        ed.putString(key, value);
        ed.commit();
    }

    /**
     * 存储key value
     *
     * @param key
     * @param value
     */
    public static void saveBoolean2SP(String key, Boolean value) {
        ed.putBoolean(key, value);
        ed.commit();
    }

    public static void saveFloat2SP(String key, Float value) {
        ed.putFloat(key, value);
        ed.commit();
    }

    public static float getFloat2SP(String key) {
        return remoteSP.getFloat(key, 0);
    }


}

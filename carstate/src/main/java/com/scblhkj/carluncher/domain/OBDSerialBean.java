package com.scblhkj.carluncher.domain;

import java.io.Serializable;

/**
 * Created by Leo on 2015/11/28.
 * OBD串口数据实体
 */
public class OBDSerialBean implements Serializable {

    private static final long serialVersionUID = 2L;
    private int[][] datass;
    private int flag;

    public int[][] getDatass() {
        return datass;
    }

    public void setDatass(int[][] datass) {
        this.datass = datass;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }
}

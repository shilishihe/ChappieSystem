package com.scblhkj.carluncher.carstate.constant;

/**
 * Created by Leo on 2016/3/1.
 * 常量类
 */
public interface Constant {

    String SERIAL_ACTION_RECEIVE = "com.scblhkj.chappie.carlauncher.receive.serialdata"; // 串口中接收数据指令的广播Action
    String SERIAL_CMD = "SERIAL_CMD";  // 串口接收数据的key
    String GPS_INFO = "android.intent.action.gps.info";
    String SERIAL_OBD_ACTION = "com.scblhkj.chappie.carlauncher.obd.serialdata";
    String SERIAL_OBD_DATA = "SERIAL_OBD_DATA";
}

package com.scblhkj.carluncher.carstate.util;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.scblhkj.carluncher.carstate.constant.Constant;

/**
 * Created by Leo on 2015/12/18.
 * OBD相关功能工具类
 */
public class ObdSerialUtil {

    /**
     * 从sp中取出obd序列号
     *
     * @return
     */
    public static byte[] getObdSerialId2SP() {
        byte[] serID = new byte[6];

            serID[0] = serID[1] = serID[2] = serID[3] = serID[4] = serID[5] = 1;

        return serID;
    }

    /**
     * 向串口发送数据
     */
    public static void sendData2Port(Context context, byte[] bs) {
        Intent dataIntent = new Intent();
        dataIntent.setAction(Constant.SERIAL_ACTION_RECEIVE);
        dataIntent.addCategory(Intent.CATEGORY_DEFAULT);
        Bundle bundle = new Bundle();
        bundle.putByteArray(Constant.SERIAL_CMD, bs);
        dataIntent.putExtras(bundle);
        context.sendBroadcast(dataIntent);
    }

    /**
     * 向串口发送指令
     */
    public static void sendOBDSerialCMD(Context context, byte i) {
        byte ids[] = getObdSerialId2SP();
        byte[] cmd = new byte[40];
        //cmd
        cmd[0] = (byte) 0x01;
        cmd[1] = (byte) 0xff;
        cmd[2] = (byte) 0x03;
        cmd[3] = (byte) 0x00;  //数据长度
        cmd[4] = (byte) 0x28;
        for (int m = 0; m < ids.length; m++) {
            cmd[m + 5] = ids[m];
        }
        cmd[11] = 0x00;
        cmd[13] = 0x00;

        switch (i) {
            case 0X01: {  // 软件版本号
                cmd[12] = i;
                cmd[14] = 0x01; //数据长度
                cmd[3] = (byte) (cmd[14] + (byte) 13);
                cmd[15] = 0x00; //没有特殊意义
                cmd[16] = getXORValue(cmd);
                cmd[17] = 0x29;
                break;
            }
            case 0X02: {  //重启模块
                cmd[12] = i;
                cmd[14] = 0x01; //数据长度
                cmd[3] = (byte) (cmd[14] + (byte) 13);
                cmd[15] = 0x01; //重启设备
                cmd[16] = getXORValue(cmd);
                cmd[17] = 0x29;
                break;
            }
            case 0X06: {  //设置通用数据流
                break;
            }
            case 0X07: {  //请求自定义数据流
                cmd[12] = i;
                cmd[14] = 0x01; //数据长度
                cmd[3] = (byte) (cmd[14] + (byte) 13);
                cmd[15] = 0x00; //查询厂家自定义数据流
                cmd[16] = getXORValue(cmd);
                cmd[17] = 0x29;
                break;
            }
            case 0X08: { //设置OBD数据流的间隔时间
             /*cmd[12] = i;
             cmd[14] = 0x01; //数据长度
             cmd[3] = (byte) (cmd[14] + (byte) 13);
             cmd[15] = 0x00; //查询厂家自定义数据流
             cmd[16] = getXORValue(cmd);
             cmd[17] = 0x29;*/
                break;
            }
            case 0X09: { //查询PID数据流
                break;
            }
            case 0X0A: { //车辆体检
                cmd[12] = i;
                cmd[14] = 0x01; //数据长度
                cmd[3] = (byte) (cmd[14] + (byte) 13);
                cmd[15] = 0x00; //车辆体检
                cmd[16] = getXORValue(cmd);
                cmd[17] = 0x29;
                break;
            }
            case 0X0C: { //查询车辆vin码  (要发送两条查询语句，才能查到VIN码,待解决)
                cmd[12] = i;
                cmd[14] = 0x01; //数据长度
                cmd[3] = (byte) (cmd[14] + (byte) 13);
                cmd[15] = 0x00; //vin码
                cmd[16] = getXORValue(cmd);
                cmd[17] = 0x29;
                break;
            }
            case 0X0D: { //发送机故障代码
                cmd[12] = i;
                cmd[14] = 0x01;
                cmd[3] = (byte) (cmd[14] + (byte) 13);
                cmd[15] = 0x00;
                cmd[16] = getXORValue(cmd);
                cmd[17] = 0x29;
                break;
            }
            case 0X0E: { //清除发送机故障代码  (发送后无返回，待解决)
                cmd[12] = i;
                cmd[14] = 0x01; //数据长度
                cmd[3] = (byte) (cmd[14] + (byte) 13);
                cmd[15] = 0x01;
                cmd[16] = getXORValue(cmd);
                cmd[17] = 0x29;
                break;
            }
            case 0X0F: { //设置里程
                break;
            }
            case 0X10: { //请求模块上传累计驾驶数据
                cmd[12] = i;
                cmd[14] = 0x01; //数据长度
                cmd[3] = (byte) (cmd[14] + (byte) 13);
                cmd[15] = 0x00;
                cmd[16] = getXORValue(cmd);
                cmd[17] = 0x29;
                break;
            }
            case 0X12: { //设置报警参数

                break;
            }
            default: {
                break;
            }
        }
        sendData2Port(context, cmd);
    }


    /**
     * 异或
     *
     * @param bs
     * @return
     */
    public static byte getXORValue(byte[] bs) {
        byte v = bs[11];
        for (int j = 0; j < bs[14] + 3; j++) {
            v = (byte) (v ^ bs[j + 12]);
        }
        return v;
    }
}

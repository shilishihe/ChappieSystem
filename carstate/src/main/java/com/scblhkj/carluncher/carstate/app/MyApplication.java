package com.scblhkj.carluncher.carstate.app;

import android.app.Application;

import com.scblhkj.carluncher.carstate.util.SpUtil;
import com.scblhkj.carluncher.carstate.util.ToastUtil;


/**
 * Created by Leo on 2016/2/29.
 */
public class MyApplication extends Application {

    public static boolean isDebug = false;

    @Override
    public void onCreate() {
        super.onCreate();
        ToastUtil.init(this);
        SpUtil.init(this);
    }
}

package com.scblhkj.carluncher.carstate.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

import com.scblhkj.carluncher.carstate.R;
import com.scblhkj.carluncher.carstate.constant.Constant;
import com.scblhkj.carluncher.domain.OBDSerialBean;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by blh on 2016-07-19.
 */
public class CarStateDetailActivity extends BaseActivity {
    private TextView tvCalculatedValue,tvEngineCoolant,tvTransmitterSpeed,tvCarSpeed,tvIgnitionAdvanceFeet,tvManifoldAbsolutePressure,tvTotalMileage;
    private TextView tvVoltageControlModule,tvInletTemperature,tvAirFlow,tvThrottlePosition,tvFuelCorrection,tvRatioCoefficient,tvAbPosition,tvFuelPressure,instantaneousFuelConsumption;
    private BroadcastReceiver serialCommReceiver;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    private int carSpeed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.car_state_detailinfo);
        sp=getSharedPreferences("deskstate", Activity.MODE_PRIVATE);
        editor=sp.edit();
        initView();
        initData();
    }


    @Override
    protected void initView() {
        tvCalculatedValue=(TextView)findViewById(R.id.tv_calculated_value);
        tvEngineCoolant=(TextView)findViewById(R.id.tv_engine_coolant);
        tvTransmitterSpeed=(TextView)findViewById(R.id.tv_transmitter_speed);
        tvCarSpeed=(TextView)findViewById(R.id.tv_car_speed);
        tvIgnitionAdvanceFeet=(TextView)findViewById(R.id.tv_ignition_advance_feet);
        tvManifoldAbsolutePressure=(TextView)findViewById(R.id.tv_manifold_absolute_pressure);
        tvVoltageControlModule=(TextView)findViewById(R.id.tv_voltage_control_module);
        tvInletTemperature=(TextView)findViewById(R.id.tv_inlet_temperature);
        tvAirFlow=(TextView)findViewById(R.id.tv_air_flow);
        tvThrottlePosition=(TextView)findViewById(R.id.tv_throttle_position);
        tvFuelCorrection=(TextView)findViewById(R.id.tv_fuel_correction);
        tvRatioCoefficient=(TextView)findViewById(R.id.tv_ratio_coefficient);
        tvAbPosition=(TextView)findViewById(R.id.tv_ab_position);
        tvFuelPressure=(TextView)findViewById(R.id.tv_fuel_pressure);
        instantaneousFuelConsumption=(TextView)findViewById(R.id.instantaneous_fuel_consumption);
        tvTotalMileage=(TextView)findViewById(R.id.tv_total_mileage);

    }

    @Override
    protected void initData() {
        initBroadcastReceiver();
        registerSerialCommReceiver();
    }

    /**
     * 注册串口通信的广播
     */
    private void registerSerialCommReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constant.SERIAL_OBD_ACTION);  // OBD数据广播
        intentFilter.addAction(Constant.GPS_INFO); // 车辆位置广播
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(serialCommReceiver, intentFilter);
    }

    /**
     * 注销串口通信的广播
     */
    private void unRegisterSerialCommReceiver() {
        if (serialCommReceiver != null) {
            unregisterReceiver(serialCommReceiver);
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        unRegisterSerialCommReceiver();
    }

    private void initBroadcastReceiver() {
        // 初始化串口通信的接收者
        serialCommReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case Constant.SERIAL_OBD_ACTION: {
                        OBDSerialBean bean = (OBDSerialBean) intent.getExtras().getSerializable(Constant.SERIAL_OBD_DATA);
                        int[][] datass = bean.getDatass();   // OBD数据
                        int length = datass.length;
                        for (int i = 0; i < length; i++) {
                            if (datass[i][0] == 0X28) {
                                if (datass[i][8] == 0x87) {
                                    tvCalculatedValue.setText(String.valueOf(datass[i][11] * 100 / 255f));
                                    tvEngineCoolant.setText(String.valueOf(datass[i][12] - 40) + "℃");
                                    tvTransmitterSpeed.setText(String.valueOf(datass[i][13] * 256 + datass[i][14] / 4) + "R/MIN");
                                    tvCarSpeed.setText(String.valueOf(datass[i][15]) + "KM/H");
                                    carSpeed = datass[i][15];
                                    tvIgnitionAdvanceFeet.setText(String.valueOf(datass[i][16] - 64) + "°");
                                    tvManifoldAbsolutePressure.setText(String.valueOf(datass[i][17]) + "kPa");
                                    tvVoltageControlModule.setText(String.valueOf(datass[i][18] / 10f) + "V");
                                    tvInletTemperature.setText(String.valueOf(datass[i][19] - 40) + "℃");
                                    tvAirFlow.setText(String.valueOf((datass[i][20] * 256 + (datass[i][21])) / 1000f) + "L/S");
                                    tvThrottlePosition.setText(String.valueOf(datass[i][22] * 100 / 255f));
                                    tvFuelCorrection.setText(String.valueOf((datass[i][23] - 128) * 100 / 128f) + "%");
                                    tvRatioCoefficient.setText(String.valueOf((datass[i][24] * 256 + datass[i][25]) * 0.0000305f) + "A/F");
                                    tvAbPosition.setText(String.valueOf(datass[i][26] * 100 / 255f));
                                    tvFuelPressure.setText(String.valueOf(datass[i][27] * 3) + "kPa");
                                    instantaneousFuelConsumption.setText(String.valueOf((datass[i][28] * 256 + datass[i][29]) / 10f) + "L/100Km");
                                    tvTotalMileage.setText(String.valueOf(((((long) datass[i][30] * 256 + (long) datass[i][31]) * 256 + (long) datass[i][32]) * 256 + (long) datass[i][33])) + "KM");
                                    //hasData = true;
                                    editor.putBoolean("hasData",true);
                                    editor.commit();

                                }
                            }
                        }
                        break;
                    }
                    default: {
                        break;
                    }
                }

            }
        };
    }
}

package com.scblhkj.carluncher.carstate.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scblhkj.carluncher.carstate.R;
import com.scblhkj.carluncher.carstate.activity.MainActivity;
import com.scblhkj.carluncher.carstate.constant.Constant;
import com.scblhkj.carluncher.domain.OBDSerialBean;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 车辆状态详情界面
 */
public class CarStateDescFragment extends Fragment {

    @Bind(R.id.tv_calculated_value)
    TextView tvCalculatedValue;
    @Bind(R.id.tv_engine_coolant)
    TextView tvEngineCoolant;
    @Bind(R.id.tv_transmitter_speed)
    TextView tvTransmitterSpeed;
    @Bind(R.id.tv_car_speed)
    TextView tvCarSpeed;
    @Bind(R.id.tv_ignition_advance_feet)
    TextView tvIgnitionAdvanceFeet;
    @Bind(R.id.tv_manifold_absolute_pressure)
    TextView tvManifoldAbsolutePressure;
    @Bind(R.id.tv_voltage_control_module)
    TextView tvVoltageControlModule;
    @Bind(R.id.tv_inlet_temperature)
    TextView tvInletTemperature;
    @Bind(R.id.tv_air_flow)
    TextView tvAirFlow;
    @Bind(R.id.tv_throttle_position)
    TextView tvThrottlePosition;
    @Bind(R.id.tv_fuel_correction)
    TextView tvFuelCorrection;
    @Bind(R.id.tv_ratio_coefficient)
    TextView tvRatioCoefficient;
    @Bind(R.id.tv_ab_position)
    TextView tvAbPosition;
    @Bind(R.id.tv_fuel_pressure)
    TextView tvFuelPressure;
    @Bind(R.id.instantaneous_fuel_consumption)
    TextView instantaneousFuelConsumption;
    @Bind(R.id.tv_total_mileage)
    TextView tvTotalMileage;
    private View view;

    private BroadcastReceiver serialCommReceiver;
    protected MainActivity activity;
    private static final String TAG = "CarStateDescFragment";

    public CarStateDescFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
        initBroadcastReceiver();
        registerSerialCommReceiver();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_car_state_desc, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    /**
     * 注册串口通信的广播
     */
    private void registerSerialCommReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constant.SERIAL_OBD_ACTION);  // OBD数据广播
        intentFilter.addAction(Constant.GPS_INFO); // 车辆位置广播
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        activity.registerReceiver(serialCommReceiver, intentFilter);
    }

    /**
     * 注销串口通信的广播
     */
    private void unRegisterSerialCommReceiver() {
        if (serialCommReceiver != null) {
            activity.unregisterReceiver(serialCommReceiver);
        }
    }

    private void initBroadcastReceiver() {
        // 初始化串口通信的接收者
        serialCommReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case Constant.SERIAL_OBD_ACTION: {
                        OBDSerialBean bean = (OBDSerialBean) intent.getExtras().getSerializable(Constant.SERIAL_OBD_DATA);
                        int[][] datass = bean.getDatass();   // OBD数据
                        int length = datass.length;
                        for (int i = 0; i < length; i++) {
                            if (datass[i][0] == 0X28) {
                                if (datass[i][8] == 0x87) {
                                    tvCalculatedValue.setText(String.valueOf(datass[i][11] * 100 / 255f));
                                    tvEngineCoolant.setText(String.valueOf(datass[i][12] - 40) + "℃");
                                    tvTransmitterSpeed.setText(String.valueOf(datass[i][13] * 256 + datass[i][14] / 4) + "R/MIN");
                                    tvCarSpeed.setText(String.valueOf(datass[i][15]) + "KM/H");
                                    activity.carSpeed = datass[i][15];
                                    tvIgnitionAdvanceFeet.setText(String.valueOf(datass[i][16] - 64) + "°");
                                    tvManifoldAbsolutePressure.setText(String.valueOf(datass[i][17]) + "kPa");
                                    tvVoltageControlModule.setText(String.valueOf(datass[i][18] / 10f) + "V");
                                    tvInletTemperature.setText(String.valueOf(datass[i][19] - 40) + "℃");
                                    tvAirFlow.setText(String.valueOf((datass[i][20] * 256 + (datass[i][21])) / 1000f) + "L/S");
                                    tvThrottlePosition.setText(String.valueOf(datass[i][22] * 100 / 255f));
                                    tvFuelCorrection.setText(String.valueOf((datass[i][23] - 128) * 100 / 128f) + "%");
                                    tvRatioCoefficient.setText(String.valueOf((datass[i][24] * 256 + datass[i][25]) * 0.0000305f) + "A/F");
                                    tvAbPosition.setText(String.valueOf(datass[i][26] * 100 / 255f));
                                    tvFuelPressure.setText(String.valueOf(datass[i][27] * 3) + "kPa");
                                    instantaneousFuelConsumption.setText(String.valueOf((datass[i][28] * 256 + datass[i][29]) / 10f) + "L/100Km");
                                    tvTotalMileage.setText(String.valueOf(((((long) datass[i][30] * 256 + (long) datass[i][31]) * 256 + (long) datass[i][32]) * 256 + (long) datass[i][33])) + "KM");
                                    activity.hasData = true;
                                }
                            }
                        }
                        break;
                    }
                    default: {
                        break;
                    }
                }

            }
        };
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unRegisterSerialCommReceiver();
    }
}

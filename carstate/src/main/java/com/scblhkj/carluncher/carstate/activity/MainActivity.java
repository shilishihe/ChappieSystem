package com.scblhkj.carluncher.carstate.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.scblhkj.carluncher.carstate.R;
import com.scblhkj.carluncher.carstate.adapter.CarStateFragmentPagerAdapter;
import com.scblhkj.carluncher.carstate.fragment.CarStateDescFragment;
import com.scblhkj.carluncher.carstate.fragment.CarStateFragment;
import com.scblhkj.carluncher.carstate.util.ObdSerialUtil;
import com.scblhkj.carluncher.carstate.util.TitleBuilder;
import com.scblhkj.carluncher.carstate.util.ToastUtil;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 主界面
 */
public class MainActivity extends BaseActivity implements View.OnClickListener {
    private static final int SCANNING_START = 0;
    private static final int SCANNING_END = 1;
    private static final int SCANNING_REPEAT = 3;
    private static final int CHECK_SCAN = 4;   // 检测是否可以体检
    private ImageView im_scan_line;
    private int idleCount = 0;
    private int scanEnable = -1;  // 能否体检标识
    private Timer timer;
    private TimerTask timerTask;
    public int carSpeed = 255;  // 实时车速
    private TextView bt_recheck;
    private static final String TAG = "MainActivity";
    /**
     * 扫描检查
     **/
    private ArrayList<Fragment> fragmentList;
    private ViewPager vp_main;
    private CarStateFragment carStateFragment;
    private CarStateDescFragment carStateDescFragment;
    private CarStateFragmentPagerAdapter vpAdapter;
    private ImageView im_dot1;
    private ImageView im_dot2;
    private ScaleAnimation scanAnim; // 扫描动画
    private boolean beginCheck = false; // 开始检测超时
    private int timeOutCount = 0;  // 超时计数
    private View view;
    private ImageView im_scan_car;
    public boolean hasData = false;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    /**
     * 启动定时器
     */
    private void startTimerTask() {
        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                if (hasData) {
                    SystemClock.sleep(3000);
                    editor.putBoolean("hasData",hasData);
                    editor.commit();
                }
                ObdSerialUtil.sendOBDSerialCMD(MainActivity.this, (byte) 0X07);  // 上传厂商自定义数据流
            }
        };
        timer.schedule(timerTask, 0, 2000);
    }

    /**
     * 取消定时器
     */
    private void destoryTimerTask() {
        if (timerTask != null) {
            timerTask.cancel();
            timerTask = null;
        }
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = View.inflate(this, R.layout.activity_main, null);
        setContentView(view);
        sp=getSharedPreferences("deskstate", Activity.MODE_PRIVATE);
        editor=sp.edit();
        initView();
        initData();
        checkScanEnable();
        //startTimerTask();
    }
    @Override
    protected void initView() {
     //   new TitleBuilder(view)
            //   .setTitleText("车辆状态")
             //   .setRightImage(R.drawable.ic_title_set_right_bt)
             //   .setLeftImage(R.drawable.ic_title_back_left_bt)
              //  .setLeftOnClickListener(new View.OnClickListener() {
                  //  @Override
                   // public void onClick(View v) {
                  //      finish();
                //    }
              //  })
              //  .setRightOnClickListener(new View.OnClickListener() {
                 //   @Override
                  //  public void onClick(View v) {
                      //  T//oastUtil.showToast("分享按钮");
                  //  }
              //  }).build();
        bt_recheck = (TextView) findViewById(R.id.bt_recheck);
        vp_main = (ViewPager) findViewById(R.id.vp_main);
        im_dot1 = (ImageView) findViewById(R.id.im_dot1);
        im_dot2 = (ImageView) findViewById(R.id.im_dot2);
        im_scan_line = (ImageView) findViewById(R.id.im_scan_line);
        im_scan_car = (ImageView) findViewById(R.id.im_scan_car);
    }

    @Override
    protected void initData() {
        String order="ATRON";
        byte[] bytes=order.getBytes();
        ObdSerialUtil.sendData2Port(this,bytes);
        Log.i("info","已经发送ATRON");
        bt_recheck.setOnClickListener(this);
        fragmentList = new ArrayList<Fragment>();
        carStateFragment = new CarStateFragment();
        carStateDescFragment = new CarStateDescFragment();
        fragmentList.add(carStateFragment);
        //fragmentList.add(carStateDescFragment);
        vpAdapter = new CarStateFragmentPagerAdapter(getSupportFragmentManager(), fragmentList);
        vp_main.setAdapter(vpAdapter);
        vp_main.setCurrentItem(0, true);
        vp_main.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0: {
                        im_dot1.setBackgroundResource(R.mipmap.ic_dot_focus);
                        im_dot2.setBackgroundResource(R.mipmap.ic_dot_normal);
                        break;
                    }
                    case 1: {
                        im_dot1.setBackgroundResource(R.mipmap.ic_dot_normal);
                        im_dot2.setBackgroundResource(R.mipmap.ic_dot_focus);
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });
    }

    /**
     * 检测是否可以体检
     */
    private void checkScanEnable() {
        mHandler.sendEmptyMessageDelayed(CHECK_SCAN, 1000);
        if (carSpeed == 0) {
            idleCount++;
            if (idleCount >= 5) {  // 持续5秒的车速为零
                scanEnable = 0;
            }
        } else {
            idleCount = 0;
            scanEnable = -1;
        }

        if (beginCheck) {
            timeOutCount++;
        }
        if (timeOutCount > 8) {  // 8秒超时
            stopScanning();
            ToastUtil.showToast("OBD通信异常,请检查设备是否连接正常");
        }
    }

    private Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case SCANNING_START: {
                    scanning();
                    break;
                }
                case SCANNING_REPEAT: {
                    break;
                }
                case SCANNING_END: {
                    break;
                }
                case CHECK_SCAN: {
                    checkScanEnable();
                    break;
                }
            }
            return true;
        }
    });

    /**
     * 扫描的效果
     */
    private void scanning() {
        im_scan_line.setVisibility(View.VISIBLE);
        scanAnim = new ScaleAnimation(1.0f, 1.0f, 0.0f, 18.0f);
        scanAnim.setRepeatCount(-1);
        scanAnim.setRepeatMode(Animation.RESTART);
        scanAnim.setInterpolator(new LinearInterpolator());
        scanAnim.setDuration(3000);
        im_scan_line.startAnimation(scanAnim);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_recheck: {
                if (scanEnable == 0) {
                    scanning();
                    if (carStateMainActivityCallBack != null) {
                        carStateMainActivityCallBack.startLoadingAnimation();
                    }
                    beginCheck = true;
                } else {
                    if (carSpeed != 0) {
                        ToastUtil.showToast("您的爱车还未进入怠速状态，请稍后重试");
                    } else {
                        ToastUtil.showToast("您还需怠速" + (5 - idleCount) + "秒,才能体检");
                    }
                }
                break;
            }
            default: {
                break;
            }
        }
    }

    /**
     * 停止扫描
     */
    public void stopScanning() {
        timeOutCount = 0;
        beginCheck = false;
        im_scan_line.setVisibility(View.GONE);
        im_scan_line.clearAnimation();
        if (carStateMainActivityCallBack != null) {
            carStateMainActivityCallBack.cleanLoadingAnimation();
            carStateMainActivityCallBack.setLastScanTime();
        }
        idleCount = 0;  // 计数清零
        scanEnable = -1; // 使能至位
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        destoryTimerTask();
    }

    public interface CarStateMainActivityCallBack {
        void startLoadingAnimation();
        void cleanLoadingAnimation();
        void setLastScanTime();
    }

    private CarStateMainActivityCallBack carStateMainActivityCallBack;

    public void setCarStateMainActivityCallBack(CarStateMainActivityCallBack carStateMainActivityCallBack) {
        this.carStateMainActivityCallBack = carStateMainActivityCallBack;
    }

    /**
     * 根据传递进来的布尔值切换体检车辆的照片
     *
     * @param state
     */
    public void changeCarImg(boolean state) {
        if (state) {
            im_scan_car.setBackgroundResource(R.mipmap.ic_car_state_worning);
        } else {
            im_scan_car.setBackgroundResource(R.mipmap.ic_car_state_normal_empty);
        }
    }

    public void change2CarDes() {
        vp_main.setCurrentItem(1);
    }
}

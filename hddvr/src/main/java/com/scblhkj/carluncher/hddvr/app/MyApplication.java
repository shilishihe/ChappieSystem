package com.scblhkj.carluncher.hddvr.app;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;

import com.duanqu.qupai.jni.ApplicationGlue;
import com.scblhkj.carluncher.hddvr.common.UnCeHandler;
import com.scblhkj.carluncher.hddvr.service.CarCollisionDetectionService;
import com.scblhkj.carluncher.hddvr.service.HddvrService;
import com.scblhkj.carluncher.hddvr.domain.HddvrSettingBean;
import com.scblhkj.carluncher.hddvr.utils.MeasureScreenUtil;
import com.scblhkj.carluncher.hddvr.utils.ToastUtil;

import java.util.ArrayList;

/**
 * Created by Leo on 2016/2/24.
 */
public class MyApplication extends Application {
    public String imei;
    ArrayList<Activity> list = new ArrayList<Activity>();
    @Override
    public void onCreate() {
        super.onCreate();
        ToastUtil.init(this);
        MeasureScreenUtil.init(this);
        MeasureScreenUtil.getScreenSize();
        startService(new Intent(this, HddvrService.class));   // 开启录制服务
        startService(new Intent(this, CarCollisionDetectionService.class));   // 开启录制服务
        System.loadLibrary("gnustl_shared");
        System.loadLibrary("qupai-media-thirdparty");
        System.loadLibrary("alivc-media-jni");
        ApplicationGlue.initialize(this);
        UnCeHandler catchExcep = new UnCeHandler(this);
        Thread.setDefaultUncaughtExceptionHandler(catchExcep);
    }

    /**
     * 关闭Activity列表中的所有Activity*/
    public void finishActivity(){
        for (Activity activity : list) {
            if (null != activity) {
                activity.finish();
            }
        }
        //杀死该应用进程
        android.os.Process.killProcess(android.os.Process.myPid());
    }
}

package com.scblhkj.carluncher.hddvr.net;

import java.util.List;

/**
 * Created by blh on 2016-08-26.
 */
public abstract class VolleyResponseCallback <T>{
    /**
     * 直接将服务器返回过来的Json转成对象放回
     *
     * @param t
     */
    public void get(T t) {
    }

    /**
     * 直接将服务器返回过来的Json转成对象数组放回
     *
     * @param t
     */
    public void getList(List<T> t) {
    }

    /**
     * 将Json数据直接放回
     *
     * @param string
     */
    public void getString(String string) {
    }
}

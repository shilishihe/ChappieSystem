package com.scblhkj.carluncher.hddvr.domain;

import java.io.Serializable;

/**
 * Created by Leo on 2016/4/7.
 * 行车记录仪通用设置实体类
 */
public class HddvrSettingBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private boolean bRecord;    // 录音
    private boolean bAdas; // ADAS开关
    private String sResolution; // 分辨率
    private String sVM; // 震动检测
    private int sTime; // 录制时长

    public int getsTime() {
        return sTime;
    }

    public void setsTime(int sTime) {
        this.sTime = sTime;
    }

    public HddvrSettingBean(boolean bRecord, boolean bAdas, String sResolution, String sVM, int sTime) {
        this.bAdas = bAdas;
        this.bRecord = bRecord;
        this.sResolution = sResolution;
        this.sVM = sVM;
        this.sTime = sTime;
    }

    @Override
    public String toString() {
        return "HddvrSettingBean{" +
                "bRecord=" + bRecord +
                ", bAdas=" + bAdas +
                ", sResolution='" + sResolution + '\'' +
                ", sVM='" + sVM + '\'' +
                ", sTime=" + sTime +
                '}';
    }

    public HddvrSettingBean() {
    }

    public boolean isbRecord() {
        return bRecord;
    }

    public void setbRecord(boolean bRecord) {
        this.bRecord = bRecord;
    }

    public boolean isbAdas() {
        return bAdas;
    }

    public void setbAdas(boolean bAdas) {
        this.bAdas = bAdas;
    }

    public String getsResolution() {
        return sResolution;
    }

    public void setsResolution(String sResolution) {
        this.sResolution = sResolution;
    }

    public String getsVM() {
        return sVM;
    }

    public void setsVM(String sVM) {
        this.sVM = sVM;
    }
}


package com.scblhkj.carluncher.hddvr;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.scblhkj.carluncher.hddvr.domain.HddvrSettingBean;
import com.scblhkj.carluncher.hddvr.service.HddvrService;
import com.scblhkj.carluncher.hddvr.utils.ACache;

/**
 * Created by Leo on 2016/2/24.
 * 监听开机广播
 */
public class BootBroadcastReceiver extends BroadcastReceiver {

    static final String ACTION = "android.intent.action.BOOT_COMPLETED";
    private HddvrSettingBean hsBean;
    private ACache aCache;

    @Override
    public void onReceive(Context context, Intent intent) {
        hsBean = (HddvrSettingBean) aCache.getAsObject("hddvr_set");
        if (hsBean == null) {
            hsBean = new HddvrSettingBean(true, true, "高清模式", "高", 180);
            aCache.put("hddvr_set", hsBean);
        }
        if (intent.getAction().equals(ACTION)) {
            if (!hsBean.getsVM().equals("关")) {
                startCarCollisionDetectionServoce(context);
            }
           startDriveRecordService(context);
        }
    }

    /**
     * 启动行车记录服务
     */
    private void startDriveRecordService(Context context) {
        Intent intent = new Intent(context, HddvrService.class);
        context.startService(intent);
    }

    /**
     * 开启碰撞检测服务
     */
    private void startCarCollisionDetectionServoce(Context context) {
        Intent intent = new Intent();
        intent.setAction("com.scblhkj.carluncher.hddvr.service.CarCollisionDetectionService");
        intent.setPackage("com.scblhkj.carluncher.hddvr");  //Android5.0后service不能采用隐式启动，故此处加上包名
        context.startService(intent);
    }

}

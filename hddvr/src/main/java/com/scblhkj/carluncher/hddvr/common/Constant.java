package com.scblhkj.carluncher.hddvr.common;


/**
 * Created by he on 2015/10/7.
 */
public interface Constant {

    // 倒车信号广播Action
    String CAR_BACK_CUSTOM_KEY_EVENT = "android.intent.action.CUSTOM_KEY_EVENT";
    String CARBACKA = "CARBACKA"; // 进入倒车
    String CARBACKB = "CARBACKB"; // 退出倒车
    String ACTION_ACC_INSERT = "android.intent.action.EXTERNAL_POWER_INSERT";  // ACC上电广播
    String ACTION_ACC_REMOVE = "android.intent.action.EXTERNAL_POWER_REMOVE";  // ACC下电广播
    String CAR_COLLISION_ACTION = "com.scblhkj.chappie.hddvr.car.collision";
    String TEMP_COLLISON_TIME = "TEMP_COLLISON_TIME";

    /**
     * 视频录制参数
     */
    int VIDEO_ENCODING_BIT_RATE_NORMAL = 1024 * 1024 * 12;//视频编码比特率
 //   int VIDEO_ENCODING_BIT_RATE_HEIGH = 1024 * 1024 * 15;//视频编码比特率
 int VIDEO_ENCODING_BIT_RATE_HEIGH = 1024 * 1024 * 1;//视频编码比特率
    int VIDEO_RECORD_WIDTH_HEIGH = 1920; // 默认的视频录制宽度
    int VIDEO_RECORD_HEIGHT_HEIGH = 1080; // 默认的视频录制高度
    int VIDEO_RECORD_WIDTH_NORMAL = 1280; // 默认的视频录制宽度
    int VIDEO_RECORD_HEIGHT_NORMAL = 720; // 默认的视频录制高度

    /**
     * 后视镜系统设置项的KEY值
     */
    //Switch开关设置
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_RECORD_VOICE = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_RECORD_VOICE"; // 录像声音开关
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_VIBRATION_ANALYSIS = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_VIBRATION_ANALYSIS";  // 震动监测开关
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_REMOTE_CONTROL_MAIN_CAMERA = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_REMOTE_CONTROL_MAIN_CAMERA"; // 远程控制前置摄像头开关
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_REMOTE_CONTROL_SECOND_CAMERA = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_REMOTE_CONTROL_SECOND_CAMERA";  // 远程控制副摄像头开关
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_LOCATION_SHARED = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_LOCATION_SHARED";  // 定位分享开关
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_ADAS = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_ADAS"; // ADAS辅助驾驶快关
    //RadioGroup开关设置
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_VIBRATION_TIME_DELAY_SETTING = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_VIBRATION_TIME_DELAY_SETTING";  // 震动延时设置
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_VIBRATION_MONITORING_LEVEL = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_VIBRATION_MONITORING_LEVEL";  // 振动监测级别
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_POWER_MODE_SETTINGS = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_POWER_MODE_SETTINGS";  // 功耗模式
    String ACTION_REMOTE_COLLISION_SERVICE = "action_remote_collision_service";  // 行车记录仪设置中设置震动级别
    String ACTION_REMOTE_COLLISION_SERVICE_VALUE = "action_remote_collision_service_value"; // 带过去的设置值
    int MAIN_CAMERA = 0; // 主摄像头
    int SECOND_CAMERA = 1; // 副摄像头
    int BACK_CAMERA = 2;  // 后摄像头

    String ACTION_COVER_STATE="action_cover_state";
    String SEND_CAR_COLLISION_VIDEO="com.scblhkj.chappie.hddvr.SEND_CAR_COLLISION_VID";
    String SEND_CAR_COLLISION_VIDEO_PATH = "com.scblhkj.chappie.hddvr.SEND_CAR_COLLISION_VIDEO_PATH";
    String SEND_MODIFY_QUAKE_SUCCESS="com.scblhkj.chappie.hddvr.SEND_MODIFY_QUAKE_SUCCESS";//修改震动值成功
    String SEND_MODIFY_QUAKE_FAIL="com.scblhkj.chappie.hddvr.SEND_MODIFY_QUAKE_FAIL";//修改震动值失败
    /**
     * 控制录像画面大小
     */
    String CAR_ACTION_HALF_SIZE = "com.scblhkj.chappie.hddvr.CAR_ACTION_HALF_SIZE";  // 屏幕窗口一半
    String CAR_ACTION_FULL_SIZE = "com.scblhkj.chappie.haddvr.CAR_ACTION_FULL_SIZE"; // 全屏
    String CAR_ACTION_MINI_SIZE = "com.scblhkj.chappie.haddvr.CAR_ACTION_MINI_SIZE"; // 最小窗口
    String SEND_CAR_COLLISION_PIC = "com.scblhkj.chappie.hddvr.SEND_CAR_COLLISION_PIC";  //发送震动事件图片广播
    String SEND_CAR_COLLISION_PIC_PATH = "com.scblhkj.chappie.hddvr.SEND_CAR_COLLISION_PIC_PATH"; // 要发送的图片的路径
    String SEND_CAR_CMD_WIFI_PIC = "com.scblhkj.chappie.hddvr.SEND_CAR_CMD_WIFI_PIC";  //发送接收指令后主动拍摄图片广播
    String SEND_CAR_CMD_WIFI_PIC_PATH = "com.scblhkj.chappie.hddvr.SEND_CAR_CMD_WIFI_PIC_PATH"; // 要发送的图片的路径
    String SEND_CAR_CMD_NET_PIC = "com.scblhkj.chappie.hddvr.SEND_CAR_CMD_NET_PIC";  //发送接收指令后主动拍摄图片广播
    String SEND_CAR_CMD_NET_PIC_PATH = "com.scblhkj.chappie.hddvr.SEND_CAR_CMD_NET_PIC_PATH"; // 要发送的图片的路径
    String FINISH_NET_VIDEO_FILE="com.scblhkj.chappie.FINISH_NET_VIDEO_FILE";
    String SEND_LIMITED_SOEED_HAPPEN="com.scblhkj.chappie.SEND_LIMITED_SOEED_HAPPEN";//发生急减速事件
    String SEND_CAR_LIMITED_PIC = "com.scblhkj.chappie.hddvr.SEND_CAR_LIMITED_PIC";  //发送急减速事件图片广播
    String SEND_CAR_LIMITED_PIC_PATH = "com.scblhkj.chappie.hddvr.SEND_CAR_LIMITED_PIC_PATH"; // 要发送的图片的路径
    String SEND_CAR_LIMITED_VID = "com.scblhkj.chappie.hddvr.SEND_CAR_LIMITED_VID";  //发送急减速事件视频广播
    String SEND_CAR_LIMITED_VID_PATH = "com.scblhkj.chappie.hddvr.SEND_CAR_LIMITED_VID_PATH"; // 要发送的视频的路径
   String SEND_VIDEO_THREAD="com.scblhkj.chappie.SEND_VIDEO_THREAD";//配置视频流服务器
   String SEND_VIDEO_THREAD_SUC="com.scblhkj.chappie.SEND_VIDEO_THREAD_SUC";//配置视频流服务器成功
   String SEND_IS_VIDEO_THREAD_STATE="com.scblhkj.chappie.SEND_IS_VIDEO_THREAD_STATE";//是否是实时视频状态
    /**
     * 打开或者是关闭ADAS
     */
    String ACTION_OPEN_ADAS = "com.scblhkj.hddvr.ACTION_OPEN_ADAS";  // 开启ADAS
    String ACTION_CLOSE_ADAS = "com.scblhkj.hddvr.ACTION_CLOSE_ADAS"; // 关闭ADAS

}

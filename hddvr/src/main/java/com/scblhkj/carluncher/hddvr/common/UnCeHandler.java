package com.scblhkj.carluncher.hddvr.common;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.scblhkj.carluncher.hddvr.MainActivity;
import com.scblhkj.carluncher.hddvr.app.MyApplication;

/**
 * Created by blh on 2016-11-28.
 */
public class UnCeHandler implements Thread.UncaughtExceptionHandler {
    private Thread.UncaughtExceptionHandler mDefaultHandler;
    public  static final String TAG="CatchExcep";
   MyApplication application;

    /***
     * 获取系统默认的unCatchException
     * @param application
     */
    public UnCeHandler(MyApplication application){
        mDefaultHandler=Thread.getDefaultUncaughtExceptionHandler();
        this.application=application;
    }
    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
    if(!handleException(ex)&&mDefaultHandler!=null){
        //如果用户没有处理则让系统来处理
        mDefaultHandler.uncaughtException(thread,ex);
    }else {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Log.e(TAG,"error："+e);
        }
        Intent intent=new Intent(application.getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent restartIntent = PendingIntent.getActivity(
                application.getApplicationContext(), 0, intent,
               0);
        //退出程序
        AlarmManager am= (AlarmManager) application.getSystemService(Context.ALARM_SERVICE);
        am.set(AlarmManager.RTC,System.currentTimeMillis()+1000,restartIntent); //一秒后重启应用
        application.finishActivity();

    }
    }
    /**
     * 自定义错误处理,收集错误信息 发送错误报告等操作均在此完成.
     *
     * @param ex
     * @return true:如果处理了该异常信息;否则返回false.
     */
    private boolean handleException(Throwable ex){
        if(ex==null){
            return false;
        }
        new Thread(){
            @Override
            public void run() {
                Looper.prepare();
                Toast.makeText(application.getApplicationContext(),"很抱歉,程序出现异常,即将退出!",Toast.LENGTH_LONG).show();
                Looper.loop();
            }
        }.start();
        return true;
    }
}

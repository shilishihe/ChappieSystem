package com.scblhkj.carluncher.hddvr.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.scblhkj.carluncher.hddvr.common.Constant;
import com.scblhkj.carluncher.hddvr.domain.HddvrSettingBean;
import com.scblhkj.carluncher.hddvr.utils.ACache;
import com.scblhkj.carluncher.hddvr.utils.ToastUtil;


/**
 * 检测汽车碰撞的后台服务
 */
public class CarCollisionDetectionService extends Service {

    private static final int START_CRASH = 100;
    // 延时屏蔽三秒之内的重复碰撞检测
    private static final int START_CRASH_DELAY = 3000;
    // 上次监测的时间
    private long lastUpdateTime = 0;
    // 传感器类对象
    private SensorManager sm;
    // 加速度传感器
    private Sensor acceleromererSensor;
    private float lastX = 0;
    private float lastY = 0;
    private float lastZ = 0;
    // 是否正在监测碰撞
    private boolean isCrash = false;
    // 震动检测到的次数
    private int crashCount = 0;
    // 震动级别
    private int collisionLevel;
    // 服务已关闭
    private boolean shutdownService;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case START_CRASH:
                    isCrash = false;
                    break;
                case 0x02:
                    ToastUtil.showToast("检测到汽车异常震动");
                    break;
                default:
                    break;
            }
            super.handleMessage(msg);
        }
    };

    private BroadcastReceiver receiver;
    private HddvrSettingBean hsBean;
    private long timeInterval;
    private ACache aCache;
    private boolean coverState;

    private void initBroadCastReceiver() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch (action) {
                    case Constant.ACTION_REMOTE_COLLISION_SERVICE: {
                        String level = intent.getStringExtra(Constant.ACTION_REMOTE_COLLISION_SERVICE_VALUE);
                        collisionLevel = Integer.parseInt(level);
                        Log.e("info","collisionLevel"+collisionLevel);
                        break;
                    }
                    case Constant.ACTION_COVER_STATE:
                        Bundle bundle=intent.getExtras();
                        coverState= bundle.getBoolean("isCarColl");
                        Log.e("info","isCarColl>>"+coverState);
                    break;
                    default: {
                        break;
                    }
                }
            }
        };
    }

    /**
     * 注册广播接收者
     */
    private void registerBroadCastReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        intentFilter.addAction(Constant.ACTION_REMOTE_COLLISION_SERVICE);
        intentFilter.addAction(Constant.CAR_BACK_CUSTOM_KEY_EVENT); // 倒车广播
        intentFilter.addAction(Constant.ACTION_COVER_STATE);
        registerReceiver(receiver, intentFilter);
    }



    /**
     * 注销广播接收者
     */
    private void unRegisterBroadCastReceiver() {
        if (receiver != null) {
            unregisterReceiver(receiver);
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
        aCache = ACache.get(this);
        hsBean = (HddvrSettingBean) aCache.getAsObject("hddvr_set");
        shutdownService = false;
        Log.e("info", "震动检测服务启动");
        // 获取传感器管理器类
        sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        // 获取加速度传感器类
        acceleromererSensor = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        // 注册监听器
        sm.registerListener(sensorEventListener, acceleromererSensor, SensorManager.SENSOR_DELAY_UI);
        initBroadCastReceiver();
        registerBroadCastReceiver();
        initcollisionLevel();
    }

    /**
     * 初始化碰撞监测的值
     */
    private void initcollisionLevel() {
        hsBean = (HddvrSettingBean) aCache.getAsObject("hddvr_set");
        Log.e("info","hsBean.getsVM()"+hsBean.getsVM());
        switch (hsBean.getsVM()) {
            case "高": {
                collisionLevel = 12;
                break;
            }
            case "中": {
                collisionLevel = 10;
                break;
            }
            case "低": {
                collisionLevel = 8;
                break;
            }
            default: {
                break;
            }
        }
        Log.e("info", "震动界别是:" + collisionLevel);
    }

    public CarCollisionDetectionService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        unRegisterBroadCastReceiver();
        shutdownService = true;
        Log.e("info", "关闭了震动监测服务");
        super.onDestroy();
    }


    private long currentUpdateTime;
    private double speed;
    /**
     * 加速度传感器监听类
     */
    private SensorEventListener sensorEventListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {
            if (!shutdownService) {
                float x = event.values[SensorManager.DATA_X];
                float y = event.values[SensorManager.DATA_Y];
                float z = event.values[SensorManager.DATA_Z];
                // 现在检测时间
                currentUpdateTime = System.currentTimeMillis();
                // 两次检测的时间间隔
                 timeInterval = currentUpdateTime - lastUpdateTime;
                // 现在的时间变成last时间
                lastUpdateTime = currentUpdateTime;
                // 获得x,y,z的变化值
                final float deltaX = x - lastX;
                final float deltaY = y - lastY;
                final float deltaZ = z - lastZ;
                // 将现在的坐标变成last坐标
                lastX = x;
                lastY = y;
                lastZ = z;
                // 速度值
                speed = Math.sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ);
                  // Log.e("info", "speed = " + speed);
                if (speed > collisionLevel) {  // 震动监测的告警级别
                    if (!isCrash) {
                        isCrash = true;
                        sendCarCollisionBroacast();
                    }
                    Message msg = Message.obtain();
                    msg.what = START_CRASH;
                    handler.sendMessageDelayed(msg, START_CRASH_DELAY);
                }
                //    Log.e(TAG, "碰撞检测传感器方法回调");
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };


    private static final String TAG = "震动监测服务";

    /**
     * 发送汽车发送碰撞的时间戳
     */
    private void sendCarCollisionBroacast() {
        //if (crashCount == 0) {
          //  Log.e("info", "屏蔽第一次震动临界值");
      //  } else {

        if(crashCount!=0&&coverState==true){
            Log.e("info","屏蔽10S多次震动多次录制视频");
            return;
        }
            Intent intent = new Intent();
            intent.setAction(Constant.CAR_COLLISION_ACTION);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            Bundle bundle = new Bundle();
            bundle.putLong(Constant.TEMP_COLLISON_TIME, System.currentTimeMillis());
            bundle.putDouble("speedQuakeValue",speed);
            intent.putExtras(bundle);
            sendBroadcast(intent);
           handler.sendEmptyMessage(0x02);
       // }
        crashCount++;
    }

}

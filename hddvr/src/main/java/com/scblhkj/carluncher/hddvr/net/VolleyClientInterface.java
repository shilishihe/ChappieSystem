package com.scblhkj.carluncher.hddvr.net;

import java.util.Map;

/**
 * Created by blh on 2016-08-26.
 */
public interface VolleyClientInterface {
    void sendCarCollsionToService(Map<String, String> params, VolleyResponseCallback object);
}

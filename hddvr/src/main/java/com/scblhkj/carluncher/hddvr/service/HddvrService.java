package com.scblhkj.carluncher.hddvr.service;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.media.SoundPool;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.alibaba.livecloud.live.AlivcMediaFormat;
import com.alibaba.livecloud.live.AlivcMediaRecorder;
import com.alibaba.livecloud.live.AlivcMediaRecorderFactory;
import com.alibaba.livecloud.live.AlivcRecordReporter;
import com.alibaba.livecloud.live.AlivcStatusCode;
import com.alibaba.livecloud.live.OnLiveRecordErrorListener;
import com.alibaba.livecloud.live.OnNetworkStatusListener;
import com.alibaba.livecloud.live.OnRecordStatusListener;
import com.duanqu.qupai.logger.DataStatistics;
import com.gm.ctrl_misc.CtrlMisc;
import com.scblhkj.carluncher.hddvr.HddvrSettingActivity;
import com.scblhkj.carluncher.hddvr.R;
import com.scblhkj.carluncher.hddvr.app.MyApplication;
import com.scblhkj.carluncher.hddvr.common.Constant;
import com.scblhkj.carluncher.hddvr.domain.HddvrSettingBean;
import com.scblhkj.carluncher.hddvr.net.VolleyClient;
import com.scblhkj.carluncher.hddvr.net.VolleyResponseCallback;
import com.scblhkj.carluncher.hddvr.utils.ACache;
import com.scblhkj.carluncher.hddvr.utils.CommonUtil;
import com.scblhkj.carluncher.hddvr.utils.FileUtil;
import com.scblhkj.carluncher.hddvr.utils.MeasureScreenUtil;
import com.scblhkj.carluncher.hddvr.utils.SDCardScannerUtil;
import com.scblhkj.carluncher.hddvr.utils.StartAppUtil;
import com.scblhkj.carluncher.hddvr.utils.ToastUtil;
import com.sinosmart.adas.ADASInterface;
import com.spore.jni.ImageUtilEngine;

import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 行车记录仪服务
 * 功能完成之后需要考虑配置上守护服务
 */
public class HddvrService extends Service implements View.OnClickListener, TextureView.SurfaceTextureListener {
    private String pushUrl;
    private WindowManager.LayoutParams layoutParams;
    private WindowManager windowManager;
    private View hddvrView;
    private ImageView im_switchCamera;  // 切换摄像头
    private ImageView im_tp; // 拍照
    private ImageView im_moreFunction; // 更多
    private RelativeLayout rl_choiceCamera; // 点击更多弹出的选择摄像头的布局
    private LinearLayout ll_mainCamera;   // 主摄像头
    private LinearLayout ll_secondCamera;  // 副摄像头
    private LinearLayout ll_backCamera;  // 倒车摄像头
    private Camera mCamera;
    private TextureView textureMain;// TextureView
    private boolean isPreviewing = false; // 是否在预览模式
    public int currentCamera; // 当前摄像头
    private ImageUtilEngine imageUtilEngine;
    private ImageView iv;
    private int mPreWidth = 1400;
    private int mPreHeight = 480;
    private int mPicWidth = 640;
    private int mPicHeight = 480;
    private static final String[] permissionManifest = {
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO
    };
    private static final int PERMISSION_REQUEST_CODE = 1;
    private MediaRecorder mMediaRecorder;
    private AlivcMediaRecorder mAlivcMediaRecorder;
    private AlivcRecordReporter mRecordReporter;
    private File currentViewFile;
    private static long tempStartRecordTime = 0;  // 每段视频开始录制的时间戳
    private static long tempEndRecordTime = 0;    // 每段视频结束录制的时间戳
    private Timer mTimer;// 计时器
    private int mTimeCount;// 时间计数
    private int mRecordMaxTime = 60;// 一次拍摄最长时间
    private final int CHECK_TF_SAPCE = 0XFF;
    private final int NO_TF_CARD = 0XF0;
    private final int FILE_DELETE_OVER = 0XF1;
    private final int FILE_DELETE_FAIL = 0XF2;
    private final int TimeUp=0xF3;
    private final int CloseChronometer=0xF4;
    private final int OpenChronometer=0xf5;
    private static long tempCollisionTime;  // 检测到震动的时间戳
    private boolean isNet;
    int videoResolution = 0; //视频传输的编码
    /*************************
     * ADAS相关
     ********************************/
    private Bitmap mBitmap;
    private Paint paint;
    private Mat rgbMat;
    private ADASInterface AI;
    private SurfaceTexture surfaceTexture;
    private BroadcastReceiver broadcastReceiver;
    private boolean isCollision = false;  // 汽车是否异常震动
    private boolean savePic = false;
    private FileOutputStream outputStream = null;
    private boolean isWifiPic = false;
    private HddvrSettingBean hsBean;
    private boolean isADASRunning = false;
    private ACache aCache;
    private boolean isCarStop; // 汽车已经停止，接收不到obd数据
    private SoundPool soundPool;
    private int cshotID;
    private int timerCount = 0;
    private int mPreviewWidth = 0;
    private int mPreviewHeight = 0;
    private Map<String, Object> mConfigure = new HashMap<>();
    // private static final String "info" = "HddvrService";
    private Timer stopRecordTimer,stopRecordTimer1; // 停止录像的定时器
    private TimerTask stopRecorderTask,stopRecorderTask1; // 停止录像的定时器任务
    private static OnCShotAccomplish onCShotAccomplish;
    private static OnHddvrServiceCreate onHddvrServiceCreate;
    private boolean isUserC,isUserP; // 用户控制的录像,拍照
    private boolean isRecording;
    private boolean isChronometer=true;
    private ImageView im_videoset;
    private ImageView iv_recorderpose;
    private Chronometer chronometer;
    adasCloseReceiver registerAdasCloseReceiver;
    adasOpenReceiver registerAdasOpenReceiver;
    private DataStatistics mDataStatistics = new DataStatistics(1000);
    protected VolleyClient volleyClient;
    private String imei;
    private boolean isCoverState=false;//当前镜头界面
    private boolean isCarColl=false;//是否发生碰撞
    private boolean isNetC;
    private SurfaceView _CameraSurface;
    private int cameraFrontFacing;
    private boolean isSurMain=false;
    private String realTimeImei,realTimeStamp,realTimeTarget,cramer,quakeValue,ModifyQuake,modifyQuakeCrame,isSendQuakePic,modifyQuakeVCrame,isSendQuakeVid;
    private boolean isFrontCramer,isBackCramer;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private File picFile;
    private MyApplication application;
    private String collsionTimeStamp;//碰撞时的时间戳
    private String isSendFile;
    private int videoTime,limteValue;
    private boolean isSettingQuakeInfo,isSettingSpeedInfo;
    private double speedQuakeValue;
    private String speedTimeStamp,deviceImei,speedCramer,isSendSpeedPic,speedVCramer,isSendSpeedVid;
    private boolean isLimtedSpeed;//是否是急减速
    private boolean isRealTimeRecord;//是否在实时查看
    private int realTimeCramer;//实时视频查看的摄像头状态，0 :主摄像头 1:副摄像头 2:没有实时视频查看
    private boolean isInterruptRealTime;//实时视频是否被打断

    //VideoContReceiver videoContReceiver;
    public interface OnHddvrServiceCreate {
        void onServiceCreate();
    }

    public interface OnCShotAccomplish {
        void onShotFinished(Bitmap bitmaps);
    }

    public static void setOnCShotAccomplish(OnCShotAccomplish ocsa) {
        onCShotAccomplish = ocsa;
    }

    public static void setOnHddvrServiceCreate(OnHddvrServiceCreate callback) {
        onHddvrServiceCreate = callback;
    }

    /**
     * 初始化行车记录控制广播接收者
     */
    private void initControlRecordReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch (action) {
                    case Constant.SEND_VIDEO_THREAD:{
                        //将接受到视频流地址保存在本地
                        Log.e("info", "接收到的视频流地址是:" + intent.getStringExtra("ffmpeg_link"));
                        editor.putString("ffmpeg_link", intent.getStringExtra("ffmpeg_link"));
                        editor.commit();
                        Intent intent1=new Intent();
                        intent1.setAction(Constant.SEND_VIDEO_THREAD_SUC);
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        intent.putExtra("timeStamp", intent.getStringExtra("timeStamp"));
                        intent.putExtra("imei",intent.getStringExtra("imei"));
                        sendBroadcast(intent);

                        break;
                    }
                    case "com.android.chappie.MODIFY_QUAKE":{ //修改震动告警参数广播
                        // sendModifyQuakeBroadcast(ModifyQuake,modifyQuakeCrame,isSendQuakePic,modifyQuakeVCrame,isSendQuakeVid);
                        quakeValue=intent.getStringExtra("ModifyQuake");
                        int value=Integer.parseInt(quakeValue);
                        realTimeImei=intent.getStringExtra("modifyQuakeImei");
                        realTimeStamp=intent.getStringExtra("modifyQuakeTime");
                        modifyQuakeCrame=intent.getStringExtra("modifyQuakeCrame");//震动拍照摄像头
                        isSendQuakePic=intent.getStringExtra("isSendQuakePic");//是否发送震动图片文件
                        modifyQuakeVCrame=intent.getStringExtra("modifyQuakeVCrame");//震动摄像摄像头
                        isSendQuakeVid=intent.getStringExtra("isSendQuakeVid");//是否发送震动视频文件
                        Log.e("info","hddrv接收到的震动修改信息:"+"ModifyQuake:"+ ModifyQuake+"  modifyQuakeCrame:"+modifyQuakeCrame+"  isSendQuakePic:"+isSendQuakePic+"  modifyQuakeVCrame:"+modifyQuakeVCrame+"  isSendQuakeVid:"+isSendQuakeVid);
                        isSettingQuakeInfo=true;//将配置震动信息参数设置为真
                        editor.putString("modifyQuakeCrame", modifyQuakeCrame);
                        editor.putString("isSendQuakePic",isSendQuakePic);
                        editor.putString("modifyQuakeVCrame",modifyQuakeVCrame);
                        editor.putString("isSendQuakeVid",isSendQuakeVid);
                        editor.putBoolean("isSettingQuakeInfo",isSettingQuakeInfo);
                        editor.commit();
                        if(value>0&&value<10){
                            hsBean.setsVM("低");
                            aCache.put("hddvr_set", hsBean);  // 关闭页面的时候保存数据
                            Log.e("info", "设置震动级别为低");
                            Log.e("info", "hsBean = " + hsBean.toString() + "数据保存成功");
                            mHandler.sendEmptyMessage(103);
                        }else if(value>=10&&value<20){
                            hsBean.setsVM("中");
                            aCache.put("hddvr_set", hsBean);  // 关闭页面的时候保存数据
                            Log.e("info", "hsBean = " + hsBean.toString() + "数据保存成功");
                            Log.e("info", "设置震动级别为中");
                            mHandler.sendEmptyMessage(103);
                        }else if(value>=20&&value<=100){
                            hsBean.setsVM("高");
                            aCache.put("hddvr_set", hsBean);  // 关闭页面的时候保存数据
                            Log.e("info", "hsBean = " + hsBean.toString() + "数据保存成功");
                            Log.e("info", "设置震动级别为高");
                            mHandler.sendEmptyMessage(103);
                        }else {
                            mHandler.sendEmptyMessage(103);
                        }
                        break;
                    }
                    case "com.android.chappie.ACTION_WIFI_C_PIC": {  // wifi主动要求拍照
                        isWifiPic = true;
                        savePic = true;
                        Log.e("info", "收到拍照广播");
                        break;
                    }
                    case "com.android.chappie.ACTION_NET_C_PIC": {  // 网络主动要求拍照
                        realTimeImei=intent.getStringExtra("imei");
                        realTimeStamp=intent.getStringExtra("ctime");
                        cramer=intent.getStringExtra("cramer");
                        isSendFile=intent.getStringExtra("isSendFile");//是否发送文件
                        Log.e("info", "hddrv收到网络状态下的拍照广播"+"cramer>>"+cramer+"isSendFile"+isSendFile);
                        getRealTimeRecorderInfo();//执行指令前先判断是否是在实时视频查看
                        if(cramer.equals("1")){
                            //如果客户端请求是前置摄像头拍照，而当前是主摄像头的情况时
                            if(currentCamera==0){
                                closeADAS();
                                stopRecordVideo();
                                chronometer.stop();
                                if(isChronometer==false){
                                    chronometer.setVisibility(View.VISIBLE);
                                    isChronometer=true;
                                }
                                isPreviewing = false;
                                chronometer.setBase(SystemClock.elapsedRealtime());
                                chronometer.start();
                                recordVideo(Constant.SECOND_CAMERA);
                                CtrlMisc.CtrlMisc_radar_powerctrl_jni(1);
                                isFrontCramer=true;
                            }else if(currentCamera==1){
                                isBackCramer=true;
                            }
                            isNet = true;
                            savePic = true;
                            isUserP=true;
                        }else if(cramer.equals("0")){
                            //如果客户端请求是主摄像头拍照，而当前后视镜是前置摄像头的情况时
                            if(currentCamera==1){
                                stopRecordVideo();
                                chronometer.stop();
                                if(isChronometer==false){
                                    chronometer.setVisibility(View.VISIBLE);
                                    isChronometer=true;
                                }
                                //  AudioSwitch.gm_radar_powerctrl(0);//关闭LEO夜视灯
                                openADAS();
                                isPreviewing = false;
                                chronometer.setBase(SystemClock.elapsedRealtime());
                                chronometer.start();
                                recordVideo(Constant.MAIN_CAMERA);
                                CtrlMisc.CtrlMisc_radar_powerctrl_jni(0);
                                isBackCramer=true;
                            }else if(currentCamera==0){
                                isFrontCramer=true;
                            }
                            isNet = true;
                            savePic = true;
                            isUserP=true;
                        }
                        break;
                    }
                    case "com.android.chappie.ACTION_NET_C_VID":{ // 客户端网络状态下远程控制拍摄视频（1min）
                        realTimeImei=intent.getStringExtra("imei");
                        realTimeStamp=intent.getStringExtra("ctime");
                        cramer=intent.getStringExtra("cramer");
                        isSendFile=intent.getStringExtra("isSendVideo");
                        videoTime=Integer.parseInt(intent.getStringExtra("videoTime"));
                        Log.e("info", "收到远程控制摄像广播"+"videoTime:"+videoTime);
                        getRealTimeRecorderInfo();
                        //客户端请求和后视镜当前的镜头都是主摄像头时
                        if(cramer.equals("0")&&currentCamera==Constant.MAIN_CAMERA){
                            mHandler.sendEmptyMessage(0x09);
                            Log.e("info", "客户端请求和后视镜当前的镜头都是主摄像头时");
                            stopRecordTimer1=new Timer();
                            stopRecorderTask1=new TimerTask() {
                                @Override
                                public void run() {
                                    stopRecordVideo();  // 首先停止录像
                                    mHandler.sendEmptyMessage(0x08);
                                    isUserC=true;
                                    recordVideo(Constant.MAIN_CAMERA);
                                    CtrlMisc.CtrlMisc_radar_powerctrl_jni(0);

                                }
                            };
                            if(videoTime!=0){
                                stopRecordTimer1.schedule(stopRecorderTask1, 1000 * videoTime);}
                            else if(videoTime==0) {
                                stopRecordTimer1.schedule(stopRecorderTask1, 1000 * 60);//如果录制时间为0，则默认录制一分钟
                            }

                        }else if(cramer.equals("0")&&currentCamera==Constant.SECOND_CAMERA){
                            Log.e("info", "客户端请求的是主摄像头，而当前后视镜为前置摄像头时");
                            mHandler.sendEmptyMessage(100);
                            stopRecordTimer1=new Timer();
                            stopRecorderTask1=new TimerTask() {
                                @Override
                                public void run() {
                                    stopRecordVideo();  // 首先停止录像 // 停止录像保存视频
                                    mHandler.sendEmptyMessage(0x08);
                                    recordVideo(Constant.MAIN_CAMERA);
                                    CtrlMisc.CtrlMisc_radar_powerctrl_jni(0);
                                }
                            };
                            if(videoTime!=0){
                                stopRecordTimer1.schedule(stopRecorderTask1, 1000 * videoTime);}
                            else if(videoTime==0) {
                                stopRecordTimer1.schedule(stopRecorderTask1, 1000 * 60);//如果录制时间为0，则默认录制一分钟
                            }
                        }else if(cramer.equals("1")&&currentCamera==Constant.SECOND_CAMERA){//当客户端的请求是前置摄像头并且当前镜头也是前置摄像头时
                            Log.e("info", "客户端请求和后视镜当前的镜头都是前置摄像头时");
                            mHandler.sendEmptyMessage(101);
                            stopRecordTimer1=new Timer();
                            stopRecorderTask1=new TimerTask() {
                                @Override
                                public void run() {
                                    stopRecordVideo();  // 首先停止录像 // 停止录像保存视频
                                    mHandler.sendEmptyMessage(0x08);
                                    recordVideo(Constant.SECOND_CAMERA);
                                    CtrlMisc.CtrlMisc_radar_powerctrl_jni(1);
                                }
                            };
                            if(videoTime!=0){
                                stopRecordTimer1.schedule(stopRecorderTask1, 1000 * videoTime);}
                            else if(videoTime==0) {
                                stopRecordTimer1.schedule(stopRecorderTask1, 1000 * 60);//如果录制时间为0，则默认录制一分钟
                            }
                        }else if(cramer.equals("1")&&currentCamera==Constant.MAIN_CAMERA){
                            Log.e("info","客户端请求的是前置摄像头，而当前后视镜为主摄像头时");
                            mHandler.sendEmptyMessage(102);
                            stopRecordTimer1=new Timer();
                            stopRecorderTask1=new TimerTask() {
                                @Override
                                public void run() {
                                    stopRecordVideo();  // 首先停止录像 // 停止录像保存视频
                                    mHandler.sendEmptyMessage(0x08);
                                    recordVideo(Constant.SECOND_CAMERA);
                                    CtrlMisc.CtrlMisc_radar_powerctrl_jni(1);
                                }
                            };
                            if(videoTime!=0){
                                stopRecordTimer1.schedule(stopRecorderTask1, 1000 * videoTime);}
                            else if(videoTime==0) {
                                stopRecordTimer1.schedule(stopRecorderTask1, 1000 * 60);//如果录制时间为0，则默认录制一分钟
                            }
                        }
                    }
                    break;
                    case Constant.CAR_COLLISION_ACTION: {
                        getRealTimeRecorderInfo();
                        Bundle bundle = intent.getExtras();
                        tempCollisionTime = bundle.getLong(Constant.TEMP_COLLISON_TIME);
                        speedQuakeValue=bundle.getDouble("speedQuakeValue");
                        isCollision = true;
                        chronometer.stop();
                        collsionTimeStamp=getTimeStamp();
                        //发送碰撞事件时对是否设置震动参数信息进行判断
                        isSettingQuakeInfo=sp.getBoolean("isSettingQuakeInfo",isSettingQuakeInfo);
                        //没有配置震动信息参数的情况下，就默认执行
                        if(isSettingQuakeInfo==false){
                            Log.e("info", "没有配置震动信息参数的情况下，就默认执行");
                            if(currentCamera==0){  //如果当前镜头是主摄像头
                                savePic = true;
                                isCoverState=true;
                                stopRecordVideo();
                                mHandler.sendEmptyMessage(1110);
                            }else{
                                savePic=true;
                                isCoverState=true;
                                stopRecordVideo();
                                mHandler.sendEmptyMessage(1111);
                            }
                        }else if(isSettingQuakeInfo==true) {
                            Log.e("info","配置震动信息参数的情况下");
                            modifyQuakeCrame=sp.getString("modifyQuakeCrame","");
                            modifyQuakeVCrame=sp.getString("modifyQuakeVCrame","");
                            Log.e("info","配置了震动参数的情况下震动拍摄进程,modifyQuakeCrame:"+modifyQuakeCrame+" modifyQuakeVCrame:"+modifyQuakeVCrame);
                            if(modifyQuakeCrame.equals("0")&&modifyQuakeVCrame.equals("0")){
                                //第一种情况:主摄像头拍照，主摄像头摄像
                                Log.e("info","主摄像头拍照,主摄像头摄像");
                                mainReview();
                                savePic=true;
                                isCoverState=true;
                                stopRecordVideo();
                                mHandler.sendEmptyMessage(1110);
                            }else if(modifyQuakeCrame.equals("0")&&modifyQuakeVCrame.equals("1")){
                                //第2种情况：主摄像头拍照,副摄像头摄像
                                Log.e("info", "主摄像头拍照,副摄像头摄像,开始碰撞录像...");
                                mainReview();
                                savePic=true;
                                isCoverState=true;
                                stopRecordVideo();
                                mHandler.sendEmptyMessage(1111);
                            }else if(modifyQuakeCrame.equals("1")&&modifyQuakeVCrame.equals("0")){
                                //第3种情况：副摄像头拍照，主摄像头摄像
                                Log.e("info", "副摄像头拍照，主摄像头摄像...");
                                secondReview();
                                savePic=true;
                                isCoverState=true;
                                stopRecordVideo();
                                mHandler.sendEmptyMessage(1110);
                            }else if(modifyQuakeCrame.equals("1") &&modifyQuakeVCrame.equals("1")){
                                //第4种情况：副摄像头拍照，副摄像头摄像
                                Log.e("info", "副摄像头拍照，副摄像头摄像...");
                                secondReview();
                                savePic=true;
                                isCoverState=true;
                                stopRecordVideo();
                                mHandler.sendEmptyMessage(1111);
                            }
                        }
                        break;
                    }
                    case Constant.SEND_LIMITED_SOEED_HAPPEN:{ //急减速事件发生
                        getRealTimeRecorderInfo();
                        speedTimeStamp=intent.getStringExtra("speedTimeStamp");
                        deviceImei=intent.getStringExtra("deviceImei");
                        speedCramer=intent.getStringExtra("speedCramer");
                        isSendSpeedPic=intent.getStringExtra("isSendSpeedPic");
                        speedVCramer=intent.getStringExtra("speedVCramer");
                        isSendSpeedVid=intent.getStringExtra("isSendSpeedVid");
                        isSettingSpeedInfo=intent.getBooleanExtra("isSettingSpeedInfo", isSettingQuakeInfo);
                        limteValue=intent.getIntExtra("limteValue", limteValue);
                        editor.putString("speedTimeStamp",speedTimeStamp);
                        editor.putString("deviceImei",deviceImei);
                        editor.putString("speedCramer",speedCramer);
                        editor.putString("isSendSpeedPic",isSendSpeedPic);
                        editor.putString("speedVCramer",speedVCramer);
                        editor.putString("isSendSpeedVid",isSendSpeedVid);
                        editor.putBoolean("isSettingSpeedInfo", isSettingSpeedInfo);
                        editor.putInt("limteValue", limteValue);
                        editor.commit();
                        isLimtedSpeed=true;
                        if(isSettingSpeedInfo==false){ //没有配置过急减速设置,默认为主摄像头拍照和摄像，不发送文件
                            mainReview();
                            savePic=true;
                            isCoverState=true;
                            stopRecordVideo();
                            mHandler.sendEmptyMessage(1113);
                        }else if(isSettingSpeedInfo==true){ //配置过急减速设置
                            if(speedCramer.equals("0")&&speedVCramer.equals("0")){
                                //第一种情况:主摄像头拍照，主摄像头摄像
                                Log.e("info", "主摄像头拍照，主摄像头摄像,开始急减速录像...");
                                mainReview();
                                savePic=true;
                                isCoverState=true;
                                stopRecordVideo();
                                mHandler.sendEmptyMessage(1113);
                            }else if(speedCramer.equals("0")&&speedVCramer.equals("1")){
                                //第2种情况：主摄像头拍照,副摄像头摄像
                                Log.e("info", "主摄像头拍照,副摄像头摄像,开始急减速录像...");
                                mainReview();
                                savePic=true;
                                isCoverState=true;
                                stopRecordVideo();
                                mHandler.sendEmptyMessage(1112);
                            }else if(speedCramer.equals("1")&&speedVCramer.equals("0")){
                                //第3种情况：副摄像头拍照，主摄像头摄像
                                Log.e("info", "副摄像头拍照，主摄像头摄像...");
                                secondReview();
                                savePic=true;
                                isCoverState=true;
                                mHandler.sendEmptyMessage(1113);
                            }else if(speedCramer.equals("1") &&speedCramer.equals("1")){
                                //第4种情况：副摄像头拍照，副摄像头摄像
                                Log.e("info", "副摄像头拍照，副摄像头摄像...");
                                secondReview();
                                savePic=true;
                                isCoverState=true;
                                stopRecordVideo();
                                mHandler.sendEmptyMessage(1112);
                            }
                        }
                        break;
                    }
                    case Constant.CAR_ACTION_FULL_SIZE: {
                        Log.e("info", "接收到了放大到全屏窗口大小的广播");
                        changeWindowManagerSize(Constant.CAR_ACTION_FULL_SIZE);
                        break;
                    }
                    case Constant.CAR_ACTION_HALF_SIZE: {
                        Log.e("info", "接收到了放大到一半窗口大小的广播");
                        changeWindowManagerSize(Constant.CAR_ACTION_HALF_SIZE);
                        break;
                    }
                    case Constant.CAR_ACTION_MINI_SIZE: {
                        Log.e("info", "MINI窗口广播");
                        changeWindowManagerSize(Constant.CAR_ACTION_MINI_SIZE);
                        // stopRecordVideo(); // 首先停止录像
                        // SystemClock.sleep(50);
                        // recordVideo(Constant.MAIN_CAMERA);
                        break;
                    }
                    case Constant.CAR_BACK_CUSTOM_KEY_EVENT: {
                        String flag = intent.getStringExtra("key_flag");
                        Log.e("info","flag>>>>>"+flag);
                        switch (flag) {
                            case Constant.CARBACKA: {
                                // 进入倒车后视模式
                                rearReview();
                                break;
                            }
                            case Constant.CARBACKB: {
                                // 退出倒车后视模式
                                if (currentCamera == Constant.MAIN_CAMERA) {
                                    mainReview();
                                } else {
                                    secondReview();
                                }
                                break;
                            }
                            default: {
                                break;
                            }
                        }
                    }
                    case Constant.ACTION_OPEN_ADAS: {
                        if (!isADASRunning) {
                            initADAS();
                            Log.e("info", "初始化ADAS广播");
                        }
                        break;
                    }
                    case Constant.ACTION_CLOSE_ADAS: {
                        if (isADASRunning) {
                            Log.e("info", "收到关闭ADAS广播");
                            releaseADASInterface();
                        }
                        break;
                    }

                    case "com.scblhkj.cp.FV_START": {   // WIFI热点时手动的开始录像
                        stopRecordVideo(); // 首先停止录像
                        chronometer.stop();
                        chronometer.setBase(SystemClock.elapsedRealtime());
                        SystemClock.sleep(50);
                        recordVideo(Constant.MAIN_CAMERA);
                        chronometer.start();
                        isUserC = true;
                        break;
                    }
                    case "com.scblhkj.cp.FV_END": {   // WIFI热点时手动的停止录像
                        chronometer.stop();
                        stopRecordVideo();  // 首先停止录像 // 停止录像保存视频
                        isUserC = false;
                        break;
                    }
                    case "com.scblhkj.cp.NET_FV_START": {   // 网络状态时手动的开始录像
                        stopRecordVideo(); // 首先停止录像
                        chronometer.stop();
                        SystemClock.sleep(50);
                        recordVideo(Constant.MAIN_CAMERA);
                        chronometer.setBase(SystemClock.elapsedRealtime());
                        chronometer.start();
                        isUserC = true;
                        isNetC=true;
                        Log.e("info"," isNetC=true; isUserC = true;");
                        mHandler.sendEmptyMessage(0x01);
                        break;
                    }
                    case "com.scblhkj.cp.NET_FV_END": {
                        stopRecordVideo();  // 首先停止录像 // 停止录像保存视频
                        isUserC = false;
                        isNetC=false;
                        mHandler.sendEmptyMessage(0x02);
                        SystemClock.sleep(50);
                        recordVideo(Constant.MAIN_CAMERA);

                        break;
                    }
                    case "com.scblhkj.cp.VIDEO_BACK_ON_REALTIME":{ //开启主摄像头视频实时查看的广播
                        Bundle bundle=intent.getExtras();
                        realTimeImei=bundle.getString("imei");
                        realTimeStamp=bundle.getString("timeStamp");
                        realTimeTarget=bundle.getString("target");
                        Log.e("info", "接收到的视频实时查看请求的imei:" + realTimeImei + "-------realtiemStamp>>" + realTimeStamp + "-----target>>" + realTimeTarget);
                        stopRecordVideo();
                        chronometer.stop();
                        textureMain.setVisibility(View.GONE);
                        stopCamera();
                        isRecording=false;
                        try {
                            mConfigure.put(AlivcMediaFormat.KEY_CAMERA_FACING, AlivcMediaFormat.CAMERA_FACING_BACK);
                            _CameraSurface.setVisibility(View.VISIBLE);
                            isRealTimeRecord=true;
                            realTimeCramer=0;
                            editor.putBoolean("isRealTimeRecord",isRealTimeRecord);
                            editor.putInt("realTimeCramer", realTimeCramer);
                            editor.commit();
                            sendIsRealTimeRecorder(isRealTimeRecord, realTimeCramer);
                        } catch (Exception e) {
                        }
                        break;
                    }
                    case "com.scblhkj.cp.VIDEO_FRO_ON_REALTIME":{  //开启副摄像头视频实时查看的广播
                        Bundle bundle=intent.getExtras();
                        realTimeImei=bundle.getString("imei");
                        realTimeStamp=bundle.getString("timeStamp");
                        realTimeTarget=bundle.getString("target");
                        Log.e("info", "接收到的视频实时查看请求的imei:" + realTimeImei + "-------realtiemStamp>>" + realTimeStamp + "-----target>>"+realTimeTarget);
                        stopRecordVideo();
                        chronometer.stop();
                        textureMain.setVisibility(View.GONE);
                        stopCamera();
                        isRecording=false;
                        try {
                            mConfigure.put(AlivcMediaFormat.KEY_CAMERA_FACING, AlivcMediaFormat.CAMERA_FACING_FRONT);
                            _CameraSurface.setVisibility(View.VISIBLE);
                            isRealTimeRecord=true;
                            realTimeCramer=1;
                            editor.putBoolean("isRealTimeRecord", isRealTimeRecord);
                            editor.putInt("realTimeCramer", realTimeCramer);
                            editor.commit();
                            sendIsRealTimeRecorder(isRealTimeRecord,realTimeCramer); //将当前是否处于实时视频查看的状态发送广播
                        } catch (Exception e) {
                        }
                        break;
                    }
                    case "com.scblhkj.cp.VIDEO_BACK_OFF_REALTIME":{ //关闭主摄像头视频实时查看的广播
                        Bundle bundle=intent.getExtras();
                        realTimeImei=bundle.getString("imei");
                        realTimeStamp=bundle.getString("timeStamp");
                        realTimeTarget=bundle.getString("target");
                        Log.e("info", "接收到的停止视频实时查看请求的imei:" + realTimeImei + "-------realtiemStamp>>" + realTimeStamp + "-----target>>" + realTimeTarget);
                        mAlivcMediaRecorder.stopRecord();
                        mAlivcMediaRecorder.reset();
                        _CameraSurface.setVisibility(View.GONE);
                        isRealTimeRecord=false;
                        isInterruptRealTime=false;
                        realTimeCramer=2;
                        editor.putBoolean("isRealTimeRecord", isRealTimeRecord);
                        editor.putInt("realTimeCramer", realTimeCramer);
                        editor.commit();
                        sendIsRealTimeRecorder(isRealTimeRecord, realTimeCramer);////将当前是否处于实时视频查看的状态发送广播
                        textureMain.setVisibility(View.VISIBLE);
                        chronometer.setBase(SystemClock.elapsedRealtime());
                        chronometer.start();
                        recordVideo(Constant.MAIN_CAMERA);//关闭副摄像头的实时查看后返回主摄像头拍摄
                        CtrlMisc.CtrlMisc_radar_powerctrl_jni(0);
                        sendStopFroRealTimeBroadcast(realTimeImei,realTimeStamp,realTimeTarget,isInterruptRealTime);
                        break;
                    }
                    case "com.scblhkj.cp.VIDEO_FRO_OFF_REALTIME":{ //关闭副摄像头实时视频查看
                        Bundle bundle=intent.getExtras();
                        realTimeImei=bundle.getString("imei");
                        realTimeStamp=bundle.getString("timeStamp");
                        realTimeTarget=bundle.getString("target");
                        Log.e("info", "接收到的停止视频实时查看请求的imei:" + realTimeImei + "-------realtiemStamp>>" + realTimeStamp + "-----target>>" + realTimeTarget);
                        mAlivcMediaRecorder.stopRecord();
                        mAlivcMediaRecorder.reset();
                        _CameraSurface.setVisibility(View.GONE);
                        isRealTimeRecord=false;
                        isInterruptRealTime=false;
                        realTimeCramer=2;
                        editor.putBoolean("isRealTimeRecord", isRealTimeRecord);
                        editor.putInt("realTimeCramer", realTimeCramer);
                        editor.commit();
                        sendIsRealTimeRecorder(isRealTimeRecord, realTimeCramer);//将当前是否处于实时视频查看的状态发送广播
                        textureMain.setVisibility(View.VISIBLE);
                        chronometer.setBase(SystemClock.elapsedRealtime());
                        chronometer.start();
                        recordVideo(Constant.MAIN_CAMERA);//关闭前置摄像头的实时查看后返回主摄像头拍摄
                        CtrlMisc.CtrlMisc_radar_powerctrl_jni(0);
                        sendStopFroRealTimeBroadcast(realTimeStamp,realTimeImei,realTimeTarget,isInterruptRealTime);
                        break;
                    }
                    case Constant.ACTION_ACC_INSERT: {  // ACC上电
                        isCarStop = false;  // 判断汽车点火
                        break;
                    }
                    case Constant.ACTION_ACC_REMOVE: { // ACC下电
                        isCarStop = true;  // 判断汽车停车
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        };
    }

    /***
     * 执行关于摄像头指令前判断是否在实时视频查看的方法
     */
    private void getRealTimeRecorderInfo() {
        if(isRealTimeRecord=true){ //如果当前处于实时视频查看状态
            if(realTimeCramer==0){ //如果实时查看的摄像头是主摄像头时
                mAlivcMediaRecorder.stopRecord();
                mAlivcMediaRecorder.reset();
                _CameraSurface.setVisibility(View.GONE);
                isRealTimeRecord=false;
                isInterruptRealTime=true;
                realTimeCramer=0;
                editor.putBoolean("isRealTimeRecord", isRealTimeRecord);
                editor.putInt("realTimeCramer", realTimeCramer);
                editor.commit();
                sendIsRealTimeRecorder(isRealTimeRecord, realTimeCramer);////将当前是否处于实时视频查看的状态发送广播
                textureMain.setVisibility(View.VISIBLE);
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
                recordVideo(Constant.MAIN_CAMERA);//关闭后置摄像头的实时查看后返回主摄像头拍摄
                CtrlMisc.CtrlMisc_radar_powerctrl_jni(0);
                sendStopFroRealTimeBroadcast(realTimeStamp,realTimeImei,realTimeTarget,isInterruptRealTime);
                SystemClock.sleep(1000);
            }else if(realTimeCramer==1){
                mAlivcMediaRecorder.stopRecord();
                mAlivcMediaRecorder.reset();
                _CameraSurface.setVisibility(View.GONE);
                isRealTimeRecord=false;
                realTimeCramer=1;
                editor.putBoolean("isRealTimeRecord", isRealTimeRecord);
                editor.putInt("realTimeCramer", realTimeCramer);
                editor.commit();
                sendIsRealTimeRecorder(isRealTimeRecord, realTimeCramer);//将当前是否处于实时视频查看的状态发送广播
                textureMain.setVisibility(View.VISIBLE);
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
                recordVideo(Constant.SECOND_CAMERA);//关闭前置摄像头的实时查看后返回主摄像头拍摄
                CtrlMisc.CtrlMisc_radar_powerctrl_jni(1);
                sendStopFroRealTimeBroadcast(realTimeStamp,realTimeImei,realTimeTarget,isInterruptRealTime);
                SystemClock.sleep(1000);
            }
        }
    }

    /***
     * 发送实时视频和普通视频状态的广播
     * @param isRealTimeRecorder 当前是否处于实时视频查看的状态
     * @param realTimeCramer  实时视频查看的摄像头 0:主摄像头 1:副摄像头 2:没有实时视频查看
     */
    private void sendIsRealTimeRecorder(Boolean isRealTimeRecorder,int realTimeCramer) {
        Intent intent=new Intent();
        intent.setAction(Constant.SEND_IS_VIDEO_THREAD_STATE);
        intent.putExtra("isRealTimeRecorder", isRealTimeRecord);
        intent.putExtra("realTimeCramer",realTimeCramer);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intent);
    }

    /***
     * 停止视频实时查看的广播
     * @param realTimeStamp 时间戳
     * @param realTimeImei   imei
     * @param realTimeTarget  摄像头
     * @param isInterruptRealTime  是否被打断
     */
    private void sendStopFroRealTimeBroadcast(String realTimeStamp,String realTimeImei,String realTimeTarget,boolean isInterruptRealTime){
        Intent intent=new Intent();
        intent.setAction("com.scblhkj.chappie.OFF_FRO_REALTIME_SUCCESS");
        intent.putExtra("realtimeimei", realTimeImei);
        intent.putExtra("realtimestamp", realTimeStamp);
        intent.putExtra("realtimetarget",realTimeTarget);
        intent.putExtra("isInterruptRealTime",isInterruptRealTime);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intent);
    }
    public class adasOpenReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            String action=intent.getAction();
            if(action.equals("com.chappie.OPENADAS")){
                openADAS();
            }
        }
    }

    public class adasCloseReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            String action=intent.getAction();
            if(action.equals("com.chappie.CLOSEADAS")){
                closeADAS();
            }
        }
    }
    private void registerAdasCloseReceiver(){
        IntentFilter adasCloseFilter=new IntentFilter();
        adasCloseFilter.addAction("com.chappie.CLOSEADAS");
        registerAdasCloseReceiver=new adasCloseReceiver();
        registerReceiver(registerAdasCloseReceiver,adasCloseFilter);

    }
    private void sendCoverStateBroadCast(){ //碰撞参数状态
        Intent intent=new Intent();
        intent.setAction(Constant.ACTION_COVER_STATE);
        Bundle bundle=new Bundle();
        bundle.putBoolean("isCarColl", isCarColl);
        intent.putExtras(bundle);
        sendBroadcast(intent);
    }
    private void unRegisterAdasOpenReceiver(){
        if(registerAdasOpenReceiver!=null){
            unregisterReceiver(registerAdasOpenReceiver);
        }
    }
    private void unRegisterAdasCloseReceiver(){
        if(registerAdasCloseReceiver!=null){
            unregisterReceiver(registerAdasCloseReceiver);
        }
    }
    private void  registerAdasOpenReceiver(){
        IntentFilter adasCloseFilter=new IntentFilter();
        adasCloseFilter.addAction("com.chappie.OPENADAS");
        registerAdasOpenReceiver=new adasOpenReceiver();
        registerReceiver(registerAdasOpenReceiver, adasCloseFilter);
    }
    /**
     * 注册视频录制广播接收者
     */
    private void registerControlRecordReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        intentFilter.addAction(Constant.CAR_BACK_CUSTOM_KEY_EVENT); // 倒车广播
        intentFilter.addAction(Constant.CAR_COLLISION_ACTION);  // 汽车震动检测广播
        intentFilter.addAction(Constant.CAR_ACTION_FULL_SIZE);  // 全屏窗口
        intentFilter.addAction(Constant.CAR_ACTION_HALF_SIZE);  // 一半窗口
        intentFilter.addAction(Constant.CAR_ACTION_MINI_SIZE);  // 迷你窗口
        intentFilter.addAction("com.android.chappie.ACTION_WIFI_C_PIC");  // wifi主动拍照
        intentFilter.addAction(Constant.ACTION_OPEN_ADAS);
        intentFilter.addAction(Constant.ACTION_CLOSE_ADAS);
        intentFilter.addAction("com.scblhkj.cp.FV_START");   // WIFI热点时手动的启动录像
        intentFilter.addAction("com.scblhkj.cp.FV_END");   // WIFI热点时手动的结束录像
        intentFilter.addAction("com.scblhkj.cp.NET_FV_START");   // 网络状态下手动的启动录像
        intentFilter.addAction("com.scblhkj.cp.NET_FV_END");   // 网络状态下手动的结束录像
        intentFilter.addAction("com.scblhkj.cp.VIDEO_FRO_ON_REALTIME");//获取前置摄像头视频实时查看
        intentFilter.addAction("com.scblhkj.cp.VIDEO_FRO_OFF_REALTIME");//获取关闭前置摄像头视频实时查看
        intentFilter.addAction("com.scblhkj.cp.VIDEO_BACK_ON_REALTIME");//获取后置主摄像头视频实时查看
        intentFilter.addAction("com.scblhkj.cp.VIDEO_BACK_OFF_REALTIME");//获取关闭后置主摄像头视频实时查看
        intentFilter.addAction(Constant.ACTION_ACC_INSERT); // ACC上电
        intentFilter.addAction(Constant.ACTION_ACC_REMOVE);  // ACC下电
        intentFilter.addAction("com.android.chappie.ACTION_NET_C_VID");//主动录像
        intentFilter.addAction("com.android.chappie.ACTION_NET_C_PIC");//主动录像
        intentFilter.addAction("com.android.chappie.MODIFY_QUAKE");//配置震动参数以及摄像头信息
        intentFilter.addAction(Constant.SEND_LIMITED_SOEED_HAPPEN);//发生急减速事件
        intentFilter.addAction(Constant.SEND_VIDEO_THREAD);//发送视频流地址
        intentFilter.addAction(Constant.SEND_IS_VIDEO_THREAD_STATE);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    /**
     * 注销广播接受者
     */
    private void unRegisterControlRecordReceiver() {
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    static {
        System.loadLibrary("opencv_java");
    }

    private void initADAS() {
        paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(5);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        if (AI != null) {
            AI.ReleaseInterface();
            AI = null;
        }
        AI = new ADASInterface(mPreHeight, mPreWidth, getApplicationContext());
        AI.setDebug(1);
        openADAS();
    }

    /**
     * 关闭ADAS
     */
    private void releaseADASInterface() {
        if (AI != null) {
            AI.setLane(ADASInterface.SET_OFF);
            AI.setVehicle(ADASInterface.SET_OFF);
            AI.ReleaseInterface();
            isADASRunning = false;
            AI = null;
            Log.e("info", "释放的ADAS操作接口");
        }
    }

    /**
     * 关闭ADAS
     */
    private void closeADAS() {
        AI.setLane(ADASInterface.SET_OFF);
        AI.setVehicle(ADASInterface.SET_OFF);
        isADASRunning=false;
        Intent intent=new Intent("com.chappie.ADASSETTING");
        Bundle bundle=new Bundle();
        bundle.putBoolean("isADASRunning",isADASRunning);
        intent.putExtras(bundle);
        sendBroadcast(intent);
        Log.e("info","关闭ADAS操作接口");
    }

    /**
     * 开启ADAS
     */
    private void openADAS() {
        AI.setLane(ADASInterface.SET_ON);
        AI.setVehicle(ADASInterface.SET_ON);
        isADASRunning = true;
        Intent intent=new Intent("com.chappie.ADASSETTING");
        Bundle bundle=new Bundle();
        bundle.putBoolean("isADASRunning",isADASRunning);
        intent.putExtras(bundle);
        sendBroadcast(intent);
        Log.e("info", "启动ADAS操作接口");
    }

    private DoDealVideoTask doDealVideoTask;

    private long rangeTime;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case TimeUp:
                    chronometer.setBase(SystemClock.elapsedRealtime());//计时器清零
                    //SystemClock.sleep(100);
                    // chronometer.start();//开始计时
                    break;
                case CHECK_TF_SAPCE: {
                    if (!SDCardScannerUtil.isSecondSDcardMounted()) {
                        mHandler.sendEmptyMessage(NO_TF_CARD);
                    } else {
                        long tfA = SDCardScannerUtil.getAvailableSize(SDCardScannerUtil.getTFDir());
                        if (tfA - (1 * 1024 * 1024 * 1024f) <= 0) {  // 空间小于1G
                            doDealVideoTask = new DoDealVideoTask();
                            doDealVideoTask.execute();
                        } else {
                            Log.e("info", "可用空间大于2G");
                        }
                    }
                    break;
                }
                case NO_TF_CARD: {
                    ToastUtil.showToast("SD卡异常,不能使用该功能!");
                    break;
                }
                case FILE_DELETE_FAIL: {
                    ToastUtil.showToast("锁存视频过多，请手动删除锁存视频");
                    break;
                }
                case FILE_DELETE_OVER: {
                    break;
                }
                case CloseChronometer:{

                    stopRecordVideo();
                    // rangeTime=   SystemClock.elapsedRealtime()-chronometer.getBase();
                    chronometer.stop();
                    ToastUtil.showToast("视频录制已停止");
                    isRecording=false;
                    break;
                }
                case OpenChronometer:{
                    recordVideo(Constant.MAIN_CAMERA);
                    chronometer.start();
                    CtrlMisc.CtrlMisc_radar_powerctrl_jni(0);
                    ToastUtil.showToast("视频录制已打开");
                    isRecording=true;
                    break;
                }
                case 0x01:
                    ToastUtil.showToast("视频录制开始了");
                    break;
                case 0x02:
                    ToastUtil.showToast("视频录制结束了");
                    break;
                case 0x03:
                    ToastUtil.showToast("开始录制震动视频");
                    break;
                case 0x07:
                    ToastUtil.showToast("震动视频录制完毕");
                    chronometer.stop();
                    SystemClock.sleep(50);
                    isCoverState=false;
                    isCarColl=false;
                    sendCoverStateBroadCast();
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();
                    recordVideo(Constant.SECOND_CAMERA);
                    CtrlMisc.CtrlMisc_radar_powerctrl_jni(1);
                    break;
                case 0x08:
                    ToastUtil.showToast("远程视频录制完毕");
                    String filePath = currentViewFile.getPath(); // 刚刚录制完成视频的路径
                    Intent filePathIntent = new Intent();
                    filePathIntent.setAction(Constant.FINISH_NET_VIDEO_FILE);
                    filePathIntent.putExtra("com.scblhkj.chappie.FINISH_VIDEO_FILE_VALUE", filePath);
                    filePathIntent.putExtra("imei",realTimeImei);
                    filePathIntent.putExtra("ctime",realTimeStamp);
                    filePathIntent.putExtra("cramer",cramer);
                    filePathIntent.putExtra("isSendVideo",isSendFile);
                    filePathIntent.addCategory(Intent.CATEGORY_DEFAULT);
                    sendBroadcast(filePathIntent);
                    Log.e("info", "发送录制好的视频文件,文件路径是" + filePath);
                    chronometer.stop();
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    SystemClock.sleep(50);
                    chronometer.start();
                    break;
                case 0x09://客户端请求和后视镜当前的镜头都是主摄像头时
                    stopRecordVideo(); // 首先停止录像
                    chronometer.stop();
                    SystemClock.sleep(50);
                    ToastUtil.showToast("开始录制远程视频!");
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();
                    recordVideo(Constant.MAIN_CAMERA);
                    CtrlMisc.CtrlMisc_radar_powerctrl_jni(0);
                    break;
                case 100:
                    //客户端请求的是主摄像头，而当前后视镜为前置摄像头时
                    stopRecordVideo(); // 首先停止录像
                    chronometer.stop();
                    SystemClock.sleep(50);
                    ToastUtil.showToast("开始录制远程视频!");
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();
                    recordVideo(Constant.MAIN_CAMERA);
                    CtrlMisc.CtrlMisc_radar_powerctrl_jni(0);
                    break;
                case 101://客户端请求和后视镜当前的镜头都是前置摄像头时
                    stopRecordVideo(); // 首先停止录像
                    chronometer.stop();
                    SystemClock.sleep(50);
                    ToastUtil.showToast("开始录制远程视频!");
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();
                    recordVideo(Constant.SECOND_CAMERA);
                    CtrlMisc.CtrlMisc_radar_powerctrl_jni(1);
                    break;
                case 102://客户端请求的是前置摄像头，而当前后视镜为主摄像头时
                    stopRecordVideo(); // 首先停止录像
                    chronometer.stop();
                    SystemClock.sleep(50);
                    ToastUtil.showToast("开始录制远程视频!");
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();
                    recordVideo(Constant.SECOND_CAMERA);
                    CtrlMisc.CtrlMisc_radar_powerctrl_jni(1);
                    break;
                case 103:
                    Intent modifyQuakeSucessIntent=new Intent();
                    modifyQuakeSucessIntent.addCategory(Intent.CATEGORY_DEFAULT);
                    modifyQuakeSucessIntent.setAction(Constant.SEND_MODIFY_QUAKE_SUCCESS);
                    modifyQuakeSucessIntent.putExtra("modifyQuakeImei", realTimeImei);
                    modifyQuakeSucessIntent.putExtra("modifyQuakeTime", realTimeStamp);
                    sendBroadcast(modifyQuakeSucessIntent);
                    ToastUtil.showToast("修改震动告警参数成功!");
                    break;
                case 104:
                    Intent modifyQuakeFailIntent=new Intent();
                    modifyQuakeFailIntent.addCategory(Intent.CATEGORY_DEFAULT);
                    modifyQuakeFailIntent.setAction(Constant.SEND_MODIFY_QUAKE_FAIL);
                    modifyQuakeFailIntent.putExtra("modifyQuakeImei", realTimeImei);
                    modifyQuakeFailIntent.putExtra("modifyQuakeTime",realTimeStamp);
                    sendBroadcast(modifyQuakeFailIntent);
                    ToastUtil.showToast("修改震动告警参数失败，参数需小于100!");
                    break;
                case 110: //当前摄像头为主摄像头，客户端请求的是主摄像头拍照和摄像，并都需要发送相关的文件
                    ToastUtil.showToast("震动视频录制完毕");
                    chronometer.stop();
                    SystemClock.sleep(50);
                    isCoverState=false;
                    isCarColl=false;
                    stopRecordVideo();
                    sendCoverStateBroadCast();
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();
                    recordVideo(Constant.MAIN_CAMERA);
                    CtrlMisc.CtrlMisc_radar_powerctrl_jni(0);
                    break;
                case 120: //当前摄像头为主摄像头，客户端请求的是主摄像头拍照和摄像，并都需要发送相关的文件
                    ToastUtil.showToast("急减速视频录制完毕");
                    chronometer.stop();
                    SystemClock.sleep(50);
                    isCoverState=false;
                    isCarColl=false;
                    sendCoverStateBroadCast();
                    stopRecordVideo();
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();
                    recordVideo(Constant.MAIN_CAMERA);
                    CtrlMisc.CtrlMisc_radar_powerctrl_jni(0);
                    break;
                case 130:
                    ToastUtil.showToast("急减速视频录制完毕");
                    chronometer.stop();
                    SystemClock.sleep(50);
                    isCoverState=false;
                    isCarColl=false;
                    sendCoverStateBroadCast();
                    stopRecordVideo();
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();
                    recordVideo(Constant.SECOND_CAMERA);
                    CtrlMisc.CtrlMisc_radar_powerctrl_jni(1);
                    break;
                case 1110://碰撞客户端请求的是主摄像头摄像
                    isCarColl=true;
                    stopRecordVideo();
                    SystemClock.sleep(1000);
                    ToastUtil.showToast("开始录制碰撞视频");
                    recordVideo(Constant.MAIN_CAMERA);
                    CtrlMisc.CtrlMisc_radar_powerctrl_jni(0);
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();
                    sendCoverStateBroadCast();
                    Log.e("info", "HddrisCarColl1>>>" + isCarColl);
                    stopRecordTimer = new Timer();
                    stopRecorderTask = new TimerTask() {
                        @Override
                        public void run() {
                            Log.e("info","HddrisCarColl2>>>"+isCarColl);
                            stopRecordVideo();
                            Log.e("info","碰撞视频录制完毕!");
                            mHandler.sendEmptyMessage(110);
                        }
                    };
                    stopRecordTimer.schedule(stopRecorderTask, 1000 * 10);//录制10s
                    break;
                case 1111://碰撞客户端请求的是副摄像头摄像
                    isCarColl=true;
                    SystemClock.sleep(100);
                    recordVideo(Constant.SECOND_CAMERA);
                    CtrlMisc.CtrlMisc_radar_powerctrl_jni(1);
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();
                    ToastUtil.showToast("开始录制碰撞视频");
                    sendCoverStateBroadCast();
                    Log.e("info", "HddrisCarColl1>>>" + isCarColl);
                    stopRecordTimer = new Timer();
                    stopRecorderTask = new TimerTask() {
                        @Override
                        public void run() {
                            Log.e("info","HddrisCarColl2>>>"+isCarColl);
                            stopRecordVideo();
                            Log.e("info","碰撞视频录制完毕!");
                            mHandler.sendEmptyMessage(0x07);
                        }
                    };
                    stopRecordTimer.schedule(stopRecorderTask, 1000 * 10);//录制10s
                    break;
                case 1112://急减速客户端请求的是副摄像头摄像
                    SystemClock.sleep(100);
                    recordVideo(Constant.SECOND_CAMERA);
                    CtrlMisc.CtrlMisc_radar_powerctrl_jni(1);
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();
                    stopRecordTimer = new Timer();
                    stopRecorderTask = new TimerTask() {
                        @Override
                        public void run() {
                            // Log.e("info","HddrisCarColl2>>>"+isCarColl);
                            stopRecordVideo();
                            Log.e("info","急减速视频录制完毕!");
                            mHandler.sendEmptyMessage(130);
                        }
                    };
                    stopRecordTimer.schedule(stopRecorderTask, 1000 * 10);//录制10s
                    break;
                case 1113://急减速客户端请求的是主摄像头摄像
                    SystemClock.sleep(100);
                    recordVideo(Constant.MAIN_CAMERA);
                    CtrlMisc.CtrlMisc_radar_powerctrl_jni(0);
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();
                    stopRecordTimer = new Timer();
                    stopRecorderTask = new TimerTask() {
                        @Override
                        public void run() {
                            stopRecordVideo();
                            Log.e("info","急减速视频录制完毕!");
                            mHandler.sendEmptyMessage(120);
                        }
                    };
                    stopRecordTimer.schedule(stopRecorderTask, 1000 * 10);//录制10s
                    break;
                default: {
                    break;
                }
            }
            super.handleMessage(msg);
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * 初始化记录仪录制View
     */
    private void initWindowManagerView() {
        windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        layoutParams = new WindowManager.LayoutParams();

        layoutParams.type = WindowManager.LayoutParams.TYPE_PHONE;  //这种模式下可以使录制窗口在屏幕最顶端
        layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED; //标志窗口不获取焦点，使录制窗口之外的其他地方可点击
        //设置图片格式，效果为背景透明
        layoutParams.format = PixelFormat.RGBA_8888;
        //调整悬浮窗显示的停靠位置为左侧置顶
        // layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
        layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
        // 以屏幕左上角为原点，设置x、y初始值，相对于gravity
        layoutParams.x = 0;
        layoutParams.y = 0;
        //设置悬浮窗口长宽数据
        layoutParams.width = 1;
        layoutParams.height = 1;
        // layoutParams.width=WindowManager.LayoutParams.MATCH_PARENT;
        // layoutParams.height=WindowManager.LayoutParams.MATCH_PARENT;
        hddvrView = View.inflate(this, R.layout.hdddvr_layout, null);
        windowManager.addView(hddvrView, layoutParams);
        hddvrView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        findView(hddvrView);
        addListener();
    }

    /**
     * 遍历所有布局
     *
     * @param view
     */
    private void findView(View view) {
        textureMain = (TextureView) view.findViewById(R.id.tv);
        _CameraSurface=(SurfaceView) view.findViewById(R.id.sur_main);
        _CameraSurface.getHolder().addCallback(_CameraSurfaceCallback);
        im_switchCamera = (ImageView) view.findViewById(R.id.im_switchCamera);
        im_tp = (ImageView) view.findViewById(R.id.im_tp);
        im_moreFunction = (ImageView) view.findViewById(R.id.im_moreFunction);
        im_videoset=(ImageView)view.findViewById(R.id.im_videoset);
        rl_choiceCamera = (RelativeLayout) view.findViewById(R.id.rl_choiceCamera);
        ll_mainCamera = (LinearLayout) view.findViewById(R.id.ll_mainCamera);
        ll_secondCamera = (LinearLayout) view.findViewById(R.id.ll_secondCamera);
        ll_backCamera = (LinearLayout) view.findViewById(R.id.ll_backCamera);
        iv_recorderpose=(ImageView)view.findViewById(R.id.iv_recorderpose);
        chronometer=(Chronometer)view.findViewById(R.id.chronometer);
        iv = (ImageView) view.findViewById(R.id.iv);
    }

    private void addListener() {
        im_switchCamera.setOnClickListener(this);
        im_tp.setOnClickListener(this);
        im_moreFunction.setOnClickListener(this);
        ll_mainCamera.setOnClickListener(this);
        ll_secondCamera.setOnClickListener(this);
        ll_backCamera.setOnClickListener(this);
        rl_choiceCamera.setOnClickListener(this);
        iv_recorderpose.setOnClickListener(this);
        textureMain.setSurfaceTextureListener(this);
        im_videoset.setOnClickListener(this);
        Log.e("info", "设置完成了所有的监听事件");
    }

    private Surface mPreviewSurface;
    /***
     * 视频实时查看的界面设置
     */
    private final SurfaceHolder.Callback _CameraSurfaceCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            holder.setKeepScreenOn(true);
            mPreviewSurface = holder.getSurface();
            Log.e("info", "surfaceCreated里的mPreviewSurface"+mPreviewSurface);
            mAlivcMediaRecorder.prepare(mConfigure, mPreviewSurface);
            isSurMain=true;
            isRecording=true;
            Log.e("info", "mAlivcMediaRecorder设置prepare(mConfigure, mPreviewSurface");
            if ((int) mConfigure.get(AlivcMediaFormat.KEY_CAMERA_FACING) == AlivcMediaFormat.CAMERA_FACING_FRONT) {
                mAlivcMediaRecorder.addFlag(AlivcMediaFormat.FLAG_BEAUTY_ON);
            }


        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            mAlivcMediaRecorder.setPreviewSize(width, height);
            mPreviewWidth = width;
            mPreviewHeight = height;
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            mPreviewSurface = null;
            mAlivcMediaRecorder.stopRecord();
            mAlivcMediaRecorder.reset();
        }
    };
    /**
     * 初始化摄像头
     *
     * @param type
     */
    private boolean openCamera(int type) {
        try {
            mCamera = null;
            switch (type) {
                case Constant.MAIN_CAMERA: {
                    CtrlMisc.CtrlMisc_camera_internal_ctrl(1);  // 退出倒车模式
                    mCamera = Camera.open(Constant.MAIN_CAMERA);
                    currentCamera = Constant.MAIN_CAMERA;
                    Log.e("info", "初始化主摄像头");
                    break;
                }
                case Constant.SECOND_CAMERA: {
                    CtrlMisc.CtrlMisc_camera_internal_ctrl(1);  // 退出倒车模式
                    mCamera = Camera.open(Constant.SECOND_CAMERA);
                    currentCamera = Constant.SECOND_CAMERA;
                    Log.e("info", "打开副摄像头时mCamera>>>>"+mCamera);
                    Log.e("info", "初始化副摄像头");
                    break;
                }
                case Constant.BACK_CAMERA: {
                    Log.e("info", "进入初始化倒车摄像头");
                    CtrlMisc.CtrlMisc_camera_internal_ctrl(0);  // 切换到倒车模式
                    Log.e("info", "切换到倒车模式");
                    mCamera = Camera.open(1);
                    currentCamera = Constant.BACK_CAMERA;
                    Log.e("info", "初始化倒车摄像头");
                    break;
                }
                default: {
                    break;
                }
            }
            return true;
        } catch (Exception e) {
            if (mCamera != null) {
                mCamera.release();
                mCamera = null;
                Log.e("info", "初始化摄像头出现异常");
            }
        }
        return false;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        sp=getSharedPreferences("hddrvstate", Context.MODE_PRIVATE);
        editor=sp.edit();
        //指定声音池的最大音频流数目为10，声音品质为5
        TelephonyManager tm = (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);
        imei=tm.getDeviceId();//String
        getAppInfo();
        mTimer=new Timer();
        Log.e("info", "HddrvImei" + imei);
        //rtmp://video-center.alivecdn.com/AppName/StreamName?vhost=vod.blhcp.com
        pushUrl="rtmp://video-center.alivecdn.com/chappie/"+imei+"?vhost=vod.blhcp.com";
        soundPool = new SoundPool(5, AudioManager.STREAM_SYSTEM, 5);
        //载入音频流，返回在池中的id
        cshotID = soundPool.load(this, R.raw.cshot, 0);
        //视频实时监控初始化
        videoResolution = AlivcMediaFormat.OUTPUT_RESOLUTION_240P;//设置拍摄的分辨率是240P
        mAlivcMediaRecorder = AlivcMediaRecorderFactory.createMediaRecorder();//创建对象
        Log.e("info", "mAlivcMediaRecorder>>>" + mAlivcMediaRecorder);
        mAlivcMediaRecorder.init(this);//初始化
        // mDataStatistics.setReportListener(mReportListener);
        mRecordReporter = mAlivcMediaRecorder.getRecordReporter();
        mAlivcMediaRecorder.setOnRecordStatusListener(mRecordStatusListener);
        mAlivcMediaRecorder.setOnNetworkStatusListener(mOnNetworkStatusListener);
        mAlivcMediaRecorder.setOnRecordErrorListener(mOnErrorListener);
        if(realTimeTarget==null){
            mConfigure.put(AlivcMediaFormat.KEY_CAMERA_FACING, AlivcMediaFormat.CAMERA_FACING_FRONT);
        }else if(realTimeTarget!=null&&realTimeTarget.equals("1")){//当请求指令为1的时候是调用前置摄像头
            mConfigure.put(AlivcMediaFormat.KEY_CAMERA_FACING, AlivcMediaFormat.CAMERA_FACING_FRONT);
        }else if(realTimeTarget!=null&&realTimeTarget.equals("0")){//当请求指令为0的时候是调用后置摄像头
            mConfigure.put(AlivcMediaFormat.KEY_CAMERA_FACING, AlivcMediaFormat.CAMERA_FACING_BACK);
        }
        mConfigure.put(AlivcMediaFormat.KEY_MAX_ZOOM_LEVEL, 3);
        mConfigure.put(AlivcMediaFormat.KEY_OUTPUT_RESOLUTION, videoResolution);
        mConfigure.put(AlivcMediaFormat.KEY_MAX_VIDEO_BITRATE, 800000);
        //mConfigure.put(AlivcMediaFormat.KEY_DISPLAY_ROTATION, screenOrientation ? AlivcMediaFormat.DISPLAY_ROTATION_90 : AlivcMediaFormat.DISPLAY_ROTATION_0);
        mConfigure.put(AlivcMediaFormat.KEY_EXPOSURE_COMPENSATION, 20);//曝光度

        aCache = ACache.get(this);
        if (hsBean == null) {
            hsBean = new HddvrSettingBean(true, true, "高清模式", "中", 60);
            aCache.put("hddvr_set", hsBean);
        }
        currentCamera = Constant.MAIN_CAMERA;
        imageUtilEngine = new ImageUtilEngine();
        /**
         * 初始化VolleyClient
         */
        volleyClient = VolleyClient.getInstance(this);
        hsBean = (HddvrSettingBean) aCache.getAsObject("hddvr_set");
        Log.e("info", "haBean》》》》"+hsBean.toString());
        if (hsBean.isbAdas()) {
            initADAS();
            Log.e("info", "初始化完成了ADAS配置");
        } else {
            Log.e("info", "关闭了ADAS");
        }

        registerAdasCloseReceiver();
        registerAdasOpenReceiver();
        initControlRecordReceiver();
        // registerVideoContReceiver();
        registerControlRecordReceiver();  // 注册广播接收者
        Log.e("info", "录制服务中，广播接收者注册完成");
        initWindowManagerView();
        Log.e("info", "录制服务启动完成");
        if (onHddvrServiceCreate != null) {
            onHddvrServiceCreate.onServiceCreate();
        }
    }

    private void getAppInfo() {

        try {
            String packageInfo=this.getPackageName();
            String versionName=this.getPackageManager().getPackageInfo(packageInfo,0).versionName;
            int versionCode=this.getPackageManager().getPackageInfo(packageInfo,0).versionCode;
            Log.e("info","packageInfo>>"+packageInfo+"   versionName>>"+versionName+"   versionCode>>"+versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    //生成行车记录仪视频文件保存的地址
    public  File buildSaveVideoFile() {
        File file;
        File dir = new File("/storage/sdcard1/ChappieLauncher/travel", CommonUtil.buildNowDay());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        file = new File(dir.getPath(), CommonUtil.buileNowTime() + ".mp4");
        Log.e("info", "构建的视频文件路径为" + file.getPath());
        return file;
    }
    private OnLiveRecordErrorListener mOnErrorListener = new OnLiveRecordErrorListener() {
        @Override
        public void onError(int errorCode) {
            switch (errorCode) {
                case AlivcStatusCode.ERROR_AUTH_FAILED:
                    Log.e("info", "==== live auth failed, need new auth key ====");
                    // ToastUtils.showToast(LiveCameraActivity.this, "live auth failed, need new auth key");
                    break;
                case AlivcStatusCode.ERROR_SERVER_CLOSED_CONNECTION:
                case AlivcStatusCode.ERORR_OUT_OF_MEMORY:
                case AlivcStatusCode.ERROR_CONNECTION_TIMEOUT:
                case AlivcStatusCode.ERROR_BROKEN_PIPE:
                case AlivcStatusCode.ERROR_ILLEGAL_ARGUMENT:
                case AlivcStatusCode.ERROR_IO:
                case AlivcStatusCode.ERROR_NETWORK_UNREACHABLE:
                case AlivcStatusCode.ERROR_OPERATION_NOT_PERMITTED:
                default:
                    Log.e("info", "=== Live stream connection error-->" + errorCode + " ===");
                    // ToastUtils.showToast(LiveCameraActivity.this, "Live stream connection error-->" + errorCode);
                    break;
            }
        }
    };


    private OnNetworkStatusListener mOnNetworkStatusListener = new OnNetworkStatusListener() {
        @Override
        public void onNetworkBusy() {
            Log.e("info", "=== onNetworkBusy ===");
            Log.e("info", "当前网络状态极差，已无法正常流畅直播");
        }

        @Override
        public void onNetworkFree() {
            Log.e("info", "=== onNetworkFree ===");
        }

        @Override
        public void onConnectionStatusChange(int status) {
            switch (status) {
                case AlivcStatusCode.STATUS_CONNECTION_START:
                    //ToastUtils.showToast(LiveCameraActivity.this, "Start live stream connection!");
                    Log.e("info", "=== AlivcStatusCode.STATUS_CONNECTION_START ===");
                    Log.e("info", "=== Start live stream connection! ===");
                    break;
                case AlivcStatusCode.STATUS_CONNECTION_ESTABLISHED:
                    Log.e("info", "=== AlivcStatusCode.STATUS_CONNECTION_ESTABLISHED ===");
                    Log.e("info", "=== Live stream connection is established! ===");
                    Intent intent=new Intent();
                    intent.setAction("com.scblhkj.chappie.FRO_REALTIME_SUCCESS");
                    intent.putExtra("realtimeimei", realTimeImei);
                    intent.putExtra("realtimestamp", realTimeStamp);
                    intent.putExtra("realtimetarget",realTimeTarget);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    sendBroadcast(intent);
                    Log.e("info","开启前置摄像头视频实时查看成功后发送的realtimeimei>>"+realTimeImei+"realTimeStamp>>"+realTimeStamp
                            +"realtimetarget"+realTimeTarget);
                    //ToastUtils.showToast(LiveCameraActivity.this, "Live stream connection is established!");
                    break;
                case AlivcStatusCode.STATUS_CONNECTION_CLOSED:
                    Log.e("info", "=== AlivcStatusCode.STATUS_CONNECTION_CLOSED ===");
                    Log.e("info", "=== Live stream connection is closed!===");
                    //ToastUtils.showToast(LiveCameraActivity.this, "Live stream connection is closed!");
                    mAlivcMediaRecorder.stopRecord();
                    break;
            }
        }

        @Override
        public boolean onNetworkReconnect() {
            return true;
        }
    };


    private OnRecordStatusListener mRecordStatusListener = new OnRecordStatusListener() {
        @Override
        public void onDeviceAttach() {
            Log.e("info","ali摄像头打开成功");
        }

        @Override
        public void onDeviceAttachFailed(int facing) {

        }

        @Override
        public void onSessionAttach() {
            if (isRecording && !TextUtils.isEmpty(pushUrl)) {
                // mAlivcMediaRecorder.reset();
                Log.e("info", "=====" + pushUrl + "======");
                // mAlivcMediaRecorder.prepare(mConfigure, mPreviewSurface);
                mAlivcMediaRecorder.startRecord(pushUrl);
                Log.e("info", "onSessionAttach中mAlivcMediaRecorder.startRecord(pushUrl);");
            }
            mAlivcMediaRecorder.focusing(0.5f, 0.5f);
        }

        @Override
        public void onSessionDetach() {

        }

        @Override
        public void onDeviceDetach() {

        }

        @Override
        public void onIllegalOutputResolution() {
            Log.e("info", "选择输出分辨率过大");
            //ToastUtils.showToast(LiveCameraActivity.this, R.string.illegal_output_resolution);
        }
    };
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.im_switchCamera: {  // 切换摄像头
                switch (currentCamera) {
                    case Constant.MAIN_CAMERA: {
                        secondReview();
                        break;
                    }
                    case Constant.SECOND_CAMERA: {
                        mainReview();
                        break;
                    }
                    case Constant.BACK_CAMERA: {
                        mainReview();
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
            }
            case R.id.iv_recorderpose:{//暂停/播放按钮
                Log.e("info","isRecording>>>"+isRecording);
                if(isRecording==true){//如果是在录制的状态就将关闭录制
                    mHandler.sendEmptyMessage(CloseChronometer);

                }else if(isRecording==false){//如果是没有录制的状态就将开始录制
                    mHandler.sendEmptyMessage(OpenChronometer);
                }
                break;
            }
            case R.id.im_tp: {  // 拍照
                savePic = true;
                isUserP=true;//用户手动拍照
                break;
            }
            case R.id.im_videoset:{//设置按钮
                Intent intent = new Intent(getBaseContext(), HddvrSettingActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            }
            case R.id.im_moreFunction: { // 更多
                StartAppUtil.doStartApplicationWithPackageName(HddvrService.this,"com.example.localalbum");
                break;
            }
            case R.id.ll_mainCamera: {  // 切换到主摄像头
                mainReview();

                break;
            }
            case R.id.ll_secondCamera: { // 切换到副摄像头
                //    secondReview();
                closeADAS();
                // releaseADASInterface();
                stopRecordVideo();
                CtrlMisc.CtrlMisc_radar_powerctrl_jni(1);
                /**********************测试视频实时查看代码************************/
                hddvrView.setVisibility(View.GONE);
                Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
                screenWidth = display.getWidth();
                screenHeight = display.getHeight();
                layoutParams.width = screenWidth / 2;
                layoutParams.height = screenHeight;
                cameraDevice = Camera.open();
                Log.i("info", "cameara open");
                cameraView = new CameraView(this, cameraDevice);
                cameraView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                windowManager.addView(cameraView, layoutParams);
                rl_choiceCamera.setVisibility(View.GONE);
                if (!recording) {
                    startRealRecording();
                    Log.e("info", "Start Button Pushed");

                }
                break;
            }
            case R.id.ll_backCamera: { // 切换到倒车后视
                rl_choiceCamera.setVisibility(View.GONE);

            }
            case R.id.rl_choiceCamera: {
                rl_choiceCamera.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 配置摄像头参数
     */
    private void configCameraParams() {
        if (mCamera != null) {
            mCamera.stopPreview();
            try {
                Camera.Parameters parameters = mCamera.getParameters();
                parameters.setPictureFormat(ImageFormat.JPEG); //Sets the image format for picture 设定相片格式为JPEG，默认为NV21
                parameters.setPreviewFormat(ImageFormat.YV12);
                parameters.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
                parameters.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
                parameters.setPictureSize(mPicWidth, mPicHeight);
                parameters.setPreviewSize(mPreWidth, mPreHeight);
                parameters.setPreviewFrameRate(24);
                List<String> focusModes = parameters.getSupportedFocusModes();
                if (focusModes.contains("continuous-video")) {
                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
                }
                mCamera.setParameters(parameters);
                mCamera.setPreviewTexture(surfaceTexture);
                mCamera.startPreview();
                mCamera.unlock();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关闭摄像头
     */
    private void stopCamera() {
        if (mCamera != null) {
            try {
                mCamera.lock();
                mCamera.stopPreview();
                mCamera.release();
                isPreviewing = false;
                mCamera = null;
                Log.e("info", "mCamera" + mCamera);
                /* 停止预览 */
            } catch (Exception e) {
                Log.e("info", "停止相机的时候出错" + e.toString());
                mHandler.sendEmptyMessage(NO_TF_CARD);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy() {
        unRegisterControlRecordReceiver();// 注销震动检测广播接收者
        unRegisterAdasCloseReceiver();
        unRegisterAdasOpenReceiver();
        // unRegisterVideoContReceiver();
        stopCamera();
        mAlivcMediaRecorder.release();
        super.onDestroy();
    }

    /**
     * 手动拍照路径
     * @return
     */
    public  String buildArttificialPicPath(){
        File file=new File("/storage/sdcard1/ChappieLauncher/arttificial",CommonUtil.buildNowDay());
        if(!file.exists()){
            file.mkdirs();
        }
        return file.getPath() + "/" +CommonUtil.buileNowTime() + ".jpg";
    }

    /***
     * 创建保存自动拍照的路径
     * @return
     */
    public  String buildAutoPicPath(){
        File file=new File("/storage/sdcard1/ChappieLauncher/auto",CommonUtil.buildNowDay());
        if(!file.exists()){
            file.mkdirs();
        }
        return file.getPath() + "/" +CommonUtil.buileNowTime() + ".jpg";
    }

    /***
     * 保存自动录制视频路径
     * @return
     */
    public  File buildAutoVidPath(){
        File file;
        File dir = new File("/storage/sdcard1/ChappieLauncher/auto", CommonUtil.buildNowDay());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        file = new File(dir.getPath(), CommonUtil.buileNowTime() + ".mp4");
        Log.e("info", "构建的视频文件路径为" + file.getPath());
        return file;
    }
    /**
     * 手动摄像路径
     * @return
     */
    public  File buildArttificialVidPath(){
        File file;
        File dir = new File("/storage/sdcard1/ChappieLauncher/arttificial", CommonUtil.buildNowDay());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        file = new File(dir.getPath(), CommonUtil.buileNowTime() + ".mp4");
        Log.e("info", "构建的视频文件路径为" + file.getPath());
        return file;
    }
    /**
     * 初始化
     *
     * @throws IOException
     * @author liuyinjun
     * @date 2015-2-5
     */
    private void initRecord() {
        try {
            if (isCarStop) {
                return;   // 如果是汽车停止了，就停止录像功能
            }
            //设置后不会崩
            if (mMediaRecorder != null) {
                try {
                    mMediaRecorder.setOnErrorListener(null);
                    mMediaRecorder.setOnInfoListener(null);
                    mMediaRecorder.setPreviewDisplay(null);
                    mMediaRecorder.stop();
                    mMediaRecorder.reset();
                    mMediaRecorder.release();
                    mMediaRecorder = null;
                } catch (IllegalStateException e) {
                    // TODO: handle exception
                    Log.i("Exception", Log.getStackTraceString(e));
                }catch (RuntimeException e) {
                    // TODO: handle exception
                    Log.e("info", Log.getStackTraceString(e));
                }catch (Exception e) {
                    // TODO: handle exception
                    Log.i("Exception", Log.getStackTraceString(e));
                }
            }
            mMediaRecorder = new MediaRecorder();

            if (mCamera != null) {
                mMediaRecorder.setCamera(mCamera);
                Log.e("info", "初始化录像的时候相机对象不为空");
            } else {
                Log.e("info", "初始化录像的时候相机对象为空!!!");
            }
            hsBean = (HddvrSettingBean) aCache.getAsObject("hddvr_set");
            // 这个方案控制比较灵活，而且视频效果要好一些
            if (hsBean.isbRecord()) {  // 记录声音
                mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);// 设置音频录入源
            }
            mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);  // 设置视频图像的录入源
            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);// 设置录入媒体的输出格式
            if (hsBean.isbRecord()) {
                mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);// 设置音频编码
            }
            mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);// 设置视频编码
            if (hsBean.getsResolution().equals("高清模式")) {
                mMediaRecorder.setVideoSize(Constant.VIDEO_RECORD_WIDTH_HEIGH, Constant.VIDEO_RECORD_HEIGHT_HEIGH);  // 设置录制视频的尺寸
                mMediaRecorder.setVideoEncodingBitRate(Constant.VIDEO_ENCODING_BIT_RATE_HEIGH); // 设置录制的视频编码比特率，然后就清晰了
            } else if (hsBean.getsResolution().equals("标清模式")) {
                mMediaRecorder.setVideoSize(Constant.VIDEO_RECORD_WIDTH_NORMAL, Constant.VIDEO_RECORD_HEIGHT_NORMAL);  // 设置录制视频的尺寸
                mMediaRecorder.setVideoEncodingBitRate(Constant.VIDEO_ENCODING_BIT_RATE_NORMAL); // 设置录制的视频编码比特率，然后就清晰了
            }
            mMediaRecorder.setVideoFrameRate(24);//设置要捕获的视频帧速率
            if(isUserC==true){//如果是手动拍摄视频
                currentViewFile=buildArttificialVidPath();
                Log.e("info","进入手动拍摄视频路径");
            }else if(isCoverState=true&&isCarColl==true){//如果发生碰撞
                currentViewFile=buildAutoVidPath();
                Log.e("info","进入自动拍摄视频路径");
            }else{
                currentViewFile = buildSaveVideoFile();
                Log.e("info","进入正常拍摄视频路径");

            }
            mMediaRecorder.setOutputFile(currentViewFile.getPath());
            // 准备录制
            mMediaRecorder.prepare();
            Log.e("info", "准备录制视频");
            // 每段视频的开始录制时间戳
            tempStartRecordTime = System.currentTimeMillis();
            if (SDCardScannerUtil.isSecondSDcardMounted()) { // 判断是否有外置的sd卡
                // 开始录制
                mMediaRecorder.start();
                //isRecording=true;
                Log.e("info", "开始录制视频");
                //开始录像后，每隔1s去更新录像的时间戳
                //mHandler.postDelayed(mTimestampRunnable, 1000);
            } else {
                ToastUtil.showToast("您未安装外置SD卡，暂时无法使用视频录制功能");
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 录制视频
     */
    private void recordVideo(final int type) {
        try {
            if (!isPreviewing) { // 如果未打开摄像头，则打开
                isPreviewing = openCamera(type);
                configCameraParams();
                Log.e("info", isPreviewing ? "摄像头打开成功" : "摄像头打开失败");
            }
            initRecord();
            mTimeCount = 0;// 时间计数器重新赋值
            chronometer.start();
            isRecording=true;

            if(mTimer==null){
                mTimer = new Timer();
            }
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    mTimeCount++;
                    hsBean = (HddvrSettingBean) aCache.getAsObject("hddvr_set");
                    mRecordMaxTime = hsBean.getsTime();
                    Log.e("info","mTimeCount>>>>>"+mTimeCount);
                    Log.e("info","mRecordMaxTime>>>>>"+mRecordMaxTime);
                    if (mTimeCount == mRecordMaxTime) {// 达到指定时间，停止拍摄
                        stopRecordVideo();
                        recordVideo(type); // 重新开始录制
                        mHandler.sendEmptyMessage(CHECK_TF_SAPCE); // 发送检测外置TF卡剩余容量的消息
                        mHandler.sendEmptyMessage(TimeUp);
                    }
                }
            }, 0, 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 停止录制视频
     */
    private void stopRecordVideo() {
        if (!SDCardScannerUtil.isSecondSDcardMounted()) {  // 没有外置SD卡
            mHandler.sendEmptyMessage(NO_TF_CARD);
            Log.e("info", "未检测到外置SD卡");
            chronometer.stop();
            return;
        }
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        if (mMediaRecorder != null) {
            //设置后不会崩
            try {
                mMediaRecorder.setOnErrorListener(null);
                mMediaRecorder.setOnInfoListener(null);
                mMediaRecorder.setPreviewDisplay(null);
                mMediaRecorder.stop();
                mMediaRecorder.reset();
                mMediaRecorder.release();
                mMediaRecorder = null;
            } catch (IllegalStateException e) {
                // TODO: handle exception
                Log.i("Exception", Log.getStackTraceString(e));
            }catch (RuntimeException e) {
                // TODO: handle exception
                Log.e("info", Log.getStackTraceString(e));
            }catch (Exception e) {
                // TODO: handle exception
                Log.i("Exception", Log.getStackTraceString(e));
            }
        }
        checkIfLockVideo();
        stopCamera();
        if (isUserC) {  // 是用户手动录制视频，并超时没有手动的停止
            isUserC = false;
            if(isNetC){//网络状态下控制视频结束时发送视频
                isNetC=false;
                String filePath = currentViewFile.getPath(); // 刚刚录制完成视频的路径
                Intent filePathIntent = new Intent();
                filePathIntent.setAction("com.scblhkj.chappie.FINISH_NET_VIDEO_FILE");
                filePathIntent.putExtra("com.scblhkj.chappie.FINISH_VIDEO_FILE_VALUE", filePath);
                filePathIntent.addCategory(Intent.CATEGORY_DEFAULT);
                sendBroadcast(filePathIntent);
                Log.e("info", "发送录制好的视频文件给wifi服务,文件路径是" + filePath);
            }
            //WIFI热点状态下控制视频结束时发送视频
            String filePath = currentViewFile.getPath(); // 刚刚录制完成视频的路径
            // 将视频路径以广播的形式发送出去
            Intent filePathIntent = new Intent();
            filePathIntent.setAction("com.scblhkj.cp.FINISH_VIDEO_FILE");
            filePathIntent.putExtra("com.scblhkj.cp.FINISH_VIDEO_FILE_VALUE", filePath);
            filePathIntent.addCategory(Intent.CATEGORY_DEFAULT);
            sendBroadcast(filePathIntent);
            Log.e("info", "发送录制好的视频文件给wifi服务,文件路径是" + filePath);
        }
    }


    /**
     * 每段视频录制完成之后判断是否是锁存视频
     */
    private void checkIfLockVideo() {
        try{
            // 每一段视频的录制结束时间戳
            tempEndRecordTime = System.currentTimeMillis();
            // 碰撞的时间点是否在刚刚录制完成的这段视频里，如果是重命名该段视频的名称
            if(currentViewFile.getPath()!=null){
                String filePath = currentViewFile.getPath();
                //   if (tempEndRecordTime >= tempCollisionTime && tempStartRecordTime <= tempCollisionTime) {  // 碰撞的时间在刚刚录制完成的这段视频区间里面，锁存这段视频
                //碰撞视频
                if (tempEndRecordTime >= tempCollisionTime &&isCoverState==true) {
                    if (TextUtils.isEmpty(filePath)) {
                        Log.e("info", "要修该的视频文件的路径为空");
                    } else {
                        filePath = FileUtil.reNameFile(filePath);
                        Log.e("info", "保存的碰撞锁存视频>>" + filePath);
                        sendSendVideoBroadCast(filePath);
                    }
                }
                //急减速视频
                if (tempEndRecordTime >= tempCollisionTime &&isLimtedSpeed==true) {
                    if (TextUtils.isEmpty(filePath)) {
                        Log.e("info", "要修该的视频文件的路径为空");
                    } else {
                        filePath = FileUtil.reNameFile(filePath);
                        Log.e("info", "保存的急减速锁存视频>>" + filePath);
                        sendLimitSpeedVideoBroadCast(filePath);
                    }
                }
                // 主动通知Android系统视频进行更新
                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + filePath)));

            }else {
                Log.e("info","锁存视频出现异常");
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e("info","锁存视频出现异常");
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        this.surfaceTexture = surface;
        Log.e("info", "onSurfaceTextureAvailable");
        AI.enableSound(1);
        rgbMat = new Mat(mPreHeight, mPreWidth, CvType.CV_8UC4);
        // 初始化摄像头
        stopCamera();  // 先关闭摄像头释放资源
        isPreviewing = openCamera(currentCamera);
        configCameraParams();
        recordVideo(currentCamera);
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        Log.e("info", "onSurfaceTextureSizeChanged");
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        Log.e("info", "onSurfaceTextureDestroyed");
        stopCamera();
        if (rgbMat != null) {
            rgbMat.release();
            rgbMat = null;
        }
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        mBitmap = textureMain.getBitmap(mPreWidth, mPreHeight);
        if (savePic) {
            if(!SDCardScannerUtil.isSecondSDcardMounted()){
                mHandler.sendEmptyMessage(NO_TF_CARD);
                return;
            }
            soundPool.play(cshotID, 10, 10, 0, 0, 1);  // 播放拍照声音
            savePic = false;
            try {
                Bitmap saveBitmap = textureMain.getBitmap(1920, 1080);
                if (onCShotAccomplish != null) {
                    onCShotAccomplish.onShotFinished(saveBitmap);
                }
                //  File picFile = new File(CommonUtil.buildSavePictruePath());
                if(isUserP==true){
                    picFile=new File(buildArttificialPicPath());//拍照保存在人工拍照的目录下
                    isUserP=false;
                    Log.e("info","拍照保存在人工拍照的目录下"+picFile);
                }else {
                    picFile=new File(buildAutoPicPath());//拍照保存在自动拍照的目录下
                    Log.e("info","拍照保存在自动拍照的目录下"+picFile);
                }

                outputStream = new FileOutputStream(picFile);
                if(isLimtedSpeed){ //急减速
                    Log.e("info","在急减速的状态下压缩并发送图片");
                    isLimtedSpeed = false;
                    saveBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
                    outputStream.flush();
                    outputStream.close();
                    // 将刚刚保存的图片文件的路径以广播的形式发送出去
                    sendLimitSpeedPicBroadCast(picFile.getPath());
                }else {
                    saveBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
                    outputStream.flush();
                    outputStream.close();
                }
                if (isCollision) { //碰撞
                    Log.e("info","在碰撞的状态下压缩并发送图片");
                    isCollision = false;
                    saveBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
                    outputStream.flush();
                    outputStream.close();
                    // 将刚刚保存的图片文件的路径以广播的形式发送出去，在Wifi的情况下写出去
                    sendSendPicBroadCast(picFile.getPath());
                } else {
                    saveBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
                    outputStream.flush();
                    outputStream.close();
                }
                if(isNet){//网络状态下
                    isNet=false;
                    if(cramer.equals("0")){
                        //调用主摄像头拍照，如果当前摄像头为前置摄像头，就要切换至主摄像头
                        if(currentCamera==1){
                            stopRecordVideo();
                            chronometer.stop();
                            CtrlMisc.CtrlMisc_radar_powerctrl_jni(1);
                            if(isChronometer==false){
                                chronometer.setVisibility(View.VISIBLE);
                                isChronometer=true;
                            }
                            isPreviewing = false;
                            chronometer.setBase(SystemClock.elapsedRealtime());
                            chronometer.start();
                            recordVideo(Constant.MAIN_CAMERA);

                        }
                        saveBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
                        outputStream.flush();
                        outputStream.close();
                        // 将刚刚保存的图片文件的路径以广播的形式发送出去，在Wifi的情况下写出去
                        sendSendCMDNetPicBroadCast(picFile.getPath());
                    }else if(cramer.equals("1")){
                        //调用前置摄像头拍照，如果当前摄像头为主摄像头，就要切换至前置摄像头
                        if(isFrontCramer==true){
                            isFrontCramer=false;
                            saveBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
                            outputStream.flush();
                            outputStream.close();
                            // 将刚刚保存的图片文件的路径以广播的形式发送出去，在NET的情况下写出去
                            sendSendCMDNetPicBroadCast(picFile.getPath());
                        }else if(isBackCramer==true){
                            isBackCramer=false;
                            saveBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
                            outputStream.flush();
                            outputStream.close();
                            // 将刚刚保存的图片文件的路径以广播的形式发送出去，在Wifi的情况下写出去
                            sendSendCMDNetPicBroadCast(picFile.getPath());
                        }
                    }

                }else{
                    saveBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
                    outputStream.flush();
                    outputStream.close();
                }
                if (isWifiPic) {//wifi状态下
                    isWifiPic = false;
                    saveBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
                    outputStream.flush();
                    outputStream.close();
                    // 将刚刚保存的图片文件的路径以广播的形式发送出去，在Wifi的情况下写出去
                    sendSendCMDWifiPicBroadCast(picFile.getPath());
                } else {
                    saveBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
                    outputStream.flush();
                    outputStream.close();
                }

                ToastUtil.showToast("拍照完成");
                // 主动通知Android系统视频进行更新
                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + picFile.getPath())));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Utils.bitmapToMat(mBitmap, rgbMat);
        double[] output;
        output = new double[256];
        Bitmap bmp = Bitmap.createBitmap(MeasureScreenUtil.width, MeasureScreenUtil.height, Bitmap.Config.ARGB_8888);

        if (AI != null) {
            // Log.e("info", "AI>>>>>>>>>>" + AI.process(rgbMat, 100, output));
            if (AI.process(rgbMat, 100, output) == 0) {
                Canvas mCanvas = new Canvas(bmp);
                paint.setTextSize((float) 64);
                mCanvas.drawText("授权码错误", 10, MeasureScreenUtil.height - 10, paint);
                // Log.e("info","授权码是0");
            }
            if(isADASRunning){

                AI.Draw(bmp, output);
                iv.setImageBitmap(bmp);
                iv.setVisibility(View.VISIBLE);
                Intent intent=new Intent();
                intent.setAction("com.chappie.ADASSETTING");
                Bundle bundle=new Bundle();
                bundle.putBoolean("isADASRunning",isADASRunning);
                //Log.e("info", "传过去的isADASRunning>>>" + isADASRunning);
                // bundle.putString("adas", "0");
                // Log.e("info", "传过去的adas>>>  0");
                intent.putExtras(bundle);
                sendBroadcast(intent);
            }else if(!isADASRunning) {
                Intent intent=new Intent();
                intent.setAction("com.chappie.ADASSETTING");
                Bundle bundle=new Bundle();
                bundle.putBoolean("isADASRunning",isADASRunning);
                // Log.e("info", "isADASRunning>>>  1"+isADASRunning);
                // Log.e("info", "传过去的isADASRunning>>>" + isADASRunning);
                intent.putExtras(bundle);
                sendBroadcast(intent);
                iv.setVisibility(View.GONE);
            }

        }


    }

    /**
     * 处理视频容量
     */
    private class DoDealVideoTask extends AsyncTask<Void, Void, Integer> {

        public DoDealVideoTask() {
            super();
            Log.e("info", "创建了DoDealVideoTask对象");
        }
        @Override
        protected Integer doInBackground(Void... params) {
            int reslut = 0;
            Log.e("info", "执行删除视频文件操作");
            int dSize = 0;
            File dir = new File("/storage/sdcard1/", "ChappieLauncher/" + "travel");
            if (dir.exists()) {
                if (dir.isDirectory()) {  //判断是否是文件夹(这里的file肯定是文件夹)  /video
                    File[] dirs = dir.listFiles();
                    out:
                    for (File subDir : dirs) {
                        if (subDir.isDirectory()) { //如果是文件夹，就删除里边的50M的文件个文件(这里的文件肯定也是文件夹,每一天都会有一个文件夹)
                            if (subDir.listFiles().length > 0) {  //文件夹下有文件,需要删除文件夹下的文件
                                File[] vfs = subDir.listFiles();
                                iner:
                                for (File f : vfs) {  //每一天视频文件夹下的所有视频文件
                                    if (!f.getName().contains("lock")) {   // 不删除所存文件
                                        dSize += FileUtil.getDirSize(f);
                                        Log.e("info", FileUtil.getDirSize(f) + "kb的文件被删除");
                                        f.delete();
                                        if ((dSize / 1024 / 1024) >= 1024) {
                                            Log.e("info", "1G文件删除成功");
                                            break out;
                                        }
                                    }
                                }
                            } else {  //文件夹为空，删除文件夹增加遍历的效率
                                subDir.delete();
                            }
                        }
                    }
                }
            }
            if ((dSize / 1024 / 1024) >= 1024) {
                reslut = 1;
            } else {
                reslut = 2;
            }
            return reslut;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            switch (integer) {
                case 1: {
                    mHandler.sendEmptyMessage(FILE_DELETE_OVER);
                    break;
                }
                case 2: {
                    mHandler.sendEmptyMessage(FILE_DELETE_FAIL);
                    break;
                }
                default: {
                    break;
                }
            }
            super.onPostExecute(integer);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onCancelled(Integer integer) {
            super.onCancelled(integer);
        }
    }

    /**
     * 倒车后视
     */
    private void rearReview() {
        closeADAS();
        releaseADASInterface();
        stopRecordVideo();
        if(isChronometer==true){
            chronometer.setVisibility(View.GONE);
            isChronometer=false;
        }
        isPreviewing = true;
        openCamera(Constant.BACK_CAMERA);
        Log.e("info", "打开了倒车摄像头");
        Log.e("info","mCamera:"+mCamera);
        try {
            mCamera.setPreviewTexture(surfaceTexture);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mCamera.startPreview();
    }

    /**
     * 切换到副摄像头
     */
    private void secondReview() {
        if(isChronometer==false){
            chronometer.setVisibility(View.VISIBLE);
            isChronometer=true;
        }
        closeADAS();
        stopRecordVideo();
        CtrlMisc.CtrlMisc_radar_powerctrl_jni(1);
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
        isPreviewing = false;
        recordVideo(Constant.SECOND_CAMERA);
        Log.e("info", "启动了副摄像头");
    }

    /**
     * 切换到主摄像头
     */
    private void mainReview() {
        if(isChronometer==false){
            chronometer.setVisibility(View.VISIBLE);
            isChronometer=true;
        }
       //  AudioSwitch.gm_radar_powerctrl(0);//关闭LEO夜视灯
        //CtrlMisc.CtrlMisc_radar_powerctrl_jni(0);
        CtrlMisc.CtrlMisc_radar_powerctrl_jni(0);
        openADAS();
        stopRecordVideo();
        isPreviewing = false;
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
        recordVideo(Constant.MAIN_CAMERA);
        Log.e("info","启动了主摄像头");
    }

    /**
     * 改变录制视频布局大小
     */
    private void changeWindowManagerSize(String action) {
        if (windowManager != null && hddvrView != null && layoutParams != null) {
            switch (action) {
                case Constant.CAR_ACTION_FULL_SIZE: {
                    // 设置悬浮窗口长宽数据
                    //layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                    layoutParams.width = 1200;
                    layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
                    break;
                }
                case Constant.CAR_ACTION_HALF_SIZE: {
                    // 设置悬浮窗口长宽数据
                    layoutParams.width = MeasureScreenUtil.width / 2;
                    layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
                    Log.e("info", "MeasureScreenUtil.width = " + MeasureScreenUtil.width + " MeasureScreenUtil.height =" + MeasureScreenUtil.height);
                    break;
                }
                case Constant.CAR_ACTION_MINI_SIZE: {
                    // 设置悬浮窗口长宽数据
                    layoutParams.width = 1;
                    layoutParams.height = 1;
                    break;
                }
                default: {
                    break;
                }
            }
            windowManager.updateViewLayout(hddvrView, layoutParams);
            hddvrView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        }
    }
    //获取当前的时间戳
    private String getTimeStamp(){
        long time=Long.parseLong(String.valueOf(System.currentTimeMillis()).toString().substring(0,10));//当前10位的时间戳
        //long time=System.currentTimeMillis()/1000;
        String timeStamp=String.valueOf(time);
        return  timeStamp;
    }
    /**
     * 发送指令(获取到急减速的视频并通过网络发送出去)
     */
    private void sendLimitSpeedVideoBroadCast(String filePath) {
        if(sp.getBoolean("isSettingSpeedInfo",isSettingSpeedInfo)==true){
            Log.e("info","isSettingSpeedInfo>>>true");
            isSettingSpeedInfo=sp.getBoolean("isSettingSpeedInfo",isSettingSpeedInfo);
            isSendSpeedVid=sp.getString("isSendSpeedVid","");
            String timestamp = sp.getString("speedTimeStamp","");
            Intent intent = new Intent();
            intent.setAction(Constant.SEND_CAR_LIMITED_VID);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.putExtra(Constant.SEND_CAR_LIMITED_VID_PATH, filePath);
            intent.putExtra("realtimeImei", imei);
            intent.putExtra("realtimeStamp", timestamp);
            intent.putExtra("isSendSpeedVid",isSendSpeedVid);//是否发送视频文件
            intent.putExtra("limteValue",sp.getInt("limteValue",limteValue));//震动值
            intent.putExtra("isSettingSpeedInfo",isSettingSpeedInfo);//是否设置过震动信息
            sendBroadcast(intent);

        }else if(sp.getBoolean("isSettingQuakeInfo", isSettingQuakeInfo) == false) {
            Log.e("info","hddrvisSettingQuakeInfo>>>false");
            isSettingSpeedInfo=sp.getBoolean("isSettingSpeedInfo", isSettingSpeedInfo);
            String timestamp = sp.getString("speedTimeStamp","");
            Intent intent = new Intent();
            intent.setAction(Constant.SEND_CAR_LIMITED_VID);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.putExtra(Constant.SEND_CAR_LIMITED_VID_PATH, filePath);
            intent.putExtra("realtimeImei", imei);
            intent.putExtra("realtimeStamp", timestamp);
            intent.putExtra("limteValue",sp.getInt("limteValue",limteValue));//震动值
            intent.putExtra("isSettingSpeedInfo",isSettingSpeedInfo);//是否设置过震动信息
            sendBroadcast(intent);
        }
        Log.e("info", "发送了急减速事件视频路径的广播，路径路径为" + filePath);
    }
    /****
     * 急减速照片信息
     * @param filePath
     */
    private void sendLimitSpeedPicBroadCast(String filePath){
        if(sp.getBoolean("isSettingSpeedInfo",isSettingSpeedInfo)==true){
            Log.e("info","急减速信息被设置过");
            isSettingSpeedInfo=sp.getBoolean("isSettingSpeedInfo",isSettingSpeedInfo);
            isSendSpeedPic=sp.getString("isSendSpeedPic","");
            String timestamp = sp.getString("speedTimeStamp","");
            Intent intent = new Intent();
            intent.setAction(Constant.SEND_CAR_LIMITED_PIC);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.putExtra(Constant.SEND_CAR_LIMITED_PIC_PATH, filePath);//图片路径
            intent.putExtra("realtimeImei", sp.getString("deviceImei",""));
            intent.putExtra("realtimeStamp",timestamp);
            intent.putExtra("isSendSpeedPic",sp.getString("isSendSpeedPic",""));//是否发送图片文件
            intent.putExtra("limteValue",sp.getInt("limteValue",limteValue));//震动值
            intent.putExtra("isSettingSpeedInfo",isSettingSpeedInfo);
            sendBroadcast(intent);
        }else if(sp.getBoolean("isSettingSpeedInfo",isSettingSpeedInfo)==false){
            Log.e("info","急减速信息没有被设置过");
            isSettingSpeedInfo=sp.getBoolean("isSettingSpeedInfo", isSettingSpeedInfo);
            String timestamp = sp.getString("speedTimeStamp","");
            Intent intent = new Intent();
            intent.setAction(Constant.SEND_CAR_LIMITED_PIC);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.putExtra(Constant.SEND_CAR_LIMITED_PIC_PATH, filePath);//图片路径
            intent.putExtra("realtimeImei", sp.getString("deviceImei",""));
            intent.putExtra("realtimeStamp",timestamp);
            intent.putExtra("limteValue",sp.getInt("limteValue",limteValue));//震动值
            intent.putExtra("isSettingSpeedInfo",isSettingSpeedInfo);
            sendBroadcast(intent);
        }
        Log.e("info", "发送了急减速事件图片路径的广播，路径路径为" + filePath);
    }
    /**
     * 发送图片指令(获取到碰撞时刻的照片并通过网络发送出去)
     *  //quakeValue 震动参数  modifyQuakeCrame 震动拍照摄像头  isSendQuakePic 是否发送震动图片文件
     //isSendQuakeVid 是否发送震动视频文件 modifyQuakeVCrame 震动摄像摄像头
     */
    private void sendSendPicBroadCast(String filePath) {
        //碰撞时刻信息被设置过
        if(sp.getBoolean("isSettingQuakeInfo",isSettingQuakeInfo)==true){
            Log.e("info","碰撞信息被设置过");
            isSettingQuakeInfo=sp.getBoolean("isSettingQuakeInfo",isSettingQuakeInfo);
            isSendQuakePic=sp.getString("isSendQuakePic","");
            String timestamp = collsionTimeStamp;
            Intent intent = new Intent();
            intent.setAction(Constant.SEND_CAR_COLLISION_PIC);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.putExtra(Constant.SEND_CAR_COLLISION_PIC_PATH, filePath);//图片路径
            intent.putExtra("realtimeImei", imei);
            intent.putExtra("realtimeStamp",timestamp);
            intent.putExtra("isSendQuakePic",isSendQuakePic);//是否发送图片文件
            intent.putExtra("speedQuakeValue",speedQuakeValue);//震动值
            intent.putExtra("isSettingQuakeInfo",isSettingQuakeInfo);
            sendBroadcast(intent);

        }else if(sp.getBoolean("isSettingQuakeInfo",isSettingQuakeInfo)==false){
            //碰撞时刻信息没有被设置过
            Log.e("info","碰撞时刻信息没有被设置过");
            String timestamp = collsionTimeStamp;
            isSettingQuakeInfo=sp.getBoolean("isSettingQuakeInfo",isSettingQuakeInfo);
            Intent intent = new Intent();
            intent.setAction(Constant.SEND_CAR_COLLISION_PIC);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.putExtra(Constant.SEND_CAR_COLLISION_PIC_PATH, filePath);
            intent.putExtra("realtimeImei",imei);
            intent.putExtra("realtimeStamp",timestamp);
            intent.putExtra("isSettingQuakeInfo",isSettingQuakeInfo);
            intent.putExtra("speedQuakeValue",speedQuakeValue);
            sendBroadcast(intent);
        }

        Log.e("info", "发送了震动事件图片路径的广播,图片路径为" + filePath);
    }
    /**
     * 发送指令(获取到碰撞时刻的视频并通过网络发送出去)
     */
    private void sendSendVideoBroadCast(String filePath) {
        if(sp.getBoolean("isSettingQuakeInfo",isSettingQuakeInfo)==true){
            Log.e("info","hddrvisSettingQuakeInfo>>>true");
            isSettingQuakeInfo=sp.getBoolean("isSettingQuakeInfo",isSettingQuakeInfo);
            isSendQuakeVid=sp.getString("isSendQuakeVid","");
            String timestamp=collsionTimeStamp;
            Intent intent = new Intent();
            intent.setAction(Constant.SEND_CAR_COLLISION_VIDEO);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.putExtra(Constant.SEND_CAR_COLLISION_VIDEO_PATH, filePath);
            intent.putExtra("realtimeImei", imei);
            intent.putExtra("realtimeStamp", timestamp);
            intent.putExtra("isSendQuakeVid",isSendQuakeVid);//是否发送视频文件
            intent.putExtra("speedQuakeValue",speedQuakeValue);//震动值
            intent.putExtra("isSettingQuakeInfo",isSettingQuakeInfo);//是否设置过震动信息
            sendBroadcast(intent);

        }else if(sp.getBoolean("isSettingQuakeInfo", isSettingQuakeInfo) == false) {
            Log.e("info","hddrvisSettingQuakeInfo>>>false");
            isSettingQuakeInfo=sp.getBoolean("isSettingQuakeInfo",isSettingQuakeInfo);
            String timestamp=collsionTimeStamp;
            Intent intent = new Intent();
            intent.setAction(Constant.SEND_CAR_COLLISION_VIDEO);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.putExtra(Constant.SEND_CAR_COLLISION_VIDEO_PATH, filePath);
            intent.putExtra("realtimeImei",imei);
            intent.putExtra("realtimeStamp",timestamp);
            intent.putExtra("speedQuakeValue",speedQuakeValue);//震动值
            intent.putExtra("isSettingQuakeInfo",isSettingQuakeInfo);//是否设置过震动信息
            sendBroadcast(intent);
        }
        Log.e("info", "发送了震动事件视频路径的广播，路径路径为" + filePath);
    }
    /**
     * 发送指令(获取到Wifi指令后拍摄照片通过网络发送出去)
     */
    private void sendSendCMDWifiPicBroadCast(String filePath) {
        Intent intent = new Intent();
        intent.setAction(Constant.SEND_CAR_CMD_WIFI_PIC);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.putExtra(Constant.SEND_CAR_CMD_WIFI_PIC_PATH, filePath);
        sendBroadcast(intent);
        Log.e("info", "发送了wifi状态下发送事件图片路径的广播，图片路径为" + filePath);
    }
    /**
     * 发送指令(获取到网络指令后拍摄照片通过网络发送出去)
     */
    private void sendSendCMDNetPicBroadCast(String filePath) {
        Intent intent = new Intent();
        intent.setAction(Constant.SEND_CAR_CMD_NET_PIC);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.putExtra(Constant.SEND_CAR_CMD_NET_PIC_PATH, filePath);
        intent.putExtra("imei", realTimeImei);
        intent.putExtra("timestamp",realTimeStamp);
        intent.putExtra("isSendFile",isSendFile );
        sendBroadcast(intent);
        Log.e("info", "发送了网络状态下拍照的照片路径的广播，图片路径为" + filePath);
    }

    /****************************
     * 视频实时查看
     ******************************/
    private final static String CLASS_LABEL = "RecordActivity";
    private final static String LOG_TAG = CLASS_LABEL;
    /* The number of seconds in the continuous record loop (or 0 to disable loop). */
    final int RECORD_LENGTH = 0;
    /* layout setting */
    private final int bg_screen_bx = 232;
    private final int bg_screen_by = 128;
    private final int bg_screen_width = 700;
    private final int bg_screen_height = 500;
    private final int bg_width = 1123;
    private final int bg_height = 715;
    private final int live_width = 640;
    private final int live_height = 480;
    long startTime = 0;
    boolean recording = false;
    volatile boolean runAudioThread = true;
    Frame[] images;
    long[] timestamps;
    ShortBuffer[] samples;
    int imagesIndex, samplesIndex;
    private String ffmpeg_link = "";
    private FFmpegFrameRecorder recorder;
    private boolean isPreviewOn = false;
    private int sampleAudioRateInHz = 44100;
    private int imageWidth = 320;
    private int imageHeight = 240;
    private int frameRate = 24;
    /* audio data getting thread */
    private AudioRecord audioRecord;
    private AudioRecordRunnable audioRecordRunnable;
    private Thread audioThread;
    /* video data getting thread */
    private Camera cameraDevice;
    private CameraView cameraView;
    private Frame yuvImage = null;
    private int screenWidth, screenHeight;

    private void initRecorder() {
        Log.w("info", "init recorder");
        if (RECORD_LENGTH > 0) {
            imagesIndex = 0;
            images = new Frame[RECORD_LENGTH * frameRate];
            timestamps = new long[images.length];
            for (int i = 0; i < images.length; i++) {
                images[i] = new Frame(imageWidth, imageHeight, Frame.DEPTH_UBYTE, 2);
                timestamps[i] = -1;
            }
        } else if (yuvImage == null) {
            yuvImage = new Frame(imageWidth, imageHeight, Frame.DEPTH_UBYTE, 2);
            Log.i("info", "create yuvImage");
        }

        Log.i(LOG_TAG, "ffmpeg_url: " + ffmpeg_link);
        recorder = new FFmpegFrameRecorder(ffmpeg_link, imageWidth, imageHeight, 1);
        recorder.setVideoCodec(28);
        recorder.setFormat("flv");
        recorder.setSampleRate(sampleAudioRateInHz);
        // Set in the surface changed method
        recorder.setFrameRate(frameRate);

        Log.i(LOG_TAG, "recorder initialize success");

        audioRecordRunnable = new AudioRecordRunnable();
        audioThread = new Thread(audioRecordRunnable);
        runAudioThread = true;
    }

    class AudioRecordRunnable implements Runnable {

        @Override
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);

            // Audio
            int bufferSize;
            ShortBuffer audioData;
            int bufferReadResult;
            bufferSize = AudioRecord.getMinBufferSize(sampleAudioRateInHz, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
            audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, sampleAudioRateInHz, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize);
            if (RECORD_LENGTH > 0) {
                samplesIndex = 0;
                samples = new ShortBuffer[RECORD_LENGTH * sampleAudioRateInHz * 2 / bufferSize + 1];
                for (int i = 0; i < samples.length; i++) {
                    samples[i] = ShortBuffer.allocate(bufferSize);
                }
            } else {
                audioData = ShortBuffer.allocate(bufferSize);
            }
            Log.d("info", "audioRecord.startRecording()");
            audioRecord.startRecording();
            /* ffmpeg_audio encoding loop */
            while (runAudioThread) {
                if (RECORD_LENGTH > 0) {
                    audioData = samples[samplesIndex++ % samples.length];
                    audioData.position(0).limit(0);
                }
                //Log.v(LOG_"info","recording? " + recording);
                bufferReadResult = audioRecord.read(audioData.array(), 0, audioData.capacity());
                audioData.limit(bufferReadResult);
                if (bufferReadResult > 0) {
                    Log.v(LOG_TAG, "bufferReadResult: " + bufferReadResult);
                    // If "recording" isn't true when start this thread, it never get's set according to this if statement...!!!
                    // Why?  Good question...
                    if (recording) {
                        if (RECORD_LENGTH <= 0) try {
                            recorder.recordSamples(audioData);
                            //Log.v(LOG_"info","recording " + 1024*i + " to " + 1024*i+1024);
                        } catch (FFmpegFrameRecorder.Exception e) {
                            Log.v(LOG_TAG, e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }
            }
            Log.e("info", "AudioThread Finished, release audioRecord");
            /* encoding finish, release recorder */
            if (audioRecord != null) {
                audioRecord.stop();
                audioRecord.release();
                audioRecord = null;
                Log.e("info", "audioRecord released");
            }
        }
    }

    //---------------------------------------------
    // 自定义SurfaceView用来直播预览
    //---------------------------------------------
    class CameraView extends SurfaceView implements SurfaceHolder.Callback, Camera.PreviewCallback {

        private SurfaceHolder mHolder;
        private Camera mCamera;

        public CameraView(Context context, Camera camera) {
            super(context);
            Log.w("camera", "camera view");
            mCamera = camera;
            mHolder = getHolder();
            mHolder.addCallback(CameraView.this);
            mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
            mCamera.setPreviewCallback(CameraView.this);
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                stopPreview();
                mCamera.setPreviewDisplay(holder);
            } catch (IOException exception) {
                mCamera.release();
                mCamera = null;
            }
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            stopPreview();

            Camera.Parameters camParams = mCamera.getParameters();
            List<Camera.Size> sizes = camParams.getSupportedPreviewSizes();
            // Sort the list in ascending order
            Collections.sort(sizes, new Comparator<Camera.Size>() {

                public int compare(final Camera.Size a, final Camera.Size b) {
                    return a.width * a.height - b.width * b.height;
                }
            });

            // Pick the first preview size that is equal or bigger, or pick the last (biggest) option if we cannot
            // reach the initial settings of imageWidth/imageHeight.
            for (int i = 0; i < sizes.size(); i++) {
                if ((sizes.get(i).width >= imageWidth && sizes.get(i).height >= imageHeight) || i == sizes.size() - 1) {
                    imageWidth = sizes.get(i).width;
                    imageHeight = sizes.get(i).height;
                    Log.v(LOG_TAG, "Changed to supported resolution: " + imageWidth + "x" + imageHeight);
                    break;
                }
            }
            camParams.setPreviewSize(imageWidth, imageHeight);

            Log.v(LOG_TAG, "Setting imageWidth: " + imageWidth + " imageHeight: " + imageHeight + " frameRate: " + frameRate);

            camParams.setPreviewFrameRate(frameRate);
            Log.v(LOG_TAG, "Preview Framerate: " + camParams.getPreviewFrameRate());

            mCamera.setParameters(camParams);

            // Set the holder (which might have changed) again
            try {
                mCamera.setPreviewDisplay(holder);
                mCamera.setPreviewCallback(CameraView.this);
                startPreview();
            } catch (Exception e) {
                Log.e(LOG_TAG, "Could not set preview display in surfaceChanged");
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            try {
                mHolder.addCallback(null);
                mCamera.setPreviewCallback(null);
            } catch (RuntimeException e) {
                // The camera has probably just been released, ignore.
            }
        }

        public void startPreview() {
            if (!isPreviewOn && mCamera != null) {
                isPreviewOn = true;
                mCamera.startPreview();
            }
        }

        public void stopPreview() {
            if (isPreviewOn && mCamera != null) {
                isPreviewOn = false;
                mCamera.stopPreview();
            }
        }

        @Override
        public void onPreviewFrame(byte[] data, Camera camera) {
            if (audioRecord == null || audioRecord.getRecordingState() != AudioRecord.RECORDSTATE_RECORDING) {
                startTime = System.currentTimeMillis();
                return;
            }
            if (RECORD_LENGTH > 0) {
                int i = imagesIndex++ % images.length;
                yuvImage = images[i];
                timestamps[i] = 1000 * (System.currentTimeMillis() - startTime);
            }
            /* get video data */
            if (yuvImage != null && recording) {
                ((ByteBuffer) yuvImage.image[0].position(0)).put(data);

                if (RECORD_LENGTH <= 0) try {
                    Log.v(LOG_TAG, "Writing Frame");
                    long t = 1000 * (System.currentTimeMillis() - startTime);
                    if (t > recorder.getTimestamp()) {
                        recorder.setTimestamp(t);
                    }
                    recorder.record(yuvImage);
                } catch (FFmpegFrameRecorder.Exception e) {
                    Log.v(LOG_TAG, e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 开始实时监控
     */
    public void startRealRecording() {
        if(TextUtils.isEmpty(sp.getString("ffmpeg_link",""))){
            ffmpeg_link = "rtmp://video-center.alivecdn.com/app/chappie?vhost=vod.blhcp.com";
        }
        ffmpeg_link = sp.getString("ffmpeg_link",ffmpeg_link);
        Log.e("info","推送实时视频的ffmpeg_link："+ffmpeg_link);
        initRecorder();
        try {
            recorder.start();
            startTime = System.currentTimeMillis();
            recording = true;
            audioThread.start();
        } catch (FFmpegFrameRecorder.Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 停止实时监控
     */
    public void stopRecording() {
        runAudioThread = false;
        try {
            audioThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        audioRecordRunnable = null;
        audioThread = null;

        if (recorder != null && recording) {
            if (RECORD_LENGTH > 0) {
                Log.v(LOG_TAG, "Writing frames");
                try {
                    int firstIndex = imagesIndex % samples.length;
                    int lastIndex = (imagesIndex - 1) % images.length;
                    if (imagesIndex <= images.length) {
                        firstIndex = 0;
                        lastIndex = imagesIndex - 1;
                    }
                    if ((startTime = timestamps[lastIndex] - RECORD_LENGTH * 1000000L) < 0) {
                        startTime = 0;
                    }
                    if (lastIndex < firstIndex) {
                        lastIndex += images.length;
                    }
                    for (int i = firstIndex; i <= lastIndex; i++) {
                        long t = timestamps[i % timestamps.length] - startTime;
                        if (t >= 0) {
                            if (t > recorder.getTimestamp()) {
                                recorder.setTimestamp(t);
                            }
                            recorder.record(images[i % images.length]);
                        }
                    }

                    firstIndex = samplesIndex % samples.length;
                    lastIndex = (samplesIndex - 1) % samples.length;
                    if (samplesIndex <= samples.length) {
                        firstIndex = 0;
                        lastIndex = samplesIndex - 1;
                    }
                    if (lastIndex < firstIndex) {
                        lastIndex += samples.length;
                    }
                    for (int i = firstIndex; i <= lastIndex; i++) {
                        recorder.recordSamples(samples[i % samples.length]);
                    }
                } catch (FFmpegFrameRecorder.Exception e) {
                    Log.v(LOG_TAG, e.getMessage());
                    e.printStackTrace();
                }
            }
            recording = false;
            Log.v(LOG_TAG, "Finishing recording, calling stop and release on recorder");
            try {
                recorder.stop();
                recorder.release();
            } catch (FFmpegFrameRecorder.Exception e) {
                e.printStackTrace();
            }
            recorder = null;
        }
        recording = false;
        if (cameraView != null) {
            cameraView.stopPreview();
        }
        if (cameraDevice != null) {
            cameraDevice.stopPreview();
            cameraDevice.release();
            cameraDevice = null;
        }

    }


}

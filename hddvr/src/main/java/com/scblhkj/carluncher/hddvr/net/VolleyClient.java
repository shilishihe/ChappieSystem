package com.scblhkj.carluncher.hddvr.net;

import android.content.Context;
import android.util.Log;

import java.util.Map;

/**
 * Created by blh on 2016-08-26.
 */
public class VolleyClient implements  VolleyClientInterface {
    private static VolleyClient volleyClient;
    private static VolleyHelper volleyHelper;
    /**
     * 单例模式
     *
     * @return
     */
    public static VolleyClient getInstance(Context context) {
        getInstance(context, false);
        return volleyClient;
    }

    /**
     * @param context
     * @param show
     * @return
     */
    public static VolleyClient getInstance(Context context, boolean show) {
        if (volleyClient == null) {
            synchronized (VolleyClient.class) {
                if (volleyClient == null && volleyHelper == null) {
                    volleyClient = new VolleyClient();
                    volleyHelper = VolleyHelper.getInstance(context, show);
                }
            }
        }
        return volleyClient;
    }

    public VolleyClient() {
    }

    @Override
    public void sendCarCollsionToService(Map<String, String> params, final VolleyResponseCallback callback) {
        Log.e("info","sendCarCollsionToService》》"+params);
        volleyHelper.doGet(params, new VolleyResponseCallback() {
            @Override
            public void getString(String string) {
                callback.getString(string);
                Log.i("info", "string:" + string);
            }
        });
    }
}

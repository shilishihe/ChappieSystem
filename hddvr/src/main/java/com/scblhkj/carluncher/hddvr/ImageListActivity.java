package com.scblhkj.carluncher.hddvr;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.scblhkj.carluncher.hddvr.common.Constant;
import com.scblhkj.carluncher.hddvr.domain.HddvrSettingBean;
import com.scblhkj.carluncher.hddvr.utils.ACache;
import com.scblhkj.carluncher.hddvr.utils.TitleBuilder;



/**
 * 行车记录仪设置界面
 */
public class ImageListActivity extends Activity  {

    private static final String TAG = "ImageListActivity";
    private ListView lv_imagelist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_imagelist);
        initView();
        initData();
    }

    private void initView() {
        lv_imagelist=(ListView)findViewById(R.id.lv_imagelist);
    }

    private void initData() {
        String selection= MediaStore.Images.Media.DATA+" like %?";
        //设定查询目录

    }


}

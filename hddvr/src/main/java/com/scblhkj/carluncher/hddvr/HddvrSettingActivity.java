package com.scblhkj.carluncher.hddvr;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.scblhkj.carluncher.hddvr.common.Constant;
import com.scblhkj.carluncher.hddvr.domain.HddvrSettingBean;
import com.scblhkj.carluncher.hddvr.utils.ACache;
import com.scblhkj.carluncher.hddvr.utils.TitleBuilder;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 行车记录仪设置界面
 */
public class HddvrSettingActivity extends Activity implements CompoundButton.OnCheckedChangeListener, RadioGroup.OnCheckedChangeListener, View.OnClickListener {

    private static final String TAG = "HddvrSettingActivity";
    @Bind(R.id.cbRecordVoice)
    CheckBox cbRecordVoice;
    @Bind(R.id.cbADAS)
    CheckBox cbADAS;
    @Bind(R.id.rbHeighMode)
    RadioButton rbHeighMode;
    @Bind(R.id.rbCommonMode)
    RadioButton rbCommonMode;
    @Bind(R.id.rgResolution)
    RadioGroup rgResolution;
    @Bind(R.id.rbHeigh)
    RadioButton rbHeigh;
    @Bind(R.id.rbMiddle)
    RadioButton rbMiddle;
    @Bind(R.id.rbLow)
    RadioButton rbLow;
    @Bind(R.id.rbClose)
    RadioButton rbClose;
    @Bind(R.id.rgVM)
    RadioGroup rgVM;
    @Bind(R.id.btDS)
    Button btDS;
    @Bind(R.id.rbYi)
    RadioButton rbYi;
    @Bind(R.id.rbSan)
    RadioButton rbSan;
    @Bind(R.id.rbWu)
    RadioButton rbWu;
    @Bind(R.id.rgLzsh)
    RadioGroup rgLzsh;
    private View view;

    private HddvrSettingBean hsBean;
    private ACache aCache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aCache = ACache.get(this);
        view = View.inflate(this, R.layout.activity_hddvr_setting, null);
        /***
         *   new TitleBuilder(view)
         .setTitleText("行车记录仪")
         .setLeftImage(R.drawable.ic_title_back_left_bt)
         .setLeftOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        finish();
        }
        })
         .build();
         */


        setContentView(view);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        initData();
        cbRecordVoice.setOnCheckedChangeListener(this);
        cbADAS.setOnCheckedChangeListener(this);
        rgResolution.setOnCheckedChangeListener(this);
        rgVM.setOnCheckedChangeListener(this);
        rgLzsh.setOnCheckedChangeListener(this);
        btDS.setOnClickListener(this);
    }

    /**
     * 初始化数据
     */
    private void initData() {
        hsBean = (HddvrSettingBean) aCache.getAsObject("hddvr_set");
        if (hsBean == null) {
            hsBean = new HddvrSettingBean(true, true, "高清模式", "高", 180);
            Log.e(TAG, "hsBean 为空");
        } else {
            Log.e(TAG, "hsBean 不为空" + hsBean.toString());
        }
        cbRecordVoice.setChecked(hsBean.isbRecord());
        cbADAS.setChecked(hsBean.isbAdas());
        switch (hsBean.getsResolution()) {
            case "高清模式": {
                rbHeighMode.setChecked(true);
                break;
            }
            case "标清模式": {
                rbCommonMode.setChecked(true);
                break;
            }
            default: {
                break;
            }
        }
        switch (hsBean.getsVM()) {
            case "高": {
                rbHeigh.setChecked(true);
                break;
            }
            case "中": {
                rbMiddle.setChecked(true);
                break;
            }
            case "低": {
                rbLow.setChecked(true);
                break;
            }
            case "关": {
                rbClose.setChecked(true);
                break;
            }
            default: {
                break;
            }
        }
        switch (hsBean.getsTime()) {
            case 60: {
                rbYi.setChecked(true);
                break;
            }
            case 180: {
                rbSan.setChecked(true);
                break;
            }
            case 300: {
                rbWu.setChecked(true);
                break;
            }
            default: {
                break;
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cbRecordVoice: {   // 录音
                hsBean.setbRecord(isChecked);
                Log.e(TAG, "录音开关" + hsBean.isbRecord());
                break;
            }
            case R.id.cbADAS: {   // ADAS
                hsBean.setbAdas(isChecked);
                if (isChecked) {
                    Intent intent = new Intent();
                    intent.setAction(Constant.ACTION_OPEN_ADAS);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    sendBroadcast(intent);
                } else {
                    Intent intent = new Intent();
                    intent.setAction(Constant.ACTION_CLOSE_ADAS);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    sendBroadcast(intent);
                }
                break;
            }
            default: {
                break;
            }
        }
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (group.getId()) {
            case R.id.rgResolution: {   // 录像分辨率
                switch (checkedId) {
                    case R.id.rbHeighMode: {
                        hsBean.setsResolution("高清模式");
                        break;
                    }
                    case R.id.rbCommonMode: {
                        hsBean.setsResolution("标清模式");
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
            }
            case R.id.rgVM: {   // 震动级别
                switch (checkedId) {
                    case R.id.rbHeigh: {
                        hsBean.setsVM("高");
                        break;
                    }
                    case R.id.rbMiddle: {
                        hsBean.setsVM("中");
                        break;
                    }
                    case R.id.rbLow: {
                        hsBean.setsVM("低");
                        break;
                    }
                    case R.id.rbClose: {
                        hsBean.setsVM("关");
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
            }
            case R.id.rgLzsh: {
                switch (checkedId) {
                    case R.id.rbYi: {
                        hsBean.setsTime(60);
                        break;
                    }
                    case R.id.rbSan: {
                        hsBean.setsTime(180);
                        break;
                    }
                    case R.id.rbWu: {
                        hsBean.setsTime(300);
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
            }
            default: {
                break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btDS: {
                hsBean = new HddvrSettingBean(true, true, "高清模式", "高", 60);
                aCache.put("hddvr_set", hsBean);
                initData();
                break;
            }
            default: {
                break;
            }
        }
    }

    @Override
    protected void onPause() {
        aCache.put("hddvr_set", hsBean);  // 关闭页面的时候保存数据
        Log.e(TAG, "hsBean = " + hsBean.toString() + "数据保存成功");
        super.onPause();
    }
}

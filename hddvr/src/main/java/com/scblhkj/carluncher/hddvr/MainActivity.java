package com.scblhkj.carluncher.hddvr;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.scblhkj.carluncher.hddvr.common.Constant;
import com.scblhkj.carluncher.hddvr.service.CatchKeyCodeUtil;
import com.scblhkj.carluncher.hddvr.service.HddvrService;
import com.scblhkj.carluncher.hddvr.utils.StartAppUtil;
import com.scblhkj.carluncher.hddvr.utils.TitleBuilder;
import com.scblhkj.carluncher.hddvr.utils.ToastUtil;

import java.util.List;
import java.util.logging.LogRecord;

/**
 * 2016-03-24明天做测试指令控制拍照之类的操作（Wifi指令）
 */
public class MainActivity extends Activity implements HddvrService.OnHddvrServiceCreate, HddvrService.OnCShotAccomplish {

    private static final String TAG = "hddvr";
    private View view;
   // private Button bt_fm;
    //private ImageView iv_current;
    private CatchKeyCodeUtil catchKeyCodeUtil;
    private Camera mCamera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 设置没有标题栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // 设置Activity为全屏模式
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // 设置为横屏模式
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().setBackgroundDrawable(null);
        HddvrService.setOnHddvrServiceCreate(this);
        HddvrService.setOnCShotAccomplish(this);
        view = View.inflate(this, R.layout.activity_main, null);
        /****
         *  new TitleBuilder(view)
         .setTitleText("行车记录仪")
         .setRightImage(R.drawable.ic_title_set_right_bt)
         .setRightOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        // 跳转到行车记录仪设置界面
        Intent intent = new Intent(MainActivity.this, HddvrSettingActivity.class);
        startActivity(intent);
        }
        }).build();
         */

        setContentView(view);
        catchKeyCodeUtil = new CatchKeyCodeUtil(this);
       // iv_current = (ImageView) view.findViewById(R.id.iv_current);
       // bt_fm = (Button) view.findViewById(R.id.bt_fm);
        /***
         *    bt_fm.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.mediatek.filemanager");   // MTK自带文件管理器
        }
        });
         */

    }

    /***
     * @Override
    public void onBackPressed() {
    super.onBackPressed();
    }

     */

    @Override
    protected void onResume() {
        Intent intent = new Intent();
        //intent.setAction(Constant.CAR_ACTION_HALF_SIZE);
       intent.setAction(Constant.CAR_ACTION_FULL_SIZE);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intent);
        Log.e(TAG, "onResume放大窗口广播发送");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Intent intent = new Intent();
        intent.setAction(Constant.CAR_ACTION_MINI_SIZE);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intent);
        Log.e(TAG, "onPause发送广播");
        super.onPause();
    }

    @Override
    public void onServiceCreate() {


        Intent intent = new Intent();
      // intent.setAction(Constant.CAR_ACTION_HALF_SIZE);
        intent.setAction(Constant.CAR_ACTION_FULL_SIZE);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intent);
        Log.e(TAG, "onResume放大窗口广播发送");
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        HddvrService.setOnHddvrServiceCreate(null);
        HddvrService.setOnCShotAccomplish(null);
        super.onDestroy();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onShotFinished(Bitmap bmp) {
        if (bmp != null) {
//            iv_current.setBackground(new BitmapDrawable(bmp));
        }
    }

    /***
     *
     * @param keyCode
     * @param event
     * @return
     *
     *
     *  @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    ToastUtil.showToast(catchKeyCodeUtil.parseKeyCode(keyCode));
    return super.onKeyDown(keyCode, event);
    }
     */

}

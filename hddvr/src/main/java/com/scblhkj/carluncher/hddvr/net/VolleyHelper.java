package com.scblhkj.carluncher.hddvr.net;

import android.content.Context;
import android.provider.SyncStateContract;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by blh on 2016-08-26.
 */
public class VolleyHelper {
    /**
     * 请求队列
     */
    public  static RequestQueue mRequestQueue;
    private static String HTTP = "http";
    /**
     * 单例对象
     */
    private static VolleyHelper volleyHelper;
    JSONObject jsonObject= null;
    /**
     * 静态上下文对象
     */
    private static Context context;
    private static String HOST = "123.57.56.254:10017/server/keepAlive.htm";
    /**
     * 是否显示通用的加载对话框
     */
    private static boolean show;
    private String str;

    private VolleyHelper() {
    }

    /**
     * 放回请求队列对象
     *
     * @return
     */
    public static RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    /**
     * 单例模式
     *
     * @return
     */
    public static VolleyHelper getInstance(Context c, boolean s) {
        show = s;
        context = c;
        if (volleyHelper == null) {
            synchronized (VolleyClient.class) {
                if (volleyHelper == null && mRequestQueue == null) {
                    mRequestQueue = Volley.newRequestQueue(c);
                    volleyHelper = new VolleyHelper();
                }
                mRequestQueue.start();
            }
        }
        return volleyHelper;
    }

    public static VolleyHelper getInstance(Context c) {
        getInstance(c, false);
        return volleyHelper;
    }

    private static final String TAG = "VolleyHelper";

    /**
     * Volley Get 请求
     *
     *
     * @param callback
     */
    public void doGet(Map<String, String> params, final VolleyResponseCallback callback) {
        Log.e("info","GET请求的参数>>"+params.toString());
        Log.e("info","GET请求的url>>"+getUrl(params));
        StringRequest stringRequest = new StringRequest(Request.Method.GET, getUrl(params),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String jsonString) {
                        callback.getString(jsonString);
                        // Log.i("info", "String:>>>>>>" + jsonString.toString());
                        try {
                            jsonObject = new JSONObject(jsonString);
                            Log.i("info", "jsonstring:>>>>>>" + jsonObject.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.e("Error: ", error.getMessage());
                    }

                }
        );
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(stringRequest);
    }


    private static String getURI() {
        return HTTP + "://%s/?&%s";
    }

    private String getUrl(Map<String, String> params) {
        String url ="http://123.57.56.254:10017/server/keepAlive.htm";
        // 添加url参数
        if (params != null) {
            Iterator<String> it = params.keySet().iterator();
            StringBuffer sb = null;
            while (it.hasNext()) {
                String key = it.next();
                String value = params.get(key);
                if (sb == null) {
                    sb = new StringBuffer();
                    sb.append("?");
                } else {
                    sb.append("&");
                }
                sb.append(key);
                sb.append("=");
                sb.append(value);
            }
            url += sb.toString();
        }
        Log.e("GET请求的地址url", url);
        return url;

    }
    private String formatSUrl(String url, Map<String, String> map) {
        if (map == null || map.isEmpty()) {
            return url;
        } else {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                if(entry.getValue()!=null){
                    url = url + "&" +entry.getKey()+"="+ entry.getValue();
                }

            }
            return url;
        }
    }
    /**
     * Volley Post 请求
     *
     * @param url
     * @param params
     * @param callback
     */
    public void doPost(String url, final Map<String, String> params, final VolleyResponseCallback callback) {
        Log.e("POST请求的地址url", formatUrl(url, params));

        StringRequest stringRequest = new StringRequest(Request.Method.POST,formatUrl(url, params),
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String jsonString) {
                        callback.getString(jsonString);

                        try {
                            jsonObject = new JSONObject(jsonString);
                            Log.i("info", "返回的jsonstring:>>>>>>" + jsonObject.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("TAG", error.getMessage(), error);
                    }

                }
        );
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(stringRequest);
    }

    /**
     * Volley Post 请求
     *
     * @param url
     * @param params
     * @param callback
     */
    public void doRequestPost(String url, final Map<String, String> params, final VolleyResponseCallback callback) {
        Log.e("POST请求的地址url", formatUrl(url, params));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, formatUrl(url, params),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String jsonString) {
                        callback.getString(jsonString);
                        Log.i("info", "jsonstring:" + jsonString);
                        try {
                            jsonObject = new JSONObject(jsonString);
                            Log.i("info", "jsonstring:>>>>>>" + jsonObject.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.e("Error: ", error.getMessage());
                    }

                }
        );
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(stringRequest);
    }

    /**
     * Volley put 请求
     *
     * @param url
     * @param params
     * @param callback
     */
    public void doPut(String url, Map<String, String> params, final VolleyResponseCallback callback) {
        /***
         * http://ip/app/ResetPassword/rest/user_login_name/用户名/user_pass /新	密码/user_pass 2/重复新密码verify_code/验证码
         */
        Log.i("info", "PUT请求的地址url:" + formatUrl(url, params));
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, formatUrl(url, params),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String jsonString) {
                        callback.getString(jsonString);
                        try {
                            jsonObject = new JSONObject(jsonString);
                            Log.i("info", "jsonstring:>>>>>>" + jsonObject.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.e("Error: ", error.getMessage());
                    }

                }
        );
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(stringRequest);
    }
    /**
     * Volley put 请求
     *
     * @param url
     * @param params
     * @param callback
     */
    public void doPUT1(String url, Map<String, String> params, final VolleyResponseCallback callback) {

        Log.i("info", "PUT请求的地址url" + formatUrl(url, params));
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, formatUrl(url, params),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String jsonString) {
                        callback.getString(jsonString);
                        try {
                            jsonObject = new JSONObject(jsonString);
                            Log.i("info", "jsonstring:>>>>>>" + jsonObject.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.e("Error: ", error.getMessage());
                    }

                }
        );
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(stringRequest);
    }

    private String formatUrl(String url, Map<String, String> map) {
        if (map == null || map.isEmpty()) {
            return url;
        } else {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                if(entry.getValue()!=null){
                    url = url + "/" +entry.getKey()+"/"+ entry.getValue();
                }

            }
            return url;
        }
    }
}

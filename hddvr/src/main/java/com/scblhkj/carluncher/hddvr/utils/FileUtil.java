package com.scblhkj.carluncher.hddvr.utils;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;

/**
 * Created by Leo on 2015/11/17.
 * 文件操作工具类
 */
public class FileUtil {

    private static final String TAG = "FileUtil";

    /**
     * @param file
     * @return
     */
    public static long getDirSize(File file) {
        // 判断文件是否存在
        if (file.exists()) {
            // 如果是目录则继续递归其内容的总大小
            if (file.isDirectory()) {
                File[] children = file.listFiles();
                long size = 0;
                for (File f : children) {
                    size += getDirSize(f);
                }
                return size;
            } else {
                long size = file.length();
                return size;
            }
        } else {
                Log.e(TAG, "文件或文件夹不存在");
            return 0;
        }
    }


    /**
     * 删除空目录
     */
    public static void doDeleteEmptyDir(String dir) {
        boolean success = (new File(dir)).delete();
        if (success) {
            System.out.println("Successfully deleted empty directory: " + dir);
        } else {
            System.out.println("Failed to delete empty directory: " + dir);
        }
    }

    /**
     * 递归删除目录下的所有文件及子目录下所有文件
     *
     * @param dir 将要删除的文件目录
     * @return
     */
    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            //递归删除目录中的子目录下
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    /**
     * 修改视频文件的名称为锁存文件名
     *
     * @param
     * @return
     */
    public static String reNameFile(String originFilePath) {
        int i = originFilePath.lastIndexOf("/");
        String oldFileName = originFilePath.substring(i + 1, originFilePath.length());
      //  String newFileName = "lock_" + oldFileName;
        String newFileName = oldFileName;
        String basePath = originFilePath.substring(0, i);
        String finalFilePath = basePath + "/" + newFileName;
        File newfile = new File(finalFilePath);
        // 先判断是否有sd卡
        File oldFile = new File(originFilePath);
        if (oldFile.exists() && oldFile.isFile()) {  // 文件存在且是文件
           // if (newfile.exists()) {
              //  Log.e(TAG, "锁存文件已经存在!!!");
           // } else {
                oldFile.renameTo(newfile);
                Log.e(TAG, "碰撞视频锁存成功!!!");
           // }
        }
        return newfile.getPath();
    }

    /**
     * @param context
     * @param fileName 将Assets路径下的.db文件拷贝到data/data/包名下
     */
    public static void copyAssetsDB2Data(Context context, String fileName) {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            File file = new File(context.getFilesDir(), fileName);
            if (file.exists() && file.length() > 0) {
                return;
            }
            inputStream = context.getAssets().open(fileName);
            outputStream = new FileOutputStream(file);
            int len = 0;
            byte[] buffer = new byte[1024];
            while ((len = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * 转换文件大小
     *
     * @param fileS
     * @return
     */
    public static String formetFileSize(long fileS) {
        DecimalFormat df = new DecimalFormat("#.00");
        String fileSizeString = "";
        String wrongSize = "0B";
        if (fileS == 0) {
            return wrongSize;
        }
        if (fileS < 1024) {
            fileSizeString = df.format((double) fileS) + "B";
        } else if (fileS < 1048576) {
            fileSizeString = df.format((double) fileS / 1024) + "KB";
        } else if (fileS < 1073741824) {
            fileSizeString = df.format((double) fileS / 1048576) + "MB";
        } else {
            fileSizeString = df.format((double) fileS / 1073741824) + "GB";
        }
        return fileSizeString;
    }

}
package com.scblhkj.carluncher.hddvr.utils;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

public class MeasureScreenUtil {

    private static MeasureScreenUtil instance;
    private static Context context;
    public static int width; // 屏幕的宽度
    public static int height; // 屏幕的高度

    private MeasureScreenUtil() {
    }

    public static void init(Context ct) {
        MeasureScreenUtil.context = ct;
    }

    // 获取屏幕的宽度 dip
    public static void getScreenSize() {
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        // since SDK_INT = 1
        MeasureScreenUtil.width = metrics.widthPixels;
        MeasureScreenUtil.height = metrics.heightPixels;
        // includes window decorations(statusbar bar/menu bar)
        if (Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT < 17) {
            try {
                MeasureScreenUtil.width = (Integer) Display.class.getMethod("getRawWidth").invoke(display);
                MeasureScreenUtil.height = (Integer) Display.class.getMethod("getRawHeight").invoke(display);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (Build.VERSION.SDK_INT >= 17) {
            try {
                Point realSize = new Point();
                Display.class.getMethod("getRealSize", Point.class).invoke(display, realSize);
                MeasureScreenUtil.width = realSize.x;
                MeasureScreenUtil.height = realSize.y;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // 获取屏幕的高度 dip
    public static int getScreenHeight() {
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        return display.getHeight();
    }
}

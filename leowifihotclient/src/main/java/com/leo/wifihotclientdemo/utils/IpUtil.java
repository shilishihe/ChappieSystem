package com.leo.wifihotclientdemo.utils;

/**
 * Created by Leo on 2016/1/29.
 */
public class IpUtil {

    //将获取的int转为真正的ip地址,参考的网上的，修改了下
    public static String intToIp(int i) {
        return (i & 0XFF) + "." + ((i >> 8) & 0xFF) + "." + ((i >> 16) & 0xFF) + "." + ((i >> 24) & 0xFF);
    }

    /**
     * 获取网关地址
     *
     * @param i
     * @return
     */
    public static String getWifiGateway(int i) {
        String ip = intToIp(i);
        ip = ip.substring(0, ip.lastIndexOf(".") + 1) + "1";
        return ip;
    }
}

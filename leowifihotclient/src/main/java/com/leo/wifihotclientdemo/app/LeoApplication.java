package com.leo.wifihotclientdemo.app;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.leo.wifihotclientdemo.utils.ToastUtil;

import it.sauronsoftware.ftp4j.FTPFile;

/**
 * Created by Leo on 2016/1/29.
 */
public class LeoApplication extends Application {

    public static String gateWay = "192.168.43.1";
    public static String port = "2121";
    public static String userName = "chappie";
    public static String password = "chappie";
    private static LeoApplication app;
    public static boolean mDameonRunning = false;  // 守护线程
    private FTPFile[] ftpFiles;
    private Context context;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public LeoApplication() {
    }


    public FTPFile[] getFtpFiles() {
        return ftpFiles;
    }

    public void setFtpFiles(FTPFile[] ftpFiles) {
        this.ftpFiles = ftpFiles;
    }

    public static LeoApplication getApp() {
        if (null == app) {
            app = new LeoApplication();
        }
        return app;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ToastUtil.init(this);
    }

    /**
     * 向FTP服务发送那个广播
     *
     * @param action
     */
    public void sendBroadCast2Ftp(String action) {
        Intent intent = new Intent();
        intent.setAction(action);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        context.sendBroadcast(intent);
    }


}

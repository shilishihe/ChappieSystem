package com.leo.wifihotclientdemo.factory;


import com.leo.wifihotclientdemo.runnable.FtpCWDRunnable;
import com.leo.wifihotclientdemo.runnable.FtpCmdRunnableInterface;
import com.leo.wifihotclientdemo.runnable.FtpConnectRunnable;
import com.leo.wifihotclientdemo.runnable.FtpDeleteRunnable;
import com.leo.wifihotclientdemo.runnable.FtpDisConnectRunnable;
import com.leo.wifihotclientdemo.runnable.FtpLISTRunnable;
import com.leo.wifihotclientdemo.runnable.FtpPWDRunnable;
import com.leo.wifihotclientdemo.runnable.FtpRenameRunnable;

import it.sauronsoftware.ftp4j.FTPClient;

/**
 * Created by Leo on 2016/2/1.
 * Ftp指令工厂类
 */
public class FtpRunnableFactory {

    public static FtpCmdRunnableInterface getConnectRunnable(FTPClient client) {
        return new FtpConnectRunnable(client);
    }

    public static FtpCmdRunnableInterface getDisConnectRunnable(FTPClient client) {
        return new FtpDisConnectRunnable(client);
    }

    public static FtpCmdRunnableInterface getPwdRunnable(FTPClient client) {
        return new FtpPWDRunnable(client);
    }

    public static FtpCmdRunnableInterface getListRunnable(FTPClient client) {
        return new FtpLISTRunnable(client);
    }

    public static FtpCmdRunnableInterface getCwdRunnable(FTPClient client, String filePath) {
        return new FtpCWDRunnable(client, filePath);
    }

    public static FtpCmdRunnableInterface getDelRunnable(FTPClient client, String filePath, boolean isDir) {
        return new FtpDeleteRunnable(client, filePath, isDir);
    }

    public static FtpCmdRunnableInterface getRenameRunnable(FTPClient client, String oldPath, String newPath) {
        return new FtpRenameRunnable(client, oldPath, newPath);
    }

}

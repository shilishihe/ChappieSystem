package com.leo.wifihotclientdemo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.leo.wifihotclientdemo.R;
import com.leo.wifihotclientdemo.app.LeoApplication;

/**
 * FTP文件管理配置界面
 */
public class FtpFileManagerConfigActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText et_ip;
    private EditText et_port;
    private EditText et_userName;
    private EditText et_userPassword;
    private Button connFTP;


    private void initView() {
        et_ip = (EditText) findViewById(R.id.et_ip);
        et_port = (EditText) findViewById(R.id.et_port);
        et_userName = (EditText) findViewById(R.id.et_userName);
        et_userPassword = (EditText) findViewById(R.id.et_userPassword);
        connFTP = (Button) findViewById(R.id.connFTP);
    }

    private void initData() {
        LeoApplication.password = et_userPassword.getText().toString();
        LeoApplication.gateWay = et_ip.getText().toString();
        LeoApplication.port = et_port.getText().toString();
        LeoApplication.userName = et_userName.getText().toString();
        connFTP.setOnClickListener(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ftp_file_config_manager);
        initView();
        initData();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.connFTP: {
                Intent intent = new Intent(this, FtpFileManagerActivity.class);
                startActivity(intent);
                break;
            }
            default: {
                break;
            }
        }
    }
}

package com.leo.wifihotclientdemo.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.leo.wifihotclientdemo.R;
import com.leo.wifihotclientdemo.adapter.FtpFileAdapter;
import com.leo.wifihotclientdemo.app.LeoApplication;
import com.leo.wifihotclientdemo.constant.Constans;
import com.leo.wifihotclientdemo.factory.FtpRunnableFactory;
import com.leo.wifihotclientdemo.runnable.FtpDameonRunnable;
import com.leo.wifihotclientdemo.utils.ToastUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;
import it.sauronsoftware.ftp4j.FTPFile;

/**
 * FTP文件管理器界面
 */
public class FtpFileManagerActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private ListView mListView;
    private String mFTPHost;
    private int mFTPPort;
//    private String mFTPUser;
//    private String mFTPPassword;
    private ExecutorService mThreadPool;
    private static String mAtSDCardPath;
    private int cpuNums; //当前系统的CPU数目
    private List<FTPFile> mFileList;
    private int mSelectedPosistion = -1;// 选中的条目索引
    private BroadcastReceiver broadcastReceiver;
    private Thread mDameonThread;  // 守护线程
    private FTPClient mFTPClient;
    private FtpFileAdapter mAdapter;
    private ProgressBar mPbLoad;
    private Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ftp_file_manager);
        initView();
        initData();
    }

    private void initView() {
        mListView = (ListView) findViewById(R.id.lv);
    }

    private void initData() {
        LeoApplication.getApp().setContext(this);
        mFTPClient = new FTPClient();
        cpuNums = Runtime.getRuntime().availableProcessors();
        mThreadPool = Executors.newFixedThreadPool(cpuNums);
        mFileList = new ArrayList<FTPFile>();
        initBroadCast();
        mListView.setOnItemClickListener(this);
        mListView.setOnItemLongClickListener(this);
        LeoApplication.mDameonRunning = true;
        // 执行连接FTP服务器请求
        mThreadPool.execute(FtpRunnableFactory.getConnectRunnable(mFTPClient));
    }

    private static final String TAG = "FtpFileManagerActivity";

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mFileList.get(position).getType() == FTPFile.TYPE_DIRECTORY) {  // 如果是文件夹，则设置操作路径
            Log.e(TAG, mFileList.get(position).getName() + "文件被点击");
            mThreadPool.execute(FtpRunnableFactory.getCwdRunnable(mFTPClient, mFileList.get(position).getName()));
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Log.e(TAG, mFileList.get(position).getName() + "文件被长点击");
        return false;
    }

    /**
     * 文件管理
     */
    private class FtpDownLoad extends AsyncTask<Void, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                String tempLocalPath = getParentRootPath() + File.separator + mFileList.get(mSelectedPosistion).getName();
                mFTPClient.download(mFileList.get(mSelectedPosistion).getName(), new File(tempLocalPath), new DownloadFTPDataTransferListener(mFileList.get(mSelectedPosistion).getSize()));
            } catch (Exception e) {
            }
            return null;
        }
    }

    /**
     * 获取自定义存储文件根目录
     *
     * @return
     */
    private static String getParentRootPath() {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            if (mAtSDCardPath != null) {
                return mAtSDCardPath;
            } else {
                mAtSDCardPath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + Constans.CHAPPIE_DOWNLOAD;
                File rootFile = new File(mAtSDCardPath);
                if (!rootFile.exists()) {
                    rootFile.mkdir();
                }
                return mAtSDCardPath;
            }
        }
        return null;
    }

    public Socket socket;

    /**
     * 初始化广播接受者
     */
    private void initBroadCast() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch (action) {
                    case Constans.MSG_CMD_CONNECT_OK: {
                        if (mDameonThread == null) {
                            /**
                             * 启动守护线程
                             */
                            mDameonThread = new Thread(new FtpDameonRunnable(mFTPClient));
                            mDameonThread.setDaemon(true);
                            mDameonThread.start();
                            ToastUtil.showToast("FTP服务器连接成功，守护线程启动成功");
                            mThreadPool.execute(FtpRunnableFactory.getListRunnable(mFTPClient));
                            wifiHotClientThread = new WifiHotClientThread();
                            wifiHotClientThread.start();
                        }
                        break;
                    }
                    case Constans.MSG_CMD_CONNECT_FAILED: {
                        ToastUtil.showToast("FTP服务器连接失败,正在尝试重新连接...");
                        mThreadPool.execute(FtpRunnableFactory.getConnectRunnable(mFTPClient));
                        break;
                    }
                    case Constans.MSG_CMD_LIST_OK: {
                        ToastUtil.showToast("获取列表数据请求成功");
                        synchronized (FtpFileManagerActivity.class) {
                            mFileList.clear();
                            mFileList.addAll(Arrays.asList(LeoApplication.getApp().getFtpFiles()));
                        }
                        buildOrUpdateDataset();
                        break;
                    }
                    case Constans.MSG_CMD_LIST_FAILED: {
                        ToastUtil.showToast("获取列表数据请求失败");
                        break;
                    }
                    case Constans.MSG_CMD_CWD_OK: {
                        ToastUtil.showToast("设置路径请求成功");
                        mThreadPool.execute(FtpRunnableFactory.getListRunnable(mFTPClient));
                        break;
                    }
                    case Constans.MSG_CMD_CWD_FAILED: {
                        ToastUtil.showToast("设置路径请求失败");
                        break;
                    }
                    case Constans.MSG_CMD_DELE_OK: {
                        ToastUtil.showToast("删除文件请求成功");
                        mThreadPool.execute(FtpRunnableFactory.getListRunnable(mFTPClient));
                        break;
                    }
                    case Constans.MSG_CMD_DELE_FAILED: {
                        ToastUtil.showToast("删除文件请求失败");
                        break;
                    }
                    case Constans.MSG_CMD_RENAME_OK: {
                        ToastUtil.showToast("重命名文件请求成功");
                        mThreadPool.execute(FtpRunnableFactory.getListRunnable(mFTPClient));
                        break;
                    }
                    case Constans.MSG_CMD_RENAME_FAILED: {
                        ToastUtil.showToast("重命名文件请求失败");
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        };
    }

    /**
     * 注册广播接收者
     */
    private void registerBroadCast() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constans.MSG_CMD_CONNECT_OK);
        intentFilter.addAction(Constans.MSG_CMD_CONNECT_FAILED);
        intentFilter.addAction(Constans.MSG_CMD_LIST_OK);
        intentFilter.addAction(Constans.MSG_CMD_CWD_OK);
        intentFilter.addAction(Constans.MSG_CMD_CWD_FAILED);
        intentFilter.addAction(Constans.MSG_CMD_DELE_OK);
        intentFilter.addAction(Constans.MSG_CMD_DELE_FAILED);
        intentFilter.addAction(Constans.MSG_CMD_RENAME_OK);
        intentFilter.addAction(Constans.MSG_CMD_RENAME_FAILED);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    private void buildOrUpdateDataset() {
        if (mAdapter == null) {
            mAdapter = new FtpFileAdapter(this, mFileList);
            mListView.setAdapter(mAdapter);
        }
        mAdapter.notifyDataSetChanged();
    }

    /**
     * 注销广播接者
     */
    private void unRegisterBroadCast() {
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    protected void onResume() {
        registerBroadCast();
        super.onResume();
    }

    @Override
    protected void onPause() {
        unRegisterBroadCast();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        LeoApplication.mDameonRunning = false;
        mThreadPool.execute(FtpRunnableFactory.getDisConnectRunnable(mFTPClient));
        SystemClock.sleep(2000);
        mThreadPool.shutdownNow();
        super.onDestroy();
    }

    @SuppressLint("LongLogTag")
    private class DownloadFTPDataTransferListener implements FTPDataTransferListener {

        private static final String TAG = "DownloadFTPDataTransferListener";
        private int totolTransferred = 0;
        private long fileSize = -1;

        public DownloadFTPDataTransferListener(long fileSize) {
            if (fileSize <= 0) {
                throw new RuntimeException("the size of file muset be larger than zero.");
            }
            this.fileSize = fileSize;
        }

        @Override
        public void aborted() {
            Log.e(TAG, "FTPDataTransferListener : aborted");
        }

        @Override
        public void completed() {
            Log.e(TAG, "FTPDataTransferListener : completed");
            setLoadProgress(mPbLoad.getMax());
        }

        @Override
        public void failed() {
            Log.e(TAG, "FTPDataTransferListener : failed");
        }

        @Override
        public void started() {
            Log.e(TAG, "FTPDataTransferListener : started");
        }

        @Override
        public void transferred(int length) {
            totolTransferred += length;
            float percent = (float) totolTransferred / this.fileSize;
            Log.e(TAG, "FTPDataTransferListener : transferred # percent @@" + percent);
            setLoadProgress((int) (percent * mPbLoad.getMax()));
        }
    }

    public void setLoadProgress(int progress) {
        if (mPbLoad != null) {
            mPbLoad.setProgress(progress);
        }
    }

    private Dialog createLoadDialog() {
        View rootLoadView = getLayoutInflater().inflate(R.layout.dialog_load_file, null);
        mPbLoad = (ProgressBar) rootLoadView.findViewById(R.id.pbLoadFile);
        progressDialog = new AlertDialog.Builder(this).setTitle("请稍等片刻...").setView(rootLoadView).setCancelable(false).create();
        progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setLoadProgress(0);
            }
        });
        return progressDialog;
    }

    private WifiHotClientThread wifiHotClientThread;

    private class WifiHotClientThread extends Thread {
        public Socket socket;
        public Context context;
        public Boolean isrun = true;
        public OutputStream os;
        public InputStream in;

        @Override
        public void run() {
            try {
                socket = new Socket("192.168.43.1", 34353);
                if (socket != null && !socket.isClosed() && !socket.isOutputShutdown()) {
                    while (isrun) {
                        String data = "客户端WIFI数据" + "\n";
                        os.write(data.getBytes());
                        os.flush();
                        SystemClock.sleep(3000);
                    }
                }else{
                    ToastUtil.showToast("Socket初始化异常");
                }
            } catch (IOException e) {
                e.printStackTrace();
                ToastUtil.showToast("IOException e Socket初始化异常");
            }
            super.run();
        }

        /**
         * 释放
         */
        public void realse() {
            isrun = false;
        }
    }

}

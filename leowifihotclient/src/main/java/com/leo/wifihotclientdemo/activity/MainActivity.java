package com.leo.wifihotclientdemo.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.leo.wifihotclientdemo.R;
import com.leo.wifihotclientdemo.adapter.WifiHotAdapter;
import com.leo.wifihotclientdemo.app.LeoApplication;
import com.leo.wifihotclientdemo.manager.WifiHotManager;
import com.leo.wifihotclientdemo.utils.IpUtil;
import com.leo.wifihotclientdemo.utils.ToastUtil;

import java.util.List;

public class MainActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener, WifiHotManager.WifiBroadCastOperations {

    private TextView tvState;
    private Button searchHot;
    private ListView listView;
    private List<ScanResult> wifiList;
    private WifiHotManager wifiHotManager;
    private WifiHotAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initData();
    }


    private void initView() {
        tvState = (TextView) findViewById(R.id.tvState);
        searchHot = (Button) findViewById(R.id.bt_searchHot);
        listView = (ListView) findViewById(R.id.listHots);
    }

    private void initData() {
        searchHot.setOnClickListener(this);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvState: {
                break;
            }
            default: {
                break;
            }
        }
    }

    private static final String TAG = "MainActivity";

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ScanResult result = wifiList.get(position);
        tvState.setText("连接中...");
        ToastUtil.showToast(result.SSID);
        wifiHotManager.connectToHotpot(result.SSID, wifiList, "12345678");
    }

    @Override
    public void disPlayWifiScanResult(List<ScanResult> wifiList) {
        Log.i(TAG, "into 扫描结果回调函数");
        this.wifiList = wifiList;
        wifiHotManager.unRegisterWifiScanBroadCast();
        refreshWifiList(wifiList);
        Log.i(TAG, "out 热点扫描结果 ： = " + wifiList);
    }

    @Override
    public boolean disPlayWifiConResult(boolean result, WifiInfo wifiInfo) {
        tvState.setText("热点连接成功");
        LeoApplication.gateWay = IpUtil.getWifiGateway(wifiInfo.getIpAddress());
        Log.e(TAG,"服务器IP = " + LeoApplication.gateWay);
        wifiHotManager.setConnectStatu(false);
        wifiHotManager.unRegisterWifiStateBroadCast();
        wifiHotManager.unRegisterWifiConnectBroadCast();
        listView.setVisibility(View.GONE);
        Intent intent = new Intent(this, FtpFileManagerConfigActivity.class);
        startActivity(intent);
        return false;
    }

    @Override
    public void operationByType(WifiHotManager.OpretionsType type, String SSID) {

    }

    // 扫描热点广播初始化
    @Override
    protected void onResume() {
        wifiHotManager = WifiHotManager.getInstance(this, this);
        wifiHotManager.scanWifiHot();
        super.onResume();
    }

    private void refreshWifiList(List<ScanResult> results) {
        Log.i(TAG, "into 刷新wifi热点列表");
        if (null == adapter) {
            Log.i(TAG, "into 刷新wifi热点列表 adapter is null！");
            adapter = new WifiHotAdapter(results, this);
            listView.setAdapter(adapter);
        } else {
            Log.i(TAG, "into 刷新wifi热点列表 adapter is not null！");
            adapter.refreshData(results);
        }
        Log.i(TAG, "out 刷新wifi热点列表");
    }
}

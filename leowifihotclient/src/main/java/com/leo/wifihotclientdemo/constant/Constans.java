package com.leo.wifihotclientdemo.constant;

/**
 * Created by Leo on 2016/1/29.
 */
public interface Constans {
    // wifi状态
    int WIFI_CONNECTING = 0;
    int WIFI_CONNECTED = 1;
    int WIFI_CONNECT_FAILED = 2;
    String SSID = "LEO_SSID";
    String HOT_PASSWORD = "LEO_MIME";
    int WIFICIPHER_NOPASS = 1;
    int WIFICIPHER_WEP = 2;
    int WIFICIPHER_WPA = 3;
    String INT_SERVER_FAIL = "INTSERVER_FAIL";
    String INT_SERVER_SUCCESS = "INTSERVER_SUCCESS";
    String INT_CLIENT_FAIL = "INTCLIENT_FAIL";
    String INT_CLIENT_SUCCESS = "INTCLIENT_SUCCESS";
    String CONNECT_SUCESS = "connect_success";
    String CONNECT_FAIL = "connect_fail";
    // 数据传输命令
    int IPMSG_SNEDCLIENTDATA = 0x00000050; // 发送单个client信息（socket连接成功后执行）
    int IPMSG_SENDALLCLIENTS = 0x00000051; // 发送全部客户端信息（Server
    // 接收一个client连接后发送当前所有客户端信息）
    int IPMSG_SENDROTARYDATA = 0x00000060; // 发送旋转角度信息
    int IPMSG_SENDROTARYRESULT = 0x00000061; // 发送旋转的结果
    int IPMSG_SENDCHANGECONTROLLER = 0x00000062; // 发送修改控制权
    int IPMSG_REQUESTCHANGECONTROLLER = 0x00000062; // 请求修改控制权

    /**
     * FTP服务
     */
    String USERNAME = "username";
    String PASSWORD = "password";
    String PORTNUM = "portNum";
    String CHROOTDIR = "chrootDir";
    String ACCEPT_WIFI = "allowWifi";
    String ACCEPT_NET = "allowNet";
    String STAY_AWAKE = "stayAwake";

    /**
     * Runnabble广播
     */
    String MSG_CMD_CONNECT_OK = "com.intent.broadcast.msg_cmd_connect_ok";
    String MSG_CMD_CONNECT_FAILED = "com.intent.broadcast.msg_cmd_connect_failed";
    String MSG_CMD_LIST_OK = "com.intent.broadcast.msg_cmd_list_ok";
    String MSG_CMD_LIST_FAILED = "com.intent.broadcast.msg_cmd_list_failed";
    String MSG_CMD_CWD_OK = "com.intent.broadcast.msg_cmd_cwd_ok";
    String MSG_CMD_CWD_FAILED = "com.intent.broadcast.msg_cmd_cwd_failed";
    String MSG_CMD_DELE_OK = "com.intent.broacast.msg_cmd_dele_ok";
    String MSG_CMD_DELE_FAILED = "com.intent.broadcast.msg_cmd_dele_failed";
    String MSG_CMD_RENAME_OK = "com.intent.broadcast.msg_cmd_rename_ok";
    String MSG_CMD_RENAME_FAILED = "com.intent.broadcast.msg_cmd_rename_failed";

    /**
     * 下载文件存放的目录
     */
    String CHAPPIE_DOWNLOAD = "CHAPPIE_DOWNLOAD";


    // 守护进程重连间隔时间
    int MAX_DAMEON_TIME_WAIT = 2 * 1000;
}

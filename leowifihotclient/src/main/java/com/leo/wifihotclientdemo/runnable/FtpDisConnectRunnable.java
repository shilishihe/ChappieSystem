package com.leo.wifihotclientdemo.runnable;

import java.io.IOException;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPException;
import it.sauronsoftware.ftp4j.FTPIllegalReplyException;

/**
 * Created by Leo on 2016/2/1.
 * FTP服务断开
 */
public class FtpDisConnectRunnable extends FtpCmdRunnableInterface {

    private FTPClient mFTPClient;

    public FtpDisConnectRunnable(FTPClient mFTPClient) {
        this.mFTPClient = mFTPClient;
    }

    @Override
    public void run() {
        if (mFTPClient != null) {
            try {
                mFTPClient.disconnect(true);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (FTPIllegalReplyException e) {
                e.printStackTrace();
            } catch (FTPException e) {
                e.printStackTrace();
            }
        }
    }
}

package com.leo.wifihotclientdemo.runnable;

import com.leo.wifihotclientdemo.app.LeoApplication;
import com.leo.wifihotclientdemo.constant.Constans;

import it.sauronsoftware.ftp4j.FTPClient;

/**
 * Created by Leo on 2016/2/1.
 * 重命名文件夹
 */
public class FtpRenameRunnable extends FtpCmdRunnableInterface {

    private FTPClient mFTPClient;
    private String oldPath;
    private String newPath;

    public FtpRenameRunnable(FTPClient mFTPClient, String oldPath, String newPath) {
        this.mFTPClient = mFTPClient;
        this.oldPath = oldPath;
        this.newPath = newPath;
    }


    @Override
    public void run() {
        try {
            mFTPClient.rename(oldPath, newPath);
            LeoApplication.getApp().sendBroadCast2Ftp(Constans.MSG_CMD_RENAME_OK);
        } catch (Exception e) {
            LeoApplication.getApp().sendBroadCast2Ftp(Constans.MSG_CMD_RENAME_FAILED);
            e.printStackTrace();
        }
    }
}

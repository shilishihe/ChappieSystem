package com.leo.wifihotclientdemo.runnable;

/**
 * Created by Leo on 2016/1/30.
 */
public abstract class FtpCmdRunnableInterface implements Runnable {
    public abstract void run();
}
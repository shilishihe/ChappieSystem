package com.leo.wifihotclientdemo.runnable;

import java.io.IOException;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPException;
import it.sauronsoftware.ftp4j.FTPIllegalReplyException;

/**
 * Created by Leo on 2016/2/1.
 * 获取当前的文件夹路径
 */
public class FtpPWDRunnable extends FtpCmdRunnableInterface {
    private FTPClient mFTPClient;

    public FtpPWDRunnable(FTPClient mFTPClient) {
        this.mFTPClient = mFTPClient;
    }

    @Override
    public void run() {
        try {
            String pwd = mFTPClient.currentDirectory();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FTPIllegalReplyException e) {
            e.printStackTrace();
        } catch (FTPException e) {
            e.printStackTrace();
        }
    }
}

package com.leo.wifihotclientdemo.runnable;

import com.leo.wifihotclientdemo.app.LeoApplication;
import com.leo.wifihotclientdemo.constant.Constans;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPFile;

/**
 * Created by Leo on 2016/2/1.
 * 请求当前设置路径下的文件(文件和文件夹)
 */
public class FtpLISTRunnable extends FtpCmdRunnableInterface {

    private FTPClient mFTPClient;

    public FtpLISTRunnable(FTPClient mFTPClient) {
        this.mFTPClient = mFTPClient;
    }

    @Override
    public void run() {
        try {
            FTPFile[] ftpFiles = mFTPClient.list();
            LeoApplication.getApp().setFtpFiles(ftpFiles);
            LeoApplication.getApp().sendBroadCast2Ftp(Constans.MSG_CMD_LIST_OK);
        } catch (Exception e) {
            LeoApplication.getApp().sendBroadCast2Ftp(Constans.MSG_CMD_LIST_FAILED);
            e.printStackTrace();
        }
    }
}

package com.leo.wifihotclientdemo.runnable;

import android.content.Context;
import android.os.SystemClock;
import android.util.Log;

import com.leo.wifihotclientdemo.app.LeoApplication;
import com.leo.wifihotclientdemo.constant.Constans;

import java.io.IOException;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPException;
import it.sauronsoftware.ftp4j.FTPIllegalReplyException;

/**
 * Created by Leo on 2016/1/30.
 * 登录
 */
public class FtpConnectRunnable extends FtpCmdRunnableInterface {

    private static final String TAG = "FtpConnectRunnable";
    private FTPClient mFTPClient;
    private String mFTPHost;
    private int mFTPPort;
    private String mFTPUser;
    private String mFTPPassword;
    private Context context;

    public FtpConnectRunnable(FTPClient mFTPClient) {
        this.mFTPHost = LeoApplication.gateWay;
        this.mFTPPort = Integer.parseInt(LeoApplication.port);
        this.mFTPUser = LeoApplication.userName;
        this.mFTPPassword = LeoApplication.password;
        this.mFTPClient = mFTPClient;
    }

    @Override
    public void run() {
        boolean errorAndRetry = false; // 根据不能的异常类型，是否重新捕获
        try {
            String[] welcome = mFTPClient.connect(mFTPHost, mFTPPort);
            if (welcome != null) {
                for (String value : welcome) {
                    Log.e(TAG, "connect " + value);
                }
            }
            mFTPClient.login(mFTPUser, mFTPPassword);
            LeoApplication application = LeoApplication.getApp();
            if (application != null) {
                LeoApplication.getApp().sendBroadCast2Ftp(Constans.MSG_CMD_CONNECT_OK);
            }
        } catch (IllegalStateException illegalEx) {
            illegalEx.printStackTrace();
            errorAndRetry = true;
        } catch (IOException ex) {
            ex.printStackTrace();
            errorAndRetry = true;
        } catch (FTPIllegalReplyException e) {
            e.printStackTrace();
        } catch (FTPException e) {
            e.printStackTrace();
            errorAndRetry = true;
        }
        if (errorAndRetry && LeoApplication.mDameonRunning) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    SystemClock.sleep(2000);
                    LeoApplication.getApp().sendBroadCast2Ftp(Constans.MSG_CMD_CONNECT_FAILED);
                }
            }).start();
        }
    }


}

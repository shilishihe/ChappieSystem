package com.leo.wifihotclientdemo.runnable;

import android.util.Log;

import com.leo.wifihotclientdemo.app.LeoApplication;
import com.leo.wifihotclientdemo.constant.Constans;

import it.sauronsoftware.ftp4j.FTPClient;

/**
 * Created by Leo on 2016/2/1.
 * 切换要请求的路径
 */
public class FtpCWDRunnable extends FtpCmdRunnableInterface {

    private static final String TAG = "FtpCWDRunnable";

    private FTPClient mFTPClient;
    private String newPath;

    public FtpCWDRunnable(FTPClient mFTPClient, String oldPath) {
        Log.e(TAG,"FtpCWDRunnable对象初始化成功");
        this.mFTPClient = mFTPClient;
        this.newPath = oldPath;
    }

    @Override
    public void run() {
        try {
            mFTPClient.changeDirectory(newPath);
            Log.e(TAG, "FtpCWDRunnable对象修改文件路径成功");
            LeoApplication.getApp().sendBroadCast2Ftp(Constans.MSG_CMD_CWD_OK);
        } catch (Exception e) {
            Log.e(TAG, "FtpCWDRunnable对象修改文件路径失败");
            LeoApplication.getApp().sendBroadCast2Ftp(Constans.MSG_CMD_CWD_FAILED);
            e.printStackTrace();
        }
    }
}

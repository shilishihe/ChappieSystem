package com.leo.wifihotclientdemo.runnable;

import com.leo.wifihotclientdemo.app.LeoApplication;
import com.leo.wifihotclientdemo.constant.Constans;

import it.sauronsoftware.ftp4j.FTPClient;

/**
 * Created by Leo on 2016/2/1.
 * 守护线程接口
 */
public class FtpDameonRunnable implements Runnable {

    private FTPClient mFTPClient;

    public FtpDameonRunnable(FTPClient mFTPClient) {
        this.mFTPClient = mFTPClient;
    }

    @Override
    public void run() {
        while (LeoApplication.mDameonRunning) {
            if (mFTPClient != null && !mFTPClient.isConnected()) {
                try {
                    mFTPClient.connect(LeoApplication.gateWay, Integer.parseInt(LeoApplication.port));
                    mFTPClient.login(LeoApplication.userName, LeoApplication.password);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            try {
                Thread.sleep(Constans.MAX_DAMEON_TIME_WAIT);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}

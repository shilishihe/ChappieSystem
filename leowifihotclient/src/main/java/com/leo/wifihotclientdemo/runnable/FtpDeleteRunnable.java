package com.leo.wifihotclientdemo.runnable;

import com.leo.wifihotclientdemo.app.LeoApplication;
import com.leo.wifihotclientdemo.constant.Constans;

import it.sauronsoftware.ftp4j.FTPClient;

/**
 * Created by Leo on 2016/2/1.
 * 删除文件操作
 */
public class FtpDeleteRunnable extends FtpCmdRunnableInterface {

    private FTPClient mFTPClient;
    private String filePath;
    private boolean isDir;

    public FtpDeleteRunnable(FTPClient mFTPClient, String filePath, boolean isDir) {
        this.mFTPClient = mFTPClient;
        this.filePath = filePath;
        this.isDir = isDir;
    }


    @Override
    public void run() {
        try {
            if (isDir) {
                mFTPClient.deleteDirectory(filePath);
            } else {
                mFTPClient.deleteFile(filePath);
            }
            LeoApplication.getApp().sendBroadCast2Ftp(Constans.MSG_CMD_DELE_OK);
        } catch (Exception e) {
            e.printStackTrace();
            LeoApplication.getApp().sendBroadCast2Ftp(Constans.MSG_CMD_DELE_FAILED);
        }
    }
}

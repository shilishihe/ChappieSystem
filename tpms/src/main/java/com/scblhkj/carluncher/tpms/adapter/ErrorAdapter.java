package com.scblhkj.carluncher.tpms.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.scblhkj.carluncher.tpms.R;
import com.scblhkj.carluncher.tpms.bean.ErrorBean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Leo on 2016/3/3.
 * 胎压异常的数据适配器
 */
public class ErrorAdapter extends BaseAdapter {

    private List<ErrorBean> list;
    private Context context;

    public ErrorAdapter(Context context) {
        this.list = new ArrayList<ErrorBean>();
        this.context = context;
    }

    public List<ErrorBean> getList() {
        return this.list;
    }

    public void setList(List<ErrorBean> list) {
        this.list = list;
    }

    /**
     * 添加异常类
     */
    public void addErrorBean(ErrorBean bean) {
        this.list.add(bean);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.activity_main_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.tv_info = (TextView) convertView.findViewById(R.id.tv_info);
            viewHolder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            viewHolder.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tv_info.setText(list.get(position).getErrorInfo());
        viewHolder.tv_name.setText(list.get(position).getErrorName());
        viewHolder.tv_time.setText(getCurrentTime());
        return convertView;
    }

    private class ViewHolder {
        public TextView tv_name;
        public TextView tv_info;
        public TextView tv_time;
    }

    private String getCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        return sdf.format(now);
    }
}

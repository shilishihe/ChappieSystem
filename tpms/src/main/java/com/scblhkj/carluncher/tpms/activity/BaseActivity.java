package com.scblhkj.carluncher.tpms.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.scblhkj.carluncher.tpms.R;


/**
 * Created by he on 2015/10/3.
 * ..
 * 所有Activity的基类
 */
public abstract class BaseActivity extends Activity {

    private static String TAG = "BaseActivity";
    protected View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_base);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().setBackgroundDrawable(null);
    }

    /**
     * 初始化布局
     */
    abstract protected void initView();


    /**
     * 初始化数据
     */
    abstract protected void initData();

    /**
     * Activity跳转
     *
     * @param tarActivity
     */
    protected void intent2Activity(Class<? extends Activity> tarActivity) {
        Intent intent = new Intent(this, tarActivity);
        startActivity(intent);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.i(TAG, "onConfigurationChanged");
        super.onConfigurationChanged(newConfig);
    }

}

package com.scblhkj.carluncher.tpms.bean;

import java.io.Serializable;

/**
 * Created by Leo on 2016/2/29.
 * 胎压数据
 */
public class TPMSSerialBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private int[][] datasss;
    private int flag;
    // 胎压原始数据
    private String originData;

    public String getOriginData() {
        return originData;
    }

    public void setOriginData(String originData) {
        this.originData = originData;
    }

    public int[][] getDatasss() {
        return datasss;
    }

    public void setDatasss(int[][] datasss) {
        this.datasss = datasss;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }
}

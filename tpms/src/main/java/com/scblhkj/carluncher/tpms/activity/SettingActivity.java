package com.scblhkj.carluncher.tpms.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.TextView;

import com.rey.material.widget.Button;
import com.scblhkj.carluncher.tpms.R;
import com.scblhkj.carluncher.tpms.common.Constant;
import com.scblhkj.carluncher.tpms.util.SpUtil;
import com.scblhkj.carluncher.tpms.util.TitleBuilder;

/**
 * 设置界面
 */
public class SettingActivity extends BaseActivity implements View.OnClickListener
        , SeekBar.OnSeekBarChangeListener {

    private TextView tv_emitter_matching;
    private TextView tv_pre_top_value;
    private TextView tv_pre_low_value;
    private TextView tv_temp_top_value;
    private Button bt_reset_params;
    private SeekBar sb_pres_top;
    private SeekBar sb_pres_low;
    private SeekBar sb_temp_top;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        initData();
    }

    @Override
    protected void initView() {
        view = View.inflate(this, R.layout.activity_setting, null);
        new TitleBuilder(view)
                .setTitleText("胎压设置")
                //.setLeftImage(R.drawable.ic_title_back_left_bt)
                //.setLeftOnClickListener(new View.OnClickListener() {
                 //   @Override
                  //  public void onClick(View view) {
                       // finish();
                   // }
             //  })
            .build();
        tv_emitter_matching = (TextView) view.findViewById(R.id.tv_emitter_matching);
        tv_pre_top_value = (TextView) view.findViewById(R.id.tv_pre_top_value);
        tv_temp_top_value = (TextView) view.findViewById(R.id.tv_temp_top_value);
        tv_pre_low_value = (TextView) view.findViewById(R.id.tv_pre_low_value);
        bt_reset_params = (Button) view.findViewById(R.id.bt_reset_params);
        sb_pres_top = (SeekBar) view.findViewById(R.id.sb_pres_top);
        sb_pres_low = (SeekBar) view.findViewById(R.id.sb_pres_low);
        sb_temp_top = (SeekBar) view.findViewById(R.id.sb_temp_top);
        setContentView(view);
    }

    @Override
    protected void initData() {
        tv_emitter_matching.setOnClickListener(this);
        bt_reset_params.setOnClickListener(this);
        sb_pres_top.setOnSeekBarChangeListener(this);
        sb_pres_low.setOnSeekBarChangeListener(this);
        sb_temp_top.setOnSeekBarChangeListener(this);
        initViewDataInSP();
    }

    /**
     * 从SP中获取控件值
     */
    private void initViewDataInSP() {
        if (SpUtil.getInt2SP(Constant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX) > 0) {
            sb_pres_top.setProgress(SpUtil.getInt2SP(Constant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX));
            tv_pre_top_value.setText(SpUtil.getInt2SP(Constant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX) + "Bar");
        }
        if (SpUtil.getInt2SP(Constant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN) > 0) {
            sb_pres_low.setProgress(SpUtil.getInt2SP(Constant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN));
            tv_pre_low_value.setText(SpUtil.getInt2SP(Constant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN) + "Bar");
        }
        if (SpUtil.getInt2SP(Constant.TMPS_FRAGMENT_SLIDER_TEMP_MAX) > 0) {
            sb_temp_top.setProgress(SpUtil.getInt2SP(Constant.TMPS_FRAGMENT_SLIDER_TEMP_MAX));
            tv_temp_top_value.setText(SpUtil.getInt2SP(Constant.TMPS_FRAGMENT_SLIDER_TEMP_MAX) + "Bar");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_emitter_matching: {
                Log.e("TAG", "进入胎压配对界面");
                // 进入胎压配对界面
                intent2Activity(EmitterMatchingActivity.class);
                break;
            }
            case R.id.bt_reset_params: {
                break;
            }
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()) {
            case R.id.sb_pres_top: {
                SpUtil.saveInt2SP(Constant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX, progress);
                tv_pre_top_value.setText(progress/10f + "Bar");
                break;
            }
            case R.id.sb_pres_low: {
                SpUtil.saveInt2SP(Constant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN, progress);
                tv_pre_low_value.setText(progress/10f + "Bar");
                break;
            }
            case R.id.sb_temp_top: {
                SpUtil.saveInt2SP(Constant.TMPS_FRAGMENT_SLIDER_TEMP_MAX, progress);
                tv_temp_top_value.setText(progress + "℃");
                break;
            }
            default: {
                break;
            }
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}

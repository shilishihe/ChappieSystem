package com.scblhkj.carluncher.tpms.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scblhkj.carluncher.tpms.R;
import com.scblhkj.carluncher.tpms.adapter.ErrorAdapter;
import com.scblhkj.carluncher.tpms.bean.ErrorBean;
import com.scblhkj.carluncher.tpms.common.Constant;
import com.scblhkj.carluncher.tpms.util.TitleBuilder;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;

/**
 * 胎压主界面
 */
public class MainActivity extends BaseActivity implements View.OnClickListener {

    // 左前轮胎压
    private TextView tv_left_top_pre;
    // 左前轮温度
    private TextView tv_left_top_temp;
    // 右前轮胎压
    private TextView tv_right_top_pre;
    // 右前轮温度
    private TextView tv_right_top_temp;
    // 左后轮胎压
    private TextView tv_left_bottom_pre;
    // 左后轮温度
    private TextView tv_left_bottom_temp;
    // 右后轮胎压
    private TextView tv_right_bottom_pre;
    // 右后轮温度
    private TextView tv_right_bottom_temp;
    // 接收串口数据的广播接收者
    private BroadcastReceiver broadcastReceiver;
    private static final String TAG = "胎压主界面";

    // 胎压单位
    private TextView tv_left_top_pre_des;
    private TextView tv_right_top_pre_des;
    private TextView tv_left_bottom_pre_des;
    private TextView tv_right_bottom_pre_des;

    // 温度单位
    private TextView tv_left_top_temp_des;
    private TextView tv_right_top_temp_des;
    private TextView tv_left_bottom_temp_des;
    private TextView tv_right_bottom_temp_des;

    // 最高胎压
    private float tpmsPreHight;
    // 最低胎压
    private float tpmsPreLow;
    // 最高轮胎温度
    private int tpmsTempHight;
    // 胎压单位
    private int preUnit;
    // 轮胎温度单位
    private int temUnit;

    private TextView tv_lt_error;
    private TextView tv_rt_error;
    private TextView tv_lb_error;
    private TextView tv_rb_error;

    private RelativeLayout rl_lt;
    private RelativeLayout rl_rt;
    private RelativeLayout rl_lb;
    private RelativeLayout rl_rb;
    private ListView listView;
    private ErrorAdapter adapter;
    private boolean isTL;
    private boolean isTR;
    private boolean isBL;
    private boolean isBR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // requestWindowFeature(Window.FEATURE_NO_TITLE); //设置无标题
       // getWindow().setFlags(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);  //设置全屏
        ButterKnife.bind(this);
        initView();
        initData();

    }

    @Override
    protected void initView() {
        view = View.inflate(this, R.layout.activity_main, null);
        new TitleBuilder(view)
                .setTitleText("胎压")
                .setRightImage(R.drawable.vodeoset)
                //.setLeftImage(R.drawable.ic_title_back_left_bt)
               // .setLeftOnClickListener(new View.OnClickListener() {
                //    @Override
                 //   public void onClick(View v) {
                     //   finish();
                   // }
              //  })
                .setRightOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 跳转到胎压设置界面
                        intent2Activity(SettingActivity.class);
                    }
                }).build();
        setContentView(view);
        tv_left_top_pre = TextView.class.cast(view.findViewById(R.id.tv_left_top_pre));
        tv_left_top_temp = TextView.class.cast(view.findViewById(R.id.tv_left_top_temp));
        tv_right_top_pre = TextView.class.cast(view.findViewById(R.id.tv_right_top_pre));
        tv_right_top_temp = TextView.class.cast(view.findViewById(R.id.tv_right_top_temp));
        tv_left_bottom_pre = TextView.class.cast(view.findViewById(R.id.tv_left_bottom_pre));
        tv_left_bottom_temp = TextView.class.cast(view.findViewById(R.id.tv_left_bottom_temp));
        tv_right_bottom_pre = TextView.class.cast(view.findViewById(R.id.tv_right_bottom_pre));
        tv_right_bottom_temp = TextView.class.cast(view.findViewById(R.id.tv_right_bottom_temp));
        tv_left_top_pre_des = TextView.class.cast(view.findViewById(R.id.tv_left_top_pre_des));
        tv_right_top_pre_des = TextView.class.cast(view.findViewById(R.id.tv_right_top_pre_des));
        tv_left_bottom_pre_des = TextView.class.cast(view.findViewById(R.id.tv_left_bottom_pre_des));
        tv_right_bottom_pre_des = TextView.class.cast(view.findViewById(R.id.tv_right_bottom_pre_des));
        tv_left_top_temp_des = TextView.class.cast(view.findViewById(R.id.tv_left_top_temp_des));
        tv_right_top_temp_des = TextView.class.cast(view.findViewById(R.id.tv_right_top_temp_des));
        tv_left_bottom_temp_des = TextView.class.cast(view.findViewById(R.id.tv_left_bottom_temp_des));
        tv_right_bottom_temp_des = TextView.class.cast(view.findViewById(R.id.tv_right_bottom_temp_des));
        tv_lt_error = (TextView) view.findViewById(R.id.tv_lt_error);
        tv_lb_error = (TextView) view.findViewById(R.id.tv_lb_error);
        tv_rt_error = (TextView) view.findViewById(R.id.tv_rt_error);
        tv_rb_error = (TextView) view.findViewById(R.id.tv_rb_error);
        rl_lt = (RelativeLayout) view.findViewById(R.id.rl_lt);
        rl_rt = (RelativeLayout) view.findViewById(R.id.rl_rt);
        rl_lb = (RelativeLayout) view.findViewById(R.id.rl_lb);
        rl_rb = (RelativeLayout) view.findViewById(R.id.rl_rb);
        listView = (ListView) view.findViewById(R.id.listView);
    }


    @Override
    protected void initData() {
        isTL = isTR = isBL = isBR = false;
        adapter = new ErrorAdapter(this);
        listView.setAdapter(adapter);
        // 初始化串口通信的接收者
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Map map = new HashMap();
                String action = intent.getAction();
                if (Constant.SERIAL_TPMS_ACTION.equals(action)) {
                    // 处理代码
                    Bundle bundle = intent.getExtras();
                    int[] data = bundle.getIntArray(Constant.SERIAL_TMPA_DATA);
                    setDatas(data);
                }
            }
        };
    }


    /**
     * 设置数据
     *
     * @param rd
     */
    private void setDatas(int[] rd) {
        // 解析数据
        if (rd[0] == 1 && rd[1] == 255) {  //判断包头
            if (rd[2] == 0X05) {  //判断是否是胎压数据
                if (0xaa == rd[4] && 0xaa == rd[5] && 0xbb == rd[rd[3] + 2] && 0xbb == rd[rd[3] + 3]) {
                    switch (rd[6]) {
                        case 0xa1: {
                            if (0X01 == rd[10]) {   // 下胎状态
                                tv_lt_error.setVisibility(View.VISIBLE);
                                rl_lt.setVisibility(View.GONE);
                            } else if (0X00 == rd[10]) {
                                tv_lt_error.setVisibility(View.VISIBLE);
                                rl_lt.setVisibility(View.GONE);
                            } else {
                                tv_left_top_pre.setText(String.valueOf((rd[7] * 255 + rd[8] - 100) / 100f));
                                tv_left_top_temp.setText(String.valueOf(rd[9] - 70));
                                tv_lt_error.setVisibility(View.GONE);
                                rl_lt.setVisibility(View.VISIBLE);
                                addErrorInListView(rd[10], "左前轮", isTL);
                            }

                            if (0X01 == rd[15]) {
                                tv_rt_error.setVisibility(View.VISIBLE);
                                rl_rt.setVisibility(View.GONE);
                            } else if (0X00 == rd[15]) {
                                tv_rt_error.setVisibility(View.VISIBLE);
                                rl_rt.setVisibility(View.GONE);
                            } else {
                                tv_right_top_pre.setText(String.valueOf((rd[12] * 255 + rd[13] - 100) / 100f));
                                tv_right_top_temp.setText(String.valueOf(rd[14] - 70));
                                tv_rt_error.setVisibility(View.GONE);
                                rl_rt.setVisibility(View.VISIBLE);
                                addErrorInListView(rd[15], "右前轮", isTR);
                            }

                            if (0X01 == rd[20]) {
                                tv_lb_error.setVisibility(View.VISIBLE);
                                rl_lb.setVisibility(View.GONE);
                            } else if (0X00 == rd[20]) {
                                tv_lb_error.setVisibility(View.VISIBLE);
                                rl_lb.setVisibility(View.GONE);
                            } else {
                                tv_left_bottom_pre.setText(String.valueOf((rd[17] * 255 + rd[18] - 100) / 100f));
                                tv_left_bottom_temp.setText(String.valueOf(rd[19] - 70));
                                tv_lb_error.setVisibility(View.GONE);
                                rl_lb.setVisibility(View.VISIBLE);
                                addErrorInListView(rd[20], "左后轮", isBL);
                            }

                            if (0X01 == rd[25]) {
                                tv_rb_error.setVisibility(View.VISIBLE);
                                rl_rb.setVisibility(View.GONE);
                            } else if (0X00 == rd[25]) {
                                tv_rb_error.setVisibility(View.VISIBLE);
                                rl_rb.setVisibility(View.GONE);
                            } else {
                                tv_right_bottom_pre.setText(String.valueOf((rd[22] * 255 + rd[23] - 100) / 100f));
                                tv_right_bottom_temp.setText(String.valueOf(rd[24] - 70));
                                tv_rb_error.setVisibility(View.GONE);
                                rl_rb.setVisibility(View.VISIBLE);
                                addErrorInListView(rd[25], "右后轮", isBR);
                            }

                            break;
                        }
                        case 0xa2: {
                            break;
                        }
                        case 0xa3: {
                            break;
                        }
                        case 0xa4: {
                            break;
                        }
                        case 0xa5: {
                            break;
                        }
                        case 0xa6: {
                            break;
                        }
                        case 0xa7: {
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                }

            } else {
                Log.e(TAG, "胎压数据不合法");
            }
        }
    }

    @Override
    public void onResume() {
        registerSerialCommReceiver();
        super.onResume();
    }

    @Override
    public void onPause() {
        unRegisterSerialCommReceiver();
        super.onPause();
    }

    /**
     * 注册串口通信的广播
     */
    private void registerSerialCommReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constant.SERIAL_TPMS_ACTION);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    /**
     * 注销串口通信的广播
     */
    private void unRegisterSerialCommReceiver() {
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    public void onClick(View v) {

    }

    private String dealStringBuilder(StringBuilder sb) {
        String sbString = sb.toString();
        String str = "";
        if (sbString.contains(",")) {
            str = sbString.substring(0, sbString.length() - 1);
        }
        return str;
    }

    /**
     * 添加异常类到ListView中
     *
     * @param i    指定轮胎的位数
     * @param name 轮胎名称
     * @param b    是否已经添加过了
     */
    private void addErrorInListView(int i, String name, boolean b) {
        StringBuilder sb = new StringBuilder();
        if ((i & 0x0B) != 0) {  // 传感器异常
            sb.append("传感器异常,");
        }
        if ((i & 0x40) != 0) { // 急速漏气
            sb.append("急速漏气,");
        }
        if ((i & 0x80) != 0) {  // 轮胎电池电压过低
            sb.append("轮胎电池电压过低,");
        }
        if ((i & 0x20) != 0) { // 温度异常
            sb.append("温度异常,");
        }
        if ((i & 0x10) != 0) {  // 轮胎胎压异常
            sb.append("轮胎胎压异常,");
        }
        if (!TextUtils.isEmpty(sb.toString())) {
            if (!b) {
                ErrorBean bean = new ErrorBean();
                bean.setErrorName(name);
                bean.setErrorInfo(dealStringBuilder(sb));
                adapter.addErrorBean(bean);
                switch (name) {
                    case "左前轮": {
                        isTL = true;
                        break;
                    }
                    case "右前轮": {
                        isTR = true;
                        break;
                    }
                    case "左后轮": {
                        isBL = true;
                        break;
                    }
                    case "右后轮": {
                        isBR = true;
                        break;
                    }
                }
            }
        }
    }
}

package com.scblhkj.carluncher.tpms.common;

/**
 * Created by Leo on 2016/2/27.
 */
public interface Constant {
    String SERIAL_ACTION_RECEIVE = "com.scblhkj.chappie.carlauncher.receive.serialdata"; // 串口中接收数据指令的广播Action
    String SERIAL_TPMS_ACTION = "com.scblhkj.chappie.carlauncher.tpms.serialdata";  // 胎压反馈的数据Action
    String SERIAL_CMD = "SERIAL_CMD";
    String SERIAL_TMPA_DATA = "SERIAL_TMPA_DATA";
    String TMPS_FRAGMENT_SLIDER_PRESSURE_MAX = "tmps_fragment_slider_pressure_max"; //压力值上限
    String TMPS_FRAGMENT_SLIDER_PRESSURE_MIN = "tmps_fragment_slider_pressure_min";//压力下限
    String TMPS_FRAGMENT_SLIDER_TEMP_MAX = "tmps_fragment_slider_temp_max";//温度上限
}

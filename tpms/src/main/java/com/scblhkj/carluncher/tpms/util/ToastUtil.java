package com.scblhkj.carluncher.tpms.util;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

public class ToastUtil {

    private static Context context;

    public static void init(Application application) {
        context = application.getApplicationContext();
    }

    private static Toast mToast;

    /**
     * 显示Toast
     */
    public static void showToast(CharSequence text) {
        if (mToast == null) {
            mToast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(text);
            mToast.setDuration(Toast.LENGTH_SHORT);
        }
        mToast.show();
    }

}

package com.scblhkj.carluncher.tpms.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rey.material.widget.Button;
import com.scblhkj.carluncher.tpms.R;
import com.scblhkj.carluncher.tpms.common.Constant;
import com.scblhkj.carluncher.tpms.util.TitleBuilder;
import com.scblhkj.carluncher.tpms.util.ToastUtil;

/**
 * 发射器配对界面
 */
public class EmitterMatchingActivity extends BaseActivity implements View.OnClickListener {

    private Button bt_lt;
    private Button bt_rt;
    private Button bt_lb;
    private Button bt_rb;
    private BroadcastReceiver broadcastReceiver;
    private RelativeLayout rl_loading;
    private TextView bt_cancel;
    private boolean canClick = true;
    private static final String TAG = "胎压配对界面";
    private TextView bt_clean_id;
    private ImageView iv_loading;
    /**
     * 图片左转动画
     */
    private Animation rAnim;
    private TextView tv_lt_id;
    private TextView tv_rt_id;
    private TextView tv_lb_id;
    private TextView tv_rb_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        initData();
    }

    @Override
    protected void initView() {
        view = View.inflate(this, R.layout.activity_emitter_matching, null);
        new TitleBuilder(view)
                .setTitleText("发射器配对")
               // .setLeftImage(R.drawable.ic_title_back_left_bt)
                //.setLeftOnClickListener(new View.OnClickListener() {
                  //  @Override
                   // public void onClick(View view) {
                     //   finish();
                   // }
               // })
                .build();
        bt_lt = (Button) view.findViewById(R.id.bt_lt);
        bt_rt = (Button) view.findViewById(R.id.bt_rt);
        bt_lb = (Button) view.findViewById(R.id.bt_lb);
        bt_rb = (Button) view.findViewById(R.id.bt_rb);
        bt_cancel = (TextView) view.findViewById(R.id.bt_cancel);
        rl_loading = (RelativeLayout) view.findViewById(R.id.rl_loading);
        bt_clean_id = (TextView) view.findViewById(R.id.bt_clean_id);
        iv_loading = (ImageView) view.findViewById(R.id.iv_loading);
        tv_lt_id = (TextView) view.findViewById(R.id.tv_lt_id);
        tv_rt_id = (TextView) view.findViewById(R.id.tv_rt_id);
        tv_lb_id = (TextView) view.findViewById(R.id.tv_lb_id);
        tv_rb_id = (TextView) view.findViewById(R.id.tv_rb_id);
        setContentView(view);
    }


    /**
     * 初始化动画
     */
    private void initAnimation() {
        rAnim = AnimationUtils.loadAnimation(this, R.anim.right_image_rotate);
        LinearInterpolator lir = new LinearInterpolator();
        rAnim.setInterpolator(lir);
    }

    private void startLoadingAnimation() {
        iv_loading.startAnimation(rAnim);
    }


    @Override
    public void initData() {
        // 初始化串口通信的接收者
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (Constant.SERIAL_TPMS_ACTION.equals(action)) {
                    // 处理代码
                    Bundle bundle = intent.getExtras();
                    int[] data = bundle.getIntArray(Constant.SERIAL_TMPA_DATA);
                    for (int i = 0; i < data.length; i++) {
                        Log.e(TAG, "data[" + i + "] = " + data[i]);
                    }
                    dealDatas(data);
                }
            }
        };
        bt_lt.setOnClickListener(this);
        bt_rt.setOnClickListener(this);
        bt_lb.setOnClickListener(this);
        bt_rb.setOnClickListener(this);
        bt_clean_id.setOnClickListener(this);
        bt_cancel.setOnClickListener(this);
        initAnimation();
    }

    private void dealDatas(int[] rd) {
        // 解析数据
        if (rd[0] == 1 && rd[1] == 255) {  //判断包头
            if (rd[2] == 0X05) {  //判断是否是胎压数据
                if (0xaa == rd[4] && 0xaa == rd[5] && 0xbb == rd[rd[3] + 2] && 0xbb == rd[rd[3] + 3]) {
                    switch (rd[6]) {
                        case 0xa1: {
                            read4TyreID();
                            break;
                        }
                        case 0XA2: {  // 发送配对接收数据
                            switch (rd[7]) {
                                case 0XBB: {
                                    ToastUtil.showToast("配对指令下发成功");
                                    // hideLoadingView();
                                    break;
                                }
                                case 0X01: {   // 左前
                                    ToastUtil.showToast("左前轮胎配对成功");
                                    read4TyreID();
                                    hideLoadingView();
                                    break;
                                }
                                case 0X02: {   // 右前
                                    ToastUtil.showToast("右前轮胎配对成功");
                                    read4TyreID();
                                    hideLoadingView();
                                    break;
                                }
                                case 0X03: {  // 左后
                                    ToastUtil.showToast("左后轮胎配对成功");
                                    read4TyreID();
                                    hideLoadingView();
                                    break;
                                }
                                case 0X04: {    // 左后
                                    ToastUtil.showToast("左后轮胎配对成功");
                                    read4TyreID();
                                    hideLoadingView();
                                    break;
                                }
                                default: {
                                    ToastUtil.showToast("发射器配对失败");
                                    hideLoadingView();
                                    break;
                                }
                            }
                            break;
                        }
                        case 0XA3: {
                            if (rd[7] == 0XAA) {
                                tv_lt_id.setText("ID:" + String.valueOf(rd[8]) + String.valueOf(rd[9]) + String.valueOf(rd[10]) + String.valueOf(rd[11]));
                            } else {
                                tv_lt_id.setText("ID:" + 0);
                            }
                            if (rd[12] == 0XAA) {
                                tv_rt_id.setText("ID:" + String.valueOf(rd[13]) + String.valueOf(rd[14]) + String.valueOf(rd[15]) + String.valueOf(rd[16]));
                            } else {
                                tv_rt_id.setText("ID:" + 0);
                            }
                            if (rd[17] == 0XAA) {
                                tv_lb_id.setText("ID:" + String.valueOf(rd[18]) + String.valueOf(rd[19]) + String.valueOf(rd[20]) + String.valueOf(rd[21]));
                            } else {
                                tv_lb_id.setText("ID:" + 0);
                            }
                            if (rd[22] == 0XAA) {
                                tv_rb_id.setText(String.valueOf(rd[23]) + String.valueOf(rd[24]) + String.valueOf(rd[25]) + String.valueOf(rd[26]));
                            } else {
                                tv_rb_id.setText("ID:" + 0);
                            }
                            break;
                        }
                        case 0xa4: {
                            break;
                        }
                        case 0xa5: {
                            break;
                        }
                        case 0xa6: {
                            ToastUtil.showToast("清除配对ID成功");
                            hideLoadingView();
                            break;
                        }
                        case 0xa7: {
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                }

            } else {
                Log.e(TAG, "胎压数据不合法");
            }
        }
    }

    @Override
    public void onResume() {
        registerSerialCommReceiver();
        sendData2Port(buildSendBs((byte) 0XA1, (byte) 0X01));  // 请求一套完成的胎压数据
        super.onResume();
    }

    @Override
    public void onPause() {
        unRegisterSerialCommReceiver();
        super.onPause();
    }

    /**
     * 注册串口通信的广播
     */
    private void registerSerialCommReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constant.SERIAL_TPMS_ACTION);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    /**
     * 注销串口通信的广播
     */
    private void unRegisterSerialCommReceiver() {
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    // 显示加载布局
    private void showLoadingView() {
        canClick = false;
        rl_loading.setVisibility(View.VISIBLE);
        startLoadingAnimation();
    }

    // 隐藏加载布局
    private void hideLoadingView() {
        canClick = true;
        rl_loading.setVisibility(View.GONE);
        iv_loading.clearAnimation();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.bt_lt: {
                sendData2Port(buildSendBs((byte) 0XA2, (byte) 0X01));
                showLoadingView();
                break;
            }
            case R.id.bt_rt: {
                sendData2Port(buildSendBs((byte) 0XA2, (byte) 0X02));
                showLoadingView();
                break;
            }
            case R.id.bt_lb: {
                sendData2Port(buildSendBs((byte) 0XA2, (byte) 0X03));
                showLoadingView();
                break;
            }
            case R.id.bt_rb: {
                sendData2Port(buildSendBs((byte) 0XA2, (byte) 0X04));
                showLoadingView();
                break;
            }
            case R.id.bt_cancel: {
                hideLoadingView();
                break;
            }
            case R.id.bt_clean_id: {
                sendData2Port(buildSendBs((byte) 0XA6, (byte) 0X01));
                break;
            }
            default: {
                break;
            }
        }
    }

    /**
     * 向串口发送数据
     */
    private void sendData2Port(byte[] bs) {
        Intent dataIntent = new Intent();
        dataIntent.setAction(Constant.SERIAL_ACTION_RECEIVE);
        dataIntent.addCategory(Intent.CATEGORY_DEFAULT);
        Bundle bundle = new Bundle();
        bundle.putByteArray(Constant.SERIAL_CMD, bs);
        dataIntent.putExtras(bundle);
        sendBroadcast(dataIntent);
    }

    /**
     * 构建要发送的字节指令
     */
    private byte[] buildSendBs(byte b6, byte b7) {
        byte[] bs = new byte[10];
        bs[0] = (byte) 0X01;
        bs[1] = (byte) 0XFF;
        bs[2] = (byte) 0X05;
        bs[3] = (byte) 0X06;
        bs[4] = (byte) 0XAA;
        bs[5] = (byte) 0XAA;
        bs[6] = b6;
        bs[7] = b7;
        bs[8] = (byte) 0XBB;
        bs[9] = (byte) 0XBB;
        return bs;
    }

    /**
     * 配对完成后读取四个轮胎的ID值
     */
    private void read4TyreID() {
        sendData2Port(buildSendBs((byte) 0X03, (byte) 0X01));
    }

}

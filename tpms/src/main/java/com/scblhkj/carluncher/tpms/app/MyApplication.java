package com.scblhkj.carluncher.tpms.app;

import android.app.Application;

import com.scblhkj.carluncher.tpms.util.SpUtil;
import com.scblhkj.carluncher.tpms.util.ToastUtil;

/**
 * Created by Leo on 2016/2/29.
 */
public class MyApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        ToastUtil.init(this);
        SpUtil.init(this);
    }
}

package com.scblhkj.carluncher.tpms.bean;

/**
 * Created by Leo on 2016/3/3.
 * 胎压异常的数据适配器
 */
public class ErrorBean {

    private String errorName;
    private String errorInfo;

    public String getErrorName() {
        return errorName;
    }

    public void setErrorName(String errorName) {
        this.errorName = errorName;
    }

    public String getErrorInfo() {
        return errorInfo;
    }

    public void setErrorInfo(String errorInfo) {
        this.errorInfo = errorInfo;
    }
}

package com.scblhkj.carluncher.db;

import com.scblhkj.carluncher.domain.SerialDataBean;

import java.util.List;

/**
 * Created by Leo on 2015/12/21.
 * 胎压数据采集接口
 */
public interface SerialDBInterface {

    /**
     * 插入数据
     *
     * @param bean
     * @return
     */
    long inserData(SerialDataBean bean);

    /**
     * 查询数据库表中的前i条记录
     *
     * @param i
     * @return
     */
    List<SerialDataBean> queryDatas(int i);

    /**
     * 根据记录的_id值更新记录的上传状态值
     *
     * @param bean upload 0 表示上传完成
     *             -1 表示上传未成功 默认都是为未成功
     */
    int tpmsRecordUploadState(SerialDataBean bean, int upload);

    void addNewTable();
}

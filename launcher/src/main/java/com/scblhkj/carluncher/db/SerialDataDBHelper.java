package com.scblhkj.carluncher.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.utils.CommonUtil;
import com.scblhkj.carluncher.utils.SpUtil;
import com.scblhkj.carluncher.utils.ToastUtil;

/**
 * Created by Leo on 2015/12/21.
 * 高德地图GPS坐标采集存储数据库操作类
 */
public class SerialDataDBHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "cpserial.db";
    public final static int DB_VERSION = 1;
    // 标识
    public static final String ID = "_id";
    // 数据插入的时间戳
    public static final String TIMESTAMP = "timestamp";
    // 胎压原始
    public static final String ORIGINDATA = "origindata";
    // 是否上传服务器的标识
    public static final String UPLOAD = "upload";


    public SerialDataDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        long currentTimeMillis = System.currentTimeMillis();
        String tableName = "serial_" + CommonUtil.timestamp2Data(currentTimeMillis);
        SpUtil.saveString2SP(ChappieConstant.TEMP_SERIAL_TABLE_NAME, tableName);
        String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " (" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TIMESTAMP + " VARCHAR(32)," + ORIGINDATA + " VARCHAR(32)," + UPLOAD + " VARCHAR" + ")";
        sqLiteDatabase.execSQL(sql);
        ToastUtil.showToast("胎压采集数据库创建成功");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}

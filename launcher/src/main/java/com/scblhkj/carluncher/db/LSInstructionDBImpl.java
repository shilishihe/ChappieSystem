package com.scblhkj.carluncher.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.domain.LSInstructionBean;

import java.io.UnsupportedEncodingException;

/**
 * Created by Leo on 2015/12/29.
 * 长连接指令Dao方法实现
 */
public class LSInstructionDBImpl implements LSInstructionDBInterface {


    private LSInstructionDBHelper lsInstructionDBHelper;
    private SQLiteDatabase db;
    private String tableName;

    private static final String TAG = "GPS坐标记录DAO";

    /**
     * @param context 上下文对象
     */
    public LSInstructionDBImpl(Context context) {
        this.lsInstructionDBHelper = new LSInstructionDBHelper(context);
        this.db = lsInstructionDBHelper.getWritableDatabase();
        this.tableName = LSInstructionDBHelper.TABLENAME;
    }


    @Override
    public long inserData(LSInstructionBean bean) {
        ContentValues values = new ContentValues();
        values.put(LSInstructionDBHelper.TIMESTAMP, bean.getTimeStamp());
        values.put(LSInstructionDBHelper.STRUCTION, bean.getInstruction());
       // Log.e("info","LSinstrucionDBimpl>>new String(bean.getInstruction()>>>"+new String(bean.getInstruction()));
        long l = db.insert(tableName, LSInstructionDBHelper.TIMESTAMP, values);
      //  Log.e("info","values>>>"+values.get(LSInstructionDBHelper.STRUCTION));
        return l;

    }

    @Override
    public void deleteRecord(LSInstructionBean tempBean) {
        int r = db.delete(tableName, LSInstructionDBHelper.TIMESTAMP + " = ? ", new String[]{String.valueOf(tempBean.getTimeStamp())});
        if (r > 0) {
            if (ChappieCarApplication.isDebug)
            Log.e(TAG, "缓存的记录删除成功");
        }
    }

    public void clearRecord(){
        String sql="delete * form"+tableName;
        db.execSQL(sql);
        Log.e("info","缓存的所有记录已经清空");
    }
    @Override
    public LSInstructionBean getFirstRecord() {
        LSInstructionBean bean = new LSInstructionBean();
        String sql = "select * from " + LSInstructionDBHelper.TABLENAME + " limit 0,1";
        Cursor cursor = db.rawQuery(sql, new String[]{});
        while (cursor.moveToNext()) {
           // Log.e("info","cursor.getInt(0)>>"+cursor.getInt(0));
          //  Log.e("info", "cursor.getString(1)>>" + cursor.getString(1));
            bean.setTimeStamp(cursor.getInt(0));
            bean.setInstruction(cursor.getString(1));
           // Log.e("info", "getFirstRecord>bean.setInstruction>>>"+bean.getInstruction());
        }
        return bean;
    }
}

package com.scblhkj.carluncher.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.scblhkj.carluncher.utils.ToastUtil;

/**
 * Created by Leo on 2015/11/11.
 */
public class VideoFileDbHelper extends SQLiteOpenHelper {

    public final static String DB_NAME = "chappie.db";
    public final static int DB_VERSION = 1;
    public final static String TABLE_NAME = "videos";
    /**
     * 标识
     */
    public static final String ID = "_id";
    /**
     * 文件名称
     */
    public static final String FILE_NAME = "file_name";
    /**
     * 文件存储路径
     */
    public static final String FILE_PATH = "file_path";
    /**
     * 是否为锁存区域
     */
    public static final String IS_LOCK = "is_lock";
    /**
     * 录制开始时间
     */
    public static final String RECORD_TIME = "record_time";
    /**
     * 录制结束时间
     */
    public static final String END_TIME = "end_time";

    private static final String TAG = "VideoFileDbHelper";


    public VideoFileDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + FILE_NAME + " VARCHAR," + FILE_PATH + " VARCHAR," + IS_LOCK + " int," + RECORD_TIME + " VARCHAR," + END_TIME + " VARCHAR)";
        db.execSQL(sql);
        ToastUtil.showToast("数据库创建成功");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }


    //更新列
    public void updateColumn(SQLiteDatabase db, String oldColumn, String newColumn, String typeColumn) {
        try {
            db.execSQL("ALTER TABLE " + TABLE_NAME + " CHANGE " + oldColumn + " " + newColumn + " " + typeColumn);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

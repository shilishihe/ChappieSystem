package com.scblhkj.carluncher.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.utils.SpUtil;
import com.scblhkj.carluncher.utils.ToastUtil;

/**
 * Created by Leo on 2015/12/21.
 * 高德地图GPS坐标采集存储数据库操作类
 */
public class GaoDeGPSDBHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "cpgps.db";
    public final static int DB_VERSION = 1;
    // 数据插入的时间戳
    public static final String TIMESTAMP = "timestamp";
    // 经度
    public static final String LONGITUDE = "LONGITUDE";
    // 纬度
    public static final String LATITUDE = "LATITUDE";
    // 是否上传服务器的标识
    public static final String UPLOAD = "upload";


    public GaoDeGPSDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        long currentTimeMillis = System.currentTimeMillis() / 1000L;
        String tableName = "gps_" + currentTimeMillis;
        SpUtil.saveString2SP(ChappieConstant.TEMP_GPS_TABLE_NAME, tableName);
        String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " (" + TIMESTAMP + " INTEGER PRIMARY KEY AUTOINCREMENT," + LONGITUDE + " VARCHAR(32)," + LATITUDE + " VARCHAR(32)," + UPLOAD + " VARCHAR" + ")";
        sqLiteDatabase.execSQL(sql);
        ToastUtil.showToast("GPS采集数据库创建成功");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}

package com.scblhkj.carluncher.db;

import com.scblhkj.carluncher.domain.CityElevationBean;

import java.util.List;

/**
 * Created by Leo on 2016/3/1.
 * 城市海拔数据库接口
 */
public interface CityElevationDao {

    /**
     * 根据城市名称查出城市名称的记录集合
     *
     * @param cityName 城市名称
     */
    List<CityElevationBean> queryRecordByCityName(String cityName);



}

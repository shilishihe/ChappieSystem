package com.scblhkj.carluncher.db;

import com.scblhkj.carluncher.domain.LSInstructionBean;

/**
 * Created by Leo on 2015/12/29.
 */
public interface LSInstructionDBInterface {
    /**
     * 插入数据
     *
     * @param bean
     * @return
     */
    long inserData(LSInstructionBean bean);

    /**
     * 删除表中的第一条记录
     */
    void deleteRecord(LSInstructionBean bean);


    /**
     * 查询表中第一条记录
     */
    LSInstructionBean getFirstRecord();

}

package com.scblhkj.carluncher.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;

import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.domain.GpsRecordBean;
import com.scblhkj.carluncher.utils.SpUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Leo on 2015/12/21.
 * 高德地图坐标记录数据库操作实例类
 */
public class GaoDeGPSDBInterfaceImpl implements GaoDeGPSDBInterface {

    private GaoDeGPSDBHelper gaoDeGPSDBHelper;
    private SQLiteDatabase db;
    private String tableName;

    private static final String TAG = "GPS坐标记录DAO";

    /**
     * @param context 上下文对象
     */
    public GaoDeGPSDBInterfaceImpl(Context context) {
        this.gaoDeGPSDBHelper = new GaoDeGPSDBHelper(context);
        this.db = gaoDeGPSDBHelper.getWritableDatabase();
        this.tableName = SpUtil.getString2SP(ChappieConstant.TEMP_GPS_TABLE_NAME);
    }

    @Override
    public long inserData(GpsRecordBean bean) {
        ContentValues values = new ContentValues();
        values.put(GaoDeGPSDBHelper.TIMESTAMP, String.valueOf(bean.getTimestamp()));
        values.put(GaoDeGPSDBHelper.LONGITUDE, String.valueOf(bean.getLongitude()));
        values.put(GaoDeGPSDBHelper.LATITUDE, String.valueOf(bean.getLatitude()));
        values.put(GaoDeGPSDBHelper.UPLOAD, String.valueOf(bean.getUpload()));
        long l = db.insert(tableName, GaoDeGPSDBHelper.TIMESTAMP, values);
        if (ChappieCarApplication.isDebug)
        Log.e(TAG, "正在执行插入方法返回值为 = " + l);
        return l;
    }

    @Override
    public List<GpsRecordBean> queryDatas(int i) {
        List<GpsRecordBean> beans = new ArrayList<GpsRecordBean>();
        String sql = "select * from " + tableName + " order by " + GaoDeGPSDBHelper.TIMESTAMP + " asc LIMIT ? ";
        Cursor cursor = db.rawQuery(sql, new String[]{String.valueOf(i)});
        while (cursor.moveToNext()) {
            Integer _id = cursor.getInt(0);
            String timestamp = cursor.getString(1);
            String longitude = cursor.getString(2);
            String latitude = cursor.getString(3);
            String upload = cursor.getString(4);
            GpsRecordBean bean = new GpsRecordBean();
            bean.set_id(_id);
            bean.setTimestamp(Long.parseLong(timestamp));
            bean.setLongitude(Double.parseDouble(longitude));
            bean.setLatitude(Double.parseDouble(latitude));
            bean.setUpload(Integer.parseInt(upload));
            beans.add(bean);
        }
        cursor.close();
        return beans;
    }

    @Override
    public int gpsRecordUploadState(GpsRecordBean bean, int upload) {
        String whereClause = GaoDeGPSDBHelper.TIMESTAMP + " = ? ";
        ContentValues values = new ContentValues();
        values.put(GaoDeGPSDBHelper.UPLOAD, String.valueOf(upload));
        int i = db.update(tableName, values, whereClause, new String[]{String.valueOf(bean.getTimestamp())});
        return i;
    }

    /**
     * 关闭数据库
     */
    public void closeDb() {
        db.close();
        gaoDeGPSDBHelper.close();
    }

    /**
     * 新建数据库表
     */
    public void addNewTable() {
        long currentTimeMillis = System.currentTimeMillis() / 1000L;
        String tableName = "gps_" + currentTimeMillis;
        String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " (" + GaoDeGPSDBHelper.TIMESTAMP + " INTEGER PRIMARY KEY AUTOINCREMENT," + GaoDeGPSDBHelper.LONGITUDE + " VARCHAR," + GaoDeGPSDBHelper.LATITUDE + " VARCHAR," + GaoDeGPSDBHelper.UPLOAD + " VARCHAR" + ")";
        try {
            db.execSQL(sql);
            SpUtil.saveString2SP(ChappieConstant.TEMP_GPS_TABLE_NAME, String.valueOf(tableName));
            if (ChappieCarApplication.isDebug)
            Log.e(TAG, "新的GPS坐标记录表生成表名是" + tableName);
        } catch (Exception e) {
            if (ChappieCarApplication.isDebug)
            Log.e(TAG, "新建GPS表失败" + tableName);
        }
    }

    /**
     * 获取现在需要操作的表名
     */
    public void getUploadTableName() {
        long finalUploadTime = 100l; // 最后上传的时间按戳
        List<String> tableNames = new ArrayList<String>();
        String sql = "select tbl_name from sqlite_master order by rootpage asc";
        Cursor cursor = db.rawQuery(sql, new String[]{});
        while (cursor.moveToNext()) {
            String tableName = cursor.getString(0);
            tableNames.add(tableName);
        }
        cursor.close();
        long cTime;
        long nextTime;
        if (tableNames.size() == 1) { // 只有一张表
            tableNames.get(0);
        } else if (tableNames.size() > 1) { // 至少是两张表
            for (int i = 0; i < tableNames.size() - 1; i++) {
                // 截取表名转成时间戳
                cTime = df(tableNames.get(i));
                nextTime = df(tableNames.get(i + 1));
                if (finalUploadTime > cTime && finalUploadTime <= nextTime) { // 那么正在正在操作的表名就是 tableNames.get(i)

                }
            }
        }
        for (String time : tableNames) {

        }
    }

    private long df(String tableName) {
        tableName = tableName.substring(tableName.indexOf("_") + 1, tableName.length());
        Long tTime = Long.valueOf(tableName);
        return tTime;
    }


    /**
     * @param tableName 表名
     * @return
     */
    private long queryLastRecordTimestamp(String tableName) {
        String sql = " SELECT " + GaoDeGPSDBHelper.TIMESTAMP + " FROM " + tableName + " ORDER BY _id DESC LIMIT 1";
        String timestamp = null;
        Cursor cursor = db.rawQuery(sql, new String[]{});
        while (cursor.moveToNext()) {
            timestamp = cursor.getString(0);
        }
        if (TextUtils.isEmpty(timestamp)) {
            return 0;
        } else {
            return Long.parseLong(timestamp);
        }
    }

}

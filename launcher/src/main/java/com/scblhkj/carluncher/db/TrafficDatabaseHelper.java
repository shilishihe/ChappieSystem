package com.scblhkj.carluncher.db;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 流量监控数据库帮助类
 */
public class TrafficDatabaseHelper {
    // 数据库对象
    private SQLiteDatabase mSQLiteDatabase = null;
    // 数据库名名称
    private final static String DATABASE_NAME = "Traffic.db";
    // 表名
    private final static String TABLE_NAME = "Traffic";
    // 主键，ID
    private final static String TABLE_ID = "FlowID";
    // 程序名称
    // private final static String TABLE_PRO = "ProName";
    // 上行流量，单位byte
    private final static String TABLE_UPF = "UpFlow";
    // 下载流量，单位byte
    private final static String TABLE_DPF = "DownFlow";
    // 储存日期
    // 格式：YYYY-MM-DD HH:MM:SS
    private final static String TABLE_TIME = "Time";
    // 联网类型，有WIFI和GPRS
    private final static String TABLE_WEB = "WebType";
    // 数据库版本号
    private final static int DB_VERSION = 1;

    private Context mContext = null;

    // 创建表的语句，用于第一次创建数据库时，创建表
    private final static String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" + TABLE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + TABLE_UPF + " Long," + TABLE_DPF + " Long," + TABLE_WEB + " INTEGER," + TABLE_TIME + " DATETIME)";

    // 数据库adapter，用于创建数据库
    private DatabaseHelper mDatabaseHelper = null;

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DB_VERSION);
        }

        /*
         * 创建数据库 创建表
         */
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE);
        }

        /*
         * 数据库跟新删除表并重新创建新表
         */
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS notes");
            onCreate(db);
        }
    }

    /* 构造函数 获取context */
    public TrafficDatabaseHelper(Context context) {
        mContext = context;
    }

    // 打开数据库，返回数据库对象
    public void open() throws SQLException {
        mDatabaseHelper = new DatabaseHelper(mContext);
        mSQLiteDatabase = mDatabaseHelper.getWritableDatabase();
    }

    // 关闭数据库
    public void close() {
        mDatabaseHelper.close();
    }

    /* 插入一条数据 */
    public void insertData(long UpFlow, long DownFlow, int WebType, Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:MM");
        String dataString = sdf.format(date);
        String insertData = " INSERT INTO " + TABLE_NAME + " (" + TABLE_UPF + ", " + TABLE_DPF + "," + TABLE_WEB + "," + TABLE_TIME + " ) values(" + UpFlow + ", " + DownFlow + "," + WebType + "," + "datetime('" + dataString + "'));";
        mSQLiteDatabase.execSQL(insertData);

    }

    /**
     * 更新数据
     *
     * @param upFlow
     * @param downFlow
     * @param webType
     * @param date
     */
    public void updateData(long upFlow, long downFlow, int webType, Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dataString = sdf.format(date);
        String updateData = " UPDATE " + TABLE_NAME + " SET " + TABLE_UPF + "=" + upFlow + " , " + TABLE_DPF + "=" + downFlow + " WHERE " + TABLE_WEB + "=" + webType + " and " + TABLE_TIME + " like '" + dataString + "%'";
        mSQLiteDatabase.execSQL(updateData);
    }


    /**
     * 检测是否存在某条数据
     *
     * @param netType
     * @param date
     * @return
     */
    public Cursor check(int netType, Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dataString = sdf.format(date);
        Cursor mCursor = mSQLiteDatabase.query(TABLE_NAME, new String[]{TABLE_UPF + " AS upPro", TABLE_DPF + " AS dwPro"}, TABLE_WEB + "=" + netType + " and " + TABLE_TIME + " like '" + dataString + "%'", null, null, null, null, null);// date转化
        return mCursor;
    }


    /**
     * 查询今日流量数据
     *
     * @param year
     * @param month
     * @param day
     * @param netType
     * @return
     */
    public Cursor fetchDayFlow(int year, int month, int day, int netType) {
        StringBuffer date = new StringBuffer();
        date.append(String.valueOf(year) + "-");
        if (month < 10) {
            date.append("0" + String.valueOf(month) + "-");
        } else {
            date.append(String.valueOf(month) + "-");
        }
        if (day < 10) {
            date.append("0" + String.valueOf(day));
        } else {
            date.append(String.valueOf(day));
        }
        Cursor mCursor = mSQLiteDatabase.query(TABLE_NAME, new String[]{"sum(" + TABLE_UPF + ") AS sumUp", "sum(" + TABLE_DPF + ") as sumDw"}, TABLE_WEB + "=" + netType + " and " + TABLE_TIME + " LIKE '" + date.toString() + "%'", null, null, null, null, null);
        return mCursor;
    }

    /**
     * 查询每月流量 可用于月报表和月流量统计
     *
     * @param year
     * @param Month
     * @param netType
     * @return
     */
    public Cursor fetchMonthFlow(int year, int Month, int netType) {
        StringBuffer date = new StringBuffer();
        date.append(String.valueOf(year) + "-");
        if (Month < 10) {
            date.append("0" + String.valueOf(Month) + "-");
        } else {
            date.append(String.valueOf(Month) + "-");
        }
        Cursor mCursor = mSQLiteDatabase.query(TABLE_NAME, new String[]{"sum(" + TABLE_UPF + ") AS monthUp", "sum(" + TABLE_DPF + ") as monthDw"}, TABLE_WEB + "=" + netType + " and " + TABLE_TIME + " LIKE '" + date.toString() + "%'", null, null, null, null, null);
        return mCursor;
    }

    /**
     * 计算每天的上传流量
     *
     * @param netType
     * @param date
     * @return
     */
    public long getProFlowUp(int netType, Date date) {
        Cursor cur = check(netType, date);
        long UP = 0;
        if (cur.moveToNext()) {
            int up = cur.getColumnIndex("upPro");
            UP = cur.getLong(up);
        }
        cur.close();
        return UP;
    }

    /**
     * 计算每天的下载流量
     *
     * @param netType
     * @param date
     * @return
     */
    public long getProFlowDw(int netType, Date date) {
        Cursor cur = check(netType, date);
        long UP = 0;
        if (cur.moveToNext()) {
            int up = cur.getColumnIndex("dwPro");
            UP = cur.getLong(up);
        }
        cur.close();
        return UP;
    }

    /**
     * 计算每日的流量
     *
     * @param year
     * @param month
     * @param day
     * @param netType
     * @return
     */
    public long calculate(int year, int month, int day, int netType) {
        Cursor calCurso = fetchDayFlow(year, month, day, netType);
        long sum = 0;
        if (calCurso != null) {
            if (calCurso.moveToFirst()) {
                do {
                    int upColumn = calCurso.getColumnIndex("sumUp");
                    int dwColumn = calCurso.getColumnIndex("sumDw");
                    sum = calCurso.getLong(upColumn) + calCurso.getLong(dwColumn);
                } while (calCurso.moveToNext());
            }
        }
        return sum;
    }

    /**
     * 计算本周上传流量
     *
     * @param netType
     * @return
     */
    public long weekUpFloew(int netType) {
        Calendar date1 = Calendar.getInstance();
        date1.set(Calendar.DAY_OF_WEEK, date1.getFirstDayOfWeek());
        long flowUp = 0;
        for (int i = 0; i < 7; i++) {
            int y = date1.get(Calendar.YEAR);
            int m = date1.get(Calendar.MONTH) + 1;
            int d = date1.get(Calendar.DAY_OF_MONTH);
            flowUp += calculateUp(y, m, d, netType);
            date1.add(Calendar.DAY_OF_WEEK, 1);
        }
        return flowUp;
    }


    /**
     * 计算本周下载流量
     *
     * @param netType
     * @return
     */
    public long weekDownFloew(int netType) {
        Calendar date1 = Calendar.getInstance();// 得到现在的日期
        date1.set(Calendar.DAY_OF_WEEK, date1.getFirstDayOfWeek());// 将日期改为今天所在周的第一天
        long flowDw = 0;
        for (int i = 0; i < 7; i++) {
            int y = date1.get(Calendar.YEAR);
            int m = date1.get(Calendar.MONTH) + 1;
            int d = date1.get(Calendar.DAY_OF_MONTH);
            flowDw += calculateDw(y, m, d, netType);
            date1.add(Calendar.DAY_OF_WEEK, 1); // date1加一天
        }

        return flowDw;
    }

    /**
     * 计算每月上传流量
     *
     * @param year
     * @param Month
     * @param netType
     * @return
     */
    public long calculateUpForMonth(int year, int Month, int netType) {
        Cursor lCursor = fetchMonthFlow(year, Month, netType);
        long sum = 0;

        if (lCursor != null) {
            if (lCursor.moveToFirst()) {
                do {
                    int upColumn = lCursor.getColumnIndex("monthUp");
                    sum += lCursor.getLong(upColumn);
                } while (lCursor.moveToNext());
            }
            lCursor.close();
        }
        return sum;
    }

    /**
     * 计算每月下载流量
     *
     * @param year
     * @param Month
     * @param netType
     * @return
     */
    public long calculateDnForMonth(int year, int Month, int netType) {
        Cursor lCursor = fetchMonthFlow(year, Month, netType);
        long sum = 0;

        if (lCursor != null) {
            if (lCursor.moveToFirst()) {
                do {
                    int dwColumn = lCursor.getColumnIndex("monthDw");
                    sum += lCursor.getLong(dwColumn);
                } while (lCursor.moveToNext());
            }
            lCursor.close();
        }
        return sum;
    }

    /**
     * 计算某月的流量
     *
     * @param year
     * @param Month
     * @param netType
     * @return
     */
    public long calculateForMonth(int year, int Month, int netType) {
        Cursor lCursor = fetchMonthFlow(year, Month, netType);
        long sum;
        long monthSum = 0;

        if (lCursor != null) {
            if (lCursor.moveToFirst()) {
                do {
                    int upColumn = lCursor.getColumnIndex("monthUp");
                    int dwColumn = lCursor.getColumnIndex("monthDw");
                    sum = lCursor.getLong(upColumn) + lCursor.getLong(dwColumn);
                    monthSum += sum;
                } while (lCursor.moveToNext());
            }
            lCursor.close();
        }
        return monthSum;
    }

    /**
     * 计算每日的上传流量
     *
     * @param year
     * @param month
     * @param day
     * @param netType
     * @return
     */
    public long calculateUp(int year, int month, int day, int netType) {
        Cursor calCurso = fetchDayFlow(year, month, day, netType);
        long sum = 0;
        if (calCurso != null) {
            if (calCurso.moveToFirst()) {
                do {
                    int upColumn = calCurso.getColumnIndex("sumUp");
                    sum = calCurso.getLong(upColumn);
                } while (calCurso.moveToNext());
            }
        }
        return sum;
    }

    /**
     * 计算每日的下载流量
     *
     * @param year
     * @param month
     * @param day
     * @param netType
     * @return
     */
    public long calculateDw(int year, int month, int day, int netType) {
        Cursor calCurso = fetchDayFlow(year, month, day, netType);
        long sum = 0;
        if (calCurso != null) {
            if (calCurso.moveToFirst()) {
                do {
                    int dwColumn = calCurso.getColumnIndex("sumDw");
                    sum = calCurso.getLong(dwColumn);
                } while (calCurso.moveToNext());
            }
        }
        return sum;
    }


    /**
     * 清空数据
     */
    public void deleteAll() {
        mSQLiteDatabase.execSQL("DROP TABLE " + TABLE_NAME);
    }

    /**
     * 清空数据
     */
    public void clear() {
        mSQLiteDatabase.delete(TABLE_NAME, null, null);
    }
}

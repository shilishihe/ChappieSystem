package com.scblhkj.carluncher.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.scblhkj.carluncher.domain.CityElevationBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Leo on 2016/3/1.
 * <p/>
 * 城市海拔数据库接口
 */
public class CityElevationDaoImpl implements CityElevationDao {

    private final static String cityElevationDBPath = "data/data/com.scblhkj.carluncher/files/city_elevation.db";
    private static CityElevationDaoImpl instance;
    private static SQLiteDatabase database;

    private CityElevationDaoImpl() {
        database = SQLiteDatabase.openDatabase(cityElevationDBPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    public static CityElevationDaoImpl getInstance() {
        if (instance == null) {
            synchronized (CityElevationDaoImpl.class) {
                if (instance == null) {
                    instance = new CityElevationDaoImpl();
                }
            }
        }
        return instance;
    }


    @Override
    public List<CityElevationBean> queryRecordByCityName(String cityName) {
        Cursor cursor = database.query("cc_j_w_h", new String[]{" * "}, " city like ? ", new String[]{cityName + "%"}, null, null, null);
        List<CityElevationBean> list = new ArrayList<CityElevationBean>();
        while (cursor.moveToNext()) {
            CityElevationBean bean = new CityElevationBean();
            bean.setId(cursor.getInt(0));
            bean.setProvince(cursor.getString(1));
            bean.setCity(cursor.getString(2));
            bean.setArea(cursor.getString(3));
            bean.setX(cursor.getFloat(4));
            bean.setY(cursor.getFloat(5));
            bean.setH(cursor.getFloat(6));
            list.add(bean);
        }
        return list;
    }
}

package com.scblhkj.carluncher.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.domain.SerialDataBean;
import com.scblhkj.carluncher.utils.CommonUtil;
import com.scblhkj.carluncher.utils.SpUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Leo on 2015/12/22.
 * 胎压数据采集数据库方法实现
 */
public class SerialDBInterfaceImpl implements SerialDBInterface {

    private SerialDataDBHelper tpmsdbHelper;
    private SQLiteDatabase db;
    private String tableName;
    private static final String TAG = "TPMS记录DAO";

    public SerialDBInterfaceImpl(Context context) {
        this.tpmsdbHelper = new SerialDataDBHelper(context);
        this.db = tpmsdbHelper.getWritableDatabase();
        this.tableName = SpUtil.getString2SP(ChappieConstant.TEMP_SERIAL_TABLE_NAME);
    }


    @Override
    public long inserData(SerialDataBean bean) {
        ContentValues values = new ContentValues();
        values.put(SerialDataDBHelper.TIMESTAMP, String.valueOf(bean.getTimestamp()));
        values.put(SerialDataDBHelper.ORIGINDATA, String.valueOf(bean.getOriginData()));
        values.put(SerialDataDBHelper.UPLOAD, String.valueOf(bean.getUpload()));
        long l = db.insert(tableName, SerialDataDBHelper.ID, values);
     //   Log.e(TAG, "TPMS正在执行插入方法返回值为 = " + l);
        return l;
    }

    @Override
    public List<SerialDataBean> queryDatas(int i) {
        List<SerialDataBean> beans = new ArrayList<SerialDataBean>();
        String sql = "select * from " + tableName + " order by " + SerialDataDBHelper.TIMESTAMP + " asc LIMIT ? ";
        Cursor cursor = db.rawQuery(sql, new String[]{String.valueOf(i)});
        while (cursor.moveToNext()) {
            Integer _id = cursor.getInt(0);
            String timestamp = cursor.getString(1);
            String origindata = cursor.getString(2);
            String upload = cursor.getString(4);
            SerialDataBean bean = new SerialDataBean();
            bean.set_id(_id);
            bean.setTimestamp(Long.parseLong(timestamp));
            bean.setOriginData(origindata);
            bean.setUpload(Integer.parseInt(upload));
            beans.add(bean);
        }
        cursor.close();
        return beans;
    }

    @Override
    public int tpmsRecordUploadState(SerialDataBean bean, int upload) {
        String whereClause = SerialDataDBHelper.ID + " = ? ";
        ContentValues values = new ContentValues();
        values.put(SerialDataDBHelper.UPLOAD, String.valueOf(upload));
        int i = db.update(tableName, values, whereClause, new String[]{String.valueOf(bean.get_id())});
        return i;
    }

    @Override
    public void addNewTable() {
        long currentTimeMillis = System.currentTimeMillis() / 1000L;
        String tableName = "serial_" + CommonUtil.timestamp2Data(currentTimeMillis);



        String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " (" + SerialDataDBHelper.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + SerialDataDBHelper.TIMESTAMP + " VARCHAR(32)," + SerialDataDBHelper.ORIGINDATA + " VARCHAR(32)," + SerialDataDBHelper.UPLOAD + " VARCHAR" + ")";
        try {
            db.execSQL(sql);
            SpUtil.saveString2SP(ChappieConstant.TEMP_SERIAL_TABLE_NAME, String.valueOf(tableName));
  //          Log.e(TAG, "新的串口数据记录表生成表名是" + tableName);
        } catch (Exception e) {
  //          Log.e(TAG, "新建GPS表失败" + tableName);
        }
    }
}

package com.scblhkj.carluncher.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.scblhkj.carluncher.domain.OBDErrorCodeBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Leo on 2015/12/19.
 * OBD错误码数据库操作实现类
 */
public class OBDErrorCodeDaoImpl implements OBDErrorCodeDaoInterface {

    private final static String obdErrorDBPath = "data/data/com.scblhkj.carluncher/files/CarErrorCode.db";
    private static OBDErrorCodeDaoImpl instance;
    private static SQLiteDatabase database;

    private OBDErrorCodeDaoImpl() {
        database = SQLiteDatabase.openDatabase(obdErrorDBPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    public static OBDErrorCodeDaoImpl getInstance() {
        if (instance == null) {
            synchronized (OBDErrorCodeDaoImpl.class) {
                if (instance == null) {
                    instance = new OBDErrorCodeDaoImpl();
                }
            }
        }
        return instance;
    }

    @Override
    public List<OBDErrorCodeBean> queryOBDError(List<String> dtcs) {
        List<OBDErrorCodeBean> list = new ArrayList<OBDErrorCodeBean>();
        database.beginTransaction();
        try {
            for (String dtc : dtcs) {
                OBDErrorCodeBean bean = queryOBDError(dtc);
                list.add(bean);
            }
            database.setTransactionSuccessful(); //设置事务处理成功，不设置会自动回滚不提交
        } catch (Exception e) {
            throw new RuntimeException("根据故障集合查询故障详情发生异常错误");
        } finally {
            database.endTransaction(); //处理完成
        }
        return list;
    }


    /**
     * 指定的故障代码查询指定的故障详情
     *
     * @param dtc
     * @return
     */
    @Override
    public OBDErrorCodeBean queryOBDError(String dtc) {
        Cursor cursor = database.query("car_obderr", new String[]{"dtc", "flag", "ch", "en", "category", "meaning"}, " dtc = ? ", new String[]{dtc}, null, null, null);
        OBDErrorCodeBean bean = new OBDErrorCodeBean();
        while (cursor.moveToNext()) {
            bean.setDtc(cursor.getString(0));
            bean.setFlag(cursor.getString(1));
            bean.setCh(cursor.getString(2));
            bean.setEn(cursor.getString(3));
            bean.setCategory(cursor.getString(4));
            bean.setMeaning(cursor.getString(5));
        }
        return bean;
    }
}



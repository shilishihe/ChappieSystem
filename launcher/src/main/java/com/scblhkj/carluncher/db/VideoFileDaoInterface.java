package com.scblhkj.carluncher.db;

import com.scblhkj.carluncher.domain.Video;

import java.util.List;

/**
 * Created by Leo on 2015/11/12.
 */
public interface VideoFileDaoInterface {

    /**
     * 插入视频
     */
    long insertVideo(Video video);

    /**
     * 移出视频
     */
    int removeVideo(Video video);

    /**
     * 将某段视频标识为锁存状态
     */
    int lockVideo(Video video);

    /**
     * 释放视频锁
     */
    int releaseVideo(Video video);

    /**
     * 查询出数据库中的前多少条记录
     * @param rc 一次要删除的钱多少条记录
     * @return
     */
    public List<Video> getRecentRecordVideos(int rc);

}

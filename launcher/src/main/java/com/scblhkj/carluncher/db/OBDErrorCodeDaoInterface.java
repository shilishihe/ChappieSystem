package com.scblhkj.carluncher.db;

import com.scblhkj.carluncher.domain.OBDErrorCodeBean;

import java.util.List;

/**
 * Created by Leo on 2015/12/19.
 * OBD故障码数据库操作类接口
 */

public interface OBDErrorCodeDaoInterface {

    /**
     * 根据传入的错误码集合查询出所有对应的故障集合
     *
     * @param dtcs 故障码集合
     * @return
     */
    List<OBDErrorCodeBean> queryOBDError(List<String> dtcs);


    /**
     * 指定的故障代码查询指定的故障
     *
     * @param dtc 故障码
     * @return
     */
    OBDErrorCodeBean queryOBDError(String dtc);
}

package com.scblhkj.carluncher.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.scblhkj.carluncher.utils.ToastUtil;

/**
 * Created by Leo on 2015/12/29.
 * 长连接指令数据库帮助类
 */
public class LSInstructionDBHelper extends SQLiteOpenHelper {


    public static final String DB_NAME = "cpinstruction.db";
    public final static int DB_VERSION = 1;
    // 数据插入的时间戳
    public static final String TIMESTAMP = "timestamp";
    // 指令
    public static final String STRUCTION = "struction";
    // 表名
    public static final String TABLENAME = "struction";

    public LSInstructionDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql = "CREATE TABLE IF NOT EXISTS " + TABLENAME + " (" + TIMESTAMP + " INTEGER PRIMARY KEY AUTOINCREMENT," + STRUCTION + " VARCHAR(200) " + ")";
        sqLiteDatabase.execSQL(sql);
        ToastUtil.showToast("长连接指令数据库创建成功");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

}

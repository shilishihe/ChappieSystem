package com.scblhkj.carluncher.domain;

/**
 * Created by Leo on 2016/4/12.
 * 请求服务器返回的实体数据类型
 */
public class ResultBean {
    public String status;
    public String msg;
}

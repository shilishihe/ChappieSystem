package com.scblhkj.carluncher.domain;

import android.graphics.Bitmap;

/**
 * Created by Leo on 2016/1/11.
 * 文件管理界面中图片实体类
 */
public class FileManagerPicBean {

    private int picId; // 缩略图ID
    private String picName;  // 缩略图名称
    private String picFilePath;  // 缩略图路径
    private Bitmap picBitmap; // 图片缩略图

    public String getPicName() {
        return picName;
    }

    public String getPicFilePath() {
        return picFilePath;
    }

    public void setPicFilePath(String picFilePath) {
        this.picFilePath = picFilePath;
    }

    public void setPicName(String picName) {
        this.picName = picName;
    }

    public int getPicId() {
        return picId;
    }

    public void setPicId(int picId) {
        this.picId = picId;
    }

    public Bitmap getPicBitmap() {
        return picBitmap;
    }

    public void setPicBitmap(Bitmap picBitmap) {
        this.picBitmap = picBitmap;
    }
}

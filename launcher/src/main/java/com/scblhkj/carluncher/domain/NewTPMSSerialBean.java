package com.scblhkj.carluncher.domain;

import java.io.Serializable;

/**
 * Created by Leo on 2015/12/16.
 * 新胎压数据实体类
 */
public class NewTPMSSerialBean implements Serializable {

    private static final long serialVersionUID = 3L;

    private int[][] datass;
    private int flag;

    public int[][] getDatass() {
        return datass;
    }

    public void setDatass(int[][] datass) {
        this.datass = datass;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }
}

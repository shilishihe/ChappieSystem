package com.scblhkj.carluncher.domain;

import java.io.Serializable;

/**
 * Created by Leo on 2015/11/12.
 * 封装行车记录仪保存的视频
 */
public class Video implements Serializable {

    private String filePath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }


}




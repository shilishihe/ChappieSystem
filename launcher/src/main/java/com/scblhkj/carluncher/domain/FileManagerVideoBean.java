package com.scblhkj.carluncher.domain;

import android.graphics.Bitmap;

/**
 * Created by Leo on 2016/1/11.
 * 文件管理界面中的视频实体类
 */
public class FileManagerVideoBean {

    private int id;//缩略图id
    private String videoFileName; // 视频文件的名称
    private String videoFilePath; // 视频文件的路径
    private Bitmap videoFileImg;  // 视频文件的缩略图
    private long duration; // 视频时长

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getVideoFileName() {
        return videoFileName;
    }

    public void setVideoFileName(String videoFileName) {
        this.videoFileName = videoFileName;
    }

    public String getVideoFilePath() {
        return videoFilePath;
    }

    public void setVideoFilePath(String videoFilePath) {
        this.videoFilePath = videoFilePath;
    }

    public Bitmap getVideoFileImg() {
        return videoFileImg;
    }

    public void setVideoFileImg(Bitmap videoFileImg) {
        this.videoFileImg = videoFileImg;
    }
}

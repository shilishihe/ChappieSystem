package com.scblhkj.carluncher.domain;

/**
 * Created by Leo on 2015/12/22.
 * 胎压实体数据
 */
public class SerialDataBean {
    // 标识
    private int _id;
    // 单条数据插入数据库的时间点
    private long timestamp;
    // 单条数据是否上传的标识
    private int upload;
    // 胎压原始数据
    private String originData;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getUpload() {
        return upload;
    }

    public void setUpload(int upload) {
        this.upload = upload;
    }

    public String getOriginData() {
        return originData;
    }

    public void setOriginData(String originData) {
        this.originData = originData;
    }
}

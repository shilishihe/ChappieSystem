package com.scblhkj.carluncher.domain;

import java.io.Serializable;

/**
 * Created by Leo on 2016/2/22.
 * 高德地图GPS信息实体类
 */
public class GPSInfo implements Serializable {

    private double latitude; // 纬度
    private double longitude; // 经度
    private float speed;//车速
    private String country; // 国家
    private String address; // 地址
    private String city; // 城市

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}

package com.scblhkj.carluncher.domain;

/**
 * Created by Leo on 2016/4/18.
 * 行车报告实体类
 */
public class TripReportBean {

    private String titleString; // 标题文字，当前日期
    private String startTime; // 启动时间
    private String startLocation; // 启动地点
    private String hotCarTime; // 热车时间
    private String REDDCast; // 碳排放量
    private String endTime; // 结束时间
    private String endLocation; // 终点位置
    private String averageSpeedCase; // 平均速度
    private String totalScoreCast; // 总评分
    private String castTime; // 总耗时
    private String topSpeedCast; // 最高速
    private String totalWordCast; // 总评语
    private String castOil; // 油耗
    private String sharpSpeedCast; // 急加速次数

    public String getTitleString() {
        return titleString;
    }

    public void setTitleString(String titleString) {
        this.titleString = titleString;
    }

    public String getSharpSpeedCast() {
        return sharpSpeedCast;
    }

    public void setSharpSpeedCast(String sharpSpeedCast) {
        this.sharpSpeedCast = sharpSpeedCast;
    }

    public String getCastOil() {
        return castOil;
    }

    public void setCastOil(String castOil) {
        this.castOil = castOil;
    }

    public String getTotalWordCast() {
        return totalWordCast;
    }

    public void setTotalWordCast(String totalWordCast) {
        this.totalWordCast = totalWordCast;
    }

    public String getTopSpeedCast() {
        return topSpeedCast;
    }

    public void setTopSpeedCast(String topSpeedCast) {
        this.topSpeedCast = topSpeedCast;
    }

    public String getCastTime() {
        return castTime;
    }

    public void setCastTime(String castTime) {
        this.castTime = castTime;
    }

    public String getTotalScoreCast() {
        return totalScoreCast;
    }

    public void setTotalScoreCast(String totalScoreCast) {
        this.totalScoreCast = totalScoreCast;
    }

    public String getAverageSpeedCase() {
        return averageSpeedCase;
    }

    public void setAverageSpeedCase(String averageSpeedCase) {
        this.averageSpeedCase = averageSpeedCase;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(String endLocation) {
        this.endLocation = endLocation;
    }

    public String getREDDCast() {
        return REDDCast;
    }

    public void setREDDCast(String REDDCast) {
        this.REDDCast = REDDCast;
    }

    public String getHotCarTime() {
        return hotCarTime;
    }

    public void setHotCarTime(String hotCarTime) {
        this.hotCarTime = hotCarTime;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
}

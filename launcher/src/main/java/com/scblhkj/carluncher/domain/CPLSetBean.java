package com.scblhkj.carluncher.domain;

import java.io.Serializable;

/**
 * Created by Leo on 2016/4/8.
 * 查派桌面设置项实体类
 */
public class CPLSetBean implements Serializable {

    private static final long serialVersionUID = -7060210544600464481L;
    private String privacyPassword;  // 隐私密码
    private boolean privacyddvr; // 隐私设置中行车记录仪
    private boolean privacyTpms;// 隐私设置中胎压
    private boolean privacyObd; // 隐私设置中obd
    private boolean privacyFile; // 隐私设置中文件
    private boolean privacyPaw; // 隐私密码开关
    private boolean setRemoteControl; // 远程控制开关
    private boolean setRravelingTrack; // 行驶轨迹开关

    public boolean isSetRemoteControl() {
        return setRemoteControl;
    }

    public void setSetRemoteControl(boolean setRemoteControl) {
        this.setRemoteControl = setRemoteControl;
    }

    public boolean isSetRravelingTrack() {
        return setRravelingTrack;
    }

    public void setSetRravelingTrack(boolean setRravelingTrack) {
        this.setRravelingTrack = setRravelingTrack;
    }

    public boolean isPrivacyPaw() {
        return privacyPaw;
    }

    public void setPrivacyPaw(boolean privacyPaw) {
        this.privacyPaw = privacyPaw;
    }

    public String getPrivacyPassword() {
        return privacyPassword;
    }

    public void setPrivacyPassword(String privacyPassword) {
        this.privacyPassword = privacyPassword;
    }

    public boolean isPrivacyddvr() {
        return privacyddvr;
    }

    public void setPrivacyddvr(boolean privacyddvr) {
        this.privacyddvr = privacyddvr;
    }

    public boolean isPrivacyTpms() {
        return privacyTpms;
    }

    public void setPrivacyTpms(boolean privacyTpms) {
        this.privacyTpms = privacyTpms;
    }

    public boolean isPrivacyObd() {
        return privacyObd;
    }

    public void setPrivacyObd(boolean privacyObd) {
        this.privacyObd = privacyObd;
    }

    public boolean isPrivacyFile() {
        return privacyFile;
    }

    public void setPrivacyFile(boolean privacyFile) {
        this.privacyFile = privacyFile;
    }

    @Override
    public String toString() {
        return "CPLSetBean{" +
                "privacyPassword='" + privacyPassword + '\'' +
                ", privacyddvr=" + privacyddvr +
                ", privacyTpms=" + privacyTpms +
                ", privacyObd=" + privacyObd +
                ", privacyFile=" + privacyFile +
                '}';
    }
}

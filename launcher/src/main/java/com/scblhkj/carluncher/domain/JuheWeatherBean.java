package com.scblhkj.carluncher.domain;

import java.util.List;

/**
 * Created by Leo on 2016/1/8.
 * 聚合天气数据(免费)
 */
public class JuheWeatherBean {


    public String error_code;
    public String reason;
    public Result result;

    @Override
    public String toString() {
        return "JuheWeatherBean{" +
                "error_code='" + error_code + '\'' +
                ", reason='" + reason + '\'' +
                ", result=" + result.toString() +
                '}';
    }

    public static class Result {
        public Data data;

        @Override
        public String toString() {
            return "Result{" +
                    "data=" + data.toString() +
                    '}';
        }
    }

    public static class Data {
        public String isForeign;
        public Life life;
        public PM pm25;
        public RealTime realtime;
        public List<Weather> weather;

        @Override
        public String toString() {
            return "Data{" +
                    "isForeign='" + isForeign + '\'' +
                    ", life=" + life.toString() +
                    ", pm25=" + pm25.toString() +
                    ", realtime=" + realtime.toString() +
                    ", weather=" + weather.toString() +
                    '}';
        }
    }

    public static class Life {
        public String date;
        public LifeInfo info;

        @Override
        public String toString() {
            return "Life{" +
                    "date='" + date + '\'' +
                    ", info=" + info.toString() +
                    '}';
        }
    }

    public static class LifeInfo {
        public List<String> chuanyi;
        public List<String> ganmao;
        public List<String> kongtiao;
        public List<String> wuran;
        public List<String> xiche;
        public List<String> yundong;
        public List<String> ziwaixian;

        @Override
        public String toString() {
            return "LifeInfo{" +
                    "chuanyi=" + chuanyi +
                    ", ganmao=" + ganmao +
                    ", kongtiao=" + kongtiao +
                    ", wuran=" + wuran +
                    ", xiche=" + xiche +
                    ", yundong=" + yundong +
                    ", ziwaixian=" + ziwaixian +
                    '}';
        }
    }

    public static class PM {
        public String cityName;
        public String dateTime;
        public String key;
        public PMs pm25;
        public String show_desc;

        @Override
        public String toString() {
            return "PM{" +
                    "cityName='" + cityName + '\'' +
                    ", dateTime='" + dateTime + '\'' +
                    ", key='" + key + '\'' +
                    ", pm25=" + pm25 +
                    ", show_desc='" + show_desc + '\'' +
                    '}';
        }
    }

    public static class PMs {
        public String curPm;
        public String des;
        public String level;
        public String pm10;
        public String pm25;
        public String quality;
    }

    public static class RealTime {
        public String city_code;
        public String city_name;
        public String dataUptime;
        public String data;
        public String moon;
        public String time;
        public RealWeather weather;
        public RealWind wind;
        public String week;

        @Override
        public String toString() {
            return "RealTime{" +
                    "city_code='" + city_code + '\'' +
                    ", city_name='" + city_name + '\'' +
                    ", dataUptime='" + dataUptime + '\'' +
                    ", data='" + data + '\'' +
                    ", moon='" + moon + '\'' +
                    ", time='" + time + '\'' +
                    ", weather=" + weather.toString() +
                    ", wind=" + wind.toString() +
                    ", week='" + week + '\'' +
                    '}';
        }
    }

    public static class RealWeather {
        public String humidity;
        public String img;
        public String info;
        public String temperature;

        @Override
        public String toString() {
            return "RealWeather{" +
                    "humidity='" + humidity + '\'' +
                    ", img='" + img + '\'' +
                    ", info='" + info + '\'' +
                    ", temperature='" + temperature + '\'' +
                    '}';
        }
    }

    public static class RealWind {
        public String direct;
        public String power;
        public String windspeed;
    }


    public static class Weather {
        public String date;
        public WeatherInfo info;
        public String nongli;
        public String week;

        @Override
        public String toString() {
            return "Weather{" +
                    "date='" + date + '\'' +
                    ", info=" + info.toString() +
                    ", nongli='" + nongli + '\'' +
                    ", week='" + week + '\'' +
                    '}';
        }
    }

    public static class WeatherInfo {
        public List<String> day;
        public List<String> night;

        @Override
        public String toString() {
            return "WeatherInfo{" +
                    "day=" + day +
                    ", night=" + night +
                    '}';
        }
    }
}

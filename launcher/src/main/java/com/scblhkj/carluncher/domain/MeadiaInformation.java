package com.scblhkj.carluncher.domain;

import android.graphics.Bitmap;

/**
 * Created by blh on 2016-10-24.
 */
public class MeadiaInformation {
    //图片路径
    public String srcPath;
    //图片：type=0 视频:type=1
    public int type;
    //bitmap资源图片
    public Bitmap bitmap;
}

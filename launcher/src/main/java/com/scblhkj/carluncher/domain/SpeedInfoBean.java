package com.scblhkj.carluncher.domain;

/**
 * Created by blh on 2016-09-23.
 */
public class SpeedInfoBean {
    private int speed;

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}

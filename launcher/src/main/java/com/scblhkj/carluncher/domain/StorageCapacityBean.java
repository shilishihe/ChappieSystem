package com.scblhkj.carluncher.domain;

/**
 * Created by Leo on 2016/1/23.
 * 存储容量实体类
 */
public class StorageCapacityBean {

    // 手机内部视频占用内存
    private long pInerVideoS;
    // 手机内部图片占用内存
    private long pInerPicS;
    // 手机内部总内存
    private long pInerTotleS;
    // 手机内部剩余内存
    private long pInerAvalibleS;
    // 手机内部其他内存
    private long pInerOherS;
    // 后视镜外置TF卡视频占用内存
    private long pOuterVideoS;
    // 后视镜外置TF卡图片占用内存
    private long pOuterPicS;
    // 后视镜外置TF卡总内存
    private long pOuterTotalS;
    // 后视镜外置TF卡剩余内存
    private long pOuterAvalibleS;
    // 后视镜外置TF卡其他内存
    private long pOutOthers;

    private String rm_total_storage;
    private String rm_use_storage;
    private String sd_total_storage;
    private String sd_use_storage;
    private String use_traffic;
    private String surplus_traffic;
    private String every_day_traffic;

    public long getpOutOthers() {
        return pOutOthers;
    }

    public void setpOutOthers(long pOutOthers) {
        this.pOutOthers = pOutOthers;
    }

    public long getpOuterVideoS() {
        return pOuterVideoS;
    }

    public void setpOuterVideoS(long pOuterVideoS) {
        this.pOuterVideoS = pOuterVideoS;
    }

    public long getpOuterPicS() {
        return pOuterPicS;
    }

    public void setpOuterPicS(long pOuterPicS) {
        this.pOuterPicS = pOuterPicS;
    }

    public long getpOuterTotalS() {
        return pOuterTotalS;
    }

    public void setpOuterTotalS(long pOuterTotalS) {
        this.pOuterTotalS = pOuterTotalS;
    }

    public long getpOuterAvalibleS() {
        return pOuterAvalibleS;
    }

    public void setpOuterAvalibleS(long pOuterAvalibleS) {
        this.pOuterAvalibleS = pOuterAvalibleS;
    }

    public String getRm_total_storage() {
        return rm_total_storage;
    }

    public void setRm_total_storage(String rm_total_storage) {
        this.rm_total_storage = rm_total_storage;
    }

    public String getRm_use_storage() {
        return rm_use_storage;
    }

    public void setRm_use_storage(String rm_use_storage) {
        this.rm_use_storage = rm_use_storage;
    }

    public String getSd_use_storage() {
        return sd_use_storage;
    }

    public void setSd_use_storage(String sd_use_storage) {
        this.sd_use_storage = sd_use_storage;
    }

    public String getSd_total_storage() {
        return sd_total_storage;
    }

    public void setSd_total_storage(String sd_total_storage) {
        this.sd_total_storage = sd_total_storage;
    }

    public String getUse_traffic() {
        return use_traffic;
    }

    public void setUse_traffic(String use_traffic) {
        this.use_traffic = use_traffic;
    }

    public String getSurplus_traffic() {
        return surplus_traffic;
    }

    public void setSurplus_traffic(String surplus_traffic) {
        this.surplus_traffic = surplus_traffic;
    }

    public String getEvery_day_traffic() {
        return every_day_traffic;
    }

    public void setEvery_day_traffic(String every_day_traffic) {
        this.every_day_traffic = every_day_traffic;
    }

    public long getpInerVideoS() {
        return pInerVideoS;
    }

    public void setpInerVideoS(long pInerVideoS) {
        this.pInerVideoS = pInerVideoS;
    }

    public long getpInerPicS() {
        return pInerPicS;
    }

    public void setpInerPicS(long pInerPicS) {
        this.pInerPicS = pInerPicS;
    }

    public long getpInerTotleS() {
        return pInerTotleS;
    }

    public void setpInerTotleS(long pInerTotleS) {
        this.pInerTotleS = pInerTotleS;
    }

    public long getpInerAvalibleS() {
        return pInerAvalibleS;
    }

    public void setpInerAvalibleS(long pInerAvalibleS) {
        this.pInerAvalibleS = pInerAvalibleS;
    }

    public long getpInerOherS() {
        return pInerOherS;
    }

    public void setpInerOherS(long pInerOherS) {
        this.pInerOherS = pInerOherS;
    }
}

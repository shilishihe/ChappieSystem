package com.scblhkj.carluncher.domain;

import java.util.List;

/**
 * Created by Leo on 2015/12/17.
 * OBD故障详情
 */
public class OBDFaultBean {

    public String faultTime; // 故障时间
    public List<Fault> faults; // 故障详情列表

    public static class Fault { // 故障
        public String faultDes;
        public int faultState;
    }

}

package com.scblhkj.carluncher.domain;

/**
 * Created by Leo on 2015/12/19.
 * 封装定位数据实体类
 */
public class GpsRecordBean {

    // 记录ID值
    private int _id;
    // 经度
    private double longitude;
    // 纬度
    private double latitude;
    // 单条数据插入数据库的时间点
    private long timestamp;
    // 单条数据是否上传的标识
    private int upload;
    private float speed;//速度

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getUpload() {
        return upload;
    }

    public void setUpload(int upload) {
        this.upload = upload;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }
}

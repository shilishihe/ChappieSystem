package com.scblhkj.carluncher.domain;

/**
 * Created by Leo on 2016/1/4.
 * OBD故障码实体类
 */
public class OBDErrorCodeBean {

    private String dtc; // 故障代码
    private String flag;  // 比如：该故障码适用于所有汽车制造商
    private String ch;    // 中文的故障概述
    private String en;    // 英文的故障概述
    private String category;  // 故障的所属类别
    private String meaning;   // 故障的详情解释

    public String getDtc() {
        return dtc;
    }

    public void setDtc(String dtc) {
        this.dtc = dtc;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCh() {
        return ch;
    }

    public void setCh(String ch) {
        this.ch = ch;
    }

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }


    @Override
    public String toString() {
        return "OBDErrorCodeBean{" +
                "dtc='" + dtc + '\'' +
                ", flag='" + flag + '\'' +
                ", ch='" + ch + '\'' +
                ", en='" + en + '\'' +
                ", category='" + category + '\'' +
                ", meaning='" + meaning + '\'' +
                '}';
    }
}

package com.scblhkj.carluncher.domain;

/**
 * Created by Leo on 2016/1/12.
 * 文件管理器类
 */
public class FileManagerFolderBean {

    private String folderName;
    private String folderPath;

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }
}

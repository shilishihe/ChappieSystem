package com.scblhkj.carluncher.domain;

/**
 * Created by Leo on 2016/1/16.
 * 我的汽车数据实体类
 */
public class MyCarInfoBean {

    private String title;  // 标题
    private String des;    // 标题对应的内容详情

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }
}

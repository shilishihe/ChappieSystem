package com.scblhkj.carluncher.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Leo on 2016/1/20.
 * 远程服务中暴露被外部App使用的数据类型
 */
public class RemoteAmapInfoBean implements Parcelable {

    private String address;
    private double latitude;
    private double longitude;
    private String city;


    public RemoteAmapInfoBean() {
    }

    protected RemoteAmapInfoBean(Parcel in) {
        address = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        city = in.readString();
    }

    public void readFromParcel(Parcel in) {
        address = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        city = in.readString();
    }

    public static final Creator<RemoteAmapInfoBean> CREATOR = new Creator<RemoteAmapInfoBean>() {
        @Override
        public RemoteAmapInfoBean createFromParcel(Parcel in) {
            return new RemoteAmapInfoBean(in);
        }

        @Override
        public RemoteAmapInfoBean[] newArray(int size) {
            return new RemoteAmapInfoBean[size];
        }
    };

    public String getAddress() {

        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(city);
    }


}

package com.scblhkj.carluncher.domain;

/**
 * Created by Leo on 2016/3/9.
 * 导航数据
 */
public class ENaviInfo {

    public String EDataType;
    public String SAPADist;
    public String SAPAType;
    public String cameraDist;
    public String cameraIndex;
    public String cameraSpeed;
    public String cameraType;
    public String carDirection;
    public String curLinkNum;
    public String curPointNum;
    public String curRoadName;
    public String curSegNum;
    public String icon;
    public String latitude;
    public String limitedSpeed;
    public String longitude;
    public String nextRoadName;
    public String routeRemainDis;
    public String routeRemainTime;
    public String segRemainDis;
    public String segRemainTime;
    public String type;


}

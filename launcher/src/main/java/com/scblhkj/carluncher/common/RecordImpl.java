package com.scblhkj.carluncher.common;

/**语音通话功能接口
 * Created by blh on 2016-11-09.
 */
public interface RecordImpl {
    /***
     * 录音准备阶段
     */
     public void readyRecord();

    /***
     * 录音开始阶段
     */
     public void startRecord();
    /***
     * 录音结束阶段
     */
    public void stopRecord();
    /***
     * 录音失败时删除旧的录音文件
     */
    public void deleteOldRecordFile();
    /***
     * 获取录音音量的大小
     */
    public double getAmplitude();

    /***
     * 获取录音文件的路径
     * @return
     */
    public String getVoicePath();
}

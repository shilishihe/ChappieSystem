package com.scblhkj.carluncher.common;


/**
 * Created by he on 2015/10/7.
 */
public interface ChappieConstant {

    /**
     * 串口波特率
     */
    int SERIAL_BAUDRATE = 19200;
    int NEW_SERIAL_BAUDRATE=9600;

    int SOCKET_CONN_PORT = 6688;
    /**
     * 长连接
     */
    String SOCKET_CONN_HOST = "120.27.142.237";
    /**
     * 普通的http请求
     */
    String HTTP_HOST = "120.27.144.186";
    String VOICE_HOST="120.27.142.237";
    String MESSAGE_ACTION = "com.chappie.message.action";
    String HEART_BEAT_ACTION = "com.chappie.heart_beat_action";
    // 默认心跳包的发送间隔
    int DATA_BEAT_TATE = 60 * 1000;
    // 默认轮询缓存数据发送时间间隔
    int DATA_CACHE_TATE = 10 * 1000;
    String LOCK_FILES = "LOCK_FILES";
    // 设备IMEI号
    String IMEI = "IMEI";
    /*****************
     * 胎压Fragment设置界面
     **********************/
    String TMPS_FRAGMENT_SLIDER_PRESSURE_MAX = "TMPS_FRAGMENT_SLIDER_PRESSURE_MAX"; //压力值上限
    String TMPS_FRAGMENT_SLIDER_PRESSURE_MIN = "TMPS_FRAGMENT_SLIDER_PRESSURE_MIN";//压力下限
    String TMPS_FRAGMENT_SLIDER_TEMP_MAX = "TMPS_FRAGMENT_SLIDER_TEMP_MAX";//温度上限
    String TMPS_FRAGMENT_SWITCH_ALARM = "TMPS_FRAGMENT_SWITCH_ALARM";//告警开关
    String TMPS_FRAGMENT_SWITCH_PRES_ALARM = "TMPS_FRAGMENT_SWITCH_PRES_ALARM";//告警开关
    String TPMS_FRAGMENT_UNIT_PRES = "TPMS_FRAGMENT_UNIT_PRES"; // 压力单位
    String TPMS_FRAGMENT_UNIT_TEMP = "TPMS_FRAGMENT_UNIT_TEMP"; // OBD单位
    String CMD = "SERIAL_CMD";
    String SERIAL_DATA_COMM_SERVICE_STATE = "SERIAL_DATA_COMM_SERVICE_STATE"; // 串口通信中存储串口状态;
    String OBD_SERIAL_ID = "OBD_SERIAL_ID"; // OBD产品序号
    String SEND_VIDEO_THREAD="com.scblhkj.chappie.SEND_VIDEO_THREAD";//配置视频流服务器
    String SEND_VIDEO_THREAD_SUC="com.scblhkj.chappie.SEND_VIDEO_THREAD_SUC";//配置视频流服务器成功
    String SEND_IS_VIDEO_THREAD_STATE="com.scblhkj.chappie.SEND_IS_VIDEO_THREAD_STATE";//是否是实时视频状态
    String TEMP_GPS_TABLE_NAME = "CURRENT_GPS_TABLE_NAME";  // 当前操作的GPS数据表
    String TEMP_SERIAL_TABLE_NAME = "TEMP_TPMS_TABLE_NAME";  // 当前操作的TPMS数据表
    String SEND_CAR_CMD_NET_PIC="com.scblhkj.chappie.hddvr.SEND_CAR_CMD_NET_PIC";
    String FINISH_NET_VIDEO_FILE="com.scblhkj.chappie.FINISH_NET_VIDEO_FILE";
    String SEND_CAR_COLLISION_VIDEO="com.scblhkj.chappie.hddvr.SEND_CAR_COLLISION_VID";//碰撞视频
    String SEND_CAR_COLLISION_VIDEO_PATH = "com.scblhkj.chappie.hddvr.SEND_CAR_COLLISION_VIDEO_PATH";//碰撞视频路径
    String NET_FV_END="com.scblhkj.cp.NET_FV_END";
    String NET_FV_START="com.scblhkj.cp.NET_FV_START";
    String SEND_CAR_COLLISION_PIC = "com.scblhkj.chappie.hddvr.SEND_CAR_COLLISION_PIC";  //发送事件图片广播
    String SEND_CAR_COLLISION_PIC_PATH = "com.scblhkj.chappie.hddvr.SEND_CAR_COLLISION_PIC_PATH"; // 要发送的图片的路径
    String FOR_REALTIME_SUCCESS="com.scblhkj.chappie.FRO_REALTIME_SUCCESS";
    String OFF_FOR_REALTIME_SUCCESS="com.scblhkj.chappie.OFF_FRO_REALTIME_SUCCESS";
    String SEND_CAR_LIMITED_SPEED="com.scblhkj.chappie.SEND_CAR_LIMITED_SPEED";//急减速告警
    String NET_NAME="com.scblhkj.chappie.SNET_NAME";//当前是否有网
    String SEND_VOICEFILE_PATH="com.scblhkj.chappie.SEND_VOICEFILE_PATH";//录音的音频文件路径
    String SCREEN_LIGHT="com.scblhkj.chappie.SCREEN_LIGHT";
    // 通知长连接服务查询数据库并上传数据广播
    String UPLOAD_CACHE_DATA_2_SERVER_ACTION = "UPLOAD_CACHE_DATA_2_SERVER_ACTION";
    // 后视镜录制状态
    int CAMERA_MAIN = 0; // 主摄像头
    int CAMERA_CECOND = 1;// 副摄像头


    // 报警参数
    String ALARM_Battery_Voltage_LOW = "ALARM_Battery_Voltage_LOW";  // 电瓶电压低
    String ALARM_OVERSPEED = "alarm_overspeed"; //超速报警
    String ALARM_FATIGUE_DRIVING = "alarm_fatigue_driving"; //疲劳驾驶
    String ALARM_CHARGING_CIRCUIT_ANOMALIES = "alarm_charging_circuit_anomalies"; //充电电路异常
    String ALARM_ECM_ERROR = "alarm_ecm_error"; // ECM异常
    String ALARM_ENGINE_COOLANT_HOT = "alarm_engine_coolant_hot"; // 冷却液温度过高
    String ALARM_ENGINE_COOLANT_COOL = "alarm_engine_coolant_cool"; // 冷却液温度过低
    String ALAEM_MAINTENANCE_ALERT = "alaem_maintenance_alert"; // 保养提醒
    String ALARM_CLEAN_REMIND_THROTTLE = "alarm_clean_remind_throttle"; // 节气门清洗提醒
    String ALARM_DEVICE_REMOVE = "alarm_device_remove";  // 设备拔掉提醒
    String ALARM_URGENT_ACCELERATE = "alarm_urgent_accelerate"; // 急加速提醒
    String ALARM_SHARP_SLOWDOWN = "alarm_sharp_slowdown";  // 急减速提醒
    String SEND_MODIFY_QUAKE_SUCCESS="com.scblhkj.chappie.hddvr.SEND_MODIFY_QUAKE_SUCCESS";//修改震动值成功
    String SEND_MODIFY_QUAKE_FAIL="com.scblhkj.chappie.hddvr.SEND_MODIFY_QUAKE_FAIL";//修改震动值失败
    String  SEND_MODIFY_SPEED_SUCCESS="com.scblhkj.chappie.SEND_MODIFY_SPEED_SUCCESS";//修改急减速成功
    String SEND_LIMITED_SOEED_HAPPEN="com.scblhkj.chappie.SEND_LIMITED_SOEED_HAPPEN";//发生急减速事件
    String SEND_CAR_LIMITED_PIC = "com.scblhkj.chappie.hddvr.SEND_CAR_LIMITED_PIC";  //发送急减速事件图片广播
    String SEND_CAR_LIMITED_PIC_PATH = "com.scblhkj.chappie.hddvr.SEND_CAR_LIMITED_PIC_PATH"; // 要发送的图片的路径
    String SEND_CAR_LIMITED_VID = "com.scblhkj.chappie.hddvr.SEND_CAR_LIMITED_VID";  //发送急减速事件视频广播
    String SEND_CAR_LIMITED_VID_PATH = "com.scblhkj.chappie.hddvr.SEND_CAR_LIMITED_VID_PATH"; // 要发送的视频的路径
    String SEND_START_APP_PACKAGENAME="com.scblhkj.chappie.SEND_START_APP_PACKAGENAME";//发送要启动的应用包名
    String SEND_START_APP_PACKAGENAME_SUCCESSS="com.scblhkj.chappie.SEND_START_APP_PACKAGENAME";//发送s设置启动的应用包名成功
    String SEND_SDCARD_STATE="com.scblhkj.chappie.SEND_SDCARD_STATE";//发送SD卡状态
    String SEND_IS_FINISH_SOCKET_ORDER="com.scblhkj.chappie.SEND_IS_FINISH_SOCKET_ORDER";
    String SEND_CLEAR_SOCKET_ORDER="com.scblhkj.chappie.SEND_IS_FINISH_SOCKET_ORDER";//清空网络请求的广播
    // 倒车信号广播Action
    String CAR_BACK_CUSTOM_KEY_EVENT = "android.intent.action.CUSTOM_KEY_EVENT";
    String CARBACKA = "CARBACKA"; // 进入倒车
    String CARBACKB = "CARBACKB"; // 退出倒车


    // GPS坐标信息
    String GPS_INFO = "android.intent.action.gps.info";

    /********************
     * 天气
     *****************************/
    String SUNNY = "晴"; // 需要区分晚上
    String CLOUDY = "多云"; // 需要区分晚上
    String YIN = "阴";
    String SHOWER = "阵雨";  // 需要区分晚上
    String THUNDER_SHOWER = "雷阵雨";
    String THUNDER_SHOWER_CLIP_HAIL = "雷阵雨并伴有冰雹";
    String SLEET = "雨夹雪";
    String LIGHT_RAIN = "小雨";
    String MODERATE_RAIN = "中雨";
    String HEAVY_RAIN = "大雨";
    String HEAVY_RAINS = "暴雨";
    String TORRENTIAL_RAIN = "大暴雨";
    String RAINSTORM = "特大暴雨";
    String SNOW_SHOWER = "阵雪";  // 需要区分晚上
    String LIGHT_SNOW = "小雪";
    String MODERATE_SNOW = "中雪";
    String HEAVY_SNOW = "大雪";
    String BLIZZARD = "暴雪";
    String FOG = "雾";
    String FREEZING_RAIN = "冻雨";
    String DUST_STORMS = "沙尘暴";
    String LIGHT_RAIN_MODERATE_RAIN = "小雨-中雨";
    String HEAVY_RAIN_HEAVY_RAIN = "中雨-大雨";
    String HEAVY_RAIN_HEAVY_RAINS = "大雨-暴雨";
    String HEAVY_RAINS_TORRENTIAL_RAIN = "暴雨-大暴雨";
    String TORRENTIAL_RAIN_RAINSTORM = "大暴雨-特大暴雨";
    String LIGHT_SNOW_MODERATE_SNOW = "小雪-中雪";
    String MODERATE_SNOW_HEAVY_SNOW = "中雪-大雪";
    String HEAVY_SNOW_BLIZZARD = "大雪-暴雪";
    String FLY_ASH = "浮尘";
    String MICROMETEOROLOGY = "扬沙";
    String STRONG_SANDSTORMS = "强沙尘暴";
    String HAZE = "霾";
    String SQUALL = "飑";
    String TORNADO = "龙卷风";
    String WEAK_BLOWING_SNOW = "弱高吹雪";
    String LIGHT_HAZE = "轻霾";

    // 聚合天气接口
    String JUHE_WEATHER_KEY = "6805e417318a246a9b7d8f12fddcc184";
    // 接口地址
    // http://op.juhe.cn/onebox/weather/query?cityname=%E6%B8%A9%E5%B7%9E&key=您申请的KEY
    String JUHE_WEATHER_HOST = "http://op.juhe.cn/onebox/weather/query?&key=" + JUHE_WEATHER_KEY + "" + "&cityname=";

    int MAX_ITEM_IN_ROW_POTRAIT = 3;
    int MAX_ITEM_IN_ROW_LANDSCAPE = 4;
    // 文件管理器类中传递视频路径
    String FILE_MANAGER_VIDEO_PATH = "FILE_MANAGER_VIDEO_PATH";


    String ACTION_ACC_INSERT = "android.intent.action.EXTERNAL_POWER_INSERT";  // ACC上电广播
    String ACTION_ACC_REMOVE = "android.intent.action.EXTERNAL_POWER_REMOVE";  // ACC下电广播
    String ACTION_INTENT_GSENSON_F5_EVENT = "android.intent.action.GSENSOR_F5_EVENT";  // 向右手势
    String ACTION_INTENT_GSENSON_F6_EVENT = "android.intent.action.GSENSOR_F6_EVENT";  // 向左手势

    /**
     * 手势开启或者是关闭
     */
    String ACTION_ENABLE_SENSORTEK = "com.sensortek.broadcast.enable"; // 使能手势
    String ACTION_DISABLE_SENSORTEK = "com.sensortek.broadcast.disable"; // 关闭手势
    /**
     * 视频录制参数
     */
    int VIDEO_ENCODING_BIT_RATE_NORMAL = 64;//视频编码比特率
    int VIDEO_ENCODING_BIT_RATE_LOW = 32;//视频编码比特率
    int VIDEO_ENCODING_BIT_RATE_HEIGH = 256;//视频编码比特率
    int VIDEO_RECORD_WIDTH_NORMAL = 1920; // 默认的视频录制宽度
    int VIDEO_RECORD_HEIGHT_NORMAL = 1080; // 默认的视频录制高度
    int VIDEO_RECORD_WIDTH_LOW = 1280; // 默认的视频录制宽度
    int VIDEO_RECORD_HEIGHT_LOW = 720; // 默认的视频录制高度

    /**
     * 后视镜系统设置项的KEY值
     */
    //Switch开关设置
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_GPS = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_GPS";    // GPS开关
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_RECORD_VOICE = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_RECORD_VOICE"; // 录像声音开关
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_VIBRATION_ANALYSIS = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_VIBRATION_ANALYSIS";  // 震动监测开关
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_REMOTE_CONTROL_MAIN_CAMERA = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_REMOTE_CONTROL_MAIN_CAMERA"; // 远程控制前置摄像头开关
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_REMOTE_CONTROL_SECOND_CAMERA = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_REMOTE_CONTROL_SECOND_CAMERA";  // 远程控制副摄像头开关
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_LOCATION_SHARED = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_LOCATION_SHARED";  // 定位分享开关
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_ADAS = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_ADAS"; // ADAS辅助驾驶快关
    //RadioGroup开关设置
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_VIBRATION_TIME_DELAY_SETTING = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_VIBRATION_TIME_DELAY_SETTING";  // 震动延时设置
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_VIBRATION_MONITORING_LEVEL = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_VIBRATION_MONITORING_LEVEL";  // 振动监测级别
    String CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_POWER_MODE_SETTINGS = "CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_POWER_MODE_SETTINGS";  // 功耗模式


    // -----------------------------------------
    String ACTION_REMOTE_COLLISION_SERVICE = "action_remote_collision_service";  // 行车记录仪设置中设置震动级别
    String ACTION_REMOTE_COLLISION_SERVICE_VALUE = "action_remote_collision_service_value"; // 带过去的设置值


    // ----流量类别
    int TRIFFICE_GPRS = 1;
    int gettriffice_wifi = 0;


    ////////////////////////////////高德地图
    String SEND_ACTION = "com.autonavi.minimap.carmode.send";
    String SEND_BUSINESS_ACTION = "send_business_action";
    String SEND_BUSINESS_DATA = "send_business_data";
    String SEND_LAUNCH_APP = "LAUNCH_APP"; // 程序启动
    String SEND_EXIT_APP = "EXIT_APP"; // 程序退出
    String SEND_OPEN_NAVI = "OPEN_NAVI"; //打开导航
    String SEND_CLOSE_NAVI = "CLOSE_NAVI"; // 关闭导航
    String SEND_PATH_FAIL = "PATH_FAIL"; // 路径规划失败
    String SEND_APP_FOREGROUND = "APP_FOREGROUND"; // 程序切到前台
    String SEND_APP_BACKGROUND = "APP_BACKGROUND"; // 程序隐藏到后台
    String SEND_NAVI_INFO = "NAVI_INFO"; // 导航信息

    /**
     * 全局设置存储类的Key
     */
    String CPSBK = "ChappieSetBeanKey";
    /**
     * 流量上传的时间点
     */
    String TRAFFIC_UPLOAD_DATA = "TRAFFIC_UPLOAD_DATA";

    /**
     * 蓝牙电话广播
     */
    String ACTION_BTCALL_CW = "com.concox.BLUETOOTH_CW";
    String ACTION_BTCALL_IF = "com.concox.BLUETOOTH_IF";
    String ACTION_BTCALL_CG = "com.concox.BLUETOOTH_CG";
    String ACTION_BTCALL_CE = "com.concox.BLUETOOTH_CE";
    String ACTION_BTCALL_MD = "com.concox.BLUETOOTH_MD";
    String MSG_CUSTOM_VOICESPEAK = "android.intent.action.CUSTOM_VOICE_SPEAK";

}

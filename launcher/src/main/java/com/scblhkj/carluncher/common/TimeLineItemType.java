package com.scblhkj.carluncher.common;

/**
 * Created by Leo on 2015/12/17.
 * 时间轴类型
 */
public class TimeLineItemType {
    public final static int NORMAL = 0;
    public final static int HEADER = 1;
    public final static int FOOTER = 2;
    public final static int START = 3;
    public final static int END = 4;
    public final static int ATOM = 5;
}
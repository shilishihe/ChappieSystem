package com.scblhkj.carluncher.manager.image;

/**
 * Created by Leo on 2016/5/4.
 * 图片加载器工厂类
 */
public class ImageLoaderFactory {

    public static ImageLoaderWrapper imageLoaderWrapper;

    private ImageLoaderFactory() {
    }

    /**
     * 获取对象
     */
    public static final ImageLoaderWrapper getImageLoaderWrapper() {
        if (imageLoaderWrapper == null) {
            synchronized (ImageLoaderFactory.class) {
                if (imageLoaderWrapper == null) {
                    imageLoaderWrapper = new ImageLoaderWrapperImpl();
                }
            }
        }
        return imageLoaderWrapper;
    }
}

package com.scblhkj.carluncher.manager.image;

import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;

/**
 * Created by Leo on 2016/5/4.
 */
public interface ImageLoaderWrapper {

    String ANDROID_RESOURCE = "android.resource://";
    String FOREWARD_SLASH = "/";

    /**
     * 将资源ID转成Uri
     *
     * @return
     */
    Uri resourceId2Uri(Context context, int resId);

    /**
     * 加载网络图片
     *
     * @param context
     * @param url
     * @param imageView
     */
    void loadUriImage(Context context, String url, ImageView imageView);

    /**
     * 加载Drawable图片
     *
     * @param context
     * @param resId
     * @param imageView
     */
    void loadResImage(Context context, int resId, ImageView imageView);

    /**
     * 加载本地图片
     *
     * @param context
     * @param path
     * @param imageView
     */
    void loadLocalImage(Context context, String path, ImageView imageView);

    /**
     * 加载网络圆形图片
     *
     * @param context
     * @param url
     * @param imageView
     */
    void loadCircleImage(Context context, String url, ImageView imageView);

    /**
     * 加载Drawable圆形图片
     *
     * @param context
     * @param resId
     * @param imageView
     */
    void loadCircleResImage(Context context, int resId, ImageView imageView);

    /**
     * 加载本地远程图片
     *
     * @param context
     * @param path
     * @param imageView
     */
    void loadCircleLocalImage(Context context, String path, ImageView imageView);
}

package com.scblhkj.carluncher.utils;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

import com.scblhkj.carluncher.receiver.LockReceiver;

/**
 * Created by Leo on 2016/3/4.
 * 屏幕电源管理器类
 */
public class ScreenPowerUtil {

    private static Context context;
    private static PowerManager pm;
    private static PowerManager.WakeLock mWakelock;
    private static final String TAG = "锁屏工具类";
    private static DevicePolicyManager policyManager;
    private static ComponentName componentName;


    public static void init(Context cox) {
        context = cox;
        pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        mWakelock = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.SCREEN_DIM_WAKE_LOCK, "SimpleTimer");
    }

    /**
     * 点亮屏幕
     */
    public static void doScreenOn() {
        if (!mWakelock.isHeld()) {
            mWakelock.acquire();
            Log.e(TAG, "电源锁Acquire");
        } else {
            Log.e(TAG, "电源锁已经被Acquire");
        }
    }

    /**
     * 熄灭屏幕
     */
    public static void doScreenOff() {
        if (mWakelock.isHeld()) {
            mWakelock.release();
            Log.e(TAG, "电源锁Release");
        } else {
            Log.e(TAG, "电源锁已经Release");
        }
        lockScreen();
    }

    private static void activeManager() {
        // 使用隐式意图调用系统方法来激活指定的设备管理器
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, componentName);
        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "一键锁屏");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Log.e(TAG, "发送激活设备的广播");
        context.startActivity(intent);
    }

    /**
     * 锁屏
     */
    public static void lockScreen() {
        policyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        componentName = new ComponentName(context, LockReceiver.class);
        if (policyManager.isAdminActive(componentName)) {// 判断是否有权限(激活了设备管理器)
            policyManager.lockNow();// 直接锁屏
            Log.e(TAG, "直接锁屏");
        } else {
            Log.e(TAG, "激活设备");
            activeManager();// 激活设备管理器获取权限
        }
    }

    public static boolean isHeld() {
        return mWakelock.isHeld();
    }

    public static void offHeld() {
        mWakelock.release();
    }

    public static void onHeld() {
        mWakelock.acquire();
    }

}

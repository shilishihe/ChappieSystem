package com.scblhkj.carluncher.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.StatFs;
import android.text.format.Formatter;
import android.util.Log;

import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.zhy.http.okhttp.request.RequestCall;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.Vector;

/**
 * Created by Leo on 2015/11/17.
 * 文件操作工具类
 */
public class FileUtil {

    private static final String TAG = "FileUtil";

    /**
     * @param file
     * @return
     */
    public static long getDirSize(File file) {
        // 判断文件是否存在
        if (file.exists()) {
            // 如果是目录则继续递归其内容的总大小
            if (file.isDirectory()) {
                File[] children = file.listFiles();
                long size = 0;
                for (File f : children) {
                    size += getDirSize(f);
                }
                return size;
            } else {
                long size = file.length();
                return size;
            }
        } else {
            if (ChappieCarApplication.isDebug)
                Log.e(TAG, "文件或文件夹不存在");
            return 0;
        }
    }

    // 获取当前目录下所有的mp4文件
    public static Vector<String> GetFileName(String fileAbsolutePath) {
        Vector<String> vecFile = new Vector<String>();
        StringBuffer sb=new StringBuffer();
        File file = new File(fileAbsolutePath);
        File[] subFile = file.listFiles();

        for (int iFileLength = 0; iFileLength < subFile.length; iFileLength++) {
            // 判断是否为文件夹
            if (!subFile[iFileLength].isDirectory()) {
                String filename = subFile[iFileLength].getName();
                vecFile.add(filename);
                sb.append(filename);
            }
        }

        return vecFile;}
    /**
     * 删除空目录
     */
    public static void doDeleteEmptyDir(String dir) {
        boolean success = (new File(dir)).delete();
        if (success) {
            System.out.println("Successfully deleted empty directory: " + dir);
        } else {
            System.out.println("Failed to delete empty directory: " + dir);
        }
    }
    /**
     * 读取某个文件夹下的所有文件
     */
    public static boolean readfile(String filepath) throws FileNotFoundException, IOException {
        try {

            File file = new File(filepath);
            if (!file.isDirectory()) {
                System.out.println("文件");
                Log.e("info", "文件");
               // System.out.println("path=" + file.getPath());
               // System.out.println("absolutepath=" + file.getAbsolutePath());
                System.out.println("name=" + file.getName());
                Log.e("info", "readfilename=" + file.getName());
            } else if (file.isDirectory()) {
                System.out.println("文件夹");
                Log.e("info", "文件夹");
                String[] filelist = file.list();
                for (int i = 0; i < filelist.length; i++) {
                    File readfile = new File(filepath + "\\" + filelist[i]);
                    if (!readfile.isDirectory()) {
                        //System.out.println("path=" + readfile.getPath());
                        //System.out.println("absolutepath=" + readfile.getAbsolutePath());
                        System.out.println("文件夹下name=" + readfile.getName());
                        Log.e("info", "文件夹下name=" + file.getName());
                    } else if (readfile.isDirectory()) {
                        readfile(filepath + "\\" + filelist[i]);
                    }
                }

            }

        } catch (FileNotFoundException e) {
            System.out.println("readfile()   Exception:" + e.getMessage());
        }
        return true;
    }

    /**
     * 将Bitmap转换成文件
     * 保存文件
     * @param bm
     * @param fileName
     * @throws IOException
     */
    public static File saveFile(Bitmap bm,String path, String fileName) throws IOException {
        File dirFile = new File(path);
        Log.e("info","Bitmap转换成文件，获取的PATH>>>>"+path);
        if(!dirFile.exists()){
            dirFile.mkdir();
        }
        File myCaptureFile = new File(path , fileName);
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(myCaptureFile));
        bm.compress(Bitmap.CompressFormat.JPEG, 60, bos);
        bos.flush();
        Log.e("info", "bos.flush();");
        bos.close();
        Log.e("info","图片压缩完毕，已生成图片file完毕");
        return myCaptureFile;
    }
    /**
     * 递归删除目录下的所有文件及子目录下所有文件
     *
     * @param dir 将要删除的文件目录
     * @return
     */
    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            //递归删除目录中的子目录下
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    /**
     * 修改视频文件的名称为锁存文件名
     *
     * @param
     * @return
     */
    public static String reNameFile(String originFilePath) {
        int i = originFilePath.lastIndexOf("/");
        String oldFileName = originFilePath.substring(i + 1, originFilePath.length());
        String newFileName = "lock_" + oldFileName;
        String basePath = originFilePath.substring(0, i);
        String finalFilePath = basePath + "/" + newFileName;
        File newfile = new File(finalFilePath);
        // 先判断是否有sd卡
        File oldFile = new File(originFilePath);
        if (oldFile.exists() && oldFile.isFile()) {  // 文件存在且是文件
            if (newfile.exists()) {
                if (ChappieCarApplication.isDebug)
                    Log.e(TAG, "锁存文件已经存在!!!");
            } else {
                oldFile.renameTo(newfile);
                if (ChappieCarApplication.isDebug)
                    Log.e(TAG, "碰撞视频锁存成功!!!");
            }
        }
        return newfile.getPath();
    }

    /**
     * @param context
     * @param fileName 将Assets路径下的.db文件拷贝到data/data/包名下
     */
    public static void copyAssetsDB2Data(Context context, String fileName) {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            File file = new File(context.getFilesDir(), fileName);
            if (file.exists() && file.length() > 0) {
                return;
            }
            inputStream = context.getAssets().open(fileName);
            outputStream = new FileOutputStream(file);
            int len = 0;
            byte[] buffer = new byte[1024];
            while ((len = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * 转换文件大小
     *
     * @param fileS
     * @return
     */
    public static String formetFileSize(long fileS) {
        DecimalFormat df = new DecimalFormat("#.00");
        String fileSizeString = "";
        String wrongSize = "0B";
        if (fileS == 0) {
            return wrongSize;
        }
        if (fileS < 1024) {
            fileSizeString = df.format((double) fileS) + "B";
        } else if (fileS < 1048576) {
            fileSizeString = df.format((double) fileS / 1024) + "KB";
        } else if (fileS < 1073741824) {
            fileSizeString = df.format((double) fileS / 1048576) + "MB";
        } else {
            fileSizeString = df.format((double) fileS / 1073741824) + "GB";
        }
        return fileSizeString;
    }

}
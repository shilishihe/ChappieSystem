package com.scblhkj.carluncher.utils;

import android.media.MediaPlayer;
import android.media.MediaRecorder;

import com.scblhkj.carluncher.common.RecordImpl;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**语音通话功能的具体实现
 * Created by blh on 2016-11-09.
 */
public class AudioRecord implements RecordImpl {
    private MediaRecorder recorder;
    private String fileName;
    private String fileFolder="/storage/sdcard1/audiorecord";
    private boolean isAudioRecord=false;

    @Override
    public void readyRecord() {
         if(SDCardScannerUtil.isSecondSDcardMounted()){
             File file=new File(fileFolder);
             if(!file.exists()){
                 file.mkdir();
             }
             fileName=getCurrentTime();
             recorder=new MediaRecorder();
             recorder.setOutputFile(fileFolder+"/"+fileName+".mp3");
             recorder.setAudioSource(MediaRecorder.AudioSource.MIC);//设置音频来源为麦克风
             recorder.setOutputFormat(MediaRecorder.OutputFormat.RAW_AMR);//设置MediaRecorder录制的音频格式
             recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);//设置MediaRecorder录制音频的编码为amr
         }else {
             ToastUtil.showToast("没有检测到SD卡,不能进入该功能");
             return;
         }
    }

    @Override
    public void startRecord() {
          if(isAudioRecord==false){
              try {
                  recorder.prepare();
                  recorder.start();
              } catch (IllegalStateException e) {
                  e.printStackTrace();
              }catch (IOException e) {
                  e.printStackTrace();
              }
                   isAudioRecord=true;
          }
    }

    @Override
    public void stopRecord() {
                if(isAudioRecord){
                    recorder.stop();
                    recorder.release();
                    isAudioRecord=false;
                }
    }

    @Override
    public void deleteOldRecordFile() {
        File file = new File(fileFolder + "/" + fileName + ".amr");
        file.deleteOnExit();
    }

    @Override
    public double getAmplitude() {
             if(!isAudioRecord){
                 return 0;
             }else {
                 return recorder.getMaxAmplitude();
             }
    }

    /***
     * 获取音频路径
     * @return
     */
    public String getVoicePath(){
        File file=new File(fileFolder + "/" + fileName + ".amr");
        return file.getPath();
    }
    /***
     * 获取当前日期
     * @return time
     */
    private String getCurrentTime(){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy_MM_dd_HHmmss");
        Date curDate=new Date(System.currentTimeMillis());//获取当前日期
        String time=sdf.format(curDate);
        return time;
    }
}

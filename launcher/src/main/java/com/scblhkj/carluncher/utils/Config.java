package com.scblhkj.carluncher.utils;

/**
 * Created by blh on 2016-08-02.
 */
public class Config {

    public static final String PERFERENCE_NAME 				= "prefer_floating";
    public static final String PREF_KEY_FLOAT_X				= "float_x";
    public static final String PREF_KEY_FLOAT_Y				= "float_y";
    public static final String PREF_KEY_DISPLAY_ON_HOME		= "display_on_home";
    public static final String PREF_KEY_IS_RIGHT			= "is_right";
}

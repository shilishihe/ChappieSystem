package com.scblhkj.carluncher.utils;

import android.view.View;

import java.util.Calendar;

/**
 * Created by blh on 2016-08-02.
 */
public abstract class NoDuplicateClickListener implements View.OnClickListener {
  public  static final int MIN_CLICK_DELAY_TIME=1000;
    private long lastClickTime=0;
    public abstract void onNoDulicateClick(View v);
    @Override
    public void onClick(View v) {
      long currentTime= Calendar.getInstance().getTimeInMillis();
        if(currentTime-lastClickTime>MIN_CLICK_DELAY_TIME){
            lastClickTime=currentTime;
            onNoDulicateClick(v);
        }
    }
}

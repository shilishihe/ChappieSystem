package com.scblhkj.carluncher.utils;

import android.text.TextUtils;
import android.util.Log;

import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.domain.ResultBean;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

/**
 * Created by Leo on 2016/4/11.
 * 后视镜往服务器通过HTTP短链接推送消息工具类
 */
public class CPPushUtil {

    private static final String TAG = "CPPushUtil";
    private static String URL = "http://" + ChappieConstant.HTTP_HOST + "/app/Push/rest";

    /**
     * 推送消息
     *
     * @param type
     * @param msg
     */
    public static void pushMsg(int type, String msg) {
        String tempUrl = URL + "/IMEI/" + ChappieCarApplication.DEVICE_IMEI + "/type/" + type + "/content/" + msg;
        Log.e(TAG, tempUrl);
        OkHttpUtils.post().url(tempUrl)
                .build()
                .execute(new StringCallback() {

                    @Override
                    public void onError(Call call, Exception e) {
                        Log.e(TAG, "发送推送消息网络请求失败，" + e.toString());
                    }

                    @Override
                    public void onResponse(String s) {
                        Log.e(TAG, s);
                        if (!TextUtils.isEmpty(s)) {
                            ResultBean bean = GsonUtil.jsonToBean(s, ResultBean.class);
                            if (bean.status.equals("1")) {  // 成功
                                Log.e(TAG, "返回结果正常" + bean.msg);
                            } else if (bean.status.equals("0")) {  // 失败
                                Log.e(TAG, "返回结果异常" + bean.msg);
                            }
                        }
                    }
                });
    }

}

package com.scblhkj.carluncher.utils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Leo on 2016/3/8.
 */
public class AMapBroadCostUtil {

    private AMapBroadCostUtil() {
    }

    public static Context context;

    private static AMapBroadCostUtil instance;

    public static void init(Context cox) {
        context = cox;
    }

    public static AMapBroadCostUtil getInstance() {
        if (instance == null) {
            instance = new AMapBroadCostUtil();
        }
        return instance;
    }

    /**
     * 退出应用
     */
    public static void exitAmap() {
        Intent mIntent = new Intent("com.autonavi.minimap");
        mIntent.putExtra("NAVI", "APP_EXIT");
        context.sendBroadcast(mIntent);
    }

    /**
     * 退出导航
     */
    public static void exitNav() {
        Intent mIntent = new Intent("com.autonavi.minimap");
        mIntent.putExtra("NAVI", "NAVI_EXIT");
        context.sendBroadcast(mIntent);
    }

    /**
     * 打开导航语音
     */
    public static void openNavVoice() {
        Intent mIntent = new Intent("com.autonavi.minimap");
        mIntent.putExtra("NAVI", "OPEN_VOICE");
        context.sendBroadcast(mIntent);
    }

    /**
     * 关闭导航语音
     */
    public static void closeNavVoice() {
        Intent mIntent = new Intent("com.autonavi.minimap");
        mIntent.putExtra("NAVI", "CLOSE_VOICE");
        context.sendBroadcast(mIntent);
    }

    /**
     * 导航白天模式
     */
    public static void dayNavMode() {
        Intent mIntent = new Intent("com.autonavi.minimap");
        mIntent.putExtra("NAVI", "OPEN_DAY_MODEL");
        context.sendBroadcast(mIntent);
    }

    /**
     * 导航黑夜模式
     */
    public static void nightNavMode() {
        Intent mIntent = new Intent("com.autonavi.minimap");
        mIntent.putExtra("NAVI", "OPEN_NIGHT_MODEL");
        context.sendBroadcast(mIntent);
    }

    /**
     * 路况播报打开
     */
    public static void roadBroadOpen() {
        Intent mIntent = new Intent("com.autonavi.minimap");
        mIntent.putExtra("NAVI", "OPEN_TRAFFIC_BROADCAST");
        context.sendBroadcast(mIntent);
    }

    /**
     * 路况播报关闭
     */
    public static void roadBroadClose() {
        Intent mIntent = new Intent("com.autonavi.minimap");
        mIntent.putExtra("NAVI", "CLOSE_TRAFFIC_BROADCAST");
        context.sendBroadcast(mIntent);
    }

    private static final String TAG = "地图广播工具类";
    /**
     * 放大地图
     */
    public static void zoomInMap() {
        Intent mIntent = new Intent("com.autonavi.minimap");
        mIntent.putExtra("NAVI", "MAP_ZOOM_IN");
        context.sendBroadcast(mIntent);
        Log.e(TAG,"放大地图广播发送出去");
    }

    /**
     * 缩小地图
     */
    public static void zoomOutMap() {
        Intent mIntent = new Intent("com.autonavi.minimap");
        mIntent.putExtra("NAVI", "MAP_ZOOM_OUT");
        context.sendBroadcast(mIntent);
        Log.e(TAG, "缩小地图广播发送出去");
    }

    /**
     * 导航2D模式
     */
    public static void nav2DMode(){
        Intent mIntent = new Intent("com.autonavi.minimap");
        mIntent.putExtra("NAVI", "MAP_2D_MODEL");
        context.sendBroadcast(mIntent);
    }

    /**
     * 导航3D模式
     */
    public static void nav3DMode(){
        Intent mIntent = new Intent("com.autonavi.minimap");
        mIntent.putExtra("NAVI", "MAP_3D_MODEL");
        context.sendBroadcast(mIntent);
    }
}

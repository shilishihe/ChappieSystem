package com.scblhkj.carluncher.utils;

import android.content.Context;
import android.net.wifi.WifiManager;

import com.scblhkj.carluncher.R;


/**
 * 手机信号工具类
 */
public class SignalUtil {

    public static final int WIFI_MIN_RSSI = -100;
    public static final int WIFI_MAX_RSSI = -55;
    public static final int WIFI_RSSI_LEVELS = 5;

    public static int getWifiImageBySignal(int signal) {

        int wifiLevel = calculateWifiSignalLevel(signal, WIFI_RSSI_LEVELS);
        switch (wifiLevel) {
           // 没有图片，暂时放放
           case -1:
                return R.drawable.nowifi;
            case 0:
                return R.drawable.wifion;
            case 1:
                return R.drawable.wifion;
            case 2:
                return R.drawable.wifion;
            case 3:
                return R.drawable.wifion;
            case 4:
                return R.drawable.wifion;

            default:
                return R.drawable.ic_shap_light_dot;
        }

    }

    /**
     * Calculates the level of the signal. This should be used any time a signal
     * is being shown.
     *
     * @param rssi      The power of the signal measured in RSSI.
     * @param numLevels The number of levels to consider in the calculated level.
     * @return A level of the signal, given in the range of 0 to numLevels-1
     * (both inclusive).
     */
    public static int calculateWifiSignalLevel(int rssi, int numLevels) {
        if (rssi <= WIFI_MIN_RSSI) {
            return 0;
        } else if (rssi >= WIFI_MAX_RSSI) {
            return numLevels - 1;
        } else {
            float inputRange = (WIFI_MAX_RSSI - WIFI_MIN_RSSI);
            float outputRange = (numLevels - 1);
            return (int) ((float) (rssi - WIFI_MIN_RSSI) * outputRange / inputRange);
        }
    }

    public static String getConnectWifiBssid(Context context) {
        WifiManager wifiManager = (WifiManager) context
                .getSystemService(Context.WIFI_SERVICE);
        android.net.wifi.WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        return wifiInfo.getBSSID();
    }

    /**
     * the GSM Signal Strength, valid values are (0-31, 99) as defined in TS
     * <p/>
     * between 0..31, 99 is unknown
     */
    public static final int SIGNAL_3G_MIN = 0;
    public static final int SIGNAL_3G_MAX = 31;
    public static final int SIGNAL_3G_LEVEL = 5;

    public static int get3GLevelImageByGmsSignalStrength(int gmsSignalStrength) {
        int signalLevel = calculate3GSignalLevel(gmsSignalStrength);
        switch (signalLevel) {
           // 没有图片，暂时放放
           case -1:
                return R.drawable.single1;
            case 0:
                return R.drawable.single2;
            case 1:
                return R.drawable.single3;
            case 2:
                return R.drawable.single4;
            case 3:
                return R.drawable.singlefull;
            case 4:
                return R.drawable.nosingle;

            default:
                return R.drawable.ic_shap_light_dot;
        }
    }

    /**
     * L4:23~31:8
     * <p/>
     * L3:14~22:8
     * <p/>
     * L2:07~13:6
     * <p/>
     * L1:01~06:5
     *
     * @param signal
     * @return
     */
    public static int calculate3GSignalLevel(int signal) {
        if (signal > 31 || signal < 0)
            return -1;
        else if (signal > 22)
            return 4;
        else if (signal > 13)
            return 3;
        else if (signal > 7)
            return 2;
        else if (signal > 0)
            return 1;
        else
            return -1;
    }

    /**
     * NETWORK_TYPE_UNKNOWN
     * <p/>
     * NETWORK_TYPE_GPRS
     * <p/>
     * NETWORK_TYPE_EDGE
     * <p/>
     * NETWORK_TYPE_UMTS
     * <p/>
     * NETWORK_TYPE_HSDPA
     * <p/>
     * NETWORK_TYPE_HSUPA
     * <p/>
     * NETWORK_TYPE_HSPA
     * <p/>
     * NETWORK_TYPE_CDMA
     * <p/>
     * NETWORK_TYPE_EVDO_0
     * <p/>
     * NETWORK_TYPE_EVDO_A
     * <p/>
     * NETWORK_TYPE_EVDO_B
     * <p/>
     * NETWORK_TYPE_1xRTT
     * <p/>
     * NETWORK_TYPE_IDEN
     * <p/>
     * NETWORK_TYPE_LTE
     * <p/>
     * NETWORK_TYPE_EHRPD
     * <p/>
     * NETWORK_TYPE_HSPAP
     */
    public static int get3GTypeImageByNetworkType(int networkType) {
        switch (networkType) {
           /* *//***** 2G *****//*
            case TelephonyManager.NETWORK_TYPE_GPRS:
                return R.drawable.ic_qs_signal_full_g; // G
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                return R.drawable.ic_qs_signal_full_1x; // 1X
            case TelephonyManager.NETWORK_TYPE_IDEN:
            case TelephonyManager.NETWORK_TYPE_EDGE:
                return R.drawable.ic_qs_signal_full_e; // E
            *//***** 3G *****//*
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
                return R.drawable.ic_qs_signal_full_h; // H
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return R.drawable.ic_qs_signal_full_h_plus; // H+
            case TelephonyManager.NETWORK_TYPE_EHRPD: // 3.9G
            case TelephonyManager.NETWORK_TYPE_UMTS:
                return R.drawable.ic_qs_signal_full_3g;
            *//***** 4G *****//*
            case TelephonyManager.NETWORK_TYPE_LTE:
                return R.drawable.ic_qs_signal_full_4g; // 4G
            *//***** Unknown *****//*
            case TelephonyManager.NETWORK_TYPE_UNKNOWN:*/
            default:
                return R.drawable.ic_shap_light_dot;
        }

    }
}

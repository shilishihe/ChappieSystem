package com.scblhkj.carluncher.utils;

/**
 * Created by Leo on 2015/11/26.
 * 字节数组异或工具类
 */
public class XORUtil {

    /**
     * 在数组尾部新增异或校验位
     *
     * @param bs
     * @return
     */
    public static byte[] xorByteArray(byte[] bs) {
        byte[] rBs = new byte[bs.length + 1];
        int[] resultBytes = new int[bs.length];
        // 将有符号十六进制字节数组转成无符号int数组
        for (int i = 0; i < bs.length; i++) {
            byte tempByte = bs[i];
            int jj = tempByte;
            jj = tempByte & 0Xff;
            resultBytes[i] = jj;
        }
        int checkData = doCheckData(resultBytes);
        byte checkR = (byte) checkData;
        System.arraycopy(bs, 0, rBs, 0, bs.length);
        rBs[bs.length] = checkR;
        return rBs;
    }


    /**
     * 异或校验检查数据的准确性
     */
    public static int doCheckData(int[] datas) {
        int r = datas[4];
        int l = datas[3];
        for (int i = 5; i < l + 3; i++) {
            r = r ^ datas[i];
        }
        return r;
    }

    /**
     * @param bs
     * @return
     */
    public static int[] doBytes2Ints(byte[] bs) {
        int[] resultBytes = new int[bs.length];
        // 将有符号十六进制字节数组转成无符号int数组
        for (int i = 0; i < bs.length; i++) {
            byte tempByte = bs[i];
            int jj = tempByte;
            jj = tempByte & 0Xff;
            resultBytes[i] = jj;
        }
        return resultBytes;
    }

    /**
     * 有符号byte转无符号int
     *
     * @return
     */
    public static long doByte2UnLong(byte b) {
        long result = b & 0XFF;
        return result;
    }

    /**
     * 有符号byte转无符号int
     *
     * @return
     */
    public static int doByte2UnInt(byte b) {
        int result = b & 0XFF;
        return result;
    }

    /**
     * @param i
     * @return
     */
    public static String int2HexString(int i) {
        String str = Integer.toHexString(i);
        if (str.length() < 2) {
            str = 0 + str;
        }
        return str;
    }


}

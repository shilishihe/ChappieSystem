package com.scblhkj.carluncher.utils;

/**
 * Created by Leo on 2015/12/16.
 * 累加和校验工具类
 */
public class AccumulatedSumUtil {

    /**
     * 累加和校验
     *
     * @param bs
     * @return
     */
    public static byte[] as(byte[] bs) {
        byte[] rBs = new byte[bs.length + 1];
        int[] resultBytes = new int[bs.length];
        // 将有符号十六进制字节数组转成无符号int数组
        for (int i = 0; i < bs.length; i++) {
            byte tempByte = bs[i];
            int jj = tempByte;
            jj = tempByte & 0Xff;
            resultBytes[i] = jj;
        }
        int checkData = doCheckData(resultBytes);
        byte checkR = (byte) checkData;
        System.arraycopy(bs, 0, rBs, 0, bs.length);
        rBs[bs.length] = checkR;
        return rBs;
    }

    /**
     * 异或校验检查数据的准确性
     */
    public static int doCheckData(int[] datas) {
        int sum =0;
        int l = datas[3];
        for (int i = 4; i < l + 3; i++) {
            sum = sum + datas[i];
        }
        return sum;
    }
}

package com.scblhkj.carluncher.utils;

import android.bluetooth.BluetoothAdapter;
import android.util.Log;

import com.scblhkj.carluncher.app.ChappieCarApplication;

/**
 * Created by Leo on 2015/11/19.
 * 蓝牙工具类
 */
public class BluetoothUtil {

    private static final String TAG = "BluetoothUtil";

    /**
     * 判断蓝牙是否打开
     *
     * @return
     */
    public static boolean isBluetoothIsOpen() {

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            if (ChappieCarApplication.isDebug)
                Log.e(TAG, "本地没有找到蓝牙硬件或者驱动");
            return false;
        }
        boolean isEnable = bluetoothAdapter.isEnabled();
        return isEnable;
    }

}




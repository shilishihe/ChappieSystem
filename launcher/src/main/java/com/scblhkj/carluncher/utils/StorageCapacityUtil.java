package com.scblhkj.carluncher.utils;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.text.format.Formatter;
import android.util.Log;

import com.scblhkj.carluncher.domain.StorageCapacityBean;

import java.io.File;

/**
 * Created by Leo on 2016/1/22.
 */
public class StorageCapacityUtil {

    private Context context;
    private static final String TAG = "StorageCapacityUtil";

    public StorageCapacityUtil(Context context) {
        this.context = context;
    }

    // ----------------------------------手机内存------------------------------------------

    /**
     * 获取手机内存中总内存
     *
     * @return
     */
    public long getSDTotalSize() {
        File path = Environment.getExternalStorageDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        Log.e("getSDTotalSize = ", Formatter.formatFileSize(context, blockSize * totalBlocks));
        return blockSize * totalBlocks;
    }

    /**
     * 获取手机内存可用空间大小
     *
     * @return
     */
    public long getSDCardAvailableSpace() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File sdcardDir = Environment.getExternalStorageDirectory();
            StatFs sf = new StatFs(sdcardDir.getPath());
            long blockSize = sf.getBlockSize();
            long availCount = sf.getAvailableBlocks();
            return availCount * blockSize;
        } else {
            return 0;
        }
    }

    /**
     * 获取手机内存中图片文件的大小
     *
     * @return
     */
    public long getSDCardPicSize() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File file = new File(Environment.getExternalStorageDirectory(), "ChappieLauncher/" + "picture");
            return (long) FileUtil.getDirSize(file);
        }
        return 0;
    }

    /**
     * 获取手机内存中视频文件的大小
     *
     * @return
     */
    public long getSDCardVideoSize() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File file = new File(Environment.getExternalStorageDirectory(), "ChappieLauncher/" + "video");
            return FileUtil.getDirSize(file);
        }
        return 0;
    }

    //------------------------------外置TF卡---------------------------------------

    /**
     * 获取外置TF卡容量大小
     *
     * @return
     */
    public long getTFTotalSize() {
        if (SDCardScannerUtil.isSecondSDcardMounted()) {
            File path = new File(SDCardScannerUtil.getTFDir());
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long totalBlocks = stat.getBlockCount();
            Log.e("getTFTotalSize = ", Formatter.formatFileSize(context, blockSize * totalBlocks));
            return blockSize * totalBlocks;
        } else {
            return 0;
        }
    }

    /**
     * 获取外置TF卡可用空间大小
     *
     * @return
     */
    public long getTFCardAvailableSpace() {
        if (SDCardScannerUtil.isSecondSDcardMounted()) {
            File path = new File(SDCardScannerUtil.getTFDir());
            StatFs sf = new StatFs(path.getPath());
            long blockSize = sf.getBlockSize();
            long availCount = sf.getAvailableBlocks();
            Log.e(TAG, "可用：" + path.getPath() + " blockSize = " + blockSize + " availCount = " + availCount + " -- " + availCount * blockSize);
            return availCount * blockSize;
        } else {
            return 0;
        }
    }

    /**
     * 获取外置TF卡中查派目录下所有图片的大小
     *
     * @return
     */
    public long getTFPictureSpace() {
       /* if (SDCardScannerUtil.isSecondSDcardMounted()) {
            File path = new File(SDCardScannerUtil.getTFDir(), "ChappieLauncher/picture");
            Log.e(TAG, "getTFPictureSpace path = " + path.getPath());
            StatFs sf = new StatFs(path.getPath());
            long blockSize = sf.getBlockSize();
            long availCount = sf.getBlockCount();
            Log.e(TAG,"图片：" + path.getPath() + " blockSize = " + blockSize + " availCount = " + availCount + " -- " + availCount * blockSize);
            return availCount * blockSize;
        } else {
            return 0;
        }*/

        if (SDCardScannerUtil.isSecondSDcardMounted()) {
            File path = new File(SDCardScannerUtil.getTFDir(), "ChappieLauncher/picture");
            Log.e(TAG, "getTFPictureSpace path = " + path.getPath());
            return FileUtil.getDirSize(path);
        } else {
            return 0;
        }


    }

    /**
     * 获取外置TF卡中查派目录下所有视频的大小
     *
     * @return
     */
    public long getTFVideoSpace() {
       /* if (SDCardScannerUtil.isSecondSDcardMounted()) {
            File path = new File(SDCardScannerUtil.getTFDir(), "ChappieLauncher/video");
            StatFs sf = new StatFs(path.getPath());
            long blockSize = sf.getBlockSize();
            long availCount = sf.getBlockCount();
            Log.e(TAG, "视频：" + path.getPath() + " blockSize = " + blockSize + " availCount = " + availCount + " -- " + availCount * blockSize);
            return availCount * blockSize;
        } else {
            return 0;
        }*/
        if (SDCardScannerUtil.isSecondSDcardMounted()) {
            File path = new File(SDCardScannerUtil.getTFDir(), "ChappieLauncher/video");
            Log.e(TAG, "getTFPictureSpace path = " + path.getPath());
            return FileUtil.getDirSize(path);
        } else {
            return 0;
        }
    }

    public StorageCapacityBean getStorageCapacityBean() {
        StorageCapacityBean bean = new StorageCapacityBean();
        long rm_total_storage = getSDTotalSize();
        long rm_use_storage = rm_total_storage - getSDCardAvailableSpace();
        long sd_total_storage = getTFTotalSize();
        long sd_use_storage = sd_total_storage - getTFCardAvailableSpace();
        bean.setRm_total_storage(String.valueOf(rm_total_storage));
        bean.setRm_use_storage(String.valueOf(rm_use_storage));
        bean.setSd_total_storage(String.valueOf(sd_total_storage));
        bean.setSd_use_storage(String.valueOf(sd_use_storage));
        Log.e(TAG, Formatter.formatFileSize(context, rm_total_storage));
        Log.e(TAG, Formatter.formatFileSize(context, rm_use_storage));
        Log.e(TAG, Formatter.formatFileSize(context, sd_total_storage));
        Log.e(TAG, Formatter.formatFileSize(context, sd_use_storage));
        return bean;
    }
}

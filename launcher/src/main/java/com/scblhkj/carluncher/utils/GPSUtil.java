package com.scblhkj.carluncher.utils;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;

/**
 * Created by Leo on 2016/2/25.
 * GPS工具类
 */
public class GPSUtil {

    private static final String TAG = "GPSUtil";

    /**
     * 打开或者是关闭GPS开关
     *
     * @param context
     */
    public static void toggleGPS(Context context) {
        Log.e(TAG, "开启和关闭GPS");
        Intent gpsIntent = new Intent();
        gpsIntent.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
        gpsIntent.addCategory("android.intent.category.ALTERNATIVE");
        gpsIntent.setData(Uri.parse("custom:3"));
        try {
            PendingIntent.getBroadcast(context, 0, gpsIntent, 0).send();
        } catch (PendingIntent.CanceledException e) {
            Log.e(TAG,"开启或者是关闭GPS异常");
            e.printStackTrace();
        }
    }

    /**
     * 判断当前设别GPS的状态
     *
     * @param context
     * @return
     */
    public static boolean isGPSEnable(Context context) {
        String str = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        Log.v("GPS", str);
        if (str != null) {
            return str.contains("gps");
        } else {
            return false;
        }
    }
}

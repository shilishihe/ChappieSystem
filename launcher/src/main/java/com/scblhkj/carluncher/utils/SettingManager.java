package com.scblhkj.carluncher.utils;

import android.content.SharedPreferences;

import com.scblhkj.carluncher.common.ChappieConstant;

/**
 * Created by Leo on 2016/2/23.
 * 后视镜设置管理器
 */
public class SettingManager {

    private SharedPreferences sp;
    private static SettingManager instance;
    private boolean swGps;
    private boolean swRecordVoice;
    private boolean swVibrationAnalysis;
    private boolean swRemoteControlMainCamera;
    private boolean swRemoteControlSecondCamera;
    private boolean swLocationShared;
    private boolean swAdas;
    private String rgVibrationTimeDelaySetting;  // 振动延时设置
    private String rgVibrationMonitoringLevel;  // 振动级别设置
    private String rgPowerModeSettings;  // 功耗模式设置

    public String getRgPowerModeSettings() {
        return SpUtil.getString2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_POWER_MODE_SETTINGS);
    }

    public void setRgPowerModeSettings(String rgPowerModeSettings) {
        this.rgPowerModeSettings = rgPowerModeSettings;
        SpUtil.saveString2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_POWER_MODE_SETTINGS, this.rgPowerModeSettings);
    }

    public String getRgVibrationMonitoringLevel() {
        return SpUtil.getString2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_VIBRATION_MONITORING_LEVEL);
    }

    public void setRgVibrationMonitoringLevel(String rgVibrationMonitoringLevel) {
        this.rgVibrationMonitoringLevel = rgVibrationMonitoringLevel;
        SpUtil.saveString2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_VIBRATION_MONITORING_LEVEL, this.rgVibrationMonitoringLevel);
    }

    public String getRgVibrationTimeDelaySetting() {
        return SpUtil.getString2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_VIBRATION_TIME_DELAY_SETTING);
    }

    public void setRgVibrationTimeDelaySetting(String rgVibrationTimeDelaySetting) {
        this.rgVibrationTimeDelaySetting = rgVibrationTimeDelaySetting;
        SpUtil.saveString2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_RG_VIBRATION_TIME_DELAY_SETTING, this.rgVibrationTimeDelaySetting);
    }

    private SettingManager() {
    }

    public static SettingManager getInstance() {

        if (instance == null) {
            instance = new SettingManager();
        }
        return instance;
    }


    public boolean isSwGps() {
        return SpUtil.getBoolean2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_GPS);
    }

    public void setSwGps(boolean swGps) {
        this.swGps = swGps;
        SpUtil.saveBoolean2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_GPS, this.swGps);
    }

    public boolean isSwRecordVoice() {
        return SpUtil.getBoolean2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_RECORD_VOICE);
    }

    public void setSwRecordVoice(boolean swRecordVoice) {
        this.swRecordVoice = swRecordVoice;
        SpUtil.saveBoolean2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_RECORD_VOICE, this.swRecordVoice);
    }

    public boolean isSwVibrationAnalysis() {
        return SpUtil.getBoolean2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_VIBRATION_ANALYSIS);
    }

    public void setSwVibrationAnalysis(boolean swVibrationAnalysis) {
        this.swVibrationAnalysis = swVibrationAnalysis;
        SpUtil.saveBoolean2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_VIBRATION_ANALYSIS, this.swVibrationAnalysis);
    }

    public boolean isSwRemoteControlMainCamera() {
        return SpUtil.getBoolean2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_REMOTE_CONTROL_MAIN_CAMERA);
    }

    public void setSwRemoteControlMainCamera(boolean swRemoteControlMainCamera) {
        this.swRemoteControlMainCamera = swRemoteControlMainCamera;
        SpUtil.saveBoolean2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_REMOTE_CONTROL_MAIN_CAMERA, this.swRemoteControlMainCamera);
    }

    public boolean isSwRemoteControlSecondCamera() {
        return SpUtil.getBoolean2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_REMOTE_CONTROL_SECOND_CAMERA);
    }

    public void setSwRemoteControlSecondCamera(boolean swRemoteControlSecondCamera) {
        this.swRemoteControlSecondCamera = swRemoteControlSecondCamera;
        SpUtil.saveBoolean2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_REMOTE_CONTROL_SECOND_CAMERA, this.swRemoteControlSecondCamera);
    }

    public boolean isSwLocationShared() {
        return SpUtil.getBoolean2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_LOCATION_SHARED);
    }

    public void setSwLocationShared(boolean swLocationShared) {
        this.swLocationShared = swLocationShared;
        SpUtil.saveBoolean2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_LOCATION_SHARED, this.swLocationShared);
    }

    public boolean isSwAdas() {
        return SpUtil.getBoolean2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_ADAS);
    }

    public void setSwAdas(boolean swAdas) {
        this.swAdas = swAdas;
        SpUtil.saveBoolean2SP(ChappieConstant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_ADAS, this.swAdas);
    }


}

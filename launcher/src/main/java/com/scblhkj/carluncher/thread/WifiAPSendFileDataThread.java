package com.scblhkj.carluncher.thread;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.utils.CommonUtil;
import com.scblhkj.carluncher.utils.ToastUtil;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.Socket;

/**
 * Created by Leo on 2016/3/24.
 * Wifi文件传输的线程
 */
public class WifiAPSendFileDataThread extends Thread {

    private static final String TAG = "WifiAPSendFileDataThread";
    private Socket socket;
    private String filePath;
    private int length = 0;
    private int sumL = 0;
    private byte[] sendBytes = null;
    private FileInputStream fis = null;
    private DataOutputStream dos;
    private String cmd,timeStamp,imei;
    private String headData;
    private boolean isFinishSocketOrder;
   private Handler mhandler=new Handler(){
       @Override
       public void handleMessage(Message msg) {
           switch (msg.what){


           }
           super.handleMessage(msg);
       }
   };
    public WifiAPSendFileDataThread(DataOutputStream dos, String filePath, String cmd,String timeStamp,String imei) {
        this.filePath = filePath;
        this.dos = dos;
        this.cmd = cmd;
        this.timeStamp=timeStamp;
        this.imei=imei;
    }

    @Override
    public void run() {
        try {
            try {
                File file = new File(filePath);
                  Log.e("info", "待发送的文件路径是" + filePath);
                  Log.e("info", "fileLength = " + file.length());
                String filelength=String.valueOf(file.length());
                String fileName = file.getName();
                fileName = CommonUtil.getFixedLenString(fileName, 15);
                if(cmd.equals("G")){  //请求后视镜指定文件
                    headData ="@"+timeStamp+","+imei+","+cmd +",1,1,"+ filelength+",@,";
                }else if(cmd.equals("C")){  //请求后视镜远程控制拍照
                    headData ="@"+timeStamp+","+imei+","+cmd +",1,1,"+filePath+","+filelength+",@,";
                }else if(cmd.equals("R")){  //请求后视镜远程控制摄像
                    headData ="@"+timeStamp+","+imei+","+cmd +",1,1,"+filePath+","+filelength+",@,";;
                }else if(cmd.equals("T")){  //请求后视镜的指定文件缩略图
                    headData="@"+timeStamp+","+imei+","+cmd+",1,1,"+filelength+",@,";
                }else if(cmd.contains("M")){  //发送对讲文件
                    headData="@"+timeStamp+","+imei+","+cmd.substring(0,1)+",1,"+cmd.substring(1)+","+filelength+",@,";
                }else if(cmd.contains("A0")){  //发送碰撞图片
                    String quekeValue=cmd.substring(2);//震动值
                    headData="@"+timeStamp+","+imei+","+cmd.substring(0,1)+",1,0,"+quekeValue+","+filePath+","+filelength+",@,";
                }else if(cmd.contains("A1")){  //发送急减速图片
                    String speedValue=cmd.substring(2);//减速值
                    headData="@"+timeStamp+","+imei+","+cmd.substring(0,1)+",1,1,"+speedValue+","+filePath+","+filelength+",@,";
                }else if(cmd.contains("V0")){  //发送碰撞视频
                    String quekeValue=cmd.substring(2);//震动值
                    headData="@"+timeStamp+","+imei+","+cmd.substring(0,1)+",1,0,"+quekeValue+","+filePath+","+filelength+",@,";
                }else if(cmd.contains("V1")){  //发送急减速视频
                    String speedValue=cmd.substring(2);//减速值
                    headData="@"+timeStamp+","+imei+","+cmd.substring(0,1)+",1,1,"+speedValue+","+filePath+","+filelength+",@,";
                }else {
                    headData =timeStamp+imei+cmd + CommonUtil.getCurrentData() + fileName + filelength;
                }
                //String headData =timeStamp+imei+cmd + CommonUtil.getCurrentData() + fileName + filelength;
                  Log.e("info", "头长度:" + headData.length() + "头内容:" + headData);
                dos.write(headData.getBytes());
                dos.flush();
                fis = new FileInputStream(file);
                Log.e("info","**fileLength = " + file.length());
                sendBytes = new byte[1024 * 4];
                while ((length = fis.read(sendBytes, 0, sendBytes.length)) > 0) {
                    sumL += length;
                    dos.write(sendBytes, 0, length);
                    dos.flush();
                    Log.e("info", "文件已经发送了" + sumL + "B");
                }
                Log.e("info","while执行完成");
                Log.e("info","***fileLength = " + file.length());
                if (file.length() == sumL) {
                    Log.e("info", "sumL =" + sumL);
                    Log.e("info", "fileLength = " + file.length() + "文件发送成功");
                    ToastUtil.showToast("文件发送成功!");
                } else {
                    Log.e("info", "文件发送过程中出现了丢包,还有" + (file.length() - sumL) + "个B没传过去");
                   long rest =file.length()-sumL;
                    ToastUtil.showToast("文件发送过程中出现了丢包,还有" + rest + "个B没传过去");
                }
            } finally {
                if (fis != null)
                    fis.close();
            }
        } catch (Exception e) {
            Log.e("info", "SendFileDataThread中出现异常");
            ToastUtil.showToast("发送失败,请检查网络");
            e.printStackTrace();
        }
        super.run();
    }
}

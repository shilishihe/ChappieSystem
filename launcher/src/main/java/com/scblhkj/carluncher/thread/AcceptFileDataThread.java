package com.scblhkj.carluncher.thread;

import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;

/**接收文件的线程
 * Created by blh on 2016-11-17.
 */
public class AcceptFileDataThread extends Thread {
    private static final String TAG = "WifiAPSendFileDataThread";
    private Socket socket;
    private String filePath;
    private int length = 0;
    private int sumL = 0;
    private byte[] sendBytes = null;
    private FileInputStream fis = null;
    private DataOutputStream dos;
    private DataInputStream dis;
    private String cmd,timeStamp,imei;
    private String headData;
    private boolean isFinishSocketOrder;
   private int fileSize;
    public AcceptFileDataThread(DataOutputStream dos,DataInputStream dis){
            this.dis=dis;
           this.dos=dos;
    }

    @Override
    public void run() {
        try {
            fileSize=(int)dis.readLong()-1;
            byte[] buffer=new byte[1024*4];
            while (true){
                int read=0;
                if(dis!= null){
                    read=dis.read(buffer);
                    fileSize+=read;
                }
                if(read==-1){
                    break;
                }
                dos.write(buffer,0,read);
            }
            dos.close();
            ///dis.close();
            Log.e("info","文件接收完毕");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            this.interrupt();
        }


    }
}

package com.scblhkj.carluncher.thread;

import android.util.Log;

import com.scblhkj.carluncher.utils.CommonUtil;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

/**
 * Created by blh on 2016-08-30.
 */
public class NetSendFileDataThread extends Thread {
    private static final String TAG = "WifiAPSendFileDataThread";
    private Socket socket;
    private String filePath;
    private int length = 0;
    private int sumL = 0;
    private byte[] sendBytes = null;
    private FileInputStream fis = null;
    private DataOutputStream dos;
    private String cmd;

    public NetSendFileDataThread(DataOutputStream dos, String filePath, String cmd) {
        this.filePath = filePath;
        this.dos = dos;
        this.cmd = cmd;
    }

    @Override
    public void run() {
        //  try {
        try {
            File file = new File(filePath);
            Log.e("info", "待发送的文件路径是>>>" + filePath);
            Log.e("info","fileLength = " + file.length());
            String fileLHex = Long.toHexString(file.length());
            fileLHex = CommonUtil.getFixedLenString(fileLHex, 8);
            String fileName = file.getName();
            fileName = CommonUtil.getFixedLenString(fileName, 15);
            String headData = cmd + CommonUtil.getCurrentData() + fileName + fileLHex;
            Log.e("info", "头长度为" + headData.length()+"----" + "头内容" + headData);
            try {
                Log.e("info", "headData------" +headData.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            try {
                dos.write(headData.getBytes());
                dos.flush();
                fis = new FileInputStream(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.e("info","**fileLength = " + file.length());
            sendBytes = new byte[1024 * 4];
            Log.e("info","**sendBytes = " + sendBytes);
            try {
                while ((length = fis.read(sendBytes, 0, sendBytes.length)) > 0) {
                    sumL += length;
                    dos.write(sendBytes, 0, length);
                    dos.flush();
                    //Log.e("info", "文件已经发送了" + sumL + "B");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.e("info","while执行完成");
            Log.e("info","***fileLength = " + file.length());
            if (file.length() == sumL) {
                Log.e("info","sumL =" + sumL);
                Log.e("info", "fileLength = " + file.length() +"文件发送成功");
            } else {
                Log.e("info", "文件发送过程中出现了丢包,还有" + (file.length() - sumL) + "个B没传过去");
            }
        } finally {
            if (fis != null)
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        // } catch (Exception e) {
        //  Log.e("info", "SendFileDataThread中出现异常");
        //  Log.e("info", "E>>"+e.toString());
        // e.printStackTrace();
        // }
        super.run();
    }
}

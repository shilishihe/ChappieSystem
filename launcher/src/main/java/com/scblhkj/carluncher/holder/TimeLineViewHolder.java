package com.scblhkj.carluncher.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.common.TimeLineItemType;
import com.scblhkj.carluncher.domain.OBDFaultBean;
import com.scblhkj.carluncher.widget.TimeLineMarkerView;

/**
 * Created by Leo on 2015/12/17.
 * ViewHolder
 */
public class TimeLineViewHolder extends RecyclerView.ViewHolder {

    private TimeLineMarkerView tmv_item;

    public TimeLineViewHolder(View itemView, int type) {
        super(itemView);
        tmv_item = (TimeLineMarkerView) itemView.findViewById(R.id.tmv_item);
        if (type == TimeLineItemType.ATOM) {
            tmv_item.setHeadLineVisible();
            tmv_item.setFootLineVissible();
        } else if (type == TimeLineItemType.START) {
            tmv_item.setHeadLineVisible();
        } else if (type == TimeLineItemType.END) {
            tmv_item.setFootLineVissible();
        }
    }

    /**
     * 设置数据
     */
    public void setData(OBDFaultBean bean) {
        tmv_item.setTime(bean.faultTime);
        for (int i = 0; i < bean.faults.size(); i++) {
            OBDFaultBean.Fault fault = bean.faults.get(i);
            tmv_item.addContentItem(fault.faultDes, fault.faultState);
        }
    }
}



package com.scblhkj.carluncher.holder;

import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Leo on 2016/1/12.
 * 文件夹AdapterHolder
 */
public class FolderBeanCardViewHolder {
    public TextView folderName;
    public RelativeLayout rlFolder;
    public RelativeLayout rlContainer;
}

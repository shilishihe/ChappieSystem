package com.scblhkj.carluncher.holder;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Leo on 2016/1/12.
 */
public class FileManagerPicCardViewHolder {
    public TextView picFileName;
    public ImageView picFileImg;
    public RelativeLayout rlContainer;
}

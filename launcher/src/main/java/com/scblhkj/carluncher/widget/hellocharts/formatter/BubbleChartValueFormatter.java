package com.scblhkj.carluncher.widget.hellocharts.formatter;


import com.scblhkj.carluncher.widget.hellocharts.model.BubbleValue;

public interface BubbleChartValueFormatter {

    public int formatChartValue(char[] formattedValue, BubbleValue value);
}

package com.scblhkj.carluncher.widget.hellocharts.listener;


import com.scblhkj.carluncher.widget.hellocharts.model.SubcolumnValue;

public class DummyColumnChartOnValueSelectListener implements ColumnChartOnValueSelectListener {

    @Override
    public void onValueSelected(int columnIndex, int subcolumnIndex, SubcolumnValue value) {

    }

    @Override
    public void onValueDeselected() {

    }

}

package com.scblhkj.carluncher.widget.hellocharts.listener;


import com.scblhkj.carluncher.widget.hellocharts.model.BubbleValue;

public class DummyBubbleChartOnValueSelectListener implements BubbleChartOnValueSelectListener {

    @Override
    public void onValueSelected(int bubbleIndex, BubbleValue value) {

    }

    @Override
    public void onValueDeselected() {

    }
}

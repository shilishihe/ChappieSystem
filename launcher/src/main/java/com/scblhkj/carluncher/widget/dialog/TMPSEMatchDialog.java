package com.scblhkj.carluncher.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scblhkj.carluncher.R;


/**
 * Created by Leo on 2015/12/10.
 * 胎压发射配对界面弹出对话框
 */
public class TMPSEMatchDialog extends Dialog {

    public TMPSEMatchDialog(Context context) {
        super(context);
    }

    public TMPSEMatchDialog(Context context, int theme) {
        super(context, theme);
    }

    protected TMPSEMatchDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public static class Builder {

        private Context context;
        private String title;
        private String cancelString;
        private View contentView;
        private Dialog.OnClickListener onCancelListener;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setTitle(int id) {
            this.title = (String) context.getText(id);
            return this;
        }


        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setContentView(View v) {
            this.contentView = v;
            return this;
        }

        public Builder setCancelButton(int id, DialogInterface.OnClickListener listener) {
            this.cancelString = (String) context.getText(id);
            this.onCancelListener = listener;
            return this;
        }

        public Builder setCancelButton(String text, DialogInterface.OnClickListener listener) {
            this.cancelString = text;
            this.onCancelListener = listener;
            return this;
        }

        public TMPSEMatchDialog create() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final TMPSEMatchDialog dialog = new TMPSEMatchDialog(context, R.style.TMPSEMatchDialog);
            View view = inflater.inflate(R.layout.tmps_match_dialog, null);
            dialog.addContentView(view, new LayoutParams(800, LayoutParams.WRAP_CONTENT));
            ((TextView) view.findViewById(R.id.tv_title)).setText(title);
            ((Button) view.findViewById(R.id.bt_cancel)).setText(cancelString);
            if (onCancelListener != null) {
                view.findViewById(R.id.bt_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onCancelListener.onClick(dialog, DialogInterface.BUTTON_NEGATIVE);
                    }
                });
            }
            if (contentView != null) {
                ((RelativeLayout) view.findViewById(R.id.content)).removeAllViews();
                ((RelativeLayout) view.findViewById(R.id.content)).addView(contentView, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
            }
            dialog.setContentView(view);
            return dialog;
        }
    }
}

package com.scblhkj.carluncher.widget.hellocharts.listener;


import com.scblhkj.carluncher.widget.hellocharts.model.SliceValue;

public interface PieChartOnValueSelectListener extends OnValueDeselectListener {

    public void onValueSelected(int arcIndex, SliceValue value);

}

package com.scblhkj.carluncher.widget.hellocharts.provider;


import com.scblhkj.carluncher.widget.hellocharts.model.ComboLineColumnChartData;

public interface ComboLineColumnChartDataProvider {

    public ComboLineColumnChartData getComboLineColumnChartData();

    public void setComboLineColumnChartData(ComboLineColumnChartData data);

}

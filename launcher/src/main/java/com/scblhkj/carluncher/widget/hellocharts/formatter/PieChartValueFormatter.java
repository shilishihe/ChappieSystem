package com.scblhkj.carluncher.widget.hellocharts.formatter;


import com.scblhkj.carluncher.widget.hellocharts.model.SliceValue;

public interface PieChartValueFormatter {

    public int formatChartValue(char[] formattedValue, SliceValue value);
}

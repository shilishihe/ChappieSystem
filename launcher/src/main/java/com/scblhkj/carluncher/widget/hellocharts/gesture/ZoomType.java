package com.scblhkj.carluncher.widget.hellocharts.gesture;

public enum ZoomType {

    HORIZONTAL, VERTICAL, HORIZONTAL_AND_VERTICAL;
}

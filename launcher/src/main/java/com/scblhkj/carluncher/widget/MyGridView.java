package com.scblhkj.carluncher.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * Created by blh on 2016-06-17.
 */
public class MyGridView extends GridView {
    public MyGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public MyGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyGridView(Context context) {
        super(context);
    }
    /**
     * 设置不滚动
     *
     *   public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
     int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
     MeasureSpec.AT_MOST);
     super.onMeasure(widthMeasureSpec, expandSpec);

     }
     */

}

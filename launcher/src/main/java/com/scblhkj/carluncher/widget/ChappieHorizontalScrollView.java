package com.scblhkj.carluncher.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

import com.scblhkj.carluncher.app.ChappieCarApplication;

/**
 * Created by Leo on 2015/12/10.
 * 让HorizontalScrollView内部控件可滑动
 */
public class ChappieHorizontalScrollView extends HorizontalScrollView {

    public ChappieHorizontalScrollView(Context context) {
        super(context);
    }

    public ChappieHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ChappieHorizontalScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ChappieHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (ChappieCarApplication.ALLOW_SCROLL == 0) {
            return false;
        } else {
            return super.onInterceptTouchEvent(ev);
        }
    }

}

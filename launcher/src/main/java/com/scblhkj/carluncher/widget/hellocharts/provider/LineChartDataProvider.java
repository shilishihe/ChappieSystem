package com.scblhkj.carluncher.widget.hellocharts.provider;


import com.scblhkj.carluncher.widget.hellocharts.model.LineChartData;

public interface LineChartDataProvider {

    public LineChartData getLineChartData();

    public void setLineChartData(LineChartData data);

}

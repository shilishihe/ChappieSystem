package com.scblhkj.carluncher.widget.hellocharts.listener;


import com.scblhkj.carluncher.widget.hellocharts.model.PointValue;

public class DummyLineChartOnValueSelectListener implements LineChartOnValueSelectListener {

    @Override
    public void onValueSelected(int lineIndex, int pointIndex, PointValue value) {

    }

    @Override
    public void onValueDeselected() {

    }
}

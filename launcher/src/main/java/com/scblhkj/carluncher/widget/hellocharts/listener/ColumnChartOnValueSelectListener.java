package com.scblhkj.carluncher.widget.hellocharts.listener;


import com.scblhkj.carluncher.widget.hellocharts.model.SubcolumnValue;

public interface ColumnChartOnValueSelectListener extends OnValueDeselectListener {

    public void onValueSelected(int columnIndex, int subcolumnIndex, SubcolumnValue value);

}

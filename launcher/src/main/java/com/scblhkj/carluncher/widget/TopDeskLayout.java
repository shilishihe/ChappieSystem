package com.scblhkj.carluncher.widget;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.scblhkj.carluncher.R;

import java.security.Policy;

/**
 * Created by blh on 2016-07-08.
 */
public class TopDeskLayout extends FrameLayout {
    public TopDeskLayout(Context context) {
        super(context);
        //设置宽高
        this.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT));

        View view = LayoutInflater.from(context).inflate(
                R.layout.float_popup_window_left, null);

        this.addView(view);
    }
}

package com.scblhkj.carluncher.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.app.ChappieCarApplication;


/**
 * Created by Leo on 2015/12/16.
 * 自定义时间轴控件
 */
public class TimeLineMarkerView extends LinearLayout {

    private int markerSize = 10;  // marker大小
    private int markerColor; // marker背景颜色
    private int lineSize = 1; // 线的粗细
    private int lineColor; // 线的颜色
    private View v_cycle;  // 时间轴上不同颜色的圆球
    private TextView tv_time;  // 故障时间
    private RelativeLayout rl_content_item;  // 每一条故障信息
    private TextView tv_content_msg;  // 故障的详细信息
    private View v_state;  // 详细信息条目中的状态（清除或是不清除）
    private View v_head; // 线头
    private View v_foot; // 线尾
    private LinearLayout ll_content; // 故障框


    public TimeLineMarkerView(Context context) {
        this(context, null);
        init(context);
    }

    public TimeLineMarkerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        init(context);
    }

    public TimeLineMarkerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        View.inflate(context, R.layout.widget_time_line_item, this);
        v_cycle = findViewById(R.id.v_cycle);
        tv_time = (TextView) findViewById(R.id.tv_time);
        rl_content_item = (RelativeLayout) findViewById(R.id.rl_content_item);
        tv_content_msg = (TextView) findViewById(R.id.tv_content_msg);
        v_state = findViewById(R.id.v_state);
        ll_content = (LinearLayout) findViewById(R.id.ll_content);
        v_head = findViewById(R.id.v_head);
        v_foot = findViewById(R.id.v_foot);
       /* final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TimeLineMarkerView);
        markerSize = a.getDimensionPixelSize(R.styleable.TimeLineMarkerView_markerSize, markerSize);
        // setMarkerSize(markerSize);
        lineSize = a.getDimensionPixelSize(R.styleable.TimeLineMarkerView_lineSize, lineSize);
        // setLineSize(lineSize);
        markerColor = a.getResourceId(R.styleable.TimeLineMarkerView_markerColor, R.color.black_alpha_50);
        //   setMarkerColor(markerColor);
        lineColor = a.getResourceId(R.styleable.TimeLineMarkerView_lineColor, R.color.black_alpha_50);
        //  setLineColor(lineColor);
        a.recycle();*/
    }


    /**
     * 设置Marker的大小
     *
     * @param markerSize
     */
    public void setMarkerSize(int markerSize) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) v_cycle.getLayoutParams();
        params.width = markerSize;
        params.height = markerSize;
        v_cycle.setLayoutParams(params);
    }

    /**
     * 设置Marker的颜色
     *
     * @param rd
     */
    public void setMarkerColor(int rd) {
        v_cycle.setBackgroundResource(rd);
    }

    /**
     * 设置线条的宽度
     *
     * @param l
     */
    public void setLineSize(int l) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) v_head.getLayoutParams();
        params.width = l;
        v_head.setLayoutParams(params);
        RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) v_foot.getLayoutParams();
        params.width = l;
        v_head.setLayoutParams(params1);
    }

    /**
     * 修改线的颜色
     *
     * @param rd
     */
    public void setLineColor(int rd) {
        v_head.setBackgroundResource(rd);
        v_foot.setBackgroundResource(rd);
    }

    /**
     * 设置故障代码状态
     *
     * @param rd
     */
    public void setStateDrawable(int rd) {
        v_state.setBackgroundResource(rd);
    }

    /**
     * 设置故障时间
     *
     * @param sTime
     */
    public void setTime(String sTime) {
        tv_time.setText(sTime);
    }

    /**
     * 设置故障的详细信息
     *
     * @param cs
     */
    public void setContentMsg(String cs) {
        tv_content_msg.setText(cs);
    }

    /**
     * 动态的添加故障信息条目
     *
     * @param contentMsg
     * @param state
     */
    public void addContentItem(String contentMsg, int state) {
       /* RelativeLayout tempItem = (RelativeLayout) View.inflate(getContext(), R.layout.obd_fault_des_content, null);
        TextView tempContentMsg = (TextView) tempItem.findViewById(R.id.tv_content_msg);
        View tempState = tempItem.findViewById(R.id.v_state);
        tempContentMsg.setText(contentMsg);
        tempState.setBackgroundResource(R.drawable.ic_timeline_default_marker);
        tempItem.removeAllViews();
        tempItem.addView(tempContentMsg);
        tempItem.addView(tempState);
        ll_content.addView(tempItem);*/
    }

    /**
     * 设置头线的可见性
     */
    public void setHeadLineVisible() {
        v_head.setVisibility(View.GONE);
        if (ChappieCarApplication.isDebug)
        Log.e(TAG, "头部英藏");
    }

    private static final String TAG = "自定义组合控件";

    /**
     * 设置尾布局的可见性
     *
     * @param
     */
    public void setFootLineVissible() {
        v_foot.setVisibility(View.GONE);
        if (ChappieCarApplication.isDebug)
        Log.e(TAG, "尾部英藏");
    }
}
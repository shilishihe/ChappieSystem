package com.scblhkj.carluncher.widget.hellocharts.formatter;


import com.scblhkj.carluncher.widget.hellocharts.model.PointValue;

public interface LineChartValueFormatter {

    public int formatChartValue(char[] formattedValue, PointValue value);
}

package com.scblhkj.carluncher.widget.hellocharts.formatter;


import com.scblhkj.carluncher.widget.hellocharts.model.SubcolumnValue;

public interface ColumnChartValueFormatter {

    public int formatChartValue(char[] formattedValue, SubcolumnValue value);

}

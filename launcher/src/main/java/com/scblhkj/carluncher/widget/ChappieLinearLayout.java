package com.scblhkj.carluncher.widget;


import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.scblhkj.carluncher.app.ChappieCarApplication;

public class ChappieLinearLayout extends LinearLayout {

    private Context context;

    public ChappieLinearLayout(Context context) {
        super(context);
        this.context = context;
    }

    public ChappieLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public ChappieLinearLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
    }

    /**
     * 添加功能块
     *
     * @param itemID
     */
    public View addItem2End(int itemID) {
        View itemView = View.inflate(context, itemID, null);
        LayoutParams params = new LayoutParams(ChappieCarApplication.SCREEN_WIDTH / 5, (int) (ChappieCarApplication.SCREEN_HEIGHT * 0.95));
        params.gravity = Gravity.CENTER_VERTICAL;
        // 此处的margin top 和 bottom无效
        params.setMargins(0, 0, 0, 0);
        itemView.setLayoutParams(params);
        this.addView(itemView);
        return itemView;
    }

    public void removeItemByID(int itemID) {
        View itemView = this.findViewById(itemID);
        this.removeView(itemView);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        //此句代码是为了通知他的父ViewPager现在进行的是本控件的操作，不要对我的操作进行干扰
        getParent().requestDisallowInterceptTouchEvent(true);
        return true;
    }
}

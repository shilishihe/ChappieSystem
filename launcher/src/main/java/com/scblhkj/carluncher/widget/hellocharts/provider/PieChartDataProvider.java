package com.scblhkj.carluncher.widget.hellocharts.provider;


import com.scblhkj.carluncher.widget.hellocharts.model.PieChartData;

public interface PieChartDataProvider {

    public PieChartData getPieChartData();

    public void setPieChartData(PieChartData data);

}

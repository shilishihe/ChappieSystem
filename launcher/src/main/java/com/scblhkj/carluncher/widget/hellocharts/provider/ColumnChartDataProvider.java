package com.scblhkj.carluncher.widget.hellocharts.provider;


import com.scblhkj.carluncher.widget.hellocharts.model.ColumnChartData;

public interface ColumnChartDataProvider {

    public ColumnChartData getColumnChartData();

    public void setColumnChartData(ColumnChartData data);

}

package com.scblhkj.carluncher.widget.hellocharts.model;

public enum ValueShape {
    CIRCLE, SQUARE, DIAMOND
}

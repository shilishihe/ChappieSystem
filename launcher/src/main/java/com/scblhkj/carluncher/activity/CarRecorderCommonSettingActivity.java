package com.scblhkj.carluncher.activity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.rey.material.widget.Switch;
import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.utils.GPSUtil;
import com.scblhkj.carluncher.utils.SettingManager;
import com.scblhkj.carluncher.utils.ToastUtil;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 行车记录仪通用设置
 */
public class CarRecorderCommonSettingActivity extends BaseActivity implements View.OnClickListener
        , RadioGroup.OnCheckedChangeListener
        , Switch.OnCheckedChangeListener {

    @Bind(R.id.iv_left)
    ImageView ivLeft;
    @Bind(R.id.asc_title)
    TextView ascTitle;
    @Bind(R.id.sw_gps)
    Switch swGps;
    @Bind(R.id.sw_record_voice)
    Switch swRecordVoice;
    @Bind(R.id.sw_vibration_analysis)
    Switch swVibrationAnalysis;
    @Bind(R.id.sw_remote_control_main_camera)
    Switch swRemoteControlMainCamera;
    @Bind(R.id.sw_remote_control_second_camera)
    Switch swRemoteControlSecondCamera;
    @Bind(R.id.sw_location_shared)
    Switch swLocationShared;
    @Bind(R.id.sw_adas)
    Switch swAdas;
    @Bind(R.id.rb_10)
    RadioButton rb10;
    @Bind(R.id.rb_20)
    RadioButton rb20;
    @Bind(R.id.rb_30)
    RadioButton rb30;
    @Bind(R.id.rb_40)
    RadioButton rb40;
    @Bind(R.id.rb_50)
    RadioButton rb50;
    @Bind(R.id.rb_60)
    RadioButton rb60;
    @Bind(R.id.rg_vibration_time_delay_setting)
    RadioGroup rgVibrationTimeDelaySetting;
    @Bind(R.id.rb_heigh)
    RadioButton rbHeigh;
    @Bind(R.id.rb_middle)
    RadioButton rbMiddle;
    @Bind(R.id.rb_low)
    RadioButton rbLow;
    @Bind(R.id.rg_vibration_monitoring_level)
    RadioGroup rgVibrationMonitoringLevel;
    @Bind(R.id.rb_safe)
    RadioButton rbSafe;
    @Bind(R.id.rb_low_power)
    RadioButton rbLowPower;
    @Bind(R.id.rb_heigh_quality)
    RadioButton rbHeighQuality;
    @Bind(R.id.rg_power_mode_settings)
    RadioGroup rgPowerModeSettings;
    @Bind(R.id.bt_default_setting)
    Button btDefaultSetting;
    private SettingManager settingManager; // 设置管理器

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_recorder_common_setting);
        ButterKnife.bind(this);
        initView();
        initData();
    }


    @Override
    protected void initView() {
    }

    @Override
    protected void initData() {
        settingManager = SettingManager.getInstance();
        ivLeft.setOnClickListener(this);
        btDefaultSetting.setOnClickListener(this);
        swGps.setOnCheckedChangeListener(this);
        swRecordVoice.setOnCheckedChangeListener(this);
        swVibrationAnalysis.setOnCheckedChangeListener(this);
        swRemoteControlMainCamera.setOnCheckedChangeListener(this);
        swRemoteControlSecondCamera.setOnCheckedChangeListener(this);
        swLocationShared.setOnCheckedChangeListener(this);
        swAdas.setOnCheckedChangeListener(this);
        rgVibrationTimeDelaySetting.setOnCheckedChangeListener(this);
        rgVibrationMonitoringLevel.setOnCheckedChangeListener(this);
        rgPowerModeSettings.setOnCheckedChangeListener(this);
        initData4SP();
        if (GPSUtil.isGPSEnable(this)) {
            Log.e(TAG, "GPS打开中");
        } else {
            Log.e(TAG, "GPS关闭中");
        }
    }

    private static final String TAG = "后视镜通用设置";

    /**
     * 从Sp中获取保存的控件属性值
     */
    private void initData4SP() {
        swGps.setChecked(settingManager.isSwGps());
        swRecordVoice.setChecked(settingManager.isSwRecordVoice());
        swVibrationAnalysis.setChecked(settingManager.isSwVibrationAnalysis());
        swRemoteControlMainCamera.setChecked(settingManager.isSwRemoteControlMainCamera());
        swRemoteControlSecondCamera.setChecked(settingManager.isSwRemoteControlSecondCamera());
        swLocationShared.setChecked(settingManager.isSwLocationShared());
        swAdas.setChecked(settingManager.isSwAdas());
        switch (settingManager.getRgVibrationTimeDelaySetting()) {
            case "10": {
                rb10.setChecked(true);
                break;
            }
            case "20": {
                rb20.setChecked(true);
                break;
            }
            case "30": {
                rb30.setChecked(true);
                break;
            }
            case "40": {
                rb40.setChecked(true);
                break;
            }
            case "50": {
                rb50.setChecked(true);
                break;
            }
            case "60": {
                rb60.setChecked(true);
                break;
            }
            default: {
                break;
            }
        }

        switch (settingManager.getRgVibrationMonitoringLevel()) {
            case "8": {
                rbHeigh.setChecked(true);
                break;
            }

            case "10": {
                rbMiddle.setChecked(true);
                break;
            }

            case "12": {
                rbLow.setChecked(true);
                break;
            }

            default: {
                break;
            }


        }

        switch (settingManager.getRgPowerModeSettings()) {
            case "安全模式": {
                rbSafe.setChecked(true);
                break;
            }
            case "低耗模式": {
                rbLowPower.setChecked(true);
                break;
            }
            case "高质模式": {
                rbHeighQuality.setChecked(true);
                break;
            }
            default: {
                break;
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_left: {
                finish();
                break;
            }
            case R.id.bt_default_setting: { // 恢复默认设置
                ToastUtil.showToast("恢复默认设置");
                break;
            }
            default: {
                break;
            }
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (group.getId()) {
            case R.id.rg_vibration_time_delay_setting: {  // 振动延时设置
                switch (checkedId) {
                    case R.id.rb_10: {
                        settingManager.setRgVibrationTimeDelaySetting("10");
                        break;
                    }
                    case R.id.rb_20: {
                        settingManager.setRgVibrationTimeDelaySetting("20");
                        break;
                    }
                    case R.id.rb_30: {
                        settingManager.setRgVibrationTimeDelaySetting("30");
                        break;
                    }
                    case R.id.rb_40: {
                        settingManager.setRgVibrationTimeDelaySetting("40");
                        break;
                    }
                    case R.id.rb_50: {
                        settingManager.setRgVibrationTimeDelaySetting("50");
                        break;
                    }
                    case R.id.rb_60: {
                        settingManager.setRgVibrationTimeDelaySetting("60");
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
            }
            case R.id.rg_vibration_monitoring_level: {  // 振动告警级别
                switch (checkedId) {
                    case R.id.rb_heigh: {   // 高
                        settingManager.setRgVibrationMonitoringLevel("8");
                        break;
                    }
                    case R.id.rb_middle: { // 中
                        settingManager.setRgVibrationMonitoringLevel("10");
                        break;
                    }
                    case R.id.rb_low: {  // 低
                        settingManager.setRgVibrationMonitoringLevel("12");
                        break;
                    }
                    default: {
                        break;
                    }
                }
                sendBroacCast2CollisionService();
                break;
            }

            case R.id.rg_power_mode_settings: { // 功耗模式
                switch (checkedId) {
                    case R.id.rb_safe: {    // 安全模式
                        settingManager.setRgPowerModeSettings("安全模式");
                        break;
                    }
                    case R.id.rb_low_power: {  // 低功耗模式
                        settingManager.setRgPowerModeSettings("低耗模式");
                        break;
                    }
                    case R.id.rb_heigh_quality: {  // 高品质模式
                        settingManager.setRgPowerModeSettings("高质模式");
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
            }
            default: {
                break;
            }
        }
    }

    /**
     * 发送
     */
    private void sendBroacCast2CollisionService() {
        Intent intent = new Intent();
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setAction(ChappieConstant.ACTION_REMOTE_COLLISION_SERVICE);
        intent.putExtra(ChappieConstant.ACTION_REMOTE_COLLISION_SERVICE_VALUE, settingManager.getRgVibrationMonitoringLevel());
        sendBroadcast(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (GPSUtil.isGPSEnable(this)) {
                swGps.setChecked(true);
            } else {
                swGps.setChecked(false);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCheckedChanged(Switch view, boolean checked) {
        switch (view.getId()) {
            case R.id.sw_gps: {
                settingManager.setSwGps(checked);
                // 转到手机设置界面，用户设置GPS
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 0); // 设置完成后返回到原来的界面
                break;
            }
            case R.id.sw_record_voice: {
                settingManager.setSwRecordVoice(checked);
                break;
            }
            case R.id.sw_vibration_analysis: {
                settingManager.setSwVibrationAnalysis(checked);
                if (settingManager.isSwVibrationAnalysis()) {   // 开启震动监测
                    Intent intent = new Intent();
                    intent.setAction("com.scblhkj.carluncher.carcorder.service.CarCollisionDetectionService");
                    intent.setPackage("com.scblhkj.carluncher.carcorder");  //Android5.0后service不能采用隐式启动，故此处加上包名
                    startService(intent);

                } else {  // 关闭震动监测
                    Intent intent = new Intent();
                    intent.setAction("com.scblhkj.carluncher.carcorder.service.CarCollisionDetectionService");
                    intent.setPackage("com.scblhkj.carluncher.carcorder");  //Android5.0后service不能采用隐式启动，故此处加上包名
                    stopService(intent);
                }
                break;
            }
            case R.id.sw_remote_control_main_camera: {
                settingManager.setSwRemoteControlMainCamera(checked);
                break;
            }
            case R.id.sw_remote_control_second_camera: {
                settingManager.setSwRemoteControlSecondCamera(checked);
                break;
            }
            case R.id.sw_location_shared: {
                settingManager.setSwLocationShared(checked);
                break;
            }
            case R.id.sw_adas: {
                settingManager.setSwAdas(checked);
                break;
            }
            default: {
                break;
            }
        }
    }
}

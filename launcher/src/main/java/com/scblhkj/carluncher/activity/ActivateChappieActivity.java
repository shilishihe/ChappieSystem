package com.scblhkj.carluncher.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.WriterException;
import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.utils.EncodingHandler;
import com.scblhkj.carluncher.utils.QRUtil;

/**
 * 激活查派设备界面
 * 1.展示生成二维码图片，供用户扫描
 */
public class ActivateChappieActivity extends BaseActivity {

    private static final String TAG = "ActivateChappieActivity";
    private Bitmap logo;
    private ImageView im_rqAppDL; // 用于生成下载手机APP的二维码
    private TextView tv_content; // 生成的二维码内容

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activate_chappie);
        initView();
        initData();
    }

    @Override
    protected void initView() {
        im_rqAppDL = (ImageView) findViewById(R.id.im_rqAppDL);
        tv_content = (TextView) findViewById(R.id.tv_content);
    }

    @Override
    protected void initData() {
        String content = ChappieCarApplication.DEVICE_IMEI + "_http://www.blhcp.com/down/ios.html";
        tv_content.setText(content);
        logo = BitmapFactory.decodeResource(super.getResources(), R.mipmap.cp_logo);
        Bitmap bm = null;
        try {
            bm = EncodingHandler.createQRCode(content, 300);
            im_rqAppDL.setImageBitmap(bm);
        } catch (WriterException e) {
        }

    }

}

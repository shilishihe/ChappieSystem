package com.scblhkj.carluncher.activity;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.WriterException;
import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.utils.EncodingHandler;
import com.scblhkj.carluncher.utils.ToastUtil;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**二维码界面
 * Created by blh on 2016-11-09.
 */
public class ImeiQrcodeActivity extends BaseActivity {
    private String imei;
    private ImageView iv_qrCode;
    private TextView tv_qrCode;
    private String imageUrl="/storage/sdcard1/imeiQrcode.png";
    private AlertDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_imeiqrcode);
        initView();
        initData();
    }

    /***
     * 文件是否存在
     * @param filePath
     * @return
     */
    private boolean isFileExist(String filePath){
        File file=new File(filePath);
        try{
            if(!file.exists()){
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return  false;
        }
        return true;
    }
    @Override
    protected void initView() {
        try{
            //获取IMEI
            TelephonyManager tm= (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);
            imei=tm.getDeviceId();
            //初始化界面
            iv_qrCode=(ImageView)findViewById(R.id.iv_imeiqrcode);
            tv_qrCode=(TextView)findViewById(R.id.tv_imeiqrcode);
            dialog=new AlertDialog.Builder(ImeiQrcodeActivity.this).create();
            dialog.setMessage("请稍等...");
            dialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    protected void initData() {
        //根据IMEI生产二维码图
         //if(!isFileExist(imageUrl)){ //如果二维码图片不存在就生成二维码并保存在本地
             try {
                 Log.e("info", "没有二维码图片,正在重新生成");
                 Bitmap qrBitmap= EncodingHandler.createQRCode(imei,400);
                 saveMyBitmap(qrBitmap, "imeiQrcode");
                 iv_qrCode.setImageBitmap(qrBitmap);
                 tv_qrCode.setText(imei);
                 dialog.dismiss();
             } catch (WriterException e) {
                  e.printStackTrace();
             }
    }

    /**
     * Save Bitmap to a file.保存图片到SD卡。
     *
     *
     * @return error message if the saving is failed. null if the saving is
     *         successful.
     * @throws IOException
     */
    public void saveMyBitmap(Bitmap mBitmap,String bitName)  {
        File f = new File( "/storage/sdcard1/"+bitName + ".jpg");
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

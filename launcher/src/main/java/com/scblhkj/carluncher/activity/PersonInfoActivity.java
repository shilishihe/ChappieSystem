package com.scblhkj.carluncher.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.utils.GsonUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;

/**
 * 个人信息界面
 */
public class PersonInfoActivity extends BaseActivity implements View.OnClickListener {

    @Bind(R.id.iv_left)
    ImageView ivLeft;
    @Bind(R.id.asc_title)
    TextView ascTitle;
    @Bind(R.id.iv_right_qr)
    ImageView ivRightQr;
    @Bind(R.id.cv_head)
    CircleImageView cvHead;
    @Bind(R.id.tv_user_name)
    TextView tvUserName;
    @Bind(R.id.tv_nickname)
    TextView tvNickname;
    @Bind(R.id.tv_nickname_modify)
    TextView tvNicknameModify;
    @Bind(R.id.tv_style_word)
    TextView tvStyleWord;
    @Bind(R.id.tv_style_word_modify)
    TextView tvStyleWordModify;
    @Bind(R.id.tv_emergency_contact)
    TextView tvEmergencyContact;
    @Bind(R.id.tv_emergency_contact_modify)
    TextView tvEmergencyContactModify;
    @Bind(R.id.tv_person_id)
    TextView tvPersonId;
    @Bind(R.id.tv_person_id_modify)
    TextView tvPersonIdModify;
    @Bind(R.id.tv_area)
    TextView tvArea;
    @Bind(R.id.tv_date)
    TextView tvDate;
    @Bind(R.id.bt_confirm)
    Button btConfirm;
    @Bind(R.id.im_user_qr)
    ImageView imUserQr;
    @Bind(R.id.rl_qr)
    RelativeLayout rlQr;
    @Bind(R.id.rbBoy)
    RadioButton rbBoy;
    @Bind(R.id.rbGilr)
    RadioButton rbGilr;
    @Bind(R.id.rgSex)
    RadioGroup rgSex;

    private Bitmap logo;  // 二维码中间logo
    private Bitmap qrBm;  // 二维码
    private boolean isQRViewShow = false;
    private static final String TAG = "PersonInfoActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_info);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        ivLeft.setOnClickListener(this);
        ivRightQr.setOnClickListener(this);
        btConfirm.setOnClickListener(this);
        // http://localhost/app/RMUserInfo/rest/IEMI/设备号
        String url = "http://" + ChappieConstant.HTTP_HOST + "/app/RMUserInfo/rest/IEMI/" + ChappieCarApplication.DEVICE_IMEI;
        Log.e(TAG, "url = " + url);
        OkHttpUtils.get().url(url).build().execute(new StringCallback() {

            @Override
            public void onError(Call call, Exception e) {
                Log.e(TAG, "获取个人信息失败" + e.toString());
            }

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "response = " + response);
                PersonInfoBean bean = GsonUtil.jsonToBean(response, PersonInfoBean.class);
                if (bean != null) {
                    if (bean.status.equals("1")) {
                        initViewData(bean);
                    } else {
                        Log.e(TAG, "对象状态为0,异常");
                    }
                } else {
                    Log.e(TAG, "个人信息回调数据转对象失败" + response);
                }
            }
        });
    }

    /**
     * 请求网络数据填充控件
     *
     * @param bean
     */
    private void initViewData(PersonInfoBean bean) {
        if (bean.status.equals("0")) {
            Log.e(TAG, "请求接口返回的数据有问题");
        } else if (bean.status.equals("1")) {
            tvUserName.setText(bean.user_login_name);
            tvNickname.setText(bean.user_nickname);
            tvStyleWord.setText(bean.personal_signature);
            tvEmergencyContact.setText(bean.emergency_contact_person);
            tvPersonId.setText(bean.ID_number);
            tvArea.setText(bean.area);
            tvDate.setText(bean.get_driving_license_date);
            initSex(bean.sex);
            // 图像格式是base64格式的一个字符串
            String base64HeadS = bean.header_pic;
            base64HeadS = base64HeadS.replace('-', '=').replace('*', '/').replace('_', '+');
            byte[] decode = Base64.decode(base64HeadS, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(decode, 0, decode.length);
            Drawable drawable = new BitmapDrawable(bitmap);
            cvHead.setImageDrawable(drawable);
            //imHead.setImageDrawable(drawable);
        }
    }

    /**
     * 初始化性别
     *
     * @param sex
     */
    private void initSex(String sex) {
        if ("男".equals(sex)) {
            rbBoy.setChecked(true);
        } else if ("女".equals(sex)) {
            rbGilr.setChecked(true);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_left: {
                finish();
                break;
            }
            case R.id.iv_right_qr: {
                if (isQRViewShow) {
                    goneQRView();
                } else {
                    showQRView();
                }
                break;
            }
            case R.id.bt_confirm: {
                break;
            }
            default: {
                break;
            }
        }
    }

    private void showQRView() {
        isQRViewShow = true;
        String qrContent = "Leo Love Marceau Forever";
        rlQr.setVisibility(View.VISIBLE);
        if (logo == null) {
            logo = BitmapFactory.decodeResource(super.getResources(), R.mipmap.cp_logo);
        }
        if (qrBm == null) {
          //  qrBm = QRUtil.createQRCode(qrContent, logo);
        }
        imUserQr.setImageBitmap(qrBm);
    }

    private void goneQRView() {
        isQRViewShow = false;
        rlQr.setVisibility(View.GONE);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (isQRViewShow) {
                goneQRView();
            } else {
                finish();
            }
            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    private class PersonInfoBean {
        public String ID_number;
        public String age;
        public String area;
        public String emergency_contact_person;
        public String get_driving_license_date;
        public String header_pic;
        public String message;
        public String personal_signature;
        public String real_name;
        public String sex;
        public String status;
        public String user_login_name;
        public String user_nickname;
    }

}

package com.scblhkj.carluncher.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.adapter.MyCarInfoAdapter;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.domain.MyCarInfoBean;
import com.scblhkj.carluncher.utils.GsonUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * 我的汽车详情
 */
public class MyCarInfoActivity extends BaseActivity implements View.OnClickListener {

    @Bind(R.id.iv_left)
    ImageView ivLeft;
    @Bind(R.id.lv)
    ListView lv;
    @Bind(R.id.bt_confirm)
    Button btConfirm;
    private MyCarInfoAdapter adapter;
    private static final String TAG = "MyCarInfoActivity";
    private List<MyCarInfoBean> dataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_car_info);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        ivLeft.setOnClickListener(this);
        btConfirm.setOnClickListener(this);
        adapter = new MyCarInfoAdapter(this);
        lv.setAdapter(adapter);
        fillData();
    }

    /**
     * 填充数据
     */
    private void fillData() {
        String url = "http://" + ChappieConstant.HTTP_HOST + "/app/RMCar/rest/IEMI/" + ChappieCarApplication.DEVICE_IMEI;
        Log.e(TAG, "url = " + url);
        OkHttpUtils.get().url(url).build().execute(new StringCallback() {

            @Override
            public void onError(Call call, Exception e) {
                Log.e(TAG, "获取车辆信息失败" + e.toString());
            }

            @Override
            public void onResponse(String response) {
                if (!TextUtils.isEmpty(response)) {
                    CarInfoBean bean = GsonUtil.jsonToBean(response, CarInfoBean.class);
                    Log.e(TAG, response);
                    if (bean != null) {
                        if ("1".equals(bean.status)) {  // 返回值正常
                            dataList = make2DataList(bean);
                            adapter.setList(dataList);
                        } else {
                            Log.e(TAG, "获取汽车数据异常");
                        }
                    }
                }
            }
        });

    }

    /**
     * 将服务器取到的实体数据类整合成一个list
     *
     * @param bean
     */
    private List<MyCarInfoBean> make2DataList(CarInfoBean bean) {
        List<MyCarInfoBean> tempist = new ArrayList<MyCarInfoBean>();
        MyCarInfoBean carMode = new MyCarInfoBean();
        carMode.setTitle("车型");
        carMode.setDes(bean.car_factory + bean.car_brand + bean.car_mode + bean.car_year + bean.car_type);
        MyCarInfoBean oilType = new MyCarInfoBean();
        oilType.setTitle("油耗");
        oilType.setDes(bean.car_oil);
        MyCarInfoBean mLastKm = new MyCarInfoBean();
        mLastKm.setTitle("上次保养里程");
        mLastKm.setDes(bean.car_maintenance_last_km);
        MyCarInfoBean carKm = new MyCarInfoBean();
        carKm.setTitle("仪表盘里程");
        carKm.setDes(bean.car_km);
        MyCarInfoBean buyDate = new MyCarInfoBean();
        buyDate.setTitle("车辆购买日期");
        buyDate.setDes(bean.car_buy_date);
        MyCarInfoBean carNumber = new MyCarInfoBean();
        carNumber.setTitle("车牌号");
        carNumber.setDes(bean.car_number);
        MyCarInfoBean carFrame = new MyCarInfoBean();
        carFrame.setTitle("车架号");
        carFrame.setDes(bean.car_frame);
        MyCarInfoBean carEngine = new MyCarInfoBean();
        carEngine.setTitle("发动机号");
        carEngine.setDes(bean.car_engine);
        MyCarInfoBean insuraFactory = new MyCarInfoBean();
        insuraFactory.setTitle("保险公司");
        insuraFactory.setDes(bean.car_insurance);
        tempist.add(carMode);
        tempist.add(oilType);
        tempist.add(mLastKm);
        tempist.add(carKm);
        tempist.add(buyDate);
        tempist.add(carNumber);
        tempist.add(carFrame);
        tempist.add(carEngine);
        tempist.add(insuraFactory);
        return tempist;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_left: {
                finish();
                break;
            }
            default: {
                break;
            }
        }
    }

    private class CarInfoBean {
        public String car_brand;
        public String car_buy_date;
        public String car_cc;
        public String car_engine;
        public String car_factory;
        public String car_frame;
        public String car_id;
        public String car_insurance;
        public String car_insurance_buy_date;
        public String car_km;
        public String car_maintenance_last_km;
        public String car_mode;
        public String car_number;
        public String car_oil;
        public String car_type;
        public String car_year;
        public String message;
        public String status;
    }
}

package com.scblhkj.carluncher.activity;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.Instrumentation;
import android.app.ProgressDialog;
import android.app.Service;
import android.app.admin.DevicePolicyManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.adapter.ChappieFragmentPagerAdapter;
import com.scblhkj.carluncher.adapter.LauncherAdapter;
import com.scblhkj.carluncher.app.ChappieCarApplication;

import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.fragment.TPMSEmitterMatchingFragment;
import com.scblhkj.carluncher.fragment.TPMSMainFragment;
import com.scblhkj.carluncher.fragment.TPMSSettingFragment;
import com.scblhkj.carluncher.fragment.TPMStyreChangingFragment;
import com.scblhkj.carluncher.fragment.TimeWeatherFragment;
import com.scblhkj.carluncher.receiver.LockReceiver;
import com.scblhkj.carluncher.service.CCLuncherCoreService;
import com.scblhkj.carluncher.service.ChappieWifiHotService;
import com.scblhkj.carluncher.service.DamenService;
import com.scblhkj.carluncher.service.SerialDataCommSerivce;
import com.scblhkj.carluncher.utils.BluetoothUtil;
import com.scblhkj.carluncher.utils.BrightnessTools;
import com.scblhkj.carluncher.utils.FloatManager;
import com.scblhkj.carluncher.utils.NetworkUtil;
import com.scblhkj.carluncher.utils.SDCardScannerUtil;
import com.scblhkj.carluncher.utils.SignalUtil;
import com.scblhkj.carluncher.utils.StartAppUtil;
import com.scblhkj.carluncher.utils.ToastUtil;
import com.scblhkj.carluncher.widget.DesktopLayout;
import com.scblhkj.carluncher.widget.MyGridView;
import com.scblhkj.carluncher.widget.TopDeskLayout;
import com.txznet.sdk.TXZAsrManager;
import com.txznet.sdk.TXZConfigManager;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 主界面
 */
public class MainActivity extends BaseActivity implements View.OnClickListener
        , View.OnTouchListener
        , GestureDetector.OnGestureListener
        , TPMSMainFragment.TPMSFragmentInterface
        // ,TPMSSettingFragment.OnTPMSSettingFragmentInterface
        , TPMSEmitterMatchingFragment.TMPSEmitterMatchingFragmentInterface
        , TPMStyreChangingFragment.OnTPMStyreChangingFragmentInterface
{

    private WindowManager mWindowManager;
    private WindowManager.LayoutParams mLayout;
    private DesktopLayout mDesktopLayout;
    private long startTime;
    ChappieWifiHotService chappieWifiHotService;
    // 声明屏幕的宽高
    float x, y;
    int top;

    private static String TAG = "MainActivity";
    private List<Fragment> fragments;
    private ChappieFragmentPagerAdapter cAdapter;
    public FragmentManager fm;
    private GestureDetector gestureDetector;
    private ImageView iv_state_airmode;  // 飞行模式
    private ImageView ic_state_wifi; // wifi状态图标
    private ImageView ic_state_signal; //信号图标
    private NetworkStateReceiver networkStateReceiver; //监听飞行模式的广播接收者
    // private WifiStateReceiver wifiStateReceiver; //监听wifi状态的广播接收者
    private MyPhoneStateListener myPhoneStateListener;  //手机状态的监听类
    private PowerConnectionReceiver powerConnectionReceiver; //检测电池电量的广播接收者
    private BroadcastReceiver serialCommReceiver; // 接收串口发送数据的广播接收者
    private TelephonyManager telephonyManager;
    private int simState; //sim卡状态信息
    private TextView tv_state_sinal_name;  //运营商名称
    private TextView tv_temperature; // 当前的城市天气
    private ImageView iv_state_bluetooth; //蓝牙状态图标
    private MyGridView myGridView;
    private ViewPager viewPager;
    private TimeWeatherFragment timeWeatherFragment;

    //屏幕宽度
    int screenWidth;
    //当前选中的项
    int currenttab=-1;
    //  private BroadcastReceiver accBroacastReceiver;

    // private ChappieLinearLayout ll_content;
    /****
     *  private View item_content_car_state; // 车辆状态模块
     private View item_tpms_watch;  // 胎压监测模块
     private View item_content_journey; // 行程模块
     private View item_navi; // 导航模块
     private View item_file_manager; // 添加模块
     private View item_monitor; //视频监控模块
     private View item_music; // 音乐模块
     private View item_contacts; // 通讯录
     private View item_weather; // 天气
     private View item_setting; // 设置
     private View item_transport_condition; // 行车状态
     private View item_me;  // 我的查派
     private View item_fm; // FM发射
     private View item_btp; // 蓝牙电话
     private View item_app_store;// 应用市场
     private View item_car_control; // 车辆控制
     private View item_message; // 消息
     */

    // 胎压监测主Frament
    private TPMSMainFragment tpmsMainFragment;
    // 胎压设置Fragment
    private TPMSSettingFragment tpmsSettingFragment;
    // 胎压发射配对界面
    private TPMSEmitterMatchingFragment tmpsEmitterMatchingFragment;
    // 轮胎调换界面
    private TPMStyreChangingFragment tpmStyreChangingFragment;

    /**
     * 手势
     */
    private DevicePolicyManager policyManager;
    private ComponentName componentName;
    private ArrayList<Fragment> viewList;
    private ImageView back_selector,home_selector;
    private ImageView im_state_bluetoothInfo;
    private ImageView ic_state_wifiInfo;
    private boolean bFlag;
    private ProgressDialog dialog1;
    private ImageView iv_state_lock_if;
    private WifiStateReceiver wifiStateReceiver;
    private Intent testActivityIntent;
    private BluetoothAdapter bluetoothAdapter;
    private ImageView voice_selector;

    private ImageView iv_state_recorder;
    private String homePackageNames;
    private int layoutState=0;
    private TopDeskLayout topDeskLayout1;
    private FrameLayout topDeskLayout;
    private LinearLayout top_back_selector,top_home_selector,top_voice_selector;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private DeskLayoutStateReceiver deskLayoutStateReceiver;
    private String windowMangerSate;
    private MyServiceConnection conn;
    private ChappieWifiHotService.MyBinder myBinder;
    ChappieCarApplication application;
    private WifihotRecevier registerWifiHotStateReceiver;
    private FloatManager manager;
    private ImageView iv_chappie;
    private Boolean isShow=false;
    private LinearLayout ll_menu;
    private String pName,aName;
    private boolean isRecorder;
    private boolean isRec=false;
    private boolean notRecorder;
    private boolean isSettingStartApp;//是否有设置过开机启动APP配置
    private String packageTime,packageImei,packageName;//开机启动APP的时间戳,imei,包名
    VideoNotRecorderReceiver videoNotRecorderReceiver;
    VideoIsRecorderReceiver videoIsRecorderReceiver;
    BroadcastReceiver settingStartAppReceiver;
    DataInputStream dis ;
    OutputStream ou ;
    InputStream is;
    FileOutputStream fos;
    private String imei;
    private static final int START_HDDRV=0X01;
    private Dialog mRecordDialog;
    private boolean isTalkEachOther;//是否配置过语音对讲功能
    private int roomNum;//语音对讲功能的房号
    private boolean isSetSystemSetting;

    public MainActivity() {
    }
    private Handler startAppHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case START_HDDRV :
                    if(SDCardScannerUtil.isSecondSDcardMounted()){
                        StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.scblhkj.carluncher.hddvr");
                    }else {
                        ToastUtil.showToast("没有安装SD卡，不能进入该功能!");
                    }
                    break;
                case 10001 ://如果配置开机启动的是行车记录仪或者相册,先要判断
                    if(sp.getString("packageNameSucc","").equals("com.scblhkj.carluncher.hddvr")||sp.getString("packageNameSucc","").equals("com.example.localalbum")){
                        if(SDCardScannerUtil.isSecondSDcardMounted()){
                            StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, sp.getString("packageNameSucc",""));
                        }else {
                            ToastUtil.showToast("没有安装SD卡，不能进入该功能!");
                        }
                    }else if(!sp.getString("packageNameSucc","").equals("com.scblhkj.carluncher.hddvr")){
                        StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, sp.getString("packageNameSucc",""));
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    };

    /***
     * 判断sd卡是否存在
     * @return
     */
    private boolean ExistSDCard() {
        if (android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED)) {
            return true;
        } else
            return false;
    }
    private void activeManager() {
        policyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        componentName = new ComponentName(this, LockReceiver.class);
        // 使用隐式意图调用系统方法来激活指定的设备管理器
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, componentName);
        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "一键锁屏");
        startActivity(intent);
        Log.e(TAG, "激活设备管理器");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //   setContentView(R.layout.activity_main);
        setContentView(R.layout.activity_launcher);
        application= (ChappieCarApplication) getApplication();
        /**
         * 初始化布局
         */
        sp=getSharedPreferences("deskstate", Activity.MODE_PRIVATE);
        editor=sp.edit();
        manager = new FloatManager(MainActivity.this);
        TelephonyManager tm = (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);
        imei=tm.getDeviceId();//String
        Log.e("info", "imei" + imei);
        editor.putString("IMEI", imei);
        editor.commit();
        initView();
        initData();
        registerDeskLayoutReceiver();
        if(sp.getBoolean("isSettingStartApp", isSettingStartApp)){ //如果有配置过开机启动的APP就启动
            Log.e("info", "mainActivity>>>sp.getString(isSettingStartApp>>>>" + sp.getBoolean("isStartNewApp", isSettingStartApp));
            Log.e("info", "mainActivity>>>sp.getString(packageName>>>>" + sp.getString("packageNameSucc", ""));
            startAppHandler.sendEmptyMessage(10001);
        }
        initSettingStartAppReceiver();
        registerSettingStartNewAppReceiver();
        activeManager();
        startCoreService();
        createWindowManager();
        createDesktopLayout();//创建桌面悬浮窗体
        mWindowManager.addView(mDesktopLayout, mLayout);
        layoutState=1;
        //windowMangerSate="1";
        //editor.putString("windowMangerSate", windowMangerSate);
        editor.putString("layoutState", "1");
        editor.commit();


    }

    private void createDesktopLayout() {
        mDesktopLayout = new DesktopLayout(this);
        back_selector=(ImageView)mDesktopLayout.findViewById(R.id.back_selector);
        back_selector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//返回键
                Log.e("info","接收到的aName"+aName+"pName:"+pName);
                if(aName.equals("com.scblhkj.carluncher.activity.MainActivity")&&pName.equals("com.scblhkj.carluncher")){
                    ToastUtil.showToast("你已经在桌面了");
                    return;
                }else if(!aName.equals("com.scblhkj.carluncher.activity.MainActivity")&&pName.equals("com.scblhkj.carluncher")){
                    Runtime runtime = Runtime.getRuntime();
                    try {
                        runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);
                    } catch (IOException e) { // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }else {
                    Runtime runtime = Runtime.getRuntime();
                    try {
                        runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);
                    } catch (IOException e) { // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                // BackKeyService.backKeyService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);

            }
        });
        home_selector=(ImageView)mDesktopLayout.findViewById(R.id.home_selector);
        home_selector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//home键

                //BackKeyService.backKeyService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME);
                Intent intent=new Intent(Intent.ACTION_MAIN);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addCategory(Intent.CATEGORY_HOME);
                startActivity(intent);
            }
        });

        voice_selector=(ImageView)mDesktopLayout.findViewById(R.id.voice_selector);
        voice_selector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(application.isTxzWakeUp==true){
                    TXZAsrManager.getInstance().start("请问有什么可以帮您?");
                }else if(application.isTxzWakeUp==false){
                    ToastUtil.showToast("请到设置中打开语音助手");
                }

                //TXZAsrManager.getInstance().triggerRecordButton();
            }
        });

    }
   private void showVoiceDialog(){
       if(mRecordDialog==null){
           mRecordDialog=new Dialog(MainActivity.this,R.style.Dialogstyle);
       }
   }
    private void createWindowManager() {
        // 取得系统窗体
        mWindowManager = (WindowManager) getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE);

        // 窗体的布局样式
        mLayout = new WindowManager.LayoutParams();

        // 设置窗体显示类型——TYPE_SYSTEM_ALERT(系统提示)
        mLayout.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;

        // 设置窗体焦点及触摸：
        // FLAG_NOT_FOCUSABLE(不能获得按键输入焦点)
        mLayout.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;

        // 设置显示的模式
        mLayout.format = PixelFormat.RGBA_8888;

        // 设置对齐的方法
        mLayout.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;

        // 设置窗体宽度和高度
        mLayout.width = WindowManager.LayoutParams.WRAP_CONTENT;
        mLayout.height = WindowManager.LayoutParams.MATCH_PARENT;
    }

    @Override
    protected void onResume() {
        // 初始化飞行模式图标
        setAirplaneIcon(NetworkUtil.isAirplaneModeOn(getApplicationContext()));
        registerNetWorkReceiver();
        registerWifiStateReceiver();
        registerBluetoothOnOffReceiver();
        registerPowerConnectionReceiver();
        registerWifiHotStateReceiver();
        registerExternalSdState();
        RegisterIsRecorderReceiver();
        RegisterNotRecorderReceiver();

        // registerSerialCommReceiver();
        // SIM卡状态
        simState = telephonyManager.getSimState();
        telephonyManager.listen(myPhoneStateListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        /**
         * 设置运营商名称
         */
        setTMobileName();
        /**
         * 设置蓝牙开关状态
         */
        if (BluetoothUtil.isBluetoothIsOpen()) {
            // 设置点亮蓝牙图标
            iv_state_bluetooth.setBackground(null);
        } else {
            // 设置不亮的蓝牙图标
            iv_state_bluetooth.setBackground(null);
        }
        //  registrAccBroacastReceiver();
        super.onResume();
    }

    @Override
    protected void onStart() {
        Log.e("info","MainActivity的onStart");

        super.onStart();
    }

    @Override
    protected void onPause() {
        unRegisterNetWorkReceiver();
        unRegisterWifiWorkReceiver();
        unRegiserBluetoothOnOffReceiver();
        unRegisterPowerConnectionReceiver();
        unRegisterExternalSdState();
        unRegisterWifiHotReceiver();
        unRegisterNotRecorderReceiver();
        unRegisterIsRecorderReceiver();
  Log.e("info","MainActivity的onPause");
        //unRegisterDeskLayoutReceiver();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        // 在程序销毁的时候要对服务解除绑定
        unbindService(conn);
        unRegisterDeskLayoutReceiver();
        unRegisterSettingStartNewAppReceiver();
        super.onDestroy();

    }
    /**
     * 注册监听wifi热点的广播接收者
     */
    private void registerWifiHotStateReceiver(){
        registerWifiHotStateReceiver=new WifihotRecevier();
        IntentFilter filter=new IntentFilter();
        filter.addAction("com.wifihotstate");
        registerReceiver(registerWifiHotStateReceiver, filter);
    }
    private void unRegisterWifiHotReceiver(){
        if(registerWifiHotStateReceiver!=null){
            unregisterReceiver(registerWifiHotStateReceiver);
        }
    }
    /**
     * 注册监听电池电量的广播接收者
     */
    private void registerPowerConnectionReceiver() {
        powerConnectionReceiver = new PowerConnectionReceiver();
        IntentFilter powerConnectionIntentFilter = new IntentFilter();
        powerConnectionIntentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        powerConnectionIntentFilter.addAction(Intent.ACTION_BATTERY_CHANGED); //监听电池电量的Action
        registerReceiver(powerConnectionReceiver, powerConnectionIntentFilter);
    }

    /**
     * 取消监听电池电量的广播接收者
     */
    private void unRegisterPowerConnectionReceiver() {
        if (powerConnectionReceiver != null) {
            unregisterReceiver(powerConnectionReceiver);
        }
    }

    private void registerDeskLayoutReceiver(){
        deskLayoutStateReceiver=new DeskLayoutStateReceiver();
        IntentFilter deskintentfilter=new IntentFilter();
        deskintentfilter.addAction("com.miya.action.damenService");
        registerReceiver(deskLayoutStateReceiver, deskintentfilter);
    }
    /**
     * 注册监听网络模式的广播接收者
     */
    private void registerNetWorkReceiver() {
        networkStateReceiver = new NetworkStateReceiver();
        IntentFilter networkIntentFilter = new IntentFilter();
        networkIntentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        networkIntentFilter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        registerReceiver(networkStateReceiver, networkIntentFilter);
    }
    /**
     * 判断某个服务是否正在运行的方法
     *
     * @param mContext
     * @param serviceName
     *            是包名+服务的类名（例如：com.example.apklock.AppService）
     * @return true代表正在运行，false代表服务没有正在运行
     */
    public static boolean isServiceWork(Context mContext, String serviceName) {
        boolean isWork = false;
        ActivityManager myAM = (ActivityManager) mContext
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> myList = myAM.getRunningServices(Integer.MAX_VALUE);
        if (myList.size() <= 0) {
            return false;
        }
        for (int i = 0; i < myList.size(); i++) {
            String mName = myList.get(i).service.getClassName().toString();
            if (mName.equals(serviceName)) {
                isWork = true;
                break;
            }
        }
        return isWork;
    }
    /**
     * 取消注册网络模式广播
     */
    private void unRegisterNetWorkReceiver() {
        if (networkStateReceiver != null) {
            unregisterReceiver(networkStateReceiver);
        }
    }

    /***
     * 取消注册监听桌面悬浮按钮的广播
     */
    private void unRegisterDeskLayoutReceiver(){
        if(deskLayoutStateReceiver!=null){
            unregisterReceiver(deskLayoutStateReceiver);
        }
    }
    /**
     * 取消wifi状态广播监听
     *
     */
    private void unRegisterWifiWorkReceiver() {
        if (wifiStateReceiver != null) {
            unregisterReceiver(wifiStateReceiver);
        }
    }

    /**
     * 注册监听wifi信号的广播接收者
     *
     *
     */
    private void registerWifiStateReceiver() {
        wifiStateReceiver = new WifiStateReceiver();
        IntentFilter wifiIntentFilter = new IntentFilter();
        wifiIntentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        wifiIntentFilter = new IntentFilter();
        wifiIntentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        wifiIntentFilter.addAction(WifiManager.RSSI_CHANGED_ACTION);
        wifiIntentFilter.setPriority(Integer.MAX_VALUE);
        registerReceiver(wifiStateReceiver, wifiIntentFilter);
    }


    /**
     * 启动查派车载系统的核心后台服务
     */
    public void startCoreService() {
        Intent intent = new Intent(this, CCLuncherCoreService.class);
        this.startService(intent);

    }

    /**
     * wifi热点服务
     */
    private void startWifiAPService() {
        Intent intent = new Intent(this, ChappieWifiHotService.class);
       /* intent.setAction("com.scblhkj.carluncher.service.ChappieWifiHotService");
        intent.setPackage("com.scblhkj.carluncher");*/
        startService(intent);
        conn=new MyServiceConnection();
        bindService(intent,conn, BIND_AUTO_CREATE);
        Log.e("info","绑定wifi热点服务成功!");
    }

    /***
     * 通过myBinder对象调用热点服务里的方法
     */
    public class MyServiceConnection implements ServiceConnection{

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            //得到MyService.MyBinder对象，我们通过这个对象来操作服务中的方法
            myBinder = (ChappieWifiHotService.MyBinder) service;
            boolean is= myBinder.isWifiap();
            Log.e("info","is>>>>>>"+is);
            if(myBinder.isWifiap()==true){
                ic_state_wifiInfo.setImageResource(R.drawable.wifion);
                editor.putString("wifihotstate", "1");
                editor.commit();
            }else if(myBinder.isWifiap()==false){
                ic_state_wifiInfo.setImageResource(R.drawable.nowifi);
                editor.putString("wifihotstate", "0");
                editor.commit();
            };
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    }
    /**
     * 初始化控件
     */
    @Override
    protected void initView() {
        iv_state_airmode = ImageView.class.cast(findViewById(R.id.iv_state_airmode));
        ic_state_wifi = ImageView.class.cast(findViewById(R.id.ic_state_wifi));
        ic_state_signal = ImageView.class.cast(findViewById(R.id.ic_state_signal));
        tv_state_sinal_name = TextView.class.cast(findViewById(R.id.tv_state_sinal_name));
        tv_temperature = TextView.class.cast(findViewById(R.id.tv_temperature));
        iv_state_bluetooth = ImageView.class.cast(findViewById(R.id.im_state_bluetooth));
        myGridView=(MyGridView)findViewById(R.id.main_gridview);
        myGridView.setAdapter(new LauncherAdapter(getApplicationContext()));
        viewPager=(ViewPager)findViewById(R.id.main_viewpager);
        timeWeatherFragment=new TimeWeatherFragment();
        viewList=new ArrayList<Fragment>();
        // viewList.add(recorderFragment);
        viewList.add(timeWeatherFragment);
        screenWidth=getResources().getDisplayMetrics().widthPixels;
        viewPager.setAdapter(new MyFrageStatePagerAdapter(getSupportFragmentManager()));
        im_state_bluetoothInfo=(ImageView)findViewById(R.id.im_state_bluetoothInfo);
        ic_state_wifiInfo=(ImageView)findViewById(R.id.ic_state_wifiInfo);
        iv_state_lock_if=(ImageView)findViewById(R.id.iv_state_lock_if);
        iv_state_recorder=(ImageView)findViewById(R.id.iv_state_recorder);
        boolean autoBrightness = BrightnessTools.isAutoBrightness(getContentResolver());
        if(autoBrightness){
            BrightnessTools.stopAutoBrightness(this);
            Log.e("info", "关闭了自动调节亮度");
            // BrightnessTools.setBrightness(this, 255);
            //  Log.e("info", "亮度已经设置为了最大");
        }else{
            Log.e("info","没有开启自动调节亮度");
        }

        //启动自定义图标服务
        // startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
        sHA1(this);
        /****
         *  ll_content = (ChappieLinearLayout) this.findViewById(R.id.ll_content);
         item_tpms_watch = ll_content.addItem2End(R.layout.content_item_tpms_watch);
         item_monitor = ll_content.addItem2End(R.layout.content_item_monitor);
         item_music = ll_content.addItem2End(R.layout.content_item_music);
         item_contacts = ll_content.addItem2End(R.layout.content_item_contacts);
         item_weather = ll_content.addItem2End(R.layout.content_item_weather);
         item_navi = ll_content.addItem2End(R.layout.content_item_navi);
         item_setting = ll_content.addItem2End(R.layout.content_item_setting);
         item_transport_condition = ll_content.addItem2End(R.layout.content_item_transport_condition);
         item_content_car_state = ll_content.addItem2End(R.layout.content_item_car_state);
         item_content_journey = ll_content.addItem2End(R.layout.content_item_chappie);
         item_file_manager = ll_content.addItem2End(R.layout.content_item_file_manager);
         item_me = ll_content.addItem2End(R.layout.content_item_me);
         item_fm = ll_content.addItem2End(R.layout.content_item_fm);
         item_btp = ll_content.addItem2End(R.layout.content_item_btphone);
         item_app_store = ll_content.addItem2End(R.layout.content_item_app_store);
         item_car_control = ll_content.addItem2End(R.layout.content_item_car_control);
         item_message = ll_content.addItem2End(R.layout.content_item_message);
         */


    }

    /***
     * 获取sha1的方法
     * @param context
     * @return
     */
    public static String sHA1(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), PackageManager.GET_SIGNATURES);
            byte[] cert = info.signatures[0].toByteArray();
            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] publicKey = md.digest(cert);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < publicKey.length; i++) {
                String appendString = Integer.toHexString(0xFF & publicKey[i])
                        .toUpperCase(Locale.US);
                if (appendString.length() == 1)
                    hexString.append("0");
                hexString.append(appendString);
                hexString.append(":");
            }
            Log.e("info", "SHA1>>>>" +hexString.toString());
            return hexString.toString();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 定义自己的ViewPager适配器。
     * 也可以使用FragmentPagerAdapter。关于这两者之间的区别，可以自己去搜一下。
     */
    class MyFrageStatePagerAdapter extends FragmentStatePagerAdapter
    {

        public MyFrageStatePagerAdapter(FragmentManager fm)
        {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return viewList.get(position);
        }

        @Override
        public int getCount() {
            return viewList.size();
        }

        /**
         * 每次更新完成ViewPager的内容后，调用该接口，此处复写主要是为了让导航按钮上层的覆盖层能够动态的移动
         */
        @Override
        public void finishUpdate(ViewGroup container)
        {
            super.finishUpdate(container);//这句话要放在最前面，否则会报错
            //获取当前的视图是位于ViewGroup的第几个位置，用来更新对应的覆盖层所在的位置
            int currentItem=viewPager.getCurrentItem();
            if (currentItem==currenttab)
            {
                return ;
            }
            imageMove(viewPager.getCurrentItem());
            currenttab=viewPager.getCurrentItem();
        }

    }
    /**
     * 移动覆盖层
     * @param moveToTab 目标Tab，也就是要移动到的导航选项按钮的位置
     * 第一个导航按钮对应0，第二个对应1，以此类推
     */
    private void imageMove(int moveToTab)
    {
        int startPosition=0;
        int movetoPosition=0;

        startPosition=currenttab*(screenWidth/4);
        movetoPosition=moveToTab*(screenWidth/4);
        //平移动画
        TranslateAnimation translateAnimation=new TranslateAnimation(startPosition,movetoPosition, 0, 0);
        translateAnimation.setFillAfter(true);
        translateAnimation.setDuration(200);
        //imageviewOvertab.startAnimation(translateAnimation);
    }


    @Override
    protected void initData() {
        if (!isServiceWork(this,"com.example.apklock.AppService")) {
            startService(new Intent(this, DamenService.class));
            // ToastUtil.showToast("你启动了DaMenService");
        }

        startWifiAPService();
        fm = getSupportFragmentManager();
        gestureDetector = new GestureDetector(this, this);
        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        connManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        //判断wIfi是否连接，根据连接状态来显示wifi状态
        //初始化蓝牙适配器，根据蓝牙状态来设置图标
        bluetoothAdapter=BluetoothAdapter.getDefaultAdapter();
        boolean BluetoothState=isBlueTooth(this.getApplicationContext());
        if(BluetoothState){
            im_state_bluetoothInfo.setImageResource(R.drawable.bluetoothon);
        }else {
            im_state_bluetoothInfo.setImageResource(R.drawable.nobluetooth);
        }
        // 3G信号
        myPhoneStateListener = new MyPhoneStateListener();
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (checkSDCardState()) {
            if (ChappieCarApplication.isDebug)
                Log.e(TAG, "SD卡安装好了");
        } else {
            if (ChappieCarApplication.isDebug)
                Log.e(TAG, "SD卡没有安装好");
        }
        if(isRec){
            iv_state_recorder.setImageResource(R.drawable.recorderon);
        }else if(!isRec){
            iv_state_recorder.setImageResource(R.drawable.recorderon);
        }
        //蓝牙图标
        im_state_bluetoothInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!bluetoothAdapter.isEnabled()){//如果蓝牙没有开启
                    Intent discoverableIntent = new Intent(
                            BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                    discoverableIntent.putExtra(
                            BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
                    startActivity(discoverableIntent);
                    im_state_bluetoothInfo.setImageResource(R.drawable.bluetoothon);
                }else{
                    bluetoothAdapter.disable();
                    im_state_bluetoothInfo.setImageResource(R.drawable.nobluetooth);
                }

            }
        });

        //WIFI图标
        ic_state_wifiInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myBinder.isWifiap()==true){//如果wifi热点开启的话
                    ic_state_wifiInfo.setImageResource(R.drawable.nowifi);
                    myBinder.closeWifiAP();
                    ToastUtil.showToast("关闭了WIFI热点");
                    editor.putString("wifihotstate", "0");
                    editor.commit();


                }else if(myBinder.isWifiap()==false){
                    ic_state_wifiInfo.setImageResource(R.drawable.wifion);
                    myBinder.openWifiAP();
                    ToastUtil.showToast("开启了WIFI热点");
                    editor.putString("wifihotstate","1");
                    editor.commit();

                }


            }
        });
        //暂停键
        iv_state_recorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.scblhkj.carluncher.hddvr");
                /***if(isRec){
                 Intent intent=new Intent();
                 intent.setAction("com.chappie.VIDEOPAUSE");
                 Bundle bundle=new Bundle();
                 bundle.putInt("isRec", 1);
                 intent.putExtras(bundle);
                 sendBroadcast(intent);
                 iv_state_recorder.setImageResource(R.drawable.ic_notrecorder);
                 }else if(!isRec){
                 Intent intent=new Intent();
                 intent.setAction("com.chappie.VIDEOPLAY");
                 Bundle bundle=new Bundle();
                 bundle.putInt("isRec",0);
                 intent.putExtras(bundle);
                 sendBroadcast(intent);
                 iv_state_recorder.setImageResource(R.drawable.recorderon);
                 }
                 *
                 */


            }
        });

        // 初始化串口通信的接收者
        serialCommReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (SerialDataCommSerivce.SERIAL_TPMS_ACTION.equals(action)) {
                }
            }
        };

        //菜单短按事件
        myGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {//行车记录仪
                    startAppHandler.sendEmptyMessage(START_HDDRV);

                } else if (position == 1) {//车辆状态
                    StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.scblhkj.carluncher.carstate");
                } else if (position == 2) {//胎压
                    ToastUtil.showToast("请绑定胎压设备!");
                    //StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.scblhkj.carluncher.tpms");
                } else if (position == 3) {//导航
                    layoutState = 2;
                    editor.putString("layoutState", "2");
                    editor.commit();
                    //当状态码是2的时候，移除侧边导航栏，添加顶部导航栏
                    StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.autonavi.minimap");
                    // manager.createView();

                } else if (position == 4) {
                    //相册
                    //ToastUtil.showToast("你点击了辅助驾驶");
                    // StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.test.JniTest");
                    if(SDCardScannerUtil.isSecondSDcardMounted()){
                        StartAppUtil.doStartApplicationWithPackageName(MainActivity.this,"com.example.localalbum");
                    }else if(!SDCardScannerUtil.isSecondSDcardMounted()) {
                        ToastUtil.showToast("SD卡异常,不能使用该功能");
                    }

                } else if (position == 5) {
                    //音乐
                    StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "cn.kuwo.kwmusiccar");
                }else if(position==6){
                    //蓝牙电话
                    StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.concox.bluetooth");
                }else if(position==7){
                    //fm发射
                    StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.rdafm.fmtx");
                }else if(position==8){
                    //设置
                    //   startActivity(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS));
                    Intent intent=new Intent(MainActivity.this,SettingActivity.class);
                    editor.putString("activityInfo","1");
                    editor.commit();
                    startActivity(intent);
                }else if(position==9){ //二维码
                    if(SDCardScannerUtil.isSecondSDcardMounted()){
                        Intent intent=new Intent(MainActivity.this,ImeiQrcodeActivity.class);
                        startActivity(intent);
                    }else {
                        ToastUtil.showToast("未找到SD卡,不能使用该功能");
                        return;
                    }


                }else if(position==10){ //语音对讲
                    if(sp.getBoolean("isTalkEachOther",isTalkEachOther)&&sp.getInt("roomNum",roomNum)!=0){
                        if(isNetWorkAvailable()){
                            Intent intent=new Intent(MainActivity.this,AudioRecordActivity.class);
                            intent.putExtra("roomNum",sp.getInt("roomNum",roomNum));
                            Log.e("info", "sp.getInt(roomNum)>>>>"+sp.getInt("roomNum",roomNum));
                            startActivity(intent);
                        }else {
                            ToastUtil.showToast("请检查网络连接是否正常");
                        }

                    }else {
                        ToastUtil.showToast("你还没开启语音对讲功能");
                        return;
                    }
                }else if(position==11){ //系统设置
                    if(!sp.getBoolean("isSetSystemSetting",isSetSystemSetting)){
                         ToastUtil.showToast("还未开启系统设置功能!");
                    }else {
                        Intent intent =  new Intent(Settings.ACTION_SETTINGS);
                        startActivity(intent);
                    }
                }
            }
        });


/****
 *  item_content_car_state.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View view) {
StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.scblhkj.carluncher.carstate");
}
});
 item_tpms_watch.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View view) {
StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.scblhkj.carluncher.tpms");
}
});
 item_navi.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View view) {
StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.autonavi.minimap");
}
});
 */

        /**
         * 行程模块暂时点击进入生成二维码界面
         *  item_content_journey.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        intent2Activity(ActivateChappieActivity.class);
        }
        });
         */

        /****
         * // 主界面上监控模块的点击事件
         item_monitor.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.scblhkj.carluncher.hddvr");
        }
        });

         item_file_manager.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        //    StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.scblhkj.carluncher.filemanager");
        StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.mediatek.filemanager");   // MTK自带文件管理器
        }
        });
         item_music.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "cn.kuwo.kwmusiccar");
        }
        });

         item_contacts.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);//vnd.android.cursor.dir/contact
        startActivity(intent);
        }
        });

         item_weather.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.scblhkj.carluncher.weather");
        }
        });

         item_transport_condition.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        intent2Activity(TransportConditionActivity.class);
        }
        });

         item_setting.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        startActivity(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS));
        }
        });

         item_me.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        intent2Activity(MyChappieActivity.class);
        }
        });

         item_fm.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.rdafm.fmtx");
        }
        });

         item_btp.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        StartAppUtil.doStartApplicationWithPackageName(MainActivity.this, "com.concox.bluetooth");
        }
        });

         item_app_store.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        // 跳转到设备管理界面
        Intent intent = new Intent("/");
        ComponentName cm = new ComponentName("com.android.settings","com.android.settings.DeviceAdminSettings");
        intent.setComponent(cm);
        intent.setAction("android.intent.action.VIEW");
        startActivityForResult(intent, 0);
        }
        });
         // 消息 暂时弄成设置音量界面
         item_message.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        startActivity(new Intent(Settings.ACTION_SOUND_SETTINGS));//即可跳到音量设置界面
        }
        });

         // 车辆控制 暂时弄成显示设置
         item_car_control.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        startActivity(new Intent(Settings.ACTION_DISPLAY_SETTINGS));//屏幕亮度设置
        }
        });
         */


    }

    /***
     * 判断是否有网络
     * @return
     */
    public boolean  isNetWorkAvailable(){
        // 得到网络连接信息
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        // 去进行判断网络是否连接
        if (manager.getActiveNetworkInfo() != null) {
            return true;
        }
        return false;
    }
    /***
     * 注销启动app广播接收器
     */
    private void  unRegisterSettingStartNewAppReceiver(){
        if(settingStartAppReceiver!=null){
            unregisterReceiver(settingStartAppReceiver);
        }
    }
    /***
     * 注册启动app广播接收器
     */
    private void registerSettingStartNewAppReceiver(){
        IntentFilter intentFilter=new IntentFilter();
        intentFilter.addAction(ChappieConstant.SEND_START_APP_PACKAGENAME);
        registerReceiver(settingStartAppReceiver, intentFilter);
    }

    private void RegisterNotRecorderReceiver(){
        videoNotRecorderReceiver=new VideoNotRecorderReceiver();
        IntentFilter filter=new IntentFilter();
        filter.addAction("com.chappie.NOTRECORDER");
        registerReceiver(videoNotRecorderReceiver,filter);

    }
    private void RegisterIsRecorderReceiver(){
        videoIsRecorderReceiver=new VideoIsRecorderReceiver();
        IntentFilter filter=new IntentFilter();
        filter.addAction("com.chappie.ISRECORDER");
        registerReceiver(videoIsRecorderReceiver,filter);

    }
    private void  unRegisterNotRecorderReceiver(){
        if(videoNotRecorderReceiver!=null){
            unregisterReceiver(videoNotRecorderReceiver);
        }
    }
    private void  unRegisterIsRecorderReceiver(){
        if(videoIsRecorderReceiver!=null){
            unregisterReceiver(videoIsRecorderReceiver);
        }
    }
    /***
     * 配置启动APP的广播接收器
     */
   private void initSettingStartAppReceiver(){
       settingStartAppReceiver=new BroadcastReceiver(){

           @Override
           public void onReceive(Context context, Intent intent) {
               String action=intent.getAction();
               if(action.equals(ChappieConstant.SEND_START_APP_PACKAGENAME)){
                   Log.e("info","MainActivity接收到了启动APP设置广播");
                   packageTime= intent.getStringExtra("packageTime");
                   packageImei=intent.getStringExtra("packageImei");
                   packageName=intent.getStringExtra("packageName");
                   isSettingStartApp=intent.getBooleanExtra("isStartNewApp", isSettingStartApp);
                   Log.e("info","activity接收到的packageName:"+packageName+" isSettingStartApp:"+isSettingStartApp);
                   isSettingStartApp=true;
                   editor.putString("packageNameSucc", packageName);
                   editor.putBoolean("isSettingStartApp", isSettingStartApp);
                   editor.commit();
                   sendSettingStartAppSucessBroadCast(packageTime,packageImei,packageName);
               }
           }
       };
   }



    /***
     * 设置启动APP成功的广播
     */
    private void sendSettingStartAppSucessBroadCast(String packageTimeSucc,String packageImeiSucc,String packageNameSucc) {
        Intent intent=new Intent();
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setAction(ChappieConstant.SEND_START_APP_PACKAGENAME_SUCCESSS);
        intent.putExtra("packageTimeSucc", packageTimeSucc);
        intent.putExtra("packageImeiSucc",packageImeiSucc);
        intent.putExtra("packageNameSucc",packageNameSucc);
        sendBroadcast(intent);
    }

    public class  VideoNotRecorderReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            String action =intent.getAction();
            if(action.equals("com.chappie.NOTRECORDER")){
                Bundle bundle=intent.getExtras();
                if(bundle!=null){
                    notRecorder=bundle.getBoolean("notRecording");
                    if(notRecorder==false){
                        isRec=false;//没有录像
                    }
                }
            }
        }
    }

    public class VideoIsRecorderReceiver extends  BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            String action=intent.getAction();
            if(action.equals("com.chappie.ISRECORDER")){
                Bundle bundle=intent.getExtras();
                if(bundle!=null){
                    isRecorder=bundle.getBoolean("isRecording");
                    if(isRecorder){
                        isRec=true;//正在录像
                    }
                }
            }

        }
    }

    /***
     * 显示导航时查派悬浮按钮以及按钮监听状态
     */
    private void createTopDeskLayout() {
        topDeskLayout=new TopDeskLayout(this);
        iv_chappie=(ImageView)topDeskLayout.findViewById(R.id.iv_chappie_ia);
        ll_menu=(LinearLayout)topDeskLayout.findViewById(R.id.ll_menu);
        top_home_selector=(LinearLayout)topDeskLayout.findViewById(R.id.top_home_selector);
        top_back_selector=(LinearLayout)topDeskLayout.findViewById(R.id.top_back_selector);
        topDeskLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isShow==false){
                    ll_menu.setVisibility(View.VISIBLE);
                    isShow=true;
                    Log.e("info","isSHow>>>"+isShow);
                }else if(isShow==true){
                    ll_menu.setVisibility(View.GONE);
                    isShow=false;
                    Log.e("info","isSHow>>>"+isShow);
                }

            }
        });

        top_home_selector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//home键
                Intent intent=new Intent(Intent.ACTION_MAIN);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addCategory(Intent.CATEGORY_HOME);
                startActivity(intent);
            }
        });

        top_back_selector.setOnClickListener(new View.OnClickListener() {//back键
            @Override
            public void onClick(View v) {
                Runtime runtime = Runtime.getRuntime();
                try {
                    runtime.exec("input keyevent " + KeyEvent.KEYCODE_BACK);
                } catch (IOException e) { // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
        topDeskLayout.setOnTouchListener(new View.OnTouchListener() {
            float mTouchStartX;
            float mTouchStartY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // 获取相对屏幕的坐标，即以屏幕左上角为原点
                x = event.getRawX();
                y = event.getRawY() - top; // 25是系统状态栏的高度
                Log.i("startP", "startX" + mTouchStartX + "====startY"
                        + mTouchStartY);
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // 获取相对View的坐标，即以此View左上角为原点
                        mTouchStartX = event.getX();
                        mTouchStartY = event.getY();
                        Log.i("startP", "startX" + mTouchStartX + "====startY"
                                + mTouchStartY);
                        long end = System.currentTimeMillis() - startTime;
                        // 双击的间隔在 300ms以下
                        // if (end < 300) {
                        //   closeDesk();
                        //}
                        // startTime = System.currentTimeMillis();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        // 更新浮动窗口位置参数
                        mLayout.x = (int) (x - mTouchStartX);
                        mLayout.y = (int) (y - mTouchStartY);
                        mWindowManager.updateViewLayout(v, mLayout);
                        break;
                    case MotionEvent.ACTION_UP:
                        // 更新浮动窗口位置参数
                        mLayout.x = (int) (x - mTouchStartX);
                        mLayout.y = (int) (y - mTouchStartY);
                        mWindowManager.updateViewLayout(v, mLayout);
                        // 可以在此记录最后一次的位置
                        mTouchStartX = mTouchStartY = 0;
                        break;
                }
                return false;
            }
        });
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Rect rect = new Rect();
        // /取得整个视图部分,注意，如果你要设置标题样式，这个必须出现在标题样式之后，否则会出错
        getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        top = rect.top;//状态栏的高度，所以rect.height,rect.width分别是系统的高度的宽度

        Log.i("top",""+top);
    }
    /***
     * 创建一个在顶部的悬浮按钮
     */
    public void createTopWindowManager() {
        // 取得系统窗体
        mWindowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        // 窗体的布局样式
        mLayout = new WindowManager.LayoutParams();
        // 设置窗体显示类型——TYPE_SYSTEM_ALERT(系统提示)
        mLayout.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        // 设置窗体焦点及触摸：
        // FLAG_NOT_FOCUSABLE(不能获得按键输入焦点)
        mLayout.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        // 设置显示的模式
        mLayout.format = PixelFormat.RGBA_8888;
        // 设置对齐的方法
        mLayout.gravity = Gravity.TOP | Gravity.LEFT;
        // 设置窗体宽度和高度
        mLayout.width = WindowManager.LayoutParams.WRAP_CONTENT;
        mLayout.height = WindowManager.LayoutParams.WRAP_CONTENT;
    }

    /****
     * 判断蓝牙是否打开
     * @param context
     * @return
     */
    public boolean isBlueTooth(Context context){
        if(!bluetoothAdapter.isEnabled()){
            return  false;
        }else {
            return true;
        }
    }

    /**
     * 判断wifi连接状态
     *
     * @param ctx
     * @return
     */
    public boolean isWifiAvailable(Context ctx) {
        ConnectivityManager conMan = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo.State wifi = conMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .getState();
        if (NetworkInfo.State.CONNECTED == wifi) {
            return true;
        } else {
            return false;
        }
    }

    // 放大模块
    private void maxView(View v) {
        /**
         * 将内容显示区域显示出来
         */
        // v.findViewById(R.id.fl_content).setVisibility(View.VISIBLE);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ChappieCarApplication.SCREEN_WIDTH / 2, (int) (ChappieCarApplication.SCREEN_HEIGHT * 0.95));
        params.gravity = Gravity.CENTER_VERTICAL;
        // 此处的margin top 和 bottom无效
        params.setMargins(10, 10, 0, 10);
        /* // 缩放动画
        Animation scaleAnimation = new ScaleAnimation(0.5f, 1.0f, 0.5f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setDuration(300);
        // 透明度动画
        AlphaAnimation alpha = new AlphaAnimation(0.1f, 1.0f);
        alpha.setDuration(200);
        //设置执行的时间
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alpha);
        v.setAnimation(animationSet);*/
        YoYo.with(Techniques.RubberBand).duration(1000).playOn(v);
        v.setLayoutParams(params);
    }

    @Override
    public void onClick(View v) {

    }


    /**
     * 屏蔽返回键
     *     @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
    ChappieCarApplication.resetIsMaxView();
    return true;
    }
    return super.onKeyDown(keyCode, event);
    }
     *
     */


    private final static int START_RECORDING = 100;


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        // 让手势识别类处理WindowManager中布局的onTouch事件
        return gestureDetector.onTouchEvent(motionEvent);
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float x, float y) {
        return true;
    }

    /**
     * 设置飞行模式图标
     */
    private void setAirplaneIcon(boolean isAirplaneOn) {
        if (isAirplaneOn) {
            Log.i(TAG, "飞行模式开");
            iv_state_airmode.setBackground(getResources().getDrawable(R.mipmap.ic_state_airmode_on));
        } else {
            Log.i(TAG, "飞行模式关");
            iv_state_airmode.setBackground(getResources().getDrawable(R.mipmap.ic_state_airmode_on));
        }
    }
    private class WifihotRecevier extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            String wifihotstate=intent.getExtras().getString("wifihotstate");
            if(wifihotstate.equals("1")){
                ic_state_wifiInfo.setImageResource(R.drawable.wifion);
            }else if(wifihotstate.equals("0")){
                ic_state_wifiInfo.setImageResource(R.drawable.nowifi);
            }
        }
    }
    /**
     * 关闭Android导航栏，实现全屏
     */
    private void closeBar() {
        try {
            String command;
            command = "LD_LIBRARY_PATH=/vendor/lib:/system/lib service call activity 42 s16 com.android.systemui";
            ArrayList<String> envlist = new ArrayList<String>();
            Map<String, String> env = System.getenv();
            for (String envName : env.keySet()) {
                envlist.add(envName + "=" + env.get(envName));
            }
            String[] envp = envlist.toArray(new String[0]);
            Process proc = Runtime.getRuntime().exec(
                    new String[] { "su", "-c", command }, envp);
            proc.waitFor();
        } catch (Exception ex) {
            // Toast.makeText(getApplicationContext(), ex.getMessage(),
            // Toast.LENGTH_LONG).show();
        }
    }
    /***
     * 桌面悬浮导航栏的显示状态判断
     */
    private class DeskLayoutStateReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            String action=intent.getAction();
            if(action.equals("com.miya.action.damenService")){
                Bundle bundle = intent.getExtras();
                if(bundle!=null){
                    String state=bundle.getString("layoutState");
                    String windowState=bundle.getString("windowState");
                    aName=bundle.getString("aName");//activity名
                    pName=bundle.getString("pName");//包名
                  // Log.e("info","广播传过来的state>>>>>"+state+"  windowState>>>"+windowState+".....pName"+pName+"-------aName"+aName);
                    if(state.equals("1")&&windowState.equals("1")){
                        if(topDeskLayout!=null&&mDesktopLayout==null){
                            Log.e("info", "完成顶部导航栏关闭，侧部导航栏开启!!!!!!!!");
                            mWindowManager.removeView(topDeskLayout);
                            topDeskLayout=null;
                            createWindowManager();
                            createDesktopLayout();
                            mWindowManager.addView(mDesktopLayout, mLayout);
                            // manager.removeView();

                        }else if(topDeskLayout!=null&&mDesktopLayout!=null){
                            //  Log.e("info","topDeskLayout!=null&&mDesktopLayout!=null");
                        }else if(topDeskLayout==null&&mDesktopLayout==null){
                            //  Log.e("info","topDeskLayout==null&&mDesktopLayout==null");
                        }else if(topDeskLayout==null&&mDesktopLayout!=null){
                            // Log.e("info","topDeskLayout==null&&mDesktopLayout!=null");
                        }
                    }else if(state.equals("2") && windowState.equals("2")) {
                        if(topDeskLayout==null&&mDesktopLayout!=null){
                            mWindowManager.removeView(mDesktopLayout);
                            Log.e("info", "完成顶部导航栏开启，侧部导航栏关闭!!!!!!!!");
                            mDesktopLayout=null;
                            SystemClock.sleep(50);
                            if(topDeskLayout==null){
                                createTopWindowManager();
                                createTopDeskLayout();
                                mWindowManager.addView(topDeskLayout, mLayout);
                                closeBar();
                            }

                        }else if(topDeskLayout!=null&&mDesktopLayout!=null){
                            // Log.e("info","topDeskLayout!=null&&mDesktopLayout!=null");
                        }else if(topDeskLayout==null&&mDesktopLayout==null){
                            // Log.e("info","topDeskLayout==null&&mDesktopLayout==null");
                        }else if(topDeskLayout==null&&mDesktopLayout!=null){
                            //    Log.e("info","topDeskLayout==null&&mDesktopLayout!=null");
                        }else if(topDeskLayout!=null&&mDesktopLayout==null){
                            //   Log.e("info","mDeskttopDeskLayout==null&&topLayout!=null");
                        }

                    }

                }
            }
        }
    }
    /**
     * 监听飞行模式
     */
    private class NetworkStateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED)) {
                boolean isAirplaneOn = intent.getBooleanExtra("state", false);
                Log.i(TAG, "飞行模式广播接受者状态" + isAirplaneOn);
                setAirplaneIcon(isAirplaneOn);
            }
        }
    }

    /**
     * 手机电池电量的广播接收者
     */
    private class PowerConnectionReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {

                int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
                boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL;
                //当前剩余电量
                int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                //电量最大值
                int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
                //电量百分比
                float batteryPct = level / (float) scale;

                if (isCharging) { // 正在充电中
                    Log.i(TAG, "切换正在充电中的图标");
                } else {  //没在充电中需要判断电池的电量,显示图片
                    Log.i(TAG, "没有在充电中，剩余电量是" + batteryPct);
                    if (batteryPct < 0.3 && batteryPct > 0) {
                        //电池电量小于30%
                        Log.i(TAG, "电池电量小于30%");
                    } else if (batteryPct >= 0.3 && batteryPct < 0.5) {
                        //电池电量小于50%大于30%
                        Log.i(TAG, "电池电量小于50%,大于30%");
                    } else if (batteryPct >= 0.5 && batteryPct < 0.8) {
                        //电池电量小于80%大于50%
                        Log.i(TAG, "电池电量小于80%,大于50%");
                    } else if (batteryPct >= 0.8 && batteryPct < 1.0) {
                        //电池电量大于80%小于100%
                        Log.i(TAG, "电池电量小于100%,大于80%");
                    }
                }
            }
        }
    }


    /******
     * wifi
     ******/
    private WifiManager wifiManager;
    private ConnectivityManager connManager;
    private NetworkInfo mWifi;

    /**
     * 更新WiF状态
     */
    private void updateWiFiState() {
        if (wifiManager.isWifiEnabled() && mWifi.isConnected()) {
            int level = ((WifiManager) getSystemService(WIFI_SERVICE)).getConnectionInfo().getRssi();// Math.abs()
            ic_state_wifiInfo.setImageResource(SignalUtil.getWifiImageBySignal(level));
            // ic_state_wifi.setImageResource(SignalUtil.getWifiImageBySignal(level));
        } else {
            // ic_state_wifi.setImageResource(R.mipmap.ic_state_wifi_3);
            ic_state_wifiInfo.setImageResource(R.drawable.nowifi);
        }
    }

    /**
     * WiFi状态Receiver

     */
    private class WifiStateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int wifi_state = intent.getIntExtra("wifi_state", 0);
            int level = ((WifiManager) getSystemService(WIFI_SERVICE)).getConnectionInfo().getRssi();// Math.abs()
            updateWiFiState();
            Log.i(TAG, "wifiIntentReceiver, Wifi Level:" + level);
            switch (wifi_state) {
                case WifiManager.WIFI_STATE_ENABLED:
                    updateWiFiState();
                    new Thread(new updateWifiThread()).start();
                    break;
                case WifiManager.WIFI_STATE_ENABLING:
                    updateWiFiState();
                    new Thread(new updateWifiThread()).start();
                    break;
                case WifiManager.WIFI_STATE_DISABLING:
                case WifiManager.WIFI_STATE_DISABLED:
                case WifiManager.WIFI_STATE_UNKNOWN:
                    updateWiFiState();
                    break;
            }
        }
    }


    /**
     * 更新wifi图标
     *
     */
    public class updateWifiThread implements Runnable {
        @Override
        public void run() {
            synchronized (updateWifiHandler) {
                int updateWifiTime = 1;
                boolean shouldUpdateWifi = true;
                while (shouldUpdateWifi) {
                    try {
                        Thread.sleep(2500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (ChappieCarApplication.isDebug)
                        Log.e(TAG, "updateWifiThread:Refresh Wifi! " + updateWifiTime);
                    Message messageWifi = new Message();
                    messageWifi.what = 1;
                    updateWifiHandler.sendMessage(messageWifi);
                    updateWifiTime++;
                    if (updateWifiTime > 5) {
                        shouldUpdateWifi = false;
                    }
                }
            }
        }
    }

    final Handler updateWifiHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    updateWiFiState();
                    break;
                default:
                    break;
            }
        }
    };


    /**
     * 手机状态的监听类
     */
    private class MyPhoneStateListener extends PhoneStateListener {

        /**
         * 更新3G信号强度
         */
        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            update3GSignalStrength(signalStrength.getGsmSignalStrength());
            setTMobileName();
            super.onSignalStrengthsChanged(signalStrength);
        }

        @Override
        public void onDataConnectionStateChanged(int state) {

            switch (state) {
                case TelephonyManager.DATA_DISCONNECTED:// 网络断开
                    Log.i(TAG, "3G TelephonyManager.DATA_DISCONNECTED");
                    break;

                case TelephonyManager.DATA_CONNECTING:// 网络正在连接
                    Log.i(TAG, "3G TelephonyManager.DATA_CONNECTING");
                    break;

                case TelephonyManager.DATA_CONNECTED:// 网络连接上
                    Log.i(TAG, "3G TelephonyManager.DATA_CONNECTED");
                    break;
            }
        }
    }

    /**  获取手机卡信号网络的类型 2G 3G 4G UI上没有，暂时不用做
     private void update3GType() {
     int networkType = telephonyManager.getNetworkType();
     Log.i(TAG, "[update3Gtype]NetworkType:" + networkType);
     image3GType.setBackground(getResources().getDrawable(SignalUtil.get3GTypeImageByNetworkType(networkType)));
     }
     */


    /**
     * 更新信号强弱的状态
     * <p/>
     * SIM_STATE_UNKNOWN = 0:Unknown.
     * <p/>
     * SIM_STATE_ABSENT = 1:no SIM card is available in the device
     * <p/>
     * SIM_STATE_PIN_REQUIRED = 2:requires the user's SIM PIN to unlock
     * <p/>
     * SIM_STATE_PUK_REQUIRED = 3:requires the user's SIM PUK to unlock
     * <p/>
     * SIM_STATE_NETWORK_LOCKED = 4:requires a network PIN to unlock
     * <p/>
     * SIM_STATE_READY = 5:Ready
     */
    private void update3GSignalStrength(int signal) {
        simState = telephonyManager.getSimState();
        Log.i(TAG, "[update3GState]SIM State:" + simState);
        if (NetworkUtil.isAirplaneModeOn(getApplicationContext())) {
            Log.i(TAG, "没有信号");
            iv_state_lock_if.setImageResource(R.drawable.nosingle);
            //ic_state_signal.setBackground(getResources().getDrawable(R.drawable.ic_qs_signal_no_signal));
        } else if (simState == TelephonyManager.SIM_STATE_READY) {
            Log.i(TAG, "有信号，但需要进一步判断信号的强弱等级");
            iv_state_lock_if.setBackground(getResources().getDrawable(SignalUtil.get3GLevelImageByGmsSignalStrength(signal)));
        } else if (simState == TelephonyManager.SIM_STATE_UNKNOWN || simState == TelephonyManager.SIM_STATE_ABSENT) {
            Log.i(TAG, "sim卡信号状态未知或是未检测到sim卡");
            //ic_state_signal.setBackground(getResources().getDrawable(R.drawable.ic_qs_signal_no_signal));
            iv_state_lock_if.setImageResource(R.drawable.nosingle);
        }
    }

    /**
     * 设置运营商名称
     */
    private void setTMobileName() {
        String operator = telephonyManager.getSimOperator();
        if (!TextUtils.isEmpty(operator)) {
            if (operator.equals("46000") || operator.equals("46002") || operator.equals("46007")) {
                // 中国移动
                tv_state_sinal_name.setText("中国移动");
            } else if (operator.equals("46001")) {
                // 中国联通
                tv_state_sinal_name.setText("中国联通");
            } else if (operator.equals("46003")) {
                // 中国电信
                tv_state_sinal_name.setText("中国电信");
            }
        }
    }


    /**
     * /**
     * 蓝牙开启状态的广播接收者
     */
    private BroadcastReceiver blueStateBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            int blueState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0);
            switch (blueState) {
                case BluetoothAdapter.STATE_OFF: // 蓝牙关闭
                    iv_state_bluetooth.setBackground(null);
                    break;
                case BluetoothAdapter.STATE_TURNING_ON: // 蓝牙正在打开
                    iv_state_bluetooth.setBackground(null);
                    break;
                case BluetoothAdapter.STATE_ON:  // 蓝牙打开
                    iv_state_bluetooth.setBackground(null);
                    break;
                default: {
                    break;
                }
            }

        }
    };

    /**
     * 注册监听蓝牙开关状态的广播
     */
    private void registerBluetoothOnOffReceiver() {
        registerReceiver(blueStateBroadcastReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
    }

    /**
     * 注销监听蓝牙开关状态的广播
     */
    private void unRegiserBluetoothOnOffReceiver() {
        if (blueStateBroadcastReceiver != null) {
            unregisterReceiver(blueStateBroadcastReceiver);
        }
    }

    /**
     * 判断sd卡的状态
     */
    private boolean checkSDCardState() {
        boolean sdCardExist = Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);   //判断sd卡是否存在
        return sdCardExist;
    }

    /**
     * 注册检测外置的SD卡插拔状态的广播接收者
     */
    private void registerExternalSdState() {
        externalSdState = new ExternalSdState();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        intentFilter.addAction(Intent.ACTION_MEDIA_CHECKING);
        intentFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        intentFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        intentFilter.addAction(Intent.ACTION_MEDIA_EJECT);
        intentFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        intentFilter.addAction(Intent.ACTION_MEDIA_REMOVED);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(externalSdState, intentFilter);
    }

    /**
     * 注销广播接收者
     */
    private void unRegisterExternalSdState() {
        if (externalSdState != null) {
            unregisterReceiver(externalSdState);
        }
    }

    private ExternalSdState externalSdState;

    private class ExternalSdState extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_MEDIA_UNMOUNTED)) {
                ToastUtil.showToast("ACTION_MEDIA_UNMOUNTED");
                if (ChappieCarApplication.isDebug)
                    Log.e(TAG, "ACTION_MEDIA_UNMOUNTED");
            } else if (action.equals(Intent.ACTION_MEDIA_CHECKING)) {
                if (ChappieCarApplication.isDebug)
                    ToastUtil.showToast("ACTION_MEDIA_CHECKING");
                if (ChappieCarApplication.isDebug)
                    Log.e(TAG, "ACTION_MEDIA_CHECKING");
            } else if (action.equals(Intent.ACTION_MEDIA_MOUNTED)) {
                if (ChappieCarApplication.isDebug)
                    ToastUtil.showToast("ACTION_MEDIA_MOUNTED");
                Log.e(TAG, "ACTION_MEDIA_MOUNTED");
            } else if (action.equals(Intent.ACTION_MEDIA_EJECT)) {
                ToastUtil.showToast("ACTION_MEDIA_EJECT");
                if (ChappieCarApplication.isDebug)
                    Log.e(TAG, "ACTION_MEDIA_EJECT");
            } else if (action.equals(Intent.ACTION_MEDIA_UNMOUNTED)) {
                ToastUtil.showToast("ACTION_MEDIA_UNMOUNTED");
                if (ChappieCarApplication.isDebug)
                    Log.e(TAG, "ACTION_MEDIA_UNMOUNTED");
            } else if (action.equals(Intent.ACTION_MEDIA_REMOVED)) {
                ToastUtil.showToast("ACTION_MEDIA_REMOVED");
                if (ChappieCarApplication.isDebug)
                    Log.e(TAG, "ACTION_MEDIA_REMOVED");
            }
        }
    }

    /**
     * 胎压主界面上的设置按钮（进入设置界面）
     * @Override

     */
    public void onTPMSFragmentSettingBtuonClick() {
        FragmentTransaction ft = fm.beginTransaction();
        ft.hide(tpmsMainFragment);
        if (tpmsSettingFragment == null) {
            tpmsSettingFragment = new TPMSSettingFragment();
        }
        //    ft.add(R.id.fl_content, tpmsSettingFragment);
        ft.commit();
    }

    /**
     * 胎压设置界面返回按钮
     *

     */

    /**
     * 从胎压设置界面跳转到胎压发射配对界面
     * @Override
    public void onTPMSSettingFragment2Matching() {
    FragmentTransaction ft = fm.beginTransaction();
    ft.hide(tpmsSettingFragment);
    if (tmpsEmitterMatchingFragment == null) {
    tmpsEmitterMatchingFragment = new TPMSEmitterMatchingFragment();
    }
    //   ft.add(R.id.fl_content, tmpsEmitterMatchingFragment);
    ft.commit();
    }
     */


    /**
     * 胎压发射配对界面返回事件
     *

     */
    @Override
    public void OnTMPSEmitterMatchingFragmentBackPressed() {
        FragmentTransaction ft = fm.beginTransaction();
        if (tmpsEmitterMatchingFragment != null) {
            ft.remove(tmpsEmitterMatchingFragment);
        }
        ft.show(tpmsSettingFragment);
        ft.commit();
    }
    /**
     * 胎压设置界面进入胎压配对界面
     *@Override
    public void onTPMSSettingFragment2StyreChaging() {
    FragmentTransaction ft = fm.beginTransaction();
    ft.hide(tpmsSettingFragment);
    if (tpmStyreChangingFragment == null) {
    tpmStyreChangingFragment = new TPMStyreChangingFragment();
    }
    //   ft.add(R.id.fl_content, tpmStyreChangingFragment);
    ft.commit();
    }
     */



    /**
     * 轮胎调换界面返回按钮点击事件
     *
     */
    @Override
    public void onTPMStyreChangingFragmentBackPressed() {
        FragmentTransaction ft = fm.beginTransaction();
        if (tpmStyreChangingFragment != null) {
            ft.remove(tpmStyreChangingFragment);
        }
        ft.show(tpmsSettingFragment);
        ft.commit();
    }

    /**
     * 胎压设置界面点击执行放大模块操作
     *   @Override
    public void onTPMSMaxView() {
    if (ChappieCarApplication.isMaxView.containsKey(item_tpms_watch)) {  // 屏蔽重复重置模块
    return;
    }
    maxView(item_tpms_watch);
    // 将选中的模块存入集合
    ChappieCarApplication.isMaxView.put(item_tpms_watch, true);
    }
     */

}

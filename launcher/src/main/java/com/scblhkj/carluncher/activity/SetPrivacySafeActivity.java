package com.scblhkj.carluncher.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.domain.CPLSetBean;
import com.scblhkj.carluncher.domain.ResultBean;
import com.scblhkj.carluncher.utils.GsonUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * 隐私安全设置
 */
public class SetPrivacySafeActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    @Bind(R.id.iv_left)
    ImageView ivLeft;
    @Bind(R.id.rl_change_password)
    RelativeLayout rlChangePassword;
    @Bind(R.id.cbOBD)
    CheckBox cbOBD;
    @Bind(R.id.cbTPMS)
    CheckBox cbTPMS;
    @Bind(R.id.cbFile)
    CheckBox cbFile;
    @Bind(R.id.cbHddvr)
    CheckBox cbHddvr;
    @Bind(R.id.cbPas)
    CheckBox cbPas;   // 是否关闭密码验证
    private CPLSetBean cplSetBean;

    private static final String TAG = "隐私安全设置";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_privacy_safe);
        ButterKnife.bind(this);
        cplSetBean = (CPLSetBean) aCache.getAsObject(ChappieConstant.CPSBK);
        initView();
        initData();
    }

    @Override
    protected void initView() {
        cbOBD.setChecked(cplSetBean.isPrivacyObd());
        cbTPMS.setChecked(cplSetBean.isPrivacyTpms());
        cbFile.setChecked(cplSetBean.isPrivacyFile());
        cbHddvr.setChecked(cplSetBean.isPrivacyddvr());
    }

    @Override
    protected void initData() {

        // 首先加载本地缓存的设置选项
        ivLeft.setOnClickListener(this);
        cbOBD.setOnCheckedChangeListener(this);
        cbTPMS.setOnCheckedChangeListener(this);
        cbFile.setOnCheckedChangeListener(this);
        cbHddvr.setOnCheckedChangeListener(this);
        cbPas.setOnCheckedChangeListener(this);
        // 再加载有网络情况下的设置选项
        //http://localhost/app/RMSet/rest/IEMI/设备号参数
        String url = "http://" + ChappieConstant.HTTP_HOST + "/app/RMSetPrivacySafety/rest/IEMI/" + ChappieCarApplication.DEVICE_IMEI;
        OkHttpUtils.get().url(url).build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e) {
                Log.e(TAG, "请求设置数据异常");
            }

            @Override
            public void onResponse(String s) {
                if (!TextUtils.isEmpty(s)) {
                    ReturnBean bean = GsonUtil.jsonToBean(s, ReturnBean.class);
                    if (bean.status.equals("1")) {
                        // 行车记录仪
                        if (bean.car_travel_recorder.equals("1")) {
                            cbHddvr.setChecked(true);
                            cplSetBean.setPrivacyddvr(true);
                        } else if (bean.car_travel_recorder.equals("0")) {
                            cbHddvr.setChecked(false);
                            cplSetBean.setPrivacyddvr(false);
                        }

                        // OBD
                        if (bean.obd.equals("1")) {
                            cbOBD.setChecked(true);
                            cplSetBean.setPrivacyObd(true);
                        } else if (bean.obd.equals("0")) {
                            cbOBD.setChecked(false);
                            cplSetBean.setPrivacyObd(false);
                        }

                        // 胎压
                        if (bean.tire_pressure.equals("1")) {
                            cbTPMS.setChecked(true);
                            cplSetBean.setPrivacyTpms(true);
                        } else if (bean.tire_pressure.equals("0")) {
                            cbTPMS.setChecked(false);
                            cplSetBean.setPrivacyTpms(false);
                        }

                        // 文件
                        if (bean.file_set.equals("1")) {
                            cbFile.setChecked(true);
                            cplSetBean.setPrivacyFile(true);
                        } else if (bean.file_set.equals("0")) {
                            cbFile.setChecked(false);
                            cplSetBean.setPrivacyFile(false);
                        }
                    }
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_left: {
                finish();
                break;
            }
            case R.id.rl_change_password: {
                intent2Activity(ChangePrivacyActivity.class);
                break;
            }
            default: {
                break;
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cbOBD: {
                cplSetBean.setPrivacyObd(isChecked);
                break;
            }
            case R.id.cbTPMS: {
                cplSetBean.setPrivacyTpms(isChecked);
                break;
            }
            case R.id.cbFile: {
                cplSetBean.setPrivacyFile(isChecked);
                break;
            }
            case R.id.cbHddvr: {
                cplSetBean.setPrivacyddvr(isChecked);
                break;
            }
            case R.id.cbPas: {
                cplSetBean.setPrivacyPaw(isChecked);  // 关闭隐私密码
                break;
            }
            default: {
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        String url = "http://" + ChappieConstant.HTTP_HOST + "/app/RMSetPrivacySafety/rest";
        OkHttpUtils.post()
                .url(url)
                .addParams("IMEI", ChappieCarApplication.DEVICE_IMEI)
                .addParams("car_travel_recorder", cplSetBean.isPrivacyddvr() ? "1" : "0")
                .addParams("obd", cplSetBean.isPrivacyObd() ? "1" : "0")
                .addParams("file", cplSetBean.isPrivacyFile() ? "1" : "0")
                .addParams("tire_pressure", cplSetBean.isPrivacyTpms() ? "1" : "0")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e) {
                        Log.e(TAG, "提交服务器出现异常" + e.toString());
                    }

                    @Override
                    public void onResponse(String s) {
                        if (!TextUtils.isEmpty(s)) {
                            ResultBean bean = GsonUtil.jsonToBean(s, ResultBean.class);
                            if (bean.status.equals("1")) {  // 正常
                                Log.e(TAG, "请求正常，接口返回数据为" + s);
                                aCache.put(ChappieConstant.CPSBK, cplSetBean);
                            } else {   // 异常
                                Log.e(TAG, "请求异常，接口返回数据为" + s);
                            }
                        }
                    }
                });
        super.onBackPressed();
    }

    private class ReturnBean {
        public String status;
        public String msg;
        public String car_travel_recorder;
        public String obd;
        public String file_set;
        public String tire_pressure;
    }
}

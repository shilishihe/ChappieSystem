package com.scblhkj.carluncher.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.rey.material.widget.Button;
import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.domain.TPMSSerialBean;
import com.scblhkj.carluncher.service.SerialDataCommSerivce;
import com.scblhkj.carluncher.utils.SerialFunUtil;
import com.scblhkj.carluncher.utils.XORUtil;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 胎压检测主界面
 */
public class TMPSActivity extends BaseActivity implements View.OnClickListener {

    @Bind(R.id.tv_1)
    TextView tv_1;
    @Bind(R.id.tv_2)
    TextView tv_2;
    @Bind(R.id.tv_3)
    TextView tv_3;
    @Bind(R.id.tv_4)
    TextView tv_4;
    @Bind(R.id.tv_5)
    TextView tv_5;
    @Bind(R.id.tv_6)
    TextView tv_6;
    @Bind(R.id.tv_7)
    TextView tv_7;
    @Bind(R.id.tv_8)
    TextView tv_8;
    @Bind(R.id.tv_9)
    TextView tv_9;
    @Bind(R.id.tv_10)
    TextView tv_10;
    @Bind(R.id.tv_11)
    TextView tv_11;
    @Bind(R.id.tv_12)
    TextView tv_12;
    @Bind(R.id.tv_13)
    TextView tv_13;
    @Bind(R.id.tv_14)
    TextView tv_14;
    @Bind(R.id.tv_15)
    TextView tv_15;
    @Bind(R.id.tv_16)
    TextView tv_16;
    @Bind(R.id.bt_set)
    Button bt_set;
    @Bind(R.id.bt_id)
    Button bt_id;
    @Bind(R.id.bt_dinaya)
    Button bt_dinaya;
    @Bind(R.id.bt_diaohuan)
    Button btDiaohuan;
    @Bind(R.id.bt_xuexi)
    Button btXuexi;

    private BroadcastReceiver serialCommReceiver;
    private static final String TAG = "TMPSActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tmps);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    protected void onResume() {
        registerSerialCommReceiver();
        super.onResume();
    }

    @Override
    protected void onPause() {
        unRegisterSerialCommReceiver();
        super.onPause();
    }


    @Override
    protected void initView() {
        bt_set.setOnClickListener(this);
        bt_id.setOnClickListener(this);
        bt_dinaya.setOnClickListener(this);
        btDiaohuan.setOnClickListener(this);
        btXuexi.setOnClickListener(this);
    }

    @Override
    protected void initData() {
        // 初始化串口通信的接收者
        serialCommReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (SerialDataCommSerivce.SERIAL_TPMS_ACTION.equals(action)) {
                    // 处理代码
                    Bundle bundle = intent.getExtras();
                    TPMSSerialBean bean = (TPMSSerialBean) bundle.getSerializable(SerialDataCommSerivce.SerialHelperControl.SERIAL_TMPA_DATA);
                    if (bean != null) {
                        int[][] datass = bean.getDatasss();
                        for (int i = 0; i < datass.length; i++) {
                            if ((datass[i][0] == 0x01) && (datass[i][2] == 0x02)) {
                                setDatas(datass[i]);
                            }
                        }
                    }
                }
            }
        };
    }


    private void setDatas(int[] rd) {
        // 解析数据
        if (rd[0] == 1 && rd[1] == 255) {  //判断包头
            if (rd[2] == 0X02) {  //判断是否是胎压数据
                if (doCheckData(rd) == rd[rd[3] + 3]) {
                    switch (rd[6]) {
                        case 0X0a: {
                            switch (rd[7]) {
                                case 0X00: {
                                    tv_1.setText("轮胎1压力值" + rd[8]);
                                    tv_2.setText("轮胎1温度" + rd[9]);
                                    break;
                                }
                                case 0X01: {
                                    tv_3.setText("轮胎2压力值" + rd[8]);
                                    tv_4.setText("轮胎2温度" + rd[9]);
                                    break;
                                }
                                case 0X10: {
                                    tv_5.setText("轮胎3压力值" + rd[8]);
                                    tv_6.setText("轮胎3温度" + rd[9]);
                                    break;
                                }
                                case 0X11: {
                                    tv_7.setText("轮胎4压力值" + rd[8]);
                                    tv_8.setText("轮胎4温度" + rd[9]);
                                    break;
                                }
                                case 0x20: {
                                    tv_13.setText("左前轮电压:" + Integer.toHexString(rd[9]));
                                    tv_14.setText("右前轮电压:" + Integer.toHexString(rd[10]));
                                    tv_15.setText("左后轮电压:" + Integer.toHexString(rd[11]));
                                    tv_16.setText("右后轮电压:" + Integer.toHexString(rd[12]));
                                    break;
                                }
                            }
                            break;
                        }
                        case 0x09: {
                            switch (rd[7]) {
                                case 0X01: {
                                    tv_9.setText("ID_1 :" + XORUtil.int2HexString(rd[8]) + XORUtil.int2HexString(rd[9]) + XORUtil.int2HexString(rd[10]) + XORUtil.int2HexString(rd[11]));
                                    break;
                                }
                                case 0X02: {
                                    tv_10.setText("ID_2 :" + XORUtil.int2HexString(rd[8]) + XORUtil.int2HexString(rd[9]) + XORUtil.int2HexString(rd[10]) + XORUtil.int2HexString(rd[11]));
                                    break;
                                }
                                case 0X03: {
                                    tv_11.setText("ID_3 :" + XORUtil.int2HexString(rd[8]) + XORUtil.int2HexString(rd[9]) + XORUtil.int2HexString(rd[10]) + XORUtil.int2HexString(rd[11]));
                                    break;
                                }
                                case 0X04: {
                                    tv_12.setText("ID_4 :" + XORUtil.int2HexString(rd[8]) + XORUtil.int2HexString(rd[9]) + XORUtil.int2HexString(rd[10]) + XORUtil.int2HexString(rd[11]));
                                    break;
                                }
                            }
                        }
                    }

                } else {
                    if (ChappieCarApplication.isDebug)
                        Log.e(TAG, "胎压数据不合法");
                }
            }
        }
    }

    /**
     * 异或校验检查数据的准确性
     */

    private int doCheckData(int[] datas) {
        int r = datas[4];
        int l = datas[3];
        for (int i = 5; i < l + 3; i++) {
            r = r ^ datas[i];
        }
        return r;
    }

    /**
     * 注册串口通信的广播
     */
    private void registerSerialCommReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SerialDataCommSerivce.SERIAL_TPMS_ACTION);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(serialCommReceiver, intentFilter);
    }

    /**
     * 注销串口通信的广播
     */
    private void unRegisterSerialCommReceiver() {
        if (serialCommReceiver != null) {
            unregisterReceiver(serialCommReceiver);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_set: {
                intent2Activity(TMPSSettingActivity.class);
                break;
            }
            case R.id.bt_id: {
                /**
                 * 获取轮胎序列号指令
                 */
                byte tmpData[] = new byte[]{(byte) 0x01, (byte) 0xff, (byte) 0x02, (byte) 0x06, (byte) 0x55, (byte) 0xaa, (byte) 0x06, (byte) 0x07, (byte) 0x00};
                byte sendData[] = XORUtil.xorByteArray(tmpData);
                sendData2Port(sendData);
                if (ChappieCarApplication.isDebug)
                    Log.e(TAG, "胎压界面准备要发送的数据是 " + SerialFunUtil.ByteArrToHexForLog(sendData));
                break;
            }
            case R.id.bt_dinaya: {
                /**
                 * 获取轮胎电池电压指令
                 */
                byte tmpData[] = new byte[]{(byte) 0x01, (byte) 0xff, (byte) 0x02, (byte) 0x06, (byte) 0x55, (byte) 0xaa, (byte) 0x06, (byte) 0x02, (byte) 0x01};
                byte sendData[] = XORUtil.xorByteArray(tmpData);
                sendData2Port(sendData);
                break;
            }


            case R.id.bt_diaohuan: {  //轮胎调换
                intent2Activity(TyreTanspositionActivity.class);
                break;
            }

            case R.id.bt_xuexi: {   //轮胎学习
                break;
            }

            default: {
                break;
            }
        }
    }

    /**
     * 向串口发送数据
     */
    private void sendData2Port(byte[] bs) {
        Intent dataIntent = new Intent();
        dataIntent.setAction(SerialDataCommSerivce.SERIAL_ACTION_RECEIVE);
        dataIntent.addCategory(Intent.CATEGORY_DEFAULT);
        Bundle bundle = new Bundle();
        bundle.putByteArray(TMPSSettingActivity.CMD, bs);
        dataIntent.putExtras(bundle);
        sendBroadcast(dataIntent);
    }
}

package com.scblhkj.carluncher.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.utils.StorageCapacityUtil;

/**
 * 我的查派界面
 */
public class MyChappieActivity extends BaseActivity implements View.OnClickListener {


    private ImageView im_userInfo;
    private ImageView im_myCar;
    private ImageView im_product;
    private ImageView im_setting;
    private ImageView iv_left;
    // 容量存储工具类
    private StorageCapacityUtil storageCapacityUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_chappie);
        initView();
        initData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void initView() {
        im_userInfo = (ImageView) findViewById(R.id.im_userInfo);
        im_myCar = (ImageView) findViewById(R.id.im_myCar);
        im_product = (ImageView) findViewById(R.id.im_product);
        im_setting = (ImageView) findViewById(R.id.im_setting);
        iv_left = (ImageView) findViewById(R.id.iv_left);
    }

    @Override
    protected void initData() {
        storageCapacityUtil = new StorageCapacityUtil(this);
        im_userInfo.setOnClickListener(this);
        im_myCar.setOnClickListener(this);
        im_product.setOnClickListener(this);
        im_setting.setOnClickListener(this);
        iv_left.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.im_userInfo: {  // 个人信息
                intent2Activity(PersonInfoActivity.class);
                break;
            }
            case R.id.im_myCar: {  // 我的汽车
                intent2Activity(MyCarInfoActivity.class);
                break;
            }
            case R.id.im_setting: {  // 通用设置
                intent2Activity(CommonSettingActivity.class);
                break;
            }
            default: {
                break;
            }
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finish();
            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

}

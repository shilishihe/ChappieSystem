package com.scblhkj.carluncher.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.domain.CPLSetBean;
import com.scblhkj.carluncher.utils.ToastUtil;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 设置隐私密码
 */
public class SetPrivacyActivity extends BaseActivity implements View.OnClickListener {

    @Bind(R.id.iv_left)
    ImageView ivLeft;
    @Bind(R.id.et_password)
    EditText etPassword;
    @Bind(R.id.et_repassword)
    EditText etRepassword;
    @Bind(R.id.bt_confirm)
    Button btConfirm;
    private CPLSetBean cplSetBean;

    private static final String TAG = "SetPrivacyActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_privacy);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        cplSetBean = (CPLSetBean) aCache.getAsObject(ChappieConstant.CPSBK);
        btConfirm.setOnClickListener(this);
        ivLeft.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_confirm: {
                Log.e(TAG, "确定按钮被点击");
                if (checkPassWord()) {  // 验证通过
                    cplSetBean.setPrivacyPassword(etPassword.getText().toString());   // 保存隐私密码
                    aCache.put(ChappieConstant.CPSBK, cplSetBean);
                    intent2Activity(SetPrivacySafeActivity.class);
                    finish();
                }
                break;
            }
            case R.id.iv_left: {
                finish();
                break;
            }
            default: {
                break;
            }
        }
    }

    private boolean checkPassWord() {
        boolean b = true;
        String pas = etPassword.getText().toString().trim();
        int len = pas.length();
        String rpas = etRepassword.getText().toString().trim();
        if (len != 6) {
            ToastUtil.showToast("密码长度必须为6位");
            b = false;
        }
        if (!pas.equals(rpas)) {
            ToastUtil.showToast("前后两次输入的密码不一致");
            b = false;
        }
        return b;
    }
}

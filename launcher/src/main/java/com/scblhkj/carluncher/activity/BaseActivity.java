package com.scblhkj.carluncher.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.domain.CPLSetBean;
import com.scblhkj.carluncher.utils.ACache;

import java.io.Serializable;

/**
 * Created by he on 2015/10/3.
 * ..
 * 所有Activity的基类
 */
public abstract class BaseActivity extends FragmentActivity {

    private static String TAG = "BaseActivity";
    protected ACache aCache;
    protected CPLSetBean cplSetBean;
    private WindowManager.LayoutParams wmParams;
    private WindowManager mWindowManager;
    private RelativeLayout mFloatLayout;
    private int top;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aCache = ACache.get(this);
        if (cplSetBean == null) {
            aCache.put(ChappieConstant.CPSBK, new CPLSetBean());
        }
        // 设置没有标题栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // 设置Activity为全屏模式
         getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // 设置为横屏模式
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        // 设置背景为空
        getWindow().setBackgroundDrawable(null);
        setContentView(R.layout.activity_base);

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Rect rect = new Rect();
        // /取得整个视图部分,注意，如果你要设置标题样式，这个必须出现在标题样式之后，否则会出错
        getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        top = rect.top;//状态栏的高度，所以rect.height,rect.width分别是系统的高度的宽度

        Log.i("top", "" + top);
    }
    /**
     * 初始化布局
     */
    abstract protected void initView();


    /**
     * 初始化数据
     */
    abstract protected void initData();

    /**
     * Activity跳转
     *
     * @param tarActivity
     */
    protected void intent2Activity(Class<? extends Activity> tarActivity) {
        Intent intent = new Intent(this, tarActivity);
        startActivity(intent);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.i(TAG, "onConfigurationChanged");
        super.onConfigurationChanged(newConfig);
    }

    public void putSerialzable(String key, Serializable t) {
        Log.e(TAG, t.toString());
        aCache.put(key, t);
        Log.e(TAG, "对象被存入");
    }


    public Object getSerialzable(String key) {
        return aCache.getAsObject(key);
    }

}

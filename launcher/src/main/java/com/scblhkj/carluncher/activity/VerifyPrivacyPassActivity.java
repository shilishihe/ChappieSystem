package com.scblhkj.carluncher.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.domain.CPLSetBean;
import com.scblhkj.carluncher.utils.ToastUtil;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 验证隐私密码界面
 */
public class VerifyPrivacyPassActivity extends BaseActivity implements View.OnClickListener {

    @Bind(R.id.iv_left)
    ImageView ivLeft;
    @Bind(R.id.et_password)
    EditText etPassword;
    @Bind(R.id.bt_confirm)
    Button btConfirm;
    @Bind(R.id.tv_fogetPass)
    TextView tvFogetPass;
    private CPLSetBean cplSetBean;
    private static final String TAG = "验证隐私密码";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_privacy_pass);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        cplSetBean = (CPLSetBean) aCache.getAsObject(ChappieConstant.CPSBK);
        tvFogetPass.setOnClickListener(this);
        btConfirm.setOnClickListener(this);
        ivLeft.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_left: {
                finish();
                break;
            }
            case R.id.et_password: {   // 重置密码

                break;
            }
            case R.id.bt_confirm: {
                String pass = etPassword.getText().toString().trim();
                Log.e(TAG, cplSetBean.toString());
                if (cplSetBean.getPrivacyPassword().equals(pass)) {  // 校验通过
                    intent2Activity(SetPrivacySafeActivity.class);
                } else {
                    ToastUtil.showToast("密码有误,请重新输入");
                }
                break;
            }
            default: {
                break;
            }
        }
    }
}

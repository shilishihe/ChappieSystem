package com.scblhkj.carluncher.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.service.DriveVideoRecordService;

/**
 * 视频监控Activity
 */
public class VideoMonitorActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "VideoMonitorActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_monitor);
        initView();
        initData();
    }

    @Override
    protected void onResume() {
        maxWindowManager();
        super.onResume();
    }

    @Override
    protected void onPause() {
        minWindowManager();
        super.onPause();
    }

    @Override
    protected void initView() {
    }

    @Override
    protected void initData() {
    }

    @Override
    public void onClick(View view) {
    }

    /**
     * 切换到副摄像头录制模式
     */
    private void switch2Scond() {
        Intent intent = new Intent();
        intent.setAction(DriveVideoRecordService.RESTART_SECOND_RECORD);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intent);
        if (ChappieCarApplication.isDebug)
            Log.e(TAG, "通知录制视频服务放大录制界面到屏幕一半大小");
    }

    /**
     * 切换到主摄像头录制模式
     */
    private void switch2Main() {
        Intent intent = new Intent();
        intent.setAction(DriveVideoRecordService.RESTART_MAIN_RECORD);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intent);
        if (ChappieCarApplication.isDebug)
            Log.e(TAG, "通知录制视频服务放大录制界面到屏幕一半大小");
    }

    /**
     * 展示一般的录制视频界面
     */
    private void showHalfWindowManager() {
        Intent intent = new Intent();
        intent.setAction(DriveVideoRecordService.HALF_WINDOW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intent);
        if (ChappieCarApplication.isDebug)
            Log.e(TAG, "通知录制视频服务放大录制界面到屏幕一半大小");
    }

    /**
     * 缩小录制视频窗口到最小化
     */
    private void minWindowManager() {
        Intent intent = new Intent();
        intent.setAction(DriveVideoRecordService.MIN_WINDOW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intent);
        if (ChappieCarApplication.isDebug)
            Log.e(TAG, "通知录制视频服务录制界面缩小到最小");
    }

    /**
     * 缩小录制视频窗口到最小化
     */
    private void maxWindowManager() {
        Intent intent = new Intent();
        intent.setAction(DriveVideoRecordService.MAX_WINDOW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intent);
        if (ChappieCarApplication.isDebug)
            Log.e(TAG, "通知录制视频服务录制界面缩小到最小");
    }

    /**
     * 主摄像头拍照
     */
    private void takeMainPicture() {
        Intent intent = new Intent();
        intent.setAction(DriveVideoRecordService.TAKE_MAIN_PICTRUE);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intent);
        if (ChappieCarApplication.isDebug)
            Log.e(TAG, "通知主摄像头拍照");
    }


    /**
     * 主摄像头拍照
     */
    private void takeSecondPicture() {
        Intent intent = new Intent();
        intent.setAction(DriveVideoRecordService.TAKE_SECOND_PICTRUE);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intent);
        if (ChappieCarApplication.isDebug)
            Log.e(TAG, "通知副摄像头拍照");
    }

    /**
     * 切换到到车摄像头
     */
    private void switch2BackView() {
        Intent intent = new Intent();
        intent.setAction(DriveVideoRecordService.RESTART_BACK_VIEW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intent);
    }
}

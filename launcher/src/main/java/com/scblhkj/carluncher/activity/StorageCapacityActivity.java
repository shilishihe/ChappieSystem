package com.scblhkj.carluncher.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.domain.StorageCapacityBean;
import com.scblhkj.carluncher.utils.FileUtil;
import com.scblhkj.carluncher.utils.StorageCapacityUtil;
import com.scblhkj.carluncher.widget.hellocharts.model.PieChartData;
import com.scblhkj.carluncher.widget.hellocharts.model.SliceValue;
import com.scblhkj.carluncher.widget.hellocharts.util.ChartUtils;
import com.scblhkj.carluncher.widget.hellocharts.view.PieChartView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 存储容量Activity
 */
public class StorageCapacityActivity extends BaseActivity implements View.OnClickListener {

    @Bind(R.id.tv_inside_totle)
    TextView tvInsideTotle;
    @Bind(R.id.chart_inside)
    PieChartView chartInside;
    @Bind(R.id.tv_outside_totle)
    TextView tvOutsideTotle;
    @Bind(R.id.chart_outside)
    PieChartView chartOutside;
    @Bind(R.id.iv_left)
    ImageView ivLeft;
    private PieChartData insideData;
    private StorageCapacityUtil storageCapacityUtil;
    private MyChappieActivity myChappieActivity;
    private static final String TAG = "StorageCapacity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storage_capacity);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    protected void initView() {
        initPieChart(chartInside);
        initPieChart(chartOutside);
    }

    @Override
    protected void initData() {
        storageCapacityUtil = new StorageCapacityUtil(this);
        storageCapacityUtil.getStorageCapacityBean();
        fillChartInsideData();
        StorageCapacityAsyn storageCapacityAsyn = new StorageCapacityAsyn();
        storageCapacityAsyn.execute();
        ivLeft.setOnClickListener(this);
    }

    /**
     * 初始化饼图
     */
    private void initPieChart(PieChartView view) {
        view.setValueSelectionEnabled(true);
        view.setChartRotationEnabled(true);
        view.setCircleFillRatio(0.6f);
    }

    /**
     * 给手机内存图表赋值
     */
    private void fillChartInsideData() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_left: {
                finish();
                break;
            }
            default: {
                break;
            }
        }
    }


    /**
     * 查询内存占用类
     */
    private class StorageCapacityAsyn extends AsyncTask<Void, Void, StorageCapacityBean> {

        @Override
        protected StorageCapacityBean doInBackground(Void... params) {
            StorageCapacityBean bean = new StorageCapacityBean();
            long sdVideoSize = storageCapacityUtil.getSDCardVideoSize();
            long sdPicSize = storageCapacityUtil.getSDCardPicSize();
            long sdASize = storageCapacityUtil.getSDCardAvailableSpace();
            long sdTotal = storageCapacityUtil.getSDTotalSize();
            long sdOtherSize = sdTotal - sdASize + sdVideoSize + sdPicSize;

            long tfVideoZize = storageCapacityUtil.getTFVideoSpace();
            long tfPicSize = storageCapacityUtil.getTFPictureSpace();
            long tfASize = storageCapacityUtil.getTFCardAvailableSpace();
            long tfTotal = storageCapacityUtil.getTFTotalSize();
            long tfOtherSize = tfTotal - tfASize + tfPicSize + tfPicSize;
            // -------------------机身内存------------------------
            bean.setpInerAvalibleS(sdASize);
            bean.setpInerOherS(sdOtherSize);
            bean.setpInerVideoS(sdVideoSize);
            bean.setpInerPicS(sdPicSize);
            bean.setpInerTotleS(sdTotal);
            // --------------------外置SD卡内存------------------------------
            bean.setpOuterAvalibleS(tfASize);
            bean.setpOuterPicS(tfPicSize);
            bean.setpOuterTotalS(tfTotal);
            bean.setpOuterVideoS(tfVideoZize);
            bean.setpOutOthers(tfOtherSize);
            return bean;
        }

        @Override
        protected void onPostExecute(StorageCapacityBean bean) {
            initChartDataFBean(bean);
        }
    }

    private void initFillCharData(List<SliceValue> v, PieChartView p, long av) {
        PieChartData iData = new PieChartData(v);
        iData.setHasLabels(true);
        iData.setCenterText1FontSize(15);
        iData.setCenterText1Color(getResources().getColor(R.color.color_444444));
        iData.setValueLabelBackgroundColor(getResources().getColor(R.color.color_null));
        iData.setValueLabelsTextColor(getResources().getColor(R.color.color_444444));
        iData.setHasLabelsOnlyForSelected(false);
        iData.setHasLabelsOutside(true);
        iData.setHasCenterCircle(true);
        iData.setSlicesSpacing(2);
        iData.setCenterText1(FileUtil.formetFileSize(av));
        p.setPieChartData(iData);
    }

    /**
     * 初始化图表数据
     *
     * @param bean
     */
    private void initChartDataFBean(StorageCapacityBean bean) {
        // 内置
        List<SliceValue> iValues = new ArrayList<SliceValue>();
        SliceValue iVideo = new SliceValue(bean.getpInerVideoS(), ChartUtils.pickColor());
        iVideo.setLabel("视频" + FileUtil.formetFileSize(bean.getpInerVideoS()));
        Log.e(TAG, "视频" + FileUtil.formetFileSize(bean.getpInerVideoS()));
        iValues.add(iVideo);
        SliceValue iPic = new SliceValue(bean.getpInerPicS(), ChartUtils.pickColor());
        iPic.setLabel("图片" + FileUtil.formetFileSize(bean.getpInerPicS()));
        Log.e(TAG, "图片" + FileUtil.formetFileSize(bean.getpInerPicS()));
        iValues.add(iPic);
        SliceValue iOther = new SliceValue(bean.getpInerOherS(), ChartUtils.pickColor());
        iOther.setLabel("其他" + FileUtil.formetFileSize(bean.getpInerOherS()));
        Log.e(TAG, "其他" + FileUtil.formetFileSize(bean.getpInerOherS()));
        iValues.add(iOther);
        initFillCharData(iValues, chartInside, bean.getpInerAvalibleS());

        // 外置
        List<SliceValue> OValues = new ArrayList<SliceValue>();
        SliceValue oVideo = new SliceValue(bean.getpOuterVideoS(), ChartUtils.pickColor());
        oVideo.setLabel("视频" + FileUtil.formetFileSize(bean.getpOuterVideoS()));
        Log.e(TAG, "视频" + FileUtil.formetFileSize(bean.getpOuterVideoS()));
        OValues.add(oVideo);
        SliceValue OPic = new SliceValue(bean.getpOuterPicS(), ChartUtils.pickColor());
        OPic.setLabel("图片" + FileUtil.formetFileSize(bean.getpOuterPicS()));
        Log.e(TAG, "图片" + FileUtil.formetFileSize(bean.getpOuterPicS()));
        OValues.add(OPic);
        SliceValue OOther = new SliceValue(bean.getpOutOthers(), ChartUtils.pickColor());
        OOther.setLabel("其他" + FileUtil.formetFileSize(bean.getpOutOthers()));
        Log.e(TAG, "其他" + FileUtil.formetFileSize(bean.getpOutOthers()));
        OValues.add(OOther);
        initFillCharData(OValues, chartOutside, bean.getpOuterAvalibleS());

        tvInsideTotle.setText("总内存" + FileUtil.formetFileSize(bean.getpInerTotleS()));
        tvOutsideTotle.setText("总内存" + FileUtil.formetFileSize(bean.getpOuterTotalS()));
    }

}

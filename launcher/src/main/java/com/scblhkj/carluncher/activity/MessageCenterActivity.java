package com.scblhkj.carluncher.activity;

import android.os.Bundle;

import com.scblhkj.carluncher.R;

/**
 * 消息中心Activity
 */
public class MessageCenterActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_center);
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }
}

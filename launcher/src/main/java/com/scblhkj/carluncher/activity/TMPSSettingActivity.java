package com.scblhkj.carluncher.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.rey.material.widget.Switch;
import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.utils.SpUtil;
import com.scblhkj.carluncher.utils.ToastUtil;
import com.scblhkj.carluncher.widget.Slider;

/**
 * 胎压设置界面
 */
public class TMPSSettingActivity extends BaseActivity implements View.OnClickListener
        , Switch.OnCheckedChangeListener
        , Slider.OnPositionChangeListener
        , RadioGroup.OnCheckedChangeListener {

    public static final String CMD = "SERIAL_CMD";
    // public static final String TAG = "TMPSSettingActivity";
    private Button tv_emitter_matching;
    private RadioGroup rg_pres_unit;
    private RadioGroup rg_temp_unit;
    private Button tv_tyre_trans;
    private Switch sw_pre;
    private Switch sw_alert;
    // 压力上限
    private TextView tv_pre_top_value;
    private Slider sl_alert_top_pre;
    // 压力下限
    private TextView tv_pre_low_value;
    private Slider sl_alert_low_pre;
    // 温度上限
    private TextView tv_temp_top_value;
    private Slider sl_aler_temp;
    // 重置按钮
    private Button bt_reset_params;
    private ImageView iv_left;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tmpssetting);
        initView();
        initData();
    }

    @Override
    protected void initView() {
        tv_emitter_matching = (Button) findViewById(R.id.tv_emitter_matching);
        rg_pres_unit = (RadioGroup) findViewById(R.id.rg_pres_unit);
        rg_temp_unit = (RadioGroup) findViewById(R.id.rg_temp_unit);
        tv_tyre_trans = (Button) findViewById(R.id.tv_tyre_trans);
        sw_pre = (Switch) findViewById(R.id.sw_pre);
        sw_alert = (Switch) findViewById(R.id.sw_alert);
        tv_pre_top_value = (TextView) findViewById(R.id.tv_pre_top_value);
        sl_alert_top_pre = (Slider) findViewById(R.id.sl_alert_top_pre);
        tv_pre_low_value = (TextView) findViewById(R.id.tv_pre_low_value);
        sl_alert_low_pre = (Slider) findViewById(R.id.sl_alert_low_pre);
        tv_temp_top_value = (TextView) findViewById(R.id.tv_temp_top_value);
        sl_aler_temp = (Slider) findViewById(R.id.sl_aler_temp);
        bt_reset_params = (Button) findViewById(R.id.bt_reset_params);
        iv_left = (ImageView) findViewById(R.id.iv_left);
    }

    @Override
    protected void initData() {
        // 按钮类
        tv_emitter_matching.setOnClickListener(this);
        tv_tyre_trans.setOnClickListener(this);
        bt_reset_params.setOnClickListener(this);
        // 组合框类
        rg_pres_unit.setOnCheckedChangeListener(this);
        rg_temp_unit.setOnCheckedChangeListener(this);
        // switch控件类
        sw_pre.setOnCheckedChangeListener(this);
        sw_alert.setOnCheckedChangeListener(this);
        // 滑动条控件类
        sl_alert_top_pre.setOnPositionChangeListener(this);
        sl_alert_low_pre.setOnPositionChangeListener(this);
        sl_aler_temp.setOnPositionChangeListener(this);
        // 返回按钮
        iv_left.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_emitter_matching: {  // 胎压发射器配对
                ToastUtil.showToast("胎压发射器配对");
                break;
            }
            case R.id.tv_tyre_trans: {  // 胎压轮胎调换
                intent2Activity(TyreTanspositionActivity.class);
                break;
            }
            case R.id.bt_reset_params: {  // 重置按钮
                ToastUtil.showToast("重置按钮");
                break;
            }
            case R.id.iv_left: {
                finish();
                break;
            }
            default: {
                break;
            }
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (radioGroup.getId()) {
            // 压力单位模块
            case R.id.rg_pres_unit: {
                int unit = SpUtil.getInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES);// 默认是 Kpa
                int max = sl_alert_top_pre.getMaxValue();  // 胎压上限最大值
                int min = sl_alert_top_pre.getMinValue();  // 胎压下限最小值
                int lmax = sl_alert_low_pre.getMaxValue(); // 胎压下限最大值
                int lmin = sl_alert_low_pre.getMinValue(); // 胎压下限最小值
                float tempValue = SpUtil.getFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX);   // 胎压上限当前值
                float tempLValue = SpUtil.getFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN);  // 胎压下限当前值
                switch (i) {   // 1代表Bar 2代表Kpa 3代表Psi
                    case R.id.rb_bar: {
                        if (2 == unit) {   // Kpa转Bar
                            /**
                             * 胎压上限部分
                             */
                            tempValue = tempValue * 0.01f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX, tempValue);
                            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES, 1);
                            sl_alert_top_pre.setValueRange((int) Math.ceil(min * 0.01), (int) Math.ceil(max * 0.01), false);
                            sl_alert_top_pre.setValue(tempValue, true);
                            tv_pre_top_value.setText(String.format("%.1f", (float) tempValue) + "Bar");
                            /**
                             * 胎压下限部分
                             */
                            tempLValue = tempLValue * 0.01f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN, tempLValue);
                            sl_alert_low_pre.setValueRange((int) Math.ceil(lmin * 0.01), (int) Math.ceil(lmax * 0.01), false);
                            sl_alert_low_pre.setValue(tempLValue, true);
                            tv_pre_low_value.setText(String.format("%.1f", (float) tempLValue) + "Bar");
                        } else if (3 == unit) {  // Psi转Bar
                            /**
                             * 胎压上限部分
                             */
                            tempValue = tempValue * 0.0689f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX, tempValue);
                            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES, 1);
                            sl_alert_top_pre.setValueRange((int) Math.ceil(min * 0.0689f), (int) Math.ceil(max * 0.0689f), false);
                            sl_alert_top_pre.setValue(tempValue, true);
                            tv_pre_top_value.setText(String.format("%.1f", (float) tempValue) + "Bar");
                            /**
                             * 胎压下限部分
                             */
                            tempLValue = tempLValue * 0.0689f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN, tempLValue);
                            sl_alert_low_pre.setValueRange((int) Math.ceil(lmin * 0.0689f), (int) Math.ceil(lmax * 0.0689f), false);
                            sl_alert_low_pre.setValue(tempLValue, true);
                            tv_pre_low_value.setText(String.format("%.1f", (float) tempLValue) + "Bar");
                        }
                        break;
                    }
                    case R.id.rb_kap: {
                        if (1 == unit) {   // Bar转Kpa
                            /**
                             * 胎压上限部分
                             */
                            tempValue = tempValue * 100f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX, tempValue);
                            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES, 2);
                            sl_alert_top_pre.setValueRange((int) Math.ceil(min * 100f), (int) Math.ceil(max * 100f), false);
                            sl_alert_top_pre.setValue(tempValue, true);
                            tv_pre_top_value.setText(String.format("%.1f", (float) tempValue) + "Kpa");
                            /**
                             * 胎压下限部分
                             */
                            tempLValue = tempLValue * 100f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN, tempLValue);
                            sl_alert_low_pre.setValueRange((int) Math.ceil(lmin * 100f), (int) Math.ceil(lmax * 100f), false);
                            sl_alert_low_pre.setValue(tempLValue, true);
                            tv_pre_low_value.setText(String.format("%.1f", (float) tempLValue) + "Kpa");
                        } else if (3 == unit) {  // Psi转Kpa
                            /**
                             * 胎压上限部分
                             */
                            tempValue = tempValue * 6.894f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX, tempValue);
                            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES, 2);
                            sl_alert_top_pre.setValueRange((int) Math.ceil(min * 6.894), (int) Math.ceil(max * 6.894), false);
                            sl_alert_top_pre.setValue(tempValue, true);
                            tv_pre_top_value.setText(String.format("%.1f", (float) tempValue) + "Kpa");
                            /**
                             * 胎压下限部分
                             */
                            tempLValue = tempLValue * 6.894f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN, tempLValue);
                            sl_alert_low_pre.setValueRange((int) Math.ceil(lmin * 6.894), (int) Math.ceil(lmax * 6.894), false);
                            sl_alert_low_pre.setValue(tempLValue, true);
                            tv_pre_low_value.setText(String.format("%.1f", (float) tempLValue) + "Kpa");
                        }
                        break;
                    }
                    case R.id.rb_psi: {
                        if (1 == unit) {   // Bar转PSI
                            /**
                             * 胎压上限部分
                             */
                            tempValue = tempValue * 14.503f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX, tempValue);
                            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES, 3);
                            sl_alert_top_pre.setValueRange((int) Math.ceil(min * 14.503), (int) Math.ceil(max * 14.503), false);
                            sl_alert_top_pre.setValue(tempValue, true);
                            tv_pre_top_value.setText(String.format("%.1f", (float) tempValue) + "Psi");
                            /**
                             * 胎压下限部分
                             */
                            tempLValue = tempLValue * 14.503f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN, tempLValue);
                            sl_alert_low_pre.setValueRange((int) Math.ceil(lmin * 14.503), (int) Math.ceil(lmax * 14.503), false);
                            sl_alert_low_pre.setValue(tempLValue, true);
                            tv_pre_low_value.setText(String.format("%.1f", (float) tempLValue) + "Psi");
                        } else if (2 == unit) {  // Kpa转Psi
                            /**
                             * 胎压上限部分
                             */
                            tempValue = tempValue * 0.145f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX, tempValue);
                            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES, 3);
                            sl_alert_top_pre.setValueRange((int) Math.ceil(min * 0.145), (int) Math.ceil(max * 0.145), false);
                            sl_alert_top_pre.setValue(tempValue, true);
                            tv_pre_top_value.setText(String.format("%.1f", (float) tempValue) + "Psi");
                            /**
                             * 胎压下限部分
                             */
                            tempLValue = tempLValue * 0.145f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN, tempLValue);
                            sl_alert_low_pre.setValueRange((int) Math.ceil(lmin * 0.145f), (int) Math.ceil(lmax * 0.145f), false);
                            sl_alert_low_pre.setValue(tempLValue, true);
                            tv_pre_low_value.setText(String.format("%.1f", (float) tempLValue) + "Psi");
                        }
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
            }


            case R.id.rg_temp_unit: {
                int unit = SpUtil.getInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_TEMP);
                int max = sl_aler_temp.getMaxValue();
                int min = sl_aler_temp.getMinValue();
                int tempMax = SpUtil.getInt2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_TEMP_MAX);
                switch (i) {
                    case R.id.rb_f: {  // 1代表摄氏度 2代表华氏度  点击了华氏度
                        if (1 == unit) {
                            tempMax = (int) (tempMax * 1.8f + 32);
                            max = (int) (max * 1.8f + 32);
                            min = (int) (min * 1.8f + 32);
                            SpUtil.saveInt2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_TEMP_MAX, tempMax);
                            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_TEMP, 2);
                            sl_aler_temp.setValueRange(min, max, false);
                            sl_aler_temp.setValue(tempMax, true);
                            tv_temp_top_value.setText(String.valueOf(tempMax + "℉"));
                        }
                        break;
                    }

                    case R.id.rb_dc: {  // 点击了摄氏度
                        if (2 == unit) {
                            tempMax = (int) ((tempMax - 32) / 1.8f);
                            max = (int) ((max - 32) / 1.8f);
                            min = (int) ((min - 32) / 1.8f);
                            SpUtil.saveInt2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_TEMP_MAX, tempMax);
                            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_TEMP, 1);
                            sl_aler_temp.setValueRange(min, max, false);
                            sl_aler_temp.setValue(tempMax, true);
                            tv_temp_top_value.setText(String.valueOf(tempMax + "℃"));
                        }

                        break;
                    }

                    default: {
                        break;
                    }
                }
                break;
            }
            default: {
                throw new RuntimeException("no such id method should invoke");
            }
        }

    }

    @Override
    public void onCheckedChanged(Switch view, boolean checked) {
        switch (view.getId()) {
            case R.id.sw_pre: {
                SpUtil.saveBoolean2SP(ChappieConstant.TMPS_FRAGMENT_SWITCH_PRES_ALARM, checked);
                if (ChappieCarApplication.isDebug)
                    Log.e(CMD, checked + "");
                break;
            }
            case R.id.sw_alert: {
                SpUtil.saveBoolean2SP(ChappieConstant.TMPS_FRAGMENT_SWITCH_ALARM, checked);
                break;
            }
            default: {
                break;
            }
        }
    }

    @Override
    public void onPositionChanged(Slider view, boolean fromUser, float oldPos, float newPos, float oldValue, float newValue) {
        switch (view.getId()) {
            case R.id.sl_alert_top_pre: {
                int presUnit = SpUtil.getInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES);
                if (1 == presUnit) {
                    tv_pre_top_value.setText(String.valueOf(newValue + "Bar"));
                } else if (2 == presUnit) {
                    tv_pre_top_value.setText(String.valueOf(newValue + "Kpa"));
                } else if (3 == presUnit) {
                    tv_pre_top_value.setText(String.valueOf(newValue + "Psi"));
                }
                SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX, newValue);
                break;
            }
            case R.id.sl_alert_low_pre: {
                int presUnit = SpUtil.getInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES);
                if (1 == presUnit) {
                    tv_pre_low_value.setText(String.valueOf(newValue) + "Bar");
                } else if (2 == presUnit) {
                    tv_pre_low_value.setText(String.valueOf(newValue) + "Kpa");
                } else if (3 == presUnit) {
                    tv_pre_low_value.setText(String.valueOf(newValue) + "Psi");
                }
                SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN, newValue);
                break;
            }
            case R.id.sl_aler_temp: {
                int tempUnit = SpUtil.getInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_TEMP);
                if (1 == tempUnit) {
                    tv_temp_top_value.setText(String.valueOf(newValue + "℃"));
                } else if (2 == tempUnit) {
                    tv_temp_top_value.setText(String.valueOf(newValue + "℉"));
                }
                SpUtil.saveInt2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_TEMP_MAX, (int) (newValue));
                break;
            }
            default: {
                throw new RuntimeException("no such id method should invoke");
            }
        }
    }


}


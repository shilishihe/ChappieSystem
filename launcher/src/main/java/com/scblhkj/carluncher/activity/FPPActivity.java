package com.scblhkj.carluncher.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.utils.ToastUtil;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 忘记隐私密码界面
 */
public class FPPActivity extends BaseActivity implements View.OnClickListener {

    @Bind(R.id.iv_left)
    ImageView ivLeft;
    @Bind(R.id.et_tel)
    EditText etTel;
    @Bind(R.id.bt_getCode)
    Button btGetCode;
    @Bind(R.id.et_yzm)
    EditText etYzm;
    @Bind(R.id.et_password)
    EditText etPassword;
    @Bind(R.id.et_repassword)
    EditText etRepassword;
    @Bind(R.id.bt_confirm)
    Button btConfirm;
    private int time = 120;
    private String messageAuthenticationCode; // 短信验证码


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1: {
                    btGetCode.setClickable(false);
                    time--;
                    btGetCode.setText(String.valueOf(time));
                    if (time == 0) {
                        handler.sendEmptyMessage(2);
                    } else {
                        handler.sendEmptyMessageDelayed(1, 1000);
                    }
                }
                case 2: {
                    time = 120;
                    btGetCode.setClickable(true);
                    btGetCode.setText("验证");
                }
                default: {
                    break;
                }
            }
            super.handleMessage(msg);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fpp);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    protected void initView() {
    }

    @Override
    protected void initData() {
        ivLeft.setOnClickListener(this);
        btGetCode.setOnClickListener(this);
        btConfirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_left: {
                finish();
                break;
            }
            case R.id.bt_confirm: {
                if (validation()) {
                    // 请求服务器验证
                }
                break;
            }
            case R.id.bt_getCode: {
                handler.sendEmptyMessage(1);
                btGetCode.setText(String.valueOf(time));
                break;
            }
            default: {
                break;
            }
        }
    }

    /**
     * 提交数据给服务器验证
     */
    private boolean validation() {
        boolean b = true;
        if (TextUtils.isEmpty(etYzm.getText().toString())) {
            ToastUtil.showToast("请先填写短信验证码");
            b = false;
        }
        if (etPassword.getText().toString().trim().length() != 6) {
            ToastUtil.showToast("密码格式有误");
            b = false;
        }
        if (!etPassword.getText().toString().trim().equals(etRepassword.getText().toString().trim())) {
            ToastUtil.showToast("两次输入的密码不一致");
            b = false;
        }
        return b;
    }
}

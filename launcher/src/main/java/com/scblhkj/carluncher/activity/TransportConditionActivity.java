package com.scblhkj.carluncher.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.amap.api.maps.AMap;
import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.CameraUpdate;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.db.CityElevationDao;
import com.scblhkj.carluncher.db.CityElevationDaoImpl;
import com.scblhkj.carluncher.domain.CityElevationBean;
import com.scblhkj.carluncher.domain.GPSInfo;
import com.scblhkj.carluncher.domain.OBDSerialBean;
import com.scblhkj.carluncher.service.SerialDataCommSerivce;
import com.scblhkj.carluncher.utils.ToastUtil;
import com.scblhkj.carluncher.widget.AndroidSegmentedControlView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 车辆行驶状态界面(指南针+MAP)
 */
public class TransportConditionActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener, AMap.OnMarkerClickListener {

    private static final int UPDATE_MARKER = 100; // 更新Marker
    private MapView mapView;
    private AMap aMap;
    private ImageView im_compass_needle;
    //  private Compass compass;
    private Marker marker;
    private Thread updateMarkerThread;
    private Runnable updateMarkerRunnable;
    private AndroidSegmentedControlView asc_map;
    private CheckBox cb_currentTraffic;
    private BroadcastReceiver broadcastReceiver;
    private TextView tvCarSpeed;  // 车速
    private TextView tv_curon;  // 转速
    private TextView tvTransmitterSpeed;  // 瞬时油耗
    private TextView tv_altitude;  // 海拔
    private LinkedList<GPSInfo> gpsInfos;  // 用于存储坐标
    private GPSInfo gpsInfo;
    private boolean isFistLocation;
    private boolean isShow = false; // 是否显示InfoWindow
    private CityElevationDao cityElevationDao;
    private List<CityElevationBean> cityElevationList = new ArrayList<CityElevationBean>();

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UPDATE_MARKER: {
                    if (gpsInfo != null) {
                        addCarMarkerOnMap(gpsInfo);
                    }
                    break;
                }
                default: {
                    break;
                }
            }
            super.handleMessage(msg);
        }
    };

    /**
     * 查询海拔信息
     */
    private void queryCityElevation(GPSInfo gpsInfo) {
        Log.e(TAG, "查询城市 = " + gpsInfo.getCity());
        cityElevationList.clear();
        cityElevationList.addAll(cityElevationDao.queryRecordByCityName(gpsInfo.getCity()));
        LatLng startLatlng = new LatLng(gpsInfo.getLatitude(), gpsInfo.getLongitude());
        float min = Integer.MAX_VALUE;
        Map<Float, Integer> cacheMap = new HashMap<Float, Integer>();
        for (int i = 0; i < cityElevationList.size(); i++) {
            LatLng tempEndLatlng = new LatLng(cityElevationList.get(i).getY(), cityElevationList.get(i).getX());
            float tempFloat = AMapUtils.calculateLineDistance(startLatlng, tempEndLatlng);
            cacheMap.put(tempFloat, i);
            if (tempFloat < min) {
                min = tempFloat;
            }
        }
        int keyIndex = cacheMap.get(min);
        tv_altitude.setText(cityElevationList.get(keyIndex).getH() + "");
    }


    /**
     * 初始化广播接收者
     */
    private void initBroadCastReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case SerialDataCommSerivce.SERIAL_OBD_ACTION: {
                        OBDSerialBean bean = (OBDSerialBean) intent.getExtras().getSerializable(SerialDataCommSerivce.SerialHelperControl.SERIAL_OBD_DATA);
                        int[][] datass = bean.getDatass();   // OBD数据
                        int length = datass.length;
                        for (int i = 0; i < length; i++) {
                            if (datass[i][0] == 0X28) {
                                if (datass[i][8] == 0x87) {
                                    tvCarSpeed.setText(String.valueOf(datass[i][15]));
                                    tvTransmitterSpeed.setText(String.valueOf(datass[i][13] * 256 + datass[i][14] / 4));
                                    tv_curon.setText(String.valueOf((datass[i][28] * 256 + datass[i][29]) / 10f));
                                }
                            }
                        }
                        break;
                    }
                    case ChappieConstant.GPS_INFO: {
                        gpsInfo = (GPSInfo) intent.getSerializableExtra(ChappieConstant.GPS_INFO);
                        if (isFistLocation) {
                            addCarMarkerOnMap(gpsInfo);
                            isFistLocation = false;
                        }
                        if (gpsInfo.getLongitude() != 0.0 && gpsInfo.getLatitude() != 0.0) {
                            gpsInfos.add(gpsInfo);
                            if (gpsInfos.size() > 3) { // 集合长度大于3，移出第一个坐标
                                gpsInfos.removeLast();
                                if (gpsInfos.size() == 3) {
                                    determineCarDirection(gpsInfos);
                                    queryCityElevation(gpsInfo);
                                }
                            }
                        }
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        };
    }

    /**
     * 确定汽车行驶的方向
     * 根据缓存的三个汽车的GPS坐标算出汽车的行驶朝向
     */
    private void determineCarDirection(List<GPSInfo> gpsInfos) {

        Log.e(TAG, "gpsInfo根据三个坐标点计算方向");

        // 1.在中国境内纬度越来越小则向南走，
        // 2.纬度越来也大表示向北走
        // 3.经度越来越大表示向东走
        // 4.经度越来越小表示向西走
        GPSInfo gpsInfo0 = gpsInfos.get(0);
        GPSInfo gpsInfo2 = gpsInfos.get(2);

        double la21 = gpsInfo2.getLatitude() - gpsInfo0.getLatitude();  // 纬度
        double lo21 = gpsInfo2.getLongitude() - gpsInfo0.getLongitude(); // 经度

        if (la21 != 0 && lo21 != 0) {
            int area = 1;
            int state = 1;

            if (la21 >= 0) {  // 纬度
                if (lo21 >= 0) {
                    area = 1;  // 东北
                } else {
                    area = 4;  // 西北
                }
            } else {
                if (lo21 >= 0) {  // 经度
                    area = 2;  // 东南
                } else {
                    area = 3;  // 西南
                }
            }

            if (la21 == 0) { // 纬度
                state = 0;  // 正南或是正北
            } else if (lo21 == 0) { // 经度
                state = 2;    //
            } else {
                double laclo = Math.abs(la21) / Math.abs(lo21);
                if ((laclo > 7 / 10) && (laclo < 10 / 7)) {
                    state = 1;
                } else if (laclo < 7 / 10) {
                    state = 2;
                } else {
                    state = 0;
                }
            }

            switch (area) {
                case 1: {   // 东北
                    switch (state) {
                        case 0: {
                            im_compass_needle.setBackgroundResource(R.mipmap.ic_pointer_north);
                            break;
                        }
                        case 1: {
                            im_compass_needle.setBackgroundResource(R.mipmap.ic_pointer_east_north);
                            break;
                        }
                        case 2: {
                            im_compass_needle.setBackgroundResource(R.mipmap.ic_pointer_south);
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                    break;
                }

                case 2: {  // 东南
                    switch (state) {
                        case 0: {
                            im_compass_needle.setBackgroundResource(R.mipmap.ic_pointer_south);
                            break;
                        }
                        case 1: {
                            im_compass_needle.setBackgroundResource(R.mipmap.ic_pointer_east_south);
                            break;
                        }
                        case 2: {
                            im_compass_needle.setBackgroundResource(R.mipmap.ic_pointer_east);
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                    break;
                }

                case 3: {  // 西南
                    switch (state) {
                        case 0: {
                            im_compass_needle.setBackgroundResource(R.mipmap.ic_pointer_south);
                            break;
                        }
                        case 1: {
                            im_compass_needle.setBackgroundResource(R.mipmap.ic_pointer_west_south);
                            break;
                        }
                        case 2: {
                            im_compass_needle.setBackgroundResource(R.mipmap.ic_pointer_west);
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                    break;
                }

                case 4: {  // 西北
                    switch (state) {
                        case 0: {
                            im_compass_needle.setBackgroundResource(R.mipmap.ic_pointer_north);
                            break;
                        }
                        case 1: {
                            im_compass_needle.setBackgroundResource(R.mipmap.ic_pointer_west_north);
                            break;
                        }
                        case 2: {
                            im_compass_needle.setBackgroundResource(R.mipmap.ic_pointer_west);
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                    break;
                }

                default: {
                    break;
                }
            }
        }
    }


    /**
     * 注册串口通信的广播
     */
    private void registerBroadCastReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SerialDataCommSerivce.SERIAL_OBD_ACTION);  // OBD数据广播
        intentFilter.addAction(ChappieConstant.GPS_INFO); // GPS信息广播
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    /**
     * 注销串口通信的广播
     */
    private void unRegisterBroadCastReceiver() {
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transport_condition);
        mapView = (MapView) findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);// 此方法必须重写
        initMap();
        initView();
        initData();
        startUpdateMarkerThread();
    }


    /**
     * 更新位置
     */
    private void startUpdateMarkerThread() {
        updateMarkerRunnable = new Runnable() {
            @Override
            public void run() {
                handler.sendEmptyMessage(UPDATE_MARKER);
                SystemClock.sleep(5000);
            }
        };
        updateMarkerThread = new Thread(updateMarkerRunnable);
        updateMarkerThread.start();
    }


    /**
     * 初始化AMap对象
     */
    private void initMap() {
        if (aMap == null) {
            aMap = mapView.getMap();
        }
    }

    @Override
    protected void initView() {
        im_compass_needle = (ImageView) findViewById(R.id.im_compass_needle);
        // 设备是没有地磁传感器无法使用指南针功能，所以做成是指针指向汽车行进的方向
        /*
        compass = new Compass(this);
        compass.arrowView = im_compass_needle;
        */
        asc_map = (AndroidSegmentedControlView) findViewById(R.id.asc_map);
        cb_currentTraffic = (CheckBox) findViewById(R.id.cb_currentTraffic);
        cb_currentTraffic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    ToastUtil.showToast("开启实时路况");
                    aMap.setTrafficEnabled(true);
                } else {
                    ToastUtil.showToast("关闭实时路况");
                    aMap.setTrafficEnabled(false);
                }
            }
        });

        tvCarSpeed = (TextView) findViewById(R.id.tvCarSpeed);
        tv_curon = (TextView) findViewById(R.id.tv_curon);
        tvTransmitterSpeed = (TextView) findViewById(R.id.tvTransmitterSpeed);
        tv_altitude = (TextView) findViewById(R.id.tv_altitude);

    }

    @Override
    protected void initData() {
        gpsInfos = new LinkedList<GPSInfo>();
        isFistLocation = true;
        asc_map.setOnCheckedChangeListener(this);
        initBroadCastReceiver();
        aMap.setOnMarkerClickListener(this);// 设置点击marker事件监听器
        cityElevationDao = CityElevationDaoImpl.getInstance();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
        // compass.start();
        registerBroadCastReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
        // compass.stop();
        unRegisterBroadCastReceiver();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //  compass.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //  compass.stop();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    /**
     * 将汽车的位置画到地图上
     */


    private void addCarMarkerOnMap(GPSInfo gpsInfo) {
        double latitude = gpsInfo.getLatitude();
        double longitude = gpsInfo.getLongitude();
        String carPostition = gpsInfo.getAddress();

        if (marker != null) {
            marker.hideInfoWindow();
            marker.remove();
            aMap.clear();
        } else {
            LatLng latLng = new LatLng(latitude, longitude);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15f);
            aMap.moveCamera(cameraUpdate);
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.draggable(false);
            markerOptions.visible(true);
            markerOptions.title("汽车正在行驶中");
            markerOptions.snippet("车辆位置：" + carPostition);
            markerOptions.position(latLng);
            markerOptions.perspective(true);
            marker = aMap.addMarker(markerOptions);
        }
    }

    private static final String TAG = "TransportCondition";

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int position) {
        Log.e(TAG, "position = " + position);
        switch (position) {
            case 1: {
                aMap.setMapType(AMap.MAP_TYPE_NORMAL);// 矢量地图模式
                Log.e(TAG, "矢量地图模式");
                break;
            }
            case 2: {
                aMap.setMapType(AMap.MAP_TYPE_SATELLITE);// 卫星地图模式
                Log.e(TAG, "卫星地图模式");
                break;
            }
            case 3: {
                aMap.setMapType(AMap.MAP_TYPE_NIGHT);//夜景地图模式
                Log.e(TAG, "夜景地图模式");
            }
            default: {
                break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            default: {
                break;
            }
        }
    }


    @Override
    public boolean onMarkerClick(Marker m) {
        if (m.getId() == marker.getId()) {
            if (!isShow) {
                marker.showInfoWindow();
                isShow = true;
            } else {
                marker.hideInfoWindow();
                isShow = false;
            }
            return true;
        }
        return false;
    }
}

package com.scblhkj.carluncher.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.domain.TPMSSerialBean;
import com.scblhkj.carluncher.service.SerialDataCommSerivce;
import com.scblhkj.carluncher.utils.ToastUtil;
import com.scblhkj.carluncher.utils.XORUtil;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;

/**
 * 轮胎调换界面
 */
public class TyreTanspositionActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {


    private boolean canClick = true;
    private CheckBox cb_lt;
    private CheckBox cb_rt;
    private CheckBox cb_lb;
    private CheckBox cb_rb;
    private RelativeLayout rl_change;
    private ImageView iv_change;
    private TextView tv_change;
    private RelativeLayout rl_loading;
    private ImageView iv_loading;
    private int selectCount; // CheckBox 选中的个数
    /**
     * 图片左转动画
     */
    private Animation rAnim;
    private BroadcastReceiver serialCommReceiver;
    private byte[] bs;
    public static final String CMD = "CMD";
    private static final String TAG = "轮胎调换片段";

    private Timer timer;
    private TimerTask timerTask;
    private int timerFlage = 0;
    private int matchState = 0;  // 配对状态标志位
    private int currentSendTime;

    private void initTimerAndTask() {
        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                // 定时器要执行的任务代码
                timerFlage++;
                if (timerFlage >= 5000) {
                    timerFlage = 0;
                }

                if (matchState == 1) {   // 处于指令发送状态
                    if (timerFlage > currentSendTime) {
                        if (timerFlage - currentSendTime > 8) {  // 8秒超时
                            matchState = 0;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    resetView();
                                }
                            });
                            ToastUtil.showToast("胎压调换失败，发生未知错误");
                        }
                    } else {
                        if (5000 - currentSendTime + timerFlage > 8) {  // 8秒超时
                            matchState = 0;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    resetView();
                                }
                            });
                            ToastUtil.showToast("胎压调换失败，发生未知错误");
                        }
                    }
                }

            }
        };
    }

    private void startTimerTask() {
        initTimerAndTask();
        if (timerTask != null && timer != null) {
            timer.schedule(timerTask, 0, 1000);
        }
    }


    private void stopTimerTask() {
        if (timerTask != null) {
            timerTask.cancel();
            timerTask = null;
        }
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    /**
     * 注册串口通信的广播
     */
    private void registerSerialCommReceiver() {
        if (serialCommReceiver == null) {
            initSerialCommReceiver();
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SerialDataCommSerivce.SERIAL_TPMS_ACTION);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(serialCommReceiver, intentFilter);
    }

    private void initSerialCommReceiver() {
        // 初始化串口通信的接收者
        serialCommReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (SerialDataCommSerivce.SERIAL_TPMS_ACTION.equals(action)) {
                    // 处理代码
                    Bundle bundle = intent.getExtras();
                    TPMSSerialBean bean = (TPMSSerialBean) bundle.getSerializable(SerialDataCommSerivce.SerialHelperControl.SERIAL_TMPA_DATA);
                    if (bean != null) {
                        int[][] datass = bean.getDatasss();
                        compareArray2Array(datass);
                    }
                }
            }
        };
    }

    /**
     * 注销串口通信的广播
     */
    private void unRegisterSerialCommReceiver() {
        if (serialCommReceiver != null) {
            unregisterReceiver(serialCommReceiver);
        }
    }

    /**
     * 轮胎调换数据校验
     *
     * @param datass
     * @return
     */
    private void compareArray2Array(int[][] datass) {
        for (int i = 0; i < datass.length; i++) {
            int[] tempBs = datass[i];
            if ((tempBs[0] == 0x01) && (tempBs[1] == 0xff) && (tempBs[2] == 0x02) && (tempBs[3] == 0x07) && (tempBs[4] == 0x55) && (tempBs[5] == 0xaa) && (tempBs[6] == 0x07) && (tempBs[7] == 0x30) && (tempBs[8] == bs[8]) && (tempBs[9] == bs[9])) {
                if (ChappieCarApplication.isDebug)
                    Log.e(TAG, "轮胎调换成功!");
                ToastUtil.showToast("轮胎调换成功!");
                resetView();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tyre_tansposition);
        ButterKnife.bind(this);
        initView();
        initData();
        startTimerTask();
        resetCheckBox();
    }

    @Override
    protected void onDestroy() {
        stopTimerTask();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        registerSerialCommReceiver();
        super.onResume();
    }

    @Override
    protected void onPause() {
        unRegisterSerialCommReceiver();
        super.onPause();
    }

    @Override
    protected void initView() {
        cb_lt = (CheckBox) findViewById(R.id.cb_lt);
        cb_rt = (CheckBox) findViewById(R.id.cb_rt);
        cb_lb = (CheckBox) findViewById(R.id.cb_lb);
        cb_rb = (CheckBox) findViewById(R.id.cb_rb);
        rl_change = (RelativeLayout) findViewById(R.id.rl_change);
        iv_change = (ImageView) findViewById(R.id.iv_change);
        tv_change = (TextView) findViewById(R.id.tv_change);
        rl_loading = (RelativeLayout) findViewById(R.id.rl_loading);
        iv_loading = (ImageView) findViewById(R.id.iv_loading);
    }

    @Override
    protected void initData() {
        bs = new byte[]{(byte) 0x01, (byte) 0xff, (byte) 0x02, (byte) 0x07, (byte) 0x55, (byte) 0xaa, (byte) 0x07, (byte) 0x03, (byte) 0x00, (byte) 0x00};
        cb_lt.setOnCheckedChangeListener(this);
        cb_rt.setOnCheckedChangeListener(this);
        cb_lb.setOnCheckedChangeListener(this);
        cb_rb.setOnCheckedChangeListener(this);
        iv_change.setOnClickListener(this);
        tv_change.setOnClickListener(this);
        initAnimation();
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {

            case R.id.cb_lt: {  // 0x00 左前轮
                if (b) {
                    cb_lt.setText("配对");
                    selectCount++;
                } else {
                    cb_lt.setText("选择");
                    selectCount--;
                }
                break;
            }
            case R.id.cb_rt: {  // 0x01 右前轮
                if (b) {
                    cb_rt.setText("配对");
                    selectCount++;
                } else {
                    cb_rt.setText("选择");
                    selectCount--;
                }
                break;
            }
            case R.id.cb_lb: {  // 0x10 左后轮
                if (b) {
                    cb_lb.setText("配对");
                    selectCount++;
                } else {
                    cb_lb.setText("选择");
                    selectCount--;
                }
                break;
            }
            case R.id.cb_rb: {   // 0x11 右后轮
                if (b) {
                    cb_rb.setText("配对");
                    selectCount++;
                } else {
                    cb_rb.setText("选择");
                    selectCount--;
                }
                break;
            }
            default: {
                break;
            }
        }
        if (selectCount == 2) {
            rl_change.setVisibility(View.VISIBLE);
        } else {
            rl_change.setVisibility(View.GONE);
        }
    }


    /**
     * 移出点击事件
     */
    private void removeCheckBoxClickble() {
        cb_lt.setClickable(false);
        cb_rt.setClickable(false);
        cb_lb.setClickable(false);
        cb_rb.setClickable(false);
    }

    /**
     * @return 返回的是要发送的数据
     */
    private byte[] storeData() {
        int i = 0;
        if (cb_lt.isChecked()) {
            bs[8 + i] = 0x00;
            i++;
        }
        if (cb_rt.isChecked()) {
            bs[8 + i] = 0x01;
            i++;
        }
        if (cb_lb.isChecked()) {
            bs[8 + i] = 0x10;
            i++;
        }
        if (cb_rb.isChecked()) {
            bs[8 + i] = 0x11;
            i++;
        }
        return bs;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_change:
            case R.id.tv_change: {
                rl_loading.setVisibility(View.VISIBLE);
                startLoadingAnimation();
                rl_change.setVisibility(View.GONE);
                removeCheckBoxClickble();
                //暂时屏蔽，方便调试
                //   canClick = false;
                bs = storeData();
                byte sendData[] = XORUtil.xorByteArray(bs);
                sendData2Port(sendData);
                currentSendTime = timerFlage;
                matchState = 1;   // 发送状态位置1
                break;
            }
        }
    }

    /**
     * 向串口发送数据
     */
    private void sendData2Port(byte[] bs) {
        Intent dataIntent = new Intent();
        dataIntent.setAction(SerialDataCommSerivce.SERIAL_ACTION_RECEIVE);
        dataIntent.addCategory(Intent.CATEGORY_DEFAULT);
        Bundle bundle = new Bundle();
        bundle.putByteArray(ChappieConstant.CMD, bs);
        dataIntent.putExtras(bundle);
        sendBroadcast(dataIntent);
    }

    /**
     * 初始化动画
     */
    private void initAnimation() {
        rAnim = AnimationUtils.loadAnimation(this, R.anim.right_image_rotate);
        LinearInterpolator lir = new LinearInterpolator();
        rAnim.setInterpolator(lir);
    }

    private void startLoadingAnimation() {
        iv_loading.startAnimation(rAnim);
    }

    /**
     * 发送成功或者失败之后重置界面
     */
    private void resetView() {
        // 加载动画要消失
        rl_loading.setVisibility(View.GONE);
        // 加载的旋转动画取消
        iv_loading.clearAnimation();
        // 调换按钮要消失
        rl_change.setVisibility(View.GONE);
        // 选择框重置
        resetCheckBox();
        // 选择按钮可点击
        setCheckBoxClickble();
        // 返回上层界面的按钮可点击
        canClick = true;
        // 发送位置0
        matchState = 0;
    }

    private void resetCheckBox() {
        cb_lt.setChecked(false);
        cb_rt.setChecked(false);
        cb_lb.setChecked(false);
        cb_rb.setChecked(false);
        selectCount = 0;
    }

    /**
     * 添加点击事件
     */
    private void setCheckBoxClickble() {
        cb_lt.setClickable(true);
        cb_rt.setClickable(true);
        cb_lb.setClickable(true);
        cb_rb.setClickable(true);
    }
}

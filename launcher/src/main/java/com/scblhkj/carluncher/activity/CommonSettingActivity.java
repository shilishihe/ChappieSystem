package com.scblhkj.carluncher.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.domain.CPLSetBean;
import com.scblhkj.carluncher.domain.ResultBean;
import com.scblhkj.carluncher.utils.GsonUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * 后视镜通用设置
 */
public class CommonSettingActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    @Bind(R.id.iv_left)
    ImageView ivLeft;     // 返回按钮
    @Bind(R.id.rl_safe_account)
    RelativeLayout rlSafeAccount;  // 账号与安全
    @Bind(R.id.rl_news_alarm)
    RelativeLayout rlNewsAlarm;   // 新消息通知
    @Bind(R.id.rl_privacy_lock)
    RelativeLayout rlPrivacyLock;  // 隐私安全设置
    @Bind(R.id.rl_general_purpose)
    RelativeLayout rlGeneralPurpose;   // 通用
    @Bind(R.id.rl_help_feedback)
    RelativeLayout rlHelpFeedback;    // 帮助与反馈
    @Bind(R.id.rl_about_chappie)
    RelativeLayout rlAboutChappie;   // 关于查派
    @Bind(R.id.rl_logout)
    RelativeLayout rlLogout;      // 退出登录
    @Bind(R.id.cbDriveReport)
    CheckBox cbDriveReport;    // 行程报告
    @Bind(R.id.cbRemoteControl)
    CheckBox cbRemoteControl;   // 远程控制
    @Bind(R.id.cbTravelPath)
    CheckBox cbTravelPath;     // 行驶轨迹
    @Bind(R.id.rl_traffic_statistics)
    RelativeLayout rlTrafficStatistics;   // 流量轨迹
    @Bind(R.id.rl_storage_space)
    RelativeLayout rlStorageSpace;   // 存储空间
    private static final String TAG = "CommonSettingActivity";
    private CPLSetBean cplSetBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_setting);
        ButterKnife.bind(this);
        cplSetBean = (CPLSetBean) aCache.getAsObject(ChappieConstant.CPSBK);
        initView();
        initData();
    }

    @Override
    protected void initView() {
        // 首先加载本地的缓存的数据
        cbRemoteControl.setChecked(cplSetBean.isSetRemoteControl() ? true : false);
        cbTravelPath.setChecked(cplSetBean.isSetRravelingTrack() ? true : false);
        // 再加载网络请求的数据
        String url = "http://" + ChappieConstant.HTTP_HOST + "/app/RMSet/rest/IEMI/" + ChappieCarApplication.DEVICE_IMEI;
        OkHttpUtils.get().url(url).build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e) {
                Log.e(TAG, "请求出现异常");
            }

            @Override
            public void onResponse(String s) {
                if (!TextUtils.isEmpty(s)) {
                    Log.e(TAG, "****" + s);
                    ReturnBean bean = GsonUtil.jsonToBean(s, ReturnBean.class);
                    if (bean.status.equals("1")) {
                        Log.e(TAG, "请求结果正常");
                        if (bean.driving_track.equals("1")) {
                            cbTravelPath.setChecked(true);
                            cplSetBean.setSetRravelingTrack(true);
                        } else if (bean.driving_track.equals("0")) {
                            cbTravelPath.setChecked(false);
                            cplSetBean.setSetRravelingTrack(false);
                        }
                        if (bean.far_control.equals("1")) {
                            cbRemoteControl.setChecked(true);
                            cplSetBean.setSetRemoteControl(true);
                        } else if (bean.far_control.equals("0")) {
                            cplSetBean.setSetRemoteControl(false);
                            cbRemoteControl.setChecked(false);
                        }
                    }
                }
            }
        });
    }

    private class ReturnBean {
        public String status;
        public String msg;
        public String far_control;
        public String driving_track;
    }


    @Override
    protected void initData() {
        rlPrivacyLock.setOnClickListener(this);
        ivLeft.setOnClickListener(this);
        rlTrafficStatistics.setOnClickListener(this);
        rlStorageSpace.setOnClickListener(this);
        cbRemoteControl.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_left: {
                finish();
                break;
            }
            case R.id.rl_privacy_lock: {
                if (TextUtils.isEmpty(cplSetBean.getPrivacyPassword())) {  // 没有设置过密码
                    intent2Activity(SetPrivacyActivity.class);  // 设置密码
                } else {   // 验证密码界面
                    intent2Activity(VerifyPrivacyPassActivity.class);
                }
                break;
            }
            case R.id.rl_storage_space: {
                intent2Activity(StorageCapacityActivity.class);
                break;
            }
            default: {
                break;
            }
        }
    }

    /**
     * @param buttonView
     * @param isChecked
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cbRemoteControl: {
                cplSetBean.setSetRemoteControl(isChecked);
                break;
            }
            case R.id.cbTravelPath: {
                cplSetBean.setSetRravelingTrack(isChecked);
                break;
            }
            default: {
                break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        // http://localhost/app/RMSet/rest
        String url = "http://" + ChappieConstant.HTTP_HOST + "/app/RMSet/rest";
        OkHttpUtils.post().url(url)
                .addParams("IEMI", ChappieCarApplication.DEVICE_IMEI)
                .addParams("far_control", cplSetBean.isSetRemoteControl() ? "1" : "0")
                .addParams("driving_track", cplSetBean.isSetRravelingTrack() ? "1" : "0")
                .build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e) {
                Log.e(TAG, "提交设置出现异常" + e.toString());
            }

            @Override
            public void onResponse(String s) {
                if (!TextUtils.isEmpty(s)) {
                    ResultBean bean = GsonUtil.jsonToBean(s, ResultBean.class);
                    if (bean.status.equals("1")) {
                        Log.e(TAG, bean.msg);
                        aCache.put(ChappieConstant.CPSBK, cplSetBean);
                    }
                }
            }
        });
        super.onDestroy();
    }
}

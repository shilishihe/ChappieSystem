package com.scblhkj.carluncher.activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.scblhkj.carluncher.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 每段行程结束后弹出的Acitivity
 */
public class TripReportActivity extends BaseActivity {

    @Bind(R.id.iv_left)
    ImageView ivLeft;
    @Bind(R.id.asc_title)
    TextView ascTitle;
    @Bind(R.id.startTime)
    TextView startTime;
    @Bind(R.id.startLocation)
    TextView startLocation;
    @Bind(R.id.tvHotCarTime)
    TextView tvHotCarTime;
    @Bind(R.id.tvREDDCast)
    TextView tvREDDCast;
    @Bind(R.id.endTime)
    TextView endTime;
    @Bind(R.id.endLocation)
    TextView endLocation;
    @Bind(R.id.tvAverageSpeedCase)
    TextView tvAverageSpeedCase;
    @Bind(R.id.tvTotalScoreCast)
    TextView tvTotalScoreCast;
    @Bind(R.id.tvCastTime)
    TextView tvCastTime;
    @Bind(R.id.tvTopSpeedCast)
    TextView tvTopSpeedCast;
    @Bind(R.id.tvTotalWordCast)
    TextView tvTotalWordCast;
    @Bind(R.id.tvCastOil)
    TextView tvCastOil;
    @Bind(R.id.tvSharpSpeedCast)
    TextView tvSharpSpeedCast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_report);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }
}

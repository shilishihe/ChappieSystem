package com.scblhkj.carluncher.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.rey.material.widget.Switch;
import com.scblhkj.carluncher.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * OBD设置界面
 */
public class OBDSettingActivity extends BaseActivity implements View.OnClickListener, Switch.OnCheckedChangeListener {

    @Bind(R.id.iv_left)
    ImageView ivLeft;
    @Bind(R.id.asc_title)
    TextView ascTitle;
    @Bind(R.id.sw_trip_report)
    Switch swTripReport;
    @Bind(R.id.sw_remote_control)
    Switch swRemoteControl;
    @Bind(R.id.sw_traveling_track)
    Switch swTravelingTrack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obdsetting);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        ivLeft.setOnClickListener(this);
        swTripReport.setOnCheckedChangeListener(this);
        swRemoteControl.setOnCheckedChangeListener(this);
        swTravelingTrack.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_left: {
                finish();
                break;
            }
            default: {
                break;
            }
        }
    }

    @Override
    public void onCheckedChanged(Switch view, boolean checked) {
        switch (view.getId()) {
            case R.id.sw_trip_report: {  // 行程报告
                if (checked) {  // 请求接口设置开关

                } else {

                }
                break;
            }
            case R.id.sw_remote_control: {   // 远程控制
                if (checked) {   // 请求接口设置开关

                } else {

                }
                break;
            }
            case R.id.sw_traveling_track: {  // 行程轨迹
                if (checked) {   // 请求接口设置开关

                } else {

                }
                break;
            }
            default: {
                break;
            }
        }
    }
}

package com.scblhkj.carluncher.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.adapter.SettingAdapter;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.service.ChappieWifiHotService;
import com.scblhkj.carluncher.utils.ConnectivityUtil;
import com.scblhkj.carluncher.utils.NetworkUtil;
import com.scblhkj.carluncher.utils.StartAppUtil;
import com.scblhkj.carluncher.utils.ToastUtil;
import com.scblhkj.carluncher.widget.MyGridView;
import com.txznet.sdk.TXZConfigManager;
import com.txznet.sdk.TXZPowerManager;
import com.txznet.sdk.TXZService;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**设置界面
 * Created by blh on 2016-07-18.
 */
public class SettingActivity extends BaseActivity {
    private SeekBar sb_pres_light,sb_pres_voice;
    private AudioManager audioManager;
    private int maxVolume;
    private int currentVolume;
    private MyServiceConnection conn;
    private ChappieWifiHotService.MyBinder myBinder;
    private LinearLayout ll_settingwifi,ll_settingnet,ll_settingbt,ll_voicesetting;
    private ImageView iv_settingwifi,iv_settingnet,iv_settingbt,iv_voicesetting;
    private BluetoothAdapter bluetoothAdapter;
    private ConnectivityManager connManager;
    ChappieCarApplication application;
    private LinearLayout ll_settingedog;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    private ImageView iv_edog;
    private ConnectivityManager mConnectivityManager;
    // 移动数据设置改变系统发送的广播
    private static final String NETWORK_CHANGE = "android.intent.action.ANY_DATA_STATE";
    private TestChange mTestChange;
    private IntentFilter mIntentFilter;
    ConnectivityUtil connectivityUtil;
    private LinearLayout ll_adas;
    private static  ImageView iv_adasstate;
    AdasSettingReceiver adasSettingReceiver;
     private   int adasState;
    int cur;
    private boolean isAdas;
    private int normal;
    private BroadcastReceiver netSateBroadCastReceiver;

    @Override
    protected void onDestroy()
    {
        // TODO Auto-generated method stub
       //// unRegisterAdasSettingReceiver();
        unRegisterNetStateBroad();
        if(adasSettingReceiver!=null){
            unregisterReceiver(adasSettingReceiver);
        }
        super.onDestroy();
        // 解除广播接收器
      //  unregisterReceiver(mTestChange);
    }

    @Override
    protected void onPause() {
       // unRegisterAdasSettingReceiver();
        super.onPause();
    }

    @Override
    protected void onResume()
    {    //registetAdasSettingReceiver();
        // TODO Auto-generated method stub
        super.onResume();
        // 注册广播接收器
//        registerReceiver(mTestChange, mIntentFilter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        sp=getSharedPreferences("edogstate", Activity.MODE_PRIVATE);
        editor=sp.edit();
        initView();
        initData();
    }

    @Override
    protected void initView() {
        application= (ChappieCarApplication) getApplication();
        connectivityUtil=new ConnectivityUtil(getApplicationContext());
        mConnectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        mTestChange = new TestChange();
        mIntentFilter = new IntentFilter();
       // 添加广播接收器过滤的广播
        mIntentFilter.addAction("android.intent.action.ANY_DATA_STATE");
        startWifiAPService();
        connManager= (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        audioManager=(AudioManager)getSystemService(Context.AUDIO_SERVICE);
        maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);  //获取系统最大音量
        sb_pres_light=(SeekBar)findViewById(R.id.sb_pres_light);
        sb_pres_voice=(SeekBar)findViewById(R.id.sb_pres_voice);
        ll_adas=(LinearLayout)findViewById(R.id.ll_adas);
        iv_adasstate=(ImageView)findViewById(R.id.iv_adasstate);
        ll_settingwifi=(LinearLayout)findViewById(R.id.ll_settingwifi);
        ll_settingbt=(LinearLayout)findViewById(R.id.ll_settingbt);
        ll_settingnet=(LinearLayout)findViewById(R.id.ll_settingnet);
        ll_voicesetting=(LinearLayout)findViewById(R.id.ll_voicesetting);
        iv_settingwifi=(ImageView)findViewById(R.id.iv_settingwifi);
        iv_settingnet=(ImageView)findViewById(R.id.iv_settingnet);
        iv_settingbt=(ImageView)findViewById(R.id.iv_settingbt);
        iv_voicesetting=(ImageView)findViewById(R.id.iv_voicesetting);
        ll_settingedog=(LinearLayout)findViewById(R.id.ll_settingedog);
        iv_edog=(ImageView)findViewById(R.id.iv_edog);
        sb_pres_voice.setMax(maxVolume); //拖动条最高值与系统最大声匹配
        currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);  //获取当前值
        sb_pres_voice.setProgress(currentVolume);
        //初始化电子狗图标
        if(isServiceWork(getApplicationContext(), "com.test.JniTest.EdogService")==true){
            Log.e("info","dakaisetingactivity,edog1::"+isServiceWork(getApplicationContext(), "com.test.JniTest.EdogService"));
            iv_edog.setImageResource(R.drawable.ic_settingdianzigouon);
        }else if(isServiceWork(getApplicationContext(), "com.test.JniTest.EdogService")==false){
            Log.e("info","dakaisetingactivity,edog1::"+isServiceWork(getApplicationContext(), "com.test.JniTest.EdogService"));
            iv_edog.setImageResource(R.drawable.ic_settingdianzigouoff);
        }
        //设置蓝牙
        //初始化蓝牙适配器，根据蓝牙状态来设置图标
        bluetoothAdapter=BluetoothAdapter.getDefaultAdapter();
        boolean BluetoothState=isBlueTooth(this.getApplicationContext());
        if(BluetoothState){
            iv_settingbt.setImageResource(R.drawable.ic_settingbluetoothon);
        }else {
            iv_settingbt.setImageResource(R.drawable.ic_settingbtoff);
        }
       // refreshButton();
         //设置移动数据网络，根据网络状态来设置图标
       if(NetworkUtil.isNetworkConnected(this)){
           iv_settingnet.setImageResource(R.drawable.ic_settingneton);
       }else {
           iv_settingnet.setImageResource(R.drawable.ic_settingnetoff);
       }
        //设置adas状态
        if(adasState==1){
            iv_adasstate.setImageResource(R.drawable.ic_settingadason);
        }else if(adasState==2){
            iv_adasstate.setImageResource(R.drawable.ic_settingadasoff);
        }
        initNetStateBroadCastReceiver();
        registerNetStateBroad();
    }

    private void startWifiAPService() {
        Intent intent = new Intent(this, ChappieWifiHotService.class);
       /* intent.setAction("com.scblhkj.carluncher.service.ChappieWifiHotService");
        intent.setPackage("com.scblhkj.carluncher");*/
        startService(intent);
        conn=new MyServiceConnection();
        bindService(intent,conn, BIND_AUTO_CREATE);
        Log.e("info","绑定wifi热点服务成功!");
    }
   private void registerNetStateBroad(){
       IntentFilter netStateFilter=new IntentFilter();
       netStateFilter.addAction(ChappieConstant.NET_NAME);
       netStateFilter.addCategory(Intent.CATEGORY_DEFAULT);
       registerReceiver(netSateBroadCastReceiver,netStateFilter);

   }
    private void unRegisterNetStateBroad(){
        if(netSateBroadCastReceiver!=null){
            unregisterReceiver(netSateBroadCastReceiver);
        }
    }
    /***
     * 注册移动网络状态监听广播
     */
   private void initNetStateBroadCastReceiver(){
       netSateBroadCastReceiver= new BroadcastReceiver() {
           @Override
           public void onReceive(Context context, Intent intent) {
                String action=intent.getAction();
               if(action.equals(ChappieConstant.NET_NAME)){
                   if(intent.getStringExtra("netState").equals("mobile")){
                       iv_settingnet.setImageResource(R.drawable.ic_settingneton);
                   }else {
                       iv_settingnet.setImageResource(R.drawable.ic_settingnetoff);
                   }

               }
           }
       };
   }
    /***
     * 通过myBinder对象调用热点服务里的方法
     */
    public class MyServiceConnection implements ServiceConnection{

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            //得到MyService.MyBinder对象，我们通过这个对象来操作服务中的方法
            myBinder = (ChappieWifiHotService.MyBinder) service;
            boolean is= myBinder.isWifiap();
            Log.e("info","is>>>>>>"+is);
            if(myBinder.isWifiap()==true){
                iv_settingwifi.setImageResource(R.drawable.ic_settingwifion);
            }else if(myBinder.isWifiap()==false){
                iv_settingwifi.setImageResource(R.drawable.ic_settingwifioff);
            };
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    }

    /****
     * 判断蓝牙是否打开
     * @param context
     * @return
     */
    public boolean isBlueTooth(Context context){
        if(!bluetoothAdapter.isEnabled()){
            return  false;
        }else {
            return true;
        }
    }
    public  class AdasSettingReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {

            String action=intent.getAction();
            if(action.equals("com.chappie.ADASSETTING")){
                Bundle bundle=intent.getExtras();
                isAdas=bundle.getBoolean("isADASRunning");
               // Log.e("info","从行车记录仪接收到的---------isADASRunning:"+isAdas);
                 if(isAdas){
                     adasState=0;
                 }else if(!isAdas){
                     adasState=1;
                 }


            }
        }
    }


    private void registetAdasSettingReceiver(){
        adasSettingReceiver=new AdasSettingReceiver();
        IntentFilter filter=new IntentFilter();
        filter.addAction("com.chappie.ADASSETTING");
        registerReceiver(adasSettingReceiver,filter);
    }

    private void unRegisterAdasSettingReceiver(){
        if(adasSettingReceiver!=null){
            unregisterReceiver(adasSettingReceiver);
        }
    }
    /**
     * 判断某个服务是否正在运行的方法
     *
     * @param mContext
     * @param serviceName
     *            是包名+服务的类名（例如：net.loonggg.testbackstage.TestService）
     * @return true代表正在运行，false代表服务没有正在运行
     */
    public boolean isServiceWork(Context mContext, String serviceName) {
        boolean isWork = false;
        ActivityManager myAM = (ActivityManager) mContext
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> myList = myAM.getRunningServices(40);
        if (myList.size() <= 0) {
            return false;
        }
        for (int i = 0; i < myList.size(); i++) {
            String mName = myList.get(i).service.getClassName().toString();
            if (mName.equals(serviceName)) {
                isWork = true;
                break;
            }
        }
        return isWork;
    }
    @Override
    protected void initData() {
        sb_pres_light.setMax(255);
        //取得当前亮度
         normal = Settings.System.getInt(getContentResolver(),
                Settings.System.SCREEN_BRIGHTNESS, 255);
        sb_pres_light.setProgress(normal);
        IntentFilter filter=new IntentFilter();
        filter.addAction("com.chappie.ADASSETTING");
        adasSettingReceiver=new AdasSettingReceiver();
        registerReceiver(adasSettingReceiver,filter);
       // registetAdasSettingReceiver();//注册ADAS状态类广播
          ll_adas.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                 if(adasState==0){//当ADAS正在运行时
                   Intent intent = new Intent();
                   intent.setAction("com.chappie.CLOSEADAS");
                   sendBroadcast(intent);
                   iv_adasstate.setImageResource(R.drawable.ic_settingadasoff);
                   ToastUtil.showToast("关闭了ADAS");
                     SystemClock.sleep(500);
                   }else if(adasState==1){//当ADAS没有运行时
                   Intent intent=new Intent();
                   intent.setAction("com.chappie.OPENADAS");
                   sendBroadcast(intent);
                   iv_adasstate.setImageResource(R.drawable.ic_settingadason);
                   ToastUtil.showToast("打开了ADAS");
                     SystemClock.sleep(500);
                   }


              }
          });

          //电子狗图标点击事件
        ll_settingedog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isServiceWork(getApplicationContext(),"com.test.JniTest.EdogService")==true){//表示电子狗在后台运行
                    Log.e("info","点击按钮时:"+isServiceWork(getApplicationContext(),"com.test.JniTest.EdogService"));
                    iv_edog.setImageResource(R.drawable.ic_settingdianzigouoff);
                    StartAppUtil.doStartApplicationWithPackageName(SettingActivity.this, "com.test.JniTest");

                } else if(isServiceWork(getApplicationContext(),"com.test.JniTest.EdogService")==false){//表示电子狗不在后台运行
                    Log.e("info","点击按钮时:"+isServiceWork(getApplicationContext(),"com.test.JniTest.EdogService"));
                    iv_edog.setImageResource(R.drawable.ic_settingdianzigouon);
                    StartAppUtil.doStartApplicationWithPackageName(SettingActivity.this, "com.test.JniTest");

                }

            }
        });
        //语音图标点击事件
        ll_voicesetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("info","isTxzWakeUp>>>"+application.isTxzWakeUp);
                if(application.isTxzWakeUp==true){

                   TXZPowerManager.getInstance().releaseTXZ();
                    iv_voicesetting.setImageResource(R.drawable.ic_settingmicoff);
                    application.isTxzWakeUp=false;
                    ToastUtil.showToast("关闭了语音助手");
                    Log.e("info","isTxzWakeUp====="+application.isTxzWakeUp);
                }else if(application.isTxzWakeUp==false){

                    TXZPowerManager.getInstance().reinitTXZ();
                    TXZConfigManager.getInstance().showFloatTool(TXZConfigManager.FloatToolType.FLOAT_NONE);
                    if(TXZConfigManager.getInstance().isInitedSuccess()){
                        iv_voicesetting.setImageResource(R.drawable.ic_settingmicon);
                        application.isTxzWakeUp=true;
                        ToastUtil.showToast("开启了语音助手");
                        Log.e("info","isTxzWakeUp===="+application.isTxzWakeUp);
                    }else if(!TXZConfigManager.getInstance().isInitedSuccess()){
                        iv_voicesetting.setImageResource(R.drawable.ic_settingmicoff);
                        application.isTxzWakeUp=false;
                        ToastUtil.showToast("语音初始化失败,请检查网络连接!");
                        Log.e("info", "isTxzWakeUp====" + application.isTxzWakeUp);
                    }

                }

            }
        });
        //蓝牙图标点击事件
        ll_settingbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!bluetoothAdapter.isEnabled()){//如果蓝牙没有开启
                    Intent discoverableIntent = new Intent(
                            BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                    discoverableIntent.putExtra(
                            BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
                    startActivity(discoverableIntent);
                    iv_settingbt.setImageResource(R.drawable.ic_settingbluetoothon);
                }else{
                    bluetoothAdapter.disable();
                    iv_settingbt.setImageResource(R.drawable.ic_settingbtoff);
                    ToastUtil.showToast("蓝牙关闭了!");
                }
            }
        });
        //网络图标点击事件
        ll_settingnet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Log.e("info","isMobileConnect>>>>>"+NetworkUtil.isNetworkConnected(SettingActivity.this));
                 if(NetworkUtil.isNetworkConnected(SettingActivity.this)==true){
                  setMobileDataStatus(false);
                 //connectivityUtil.setMobileDataEnabled(false);
                 iv_settingnet.setImageResource(R.drawable.ic_settingnetoff);
                 ToastUtil.showToast("关闭了移动数据网络连接");
                 }else if(NetworkUtil.isNetworkConnected(SettingActivity.this)==false){
                     //connectivityUtil.setMobileDataEnabled(true);
                     setMobileDataStatus(true);
                if(NetworkUtil.isNetworkConnected(SettingActivity.this)==true){
                    iv_settingnet.setImageResource(R.drawable.ic_settingneton);
                    ToastUtil.showToast("打开了移动数据网络连接");
                   // dialog.dismiss();
                }else if(NetworkUtil.isNetworkConnected(SettingActivity.this)==false){
                    //iv_settingnet.setImageResource(R.drawable.ic_settingnetoff);
                    ToastUtil.showToast("正在连接网络...");
                   // dialog.dismiss();
                }

                 }

            }
        });
        //wifi热度点击事件
        ll_settingwifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myBinder.isWifiap() == true) {//如果wifi热点开启的话
                    myBinder.closeWifiAP();
                    iv_settingwifi.setImageResource(R.drawable.ic_settingwifioff);
                    ToastUtil.showToast("关闭了WIFI热点");
                    Intent intent1 = new Intent();//关闭热点后发送广播给主界面状态栏
                    intent1.setAction("com.wifihotstate");
                    intent1.putExtra("wifihotstate", "0");
                    SettingActivity.this.sendBroadcast(intent1);

                } else if (myBinder.isWifiap() == false) {
                    iv_settingwifi.setImageResource(R.drawable.ic_settingwifion);
                    myBinder.openWifiAP();
                    ToastUtil.showToast("开启了WIFI热点");
                    Intent intent2 = new Intent();
                    intent2.setAction("com.wifihotstate");
                    intent2.putExtra("wifihotstate", "1");
                    SettingActivity.this.sendBroadcast(intent2);
                }
            }
        });
        /***
         * 调整屏幕亮度
         */

        sb_pres_light.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
              //  cur = seekBar.getProgress();
               // SettingActivity.this.setScreenBrightness(cur / 100);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                int tmpInt = seekBar.getProgress();
                //当进度小于40时，设置成40，防止太黑看不见的后果。
                if (tmpInt < 40) {
                    tmpInt = 40;
                }
                //根据当前进度改变亮度
                Settings.System.putInt(getContentResolver(),
                        Settings.System.SCREEN_BRIGHTNESS, tmpInt);
                tmpInt = Settings.System.getInt(getContentResolver(),
                        Settings.System.SCREEN_BRIGHTNESS, -1);
                WindowManager.LayoutParams wl = getWindow()
                        .getAttributes();
                float tmpFloat = (float) tmpInt / 255;
                if (tmpFloat > 0 && tmpFloat <= 1) {
                    wl.screenBrightness = tmpFloat;
                }
                getWindow().setAttributes(wl);
                /***
                 *   Intent intent=new Intent();
                 intent.setAction(ChappieConstant.SCREEN_LIGHT);
                 Bundle bundle=new Bundle();
                 bundle.putInt("screenlight", cur);
                 intent.putExtras(bundle);
                 intent.addCategory(Intent.CATEGORY_DEFAULT);
                 sendBroadcast(intent);
                 Log.e("info","亮度调节完毕，已发送广播!"+"亮度值:"+cur);
                 */

            }
        });
        /***
         * 调整音量
         */
      sb_pres_voice.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
          @Override
          public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
              audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
              currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);  //获取当前值
              sb_pres_voice.setProgress(currentVolume);
              // mVolume.setText(currentVolume*100/maxVolume + " %");
          }

          @Override
          public void onStartTrackingTouch(SeekBar seekBar) {

          }

          @Override
          public void onStopTrackingTouch(SeekBar seekBar) {

          }
      });
    }

    /***
     * 设置屏幕亮度
     * @param num
     */
    private void setScreenBrightness(float  num){
        WindowManager.LayoutParams layoutParams=super.getWindow().getAttributes();
        layoutParams.screenBrightness=num;//设置屏幕的亮度
        super.getWindow().setAttributes(layoutParams);

    }

    /**
     * 设置手机的移动数据
     */
    public static void setMobileData(Context pContext, boolean pBoolean) {

        try {

            ConnectivityManager mConnectivityManager = (ConnectivityManager) pContext.getSystemService(Context.CONNECTIVITY_SERVICE);

            Class ownerClass = mConnectivityManager.getClass();

            Class[] argsClass = new Class[1];
            argsClass[0] = boolean.class;

            Method method = ownerClass.getMethod("setMobileDataEnabled", argsClass);

            method.invoke(mConnectivityManager, pBoolean);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println("移动数据设置错误: " + e.toString());
        }
    }


    // 通过反射实现开启或关闭移动数据
    private void setMobileDataStatus(boolean enabled)
    {
        try
        {
            Class<?> conMgrClass = Class.forName(mConnectivityManager.getClass().getName());
            //得到ConnectivityManager类的成员变量mService（ConnectivityService类型）
            Field iConMgrField = conMgrClass.getDeclaredField("mService");
            iConMgrField.setAccessible(true);
            //mService成员初始化
            Object iConMgr = iConMgrField.get(mConnectivityManager);
            //得到mService对应的Class对象
            Class<?> iConMgrClass = Class.forName(iConMgr.getClass().getName());
            /*得到mService的setMobileDataEnabled(该方法在android源码的ConnectivityService类中实现)，
             * 该方法的参数为布尔型，所以第二个参数为Boolean.TYPE
             */
            Method setMobileDataEnabledMethod = iConMgrClass.getDeclaredMethod(
                    "setMobileDataEnabled", Boolean.TYPE);
            setMobileDataEnabledMethod.setAccessible(true);
            /*调用ConnectivityManager的setMobileDataEnabled方法（方法是隐藏的），
             * 实际上该方法的实现是在ConnectivityService(系统服务实现类)中的
             */
            setMobileDataEnabledMethod.invoke(iConMgr, enabled);
        } catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        } catch (NoSuchFieldException e)
        {
            e.printStackTrace();
        } catch (SecurityException e)
        {
            e.printStackTrace();
        } catch (NoSuchMethodException e)
        {
            e.printStackTrace();
        } catch (IllegalArgumentException e)
        {
            e.printStackTrace();
        } catch (IllegalAccessException e)
        {
            e.printStackTrace();
        } catch (InvocationTargetException e)
        {
            e.printStackTrace();
        }
    }

    private class TestChange extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent)
        {
            // TODO Auto-generated method stub
            String action = intent.getAction();

            if (NETWORK_CHANGE.equals(action))
            {
                if(NetworkUtil.isNetworkConnected(context)==true){
                    iv_settingnet.setImageResource(R.drawable.ic_settingneton);
                    ToastUtil.showToast("移动数据开启");
                }else if(NetworkUtil.isNetworkConnected(context)==false){
                    iv_settingnet.setImageResource(R.drawable.ic_settingnetoff);
                    ToastUtil.showToast("移动数据连接异常");
                }
            }
        }

    }
}

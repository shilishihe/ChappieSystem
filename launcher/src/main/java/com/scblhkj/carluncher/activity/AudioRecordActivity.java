package com.scblhkj.carluncher.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.common.RecordImpl;
import com.scblhkj.carluncher.utils.AudioRecord;
import com.scblhkj.carluncher.utils.SDCardScannerUtil;
import com.scblhkj.carluncher.utils.ToastUtil;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by blh on 2016-11-16.
 */

public class AudioRecordActivity extends BaseActivity implements RecordImpl {
    private MediaRecorder recorder;
    private String fileName;
    private String fileFolder="/storage/sdcard1/audiorecord";
    private boolean isAudioRecord=false;
    private LinearLayout ll_voiceRecording,ll_voiceCancel,ll_voiceSend;
    private static final int MIN_RECORD_TIME=1;//最少录制时间
    private ImageView iv_recording;
    private int recordState = 0; // 录音状态
    private float recodeTime = 0.0f; // 录音时长，如果录音时间太短则录音失败
    private double voiceValue = 0.0; // 录音的音量值
    private boolean isCanceled = false; // 是否取消录音
   // private RecordListener listener;
    //private Thread mRecordThread;
    private Handler recordHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            int what=msg.what;
            if(what>7){
                what=7;
            }
            iv_recording.setImageDrawable(micImages[what]);
            /****
             *  switch (msg.what){
             case 0x01:
             showVoiceAnimation();
             break;
             }
             */

        }
    };
    private int roomNum; //语音对讲的房号
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private Drawable[] micImages;
    private int BASE = 600;
    private int SPACE = 200;// 间隔取样时间
    /***
     * 根据音量大小显示动画
     */
    private void showVoiceAnimation() {
        Log.e("info", "voiceValue:" + voiceValue);
        if(voiceValue<1000){
            iv_recording.setImageResource(R.drawable.record_animate_01);
        }else if(voiceValue>1000&&voiceValue<3000){
            iv_recording.setImageResource(R.drawable.record_animate_02);
        }else if(voiceValue>3000&&voiceValue<5000){
            iv_recording.setImageResource(R.drawable.record_animate_03);
        }else if(voiceValue>5000&&voiceValue<7000){
            iv_recording.setImageResource(R.drawable.record_animate_04);
        }else if(voiceValue>7000&&voiceValue<9000){
            iv_recording.setImageResource(R.drawable.record_animate_05);
        }else if(voiceValue>9000&&voiceValue<11000){
            iv_recording.setImageResource(R.drawable.record_animate_06);
        }else if(voiceValue>11000){
            iv_recording.setImageResource(R.drawable.record_animate_07);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audiorecord);
        sp=getSharedPreferences("deskstate", Activity.MODE_PRIVATE);
        editor=sp.edit();
        initView();
        initData();
    }

    @Override
    protected void initView() {
        ll_voiceSend=(LinearLayout)findViewById(R.id.ll_voiceSend);
        ll_voiceCancel=(LinearLayout)findViewById(R.id.ll_voiceCancel);
        ll_voiceRecording=(LinearLayout)findViewById(R.id.ll_voiceRecording);
        iv_recording=(ImageView)findViewById(R.id.record_voice);
        // 动画资源文件,用于录制语音时
        micImages = new Drawable[] {
                getResources().getDrawable(R.drawable.record_animate_01),
                getResources().getDrawable(R.drawable.record_animate_02),
                getResources().getDrawable(R.drawable.record_animate_03),
                getResources().getDrawable(R.drawable.record_animate_04),
                getResources().getDrawable(R.drawable.record_animate_05),
                getResources().getDrawable(R.drawable.record_animate_06),
                getResources().getDrawable(R.drawable.record_animate_07),
                };
            readyRecord();
            startRecord();
            updateMicStatus();
           // callRecorderTimeThread();
            ToastUtil.showToast("开始录音...");

    }

    @Override
    protected void initData() {

        /***
         * 录音取消点击事件
         */
        ll_voiceCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isAudioRecord){
                    isAudioRecord=false;
                    stopRecord();
                   // mRecordThread.interrupt();
                   // voiceValue = 0.0;
                    deleteOldRecordFile();
                    AudioRecordActivity.this.finish();
                    ToastUtil.showToast("您已取消发送!");
                }else {
                    AudioRecordActivity.this.finish();
                    ToastUtil.showToast("您已退出!");
                }

            }
        });
        /***
         * 录音发送点击事件
         */
        ll_voiceSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRecord();
//                mRecordThread.interrupt();
                //voiceValue = 0.0;
                isAudioRecord = false;
               // if (recodeTime < MIN_RECORD_TIME) { //如果录音时间比最短时间还小的话就不发送
                  //  deleteOldRecordFile();
                   // ToastUtil.showToast("时间太短,录音失败");
                   // AudioRecordActivity.this.finish();
             //   } else {
                   // if (listener != null) {
                       // listener.recordEnd();
                 //   }
                    String path = getVoicePath();
                    roomNum = sp.getInt("roomNum", roomNum);
                    Intent intent = new Intent();
                    intent.setAction(ChappieConstant.SEND_VOICEFILE_PATH);
                    intent.putExtra("voiceFilePath", path);
                    intent.putExtra("roomNum", roomNum);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    sendBroadcast(intent);
                    Log.e("info", "audioRecordActivity发送的录音文件的路径:" + path + ",房号：" + roomNum);
                    ToastUtil.showToast("正在发送录音文件...");
                    AudioRecordActivity.this.finish();
               // }
            }
        });
    }

    /***
     * 启动录音计时线程
     *  private void callRecorderTimeThread() {
     mRecordThread=new Thread(recordThread);
     mRecordThread.start();
     }
     */


    private  Runnable recordThread=new Runnable() {
        @Override
        public void run() {
            recodeTime=0.0f;
                try{
                    while (isAudioRecord==true){
                        Thread.sleep(100);
                        recodeTime+=0.1;
                        voiceValue=getAmplitude();
                        recordHandler.sendEmptyMessage(0x01);
                    }

                }catch (InterruptedException e){
                    e.printStackTrace();
                }



        }
    };

    @Override
    protected void onDestroy() {
        stopRecord();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        stopRecord();
        super.onPause();
    }

    @Override
    public void readyRecord() {
        if(SDCardScannerUtil.isSecondSDcardMounted()){
            File file=new File(fileFolder);
            if(!file.exists()){
                file.mkdir();
            }
            if(recorder!=null){
                recorder.stop();
                recorder.release();
                recorder=null;
            }
            fileName=getCurrentTime();
            recorder=new MediaRecorder();
            recorder.setOutputFile(fileFolder+"/"+fileName+".amr");
            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);//设置音频来源为麦克风
            recorder.setOutputFormat(MediaRecorder.OutputFormat.RAW_AMR);//设置MediaRecorder录制的音频格式
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);//设置MediaRecorder录制音频的编码为amr
        }else {
            ToastUtil.showToast("没有检测到SD卡,不能进入该功能");
            return;
        }
    }

    @Override
    public void startRecord() {
        if(isAudioRecord==false){
            try {
                recorder.prepare();
                recorder.start();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }catch (IOException e) {
                e.printStackTrace();
            }
            isAudioRecord=true;
        }
    }

    @Override
    public void stopRecord() {
        if(isAudioRecord){
            recorder.stop();
            recorder.release();
            isAudioRecord=false;
            recorder=null;
        }
    }

    @Override
    public void deleteOldRecordFile() {
        File file = new File(fileFolder + "/" + fileName + ".amr");
        file.deleteOnExit();
    }

    private Runnable mUpdateMicStatusTimer = new Runnable() {
        public void run() {
            updateMicStatus();
        }
    };
    private void updateMicStatus() {
        if(recorder!=null){
            int ratio = recorder.getMaxAmplitude() / BASE;
            int db = 0;// 分贝
            if (ratio > 1)
                db = (int) (20 * Math.log10(ratio));
            System.out.println("分贝值："+db+"     "+Math.log10(ratio));
            //我对着手机说话声音最大的时候，db达到了35左右，
            recordHandler.postDelayed(mUpdateMicStatusTimer, SPACE);
            //所以除了2，为的就是对应14张图片
            recordHandler.sendEmptyMessage(db/5);
        }
    }
    @Override
    public double getAmplitude() {
        if(!isAudioRecord){
            return 0;
        }else {
            return recorder.getMaxAmplitude();
        }
    }

    @Override
    public String getVoicePath() {
        File file=new File(fileFolder + "/" + fileName + ".amr");
        return file.getPath();
    }

    /***
     * 获取当前日期
     * @return time
     */
    private String getCurrentTime(){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy_MM_dd_HHmmss");
        Date curDate=new Date(System.currentTimeMillis());//获取当前日期
        String time=sdf.format(curDate);
        return time;
    }
    public interface RecordListener {
        public void recordEnd();
    }
}

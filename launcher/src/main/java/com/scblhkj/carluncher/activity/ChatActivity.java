package com.scblhkj.carluncher.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scblhkj.carluncher.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 聊天界面
 * 1.封装消息的实体类
 * 2.消息内容需要缓存(数据库)
 * 3.上拉需要加载之前的历史消息(分页查询数据库)
 * 4.实时的读取服务器转发过来的消息内容
 * 5.录音功能
 * 6.地图坐标显示
 * 7.实时地图坐标显示
 * 8.自定义图片获取
 * 9.拍照功能
 */
public class ChatActivity extends BaseActivity implements View.OnClickListener {

    @Bind(R.id.ivLeft)
    ImageView ivLeft;
    @Bind(R.id.tvFriend)
    TextView tvFriend;
    @Bind(R.id.ivLogo)
    ImageView ivLogo;
    @Bind(R.id.ivAnimation)
    ImageView ivAnimation;
    @Bind(R.id.imEmoji)
    ImageView imEmoji;
    @Bind(R.id.btRecorderV)
    Button btRecorderV;
    @Bind(R.id.imPluse)
    ImageView imPluse;
    @Bind(R.id.rlBBar)
    RelativeLayout rlBBar;
    @Bind(R.id.rlLocation)
    RelativeLayout rlLocation;
    @Bind(R.id.rlAlbum)
    RelativeLayout rlAlbum;
    @Bind(R.id.rlTp)
    RelativeLayout rlTp;
    @Bind(R.id.listView)
    ListView listView;
    @Bind(R.id.llCard)
    LinearLayout llCard;
    private boolean isBottomShow; // 底部菜单是否显示


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        ivLeft.setOnClickListener(this);
        imEmoji.setOnClickListener(this);
        imPluse.setOnClickListener(this);
        rlBBar.setOnClickListener(this);
        rlAlbum.setOnClickListener(this);
        rlTp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivLeft: {
                finish();
                break;
            }
            case R.id.imEmoji: {
                // 弹出Emoji表情供用户选择
                break;
            }
            case R.id.imPluse: {
                // 弹出底部菜单
                if (isBottomShow) {
                    llCard.setVisibility(View.GONE);
                } else {
                    llCard.setVisibility(View.VISIBLE);
                }
                isBottomShow = !isBottomShow;
                break;
            }
            case R.id.rlBBar: {
                break;
            }
            case R.id.rlAlbum: {
                break;
            }
            case R.id.rlTp: {
                break;
            }
            default: {
                break;
            }
        }
    }
}

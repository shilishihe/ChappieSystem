package com.scblhkj.carluncher.app;

import android.accessibilityservice.AccessibilityService;
import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.common.UnCeHandler;
import com.scblhkj.carluncher.domain.ENaviInfo;
import com.scblhkj.carluncher.service.BackKeyService;
import com.scblhkj.carluncher.utils.AMapBroadCostUtil;
import com.scblhkj.carluncher.utils.FileUtil;
import com.scblhkj.carluncher.utils.GsonUtil;
import com.scblhkj.carluncher.utils.MeasureScreenUtil;
import com.scblhkj.carluncher.utils.SDCardScannerUtil;
import com.scblhkj.carluncher.utils.ScreenPowerUtil;
import com.scblhkj.carluncher.utils.SpUtil;
import com.scblhkj.carluncher.utils.ToastUtil;
import com.scblhkj.carluncher.widget.TopDeskLayout;
import com.txznet.sdk.TXZAsrManager;
import com.txznet.sdk.TXZCallManager;
import com.txznet.sdk.TXZConfigManager;
import com.txznet.sdk.TXZMusicManager;
import com.txznet.sdk.TXZNavManager;
import com.txznet.sdk.TXZStatusManager;
import com.txznet.sdk.TXZSysManager;
import com.txznet.sdk.TXZTtsManager;
import com.zhy.http.okhttp.OkHttpUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by he on 2015/10/10.
 */
public class ChappieCarApplication extends Application {

    public static int SCREEN_WIDTH = 0;
    public static int SCREEN_HEIGHT = 0;
    // 记录桌面上哪个模块处于放大状态
    public static Map<View, Boolean> isMaxView;
    public static final String TAG = "ChappieCarApplication";
    // 是否允许HorizatialScrollView消费子控件的滑动事件，默认为消费
    public static int ALLOW_SCROLL = -1;
    public static int CMD_FLAGE = 0x0000; // 服务器请求指令标识
    // 设备的IMIE码
    public static String DEVICE_IMEI;
    // 发送给服务器的胎压数据
    public static byte[] tpmsCaches = new byte[13];
    // 外置SD卡是否存在的标识
    public static boolean IS_CARD_EXIST = false;
    // 车辆当前的位置
    public static String CAR_POSITION = "";
    // 车辆当前所在的城市（用于查询天气）
    public static String CAR_CITY = "";
    public static boolean isDebug = false; // Debug版
    public static boolean isRelease = true; // Release版
    public static double LONGITUDE;  // 经度
    public static double LATITUDE;   // 纬度
    public static boolean isHibernation = false;
    private TXZAsrManager.AsrComplexSelectCallback mAsrComplexSelectCallback; // 唤醒命令词
    private BroadcastReceiver aMapNavMiniBr;
    private ENaviInfo eNaviInfo;
    private boolean isAllView = false;
    public ENaviInfo geteNaviInfo() {
        return eNaviInfo;
    }
    public  boolean isTxzWakeUp=false;
    private TXZCallManager.CallToolStatusListener mCallToolStatusListener;  // 电话状态监听者
    private BroadcastReceiver broadcastReceiver;

    ArrayList<Activity> list = new ArrayList<Activity>();
    /**
     * 关闭Activity列表中的所有Activity*/
    public void finishActivity(){
        for (Activity activity : list) {
            if (null != activity) {
                activity.finish();
            }
        }
        //杀死该应用进程
        android.os.Process.killProcess(android.os.Process.myPid());
    }
    /**
     * 初始化广播接收者
     */
    private void initBroadcastReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch (action) {
                    case "android.intent.action.CUSTOM_VOICE_SPEAK": {
                        Bundle bundle = intent.getExtras();
                        String speakContent = bundle.getString("speakContent");
                        TXZTtsManager.getInstance().speakText(speakContent);
                        syncContacts(); // 同步联系人
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        };
    }

    /**
     * 注册广播接收者
     */
    private void registBroadcastReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.CUSTOM_VOICE_SPEAK");
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        OkHttpUtils.getInstance().debug("OkHttpUtils").setConnectTimeout(100000, TimeUnit.MILLISECONDS);
        initBroadcastReceiver();  // 初始化广播接收者
        registBroadcastReceiver(); // 注册广播接收者
        isMaxView = new HashMap<View, Boolean>();
        ToastUtil.init(this);
        SpUtil.init(this);
        //设置该CrashHandler为程序的默认处理器
        UnCeHandler catchExcep = new UnCeHandler(this);
        Thread.setDefaultUncaughtExceptionHandler(catchExcep);
        MeasureScreenUtil.init(this);
        ScreenPowerUtil.init(this);  // 初始化锁屏工具类
        AMapBroadCostUtil.init(this);
        MeasureScreenUtil.getScreenWidth();
      //  startService(new Intent(this, BackKeyService.class));   // 开启录制服务
        ChappieCarApplication.SCREEN_WIDTH = MeasureScreenUtil.width;
        ChappieCarApplication.SCREEN_HEIGHT = MeasureScreenUtil.height;
        saveDeviceIMIE();
        IS_CARD_EXIST = SDCardScannerUtil.isSecondSDcardMounted();
        printPakagesName();
        FileUtil.copyAssetsDB2Data(this, "CarErrorCode.db"); // 复制OBD故障码数据库
        FileUtil.copyAssetsDB2Data(this, "city_elevation.db"); // 城市海拔数据库
        initAMapNavMiniBr();
        Log.e(TAG, "桌面应用启动");
        new Thread(){
            @Override
            public void run() {
                super.run();
                try {
                    sleep(5000);
                    initTXZSDK();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();

        Log.e(TAG, "同行者启动");
    }

    /**
     * 初始化机车版高德地图广播接收者
     */
    private void initAMapNavMiniBr() {
        aMapNavMiniBr = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {

                    case ChappieConstant.SEND_ACTION: {
                        switch (intent.getStringExtra(ChappieConstant.SEND_BUSINESS_ACTION)) {

                            case ChappieConstant.SEND_LAUNCH_APP: {
                                Log.e(TAG, "高德地图启动");
                                break;
                            }
                            case ChappieConstant.SEND_EXIT_APP: {
                                Log.e(TAG, "高德地图退出");
                                unRegisterAmapBroadCost();
                                break;
                            }
                            case ChappieConstant.SEND_OPEN_NAVI: {
                                String sData = intent.getStringExtra(ChappieConstant.SEND_BUSINESS_DATA);
                                Log.e(TAG, "导航终点数据:" + sData);
                                break;
                            }
                            case ChappieConstant.SEND_CLOSE_NAVI: {
                                Log.e(TAG, "关闭导航");
                                break;
                            }
                            case ChappieConstant.SEND_PATH_FAIL: {
                                Log.e(TAG, "导航路径规划失败");
                                break;
                            }
                            case ChappieConstant.SEND_APP_FOREGROUND: {
                                Log.e(TAG, "程序切换到前台");
                                break;
                            }
                            case ChappieConstant.SEND_APP_BACKGROUND: {
                                Log.e(TAG, "程序切换到后台");
                                break;
                            }
                            case ChappieConstant.SEND_NAVI_INFO: {
                                String sData = intent.getStringExtra(ChappieConstant.SEND_BUSINESS_DATA);
                                Log.e(TAG, "导航数据：" + sData);
                                if (!TextUtils.isEmpty(sData)) {
                                    eNaviInfo = GsonUtil.jsonToBean(sData, ENaviInfo.class);
                                }
                                break;
                            }
                            default: {
                                break;
                            }
                        }
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        };
        registerAmapBroadCost();
    }

    /**
     * 注册高德地图广播接收者
     */
    public void registerAmapBroadCost() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ChappieConstant.SEND_ACTION);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(aMapNavMiniBr, intentFilter);
    }

    /**
     * 注销广播接收者
     */
    public void unRegisterAmapBroadCost() {
        if (aMapNavMiniBr != null) {
            unregisterReceiver(aMapNavMiniBr);
            Log.e(TAG, "退出高德地图,注销广播接收者");
        }
    }

    /**
     * 初始化同行者SDK
     */
    private void initTXZSDK() {
        TXZConfigManager.InitParam initParam = new TXZConfigManager.InitParam(getString(R.string.txz_sdk_init_app_id), getString(R.string.txz_sdk_init_app_token));
        initParam.setWakeupKeywordsNew(getResources().getStringArray(R.array.txz_sdk_init_wakeup_keywords));
        initParam.setAsrType(TXZConfigManager.AsrEngineType.ASR_YUNZHISHENG); // 设置语音识别引擎
        initParam.setTtsType(TXZConfigManager.TtsEngineType.TTS_YUNZHISHENG); // 设置语音播报引擎
        initParam.setAsrMode(TXZConfigManager.AsrMode.ASR_MODE_CHAT);
        TXZConfigManager.getInstance().showFloatTool(TXZConfigManager.FloatToolType.FLOAT_NONE);
        TXZConfigManager.getInstance().initialize(this, initParam,
                new TXZConfigManager.InitListener() {
                    @Override
                    public void onSuccess() {
                        syncContacts(); // 同步联系人
                        isTxzWakeUp=true;
                        TXZMusicManager.getInstance().setMusicTool(TXZMusicManager.MusicToolType.MUSIC_TOOL_KUWO);  // 设置声控音乐为酷我
                        TXZNavManager.getInstance().setNavTool(TXZNavManager.NavToolType.NAV_TOOL_GAODE_MAP);
                        TXZCallManager.getInstance().setCallTool(new TXZCallManager.CallTool() {
                            @Override
                            public CallStatus getStatus() {
                                return CallStatus.CALL_STATUS_IDLE;
                            }

                            @Override
                            public boolean makeCall(TXZCallManager.Contact contact) {
                                Intent intent = new Intent(ChappieConstant.ACTION_BTCALL_CW);
                                Bundle bundle = new Bundle();
                                bundle.putString("Number", contact.getNumber());
                                intent.putExtras(bundle);
                                sendBroadcast(intent);
                                return false;
                            }

                            @Override
                            public boolean acceptIncoming() {
                                int intAutoAnswer = 0;
                                try {
                                    intAutoAnswer = Settings.System.getInt(getContentResolver(), "bluetooth_phone_autoanswer_state");
                                } catch (Settings.SettingNotFoundException e) {
                                    e.printStackTrace();
                                }
                                if (intAutoAnswer == 1) {
                                    mCallToolStatusListener.onOffhook();
                                } else {
                                    sendBroadcast(new Intent(ChappieConstant.ACTION_BTCALL_CE));
                                }
                                return false;
                            }
                            @Override
                            public boolean rejectIncoming() {
                                sendBroadcast(new Intent(ChappieConstant.ACTION_BTCALL_CG));
                                return false;
                            }
                            @Override
                            public boolean hangupCall() {
                                sendBroadcast(new Intent(ChappieConstant.ACTION_BTCALL_CG));
                                return false;
                            }
                            @Override
                            public void setStatusListener(TXZCallManager.CallToolStatusListener callToolStatusListener) {
                                mCallToolStatusListener = callToolStatusListener;
                                if (callToolStatusListener != null) {
                                    //  通知最后的电话状态
                                    callToolStatusListener.onEnabled();
                                    callToolStatusListener.onIdle();
                                }
                            }
                        });
                        TXZSysManager.AppInfo gaodeMap = new TXZSysManager.AppInfo();
                        gaodeMap.strAppName = "高德地图";
                        gaodeMap.strPackageName = "com.autonavi.minimap";
                        TXZSysManager.getInstance().syncAppInfoList(new TXZSysManager.AppInfo[]{gaodeMap});
                        Log.e(TAG, "声控引擎初始化成功");
                        TXZStatusManager.getInstance().addStatusListener(
                                new TXZStatusManager.StatusListener() {
                                    @Override
                                    public void onMusicPlay() {
                                    }

                                    @Override
                                    public void onMusicPause() {
                                    }

                                    @Override
                                    public void onEndTts() {
                                        Log.d("StatusListener", "onEndTts");
                                    }

                                    @Override
                                    public void onEndCall() {
                                        Log.d("StatusListener", "onEndCall");
                                    }

                                    @Override
                                    public void onEndAsr() {
                                        Log.d("StatusListener", "onEndAsr");
                                    }

                                    @Override
                                    public void onBeginTts() {
                                        Log.d("StatusListener", "onBeginTts");
                                    }

                                    @Override
                                    public void onBeginCall() {
                                        Log.d("StatusListener", "onBeginCall");
                                    }

                                    @Override
                                    public void onBeginAsr() {
                                        Log.d("StatusListener", "onBeginAsr");
                                    }

                                    @Override
                                    public void onBeepEnd() {
                                        Log.d("StatusListener", "onBeepEnd");
                                    }
                                });
                    }
                    @Override
                    public void onError(int errCode, String errDesc) {
                        TXZTtsManager.getInstance().speakText("声控引擎初始化发生异常：" + errDesc);
                        Log.e(TAG, "声控引擎初始化发生异常");
                    }
                });
        initWakeUpCommandWord();
    }
    /**
     * 初始化唤醒命令词
     */
    private void initWakeUpCommandWord() {
        mAsrComplexSelectCallback = new TXZAsrManager.AsrComplexSelectCallback() {
            @Override
            public boolean needAsrState() {
                // TODO 是否需要识别状态，识别会对系统静音
                return false;
            }

            @Override
            public String getTaskId() {
                // TODO 返回任务ID，可以取消唤醒识别任务
                return "WAKEUP_TASK";
            }

            @Override
            public void onCommandSelected(String type, String command) {
                Log.e(TAG, "已识别到：" + command);
                switch (type) {
                    case "OPEN_SCREEN": {
                        break;
                    }
                    case "CLOSE_SCREEN": {
                        break;
                    }
                    case "ZOOM_IN_MAP": {
                        AMapBroadCostUtil.zoomInMap();
                        Log.e(TAG, "识别到放大地图");
                        break;
                    }
                    case "ZOOM_OUT_MAP": {
                        AMapBroadCostUtil.zoomOutMap();
                        Log.e(TAG, "识别到缩小地图");
                        break;
                    }
                    case "ROUTE_REMAIN_DIS": {
                        Log.e(TAG, "识别到还有多远");
                        ENaviInfo info = geteNaviInfo();
                        TXZTtsManager.getInstance().speakText("您距离目的地还有" + (Integer.parseInt(info.routeRemainDis) / 1000f) + "公里");
                        break;
                    }
                    case "ROUTE_REMAIN_TIME": {
                        Log.e(TAG, "识别到还要多久");
                        ENaviInfo info = geteNaviInfo();
                        TXZTtsManager.getInstance().speakText("您距离目的地还有" + (Integer.parseInt(info.routeRemainTime) / 60) + "分钟");
                        break;
                    }
                    case "OPEN_ROAD_CURRENT": {
                        Log.e(TAG, "识别到打开路况");
                        TXZTtsManager.getInstance().speakText("已为您打开路况播报");
                        AMapBroadCostUtil.roadBroadOpen();
                        break;
                    }
                    case "CLOSE_ROAD_CURRENT": {
                        Log.e(TAG, "识别到关闭路况");
                        TXZTtsManager.getInstance().speakText("已为您关闭路况播报");
                        AMapBroadCostUtil.roadBroadClose();
                        break;
                    }
                    case "NIGHT_MODE": {
                        Log.e(TAG, "识别到夜间模式");
                        TXZTtsManager.getInstance().speakText("已为您切换为夜间模式");
                        AMapBroadCostUtil.nightNavMode();
                        break;
                    }
                    case "DAY_MODE": {
                        Log.e(TAG, "识别白天模式");
                        TXZTtsManager.getInstance().speakText("已为您切换为白天模式");
                        AMapBroadCostUtil.dayNavMode();
                        break;
                    }
                    case "EXIT_AMP_NAV": {
                        Log.e(TAG, "识别到退出导航");
                        AMapBroadCostUtil.exitNav();
                        break;
                    }
                    case "EIXT_AMP_AMAP": {
                        Log.e(TAG, "识别到退出地图应用");
                        AMapBroadCostUtil.exitAmap();
                        break;
                    }
                    case "OPEN_NAV_VOICE": {
                        Log.e(TAG, "识别到开启导航声音");
                        TXZTtsManager.getInstance().speakText("已为您开启导航声音");
                        AMapBroadCostUtil.openNavVoice();
                        break;
                    }
                    case "CLOSE_NAV_VOICE": {
                        Log.e(TAG, "识别到关闭导航声音");
                        TXZTtsManager.getInstance().speakText("已为您关闭导航声音");
                        AMapBroadCostUtil.closeNavVoice();
                        break;
                    }
                    case "LOOK_ALL_VIEW": {
                        if (!isAllView) {
                            for (int i = 0; i < 10; i++) {  // 查看全程连续缩小地图10次
                                SystemClock.sleep(10);
                                AMapBroadCostUtil.zoomOutMap();
                            }
                            isAllView = true;
                        }
                        break;
                    }
                    case "CLOSE_LOOK_ALL_VIEW": {
                        if (isAllView) {
                            for (int i = 0; i < 10; i++) {  // 查看全程连续放大地图10次
                                SystemClock.sleep(10);
                                AMapBroadCostUtil.zoomInMap();
                            }
                            isAllView = false;
                        }
                    }
                    default: {
                        break;
                    }
                }
            }
        }
                .addCommand("OPEN_SCREEN", "打开屏幕", "开启屏幕")
                .addCommand("CLOSE_SCREEN", "关闭屏幕", "熄灭屏幕")
                .addCommand("ZOOM_IN_MAP", "放大地图")
                .addCommand("ZOOM_OUT_MAP", "缩小地图")
                .addCommand("ROUTE_REMAIN_DIS", "还有多远")
                .addCommand("ROUTE_REMAIN_TIME", "还有多久", "还要多久")
                .addCommand("OPEN_ROAD_CURRENT", "打开路况播报", "开启路况播报")
                .addCommand("CLOSE_ROAD_CURRENT", "关闭路况播报", "停止路况播报")
                .addCommand("NIGHT_MODE", "夜间模式")
                .addCommand("DAY_MODE", "白天模式")
                .addCommand("EXIT_AMP_NAV", "退出导航", "退出高德导航", "关闭导航", "关闭高德导航")
                .addCommand("EIXT_AMP_AMAP", "退出地图", "退出高德地图", "关闭地图", "关闭高德地图")
                .addCommand("OPEN_NAV_VOICE", "打开导航语音")
                .addCommand("CLOSE_NAV_VOICE", "关闭导航语音")
                .addCommand("LOOK_ALL_VIEW", "查看全程", "浏览全程")
                .addCommand("CLOSE_LOOK_ALL_VIEW", "退出查看全程", "退出浏览全程", "关闭查看全程", "关闭查看全程");
        TXZAsrManager.getInstance().useWakeupAsAsr(mAsrComplexSelectCallback);
        // ToastUtil.showToast("唤醒命令词配置成功");
    }

    /**
     * 重置Map集合
     */
    public static void resetIsMaxView() {
        if (isMaxView != null && !isMaxView.isEmpty()) {
            for (Map.Entry<View, Boolean> entry : ChappieCarApplication.isMaxView.entrySet()) {
                if (entry.getValue()) {
                    // 隐藏掉放大的内容布局
                    // entry.getKey().findViewById(R.id.fl_content).setVisibility(View.GONE);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ChappieCarApplication.SCREEN_WIDTH / 4, (int) (ChappieCarApplication.SCREEN_HEIGHT * 0.95));
                    params.gravity = Gravity.CENTER_VERTICAL;
                    // 此处的margin top 和 bottom无效
                    params.setMargins(10, 10, 0, 10);
                    // 初始化
                    Animation scaleAnimation = new ScaleAnimation(0.5f, 1.0f, 1.0f, 1.0f);
                    // 设置动画时间
                    scaleAnimation.setDuration(800);
                    entry.getKey().setAnimation(scaleAnimation);
                    entry.getKey().setLayoutParams(params);
                    // YoYo.with(Techniques.ZoomOut).duration(700).playOn(entry.getKey());
                }
            }
            isMaxView.clear();
        }
    }

    /**
     * 将设备的IMIE码存入到SP中
     */
    public void saveDeviceIMIE() {
        TelephonyManager tm = ((TelephonyManager) getSystemService(TELEPHONY_SERVICE));
        DEVICE_IMEI = tm.getDeviceId();
        Log.e(TAG,"DEVICE_IMEI>>>>>>"+DEVICE_IMEI);
    }

    private void printPakagesName() {
        PackageManager packageManager = this.getPackageManager();
        List<PackageInfo> mPacks = packageManager.getInstalledPackages(0);
        StringBuilder sb = new StringBuilder();
        for (PackageInfo pf : mPacks) {
            sb.append(pf.packageName + "  " + pf.applicationInfo.name + "\n");
        }
        Log.e(TAG, sb.toString());
    }

    /**
     * 同步本机联系人
     */
    public void syncContacts() {
        List<TXZCallManager.Contact> lst = new ArrayList<TXZCallManager.Contact>();
        Uri contact_uri = Uri.parse("content://com.concox.ContactProvider/contact"); //获得联系人默认uri
        ContentResolver resolver = this.getContentResolver();  //获得ContentResolver对象
        Cursor cursor = resolver.query(contact_uri, null, null, null, null); //获取电话本中开始一项光标
        if (cursor != null) {
            while (cursor.moveToNext()) {
                TXZCallManager.Contact con = new TXZCallManager.Contact();
                String telPhone = cursor.getString(cursor.getColumnIndex("telPhone")); // 电话号码
                if (TextUtils.isEmpty(telPhone)) continue;
                String name = cursor.getString(cursor.getColumnIndex("name"));  // 联系人
                con.setNumber(telPhone);
                con.setName(name);
                lst.add(con);
            }
        }
        TXZCallManager.getInstance().syncContacts(lst);
        Log.e(TAG, "同步联系人成功");
    }
}

package com.scblhkj.carluncher.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.scblhkj.carluncher.AmapInfoInterface;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.db.LSInstructionDBImpl;
import com.scblhkj.carluncher.domain.GPSInfo;
import com.scblhkj.carluncher.domain.GpsRecordBean;
import com.scblhkj.carluncher.domain.LSInstructionBean;
import com.scblhkj.carluncher.domain.RemoteAmapInfoBean;
import com.scblhkj.carluncher.domain.SpeedInfoBean;
import com.scblhkj.carluncher.utils.CommonUtil;
import com.scblhkj.carluncher.utils.ToastUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 记录GPS坐标的服务
 */
public class RouteRecordService extends Service implements AMapLocationListener {

    private AMapLocationClient locationClient = null;
    private AMapLocationClientOption locationOption = null;
    double longitude; // 经度
    double latitude; // 纬度
    private static final String TAG = "GPS记录服务";
    // 每五个坐标存一条指令
    private List<GpsRecordBean> cacheList;
    private ArrayList speedInfoList;
    // 指令数据库Dao
    private LSInstructionDBImpl lsDao;
    private int num;
    private RemoteAmapInfoBean remoteAmapInfoBean = new RemoteAmapInfoBean();
    private float speed;//当前车速
    private int speedValue = 30;//用户自定义的急减速的速度值,默认值是30
    private int timeValue = 5;//用户自定义的急减速的时间值，默认值是5S
    // 延时屏蔽三秒之内的重复碰撞检测
    private static final int START_CRASH_DELAY = 3000;
    // 上次监测的时间
    private long lastUpdateTime = 0;
    private boolean isSpeedLimitedHappend;//是否发生急减速事件
    private String Cimei, Ctime, speedTimeSpeed;
    private boolean isSettingSpeedInfo;
    RegisterSpeedReceiver registerSpeedReceiver;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    private Handler recordHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0x01:
                    ToastUtil.showToast("设置失败,参数不能为0");
                    break;
                case 0x02:

                    break;
            }
            super.handleMessage(msg);
        }
    };
    private String speedCramer, isSendSpeedPic, speedVCramer, isSendSpeedVid, imei;
    private int limteValue;//减速值
    private long currentUpdateTime;
    private long timeInterval;

    public RouteRecordService() {
    }


    @Override
    public void onCreate() {
        super.onCreate();
        sp = getSharedPreferences("deskstate", Context.MODE_PRIVATE);
        editor = sp.edit();
        //获取速度值
        if (sp.getInt("speedValue", speedValue) != 0) {
            speedValue = sp.getInt("speedValue", speedValue);//用户自定义的急减速的速度值
            Log.e("info", "sp里的speedValue:" + sp.getInt("speedValue", speedValue));
        } else {
            speedValue = 30;//用户自定义的急减速的速度值,默认值是30
            editor.putInt("speedValue", speedValue);
            editor.commit();
        }
        //获取时间值
        if (sp.getInt("timeValue", timeValue) != 0) {
            timeValue = sp.getInt("timeValue", timeValue);//用户自定义的急减速的时间值
            Log.e("info", "sp里的timeValue:" + sp.getInt("timeValue", timeValue));
        } else {
            timeValue = 5;//用户自定义的急减速的时间值，默认值是5S
            editor.putInt("timeValue", timeValue);
            editor.commit();
        }
        TelephonyManager tm = (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);
        imei = tm.getDeviceId();//String
        editor.putString("deviceImei", imei);
        editor.commit();
        Log.e("info", "HddrvImei" + imei);
        lsDao = new LSInstructionDBImpl(this);
        cacheList = new ArrayList<GpsRecordBean>();
        speedInfoList = new ArrayList<SpeedInfoBean>();
        locationClient = new AMapLocationClient(this.getApplicationContext());
        locationOption = new AMapLocationClientOption();
        // 设置定位监听
        locationClient.setLocationListener(this);
        // 设置定位参数
        initOption();
        // 设置定位参数
        locationClient.setLocationOption(locationOption);
        // 启动定位
        locationClient.startLocation();
        //  注册监听用户请求发送修改急减速告警参数的广播;
        registerLimitedSpeedReceiver();
    }

    private void registerLimitedSpeedReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ChappieConstant.SEND_CAR_LIMITED_SPEED);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerSpeedReceiver = new RegisterSpeedReceiver();
        registerReceiver(registerSpeedReceiver, filter);

    }

    private void unRegisterregisterLimitedSpeedReceiver() {
        if (registerSpeedReceiver != null) {
            unregisterReceiver(registerSpeedReceiver);
        }
    }

    //获取当前的时间戳
    private String getTimeStamp() {
        long time = Long.parseLong(String.valueOf(System.currentTimeMillis()).toString().substring(0, 10));//当前10位的时间戳
        //long time=System.currentTimeMillis()/1000;
        String timeStamp = String.valueOf(time);
        return timeStamp;
    }

    private class RegisterSpeedReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ChappieConstant.SEND_CAR_LIMITED_SPEED)) {
                Log.e("info", "routeRecordService收到急减速修改参数的广播.");
                speedValue = Integer.parseInt(intent.getStringExtra("speedValue"));
                timeValue = Integer.parseInt(intent.getStringExtra("speedTime"));
                Cimei = intent.getStringExtra("imei");
                Ctime = intent.getStringExtra("Ctime");
                speedCramer = intent.getStringExtra("speedCramer");
                isSendSpeedPic = intent.getStringExtra("isSendSpeedPic");
                speedVCramer = intent.getStringExtra("speedVCramer");
                isSendSpeedVid = intent.getStringExtra("isSendSpeedVid");
                isSettingSpeedInfo = true;
                Log.e("info", "routeRecordService接收到的修改时间是:" + timeValue + "--修改的速度值是:" + speedValue + "--急减速拍照:" + speedCramer
                        + "急减速照片文件是否发送>>" + isSendSpeedPic + " 急减速摄像的摄像头>>" + speedVCramer + " isSendSpeedVid>>" + isSendSpeedVid);
                if (speedValue == 0 || timeValue == 0) {//如果设置的参数有0的话，就返回
                    recordHandler.sendEmptyMessage(0x01);
                    return;
                }
                //将远程端请求值缓存在本地
                editor.putInt("speedValue", speedValue);
                editor.putInt("timeValue", timeValue);
                editor.putString("speedCramer", speedCramer);
                editor.putString("isSendSpeedPic", isSendSpeedPic);
                editor.putString("speedVCramer", speedVCramer);
                editor.putString("isSendSpeedVid", isSendSpeedVid);
                editor.putBoolean("isSettingSpeedInfo", isSettingSpeedInfo);
                editor.commit();
                sendTimeSpeedBroadcast();

            }
        }
    }

    private void sendTimeSpeedBroadcast() {
        Intent intent = new Intent();
        intent.setAction(ChappieConstant.SEND_MODIFY_SPEED_SUCCESS);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.putExtra("imei", Cimei);
        intent.putExtra("Ctime", Ctime);
        sendBroadcast(intent);
        Log.e("info", "发送急减速修改参数成功的广播.");
    }

    //获取ArrayList中的最小值
    public int ArrayListMin(ArrayList sampleList) {
        try {
            int mixDevation = 0;
            int totalCount = sampleList.size();
            if (totalCount >= 1) {
                int min = Integer.parseInt(sampleList.get(0).toString());
                for (int i = 0; i < totalCount; i++) {
                    int temp = Integer.parseInt(sampleList.get(i).toString());
                    if (min > temp) {
                        min = temp;
                    }
                }
                mixDevation = min;
            }
            return mixDevation;
        } catch (Exception ex) {
            throw ex;
        }
    }

    //获取ArrayList中的最大值
    public int ArrayListMax(ArrayList sampleList) {
        try {
            int maxDevation = 0;
            int totalCount = sampleList.size();
            if (totalCount >= 1) {
                int max = Integer.parseInt(sampleList.get(0).toString());
                for (int i = 0; i < totalCount; i++) {
                    int temp = Integer.parseInt(sampleList.get(i).toString());
                    if (temp > max) {
                        max = temp;
                    }
                }
                maxDevation = max;
            }
            return maxDevation;
        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * 获取速度值
     */
    private void getRandumNum() {
        int val = (int) speed;
        speedInfoList.add(val);
        if (speedInfoList.size() >= timeValue) {
            int maxNum = ArrayListMax(speedInfoList);//获取最大值
            int indexMaxNum = speedInfoList.indexOf(maxNum); //最大值的下标
            int minNum = ArrayListMin(speedInfoList);//获取最小值
            int indexMinNum = speedInfoList.indexOf(minNum);//最小值的下标
            if (indexMaxNum < indexMinNum) { //如果最大值的下标小于最小值的下标,则是急减速事件
                int resultNum = maxNum - minNum;
                Log.e("info", "发生急减速事件，事件值是" + resultNum);
                // 现在检测时间
                currentUpdateTime = System.currentTimeMillis();
                // 两次检测的时间间隔
                timeInterval = currentUpdateTime - lastUpdateTime;
                // 现在的时间变成last时间
                lastUpdateTime = currentUpdateTime;
                if (!isSpeedLimitedHappend) {
                    speedTimeSpeed = getTimeStamp();
                    limteValue = maxNum - minNum;
                    isSpeedLimitedHappend = true;
                    sendSpeedLimtedHappenBroadcast(speedTimeSpeed, limteValue);
                }
                Message message = Message.obtain();
                message.what = 0x02;
                recordHandler.sendEmptyMessageDelayed(0x02, START_CRASH_DELAY);//屏蔽3秒内重复急减速告警

            }
            speedInfoList.remove(0);
            //Log.e("info", "speedlist" + speedInfoList.toString() + "--max:" + maxNum + "--min:" + minNum);
            /***
             *  if(maxNum-minNum>=speedValue&&maxNum-minNum!=0){
             Log.e("info","发生急减速事件，事件值是"+resultNum);
             // 现在检测时间
             currentUpdateTime = System.currentTimeMillis();
             // 两次检测的时间间隔
             timeInterval = currentUpdateTime - lastUpdateTime;
             // 现在的时间变成last时间
             lastUpdateTime = currentUpdateTime;
             if(!isSpeedLimitedHappend){
             speedTimeSpeed=getTimeStamp();
             limteValue = maxNum - minNum;
             isSpeedLimitedHappend=true;
             sendSpeedLimtedHappenBroadcast(speedTimeSpeed,limteValue);
             }
             Message message=Message.obtain();
             message.what=0x02;
             recordHandler.sendEmptyMessageDelayed(0x02,START_CRASH_DELAY);//屏蔽3秒内重复急减速告警


             }
             */


        }
        /**测试用的
         * Timer executeSchedule = new Timer();
         executeSchedule.schedule(new TimerTask() {
        @Override public void run() {
        int val = (int)(Math.random()*100+1);
        Log.e("info", "val:" + val);
        speedInfoList.add(val);
        if(speedInfoList.size()>=5){
        int maxNum=ArrayListMax(speedInfoList);//获取最大值
        int minNum=ArrayListMin(speedInfoList);//获取最小值
        Log.e("info","speedlist"+speedInfoList.toString()+"--max:"+maxNum+"--min:"+minNum);
        if(maxNum-minNum>=20){
        Log.e("info","发生急减速事件，事件值是20");
        }
        speedInfoList.remove(0);
        Log.e("info","remove:"+speedInfoList.toString());
        }
        }
        }, 0, 1000);*
         *
         */


    }
    // String speedCramer,isSendSpeedPic,speedVCramer,isSendSpeedVid;

    /***
     * 发生急减速事件广播，接收端:hddrvService
     *
     * @param speedTimeStamp
     * @param limteValue
     */
    private void sendSpeedLimtedHappenBroadcast(String speedTimeStamp, int limteValue) {
        if (sp.getBoolean("isSettingSpeedInfo", isSettingSpeedInfo) == true) {
            isSettingSpeedInfo = sp.getBoolean("isSettingSpeedInfo", isSettingSpeedInfo);
            Intent inten = new Intent();
            inten.putExtra("speedTimeStamp", speedTimeStamp);
            inten.putExtra("deviceImei", imei);
            inten.putExtra("limteValue", limteValue);//减速值
            inten.putExtra("speedCramer", sp.getString("speedCramer", ""));
            inten.putExtra("isSendSpeedPic", sp.getString("isSendSpeedPic", ""));
            inten.putExtra("speedVCramer", sp.getString("speedVCramer", ""));
            inten.putExtra("isSendSpeedVid", sp.getString("isSendSpeedVid", ""));
            inten.putExtra("isSettingSpeedInfo", isSettingSpeedInfo);
            inten.setAction(ChappieConstant.SEND_LIMITED_SOEED_HAPPEN);
            inten.addCategory(Intent.CATEGORY_DEFAULT);
            sendBroadcast(inten);

        } else if (sp.getBoolean("isSettingSpeedInfo", isSettingSpeedInfo) == false) {
            //默认情况下主摄像头拍照和摄像，不传图片和视频文件
            isSettingSpeedInfo = sp.getBoolean("isSettingSpeedInfo", isSettingSpeedInfo);
            Intent inten = new Intent();
            inten.putExtra("speedTimeStamp", speedTimeSpeed);
            inten.putExtra("deviceImei", imei);
            inten.putExtra("speedCramer", "0");
            inten.putExtra("isSendSpeedPic", "0");
            inten.putExtra("speedVCramer", "0");
            inten.putExtra("isSendSpeedVid", "0");
            inten.putExtra("isSettingSpeedInfo", isSettingSpeedInfo);
            inten.setAction(ChappieConstant.SEND_LIMITED_SOEED_HAPPEN);
            inten.addCategory(Intent.CATEGORY_DEFAULT);
            sendBroadcast(inten);
        }


    }

    /**
     * 设置定位参数
     */
    private void initOption() {
        // 设置高精度定位
        locationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        locationOption.setOnceLocation(false);
        locationOption.setNeedAddress(true);
        locationOption.setInterval(2000);
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (aMapLocation != null) {
            longitude = aMapLocation.getLongitude(); // 经度
            latitude = aMapLocation.getLatitude(); // 纬度
            speed = aMapLocation.getSpeed();//车速
            // Log.e("info","RouteRecordService获取的车速"+speed+" 经度:"+longitude+" 纬度:"+latitude);
            GPSInfo gpsInfo = new GPSInfo();
            gpsInfo.setSpeed(speed);
            gpsInfo.setLatitude(latitude);
            gpsInfo.setLongitude(longitude);
            gpsInfo.setCountry(aMapLocation.getCountry());
            gpsInfo.setCity(aMapLocation.getCity());
            gpsInfo.setAddress(aMapLocation.getCountry() + aMapLocation.getAddress());

            remoteAmapInfoBean.setAddress(aMapLocation.getCountry() + aMapLocation.getAddress());
            remoteAmapInfoBean.setCity(aMapLocation.getCity());
            remoteAmapInfoBean.setLatitude(latitude);
            remoteAmapInfoBean.setLongitude(longitude);
            getRandumNum();
            sendGPSBroadCast(gpsInfo); // 发送GPS坐标信息广播
            if (ChappieCarApplication.isDebug)
                Log.e("info", " 经度= " + longitude + " 纬度 =" + latitude + " 错误码 = " + aMapLocation.getErrorCode() + " 错误信息 =" + aMapLocation.getErrorInfo());
            recordRoute();
        } else {
            Log.e("info", "地图回调数据为空");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unRegisterregisterLimitedSpeedReceiver();
        if (null != locationClient) {
            /**
             * 如果AMapLocationClient是在当前Activity实例化的，
             * 在Activity的onDestroy中一定要执行AMapLocationClient的onDestroy
             */
            locationClient.onDestroy();
            locationClient = null;
            locationOption = null;
        }
    }

    /**
     * 处理高德地图回调过来的数据
     */
    private void recordRoute() {
        try {
            if (longitude != 0.0 && latitude != 0.0) { // 经纬度不能超过预置值
                //     if (cacheList.size() > 0 && cacheList.get(cacheList.size() - 1).getLongitude() == LONGITUDE && cacheList.get(cacheList.size() - 1).getLatitude() == LATITUDE) {
                // 前后两次获取到的经纬度值未改变
                //          Log.e(TAG, "前后两次定位的坐标值未改变");
                //      } else {
                GpsRecordBean bean = new GpsRecordBean();
                bean.setLongitude(longitude);
                bean.setLatitude(latitude);
                bean.setSpeed(speed);
                cacheList.add(bean);
                //      }
                if (cacheList.size() >= 5) {
                    // 拼装指令到数据库
                    insertGPSData2DataBase(cacheList);
                    cacheList.clear();
                    if (ChappieCarApplication.isDebug)
                        Log.e("info", "缓存队列超过5条，清空集合");
                }
            }
        } catch (Exception e) {
            if (ChappieCarApplication.isDebug)
                Log.e("info", "[RouteRecordService]startRecordRoute catch exception:" + e.toString());
        }
    }

    /**
     * 将缓存的5个GPS坐标拼装成指令存放到数据库中
     *
     * @param list
     */
    private void insertGPSData2DataBase(List<GpsRecordBean> list) {
        LSInstructionBean bean = new LSInstructionBean();
        try {
            StringBuilder cmdSB = new StringBuilder();
            cmdSB.append('G');
            cmdSB.append(ChappieCarApplication.DEVICE_IMEI);//imei
            cmdSB.append(CommonUtil.getUnixTimeStamp());//时间轴
            for (GpsRecordBean grBean : list) {
                double longitude = grBean.getLongitude(); // 经度
                double latitude = grBean.getLatitude();  // 纬度
                float speed = grBean.getSpeed();//车速
                String latStr = latitude + "";
                String lotStr = longitude + "";
                if (ChappieCarApplication.isDebug)
                    Log.e("info", "routerecorderservice纬度 = " + latStr);
                cmdSB.append(latitude);  // 添加纬度
                if (ChappieCarApplication.isDebug)
                    Log.e("info", "routerecorderservice经度 = " + lotStr);
                cmdSB.append(longitude); // 添加经度
                if (ChappieCarApplication.isDebug) {
                    cmdSB.append(speed);//添加速度
                    Log.e("info", "routerecorderservicespeed=" + speed);
                }
            }
            //设置拼装好的指令(标识 + IMEI + [时间戳 + 纬度 + 经度+车速] )
            bean.setTimeStamp(CommonUtil.getTimeStamp());
            bean.setInstruction(cmdSB.toString());
            Log.e("info", " bean.setInstruction=" + cmdSB.toString());
            if (ChappieCarApplication.isDebug)
                Log.e("info", "插入数据库之前数据为 ： " + cmdSB.toString());
            long l = lsDao.inserData(bean);
            if (l > 0) {
                Log.e("info", "定位数据插入数据库成功");
            }
            sendBroaCast2Server();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 发送广播，通知核心服务GPS数据包已经准备好，可以发送至服务器了
     */
    private void sendBroaCast2Server() {
        Intent dataIntent = new Intent();
        dataIntent.setAction(ChappieConstant.UPLOAD_CACHE_DATA_2_SERVER_ACTION);
        dataIntent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(dataIntent);
        if (ChappieCarApplication.isDebug)
            Log.e("info", "发送GPS数据插入成功的广播");
    }

    private final AmapInfoInterface.Stub mBinder = new AmapInfoInterface.Stub() {
        @Override
        public RemoteAmapInfoBean AmapInfoInterfaceCallBack() throws RemoteException {
            return remoteAmapInfoBean;
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    /**
     * 广播GPS坐标数据
     *
     * @param gpsInfo
     */
    private void sendGPSBroadCast(GPSInfo gpsInfo) {
        Intent dataIntent = new Intent();
        Bundle mBundle = new Bundle();
        dataIntent.setAction(ChappieConstant.GPS_INFO);
        mBundle.putSerializable(ChappieConstant.GPS_INFO, gpsInfo);
        dataIntent.putExtras(mBundle);
        dataIntent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(dataIntent);
    }
}

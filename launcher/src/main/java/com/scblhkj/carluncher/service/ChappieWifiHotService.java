package com.scblhkj.carluncher.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.thread.WifiAPSendFileDataThread;
import com.scblhkj.carluncher.utils.CommonUtil;
import com.scblhkj.carluncher.utils.FileToolkit;
import com.scblhkj.carluncher.utils.FileUtil;
import com.scblhkj.carluncher.utils.ThumbnailUtil;
import com.scblhkj.carluncher.utils.ToastUtil;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * wifi热点通信服务
 */
public class ChappieWifiHotService extends Service {

    private WifiAPListenThread wifiAPListenThread;
    private WifiManager wifiManager;
    private static final String TAG = "WifiAP";
    private WifiConfiguration apConfig;
    private BroadcastReceiver broadcastReceiver;
    private DataOutputStream dos = null;
    private boolean isWifiAp;
    private ArrayList<File> fileList;
    private StringBuffer fileBuffer;
    private WeakReference<Socket> mSocket;
    private long heartBeatSendTime = 0L;
    private OutputStream ou;

    /**
     * 初始化广播接收者
     */
    private void initBroadCast() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {

                    case "com.scblhkj.cp.FINISH_VIDEO_FILE": {
                        String videoFilePath = intent.getStringExtra("com.scblhkj.cp.FINISH_VIDEO_FILE_VALUE");   // 获取到了手动录制视频的文件路径
                        Log.e("info", "接收到了手动录制完成后发送过来的视频路径" + videoFilePath);
                        //new WifiAPSendFileDataThread(dos, videoFilePath, "FV").start();
                        break;
                    }
                    case "com.scblhkj.chappie.hddvr.SEND_CAR_CMD_WIFI_PIC": {
                        String filePath = intent.getStringExtra("com.scblhkj.chappie.hddvr.SEND_CAR_CMD_WIFI_PIC_PATH");
                        Log.e("info", "接收到了手动录制完成后发送过来的图片路径" );
                       // new WifiAPSendFileDataThread(dos, filePath, "CP").start();// 消息发送方启动线程发送消息
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        };
    }

    /**
     * 注册广播接收者
     */
    private void registerBroadCastReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        intentFilter.addAction("com.scblhkj.chappie.hddvr.SEND_CAR_CMD_WIFI_PIC");
        intentFilter.addAction("com.scblhkj.chappie.hddvr.SEND_CAR_COLLISION_PIC");
        intentFilter.addAction("com.scblhkj.cp.FINISH_VIDEO_FILE");  // 手动视频录制完成之后的广播,携带的广播数据是录制完成视频的路径
        registerReceiver(broadcastReceiver, intentFilter);

    }

    private void unRegisterBroadCastReceiver() {
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
            broadcastReceiver = null;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MyBinder();
    }
    public class MyBinder extends Binder

    {
      //判断是否开启WIFI热点
        public boolean isWifiap()

        {

            return isWifiAp;

        }
        /**
         * 关闭WifiAP
         */
        public void closeWifiAP() {
            wifiManager.setWifiEnabled(false);
            try {
                Method method = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, Boolean.TYPE);
                method.invoke(wifiManager, apConfig, false);
                Log.e("info","关闭了wifi热点");
                isWifiAp=false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        /**
         * 开启Wifi热点
         */
        public void openWifiAP() {
            try {
                // 热点的配置类
                apConfig = new WifiConfiguration();
                // 配置热点的名称(可以在名字后面加点随机数什么的)
                apConfig.SSID = "Chappie_" + ChappieCarApplication.DEVICE_IMEI;
                String PWSS = "12345678";
                // 配置热点的密码
                apConfig.preSharedKey = PWSS;
                apConfig.hiddenSSID = false;
                apConfig.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                apConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                apConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                apConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                apConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                apConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                apConfig.status = WifiConfiguration.Status.ENABLED;
                // 通过反射调用设置热点
                Method method = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, Boolean.TYPE);
                // 返回热点打开状态
                method.invoke(wifiManager, apConfig, true);
                Log.e("info", "热点打开成功");
                isWifiAp=true;
            } catch (Exception e) {
                Log.e("info", "热点打开失败");
                isWifiAp=false;
                e.printStackTrace();
            }
        }


    }
    @Override
    public void onDestroy() {
        unRegisterBroadCastReceiver();
        super.onDestroy();
    }

    @Override
    public void onCreate() {
        initBroadCast();
        registerBroadCastReceiver();
        wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
        openWifiAP();
        wifiAPListenThread = new WifiAPListenThread();
        wifiAPListenThread.start();

        super.onCreate();
        Log.e("info", "WifiAP服务启动");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * 开启Wifi热点
     */
    private void openWifiAP() {
        try {
            // 热点的配置类
            apConfig = new WifiConfiguration();
            // 配置热点的名称(可以在名字后面加点随机数什么的)
            apConfig.SSID = "Chappie_" + ChappieCarApplication.DEVICE_IMEI;
            String PWSS = "12345678";
            // 配置热点的密码
            apConfig.preSharedKey = PWSS;
            apConfig.hiddenSSID = false;
            apConfig.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
            apConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            apConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            apConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            apConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            apConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            apConfig.status = WifiConfiguration.Status.ENABLED;
            // 通过反射调用设置热点
            Method method = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, Boolean.TYPE);
            // 返回热点打开状态
            method.invoke(wifiManager, apConfig, true);
            Log.e("info", "热点打开成功");
            isWifiAp=true;
        } catch (Exception e) {
            Log.e("info", "热点打开失败");
            isWifiAp=false;
            e.printStackTrace();
        }
    }

    /**
     * 关闭WifiAP
     */
    private void closeWifiAP() {
        wifiManager.setWifiEnabled(false);
        try {
            Method method = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, Boolean.TYPE);
            method.invoke(wifiManager, apConfig, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * 获取指定文件夹下的所有文件列表
     * @param file
     */
    public   void getAllFiles(File file){
        fileList=new ArrayList<File>();
        fileBuffer=new StringBuffer();
        File files[]=file.listFiles();//获取file文件夹下的集合
        if(files!=null){
            for(File f:files){
                if(f.isDirectory()){ //如果path表示的是一个目录则返回true
                    getAllFiles(f);
                }else{
                    Log.e("info","每个文件的名字:"+f.toString());
                    fileList.add(f);
                    fileBuffer.append(f.getName());
                }
            }
            Log.e("info","file.size:"+fileList.size());
        }else{
            ToastUtil.showToast("该文件夹下没有文件!");

        }
    }


    /**
     * 发送数据到服务器上
     *
     * @param data
     * @return
     * @ todo
     */
    private OutputStream os;  // 输出流
    public boolean sendLocalData2Server(String data) {
        if (null == mSocket || null == mSocket.get()) {
            return false;
        }
        Socket soc = mSocket.get();
        try {
            if (!soc.isClosed() && !soc.isOutputShutdown()) {
                os = soc.getOutputStream();
                data = data + "\n";
                os.write(data.getBytes());
                os.flush();
                Log.e("info", "发送到服务器的Data>>" + data);
                heartBeatSendTime = System.currentTimeMillis();//每次发送完数据，就改一下最后成功发送的时间，节省心跳间隔时间
            } else {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /***
     * 判断文件是否存在
     * @param filePath
     * @return
     */
    public boolean fileIsExist(String filePath){
        try{
            File f=new File(filePath);
            if(!f.exists()){
                return  false;
            }
        }catch(Exception e){
            return false;
        }
        return true;
    }
    /**
     * 收到数据的回调方法
     *
     * @author Leo
     */
    private class WifiAPListenThread extends Thread {
        private static final String TAG = "WifiAPListenThread";
        private ServerSocket serverSocket = null;
        private Socket socket;

        public WifiAPListenThread() {
            try {
                serverSocket = new ServerSocket(12345);// 监听本机的12345端口
            } catch (IOException e) {
                Log.d("info", "WifiAPListenThread ServerSocket init() has exception");
            }
        }
        /**
         * 发送文件到服务器上
         * @return
         */
        public boolean sendLocalFile2Server(File file,String timeStamp,String dataImei,String cmd){
            if (null == mSocket || null == mSocket.get()) {
                return false;
            }
            Socket soc = mSocket.get();
            try {
                if (!soc.isClosed() && !soc.isOutputShutdown()) {
                    new WifiAPSendFileDataThread(new DataOutputStream(soc.getOutputStream()),file.getPath(),cmd,timeStamp,dataImei).start();
                    heartBeatSendTime = System.currentTimeMillis();//每次发送完数据，就改一下最后成功发送的时间，节省心跳间隔时间
                } else {
                    return false;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }
        @Override
        public void run() {
            while (true) {
                try {
                    socket = serverSocket.accept();// 等待消息
                    socket.setSoTimeout(1000 * 3600);
                    socket.getKeepAlive();
                    Log.e("info", "执行到这里");
                    if (socket != null && !socket.isClosed()) {
                        dos = new DataOutputStream(socket.getOutputStream());
                        String clientIP = socket.getInetAddress().getHostAddress();
                        InputStream is = socket.getInputStream();
                        byte[] buffer = new byte[1024 * 2];
                        int length = 0;
                        if (!socket.isClosed() && !socket.isInputShutdown() && ((length = is.read(buffer)) != -1)) {
                            if (length > 15) {
                                String msgData = new String(Arrays.copyOf(buffer, length));
                                String IMEI = msgData.substring(0, 15);  // 客户端回传的设备IMEI号
                                msgData = msgData.substring(15, msgData.length());
                                Log.e("info", "IMEI = " + IMEI);
                                Log.e("info", "接收到客户端发过来的数据是:" + msgData);
                            //    dos.write("heartbeat".getBytes());  屏蔽掉心跳数据发送message.length()==39&&message.substring(26,27).equals("C")
                            //    dos.flush();
                                if (msgData.length()==39&&msgData.substring(26,27).equals("C")) {//wifi状态下拍照
                                    Log.e("info", "进入外网状态下远程控制拍照操作！");
                                    String imei=msgData.substring(11,26);
                                    String Ctime=msgData.substring(0,11);
                                    String cramer=msgData.substring(38);
                                    Intent intent = new Intent();
                                    intent.setAction("com.android.chappie.ACTION_WIFI_C_PIC");
                                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                                    intent.putExtra("imei", imei);
                                    intent.putExtra("ctime", Ctime);
                                    intent.putExtra("cramer", cramer);
                                    sendBroadcast(intent);
                                    Log.e("info", "收到了C指令，并发送了拍照广播");
                                } else if (msgData.length()==39&&msgData.substring(26,27).equals("R")) {//wifi录像
                                    String imei=msgData.substring(11,26);
                                    String Ctime=msgData.substring(0,11);
                                    String cramer=msgData.substring(38);
                                    Intent intent = new Intent();
                                    intent.setAction("com.android.chappie.ACTION_NET_C_VID");
                                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                                    intent.putExtra("imei", imei);
                                    intent.putExtra("ctime", Ctime);
                                    intent.putExtra("cramer", cramer);
                                    sendBroadcast(intent);
                                } else if(msgData.length()==70&&msgData.substring(26,27).equals("T")){////请求指定文件的缩略图
                                    String orderTime=msgData.substring(0,11);
                                    String orderImei=msgData.substring(11,26);
                                    String FileDir=msgData.substring(38,50);
                                    String childDir=msgData.substring(50,60);
                                    String FileName=msgData.substring(60,msgData.length());
                                    Log.e("info","收到客户端指令信息orderTime"+orderTime+"--orderImei"+orderImei+"--videoDir"+FileDir
                                            +"--childDir"+childDir+"--videoName"+FileName);
                                    //判断目录名称
                                    if(FileDir.contains("travel")){
                                        FileDir="travel";
                                    }else if(FileDir.contains("auto")){
                                        FileDir="auto";
                                    }else if(FileDir.contains("arttificial")){
                                        FileDir="arttificial";
                                    }
                                    //判断子目录名称
                                    childDir=msgData.substring(52,60);
                                    Log.e("info", "请求下载的文件缩略图的目录是:" + FileDir + "子目录名称:" + childDir + "视频文件名称是:" + FileName);
                                    //请求文件路径
                                    String path="/storage/sdcard1/ChappieLauncher/" + FileDir + "/" +childDir+"/"+ FileName;
                                    Log.e("info","path:"+path);
                                    //判断请求的文件是否存在
                                    if(fileIsExist(path)){
                                        if(FileName.substring(7,10).equals("jpg")){
                                            Log.e("info","进入图片缩略图传输");
                                            //获取缩略图对象
                                           // Bitmap imageBitmap= ThumbnailUtil.getImageThumbnail(path);
                                            //将缩略图转为file对象
                                            File imageFile= ThumbnailUtil.getImageThumbnail2File(path);
                                            sendLocalFile2Server(imageFile,orderTime,orderImei,"T");
                                        }else if(FileName.substring(7,10).equals("mp4")){
                                            Log.e("info","进入图片缩略图传输");
                                            Bitmap videoBitmap=ThumbnailUtil.getVideoThumbnail(path);
                                            //将缩略图转为file对象
                                            File imageFile=FileUtil.saveFile(videoBitmap, path, FileName);
                                            sendLocalFile2Server(imageFile,orderTime,orderImei,"T");
                                        }
                                    }else {
                                      ToastUtil.showToast("请求的文件不存在");
                                        return;
                                    }
                                }else if (msgData.length()==60&&msgData.substring(26,27).equals("F")) {//请求后视镜指定目录的文件列表
                                    String Fimei=msgData.substring(11,26);
                                    String Ftime=msgData.substring(0,11);
                                    String dir=msgData.substring(38,50);
                                    String childDir=msgData.substring(50,60);
                                    //判断目录名称
                                    if(dir.contains("travel")){
                                        dir="travel";
                                    }else if(dir.contains("auto")){
                                        dir="auto";
                                    }else if(dir.contains("arttificial")){
                                        dir="arttificial";
                                    }
                                    //判断子目录名称
                                    childDir=msgData.substring(52,60);
                                    String path="/storage/sdcard1/ChappieLauncher/" + dir + "/" + childDir;
                                    File file=new File(path);
                                    getAllFiles(file);//获取指定文件夹里的文件列表

                                    if(fileList.size()>0){ //拼接文件列表长度
                                        String fileSize=fileList.size()+"0";
                                        if(fileSize.length()==2){
                                            fileSize="00000000"+fileSize;
                                        }else if(fileSize.length()==3){
                                            fileSize="0000000"+fileSize;
                                        }else if(fileSize.length()==4){
                                            fileSize="000000"+fileSize;
                                        }else if(fileSize.length()==5){
                                            fileSize="00000"+fileSize;
                                        }else if(fileSize.length()==6){
                                            fileSize="0000"+fileSize;
                                        }else if(fileSize.length()==7){
                                            fileSize="000"+fileSize;
                                        }else if(fileSize.length()==8){
                                            fileSize="00"+fileSize;
                                        }else if(fileSize.length()==9){
                                            fileSize="0"+fileSize;
                                        }else if(fileSize.length()==3){
                                            fileSize=fileSize;
                                        }
                                        String data=Ftime+Fimei+"F1"+fileSize+fileBuffer;
                                        sendLocalData2Server(data);
                                    }
                                } else if (msgData.length()==70&&msgData.substring(26,27).equals("G")) {//wifi下请求后视镜指定文件
                                    String orderTime=msgData.substring(0,11);
                                    String orderImei=msgData.substring(11,26);
                                    String videoDir=msgData.substring(38,50);
                                    String childDir=msgData.substring(50,60);
                                    String videoName=msgData.substring(60, msgData.length());
                                    Log.e("info","收到客户端wifi指令信息orderTime"+orderTime+"--orderImei"+orderImei+"--videoDir"+videoDir
                                            +"--childDir"+childDir+"--videoName"+videoName);
                                    if(videoDir.contains("travel")){
                                        videoDir="travel";
                                    }else if(videoDir.contains("auto")){
                                        videoDir="auto";
                                    }else if(videoDir.contains("arttificial")){
                                        videoDir="arttificial";
                                    }
                                    //判断子目录名称
                                    childDir=msgData.substring(52,60);
                                    //请求文件路径
                                    String path="/storage/sdcard1/ChappieLauncher/" + videoDir + "/" +childDir+"/"+ videoName;
                                    Log.e("info", "请求下载的文件目录是:" + videoDir + "子目录名称:" + childDir + "视频文件名称是:" + videoName);
                                    if(fileIsExist(path)){//如果请求的文件存在
                                        new WifiAPSendFileDataThread(new DataOutputStream(ou),"/storage/sdcard1/ChappieLauncher/" + videoDir + "/" +childDir+"/"+ videoName,"G",orderTime,orderImei).start();
                                    }else {
                                        ToastUtil.showToast("请求的文件不存在");
                                        return;
                                    }

                                } else if (msgData.length()==70&&msgData.substring(26,27).equals("D")) {//wifi下删除指定文件
                                    String orderTime=msgData.substring(0,11);
                                    String orderImei=msgData.substring(11,26);
                                    String fileDir=msgData.substring(38,50);
                                    String childDir=msgData.substring(50,60);
                                    //判断目录名称
                                    if(fileDir.contains("travel")){
                                        fileDir="travel";
                                    }else if(fileDir.contains("auto")){
                                        fileDir="auto";
                                    }else if(fileDir.contains("arttificial")){
                                        fileDir="arttificial";
                                    }
                                    //判断子目录名称
                                    childDir=msgData.substring(52,60);
                                    String fileName=msgData.substring(60,msgData.length());
                                    String path="/storage/sdcard1/ChappieLauncher/" + fileDir + "/" + childDir + "/" + fileName;
                                    if(fileIsExist(path)){
                                        Log.e("info", "请求删除的文件是:" + fileDir + "--子目录名称:" + childDir + "--视频文件名称是:" + fileName);
                                        if( FileToolkit.deleteFile("/storage/sdcard1/ChappieLauncher/" + fileDir + "/" + childDir + "/" + fileName)==true){
                                            Log.e("info","删除指定文件的结果:"+FileToolkit.deleteFile("/storage/sdcard1/ChappieLauncher/" + fileDir + "/" + childDir + "/" + fileName));
                                            String data=orderTime+orderImei+"D10000000001"+1;
                                            sendLocalData2Server(data);
                                            ToastUtil.showToast("文件"+fileDir+childDir+fileName+"删除成功!");
                                        }else{
                                            String data=orderTime+orderImei+"D10000000001"+0;
                                            sendLocalData2Server(data);
                                            ToastUtil.showToast("文件" + fileDir + childDir + fileName + "删除失败!");
                                        };
                                    }else {
                                        ToastUtil.showToast("没有找到指定文件");
                                    };
                                }
                            }
                        }
                    } else {
                        Log.e("info", "客户端Socket异常");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

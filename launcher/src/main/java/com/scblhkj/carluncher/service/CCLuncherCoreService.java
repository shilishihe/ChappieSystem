package com.scblhkj.carluncher.service;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.StatFs;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Log;

import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.db.GaoDeGPSDBInterfaceImpl;
import com.scblhkj.carluncher.db.LSInstructionDBImpl;
import com.scblhkj.carluncher.domain.GPSInfo;
import com.scblhkj.carluncher.domain.LSInstructionBean;
import com.scblhkj.carluncher.domain.MeadiaInformation;
import com.scblhkj.carluncher.domain.OBDSerialBean;
import com.scblhkj.carluncher.domain.StorageCapacityBean;
import com.scblhkj.carluncher.domain.TripReportBean;
import com.scblhkj.carluncher.receiver.StartCoreServiceReceiver;
import com.scblhkj.carluncher.thread.WifiAPSendFileDataThread;
import com.scblhkj.carluncher.utils.BluetoothUtil;
import com.scblhkj.carluncher.utils.CommonUtil;
import com.scblhkj.carluncher.utils.FileToolkit;
import com.scblhkj.carluncher.utils.FileUtil;
import com.scblhkj.carluncher.utils.ScreenPowerUtil;
import com.scblhkj.carluncher.utils.SerialFunUtil;
import com.scblhkj.carluncher.utils.StorageCapacityUtil;
import com.scblhkj.carluncher.utils.ThumbnailUtil;
import com.scblhkj.carluncher.utils.ToastUtil;
import com.txznet.sdk.TXZAsrManager;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 车载桌面的核心后台服务,主要负责后台数据的采集和发送
 */
public class CCLuncherCoreService extends Service implements MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnBufferingUpdateListener,
        MediaPlayer.OnPreparedListener {
    public static final String SERIAL_UPLOAD_DATA = "SERIAL_UPLOAD_DATA";
    private static String TAG = "CCLuncherCoreService";
    private ReadDataThread readDataThread;
    private WeakReference<Socket> mSocket;
    private long heartBeatSendTime = 0L;
    private Send2ServerRecevier send2ServerRecevier;
    private GaoDeGPSDBInterfaceImpl gpsDao;
    private BroadcastReceiver cpBroadcastReceiver;
    private LSInstructionDBImpl lsDao;
    private static final int SEND_CACHE = 0;
    private static final int CHECK_SERVICE = 100;
    private Thread checkAvailableSpaceThread;  // 检测存储空间大小，定时删除录制视频的线程
    private Runnable checkAvailableSpaceRunnalbe; // 配合检测线程一起操作
    private StorageCapacityUtil storageCapacityUtil; // 容量获取工具类
    private StorageCapacityBean storageCapacityBean;
    private AudioManager audiomanage; // 系统音频服务
    private int cStreamMusic; // 当前的音乐音量
    private int cStreamRing; // 当前的铃声音量
    private int cStreamSystem; // 当前的系统音量
    private int cStreamAlarm; // 当前的闹铃声
    private int sdCardState;
    private boolean isConfigureServicePort;//是否配置服务器
    private boolean isShutDownAllVoice = false; // 是否关闭所有声音
    private TripReportBean tripReportBean;  // 形成报告实体类
    private GPSInfo gpsInfo;  // gps信息
    private OutputStream ou;
    private String socketConnIp;
    String fileName;
    private int socketConnPort;
    private String imei, modifyQuakeImei, modifyQuakeTime, sdCardMemorySize, dataMemorySize;
    private DataOutputStream dos = null;
    private String timeStamp, dataImei, modifyTime, ModifyQuake;//时间轴，数据IMEI号，数据采集时间间隔设置，震动告警参数设置
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private List<String> fileList;
    private StringBuffer fileBuffer;
    private String[] messageArray, firstMessagInfo;
    private String mainHost = "120.27.142.237";//主IP
    private String assHost = "chappieio.scblhkj.com";
    private String serviceData, serviceImei, servicetime, servicePackageName, modifyQuakeCrame, isSendQuakePic, isSendQuakeVid, modifyQuakeVCrame;
    private String isSendFile;
    //构造的多媒体对象
    private MeadiaInformation infor;
    private boolean isStartNewApp;//是否配置设置开机自动启动的APP
    private boolean isSettingQuakeInfo, isSettingSpeedInfo;//是否配置震动信息，急减速信息
    private double speedQuakeValue;//发生震动时的震动值
    private String isSendSpeedPic, isSendSpeedVid;
    private int limteValue;
    private String packageTime, packageImei, packageName;
    private ArrayList<String[]> messageList;//接收指令的集合
    private boolean isFinishSocketOrder, isFristConnectSocket;//socket指令是否执行完毕，是否第一次连接socket
    private boolean isRealTimeRecorder;//当前状态是否是实时查看
    private int realTimeCramer;//实时视频被其他指令打断时的摄像头 0:主摄像头 1:副摄像头 2:没有实时视频查看
    private boolean isInterruptRealTime;
    private OkHttpClient okHttpClient;
    private String downLoadFileName;//请求下载的Apk名称
    private String downLoadImei, downLoadTime;
    private InputStream downLoadIs;//下载Apk用的输入流
    private FileOutputStream downLoadOus;//下载Apk用的输出流
    private boolean isTalkEachOther;//是否开启对讲功能
    private StringBuilder sb;
    private FileOutputStream fos;
    private int sendRoomNum;//发送语音对讲的房号
    private int roomNum;//设置语音对讲的房号
    private MediaPlayer mediaPlayer;//音频播放器
    private String voiceImei;
    private HashMap<String, String> map;
    private String dirName, dirNameCover, fileImei, fileTime;
    private ArrayList<String> fileNumList;//文件数
    private int fileNumListSize, fileDirNumListSize;
    private StringBuffer stringBuffer1;
    private boolean isSetSystemSetting;//系统设置权限是否打开
    private int timeValue;//修改的急减速时间值和速度值
    private int speedValue;
    private int setSystemSetting = 0;//系统设置开关默认为0 关闭
    private String dataPackageName, dataMainHost, dataAssHost, dataFfmpeg_link, dataModifyTime, dataModifyQuakeCrame, dataIsSendQuakePic, dataModifyQuakeVCrame, dataIsSendQuakeVid, dataModifyQuake, dataSpeedCramer, dataIsSendSpeedPic, dataSpeedVCramer, dataIsSendSpeedVid;
    private MediaPlayer player;
    private boolean isDownLoadVoice;//是否在下载声音文件
    private boolean isModifySpeed;//是否配置过急减速

    private static final int longServerErrorCode = -1024;   // 长连接发送数据失败标识
    /**
     * 发送数据库中的指令到服务器
     */
    private Runnable sendCacheDataRunnable = new Runnable() {
        @Override
        public void run() {
            if (System.currentTimeMillis() - heartBeatSendTime >= ChappieConstant.DATA_BEAT_TATE) {
                sendCacheData2Server();
            }
        }
    };


    private Handler dataHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case longServerErrorCode: {
                    restartScocketThread();
                    break;
                }
                case SEND_CACHE: {
                    modifyTime = sp.getString("modifyTime", "");
                    if (!modifyTime.equals("0")) { //如果时间不为0，才发送缓存数据
                        sendCacheData2Server();
                    }
                    if (!TextUtils.isEmpty(modifyTime)) {//如果客户端设置了时间
                        dataHandler.sendEmptyMessageDelayed(SEND_CACHE, Integer.parseInt(modifyTime) * 1000);
                    } else {//如果客户端没有设置时间，默认为5秒
                        dataHandler.sendEmptyMessageDelayed(SEND_CACHE, ChappieConstant.DATA_CACHE_TATE);
                    }
                    break;
                }
                case 0x08: {
                    ToastUtil.showToast("已连上服务器");
                    break;
                }
                case 0x09: {
                    ToastUtil.showToast("与服务器断开");
                    break;
                }

                case 110: {
                    sendLocalData2Server(serviceData);
                    ToastUtil.showToast("收到配置通信服务器请求,已将信息返回客户端");
                    break;
                }

                case 111: {
                    ToastUtil.showToast("配置服务器失败，地址不能为空!");
                    break;
                }

                case 112: {
                    dataHandler.sendEmptyMessageDelayed(SEND_CACHE, ChappieConstant.DATA_CACHE_TATE);
                    ToastUtil.showToast("采集数据时间间隔为0秒设置成功，将不再发送采集数据");
                    break;
                }

                case 113: {
                    dataHandler.sendEmptyMessageDelayed(SEND_CACHE, Integer.parseInt(modifyTime) * 1000);
                    ToastUtil.showToast("采集数据时间间隔修改成功，间隔时间为" + modifyTime + "秒");
                    break;
                }
                case 114: {
                    ToastUtil.showToast("震动告警参数修改失败，震动设置不能为0");
                    break;
                }
                case 115: {
                    ToastUtil.showToast("震动告警参数修改失败,震动值设置不能大于100");
                    break;
                }
                case 116: {

                    ToastUtil.showToast("设置的参数不能为0");
                    break;
                }
                case 117: {
                    ToastUtil.showToast("设置的参数只能是整数");
                    break;
                }
                case 118: {
                    ToastUtil.showToast("急减速参数设置成功");
                    isModifySpeed = true;
                    editor.putBoolean("isModifySpeed", isModifySpeed);
                    editor.commit();
                    break;
                }
                case 119: {
                    ToastUtil.showToast("已和服务器断开连接");
                    break;
                }
                case 120: {
                    ToastUtil.showToast("请求的文件不存在");
                    break;
                }
                case 121: {
                    ToastUtil.showToast("重新连接服务器");
                    break;
                }
                case 122: {
                    ToastUtil.showToast("删除文件成功");
                    break;
                }
                case 123: {
                    ToastUtil.showToast("删除文件失败");
                    break;
                }
                case 124: {
                    ToastUtil.showToast("间隔时间不能为空,修改失败");
                    break;
                }
                case 125: {
                    ToastUtil.showToast("包名为空,修改失败!");
                    break;
                }
                case 126: {
                    ToastUtil.showToast("包名不存在或包名有误,修改失败!");
                    break;
                }
                case 127: {
                    ToastUtil.showToast("与客户端连接成功!");
                    updateSocketListItem();
                    break;
                }
                case 128: {
                    ToastUtil.showToast("没有与客户端连接!");
                    updateSocketListItem();
                    break;
                }
                case 129: {
                    ToastUtil.showToast("已成功配置视频流服务器!");
                    updateSocketListItem();
                    break;
                }
                case 130: {
                    ToastUtil.showToast("清除指令有误,清除指令失败!");
                    updateSocketListItem();
                    break;
                }
                case 131: {
                    ToastUtil.showToast("清除缓存成功!");
                    updateSocketListItem();
                    break;
                }
                case 132: {
                    ToastUtil.showToast("文件下载成功");
                    File file = new File("/storage/sdcard1/downloaddata/" + downLoadFileName);
                    if (file != null) {
                        if (!isDownLoadVoice) {
                            Log.e("info", "你进入了非isDownLoadVoice");
                            openFile(file);
                        } else {
                            Log.e("info", "你进入了isDownLoadVoice");
                            player = new MediaPlayer();
                            try {
                                player.setDataSource("/storage/sdcard1/downloaddata/" + downLoadFileName);
                                player.prepare();
                                player.start();
                                isDownLoadVoice = false;
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }

                    }
                    updateSocketListItem();
                    break;
                }
                case 133: {
                    Log.e("info", "已下载:" + msg.obj + "%");
                    break;
                }
                case 134: {
                    Log.e("info", "下载失败");
                    ToastUtil.showToast("下载失败");
                    String data = "@" + downLoadTime + "," + downLoadImei + "I,1,0";
                    sendLocalData2Server(data);
                    updateSocketListItem();
                    break;
                }
                case 135: {
                    ToastUtil.showToast("对讲功能已开启!");
                    break;
                }
                case 136: {
                    ToastUtil.showToast("对讲功能已关闭!");
                    break;
                }
                case 137: {
                    ToastUtil.showToast("抱歉,你还没开通语音功能!");
                    break;
                }
                case 138: {
                    Bundle b = msg.getData();
                    String voicePath = b.getString("voiceUrl");
                    isDownLoadVoice = true;
                    downloadFile(voicePath);
                    Log.e("info", "要播放的语音是:" + voicePath);

                    break;
                }
                case 139: {
                    ToastUtil.showToast("抱歉,语音文件发送失败!");
                    break;
                }
                case 140: {
                    ToastUtil.showToast("没有检测到SD卡,语音文件接收失败!");
                    break;
                }
                case 141: {
                    ToastUtil.showToast("语音接收失败！");
                    break;
                }
                case 142: {
                    ToastUtil.showToast("已关闭系统设置权限！");
                    break;
                }
                case 143: {
                    ToastUtil.showToast("已打开系统设置权限！");
                    break;
                }
                case 144: {
                    ToastUtil.showToast("系统设置权限指令错误,设置失败！");
                    break;
                }
                default: {
                    break;
                }
            }
            super.handleMessage(msg);
        }
    };


    /**
     * 发送缓冲区的数据给服务器
     */

    private void sendCacheData2Server() {
        String data = "";
        LSInstructionBean tempBean = lsDao.getFirstRecord();
        if (tempBean != null && !TextUtils.isEmpty(tempBean.getInstruction())) {
            String caheTimeStamp = String.valueOf(tempBean.getTimeStamp());
            String caheInstruction = tempBean.getInstruction();
            int speed = tempBean.getSpeed();
            caheTimeStamp = caheTimeStamp.substring(1);//时间戳
            String chheImei = caheInstruction.substring(1, 16);//IMEI
            String lon = caheInstruction.substring(27, 33);//经度
            String lon1 = lon.substring(0, 2);
            String lon2 = lon.substring(2);
            lon = lon1 + "." + lon2;
            String lat = caheInstruction.substring(33, 40);//纬度
            String lat1 = lat.substring(0, 3);
            String lat2 = lat.substring(3);
            lat = lat1 + "." + lat2;
            Log.e("info", "缓存中的数据:" + caheInstruction);
            if (sdCardState == 00) {//如果SD卡正常的情况下，获取SD卡的内存状态
                File sdCardFileDir = Environment.getExternalStorageDirectory();
                String sdCardMemory = getMemoryInfo(sdCardFileDir);
                sdCardMemorySize = sdCardMemory;
            }
            // 获得手机内部存储控件的状态
            File dataFileDir = Environment.getDataDirectory();
            String dataMemory = getMemoryInfo(dataFileDir);
            dataMemorySize = dataMemory;
            //最后发送的数据
            data = "@" + caheTimeStamp + "," + chheImei + ",U,1," + lat + "," + lon + ",14423," + speed + ",10,00,08," + "0" + sdCardState + ",00,00,00," + dataMemorySize + "," + sdCardMemorySize;
        } else {
            data = "heart data";
        }
        boolean ok = sendLocalData2Server(data);
        Log.e("info", "发送给服务器的缓存指令是:" + data + "字符串长度:" + data.length());
        if (ok) {  // 发送成功
            lsDao.deleteRecord(tempBean);
        } else {  // 发送失败重连
            dataHandler.sendEmptyMessageDelayed(longServerErrorCode, 1000);
        }
    }


    private void unRegisterCpBoradCastReceiver() {
        if (cpBroadcastReceiver != null) {
            unregisterReceiver(cpBroadcastReceiver);
            Log.e("info", "=========================Cpluncher广播接收者已注销=============================");
        }
    }

    private void registerCpBroadcasterReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ChappieConstant.SEND_CAR_CMD_NET_PIC);//远程控制拍照
        intentFilter.addAction(ChappieConstant.FINISH_NET_VIDEO_FILE);//远程控制摄像
        intentFilter.addAction(ChappieConstant.SEND_MODIFY_SPEED_SUCCESS);
        intentFilter.addAction(ChappieConstant.SEND_MODIFY_QUAKE_SUCCESS);//震动值修改成功
        intentFilter.addAction(ChappieConstant.SEND_MODIFY_QUAKE_FAIL);//震动值修改失败
        intentFilter.addAction(ChappieConstant.SEND_CAR_COLLISION_VIDEO);//震动视频
        intentFilter.addAction(ChappieConstant.SEND_CAR_COLLISION_PIC);//震动图片
        intentFilter.addAction(ChappieConstant.SEND_CAR_LIMITED_VID);//急减速视频
        intentFilter.addAction(ChappieConstant.SEND_CAR_LIMITED_PIC);//急减速图片
        intentFilter.addAction(ChappieConstant.OFF_FOR_REALTIME_SUCCESS);//关闭实时视频查看成功返回
        intentFilter.addAction(ChappieConstant.FOR_REALTIME_SUCCESS);//打开实时视频查看成功返回
        intentFilter.addAction(ChappieConstant.SEND_SDCARD_STATE);//发送SD卡状态
        intentFilter.addAction(SerialDataCommSerivce.UPLOAD_DATA_2_SERVICE_ACTION);
        intentFilter.addAction(ChappieConstant.UPLOAD_CACHE_DATA_2_SERVER_ACTION);
        intentFilter.addAction(ChappieConstant.ACTION_ACC_INSERT); // ACC上电
        intentFilter.addAction(ChappieConstant.ACTION_ACC_REMOVE); // ACC下电
        intentFilter.addAction(ChappieConstant.ACTION_INTENT_GSENSON_F5_EVENT);  // 向右手势
        intentFilter.addAction(ChappieConstant.ACTION_INTENT_GSENSON_F6_EVENT);  // 向左手势
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);  // 锁屏
        intentFilter.addAction(Intent.ACTION_SCREEN_ON);   // 开屏
        intentFilter.addAction(ChappieConstant.GPS_INFO);  // GPS信息
        intentFilter.addAction(SerialDataCommSerivce.SERIAL_OBD_ACTION);   // OBD数据广播
        intentFilter.addAction(ChappieConstant.SEND_CAR_CMD_NET_PIC);//远程控制拍照
        intentFilter.addAction(ChappieConstant.SEND_MODIFY_SPEED_SUCCESS);//获取修改急减速参数设置结果
        intentFilter.addAction(ChappieConstant.SEND_START_APP_PACKAGENAME_SUCCESSS);//配置开机启动APP成功广播
        intentFilter.addAction(ChappieConstant.SEND_CLEAR_SOCKET_ORDER);//后视镜自己清空网络请求广播
        intentFilter.addAction(ChappieConstant.SEND_VIDEO_THREAD_SUC);//视频流配置成功
        intentFilter.addAction(ChappieConstant.SEND_IS_VIDEO_THREAD_STATE);//判断当前是否在实时视频查看
        intentFilter.addAction(ChappieConstant.SEND_VOICEFILE_PATH);//发送录音文件的路径
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(cpBroadcastReceiver, intentFilter);
    }

    private void cpBroadCastReceiver() {
        cpBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String cpAction = intent.getAction();
                switch (cpAction) {
                    case ChappieConstant.SEND_VOICEFILE_PATH: { //接收到音频文件路径的广播
                        sendRoomNum = sp.getInt("roomNum", roomNum);
                        String path = intent.getStringExtra("voiceFilePath");
                        Log.e("info", "cpService接收到音频文件路径" + path + ",房号:" + sendRoomNum);
                        File file = new File(path);
                        String time = getTimeStamp();
                        if (ChappieCarApplication.DEVICE_IMEI != null) {
                            voiceImei = ChappieCarApplication.DEVICE_IMEI;
                        } else {
                            TelephonyManager tm = ((TelephonyManager) getSystemService(TELEPHONY_SERVICE));
                            voiceImei = tm.getDeviceId();
                        }
                        Log.e("info", "voiceImei:" + voiceImei);
                        if (file.exists()) {
                            sendLocalFile2Server(file, time, voiceImei, "M" + sendRoomNum);
                        } else {
                            dataHandler.sendEmptyMessage(139);
                            return;
                        }
                        break;
                    }
                    case ChappieConstant.SEND_IS_VIDEO_THREAD_STATE: { //保存当前是否处于实时视频的状态值
                        isRealTimeRecorder = intent.getBooleanExtra("isRealTimeRecorder", isRealTimeRecorder);
                        realTimeCramer = intent.getIntExtra("realTimeCramer", realTimeCramer);
                        editor.putBoolean("isRealTimeRecorder", isRealTimeRecorder);
                        editor.putInt("realTimeCramer", realTimeCramer);
                        editor.commit();
                        break;
                    }
                    case ChappieConstant.SEND_VIDEO_THREAD_SUC: {
                        String data = "@" + intent.getStringExtra("timeStamp") + "," + intent.getStringExtra("imei") + "3,1,1";
                        sendLocalData2Server(data);
                        dataHandler.sendEmptyMessage(129);
                        break;
                    }
                    case ChappieConstant.SEND_CLEAR_SOCKET_ORDER: { //收到后视镜自己清除socket消息集合
                        if (messageList != null) {
                            messageList.clear();
                        }
                        break;
                    }
                    case ChappieConstant.SEND_SDCARD_STATE: { //SD卡状态
                        sdCardState = intent.getIntExtra("sdCardState", sdCardState);
                        Log.e("info", "cp接收到sdCardState:" + sdCardState);
                        break;
                    }
                    case ChappieConstant.SEND_START_APP_PACKAGENAME_SUCCESSS: {
                        Log.e("info", "cp接收到了配置启动app成功的广播");
                        packageTime = intent.getStringExtra("packageTimeSucc");
                        packageImei = intent.getStringExtra("packageImeiSucc");
                        packageName = intent.getStringExtra("packageNameSucc");
                        isStartNewApp = true;
                        editor.putBoolean("isStartNewApp", isStartNewApp);
                        editor.putString("packageNameSucc", packageName);
                        editor.commit();
                        String data = "@" + packageTime + "," + packageImei + "1,1,1";
                        sendLocalData2Server(data);
                        break;
                    }
                    case ChappieConstant.SEND_CAR_CMD_NET_PIC: { //网络状态下拍照
                        Log.e("info", "cp接收到了手动拍照");
                        String filePath = intent.getStringExtra("com.scblhkj.chappie.hddvr.SEND_CAR_CMD_NET_PIC_PATH");
                        timeStamp = intent.getStringExtra("timestamp");
                        dataImei = intent.getStringExtra("imei");
                        isSendFile = intent.getStringExtra("isSendFile");
                        Log.e("info", "cp接收到了手动拍照完成后发送过来的" + "isSendFile:" + isSendFile);
                        File file = new File(filePath);
                        if (isSendFile.equals("0")) {  //当isSendFile为0的时候，只传文件路径和大小
                            String data = "@" + timeStamp + "," + dataImei + ",C,1,1," + filePath + "," + file.length();
                            sendLocalData2Server(data);
                        } else if (isSendFile.equals("1")) {
                            sendLocalFile2Server(file, timeStamp, dataImei, "C");
                        } else { //失败
                            String data = "@" + timeStamp + "," + dataImei + ",C,1,0";
                            sendLocalData2Server(data);
                        }
                        break;
                    }

                    case ChappieConstant.FINISH_NET_VIDEO_FILE: {
                        Log.e("info", "cp接收到了手动录制视频结束");
                        String videoFilePath = intent.getStringExtra("com.scblhkj.chappie.FINISH_VIDEO_FILE_VALUE");   // 获取到了手动录制视频的文件路径
                        Log.e("info", "cp接收到了手动录制视频完成后发送过来的视频路径" + videoFilePath);
                        timeStamp = intent.getStringExtra("ctime");
                        dataImei = intent.getStringExtra("imei");
                        isSendFile = intent.getStringExtra("isSendVideo");
                        File videoFile = new File(videoFilePath);
                        if (isSendFile.equals("0")) {
                            String data = "@" + timeStamp + "," + dataImei + ",R,1,1," + videoFilePath + "," + videoFile.length();
                            sendLocalData2Server(data);
                        } else if (isSendFile.equals("1")) {
                            sendLocalFile2Server(videoFile, timeStamp, dataImei, "R");
                        } else { //失败
                            String data = "@" + timeStamp + "," + dataImei + ",R,1,0";
                            sendLocalData2Server(data);
                        }

                        break;
                    }
                    case ChappieConstant.SEND_MODIFY_SPEED_SUCCESS: { // 急减速结果
                        Log.e("info", "cp接收到了手动急减速结果,收到急减速就修改成功的广播");
                        String realtimeImei = intent.getStringExtra("imei");
                        String realtimeStamp = intent.getStringExtra("Ctime");
                        String data = "@" + realtimeStamp + "," + realtimeImei + "7,1,1";
                        sendLocalData2Server(data);
                        dataHandler.sendEmptyMessage(118);
                        break;
                    }
                    case ChappieConstant.SEND_MODIFY_QUAKE_SUCCESS: {//震动值修改成功
                        Log.e("info", "cp接收到了手动震动结果");
                        String realtimeImei = intent.getStringExtra("modifyQuakeImei");
                        String realtimeStamp = intent.getStringExtra("modifyQuakeTime");
                        String data = "@" + realtimeStamp + "," + realtimeImei + "6,1,1";
                        sendLocalData2Server(data);
                        break;
                    }
                    case ChappieConstant.SEND_MODIFY_QUAKE_FAIL: {//震动值修改失败
                        Log.e("info", "cp接收到了手动震动结果");
                        String realtimeImei = intent.getStringExtra("modifyQuakeImei");
                        String realtimeStamp = intent.getStringExtra("modifyQuakeTime");
                        String data = "@" + realtimeStamp + "," + realtimeImei + "6,1,0";
                        sendLocalData2Server(data);
                        break;
                    }
                    case ChappieConstant.OFF_FOR_REALTIME_SUCCESS: { //关闭后视镜实时查看成功
                        String realtimeImei = intent.getStringExtra("realtimeimei");
                        String realtimeStamp = intent.getStringExtra("realtimestamp");
                        String realtimeTarget = intent.getStringExtra("realtimetarget");
                        isInterruptRealTime = intent.getBooleanExtra("isInterruptRealTime", isInterruptRealTime);
                        Log.e("info", "cp接收到的realtimeImei>>>" + realtimeImei + "---realtimeStamp" + realtimeStamp + "---realtimeTarget" + realtimeTarget);
                        if (isInterruptRealTime == false) {
                            String data = "@" + realtimeStamp + "," + realtimeImei + ",S," + "1,1";
                            Log.e("info", "data>>>>" + data);
                            sendLocalData2Server(data);
                        } else {
                            String data = "@" + realtimeStamp + "," + realtimeImei + ",S," + "1,0";
                            Log.e("info", "data>>>>" + data);
                            sendLocalData2Server(data);
                        }
                        break;
                    }

                    case ChappieConstant.FOR_REALTIME_SUCCESS: { //开启后视镜实时查看成功
                        String realtimeImei = intent.getStringExtra("realtimeimei");
                        String realtimeStamp = intent.getStringExtra("realtimestamp");
                        String realtimeTarget = intent.getStringExtra("realtimetarget");
                        Log.e("info", "cp接收到的realtimeImei>>>" + realtimeImei + "---realtimeStamp" + realtimeStamp + "---realtimeTarget" + realtimeTarget);
                        // String data=realtimeStamp+realtimeImei+"S"+realtimeTarget+"0000000001"+1;
                        String data = "@" + realtimeStamp + "," + realtimeImei + ",S," + "1,1";
                        Log.e("info", "data>>>>" + data);
                        sendLocalData2Server(data);
                        break;
                    }

                    case ChappieConstant.SEND_CAR_COLLISION_VIDEO: { //主程序接收到震动后需要发送的视频信息
                        Log.e("info", "主程序接收到震动后需要发送的视频信息");
                        isSettingQuakeInfo = intent.getBooleanExtra("isSettingQuakeInfo", isSettingQuakeInfo);
                        if (isSettingQuakeInfo) {
                            String filePath = intent.getStringExtra(ChappieConstant.SEND_CAR_COLLISION_VIDEO_PATH);
                            Log.e("info", "当配置了震动信息后,cp接收到了震动发送过来的视频路径" + filePath);
                            String realtimeImei = intent.getStringExtra("realtimeImei");
                            String realtimeStamp = intent.getStringExtra("realtimeStamp");
                            isSendQuakeVid = intent.getStringExtra("isSendQuakeVid");
                            speedQuakeValue = intent.getDoubleExtra("speedQuakeValue", speedQuakeValue);
                            File file = new File(filePath);
                            if (isSendQuakePic.equals("0")) {  //当碰撞照片不需要传文件的时候
                                String data = "@" + realtimeStamp + "," + realtimeImei + "," + "V,1,0," + speedQuakeValue + "," + filePath + "," + file.length();
                                sendLocalData2Server(data);
                                Log.e("info", "碰撞视频发送至服务器的信息:" + data);
                            } else if (isSendQuakePic.equals("1")) {//发送碰撞时的照片详细信息至服务器
                                //发送碰撞时的照片详细信息至服务器
                                sendLocalFile2Server(file, realtimeStamp, realtimeImei, "V0" + speedQuakeValue);
                            }
                        } else if (isSettingQuakeInfo == false) {
                            //默认情况下，不发生图片文件
                            String filePath = intent.getStringExtra(ChappieConstant.SEND_CAR_COLLISION_VIDEO_PATH);
                            Log.e("info", "当没有配置震动信息,cp接收到了震动发送过来的视频路径" + filePath);
                            String realtimeImei = intent.getStringExtra("realtimeImei");
                            String realtimeStamp = intent.getStringExtra("realtimeStamp");
                            speedQuakeValue = intent.getDoubleExtra("speedQuakeValue", speedQuakeValue);
                            File file = new File(filePath);
                            String data = "@" + realtimeStamp + "," + realtimeImei + "," + "V,1,0," + speedQuakeValue + "," + filePath + "," + file.length();
                            sendLocalData2Server(data);
                            Log.e("info", "碰撞视频发送至服务器的信息:" + data);
                        }
                        break;
                    }
                    case ChappieConstant.SEND_CAR_COLLISION_PIC: { //震动后发送图片至服务器
                        Log.e("info", "主程序接收到震动后需要发送的图片信息");
                        isSettingQuakeInfo = intent.getBooleanExtra("isSettingQuakeInfo", isSettingQuakeInfo);
                        if (isSettingQuakeInfo) {
                            String filePath = intent.getStringExtra(ChappieConstant.SEND_CAR_COLLISION_PIC_PATH);
                            Log.e("info", "当配置了震动信息后,cp接收到了震动发送过来的图片路径" + filePath);
                            String realtimeImei = intent.getStringExtra("realtimeImei");
                            String realtimeStamp = intent.getStringExtra("realtimeStamp");
                            isSendQuakePic = intent.getStringExtra("isSendQuakePic");
                            speedQuakeValue = intent.getDoubleExtra("speedQuakeValue", speedQuakeValue);
                            File file = new File(filePath);
                            if (isSendQuakePic.equals("0")) {  //当碰撞照片不需要传文件的时候
                                String data = "@" + realtimeStamp + "," + realtimeImei + "," + "A,1,0," + speedQuakeValue + "," + filePath + "," + file.length();
                                sendLocalData2Server(data);
                                Log.e("info", "碰撞图片发送至服务器的信息:" + data);
                            } else if (isSendQuakePic.equals("1")) {//发送碰撞时的照片详细信息至服务器
                                //发送碰撞时的照片详细信息至服务器
                                sendLocalFile2Server(file, realtimeStamp, realtimeImei, "A0" + speedQuakeValue);
                            }
                        } else if (isSettingQuakeInfo == false) {
                            //默认情况下，不发生图片文件
                            String filePath = intent.getStringExtra(ChappieConstant.SEND_CAR_COLLISION_PIC_PATH);
                            Log.e("info", "当没有配置震动信息,cp接收到了震动发送过来的图片路径" + filePath);
                            String realtimeImei = intent.getStringExtra("realtimeImei");
                            String realtimeStamp = intent.getStringExtra("realtimeStamp");
                            speedQuakeValue = intent.getDoubleExtra("speedQuakeValue", speedQuakeValue);
                            File file = new File(filePath);
                            String data = "@" + realtimeStamp + "," + realtimeImei + "," + "A,1,0," + speedQuakeValue + "," + filePath + "," + file.length();
                            sendLocalData2Server(data);
                            Log.e("info", "碰撞图片发送至服务器的信息:" + data);
                        }
                        break;
                    }
                    case ChappieConstant.SEND_CAR_LIMITED_VID: { //主程序接收到急减速后需要发送的视频信息
                        Log.e("info", "主程序接收到急减速后需要发送的视频信息");
                        isSettingSpeedInfo = intent.getBooleanExtra("isSettingSpeedInfo", isSettingSpeedInfo);
                        if (isSettingSpeedInfo) {
                            String filePath = intent.getStringExtra(ChappieConstant.SEND_CAR_LIMITED_VID_PATH);
                            Log.e("info", "当配置了急减速信息后,cp接收到了急减速发送过来的视频路径" + filePath);
                            String realtimeImei = intent.getStringExtra("realtimeImei");
                            String realtimeStamp = intent.getStringExtra("realtimeStamp");
                            isSendSpeedVid = intent.getStringExtra("isSendSpeedVid");
                            limteValue = intent.getIntExtra("limteValue", limteValue);
                            File file = new File(filePath);
                            if (isSendSpeedVid.equals("0")) {  //当碰撞照片不需要传文件的时候
                                String data = "@" + realtimeStamp + "," + realtimeImei + "," + "V,1,1," + limteValue + "," + filePath + "," + file.length();
                                sendLocalData2Server(data);
                                Log.e("info", "急减速视频发送至服务器的信息:" + data);
                            } else if (isSendSpeedVid.equals("1")) {//发送碰撞时的照片详细信息至服务器
                                //发送碰撞时的照片详细信息至服务器
                                sendLocalFile2Server(file, realtimeStamp, realtimeImei, "V1" + speedQuakeValue);
                            }
                        } else if (isSettingQuakeInfo == false) {
                            //默认情况下，不发送视频文件
                            String filePath = intent.getStringExtra(ChappieConstant.SEND_CAR_LIMITED_VID_PATH);
                            Log.e("info", "当配置了急减速信息后,cp接收到了急减速发送过来的视频路径" + filePath);
                            String realtimeImei = intent.getStringExtra("realtimeImei");
                            String realtimeStamp = intent.getStringExtra("realtimeStamp");
                            limteValue = intent.getIntExtra("limteValue", limteValue);
                            File file = new File(filePath);
                            String data = "@" + realtimeStamp + "," + realtimeImei + "," + "V,1,1," + limteValue + "," + filePath + "," + file.length();
                            sendLocalData2Server(data);
                            Log.e("info", "急减速视频发送至服务器的信息:" + data);
                        }
                        break;
                    }
                    case ChappieConstant.SEND_CAR_LIMITED_PIC: { //急减速后发送图片至服务器
                        Log.e("info", "主程序接收到震动后需要发送的图片信息");
                        isSettingSpeedInfo = intent.getBooleanExtra("isSettingSpeedInfo", isSettingSpeedInfo);
                        if (isSettingSpeedInfo) {
                            String filePath = intent.getStringExtra(ChappieConstant.SEND_CAR_LIMITED_PIC_PATH);
                            Log.e("info", "当配置了急减速信息后,cp接收到了急减速发送过来的图片路径" + filePath);
                            String realtimeImei = intent.getStringExtra("realtimeImei");
                            String realtimeStamp = intent.getStringExtra("realtimeStamp");
                            isSendSpeedPic = intent.getStringExtra("isSendSpeedPic");
                            limteValue = intent.getIntExtra("limteValue", limteValue);
                            File file = new File(filePath);
                            if (isSendSpeedPic.equals("0")) {  //当碰撞照片不需要传文件的时候
                                String data = "@" + realtimeStamp + "," + realtimeImei + "," + "A,1,1," + limteValue + "," + filePath + "," + file.length();
                                sendLocalData2Server(data);
                                Log.e("info", "急减速图片发送至服务器的信息:" + data);
                            } else if (isSendSpeedPic.equals("1")) {//发送碰撞时的照片详细信息至服务器
                                //发送碰撞时的照片详细信息至服务器
                                sendLocalFile2Server(file, realtimeStamp, realtimeImei, "A1" + limteValue);
                            }
                        } else if (isSettingQuakeInfo == false) {
                            //默认情况下，不发生图片文件
                            String filePath = intent.getStringExtra(ChappieConstant.SEND_CAR_LIMITED_PIC_PATH);
                            Log.e("info", "当配置了急减速信息后,cp接收到了急减速发送过来的图片路径" + filePath);
                            String realtimeImei = intent.getStringExtra("realtimeImei");
                            String realtimeStamp = intent.getStringExtra("realtimeStamp");
                            limteValue = intent.getIntExtra("limteValue", limteValue);
                            File file = new File(filePath);
                            String data = "@" + realtimeStamp + "," + realtimeImei + "," + "A,1,1," + limteValue + "," + filePath + "," + file.length();
                            sendLocalData2Server(data);
                            Log.e("info", "急减速图片发送至服务器的信息:" + data);
                        }
                        break;
                    }
                    case SerialDataCommSerivce.UPLOAD_DATA_2_SERVICE_ACTION: {   // 收到此广播直接发送数据到服务器
                        String receiveData = intent.getCharSequenceExtra(CCLuncherCoreService.SERIAL_UPLOAD_DATA).toString();
                        if (ChappieCarApplication.isDebug)
                            Log.e("info", "++收到串口回传的数据" + receiveData);
                        if (!TextUtils.isEmpty(receiveData)) {
                            boolean success = sendLocalData2Server(receiveData);
                            if (!success) {
                                dataHandler.sendEmptyMessageDelayed(longServerErrorCode, 1000);
                            }
                        }
                        break;
                    }
                    case ChappieConstant.UPLOAD_CACHE_DATA_2_SERVER_ACTION: {
                        sendCacheData2Server();
                        break;
                    }
                    case ChappieConstant.ACTION_ACC_INSERT: {   // ACC上电广播接收
                        Log.e("info", "ACC上电");
                        // 从待机模式中唤醒到正常模式
                        // 休眠模式标识重置
                        ChappieCarApplication.isHibernation = false; // 标识置为休眠
                        storageCapacityBean = storageCapacityUtil.getStorageCapacityBean();
                        tripReportBean = new TripReportBean();
                        if (gpsInfo != null) {
                            tripReportBean.setStartLocation(gpsInfo.getAddress());  // 启动位置
                        }
                        tripReportBean.setStartTime(CommonUtil.getCarTime());   // 启动时间
                        break;
                    }
                    case ChappieConstant.ACTION_ACC_REMOVE: {   // ACC下电广播接收
                        tripReportBean.setEndTime(CommonUtil.getCarTime());
                        tripReportBean.setEndLocation(gpsInfo.getAddress());
                        //
                        Log.e("info", "ACC下电，停止GPS采集");
                        // 从工作模式到休眠模式
                        stopRouteRecordService(); // 关闭定位 关闭GPS
                        // 关闭蓝牙
                        if (BluetoothUtil.isBluetoothIsOpen()) {
                            Intent mIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            startActivity(mIntent);
                        }
                        // 减少视频录制帧数
                        // 休眠模式标识重置
                        ChappieCarApplication.isHibernation = true; // 标识置为休眠
                        break;
                    }
                    case ChappieConstant.ACTION_INTENT_GSENSON_F5_EVENT: {  // 向右手势(点亮屏幕)
                        // 点亮屏幕
                        //  ScreenPowerUtil.doScreenOn();
                        TXZAsrManager.getInstance().cancel();
                        break;
                    }
                    case ChappieConstant.ACTION_INTENT_GSENSON_F6_EVENT: {  // 向左手势(熄灭屏幕)
                        //ScreenPowerUtil.doScreenOff();
                        TXZAsrManager.getInstance().start("请问有什么可以帮您?");
                        break;
                    }
                    case Intent.ACTION_SCREEN_ON: {  // 开屏幕
                        break;
                    }
                    case Intent.ACTION_SCREEN_OFF: {  // 锁屏
                        if (ScreenPowerUtil.isHeld()) {
                            ScreenPowerUtil.offHeld();
                        }
                        break;
                    }
                    case ChappieConstant.GPS_INFO: {  // 定位信息
                        gpsInfo = (GPSInfo) intent.getSerializableExtra(ChappieConstant.GPS_INFO);
                        break;
                    }
                    case SerialDataCommSerivce.SERIAL_OBD_ACTION: {  // OBD广播
                        OBDSerialBean bean = (OBDSerialBean) intent.getSerializableExtra(SerialDataCommSerivce.SerialHelperControl.SERIAL_OBD_DATA);  // OBD数据
                        int[][] datass = bean.getDatass();
                        // 解析datass
                        break;
                    }
                    default: {
                        Log.e("info", "没有注册此类广播");
                        break;
                    }
                }
            }
        };
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.i(TAG, "核心服务中的onCreate方法");
        storageCapacityUtil = new StorageCapacityUtil(this);
        lsDao = new LSInstructionDBImpl(this);
        cpBroadCastReceiver();
        okHttpClient = new OkHttpClient();//初始化okhttp
        registerCpBroadcasterReceiver();
        gpsDao = new GaoDeGPSDBInterfaceImpl(this);
        new InitSocketThread().start();
        regiestSend2ServiceReceive();
        startSerialCommService();
        sp = getSharedPreferences("deskstate", Context.MODE_PRIVATE);
        editor = sp.edit();
        imei = sp.getString("IMEI", "");
        player = new MediaPlayer();
        player.setOnCompletionListener(this);
        player.setOnErrorListener(this);
        player.setOnBufferingUpdateListener(this);
        startRouteRecordService();
        startWifiAPService();//启动wifi热点下远程拍照和视频的服务
        sendBroacast2EnableSensortek();
        fileList = new ArrayList<String>();
        fileNumList = new ArrayList<String>();
        super.onCreate();
    }


    /**
     * wifi热点服务
     */
    private void startWifiAPService() {
        Intent intent = new Intent(this, ChappieWifiHotService.class);
        startService(intent);
    }

    /**
     * 启动行车GPS记录服务
     */
    private void startRouteRecordService() {
        Intent intent = new Intent();
        intent.setAction("com.scblhkj.carluncher.service.RouteRecordService");
        intent.setPackage("com.scblhkj.carluncher");  //Android5.0后service不能采用隐式启动，故此处加上包名
        startService(intent);
        Log.e("info", "启动了行车GPS记录服务");
    }

    /**
     * 关闭行车GPS记录服务
     */
    private void stopRouteRecordService() {
        Intent intent = new Intent();
        intent.setAction("com.scblhkj.carluncher.service.RouteRecordService");
        intent.setPackage("com.scblhkj.carluncher");  //Android5.0后service不能采用隐式启动，故此处加上包名
        stopService(intent);
    }


    /**
     * 启动串口通信服务
     */
    private void startSerialCommService() {
        Intent intent = new Intent(this, SerialDataCommSerivce.class);
        this.startService(intent);
        Log.e(TAG, "在核心服务中启动串口通信服务");
    }


    /**
     * 代码注册广播核心服务的广播接收者
     */
    private void regiestSend2ServiceReceive() {
        send2ServerRecevier = new Send2ServerRecevier();
        IntentFilter filter = new IntentFilter("com.chappie.alarm.action");
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(send2ServerRecevier, filter);
    }

    /**
     * 注销广播核心服务的广播接收者
     */
    private void unRegiestSend2ServiceReceive() {
        unregisterReceiver(send2ServerRecevier);
        send2ServerRecevier = null;
    }

    @Override
    public int onStartCommand(Intent i, int flags, int startId) {
        return super.onStartCommand(i, flags, startId);
    }

    @Override
    public void onDestroy() {
        stopForeground(true);
        // PhoneReceiver电话广播接收者
        Intent intent = new Intent(this, StartCoreServiceReceiver.class);
        intent.setAction("com.chappie.relodservice");
        // 发送
        sendBroadcast(intent);
        Intent protectintent = new Intent(this, StartCoreServiceReceiver.class);
        // 保护启动服务
        this.startService(protectintent);
        // 释放Socket
        releaseLastSocket(mSocket);
        // 注销广播接受者
        unRegiestSend2ServiceReceive();
        unRegisterCpBoradCastReceiver();
        super.onDestroy();
    }

    @Override
    public boolean stopService(Intent name) {
        Intent intent = new Intent(this, StartCoreServiceReceiver.class);
        intent.setAction("com.chappie.restarservice");
        sendBroadcast(intent);
        return super.stopService(name);
    }

    /**
     * 重启长连接线程
     */
    private void restartScocketThread() {
        if (dataHandler != null && readDataThread != null && mSocket != null) {
            dataHandler.removeCallbacks(sendCacheDataRunnable);
            readDataThread.release();
            releaseLastSocket(mSocket);
        }
        new InitSocketThread().start();
        if (ChappieCarApplication.isDebug)
            Log.e("info", "重启长连接socket服务");
        dataHandler.sendEmptyMessage(121);
    }


    private OutputStream os;  // 输出流

    /**
     * 发送数据到服务器上
     *
     * @param data
     * @return
     * @ todo
     */
    public boolean sendLocalData2Server(String data) {
        if (null == mSocket || null == mSocket.get()) {
            ToastUtil.showToast("没有连接上服务器，发送失败!");
            return false;
        }
        Socket soc = mSocket.get();

        try {
            if (!soc.isClosed() && !soc.isOutputShutdown()) {
                os = soc.getOutputStream();
                os.write(data.getBytes());
                os.flush();
                Log.e("info", "发送到服务器的Data>>" + data);
                heartBeatSendTime = System.currentTimeMillis();//每次发送完数据，就改一下最后成功发送的时间，节省心跳间隔时间
                updateSocketListItem();
            } else {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 发送文件到服务器上
     *
     * @return
     */
    public boolean sendLocalFile2Server(File file, String timeStamp, String dataImei, String cmd) {
        if (null == mSocket || null == mSocket.get()) {
            ToastUtil.showToast("没有连接上服务器，发送失败!");
            return false;
        }
        Socket soc = mSocket.get();
        try {
            if (!soc.isClosed() && !soc.isOutputShutdown()) {
                Timer startTimer = new Timer();
                TimerTask startTask = new TimerTask() {
                    @Override
                    public void run() {
                        releaseLastSocket(mSocket);
                        new InitSocketThread().start();
                    }
                };
                startTimer.schedule(startTask, 20 * 60 * 1000);
                new WifiAPSendFileDataThread(new DataOutputStream(soc.getOutputStream()), file.getPath(), cmd, timeStamp, dataImei).start();
                heartBeatSendTime = System.currentTimeMillis();//每次发送完数据，就改一下最后成功发送的时间，节省心跳间隔时间
                updateSocketListItem();
                if (isFinishSocketOrder == true) {
                    startTimer.cancel();
                }
            } else {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 释放最后的socket连接
     *
     * @param mSocket
     */
    private void releaseLastSocket(WeakReference<Socket> mSocket) {
        try {
            if (null != mSocket) {
                Socket sk = mSocket.get();
                if (sk != null) {
                    if (!sk.isClosed()) {
                        sk.close();
                        dataHandler.sendEmptyMessage(119);
                    }
                    sk = null;
                    mSocket = null;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 初始化长连接Socket
     */
    private void initSocket() {
        try {
            //初始化Socket
            isConfigureServicePort = sp.getBoolean("isConfigureServicePort", isConfigureServicePort);
            if (isConfigureServicePort) {//如果配置了服务器的话就使用配置服务器的地址和端口
                Log.e("info", "进入了已经配置服务器信息的步骤");
                mainHost = sp.getString("mainHost", "");
                Log.e("info", "进入了已经配置服务器信息的步骤" + mainHost);
                Socket so = new Socket(mainHost, 6688);
                mSocket = new WeakReference<Socket>(so);
                readDataThread = new ReadDataThread(so);
                readDataThread.start();
            } else {//如果没有配置服务器的话就使用查派的服务器地址和端口
                Socket so = new Socket(mainHost, ChappieConstant.SOCKET_CONN_PORT);
                Log.e("info", "进入了默认服务器信息的步骤" + mainHost);
                mSocket = new WeakReference<Socket>(so);
                readDataThread = new ReadDataThread(so);
                readDataThread.start();
            }
            dataHandler.postDelayed(sendCacheDataRunnable, ChappieConstant.DATA_BEAT_TATE); //初始化成功后，就准备发送数据包
            modifyTime = sp.getString("modifyTime", "");
            if (modifyTime == null) {
                dataHandler.sendEmptyMessageDelayed(SEND_CACHE, ChappieConstant.DATA_CACHE_TATE);
            } else if (modifyTime.equals("0")) {
                dataHandler.sendEmptyMessageDelayed(SEND_CACHE, ChappieConstant.DATA_CACHE_TATE);
            } else {
                dataHandler.sendEmptyMessageDelayed(SEND_CACHE, Integer.parseInt(modifyTime) * 1000);
            }
            Log.e("info", "*******长连接Socket连接成功initSocket方法执行*******");
        } catch (Exception e) {
            dataHandler.sendEmptyMessageDelayed(longServerErrorCode, 1000);
            e.printStackTrace();
        }
    }


    /***
     * 获取指定文件夹下的所有文件列表
     *
     * @param file
     */
    public void getAllFiles(File file) {
        fileBuffer = new StringBuffer();
        File files[] = file.listFiles();//获取file文件夹下的集合
        if (files != null) {
            for (File f : files) {
                if (!f.isDirectory()) {
                    //  Log.e("info", "每个文件的名字:" + f.toString());
                    if (f.toString().contains("travel")) {
                        dirName = f.toString().substring(40, 48);
                    } else if (f.toString().contains("auto")) {
                        dirName = f.toString().substring(38, 46);
                    } else if (f.toString().contains("arttificial")) {
                        dirName = f.toString().substring(45, 53);
                    }
                    fileNumList.add(f.toString());
                    fileNumListSize = fileNumList.size();
                    fileBuffer.append(dirName + f.getName() + ";");
                } else { //如果path表示的是一个目录则返回true
                    getAllFiles(f);
                    Log.e("info", "如果是文件夹:" + f);
                    dirNameCover = "(" + f.getName() + ")" + fileBuffer;
                }
            }
            if (dirNameCover != null) {
                fileList.add(dirNameCover);
                Log.e("info", "fileNumList>>" + fileNumList.size());
                Log.e("info", "dirNameCover" + dirNameCover);
                Log.e("info", "fileList.size()" + fileList.size());
            }
        } else {
            ToastUtil.showToast("该文件夹下没有文件!");
            return;

        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        player.start();
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        Log.e("info", "正在接收录音:" + percent + "%");
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        ToastUtil.showToast("收到录音");


    }

    /**
     * 初始化长连接Socket线程
     */
    class InitSocketThread extends Thread {
        @Override
        public void run() {
            initSocket();
            super.run();
        }
    }

    /***
     * 判断文件是否存在
     *
     * @param filePath
     * @return
     */
    public boolean fileIsExist(String filePath) {
        try {
            File f = new File(filePath);
            if (!f.exists()) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /***
     * 更新SockteList的数据和执行状态码
     */
    private void updateSocketListItem() {
        if (messageList.size() > 0) {
            messageList.remove(0);
        }
        Log.e("info", "执行完socket请求后，messageList：》》》》" + messageList.size());
        isFinishSocketOrder = true;
        Log.e("info", "已经执行完毕,可以执行下一个指令!");
    }

    //获取当前的时间戳
    private String getTimeStamp() {
        long time = System.currentTimeMillis() / 1000;
        String timeStamp = String.valueOf(time);
        return timeStamp;
    }

    /**
     * socket读取数据
     */
    class ReadDataThread extends Thread {

        private WeakReference<Socket> mWeakSocket;
        private boolean isStart = true;

        public ReadDataThread(Socket socket) {
            mWeakSocket = new WeakReference<Socket>(socket);
        }

        public void release() {
            isStart = false;
            releaseLastSocket(mWeakSocket);
        }

        private List<Byte> tempSendBuffer = new ArrayList<Byte>();

        @Override
        public void run() {
            super.run();
            Socket socket = mWeakSocket.get();
            if (null != socket && !socket.isClosed()) {
                try {
                    ou = socket.getOutputStream();
                    dos = new DataOutputStream(ou);
                    String time = getTimeStamp();
                    TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                    String deviceImei = tm.getDeviceId();//String
                    String order = "@" + time + "," + deviceImei + ",L,1";
                    ou.write(order.getBytes("UTF-8"));
                    ou.flush();
                    isFristConnectSocket = true;
                    Log.e("info", "发给服务器的数据 ：" + order);
                    dataHandler.sendEmptyMessage(0x08);
                    InputStream is = socket.getInputStream();
                    messageList = new ArrayList<String[]>();//接收到的信息列表
                    Log.e("info", "创建arraylist时 ：" + messageList.size());
                    sb = new StringBuilder();
                    byte[] buffer = new byte[1024 * 4];
                    int length = 0;
                    while (!socket.isClosed() && !socket.isInputShutdown() && isStart && ((length = is.read(buffer)) != -1)) {
                        if (length > 0) {
                            String info = new String(buffer, 0, length, "UTF-8");
                            final String message = new String(Arrays.copyOf(buffer, length));
                            Log.e("info", "从服务器获取的指令是：" + message);
                            if (message.contains("@")) {
                                firstMessagInfo = message.split(",");//按照逗号分离
                                messageList.add(firstMessagInfo); //将客户端请求加入消息集合里
                                Log.e("info", "添加指令arraylist时 ：" + messageList.size());
                                for (int i = 0; i < messageList.size(); i++) {
                                    String[] messsgeitem = messageList.get(i);
                                    Log.e("info", "messageList里的指令分别是:" + Arrays.toString(messsgeitem));
                                }
                                if (isFinishSocketOrder == false) { //如果没有执行完毕就等待执行完毕
                                    Log.e("info", "之前的任务未执行完毕,已将请求加入集合中");
                                    return;
                                } else if (isFinishSocketOrder == true || isFristConnectSocket == true) {
                                    isFristConnectSocket = false;//当第一次接收到通信指令时变成false
                                    if (messageList.size() > 0 && messageList.get(0) != null) {
                                        messageArray = messageList.get(0);
                                        Log.e("info", "messageList.get(0)》》》" + Arrays.toString(messageArray));
                                    }
                                }
                                if (messageArray[2].equals("P")) { //接收到服务器传来的心跳包
                                    updateSocketListItem();
                                } else if (messageArray[2].equals("L")) { //如果收到的指令是连接客户端
                                    isFinishSocketOrder = false;
                                    if (messageArray[3].equals("1")) {
                                        dataHandler.sendEmptyMessage(127);
                                    } else if (messageArray[3].equals("0")) {
                                        dataHandler.sendEmptyMessage(128);
                                    }
                                    Log.e("info", "socketList内数据有" + messageList.size() + "条");
                                } else if (messageArray[2].equals("M") && messageArray[3].equals("2")) { //配置语音对讲功能
                                    roomNum = Integer.parseInt(messageArray[4]);
                                    if (roomNum == 0) {
                                        //房号为0时关闭对讲功能
                                        isTalkEachOther = false;
                                        editor.putBoolean("isTalkEachOther", isTalkEachOther);
                                        editor.commit();
                                        dataHandler.sendEmptyMessage(136);
                                    } else {
                                        isTalkEachOther = true;
                                        editor.putBoolean("isTalkEachOther", isTalkEachOther);
                                        editor.putInt("roomNum", roomNum);//对讲的房号
                                        editor.commit();
                                        dataHandler.sendEmptyMessage(135);
                                    }

                                } else if (messageArray[2].equals("M") && messageArray[3].equals("0")) { //接收语音对讲信息
                                    if (!sp.getBoolean("isTalkEachOther", isTalkEachOther) || sp.getInt("roomNum", roomNum) == 0) { //没有配置语音信息就不执行该操作
                                        dataHandler.sendEmptyMessage(137);
                                        return;
                                    } else {
                                        Log.e("info", "配置的对讲房号是:" + sp.getInt("roomNum", roomNum));
                                        //String voiceUrl=messageArray[6];
                                        updateSocketListItem();
                                        String sendDir = messageArray[5];    //语音发送者文件夹路径
                                        String fileName = messageArray[6];     //语音文件名'

                                        if (fileName.length() > 14) {
                                            fileName = fileName.substring(0, 14);
                                            Log.e("info", "fileName:" + fileName);
                                        } else if (fileName.length() < 14) {
                                            dataHandler.sendEmptyMessage(140);
                                            return;
                                        }
                                        downLoadFileName = fileName;
                                        String voiceUrl = "http://" + ChappieConstant.VOICE_HOST + "/" + sendDir + "/" + fileName;
                                        Log.e("info", "请求的url:" + voiceUrl);
                                        Message msg = new Message();
                                        msg.what = 138;
                                        Bundle bundle = new Bundle();
                                        bundle.putString("voiceUrl", voiceUrl);
                                        msg.setData(bundle);
                                        dataHandler.sendMessage(msg);

                                    }
                                } else if (messageArray[2].equals("8")) { //配置 系统设置开关
                                    isFinishSocketOrder = false;
                                    String Cimei = messageArray[1];
                                    String Ctime = messageArray[0].substring(1);
                                    String code = messageArray[4];
                                    if (code.equals("0")) { //关闭系统设置
                                        isSetSystemSetting = false;
                                        setSystemSetting = 0;
                                        editor.putInt("setSystemSetting", setSystemSetting);
                                        editor.putBoolean("isSetSystemSetting", isSetSystemSetting);
                                        editor.commit();
                                        String data = "@" + Ctime + "," + Cimei + ",8,1,1";
                                        sendLocalData2Server(data);
                                        dataHandler.sendEmptyMessage(142);
                                    } else if (code.equals("1")) {
                                        isSetSystemSetting = true;
                                        setSystemSetting = 1;
                                        editor.putInt("setSystemSetting", setSystemSetting);
                                        editor.putBoolean("isSetSystemSetting", isSetSystemSetting);
                                        editor.commit();
                                        String data = "@" + Ctime + "," + Cimei + ",8,1,1";
                                        sendLocalData2Server(data);
                                        dataHandler.sendEmptyMessage(143);
                                    } else {
                                        String data = "@" + Ctime + "," + Cimei + ",8,1,0";
                                        sendLocalData2Server(data);
                                        dataHandler.sendEmptyMessage(144);
                                    }
                                } else if (messageArray[2].equals("Q")) { //请求配置信息
                                    Log.e("info", "进入外网状态下请求配置信息！");
                                    String Cimei = messageArray[1];
                                    String Ctime = messageArray[0].substring(1);
                                    String code = messageArray[4];
                                    if (code.equals("1")) { //请求查看启动包
                                        Log.e("info", "客户端请求查看启动包servicePackageName");
                                        String packageName = sp.getString("servicePackageName", "");
                                        if (TextUtils.isEmpty(packageName)) {
                                            String data = "@" + Ctime + "," + Cimei + ",Q,1,";
                                            sendLocalData2Server(data);
                                            Log.e("info", "配置信息发送的data:" + data);
                                        } else {
                                            String data = "@" + Ctime + "," + Cimei + ",Q,1," + packageName;
                                            sendLocalData2Server(data);
                                            Log.e("info", "配置信息发送的data:" + data);
                                        }

                                    } else if (code.equals("2")) { //客户端请求查看配置服务器
                                        //editor.putString("mainHost", mainHost);editor.putString("assHost", assHost);
                                        Log.e("info", "客户端请求查看配置服务器");
                                        if (TextUtils.isEmpty(sp.getString("mainHost", "")) && TextUtils.isEmpty(sp.getString("assHost", ""))) {
                                            //当没有配置过服务器
                                            String data = "@" + Ctime + "," + Cimei + ",Q,1,";
                                            sendLocalData2Server(data);
                                            Log.e("info", "配置信息发送的data:" + data);
                                        } else if (!TextUtils.isEmpty(sp.getString("mainHost", "")) && TextUtils.isEmpty(sp.getString("assHost", ""))) {
                                            //配置了主服务器,没有配置副服务器
                                            String data = "@" + Ctime + "," + Cimei + ",Q,1," + sp.getString("mainHost", "") + ",";
                                            sendLocalData2Server(data);
                                            Log.e("info", "配置信息发送的data:" + data);
                                        } else if (TextUtils.isEmpty(sp.getString("mainHost", "")) && !TextUtils.isEmpty(sp.getString("assHost", ""))) {
                                            //没配置了主服务器,配置过副服务器
                                            String data = "@" + Ctime + "," + Cimei + ",Q,1,," + sp.getString("assHost", "");
                                            sendLocalData2Server(data);
                                            Log.e("info", "配置信息发送的data:" + data);
                                        } else if (!TextUtils.isEmpty(sp.getString("mainHost", "")) && !TextUtils.isEmpty(sp.getString("assHost", ""))) {
                                            //既主服务器,也配置过副服务器
                                            String data = "@" + Ctime + "," + Cimei + ",Q,1," + sp.getString("mainHost", "") + "," + sp.getString("assHost", "");
                                            sendLocalData2Server(data);
                                            Log.e("info", "配置信息发送的data:" + data);
                                        }
                                    } else if (code.equals("3")) { //客户端请求查看配置视频流服务器 ffmpeg_link
                                        Log.e("info", "客户端请求查看配置视频流服务器");
                                        String data = "@" + Ctime + "," + Cimei + ",Q,1," + sp.getString("ffmpeg_link", "");
                                        sendLocalData2Server(data);
                                        Log.e("info", "配置信息发送的data:" + data);
                                    } else if (code.equals("5")) { //客户端请求查看配置采集时间间隔
                                        Log.e("info", "客户端请求查看配置采集时间间隔");
                                        String data = "@" + Ctime + "," + Cimei + ",Q,1," + sp.getString("modifyTime", "");
                                        sendLocalData2Server(data);
                                        Log.e("info", "配置信息发送的data:" + data);
                                    } else if (code.equals("6")) { //客户端请求查看震动配置信息 isSettingQuakeInfo
                                        Log.e("info", "客户端请求查看震动配置信息");
                                        if (!sp.getBoolean("isSettingQuakeInfo", isSettingQuakeInfo)) {
                                            //如果没有配置过震动信息
                                            String data = "@" + Ctime + "," + Cimei + ",Q,1,0,0,0,0,20";
                                            sendLocalData2Server(data);
                                            Log.e("info", "配置信息发送的data:" + data);
                                        } else { //如果配置过震动信息
                                            String data = "@" + Ctime + "," + Cimei + ",Q,1," + sp.getString("modifyQuakeCrame", "")
                                                    + "," + sp.getString("isSendQuakePic", "")
                                                    + "," + sp.getString("modifyQuakeVCrame", "")
                                                    + "," + sp.getString("isSendQuakeVid", "")
                                                    + "," + sp.getString("ModifyQuake", "");
                                            sendLocalData2Server(data);
                                            Log.e("info", "配置信息发送的data:" + data);
                                        }
                                    } else if (code.equals("7")) { //客户端请求查看急减速配置信息
                                        if (sp.getBoolean("isModifySpeed", isModifySpeed)) {
                                            String data = "@" + Ctime + "," + Cimei + ",Q,1," + sp.getString("speedCramer", "") + "," + sp.getString("isSendSpeedPic", "") +
                                                    "," + sp.getString("speedVCramer", "") + "," + sp.getString("isSendSpeedVid", "") + "," + sp.getInt("timeValue", timeValue) +
                                                    "," + sp.getInt("speedValue", speedValue);
                                            sendLocalData2Server(data);
                                            Log.e("info", "配置信息发送的data:" + data);
                                        } else {
                                            String data = "@" + Ctime + "," + Cimei + ",Q,1,0,0,0,0,5,30";
                                            sendLocalData2Server(data);
                                            Log.e("info", "配置信息发送的data:" + data);
                                        }

                                    } else if (code.equals("8")) { //客户端请求查看系统设置
                                        Log.e("info", "客户端请求查看系统设置是否开启");
                                        if (sp.getBoolean("isSetSystemSetting", isSetSystemSetting)) {
                                            //如果是开启了的

                                            String data = "@" + Ctime + "," + Cimei + ",Q,1,1";
                                            sendLocalData2Server(data);
                                            Log.e("info", "配置信息发送的data:" + data);
                                        } else {
                                            //如果是开启了的
                                            String data = "@" + Ctime + "," + Cimei + ",Q,1,0";
                                            sendLocalData2Server(data);
                                            Log.e("info", "配置信息发送的data:" + data);
                                        }
                                    } else if (code.equals("9")) { //客户端请求查看消息队列里消息的数量
                                        Log.e("info", "客户端请求查看消息队列里消息的数量");
                                        String data = "@" + Ctime + "," + Cimei + ",Q,1," + messageList.size();
                                        sendLocalData2Server(data);
                                        Log.e("info", "配置信息发送的data:" + data);
                                    } else if (code.equals("0")) { //客户端请求查看所有的配置信息
                                        Log.e("info", "客户端请求查看所有的配置信息");
                                        try {
                                            //  private String dataPackageName,dataMainHost,dataAssHost,dataFfmpeg_link,
                                            // dataModifyTime,dataModifyQuakeCrame,dataIsSendQuakePic,
                                            // dataModifyQuakeVCrame,dataIsSendQuakeVid,dataModifyQuake
                                            //,dataSpeedCramer,dataIsSendSpeedPic,dataSpeedVCramer,dataIsSendSpeedVid;
                                            if (TextUtils.isEmpty(sp.getString("servicePackageName", ""))) {
                                                dataPackageName = "";
                                            } else {
                                                dataPackageName = sp.getString("servicePackageName", "");
                                            }
                                            if (TextUtils.isEmpty(sp.getString("mainHost", ""))) {
                                                dataMainHost = "";
                                            } else {
                                                dataMainHost = sp.getString("mainHost", "");
                                            }
                                            if (TextUtils.isEmpty(sp.getString("assHost", ""))) {
                                                dataAssHost = "";
                                            } else {
                                                dataAssHost = sp.getString("assHost", "");
                                            }
                                            if (TextUtils.isEmpty(sp.getString("ffmpeg_link", ""))) {
                                                dataFfmpeg_link = "";
                                            } else {
                                                dataFfmpeg_link = sp.getString("ffmpeg_link", "");
                                            }
                                            if (TextUtils.isEmpty(sp.getString("modifyTime", ""))) {
                                                dataModifyTime = "";
                                            } else {
                                                dataModifyTime = sp.getString("modifyTime", "");
                                            }
                                            if (TextUtils.isEmpty(sp.getString("modifyQuakeCrame", ""))) {
                                                dataModifyQuakeCrame = "";
                                            } else {
                                                dataModifyQuakeCrame = sp.getString("modifyQuakeCrame", "");
                                            }
                                            if (TextUtils.isEmpty(sp.getString("isSendQuakePic", ""))) {
                                                dataIsSendQuakePic = "";
                                            } else {
                                                dataIsSendQuakePic = sp.getString("isSendQuakePic", "");
                                            }
                                            if (TextUtils.isEmpty(sp.getString("modifyQuakeVCrame", ""))) {
                                                dataModifyQuakeVCrame = "";
                                            } else {
                                                dataModifyQuakeVCrame = sp.getString("modifyQuakeVCrame", "");
                                            }

                                            if (TextUtils.isEmpty(sp.getString("isSendQuakeVid", ""))) {
                                                dataIsSendQuakeVid = "";
                                            } else {
                                                dataIsSendQuakeVid = sp.getString("isSendQuakeVid", "");
                                            }
                                            if (TextUtils.isEmpty(sp.getString("ModifyQuake", ""))) {
                                                dataModifyQuake = "";
                                            } else {
                                                dataModifyQuake = sp.getString("ModifyQuake", "");
                                            }
                                            if (TextUtils.isEmpty(sp.getString("speedCramer", ""))) {
                                                dataSpeedCramer = "";
                                            } else {
                                                dataSpeedCramer = sp.getString("speedCramer", "");
                                            }
                                            if (TextUtils.isEmpty(sp.getString("isSendSpeedPic", ""))) {
                                                dataIsSendSpeedPic = "";
                                            } else {
                                                dataIsSendSpeedPic = sp.getString("isSendSpeedPic", "");
                                            }
                                            if (TextUtils.isEmpty(sp.getString("speedVCramer", ""))) {
                                                dataSpeedVCramer = "";
                                            } else {
                                                dataSpeedVCramer = sp.getString("speedVCramer", "");
                                            }
                                            if (TextUtils.isEmpty(sp.getString("isSendSpeedVid", ""))) {
                                                dataIsSendSpeedVid = "";
                                            } else {
                                                dataIsSendSpeedVid = sp.getString("isSendSpeedVid", "");
                                            }

                                            String data = "@" + Ctime + "," + Cimei + ",Q,1"
                                                    + "," + dataPackageName
                                                    + "," + dataMainHost
                                                    + "," + dataAssHost
                                                    + "," + dataFfmpeg_link
                                                    + "," + dataModifyTime
                                                    + "," + dataModifyQuakeCrame
                                                    + "," + dataSpeedCramer
                                                    + "," + dataIsSendQuakeVid
                                                    + "," + dataModifyQuake
                                                    + "," + dataSpeedCramer
                                                    + "," + dataIsSendSpeedPic
                                                    + "," + dataSpeedVCramer
                                                    + "," + dataIsSendSpeedVid
                                                    + "," + sp.getInt("timeValue", timeValue)
                                                    + "," + sp.getInt("speedValue", speedValue)
                                                    + "," + sp.getInt("setSystemSetting", setSystemSetting)
                                                    + "," + messageList.size();
                                            /****
                                             String data="@"+Ctime+","+Cimei+",Q,1,"
                                             +sp.getString("servicePackageName","")
                                             +","+sp.getString("mainHost","")
                                             +","+sp.getString("assHost","")
                                             +","+sp.getString("ffmpeg_link","")
                                             +","+sp.getString("modifyTime", "")
                                             +","+sp.getString("modifyQuakeCrame","")
                                             +","+sp.getString("isSendQuakePic","")
                                             +","+sp.getString("modifyQuakeVCrame","")
                                             +","+sp.getString("isSendQuakeVid","")
                                             +","+sp.getString("ModifyQuake","")
                                             +","+sp.getString("speedCramer","")
                                             +","+sp.getString("isSendSpeedPic","")
                                             +","+sp.getString("speedVCramer","")
                                             +","+sp.getString("isSendSpeedVid","")
                                             +","+sp.getInt("timeValue",timeValue)
                                             + ","+sp.getInt("speedValue",speedValue)
                                             +","+sp.getInt("setSystemSetting",setSystemSetting)
                                             +","+messageList.size();
                                             */

                                            sendLocalData2Server(data);
                                            Log.e("info", "配置信息发送的data:" + data);
                                        } catch (Exception e) {
                                            Log.e("info", "e:" + e);
                                            e.printStackTrace();
                                        }

                                    }

                                } else if (messageArray[2].equals("9")) { //清空后视镜指令
                                    Log.e("info", "进入外网状态下清除后视镜指令操作！");
                                    isFinishSocketOrder = false;
                                    String imei = messageArray[1];
                                    String Ctime = messageArray[0].substring(1);
                                    String clearRange = messageArray[4];
                                    if (clearRange.equals("0")) { //清空全部数据
                                        if (messageList.size() > 0) {
                                            messageList.clear();
                                        }
                                        lsDao.clearRecord();//清空缓存记录
                                        String data = "@" + Ctime + "," + imei + ",9,1,1";
                                        sendLocalData2Server(data);
                                        dataHandler.sendEmptyMessage(131);
                                    } else if (clearRange.equals("1")) { //保留报警和采集数据
                                        if (messageList.size() > 0) {
                                            messageList.clear();
                                        }
                                        String data = "@" + Ctime + "," + imei + ",9,1,1";
                                        sendLocalData2Server(data);
                                        dataHandler.sendEmptyMessage(131);
                                    } else { //指令识别码错误
                                        String data = "@" + Ctime + "," + imei + ",9,1,0";
                                        sendLocalData2Server(data);
                                        dataHandler.sendEmptyMessage(130);
                                    }

                                } else if (messageArray[2].equals("I")) { //安装新的apk
                                    isFinishSocketOrder = false;
                                    downLoadImei = messageArray[1];
                                    downLoadTime = messageArray[0].substring(1);
                                    String apkUrl = messageArray[4]; //apk地址
                                    downLoadFileName = apkUrl.substring(apkUrl.lastIndexOf("/") + 1);
                                    Log.e("info", "downLoadFileName>>>>" + downLoadFileName);
                                    try {
                                        downloadFile(apkUrl);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else if (messageArray[2].equals("C")) { //网络状态下控制拍照
                                    Log.e("info", "进入外网状态下远程控制拍照操作！");
                                    isFinishSocketOrder = false;
                                    String imei = messageArray[1];
                                    String Ctime = messageArray[0].substring(1);
                                    String cramer = messageArray[4];
                                    String isSendFile = messageArray[5];
                                    Log.e("info", "进入外网状态下远程控制拍照操作" + "imei:" + imei + " ctime:" + Ctime + "cramer:" + cramer + "isSendFile:" + isSendFile);
                                    Intent picIntent = new Intent();
                                    picIntent.setAction("com.android.chappie.ACTION_NET_C_PIC");
                                    picIntent.addCategory(Intent.CATEGORY_DEFAULT);
                                    picIntent.putExtra("imei", imei);
                                    picIntent.putExtra("ctime", Ctime);
                                    picIntent.putExtra("cramer", cramer);
                                    picIntent.putExtra("isSendFile", isSendFile);
                                    sendBroadcast(picIntent);
                                } else if (messageArray[2].equals("T")) { //进入外网状态下请求指定文件的缩略图
                                    Log.e("info", "进入外网状态下请求指定文件的缩略图");
                                    isFinishSocketOrder = false;
                                    String imei = messageArray[1];
                                    String Ctime = messageArray[0].substring(1);//时间戳
                                    String filePath = messageArray[4];
                                    fileName = filePath.substring(filePath.lastIndexOf("/") + 1);
                                    Log.e("info", "请求指定文件的缩略图filePath:" + filePath);
                                    Log.e("info", "fileName:" + fileName);
                                    //判断请求的文件是否存在
                                    if (fileIsExist(filePath)) {
                                        if (filePath.substring(filePath.lastIndexOf(".")).equals(".jpg")) {
                                            Log.e("info", "进入图片缩略图传输");
                                            //获取缩略图对象
                                            // Bitmap imageBitmap= ThumbnailUtil.getImageThumbnail(filePath);
                                            //将缩略图转为file对象
                                            Log.e("info", "找到图片缩略图，正在压缩...");
                                            // File imageFile = FileUtil.saveFile(imageBitmap, filePath, fileName);
                                            File imageFile = ThumbnailUtil.getImageThumbnail2File(filePath);
                                            Log.e("info", "缩略图转换完毕，正在发送...");
                                            sendLocalFile2Server(imageFile, Ctime, imei, "T");
                                            Log.e("info", "图片缩略图已成功发送!");
                                        } else if (filePath.substring(filePath.lastIndexOf(".")).equals(".mp4")) {
                                            Log.e("info", "进入视频缩略图传输");
                                            //Bitmap videoBitmap=getBitmapFromFile();
                                            //利用ThumnailUtils
                                            Bitmap videoBitmap = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Images.Thumbnails.MINI_KIND);
                                            Log.e("info", "找到图片缩略图，正在压缩...");
                                            String thumbVideoPath = "/storage/sdcard1/ChappieLauncher/thumbVideo.jpg";
                                            File videoFile = FileUtil.saveFile(videoBitmap, thumbVideoPath, fileName);
                                            sendLocalFile2Server(videoFile, Ctime, imei, "T");
                                            Log.e("info", "视频缩略图已成功发送!");
                                        }
                                    } else {
                                        dataHandler.sendEmptyMessage(120);
                                        String data = "@" + imei + "," + Ctime + ",T,1,0,@,";
                                        sendLocalData2Server(data);
                                        return;
                                    }

                                } else if (messageArray[2].equals("G")) { //网络状态下请求指定文件
                                    Log.e("info", "进入外网状态下请求下载指定文件操作！");
                                    isFinishSocketOrder = false;
                                    String imei = messageArray[1];
                                    String Ctime = messageArray[0].substring(1);//时间戳
                                    String filePath = messageArray[4];
                                    if (fileIsExist(filePath)) {//如果请求的文件存在
                                        new WifiAPSendFileDataThread(new DataOutputStream(ou), filePath, "G", Ctime, imei).start();
                                    } else {
                                        dataHandler.sendEmptyMessage(120);
                                        String data = "@" + imei + "," + Ctime + ",G,1,0";
                                        sendLocalData2Server(data);
                                        return;
                                    }
                                } else if (messageArray[2].equals("R")) { //网络状态下控制录像
                                    Log.e("info", "进入外网状态下远程控制录像操作！");
                                    isFinishSocketOrder = false;
                                    String imei = messageArray[1];
                                    String Ctime = messageArray[0].substring(1);//时间戳
                                    String cramer = messageArray[4];//主/副摄像头
                                    String videoTime = messageArray[5];//录制时间
                                    String isSendVideo = messageArray[6];//是否发送视频文件
                                    Intent vidIntent = new Intent();
                                    vidIntent.setAction("com.android.chappie.ACTION_NET_C_VID");
                                    vidIntent.addCategory(Intent.CATEGORY_DEFAULT);
                                    vidIntent.putExtra("imei", imei);
                                    vidIntent.putExtra("ctime", Ctime);
                                    vidIntent.putExtra("cramer", cramer);
                                    vidIntent.putExtra("videoTime", videoTime);
                                    vidIntent.putExtra("isSendVideo", isSendVideo);
                                    sendBroadcast(vidIntent);
                                } else if (messageArray[2].equals("F")) {
                                    Log.e("info", "收到客户端请求后视镜指定目录的文件列表的请求");
                                    isFinishSocketOrder = false;
                                    fileImei = messageArray[1];
                                    fileTime = messageArray[0].substring(1);//时间戳
                                    String dir = messageArray[4];//文件目录
                                    //String childDir=message.substring(50,60);
                                    //判断目录名称
                                    if (dir.contains("travel")) {
                                        dir = "travel";
                                    } else if (dir.contains("auto")) {
                                        dir = "auto";
                                    } else if (dir.contains("arttificial")) {
                                        dir = "arttificial";
                                    }
                                    //判断子目录名称
                                    //childDir=message.substring(52,60);
                                    String path = "/storage/sdcard1/ChappieLauncher/" + dir;
                                    if (fileIsExist(path)) {
                                        File file = new File(path);
                                        File[] tempList = file.listFiles();
                                        ArrayList<String> listFileName = new ArrayList<String>();
                                        getAllFiles(file);//获取指定文件夹里的文件列表并进行拼接
                                        stringBuffer1 = new StringBuffer();
                                        for (int i = 0; i < fileList.size(); i++) {
                                            if (fileList.get(i) != null) {
                                                stringBuffer1.append(fileList.get(i));
                                            }
                                        }
                                        Log.e("info", "stringBuffer1>>" + stringBuffer1);
                                        if (stringBuffer1.length() != 0) {
                                            String data = "@" + fileTime + "," + fileImei + ",F,1,1," + fileList.size() + "," + fileNumListSize + "," + stringBuffer1;
                                            sendLocalData2Server(data);
                                            if (fileList.size() > 0) {
                                                fileList.clear();
                                                Log.e("info", "请求成功fileList.clear();" + fileList.size());
                                            }
                                            if (fileNumList.size() > 0) {
                                                fileNumList.clear();
                                                Log.e("info", "请求成功fileNumList.clear();" + fileNumList.size());
                                            }
                                            if (stringBuffer1.length() != 0) {
                                                stringBuffer1 = null;
                                                Log.e("info", "请求成功stringBuffer1" + stringBuffer1);
                                            }
                                            if (dirNameCover.length() != 0) {
                                                dirNameCover = null;
                                                Log.e("info", "请求成功dirNameCover" + dirNameCover);
                                            }
                                        } else {
                                            dataHandler.sendEmptyMessage(120);
                                            String data = "@" + fileTime + "," + fileImei + ",F,1,0";
                                            sendLocalData2Server(data);
                                            if (fileList.size() > 0) {
                                                fileList.clear();
                                                Log.e("info", "请求bu成功fileList.clear();" + fileList.size());
                                            }
                                            if (fileNumList.size() > 0) {
                                                fileNumList.clear();
                                                Log.e("info", "请求bu成功fileNumList.clear();" + fileNumList.size());
                                            }
                                            if (stringBuffer1.length() != 0) {
                                                stringBuffer1 = null;
                                                Log.e("info", "请求bu成功stringBuffer1=null;");
                                            }
                                            if (dirNameCover.length() != 0) {
                                                dirNameCover = null;
                                                Log.e("info", "请求bu成功dirNameCover" + dirNameCover);
                                            }
                                        }

                                    } else { //失败
                                        dataHandler.sendEmptyMessage(120);
                                        String data = "@" + fileTime + "," + fileImei + ",F,1,0";
                                        sendLocalData2Server(data);
                                    }

                                } else if (messageArray[2].equals("0")) { //客户端配置查派服务器


                                } else if (messageArray[2].equals("1")) { //客户端配置开机启动的APP请求
                                    Log.e("info", "收到客户端配置开机启动的APP请求");
                                    isFinishSocketOrder = false;
                                    serviceImei = messageArray[1];//imei
                                    servicetime = messageArray[0].substring(1);//时间戳
                                    servicePackageName = messageArray[4];
                                    if (!TextUtils.isEmpty(servicePackageName)) {
                                        if (isPackageNameExist(servicePackageName)) { //如果包名存在则发送广播
                                            Log.e("info", "isPackageNameExist");
                                            editor.putString("servicePackageName", servicePackageName);
                                            editor.commit();
                                            sendPackNameBroadcast(servicetime, serviceImei, servicePackageName, isStartNewApp);
                                        } else if (isPackageNameExist(servicePackageName) == false) {
                                            String data = "@" + servicetime + "," + serviceImei + ",1,1,0";
                                            sendLocalData2Server(data);
                                            dataHandler.sendEmptyMessage(126);
                                        }
                                    } else { //包名为空就返回失败
                                        String data = "@" + servicetime + "," + serviceImei + ",1,1,0";
                                        sendLocalData2Server(data);
                                        dataHandler.sendEmptyMessage(125);
                                    }
                                } else if (messageArray[2].equals("2")) { //客户端配置通信服务器请求
                                    Log.e("info", "收到客户端配置通信服务器请求");
                                    isFinishSocketOrder = false;
                                    serviceImei = messageArray[1];//imei
                                    servicetime = messageArray[0].substring(1);//时间戳
                                    mainHost = messageArray[4];//主服务器域名
                                    assHost = messageArray[5];//副服务器域名
                                    Log.e("info", "imei:" + serviceImei + "Ctime:" + servicetime + "MainHost:" + mainHost + "AssHost" + assHost);
                                    if (!TextUtils.isEmpty(mainHost) && !TextUtils.isEmpty(assHost)) { //如果主服务器和副服务器都不为空
                                        isConfigureServicePort = true;
                                        editor.putBoolean("isConfigureServicePort", isConfigureServicePort);
                                        editor.putString("mainHost", mainHost);
                                        editor.putString("assHost", assHost);
                                        editor.commit();
                                        restartScocketThread();
                                        serviceData = "@" + servicetime + "," + serviceImei + ",2,1,1";
                                        dataHandler.sendEmptyMessage(110);
                                    } else if (mainHost != null && assHost == null) { //如果主服务器不为空，副服务器为空
                                        isConfigureServicePort = true;
                                        editor.putBoolean("isConfigureServicePort", isConfigureServicePort);
                                        editor.putString("mainHost", mainHost);
                                        editor.commit();
                                        restartScocketThread();
                                        serviceData = "@" + servicetime + "," + serviceImei + ",2,1,1";
                                        dataHandler.sendEmptyMessage(110);
                                    } else if (assHost != null && mainHost == null) { //如果副服务器不为空，主服务器为空
                                        isConfigureServicePort = true;
                                        editor.putBoolean("isConfigureServicePort", isConfigureServicePort);
                                        editor.putString("assHost", assHost);
                                        editor.commit();
                                        restartScocketThread();
                                        serviceData = "@" + servicetime + "," + serviceImei + ",2,1,1";
                                        sendLocalData2Server(serviceData);
                                        dataHandler.sendEmptyMessage(110);
                                    } else if (assHost != null && mainHost != null) {
                                        serviceData = "@" + servicetime + "," + serviceImei + ",2,1,0";
                                        sendLocalData2Server(serviceData);
                                        dataHandler.sendEmptyMessage(110);
                                    }

                                } else if (messageArray[2].equals("3")) { //客户端配置视频流服务器请求(meiyou zuo)
                                    Log.e("info", "收到客户端配置视频流服务器请求");
                                    isFinishSocketOrder = false;
                                    String imei = messageArray[1];//imei;
                                    String Ctime = messageArray[0].substring(1);//时间戳;
                                    String ffmpeg_link = messageArray[4];
                                    if (TextUtils.isEmpty(ffmpeg_link)) {
                                        editor.putString("ffmpeg_link", ffmpeg_link);
                                        editor.commit();
                                        sendVideoThreadBroadcast(Ctime, imei, ffmpeg_link);
                                    } else {
                                        //如果地址为空则报错
                                        String videoThreadService = "@" + Ctime + "," + imei + "," + "3,1,0";
                                        sendLocalData2Server(videoThreadService);
                                        dataHandler.sendEmptyMessage(111);
                                    }


                                } else if (messageArray[2].equals("5")) {  //客户端请求配置数据采集时间间隔设置
                                    Log.e("info", "收到客户端请求配置数据采集时间间隔设置");
                                    isFinishSocketOrder = false;
                                    String imei = messageArray[1];//imei
                                    String Ctime = messageArray[0].substring(1);//时间戳;
                                    modifyTime = messageArray[4];
                                    Log.e("info", "收到客户端请求的ModifyTime" + modifyTime);
                                    if (modifyTime.equals("0")) { //如果修改的时间为0，则不再发送该类信息
                                        editor.putString("modifyTime", modifyTime);
                                        editor.commit();
                                        String modifyTimeService = "@" + Ctime + "," + imei + "," + "5,1,1";
                                        sendLocalData2Server(modifyTimeService);
                                        dataHandler.sendEmptyMessage(112);
                                        Log.e("info", "发送给服务器的数据是:" + modifyTimeService);
                                    } else if (TextUtils.isEmpty(modifyTime)) { //如果修改时间为空
                                        String modifyTimeService = "@" + Ctime + "," + imei + "," + "5,1,0";
                                        sendLocalData2Server(modifyTimeService);
                                        dataHandler.sendEmptyMessage(124);
                                        Log.e("info", "发送给服务器的数据是:" + modifyTimeService);
                                    } else if (!TextUtils.isEmpty(modifyTime)) { //如果修改时间不为空
                                        editor.putString("modifyTime", modifyTime);
                                        editor.commit();
                                        String modifyTimeService = "@" + Ctime + "," + imei + "," + "5,1,1";
                                        sendLocalData2Server(modifyTimeService);
                                        dataHandler.sendEmptyMessage(113);
                                        Log.e("info", "发送给服务器的数据是:" + modifyTimeService);
                                    }
                                } else if (messageArray[2].equals("6")) { //客户端请求配置震动告警参数
                                    Log.e("info", "收到客户端请求配置震动告警参数");
                                    isFinishSocketOrder = false;
                                    modifyQuakeTime = messageArray[0].substring(1);//时间戳
                                    modifyQuakeImei = messageArray[1];//imei;
                                    modifyQuakeCrame = messageArray[4];//震动拍照的摄像头
                                    isSendQuakePic = messageArray[5];//震动照片文件是否发送
                                    modifyQuakeVCrame = messageArray[6];//震动摄像的摄像头
                                    isSendQuakeVid = messageArray[7];//震动视频文件是否发送
                                    ModifyQuake = messageArray[8];//震动参数
                                    Log.e("info", "ModifyQuake:" + ModifyQuake + "  modifyQuakeCrame:" + modifyQuakeCrame + "  isSendQuakePic:" + isSendQuakePic + "  modifyQuakeVCrame:" + modifyQuakeVCrame + "  isSendQuakeVid:" + isSendQuakeVid);
                                    if (ModifyQuake.equals("0")) { //当震动参数为0的时候
                                        Log.e("info", "当震动参数为0时");
                                        String modifyQuakeOrder = "@" + modifyQuakeTime + "," + modifyQuakeImei + "6,1,0";
                                        sendLocalData2Server(modifyQuakeOrder);
                                        dataHandler.sendEmptyMessage(114);
                                        Log.e("info", "发送给服务器的数据是:" + modifyQuakeOrder);
                                    } else if (!ModifyQuake.equals("0")) {
                                        Log.e("info", "当震动参数不为0时");
                                        editor.putString("ModifyQuake", ModifyQuake);
                                        editor.putString("modifyQuakeCrame", modifyQuakeCrame);
                                        editor.putString("isSendQuakePic", isSendQuakePic);
                                        editor.putString("modifyQuakeVCrame", modifyQuakeVCrame);
                                        editor.putString("isSendQuakeVid", isSendQuakeVid);
                                        editor.commit();
                                        sendModifyQuakeBroadcast(ModifyQuake, modifyQuakeCrame, isSendQuakePic, modifyQuakeVCrame, isSendQuakeVid);
                                    }
                                } else if (messageArray[2].equals("7")) {
                                    Log.e("info", "进入急减速告警参数设置");
                                    isFinishSocketOrder = false;
                                    String imei = messageArray[1];//imei;;
                                    String Ctime = messageArray[0].substring(1);//时间戳
                                    String speedCramer = messageArray[4];//急减速拍照的摄像头
                                    String isSendSpeedPic = messageArray[5];//急减速照片文件是否发送
                                    String speedVCramer = messageArray[6];//急减速摄像的摄像头
                                    String isSendSpeedVid = messageArray[7];//急减速视频文件是否发送
                                    String speedTime = messageArray[8];//急减速时间
                                    String speedValue = messageArray[9];//急减速值

                                    Log.e("info", "在急减速告警请求中,接收到的修改时间是:" + speedTime + "--修改的速度值是:" + speedValue + "--急减速拍照:" + speedCramer
                                            + "急减速照片文件是否发送>>" + isSendSpeedPic + " 急减速摄像的摄像头>>" + speedVCramer + " isSendSpeedVid>>" + isSendSpeedVid);
                                    //将急减速告警参数以广播的形式发送出去
                                    if (Integer.parseInt(speedTime) == 0 || Integer.parseInt(speedValue) == 0) {
                                        String data = "@" + Ctime + "," + imei + "," + "7,1,0";
                                        sendLocalData2Server(data);
                                        dataHandler.sendEmptyMessage(116);
                                        return;
                                    } else if (isInteger(speedTime) && isInteger(speedValue)) { //当两个参数都是整数时
                                        Intent intent = new Intent();
                                        intent.setAction(ChappieConstant.SEND_CAR_LIMITED_SPEED);
                                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                                        intent.putExtra("speedTime", speedTime);
                                        intent.putExtra("speedValue", speedValue);
                                        intent.putExtra("speedCramer", speedCramer);
                                        intent.putExtra("isSendSpeedPic", isSendSpeedPic);
                                        intent.putExtra("speedVCramer", speedVCramer);
                                        intent.putExtra("isSendSpeedVid", isSendSpeedVid);
                                        intent.putExtra("imei", imei);
                                        intent.putExtra("Ctime", Ctime);
                                        sendBroadcast(intent);
                                        Log.e("info", "已发送急减速修改参数的广播");
                                    } else {
                                        dataHandler.sendEmptyMessage(117);
                                    }


                                } else if (messageArray[2].equals("D")) { //网络状态下删除指定文件
                                    isFinishSocketOrder = false;
                                    String orderTime = messageArray[0].substring(1);
                                    String orderImei = messageArray[1];
                                    String filePath = messageArray[4];//删除文件的路径

                                    if (fileIsExist(filePath)) {
                                        Log.e("info", "请求删除的文件是:" + filePath);
                                        if (FileToolkit.deleteFile(filePath) == true) {
                                            Log.e("info", "删除指定文件的结果:" + FileToolkit.deleteFile(filePath));
                                            String data = "@" + orderTime + "," + orderImei + ",D,1,1";
                                            sendLocalData2Server(data);
                                            dataHandler.sendEmptyMessage(122);
                                        } else {
                                            String data = "@" + orderTime + "," + orderImei + ",D,1,0";
                                            sendLocalData2Server(data);
                                            dataHandler.sendEmptyMessage(123);
                                        }
                                    } else {
                                        dataHandler.sendEmptyMessage(120);
                                        return;
                                    }
                                    ;
                                } else if (messageArray[2].equals("S")) {
                                    isFinishSocketOrder = false;
                                    String imei = messageArray[1];
                                    String timeStamp = messageArray[0].substring(1);//时间戳
                                    String target = messageArray[4];//主/副摄像头
                                    String onOff = messageArray[5]; //打开或关闭
                                    Log.e("info", "收到客户端发来的开启或关闭视频实时查看的请求");
                                    if (onOff.equals("1")) {
                                        Log.e("info", "客户端请求打开视频实时查看功能");
                                        if (target.equals("0")) {
                                            Log.e("info", "客户端请求打开主摄像头的视频实时查看功能");
                                            Intent intent = new Intent();
                                            intent.setAction("com.scblhkj.cp.VIDEO_BACK_ON_REALTIME");
                                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                                            Bundle bundle = new Bundle();
                                            bundle.putString("imei", imei);
                                            bundle.putString("timeStamp", timeStamp);
                                            bundle.putString("target", target);
                                            intent.putExtras(bundle);
                                            sendBroadcast(intent);
                                            Log.e("info", "imei>>>" + imei + "timeStamp>>" + timeStamp + "target>>" + target);
                                        } else if (target.equals("1")) {
                                            Log.e("info", "客户端请求打开前置摄像头的视频实时查看功能");
                                            //发送开启前置摄像头的广播
                                            Intent intent = new Intent();
                                            intent.setAction("com.scblhkj.cp.VIDEO_FRO_ON_REALTIME");
                                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                                            Bundle bundle = new Bundle();
                                            bundle.putString("imei", imei);
                                            bundle.putString("timeStamp", timeStamp);
                                            bundle.putString("target", target);
                                            intent.putExtras(bundle);
                                            sendBroadcast(intent);
                                            Log.e("info", "imei>>>" + imei + "timeStamp>>" + timeStamp + "target>>" + target);
                                        }
                                    } else if (onOff.equals("0")) {
                                        if (target.equals("1")) {
                                            Log.e("info", "客户端请求关闭前置摄像头视频实时查看功能");
                                            Intent intent = new Intent();
                                            intent.setAction("com.scblhkj.cp.VIDEO_FRO_OFF_REALTIME");
                                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                                            Bundle bundle = new Bundle();
                                            bundle.putString("imei", imei);
                                            bundle.putString("timeStamp", timeStamp);
                                            bundle.putString("target", target);
                                            intent.putExtras(bundle);
                                            sendBroadcast(intent);
                                        } else if (target.equals("0")) {
                                            Log.e("info", "客户端请求关闭后置主摄像头视频实时查看功能");
                                            Intent intent = new Intent();
                                            intent.setAction("com.scblhkj.cp.VIDEO_BACK_OFF_REALTIME");
                                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                                            Bundle bundle = new Bundle();
                                            bundle.putString("imei", imei);
                                            bundle.putString("timeStamp", timeStamp);
                                            bundle.putString("target", target);
                                            intent.putExtras(bundle);
                                            sendBroadcast(intent);
                                        }
                                    }
                                }
                            }
                            String[] tempMessage = message.split("\n"); // 按照换行符分离
                            for (int i = 0; i < tempMessage.length; i++) {
                                byte[] tempBytes = SerialFunUtil.HexToByteArr(toHexString(tempMessage[i]));
                                if (tempMessage[i].charAt(0) == 'O') {  // 判断包头
                                    if (tempMessage[i].length() == tempBytes[2] + 4) {  // 判断包的完成性
                                        if (tempBytes[1] != (byte) 0x05) { // 模块上传通用数据流 不需向下发送数据
                                            Log.e("info", "tempBytes2" + tempBytes.toString());
                                            sendSeriveOBDCMD2Port(tempBytes);
                                        }
                                        setOBDSendDataFlag(tempBytes[1]);
                                        tempSendBuffer.clear();
                                    } else {  // 不完整包
                                        tempSendBuffer.clear();
                                        for (int j = 0; j < tempBytes.length; j++) {
                                            tempSendBuffer.add(tempBytes[j]);
                                        }
                                    }
                                } else if (tempMessage[i].charAt(0) == 'T') {
                                    if (ChappieCarApplication.isDebug)
                                        Log.e("info", "+++收到胎压指令+++");
                                    if (tempMessage[i].length() == tempBytes[2] + 4) {  // 判断包的完成性
                                        Log.e("info", "胎压数据待发送");
                                        sendTpmsData2Server();  // 发送缓存的胎压数据服务器
                                    }
                                } else {  // 中间值
                                    for (int j = 0; j < tempBytes.length; j++) {
                                        tempSendBuffer.add(tempBytes[j]);
                                    }
                                    if (tempSendBuffer.size() == tempSendBuffer.get(2) + 3) {  // 判断包的完成性
                                        Log.e("info", "tempSendBuffer.size() == tempSendBuffer.get(2) + 3" + tempSendBuffer.toString());
                                        //这个地方有问题，需要处理
                                        // sendSeriveOBDCMD2Port(tempBytes);
                                    } else {  // 不完整包
                                        for (int j = 0; j < tempBytes.length; j++) {
                                            tempSendBuffer.add(tempBytes[j]);
                                        }
                                    }
                                }
                            }
                            // 收到服务器过来的消息，就通过Broadcast发送出去
                            if (message.equals("ok")) {//处理心跳回复
                                Intent send2server = new Intent();
                                send2server.setAction("com.chappie.alarm.heart_beat_action");
                                sendBroadcast(send2server);
                            } else {// 其他消息回复
                                Intent send2server = new Intent();
                                send2server.setAction("com.chappie.alarm.message_action");
                                send2server.putExtra("message_action", message);
                                sendBroadcast(send2server);
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /***
     * 打开下载的apk
     *
     * @param file
     */
    private void openFile(File file) {
        Log.e("info", "openFile>>" + file.getName());
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(android.content.Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file),
                "application/vnd.android.package-archive");
        startActivity(intent);
    }

    /**
     * 下载文件
     */
    public void downloadFile(String url) {
        Request request = new Request.Builder().url(url).tag(this).build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if (!isDownLoadVoice) {
                    dataHandler.sendEmptyMessage(134);
                } else {
                    dataHandler.sendEmptyMessage(141);
                    isDownLoadVoice = false;
                }

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!isDownLoadVoice) {
                    try {
                        Log.e("info", "马上进入saveFile()");
                        saveFile(response);
                        dataHandler.sendEmptyMessage(132);

                    } catch (IOException e) {
                        dataHandler.sendEmptyMessage(134);
                        e.printStackTrace();
                    }
                } else {
                    try {
                        Log.e("info", "voice马上进入saveFile()");
                        saveFile(response);
                        dataHandler.sendEmptyMessage(132);

                    } catch (IOException e) {
                        // dataHandler.sendEmptyMessage(134);
                        e.printStackTrace();
                    }
                }

            }

        });
    }

    public File saveFile(Response response) throws IOException {
        InputStream is = null;
        byte[] buf = new byte[2048];
        int len = 0;
        FileOutputStream fos = null;
        try {
            is = response.body().byteStream();
            final long total = response.body().contentLength();
            Log.e("info", "total:" + total);
            long sum = 0;
            File dir = new File("/storage/sdcard1/downloaddata");
            if (!dir.exists()) dir.mkdirs();
            File file = new File(dir, downLoadFileName);
            fos = new FileOutputStream(file);
            while ((len = is.read(buf)) != -1) {
                sum += len;

                fos.write(buf, 0, len);
                final long finalSum = sum;
                dataHandler.obtainMessage(133, String.valueOf(finalSum * 1.0f / total)).sendToTarget();
            }
            fos.flush();
            return file;
        } finally {
            try {
                if (is != null) is.close();
            } catch (IOException e) {
            }
            try {
                if (fos != null) fos.close();
            } catch (IOException e) {
            }
        }
    }

    /***
     * 发送配置视频流服务器广播
     */
    private void sendVideoThreadBroadcast(String timeStamp, String imei, String videoThreadAdd) {
        Intent intent = new Intent();
        intent.setAction(ChappieConstant.SEND_VIDEO_THREAD);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.putExtra("videoThreadAdd", videoThreadAdd);
        intent.putExtra("timeStamp", timeStamp);
        intent.putExtra("imei", imei);
        sendBroadcast(intent);
    }

    /***
     * 发送设置启动应用的广播
     *
     * @param packageTime
     * @param packageImei
     * @param packageName
     * @param isStartNewApp
     */
    private void sendPackNameBroadcast(String packageTime, String packageImei, String packageName, Boolean isStartNewApp) {
        Intent intent = new Intent();
        intent.setAction(ChappieConstant.SEND_START_APP_PACKAGENAME);
        intent.putExtra("packageTime", packageTime);
        intent.putExtra("packageImei", packageImei);
        intent.putExtra("packageName", packageName);
        intent.putExtra("isStartNewApp", isStartNewApp);
        Log.e("info", "sendPackNameBroadcast--packageName>>>" + packageName + "  isStartNewApp:" + isStartNewApp);
        sendBroadcast(intent);


    }

    /***
     * 遍历后视镜所有包名，并根据传递的包名来判断该包名是否存在
     *
     * @param packageName
     * @return
     */
    public boolean isPackageNameExist(String packageName) {
        if (packageName == null || "".equals(packageName))
            return false;
        try {
            getPackageManager().getApplicationInfo(packageName, PackageManager.GET_UNINSTALLED_PACKAGES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    /**
     * 根据路径获取内存状态
     *
     * @param path
     * @return
     */

    private String getMemoryInfo(File path) {
        StatFs statFs = new StatFs(path.getPath());
        long blockSize = statFs.getBlockSize();//获取一个扇区的大小
        long totalBlocks = statFs.getBlockCount();    // 获得扇区的总数

        long availableBlocks = statFs.getAvailableBlocks();   // 获得可用的扇区数量
        // 总空间
        String totalMemory = Formatter.formatFileSize(this, totalBlocks * blockSize);
        // 可用空间
        // String availableMemory = Formatter.formatFileSize(this, availableBlocks * blockSize);
        String availableMemory = String.valueOf((availableBlocks * blockSize) / 1024);
        return availableMemory;
        // return "总空间: " + totalMemory + "\n可用空间: " + availableMemory;
    }

    /**
     * 功能：检查请求isInteger方法的参数是否为整数
     *
     * @param str String
     * @return 返回boolean类型，false表示不是整数，true表示是整数
     */
    public static boolean isInteger(String str) {
        int begin = 0;
        if (str == null || str.trim().equals("")) {
            return false;
        }
        str = str.trim();
        if (str.startsWith("+") || str.startsWith("-")) {
            if (str.length() == 1) {
                return false;
            }
            begin = 1;
        }
        for (int i = begin; i < str.length(); i++) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /****
     * 发送震动设置的广播
     *
     * @param modifyQuakeCrame  震动拍照摄像头
     * @param isSendQuakePic    是否发送震动图片文件
     * @param modifyQuakeVCrame 震动摄像摄像头
     * @param isSendQuakeVid    是否发送震动视频文件
     *                          sendModifyQuakeBroadcast(ModifyQuake,modifyQuakeCrame,isSendQuakePic,modifyQuakeVCrame,isSendQuakeVid);
     */
    private void sendModifyQuakeBroadcast(String ModifyQuake, String modifyQuakeCrame, String isSendQuakePic, String modifyQuakeVCrame, String isSendQuakeVid) {
        Intent modifyQuakeIntent = new Intent();
        modifyQuakeIntent.setAction("com.android.chappie.MODIFY_QUAKE");
        modifyQuakeIntent.addCategory(Intent.CATEGORY_DEFAULT);
        modifyQuakeIntent.putExtra("ModifyQuake", ModifyQuake);
        modifyQuakeIntent.putExtra("modifyQuakeCrame", modifyQuakeCrame);
        modifyQuakeIntent.putExtra("isSendQuakePic", isSendQuakePic);
        modifyQuakeIntent.putExtra("modifyQuakeVCrame", modifyQuakeVCrame);
        modifyQuakeIntent.putExtra("isSendQuakeVid", isSendQuakeVid);
        modifyQuakeIntent.putExtra("modifyQuakeImei", modifyQuakeImei);
        modifyQuakeIntent.putExtra("modifyQuakeTime", modifyQuakeTime);
        sendBroadcast(modifyQuakeIntent);
        Log.e("info", " ccluncher发送的震动数据:" + " ModifyQuake>>" + ModifyQuake + " modifyQuakeCrame>>" + modifyQuakeCrame
                + " isSendQuakePic>>" + isSendQuakePic + " modifyQuakeVCrame" + modifyQuakeVCrame + " isSendQuakeVid>>" + isSendQuakeVid);
    }

    /**
     * 往服务发送数据反馈的广播接受者
     */
    private class Send2ServerRecevier extends BroadcastReceiver {

        private String TAG = "Send2ServerRecevier";

        public Send2ServerRecevier() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.chappie.alarm.heart_beat_action")) {
                if (ChappieCarApplication.isDebug)
                    Log.e("info", "接收到了com.chappie.alarm.heart_beat_action广播");
            } else if (intent.getAction().equals("com.chappie.alarm.message_action")) {
                if (ChappieCarApplication.isDebug)
                    Log.e("info", "接收到了com.chappie.alarm.message_action广播");
                String message = intent.getStringExtra("message_action");
                Log.e("info", "接收到了com.chappie.alarm.message_action广播" + message);
                if (ChappieCarApplication.isDebug)
                    Log.e("info", message);
            }
        }
    }


    /**
     * 发送指令给服务串口  (组包)
     *
     * @param bytes
     */
    private void sendSeriveOBDCMD2Port(byte[] bytes) {
        Log.e("info", "fuck fuck fuck");
        byte[] cmd = new byte[128];
        byte ids[] = CommonUtil.getObdSerialId2SP();
        cmd[0] = (byte) 0x01;
        cmd[1] = (byte) 0xff;
        cmd[2] = (byte) 0x03;
        cmd[4] = (byte) 0x28;
        // OBD序列号复制
        cmd[5] = ids[0];
        cmd[6] = ids[1];
        cmd[7] = ids[2];
        cmd[8] = ids[3];
        cmd[9] = ids[4];
        cmd[10] = ids[5];
        cmd[11] = 0x00;
        cmd[12] = bytes[1]; // 数据指令
        cmd[13] = 0x00;
        cmd[14] = bytes[2]; // 数据长度
        int length = bytes[2];  // 参数长度
        for (int i = 0; i < length; i++) {  // 获取参数
            cmd[15 + i] = bytes[3 + i];
        }
        cmd[15 + length] = getXORValue(cmd); // 数据校验位
        cmd[3] = (byte) (13 + length);
        cmd[16 + length] = 0x29; // 结束符
        if (ChappieCarApplication.isDebug)
            Log.e("info", "数据组装完成");
        sendCMD2Port(cmd);
    }


    /**
     * 异或
     *
     * @param bs
     * @return
     */
    private byte getXORValue(byte[] bs) {
        int length = bs.length;
        byte xor = 0;
        for (int i = 11; i < length; i++) {
            xor = (byte) (xor ^ bs[i]);
        }
        return xor;
    }


    /**
     * 向串口发送数据
     */
    private void sendCMD2Port(byte[] bs) {
        Intent dataIntent = new Intent();
        dataIntent.setAction(SerialDataCommSerivce.SERIAL_ACTION_RECEIVE);
        dataIntent.addCategory(Intent.CATEGORY_DEFAULT);
        Bundle bundle = new Bundle();
        bundle.putByteArray(ChappieConstant.CMD, bs);
        dataIntent.putExtras(bundle);
        sendBroadcast(dataIntent);
        if (ChappieCarApplication.isDebug)
            Log.e("info", "服务器请求串口的广播发送出去,传送给串口的数据为：" + SerialFunUtil.ByteArrToHexForLog(bs));
    }


    /**
     * 字符串转十六进制字符串
     *
     * @param
     * @return
     */
    public static String toHexString(String s) {
        String str = "";
        try {
            byte[] bs = s.getBytes("UTF-8");
            for (int i = 0; i < bs.length; i++) {
                String hexByte = Integer.toHexString(bs[i]);
                if (hexByte.length() == 1) {
                    hexByte = "0" + hexByte;
                }
                str = str + hexByte;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return str;
    }


    /**
     * 设置OBD发送指令标识
     */
    private void setOBDSendDataFlag(byte b) {
        Log.e(TAG, "数据标识位的值为 ： " + SerialFunUtil.Byte2Hex(b));
        switch (b) {  // 指令位
            case 0x00: {   // 查询产品ID
                ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE | 0X0001;
                if (ChappieCarApplication.isDebug)
                    Log.e(TAG, "查询产品ID或指令匹配成功");
                break;
            }
            case 0x01: {  // 查询软件版本号
                ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE | 0X0002;
                break;
            }
            case 0x02: {  // 请求模块重启
                ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE | 0X0004;
                break;
            }
            case 0x03: {  // 模块休眠唤醒
                ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE | 0X0008;
                break;
            }
            case 0x05: { // 模块上传通用数据流
                ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE | 0X0010;
                break;
            }
            case 0x06: { // 设置是否打开通用数据流
                ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE | 0X0020;
                break;
            }
            case 0x07: { // 厂家自定义OBD数据流
                ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE | 0X0040;
                break;
            }
            case 0x08: { // 设置查询厂家自定义OBD数据流时间间隔
                ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE | 0X0080;
                break;
            }
            case 0x09: {  // 查询/上传用户自定义PID数据
                ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE | 0X0100;
                break;
            }
            case 0x0a: { // 车辆体检
                ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE | 0X0200;
                break;
            }
            case 0x0c: {  // 设置查询VIN码
                ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE | 0X0400;
                break;
            }
            case 0x0d: { // 设置查询发动机故障代码
                ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE | 0X0800;
                break;
            }

            case 0x0e: { // 清除发动机故障代码
                ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE | 0X1000;
                break;
            }
            case 0x0f: {  // 设置查询里程
                ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE | 0X2000;
                break;
            }
            case 0x10: { // 请求上传模块累计驾驶数据
                ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE | 0X4000;
                break;
            }
            case 0x12: {// 设置查询报警参数
                ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE | 0X8000;
                break;
            }
        }
        if (ChappieCarApplication.isDebug)
            Log.e(TAG, "ChappieCarApplication.CMD_FLAGE = " + ChappieCarApplication.CMD_FLAGE);
    }

    /**
     * 发送胎压基础数据给服务器
     *
     * @param
     */
    private void sendTpmsData2Server() {
        try {
            // 组装缓存在内存中的胎压数据给服务器
            StringBuilder cmdSB = new StringBuilder();
            cmdSB.append('T');
            cmdSB.append(ChappieCarApplication.DEVICE_IMEI);
            cmdSB.append(CommonUtil.getUnixTimeStamp());
            cmdSB.append(SerialFunUtil.ByteArrToHex(ChappieCarApplication.tpmsCaches));
            sendLocalData2Server(cmdSB.toString());
            if (ChappieCarApplication.isDebug)
                Log.e(TAG, "胎压发送过去的指令是" + cmdSB.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 广播通知开启手势
     */
    private void sendBroacast2EnableSensortek() {
        sendBroadcast(new Intent("com.sensortek.broadcast.enable"));
        Log.e(TAG, "发送开启手势广播");
    }


}

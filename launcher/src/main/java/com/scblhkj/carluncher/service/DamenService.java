package com.scblhkj.carluncher.service;

import android.accessibilityservice.AccessibilityService;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;

import android.view.View;
import android.view.WindowManager;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.activity.MainActivity;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.utils.NetworkUtil;
import com.scblhkj.carluncher.utils.SDCardScannerUtil;
import com.scblhkj.carluncher.utils.ToastUtil;
import com.scblhkj.carluncher.widget.TopDeskLayout;
import com.txznet.sdk.TXZAsrManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by blh on 2016-07-11.
 */
public class DamenService extends Service {
    ActivityManager mActivityManager;
    ChappieCarApplication chappieCarApplication;
    private android.view.View topDeskLayout;
    private WindowManager mWindowManager;
    private WindowManager.LayoutParams mLayout;
    private RelativeLayout top_back_selector,top_home_selector,top_voice_selector;
   private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private int layoutState;
    private String windowState;
    private ConnectivityManager connectivityManager;
    private NetworkInfo info;
    private Timer mTimer,disConnectNetTimer;
    private int disConnectNetTime=0; //断网的次数
    private Handler sdCardHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 0x01:
                    sendSdCardStateBroadcast(sdCardState);
                    ToastUtil.showToast("sd卡正常");
                    break;
                case 0x02:
                    sendSdCardStateBroadcast(sdCardState);
                    ToastUtil.showToast("sd卡已插入，但未挂载");
                    break;
                case 0x03:
                    sendSdCardStateBroadcast(sdCardState);
                    ToastUtil.showToast("sd卡被移除");
                    break;
                case 0x04:
                    sendSdCardStateBroadcast(sdCardState);
                    ToastUtil.showToast("sd卡已经从sd卡插槽拔出，但是挂载点还没解除");
                    break;
                case 0x06:
                    sendSdCardStateBroadcast(sdCardState);
                    ToastUtil.showToast("网络断开了!");
                    break;
                case 0x05:
                    sendSdCardStateBroadcast(sdCardState);
                    ToastUtil.showToast("网络已连接!");
                    break;
            }
            super.handleMessage(msg);

        }
    };
    private TimerTask mTimerTask,disConnectNetTimerTask;
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                Log.e("info", "网络状态已经改变");
                connectivityManager = (ConnectivityManager)
                        getSystemService(Context.CONNECTIVITY_SERVICE);
                info = connectivityManager.getActiveNetworkInfo();
                if(info != null && info.isAvailable()) {
                    String name = info.getTypeName();
                    sdCardHandler.sendEmptyMessage(0x05);
                    Log.e("info", "当前网络名称：" + name);
                    if(name.equals("mobile")){
                        Intent netNameIntent=new Intent();
                        netNameIntent.addCategory(Intent.CATEGORY_DEFAULT);
                        netNameIntent.setAction(ChappieConstant.NET_NAME);
                        netNameIntent.putExtra("netState","mobile");
                        sendBroadcast(netNameIntent);
                    }
                    if(mTimer!=null){
                        mTimer.cancel();
                        Log.e("info", "DameService网络恢复,断网计时器停止计时mTimer.cancel();");
                    }
                } else {
                    sendClearorSrartSocketBroadcast();
                    sdCardHandler.sendEmptyMessage(0x06);
                    disConnectNetTime++;
                    disConnectNetTimer=new Timer();
                     //发生断网后启动断网计时器
                    mTimer=new Timer();
                    mTimerTask=new TimerTask(){
                        @Override
                        public void run() {
                            //发送清空网络请求的广播
                            sendClearSocketOrderBroadcast();
                        }
                    };
                   mTimer.schedule(mTimerTask,30*60*1000);
                    Log.e("info", "网络断开,断网计时器开始计时");
                }
            }
        }
    };

    private void sendClearorSrartSocketBroadcast() {

    }

    private void sendClearSocketOrderBroadcast(){
        Intent intent=new Intent();
        intent.setAction(ChappieConstant.SEND_CLEAR_SOCKET_ORDER);
        sendBroadcast(intent);
    }
    private int sdCardState;
    private BroadcastReceiver sdCardStateBroadCastReceiver;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    /**
     * 注册网络状态监听广播
     */
   private void RegisterNetStateBrocadcast (){
       IntentFilter mFilter = new IntentFilter();
       mFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
       registerReceiver(mReceiver, mFilter);
   }


    /**
     * 注销网络状态监听广播
     */

    private void unRegisterNetStateBroadcast(){
        if(mReceiver!=null){
            unregisterReceiver(mReceiver);
        }
    }
    @Override
    public void onCreate() {
        super.onCreate();
        chappieCarApplication= (ChappieCarApplication) getApplication();
        sp=getSharedPreferences("deskstate", Activity.MODE_PRIVATE);
        editor=sp.edit();

        if(SDCardScannerUtil.isSecondSDcardMounted()){
            sdCardState=00;
            sendSdCardStateBroadcast(sdCardState);
        }else if (!SDCardScannerUtil.isSecondSDcardMounted()) {
            sdCardState=01;
            sendSdCardStateBroadcast(sdCardState);
        }
       if(isForeground(getApplicationContext(), "SettingActivity")){

           Log.e("info", "你在设置界面");
       } else{
           Log.e("info","你不在设置界面");
       };
        RegisterNetStateBrocadcast();
        initSdCardStateBroadCastReceiver();
        registerSdCardStateBrocast();
        /**
         * 当service运行在低内存的环境时，将会kill掉一些存在的进程。因此进程的优先级将会很重要，
         * 可以使用startForeground API将service放到前台状态。这样在低内存时被kill的几率更低
         */
        startForeground(0, new Notification());
        TimerTask task = new TimerTask() {

            @Override
            public void run() {
                //closeBar();
                ActivityManager mActivityManager;
                mActivityManager = (ActivityManager) DamenService.this.getSystemService(Context.ACTIVITY_SERVICE);
                ComponentName topActivity = mActivityManager.getRunningTasks(1).get(0).topActivity;
                String packageName = topActivity.getPackageName();
                String aName=getRunningActivityName();
                //Log.e("info","packageName:"+packageName+"aName:"+aName);
                if (packageName.equals("com.autonavi.minimap")||packageName.equals("com.android.settings")) {
                    //Log.e("appService", "appService>>>>>启动了高低地图");
                    layoutState = 2;
                    windowState="2";
                    editor.putString("layoutState", "2");
                    editor.putString("windowState","2");
                    editor.commit();
                    Intent intent=new Intent("com.miya.action.damenService");
                    Bundle bundle=new Bundle();
                    bundle.putString("windowState",sp.getString("windowState",""));
                    bundle.putString("layoutState","2");
                    bundle.putString("pName",packageName);
                    bundle.putString("aName",aName);
                    intent.putExtras(bundle);
                    sendBroadcast(intent);
                }else {
                    layoutState=1;
                    windowState="1";
                    editor.putString("layoutState", "1");
                    editor.putString("windowState","1");
                    editor.commit();
                    Intent intent=new Intent("com.miya.action.damenService");
                    Bundle bundle=new Bundle();
                    bundle.putString("layoutState","1");
                    bundle.putString("windowState",sp.getString("windowState",""));
                    bundle.putString("pName",packageName);
                    bundle.putString("aName",aName);
                    intent.putExtras(bundle);
                    sendBroadcast(intent);
                }
            }
        };

        Timer timer = new Timer();
        timer.schedule(task, 0, 1000);//从现在开始，每隔1秒运行一次run方法
    }
   private void registerSdCardStateBrocast(){
       IntentFilter intentFilter=new IntentFilter();
       intentFilter.setPriority(1000);// 设置最高优先级
       intentFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);//sd卡插入并已挂载
       intentFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);// sd卡存在，但还没有挂载
       intentFilter.addAction(Intent.ACTION_MEDIA_REMOVED);// sd卡被移除
       intentFilter.addAction(Intent.ACTION_MEDIA_BAD_REMOVAL);// sd卡已经从sd卡插槽拔出，但是挂载点还没解除
       intentFilter.addDataScheme("file");
       registerReceiver(sdCardStateBroadCastReceiver, intentFilter);
   }
    private void unRegisterSdCardStateBroadCastReceiver(){
        if(sdCardStateBroadCastReceiver!=null){
            unregisterReceiver(sdCardStateBroadCastReceiver);
        }
    }
    private void initSdCardStateBroadCastReceiver(){
        sdCardStateBroadCastReceiver=new BroadcastReceiver(){

            @Override
            public void onReceive(Context context, Intent intent) {
                String action=intent.getAction();
                switch (action){
                    case Intent.ACTION_MEDIA_MOUNTED:{ // sd卡正常
                        sdCardState=00;
                        sdCardHandler.sendEmptyMessage(0x01);
                        break;
                    }

                    case Intent.ACTION_MEDIA_UNMOUNTED:{  // sd卡已插入，但未挂载
                        sdCardState=01;
                        sdCardHandler.sendEmptyMessage(0x02);
                        break;
                    }

                    case Intent.ACTION_MEDIA_REMOVED:{  // sd卡被移除
                        sdCardState=01;
                        sdCardHandler.sendEmptyMessage(0x03);
                        break;
                    }

                    case Intent.ACTION_MEDIA_BAD_REMOVAL:{  // sd卡已经从sd卡插槽拔出，但是挂载点还没解除
                        sdCardState=01;
                        sdCardHandler.sendEmptyMessage(0x04);
                        break;
                    }
                }

            }
        };
    }
   private void sendSdCardStateBroadcast(int sdCardState){
       Intent intent=new Intent();
       intent.setAction(ChappieConstant.SEND_SDCARD_STATE);
       intent.putExtra("sdCardState",sdCardState);
       sendBroadcast(intent);
   }

    @Override
    public void onDestroy() {
        stopForeground(true);
        Intent localIntent = new Intent();
        localIntent.setClass(this, DamenService.class);
        this.startService(localIntent);    //销毁时重新启动Service
        unRegisterNetStateBroadcast();
        unRegisterSdCardStateBroadCastReceiver();
    }

    private String getRunningActivityName(){
        ActivityManager activityManager=(ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        String runningActivity=activityManager.getRunningTasks(1).get(0).topActivity.getClassName();
        return runningActivity;
    }
    /**
     * 判断某个界面是否在前台
     *
     * @param context
     * @param className
     *            某个界面名称
     */
    private boolean isForeground(Context context, String className) {
        if (context == null || TextUtils.isEmpty(className)) {
            return false;
        }

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(1);
        if (list != null && list.size() > 0) {
            ComponentName cpn = list.get(0).topActivity;
            if (className.equals(cpn.getClassName())) {
                return true;
            }
        }

        return false;
    }
}

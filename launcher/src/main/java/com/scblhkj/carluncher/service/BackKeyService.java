package com.scblhkj.carluncher.service;

/**
 * Created by blh on 2016-06-23.
 */

import android.accessibilityservice.AccessibilityService;
import android.content.res.Configuration;
import android.view.accessibility.AccessibilityEvent;

public class BackKeyService extends AccessibilityService
{
    public static BackKeyService backKeyService;

    public void onAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
    }

    public void onConfigurationChanged(Configuration paramConfiguration)
    {
        super.onConfigurationChanged(paramConfiguration);
    }

    public void onDestroy()
    {
        super.onDestroy();
    }

    public void onInterrupt()
    {
    }

    protected void onServiceConnected()
    {
        super.onServiceConnected();
        backKeyService = this;
    }
}
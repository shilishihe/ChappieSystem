package com.scblhkj.carluncher.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.activity.TMPSSettingActivity;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.db.LSInstructionDBImpl;
import com.scblhkj.carluncher.db.SerialDBInterfaceImpl;
import com.scblhkj.carluncher.domain.GPSInfo;
import com.scblhkj.carluncher.domain.LSInstructionBean;
import com.scblhkj.carluncher.domain.NewTPMSSerialBean;
import com.scblhkj.carluncher.domain.OBDSerialBean;
import com.scblhkj.carluncher.domain.SerialDataBean;
import com.scblhkj.carluncher.serialport.ComBean;
import com.scblhkj.carluncher.serialport.SerialHelper;
import com.scblhkj.carluncher.utils.CPPushUtil;
import com.scblhkj.carluncher.utils.CommonUtil;
import com.scblhkj.carluncher.utils.SerialFunUtil;
import com.scblhkj.carluncher.utils.SpUtil;
import com.scblhkj.carluncher.utils.ToastUtil;
import com.scblhkj.carluncher.utils.XORUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.io.IOException;
import java.lang.reflect.Method;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import okhttp3.Call;

/**
 * 串口通信的后台服务
 */
public class SerialDataCommSerivce extends Service {

    private SerialHelperControl serialHelperControl;

    public static final String SERIAL_OBD_ACTION = "com.scblhkj.chappie.carlauncher.obd.serialdata";
    public static final String SERIAL_TPMS_ACTION = "com.scblhkj.chappie.carlauncher.tpms.serialdata";
    public static final String SERIAL_NEW_TPMS_ACTION = "com.scblhkj.chappie.carlauncher.new.tpms.serialdata";  // 新胎压数据广播标识
    public static final String SERIAL_ACTION_RECEIVE = "com.scblhkj.chappie.carlauncher.receive.serialdata";
    public static final String UPLOAD_DATA_2_SERVICE_ACTION = "UPLOAD_DATA_2_SERVICE_ACTION";  // 上传数据到服务器
    // private static final String "info" = "SerialDataCommSerivce";
    private static final int PLAY_ALARM = 100;
    private static final int PLAY_ALARM_DELAY_TIME = 10000;
    private static final int SAVE_ALARM_DATA = 150;
    private static final int SAVE_ALARM_DATA_TIME = 1000;
    private BroadcastReceiver broadcastReceiver;
    private int[][] tempTPMS;
    private int[][] tempOBD;
    private int[][] tempNewTPMS; // 新胎压数据
    private SerialDBInterfaceImpl serialDao;
    private List<String> alarmCacheList; // 报警参数的缓存列表
    // 指令数据库Dao
    private LSInstructionDBImpl lsDao;
    private GPSInfo gpsInfo;

    /**
     * 初始化发送串口数据的广播接收者
     */
    private void initSendSerialDataReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch (action) {
                    case SerialDataCommSerivce.SERIAL_ACTION_RECEIVE: {  // 接收到广播，请求发送串口数据
                        Log.e("info", "收到了串口数据广播");
                        byte[] bs = intent.getExtras().getByteArray(TMPSSettingActivity.CMD); //获取到要发送给串口的指令
                        if (bs != null) {
                            sendPortData(serialHelperControl, bs);
                        } else {
                            Log.e("info", "待发送给串口的数据为空");
                            throw new RuntimeException("待发送给串口的数据为空");
                        }
                    }
                    case ChappieConstant.GPS_INFO: {
                        gpsInfo = (GPSInfo) intent.getExtras().getSerializable(ChappieConstant.GPS_INFO);
                        break;
                    }
                    case "com.scblhkj.chappie.hddvr.car.collision": {     // 车辆碰撞广播
                        Log.e("info", "串口服务中接收到汽车震动广播消息");
                        CPPushUtil.pushMsg(230, String.valueOf(CommonUtil.getTimeStamp()));
                        break;
                    }
                    case "com.scblhkj.chappie.carlauncher.obd.serialdata": {
                        OBDSerialBean bean = (OBDSerialBean) intent.getExtras().getSerializable("SERIAL_OBD_DATA");
                        int[][] datass = bean.getDatass();   // OBD数据
                        // Log.e("info","obd数据：》》》"+datass);
                        Map<String, String> params = buildParams(datass);

                        if (gpsInfo != null) {
                            params.put("latitude", gpsInfo.getLatitude() + "");
                            params.put("longitude", gpsInfo.getLongitude() + "");
                        }
                        OkHttpUtils.post().params(params).url("http://" + ChappieConstant.HTTP_HOST + "/app/RMCarCheck/rest").build().execute(new StringCallback() {
                            @Override
                            public void onError(Call call, Exception e) {
                            }

                            @Override
                            public void onResponse(String s) {
                                Log.e("info", s);
                            }
                        });
                        break;
                    }


                    default: {
                        break;
                    }
                }

              /* 车辆体检
                if ("com.scblhkj.chappie.carlauncher.obd.serialdata".equals(action)) {
                    OBDSerialBean bean = (OBDSerialBean) intent.getExtras().getSerializable("SERIAL_OBD_DATA");
                    int[][] datass = bean.getDatass();   // OBD数据
                     Log.e("info","obd数据：》》》"+datass);
                    Map<String, String> params = buildParams(datass);

                    if (gpsInfo != null) {
                        params.put("latitude", gpsInfo.getLatitude() + "");
                        params.put("longitude", gpsInfo.getLongitude() + "");
                    }
                    OkHttpUtils.post().params(params).url("http://" + ChappieConstant.HTTP_HOST + "/app/RMCarCheck/rest").build().execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e) {

                        }

                        @Override
                        public void onResponse(String s) {
                            Log.e("info", s);
                        }
                    });
                }

        */

            }
        };
    }


    private Map<String, String> buildParams(int[][] datass) {
        Map<String, String> params = new HashMap<String, String>();
        for (int i = 0; i < datass.length; i++) {
            //  System.out.println("datass>>>"+datass);
            if (datass[i][0] == 0X28) {
                if (datass[i][8] == 0x85) {
                    float result = datass[i][31] / 10f;  // 在此可以显示实时电压
                } else if (datass[i][8] == 0X8A) {  // 车辆体检
                    if ((datass[i][12] & 0x01) == 0x01) {  // 发动机系统
                        Log.e("info", "发动机无故障");
                        params.put("engine", "0");
                    } else {
                        Log.e("info", "发动机有故障");
                        params.put("engine", "-1");
                    }
                    if ((datass[i][12] & 0x04) == 0x04) {  // 节气门系统
                        Log.e("info", "节气门开度正常");
                        params.put("term_door", "0");
                    } else {
                        Log.e("info", "节气门开度异常");
                        params.put("term_door", "-1");
                    }
                    if ((datass[i][12] & 0x10) == 0x10) {  // 冷却系统s
                        Log.e("info", "冷却系统良好");
                        params.put("cool_sys", "0");
                    } else {
                        Log.e("info", "冷却系统异常");
                        params.put("cool_sys", "-1");
                    }

                    if ((datass[i][12] & 0x08) == 0x08) {// 排放系统
                        Log.e("info", "排放系统良好");
                        params.put("emission_sys", "0");
                    } else {
                        Log.e("info", "排放系统异常");
                        params.put("emission_sys", "-1");
                    }

                    if ((datass[i][12] & 0x02) == 0x02) {     // 电源系统s
                        Log.e("info", "电源系统电路正常");
                        params.put("power_sys", "0");
                    } else {
                        Log.e("info", "电源系统电路异常");
                        params.put("power_sys", "0");
                    }
                    if ((datass[i][12] & 0x20) == 0x20 || (datass[i][12] & 0x40) == 0x40) {  // 怠速系统
                        Log.e("info", "怠速系统高或者怠速低");
                        params.put("idling_sys", "-1");
                    } else {
                        params.put("idling_sys", "0");
                    }
                    params.put("intake_sys", "0");  // 进气系统
                }
            } else if (datass[i][8] == 0x91) {  // 获取报警消息
                Log.e("info", "获取报警消息");
            }
        }
        return params;
    }

    public SerialDataCommSerivce() {
    }

    @Override
    public void onCreate() {
        alarmPlayer = new MediaPlayer();
        lsDao = new LSInstructionDBImpl(this);
        serialDao = new SerialDBInterfaceImpl(this);
        alarmCacheList = new LinkedList<String>();
        startSerialPostComm();
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * 开始串口通信
     */
    private void startSerialPostComm() {
        Log.e("info", "开始串口通信...");
        serialHelperControl = new SerialHelperControl();
        initSendSerialDataReceiver();
        registerSerialSendDataReceiver();
        openComPort(serialHelperControl);
    }

    /**
     * 打开串口
     *
     * @param serialHelper
     */
    private void openComPort(SerialHelper serialHelper) {
        try {
            Log.e("info", "开始打开串口");
            //serialHelper.open();
            serialHelper.openNewObd();
        } catch (SecurityException e) {
            Log.e("info", "打开串口失败:没有串口读/写权限!");
            ToastUtil.showToast("打开串口失败:没有串口读/写权限!");
        } catch (IOException e) {
            Log.e("info", "打开串口失败:参数错误!");
            ToastUtil.showToast("打开串口失败:未知错误!");
        } catch (InvalidParameterException e) {
            Log.e("info", "打开串口失败:参数错误!");
            ToastUtil.showToast("打开串口失败:参数错误!");
        }
    }

    @Override
    public void onDestroy() {
        closeComPort(serialHelperControl);
        unRegisterSerialSendDataReceiver();
        super.onDestroy();
    }

    /**
     * 关闭串口
     */
    private void closeComPort(SerialHelper serialHelper) {
        Log.e("info", "关闭串口通信...");
        if (serialHelper != null) {
            serialHelper.stopSend();
            serialHelper.close();
        }
    }

    /**
     * 发送汽车行驶或者停止的广播
     */
    private void sendCarRunningCast() {
        Intent intent = new Intent();
        intent.setAction("com.cp.cast.CAR_RUNNING");
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intent);
    }


    /**
     * Created by Leo on 2015/11/4.
     */
    public class SerialHelperControl extends SerialHelper {

       /* private static final int CLOCK_ADD = 0;  // 时钟加1
        private static final int CLOCK_RESET = 1; // 时钟重置
        private boolean CAR_RUNNING = false; // 汽车行驶中
        private long clock = 0;
        private Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case CLOCK_ADD: {
                        clock++;
                        if (CAR_RUNNING) {
                            handler.sendEmptyMessageDelayed(CLOCK_ADD, 1000);
                        }
                        break;
                    }
                    case CLOCK_RESET: {
                        CAR_RUNNING = false;
                        clock = 0; // 计数清零
                        break;
                    }
                    default: {
                        break;
                    }
                }
                super.handleMessage(msg);
            }
        };*/


        private byte[][] datass;
        private byte[] bytes;
        private String oneceTime; // 本次驾驶时长
        private String onceMile; // 本次驾驶里程


        public static final String SERIAL_TMPA_DATA = "SERIAL_TMPA_DATA";
        public static final String SERIAL_NEW_TMPA_DATA = "SERIAL_NEW_TMPA_DATA";
        public static final String SERIAL_OBD_DATA = "SERIAL_OBD_DATA";

        public SerialHelperControl() {
        }

        @Override
        protected void onDataReceived(ComBean comRecData) {
            // 数据接收量大或接收时弹出软键盘，界面会卡顿,可能和6410的显示性能有关
            // 直接刷新显示，接收数据量大时，卡顿明显，但接收与显示同步。
            // 用线程定时刷新显示可以获得较流畅的显示效果，但是接收数据速度快于显示速度时，显示会滞后。
            // 最终效果差不多-_-，线程定时刷新稍好一些。
            // 将数据广播出去
            sendSeriaDataBroadCast(comRecData);
        }

        @Override
        protected void restartSerialHelper() {  // 串口出现异常则重启串口
            startSerialPostComm();
        }

        /**
         * 发送窗口数据的广播
         */
        private void sendSeriaDataBroadCast(ComBean comRecData) {

            tempTPMS = new int[5][100];
            tempOBD = new int[5][100];
            tempNewTPMS = new int[5][100];

            bytes = comRecData.getbRec();
            if (bytes != null && bytes.length > 0) {   // 如果数据长度有效则发送广播
                sendCarRunningCast();
            }

            // 封装成一个二维数组

            String origin = SerialFunUtil.ByteArrToHex(bytes); // 原始的串口数据
            //   Log.e("info","原始的串口数据>>>>"+origin);
            int x = CommonUtil.getSubCount(origin, "01FF");
            datass = new byte[x][255];
            int j = 0;
            int k = 0;
            /**
             * for循环解析会出现问题 解决
             */
            for (int i = 0; i < (bytes.length - 5); ) {   // 截包封装到二维数组
                if ((bytes[i] == (byte) 0X01) && (bytes[i + 1] == (byte) 0Xff)) {//寻找包头&& ((bytes[i + 2] == (byte) 0X02) || (bytes[i + 2] == (byte) 0X03) || (bytes[i + 2] == (byte) 0X05) || (bytes[i + 2] == (byte) 0X00)
//                    if ((bytes[i + 2] == (byte) 0X02) || ((bytes[i + 2] == (byte) 0X03 && (bytes[bytes[i + 3] + 3 + i] == 0X29)) || (bytes[i + 2] == (byte) 0X05)) {//筛选有效数据
                    if ((bytes[i + 2] == (byte) 0X02) || (bytes[i + 2] == (byte) 0X03) || (bytes[i + 2] == (byte) 0X00) || (bytes[i + 2] == (byte) 0X05)) {//筛选有效数据
                        k = i;
                        for (int m = k; m <= bytes[k + 3] + 3 + k; m++) {
                            datass[j][m - k] = bytes[m];
                            i++;
                            if (m > 99) {
                                break;
                            }
                        }
                        j++;
                    }
                } else {
                    i++;
                }
                if (j >= 5) {
                    break;
                }
            }

            int ti = 0;
            int oi = 0;
            int ni = 0;
            //-------------------------------------------------分离二维数组
            for (int i = 0; i < datass.length; i++) {
                if (datass[i][0] == (byte) 0x01 && datass[i][1] == (byte) 0xff && datass[i][2] == (byte) 0x05) {  // 长沙路吉通胎压数据
                    if ((datass[i][4] == (byte) 0xaa && datass[i][5] == (byte) 0xaa) && (datass[i][datass[i][3] + 2] == (byte) 0xbb && datass[i][datass[i][3] + 3] == (byte) 0xbb)) { // 数据校验
                        tempTPMS[ti] = XORUtil.doBytes2Ints(datass[i]);
                        StringBuffer sb = new StringBuffer();
                        if (tempTPMS[ti][6] == 0XA1) {  // 判断是胎压通用数据包
                            sb.append((tempTPMS[ti][7] * 256 + tempTPMS[ti][8] - 100) + "," + tempTPMS[ti][9]);  // 左前轮
                            sb.append("," + (tempTPMS[ti][12] * 256 + tempTPMS[ti][13] - 100) + "," + tempTPMS[ti][14]); //右前轮
                            sb.append("," + (tempTPMS[ti][17] * 256 + tempTPMS[ti][18] - 100) + "," + tempTPMS[ti][19]); //左后轮
                            sb.append("," + (tempTPMS[ti][22] * 256 + tempTPMS[ti][23] - 100) + "," + tempTPMS[ti][24]); //右后轮
                        }
                        getTpmsData2Service(sb.toString());
                        Log.e("info", "一维的胎压数据:" + sb.toString());
                        ti++;
                    }
                } else if (datass[i][0] == (byte) 0x01 && datass[i][1] == (byte) 0xff && datass[i][2] == (byte) 0x00) {  // 电压数据
                    int powerValue = XORUtil.doByte2UnInt(datass[i][4]) * 256 + XORUtil.doByte2UnInt(datass[i][5]);
                    if (powerValue <= 1180) {
                        Log.e("info", "电压过低发送推送消息到服务器");
                        CPPushUtil.pushMsg(240, String.valueOf(powerValue));
                        // 弹出5秒倒计时


                    } else {
                        Log.e("info", "电压值正常当前电压值为" + powerValue + "mV");
                    }
                } else if (datass[i][0] == (byte) 0x01 && datass[i][1] == (byte) 0xff && datass[i][2] == (byte) 0x03) {  // OBD数据
                    if ((datass[i][4] == (byte) 0x28) && (datass[i][datass[i][3] + 3] == (byte) 0x29)) {  // 数据校验
                        StringBuilder sb = new StringBuilder();
                        for (int mm = 5; mm < 11; mm++) {
                            String hex = SerialFunUtil.Byte2Hex(datass[i][mm]) + "-";
                            sb.append(hex);
                            //   Log.e("info", "obd..SB>>>" + sb.toString());
                        }
                        SpUtil.saveString2SP(ChappieConstant.OBD_SERIAL_ID, sb.toString());
                        // Log.e("info","obd....SBAPPEND>>>" + sb.toString());
                        /**
                         * 数据转义，屏蔽0X3D
                         */
                        byte[] bs = new byte[100];  // bs不带01 ff
                        byte b = 0;
                        //    int rr[] = new int[datass[i].length];
                        for (int m = 0; m < datass[i][3]; m++) {     // 出现问题（重要）
                            if (datass[i][m + 4] == (byte) 0X3D) {
                                bs[b] = (byte) (datass[i][m + 4] ^ datass[i][m + 5]);
                                i++;
                            } else {
                                bs[b] = datass[i][m + 4];
                            }
                            b++;
                        }
                        tempOBD[oi] = XORUtil.doBytes2Ints(bs);

                        //---------------打印bs[]
                        if (bs[8] == (byte) 0x85) {
                            long rr1 = XORUtil.doByte2UnLong((byte) bs[20]) * 256 + XORUtil.doByte2UnLong((byte) bs[21]);  // 本次驾驶时间（分钟）
                            long rr3 = XORUtil.doByte2UnLong(bs[23]) << 24;
                            long rr4 = XORUtil.doByte2UnLong(bs[24]) << 16;
                            long rr5 = XORUtil.doByte2UnLong(bs[25]) << 8;
                            long rr6 = XORUtil.doByte2UnLong(bs[26]) << 0;
                            oneceTime = String.valueOf(rr1);
                            onceMile = String.valueOf(rr3 + rr4 + rr5 + rr6);
                            Log.e("info", "汽车熄火总里程=" + onceMile + "总行驶时间" + oneceTime);
                        }

                        if (bs[8] == (byte) 0x84) {//点火熄火数据
                            if (bs[11] == (byte) 0x01) {//点火指令
                                CPPushUtil.pushMsg(210, "");
                                Log.e("info", "汽车点火");
                            } else if (bs[11] == (byte) 0x00) {//熄火指令
                                CPPushUtil.pushMsg(210, onceMile + "," + oneceTime);
                            }
                        }

                       /* StringBuilder sb2 = new StringBuilder();
                        // TODO: 2016/4/11
                        for (int i1 = 0; i1 < XORUtil.doBytes2Ints(bs).length; i1++) {
                            sb2.append(XORUtil.doBytes2Ints(bs)[i1] + ",");
                        }
                        Log.e("info", "sb2 = " + sb2.toString());*/
                        oi++;
                        sendOBDData2Service(bs); // 将串口数据发送至服务器
                        Log.e("info", "将串口数据发送至服务器=" + bs.toString());
                    }
                }/* else if (datass[i][0] == (byte) 0x01 && datass[i][1] == (byte) 0xff && datass[i][2] == (byte) 0x04) {  // 永奥图新胎压数据
                    byte sum = 0;
                    for (int q = 0; q < datass[i][3] - 1; q++) {  // 数据校验
                        sum = (byte) (sum + datass[i][4 + q]);
                    }
                    if (sum == datass[i][3 + datass[i][3]]) {    // 有效数据
                        tempNewTPMS[ni] = XORUtil.doBytes2Ints(datass[i]);
                        ni++;
                        Log.d("info", "新胎压数据有效");
                    }
                }*/

            }

            // float maxPre = SpUtil.getFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX);  // 压力上限值
            // float minPre = SpUtil.getFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN);  // 压力下限值;
            // float temPre = SpUtil.getFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_TEMP_MAX);  // 温度上限值


            for (int i = 0; i < datass.length; i++) { // 异常报警处理
                if (datass[i][0] == (byte) 0x01 && datass[i][1] == (byte) 0xff && datass[i][2] == (byte) 0x02 && datass[i][3] == (byte) 0x0a && datass[i][6] == (byte) 0x0a) { //胎压数据流 4S/次
                    ChappieCarApplication.tpmsCaches[0] = ((byte) 0x02);//为胎牛TPMS
                    if ((byte) 0x00 == datass[i][7]) {//胎压数据赋值
                        ChappieCarApplication.tpmsCaches[1] = (datass[i][8]); //压力
                        ChappieCarApplication.tpmsCaches[2] = (datass[i][9]); //温度
                        ChappieCarApplication.tpmsCaches[3] = (datass[i][10]);//状态
                    } else if ((byte) 0x01 == datass[i][7]) {
                        ChappieCarApplication.tpmsCaches[4] = (datass[i][8]); //压力
                        ChappieCarApplication.tpmsCaches[5] = (datass[i][9]); //温度
                        ChappieCarApplication.tpmsCaches[6] = (datass[i][10]);//状态
                    } else if ((byte) 0x10 == datass[i][7]) {
                        ChappieCarApplication.tpmsCaches[7] = (datass[i][8]); //压力
                        ChappieCarApplication.tpmsCaches[8] = (datass[i][9]); //温度
                        ChappieCarApplication.tpmsCaches[9] = (datass[i][10]);//状态
                    } else if ((byte) 0x11 == datass[i][7]) {
                        ChappieCarApplication.tpmsCaches[10] = (datass[i][8]); //压力
                        ChappieCarApplication.tpmsCaches[11] = (datass[i][9]); //温度
                        ChappieCarApplication.tpmsCaches[12] = (datass[i][10]);//状态
                    }


                } else if (datass[i][0] == (byte) 0x01 && datass[i][1] == (byte) 0xff && datass[i][2] == (byte) 0x04 && datass[i][3] == (byte) 0xaa) {//新胎压数据
                   /*
                       tempNewTPMS[i] = XORUtil.doBytes2Ints(datass[i]);
                       if ((byte) 0x63 == datass[i][8]) {
                        ChappieCarApplication.tpmsBs[0] = 0x04;//为永奥图TPMS
                        if ((byte) 0x00 == datass[i][7]) {//胎压数据赋值
                            ChappieCarApplication.tpmsBs[1] = datass[i][8];
                            ChappieCarApplication.tpmsBs[2] = datass[i][9];
                            ChappieCarApplication.tpmsBs[3] = datass[i][10];
                        } else if ((byte) 0x01 == datass[i][7]) {
                            ChappieCarApplication.tpmsBs[4] = datass[i][8];
                            ChappieCarApplication.tpmsBs[5] = datass[i][9];
                            ChappieCarApplication.tpmsBs[6] = datass[i][10];
                        } else if ((byte) 0x10 == datass[i][7]) {
                            ChappieCarApplication.tpmsBs[7] = datass[i][8];
                            ChappieCarApplication.tpmsBs[8] = datass[i][9];
                            ChappieCarApplication.tpmsBs[9] = datass[i][10];
                        } else if ((byte) 0x11 == datass[i][7]) {
                            ChappieCarApplication.tpmsBs[10] = datass[i][8];
                            ChappieCarApplication.tpmsBs[11] = datass[i][9];
                            ChappieCarApplication.tpmsBs[12] = datass[i][10];
                        }
                    }*/
                } else {   // OBD数据
                    //tempOBD[i] = XORUtil.doBytes2Ints(datass[i]);
                }
            }
            sendBroadCast2OBD(tempOBD);  // OBD数据
            // Log.e("info", "发送OBD广播数据");
            sendBroadCast2TPMS(tempTPMS); // 发送胎压数据广播
            Log.e("info", "发送广播数据");
        }
    }

    /**
     * 关机代码
     */
    private void shutDown() {
        try {
            //获得ServiceManager类
            Class<?> ServiceManager = Class.forName("android.os.ServiceManager");
            //获得ServiceManager的getService方法
            Method getService = ServiceManager.getMethod("getService", java.lang.String.class);
            //调用getService获取RemoteService
            Object oRemoteService = getService.invoke(null, Context.POWER_SERVICE);
            //获得IPowerManager.Stub类
            Class<?> cStub = Class.forName("android.os.IPowerManager$Stub");
            //获得asInterface方法
            Method asInterface = cStub.getMethod("asInterface", android.os.IBinder.class);
            //调用asInterface方法获取IPowerManager对象
            Object oIPowerManager = asInterface.invoke(null, oRemoteService);
            //获得shutdown()方法
            Method shutdown = oIPowerManager.getClass().getMethod("shutdown", boolean.class, boolean.class);
            //调用shutdown()方法
            shutdown.invoke(oIPowerManager, false, true);
        } catch (Exception e) {
            Log.e("info", e.toString(), e);
        }
    }

    /**
     * 关机对话框
     */
    private void shouShutDownDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
        builder.setMessage("车辆电瓶电压过低，系统将在5秒后自动关机");
        builder.setTitle("警告");
        builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                shutDown();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }


    /**
     * 将胎压通用数据通过http短链接
     * 发送胎压通用数据和OBD体检数据
     *
     * @return
     */
    private void getTpmsData2Service(String data) {
        // http://localhost/app/RMTirePressure/rest/ IEMI/设备号/tire_pressure/胎压数据
        String url = "http://" + ChappieConstant.HTTP_HOST + "/app/RMTirePressure/rest/IEMI/" + ChappieCarApplication.DEVICE_IMEI + "/tire_pressure/" + data;
        Log.e("info", "url" + url);
        OkHttpUtils.get().url(url).build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e) {

            }

            @Override
            public void onResponse(String s) {
                Log.e("info", "**getTpmsData2Service*" + s);
            }
        });
    }

    /**
     * 播放缓存报警
     *
     * @param list
     */
    private void playCacheListAlarm(List<String> list) {
        int length = list.size();
        if (ChappieCarApplication.isDebug)
            Log.e("info", "缓存集合的长度为" + length);
        if (length <= 0) {
            return;
        }
        for (int i = 0; i < length; i++) {
            String alarm = list.get(i);
            switch (alarm) {
                case ChappieConstant.ALARM_Battery_Voltage_LOW: {
                    playAlarm(R.raw.battery_voltage_low);
                    break;
                }
                case ChappieConstant.ALARM_OVERSPEED: {
                    playAlarm(R.raw.alarm_overspeed);
                    break;
                }
                case ChappieConstant.ALARM_FATIGUE_DRIVING: {
                    playAlarm(R.raw.alarm_fatigue_driving);
                    break;
                }
                case ChappieConstant.ALARM_CHARGING_CIRCUIT_ANOMALIES: {
                    playAlarm(R.raw.alarm_charging_circuit_anomalies);
                    break;
                }
                case ChappieConstant.ALARM_ECM_ERROR: {
                    playAlarm(R.raw.alarm_ecm_error);
                    break;
                }
                case ChappieConstant.ALARM_ENGINE_COOLANT_HOT: {
                    playAlarm(R.raw.alarm_engine_coolant_hot);
                    break;
                }
                case ChappieConstant.ALARM_ENGINE_COOLANT_COOL: {
                    playAlarm(R.raw.alarm_engine_coolant_cool);
                    break;
                }
                case ChappieConstant.ALAEM_MAINTENANCE_ALERT: {
                    playAlarm(R.raw.alaem_maintenance_alert);
                    break;
                }
                case ChappieConstant.ALARM_CLEAN_REMIND_THROTTLE: {
                    playAlarm(R.raw.alarm_clean_remind_throttle);
                    break;
                }
                case ChappieConstant.ALARM_DEVICE_REMOVE: {
                    playAlarm(R.raw.alarm_device_remove);
                    break;
                }
                case ChappieConstant.ALARM_URGENT_ACCELERATE: {
                    playAlarm(R.raw.alarm_urgent_accelerate);
                    break;
                }
                case ChappieConstant.ALARM_SHARP_SLOWDOWN: {
                    playAlarm(R.raw.alarm_sharp_sdown);
                    break;
                }
                default: {
                    break;
                }
            }
            list.remove(i);
            if (ChappieCarApplication.isDebug)
                Log.e("info", "删除播放过的报警声音");
        }
    }

    /**
     * 告警播报
     *
     * @param datass
     */
    private void detectionAlertReport(int[][] datass) {
        int length = datass.length;
        for (int i = 0; i < length; i++) {
            if (datass[i][0] == 0x28 && datass[i][8] == 0x85) {
                float result = datass[i][31] / 10f;
                // 在此可以显示实时电压
            } else if (datass[i][0] == 0x28 && datass[i][8] == 0x91) {
                StringBuilder sb = new StringBuilder();
                for (int jj = 0; jj < datass[i].length; jj++) {
                    sb.append(datass[i][jj] + "-");
                }
                switch (datass[i][11]) {
                    case 0x00: {
                        if (ChappieCarApplication.isDebug)
                            Log.e("info", "超速报警" + datass[i][13] + "KM/H");
                        alarmCacheList.add(ChappieConstant.ALARM_OVERSPEED); // 超速报警
                        break;
                    }
                    case 0x01: {
                        if (ChappieCarApplication.isDebug)
                            Log.e("info", "疲劳驾驶" + datass[i][13] + "-" + datass[i][14] + "KM/H");
                        alarmCacheList.add(ChappieConstant.ALARM_FATIGUE_DRIVING); // 疲劳驾驶
                        break;
                    }
                    case 0x02: {
                        if (ChappieCarApplication.isDebug)
                            Log.e("info", "电瓶欠压" + (datass[i][13] / 10f) + "V");
                        alarmCacheList.add(ChappieConstant.ALARM_Battery_Voltage_LOW);  // 电瓶电压低提醒
                        break;
                    }
                    case 0x03: {
                        if (ChappieCarApplication.isDebug)
                            Log.e("info", "充电电路异常" + datass[i][13] + "V");
                        alarmCacheList.add(ChappieConstant.ALARM_CHARGING_CIRCUIT_ANOMALIES);  // 充电电路异常
                        break;
                    }
                    case 0x04: {
                        if (ChappieCarApplication.isDebug)
                            Log.e("info", "ECM异常");
                        alarmCacheList.add(ChappieConstant.ALARM_ECM_ERROR); // ECM异常
                        break;
                    }
                    case 0x05: {
                        if (ChappieCarApplication.isDebug)
                            Log.e("info", "冷却液温度过高" + datass[i][13] + "摄氏度");
                        alarmCacheList.add(ChappieConstant.ALARM_ENGINE_COOLANT_HOT); // 冷却液温度过高
                        break;
                    }
                    case 0x06: {
                        if (ChappieCarApplication.isDebug)
                            Log.e("info", "冷却液温度过低" + datass[i][13] + "摄氏度");
                        alarmCacheList.add(ChappieConstant.ALARM_ENGINE_COOLANT_COOL); //冷却液温度过低
                        break;
                    }
                    case 0x07: {
                        if (ChappieCarApplication.isDebug)
                            Log.e("info", "保养提醒");
                        alarmCacheList.add(ChappieConstant.ALAEM_MAINTENANCE_ALERT); //保养提醒
                        break;
                    }
                    case 0x08: {
                        if (ChappieCarApplication.isDebug)
                            Log.e("info", "节气门清洁提醒");
                        alarmCacheList.add(ChappieConstant.ALARM_CLEAN_REMIND_THROTTLE); // 节气门清洗提醒
                        break;
                    }
                    case 0x09: {
                        if (ChappieCarApplication.isDebug)
                            Log.e("info", "设备被拔掉提醒");
                        alarmCacheList.add(ChappieConstant.ALARM_DEVICE_REMOVE);//设备拔掉提醒
                        break;
                    }
                    case 0x0A: {
                        if (ChappieCarApplication.isDebug)
                            Log.e("info", "急加速提醒");
                        alarmCacheList.add(ChappieConstant.ALARM_URGENT_ACCELERATE); //急加速提醒
                        break;
                    }
                    case 0x0B: {
                        if (ChappieCarApplication.isDebug)
                            alarmCacheList.add(ChappieConstant.ALARM_SHARP_SLOWDOWN); // 急减速提醒
                        Log.e("info", "急减速提醒");
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        }
        playCacheListAlarm(alarmCacheList);
    }

    private int timeClock = 0;

    /**
     * 将串口数据插入到数据库
     *
     * @param serialData
     */
    private void recordSerialData(String serialData) {
        try {
            SerialDataBean bean = new SerialDataBean();
            bean.setOriginData(serialData);
            bean.setTimestamp(System.currentTimeMillis() / 1000L);
            bean.setUpload(-1);
            if (serialDao.inserData(bean) > 0) {
                //    Log.d("info", "串口数据插入成功");
            } else {
                Log.d("info", "串口数据插入失败");
            }
        } catch (Exception e) {
            if (ChappieCarApplication.isDebug)
                Log.e("info", "[RouteRecordService]startRecordRoute catch exception:" + e.toString());
        }
    }

    // ----------------------------------------------------串口发送
    private void sendPortData(SerialHelper serialHelper, byte[] bs) {
        if (serialHelper != null && serialHelper.isOpen()) {
            serialHelper.send(bs);
        }
    }


    /**
     * 异或校验检查数据的准确性
     */

    private boolean doCheckData(byte[] bs, byte tempb) {
        byte b = bs[4];
        int l = bs[3];
        for (int i = 5; i < l + 3; i++) {
            b = (byte) (b ^ bs[i]);
        }
        if (b == tempb) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * signe的转unsigned int
     *
     * @return
     */
    private int parse2Int(byte b) {
        int i = b;
        int j = i;
        j = i & 0Xff;
        return j;
    }

    private Intent intent;

/*    */
    /**
     * 根据服务器收到的消息，显示指定的通知
     *//*
    private void showNotification(Context context, byte b) {
        // 创建一个NotificationManager的引用
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(android.content.Context.NOTIFICATION_SERVICE);
        int icon = R.mipmap.ic_launcher;
        long when = System.currentTimeMillis();
        Notification notification = new Notification(icon, "胎压告警", when);
        intent = new Intent(context, TMPSActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
        notification.defaults = Notification.FLAG_AUTO_CANCEL;
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        notification.setLatestEventInfo(context, "超级查派", "胎压告警," + parse2Int(b) + "号轮胎炸了", pIntent);
        notificationManager.notify(NOTIFY_ID++, notification);
    }*/

    private MediaPlayer alarmPlayer;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case SAVE_ALARM_DATA: {
                    startSaveAlarmState();
                    break;
                }
                default: {
                    break;
                }
            }


            super.handleMessage(msg);
        }
    };


    private static final int AUDIO_SESSION_ID = 200;

    /**
     * 播放告警声音
     */
    private void playAlarm(int vStyle) {
        if (!alarmPlayer.isPlaying()) {
            Uri alarmFileUri = Uri.parse("android.resource://" + this.getPackageName() + "/" + vStyle);
            alarmPlayer = MediaPlayer.create(this, alarmFileUri);
            alarmPlayer.start();
        }
       /* else {
            if (!alarmPlayer.isPlaying()) {
                if (alarmCacheList != null && alarmCacheList.size() > 0) {
                    playCacheListAlarm(alarmCacheList);
                }
            }
        }*/
    }

    /**
     * 关闭告警声音
     */
    private void stopAlarm(boolean y) {
        if (alarmPlayer != null) {    // 关掉所有
            if (y) {
                alarmPlayer.setLooping(false);
                alarmPlayer.release();
                alarmPlayer.stop();
                alarmPlayer = null;
            } else {   // 判断要关闭的是否是正在播放的
                if (AUDIO_SESSION_ID == alarmPlayer.getAudioSessionId()) {
                    if (ChappieCarApplication.isDebug)
                        Log.e("info", "当前播放的告警和准备要停止的告警一致，无需关闭");
                } else {
                    if (ChappieCarApplication.isDebug)
                        Log.e("info", "停止当前告警");
                    alarmPlayer.setLooping(false);
                    alarmPlayer.stop();
                    alarmPlayer = null;
                }
            }
        }
    }

    private static int sysLock = 0;
    private static int tmpsState = 0;
    private static int alarmCount = 0;

    /**
     * 开始每隔一秒存储串口数据的状态
     */
    private void startSaveAlarmState() {
        // 从文件中获取报警状态
        int serialDataState = Integer.valueOf(SpUtil.getString2SP(ChappieConstant.SERIAL_DATA_COMM_SERVICE_STATE));
        sysLock++;
        if (sysLock > 50000) {//sysLock 超出范围 清零
            sysLock = 0;
        }
        if (serialDataState == 1) { // 获取报警状态 一秒一次 如果有报警alarmCount++ ----------------------------
            alarmCount++;
            if (alarmCount == 1) {
                //        playAlarm(R.raw.yc);
            }
        }
        if (alarmCount > 15) {
            alarmCount = 0;//清除报警计时
            tmpsState = 0;//清除报警标志位
        }
        Message msg = Message.obtain();
        msg.what = SAVE_ALARM_DATA;
        handler.sendMessageDelayed(msg, SAVE_ALARM_DATA_TIME);
    }

    /**
     * 注册接收发送指令的广播接收者
     */
    private void registerSerialSendDataReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SerialDataCommSerivce.SERIAL_ACTION_RECEIVE);
        intentFilter.addAction(ChappieConstant.GPS_INFO);
        intentFilter.addAction("com.scblhkj.chappie.carlauncher.obd.serialdata");
        intentFilter.addAction("com.scblhkj.chappie.hddvr.car.collision");  // 汽车震动检测广播
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    /**
     * 注销广播监听
     */
    private void unRegisterSerialSendDataReceiver() {
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
            broadcastReceiver = null;
        }
    }

    /**
     * 发送新胎压数据广播
     *
     * @param
     */
    private void sendBroadCast2NewTPMS(int[][] datass) {
        NewTPMSSerialBean bean = new NewTPMSSerialBean();
        bean.setDatass(datass);
        Intent dataIntent = new Intent();
        dataIntent.setAction(SerialDataCommSerivce.SERIAL_NEW_TPMS_ACTION);
        dataIntent.addCategory(Intent.CATEGORY_DEFAULT);
        Bundle bundle = new Bundle();
        bundle.putSerializable(SerialHelperControl.SERIAL_NEW_TMPA_DATA, bean);
        dataIntent.putExtras(bundle);
        sendBroadcast(dataIntent);
    }

    /**
     * 发送胎压数据广播
     */
    private void sendBroadCast2TPMS(int[][] datass) {
        for (int i = 0; i < datass.length; i++) {
            if (datass[i][0] == 0x01 && datass[i][1] == 0xff && datass[i][2] == 0x05) {
                Intent dataIntent = new Intent();
                dataIntent.setAction(SerialDataCommSerivce.SERIAL_TPMS_ACTION);
                dataIntent.addCategory(Intent.CATEGORY_DEFAULT);
                Bundle bundle = new Bundle();
                bundle.putIntArray(SerialHelperControl.SERIAL_TMPA_DATA, datass[i]);
                dataIntent.putExtras(bundle);
                sendBroadcast(dataIntent);
            }
        }

       /* TPMSSerialBean bean = new TPMSSerialBean();
        bean.setDatasss(datass);
        Intent dataIntent = new Intent();
        dataIntent.setAction(SerialDataCommSerivce.SERIAL_TPMS_ACTION);
        dataIntent.addCategory(Intent.CATEGORY_DEFAULT);
        Bundle bundle = new Bundle();
        bundle.putSerializable(SerialHelperControl.SERIAL_TMPA_DATA, bean);
        dataIntent.putExtras(bundle);
        sendBroadcast(dataIntent);*/
    }

    /**
     * 发送OBD数据广播
     */
    private void sendBroadCast2OBD(int[][] datass) {
        detectionAlertReport(datass); // 告警数据
        OBDSerialBean bean = new OBDSerialBean();
        bean.setDatass(datass);
        Intent dataIntent = new Intent();
        dataIntent.setAction(SerialDataCommSerivce.SERIAL_OBD_ACTION);
        dataIntent.addCategory(Intent.CATEGORY_DEFAULT);
        Bundle bundle = new Bundle();
        bundle.putSerializable(SerialHelperControl.SERIAL_OBD_DATA, bean);
        dataIntent.putExtras(bundle);
        sendBroadcast(dataIntent);
    }


    /**
     * 发送广播给核心服务中的长连接，要长连接将接收到的数据发送给服务器
     */
    private void sendBroadCast2LongService(String data) {
        Intent dataIntent = new Intent();
        dataIntent.setAction(SerialDataCommSerivce.UPLOAD_DATA_2_SERVICE_ACTION);
        dataIntent.addCategory(Intent.CATEGORY_DEFAULT);
        dataIntent.putExtra(CCLuncherCoreService.SERIAL_UPLOAD_DATA, data);
        sendBroadcast(dataIntent);
    }


    /**
     * 发送指令到长连接服务
     *
     * @param bs
     */
    private void sendCMD2LongService(byte[] bs) {
        try {
            StringBuilder cmdSB = new StringBuilder();
            //    StringBuilder tempSB = new StringBuilder();
            cmdSB.append('O');
            cmdSB.append(ChappieCarApplication.DEVICE_IMEI); // 产品IMIE
            cmdSB.append(CommonUtil.getUnixTimeStamp()); // 时间戳
            cmdSB.append(SerialFunUtil.Byte2Hex(bs[8])); // 添加指令值
            cmdSB.append(SerialFunUtil.Byte2Hex(bs[10])); // 数据长度

            //   tempSB.append("%" + SerialFunUtil.Byte2Hex(bs[8]));//添加指令值
            //   tempSB.append("%" + SerialFunUtil.Byte2Hex(bs[10]));// 数据长度
            for (int kk = 0; kk < bs[10]; kk++) {
                // tempSB.append("%" + SerialFunUtil.Byte2Hex(bs[11 + kk]));//添加命令内容
                cmdSB.append(SerialFunUtil.Byte2Hex(bs[11 + kk])); // 数据长度
            }
            //  cmdSB.append(URLDecoder.decode(tempSB.toString(), "UTF-8"));
            sendBroadCast2LongService(cmdSB.toString());
            Log.e("info", "++回传给服务器的数据是 : " + cmdSB.toString());
            if (ChappieCarApplication.isDebug)
                Log.e("info", "++回传给服务器的数据是 : " + cmdSB.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 发送广播，通知核心服务GPS数据包已经准备好，可以发送至服务器了
     */
    private void sendBroaCast2Server() {
        Intent dataIntent = new Intent();
        dataIntent.setAction(ChappieConstant.UPLOAD_CACHE_DATA_2_SERVER_ACTION);
        dataIntent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(dataIntent);
        if (ChappieCarApplication.isDebug)
            Log.e("info", "发送GPS数据插入成功的广播");
    }

    /**
     * 发送需要缓存到数据库中的广播
     *
     * @param bs
     */
    private void sendCacheData2LongServer(byte[] bs) {
        try {
            LSInstructionBean bean = new LSInstructionBean();
            StringBuilder cmdSB = new StringBuilder();

            //    StringBuilder tempSB = new StringBuilder();

            cmdSB.append('O');
            cmdSB.append(ChappieCarApplication.DEVICE_IMEI); // 产品IMIE
            cmdSB.append(CommonUtil.getTimeStamp()); // 添加时间戳
            if (ChappieCarApplication.isDebug)
                Log.e("info", "第一段：= " + cmdSB.toString());
            cmdSB.append(SerialFunUtil.Byte2Hex(bs[8]));
            cmdSB.append(SerialFunUtil.Byte2Hex(bs[10]));


            //    tempSB.append("%" + SerialFunUtil.Byte2Hex(bs[8]));//添加指令值
            //    tempSB.append("%" + SerialFunUtil.Byte2Hex(bs[10]));// 数据长度
            for (int kk = 0; kk < bs[10]; kk++) {
                //  tempSB.append("%" + SerialFunUtil.Byte2Hex(bs[11 + kk]));//添加命令内容
                cmdSB.append(SerialFunUtil.Byte2Hex(bs[11 + kk]));  //添加命令内容
            }
            //  cmdSB.append(URLDecoder.decode(tempSB.toString(), "UTF-8"));
            bean.setTimeStamp(CommonUtil.getTimeStamp());
            bean.setInstruction(cmdSB.toString());
            long l = lsDao.inserData(bean);
            if (l > 0) {  // 存取一条记录，缓存记录数加一
                if (ChappieCarApplication.isDebug)
                    Log.e("info", "OBD需要缓存的数据指令插入数据库成功");
            }
            sendBroaCast2Server();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 串口发送返回给服务器的数据
     *
     * @param bs
     */
    private void sendOBDData2Service(byte[] bs) {
        if (ChappieCarApplication.isDebug)
            Log.e("info", "ChappieCarApplication.CMD_FLAGE & 0X0002 = " + (ChappieCarApplication.CMD_FLAGE & 0X0002));
        if (ChappieCarApplication.isDebug)
            Log.e("info", "bs[8] = " + SerialFunUtil.Byte2Hex(bs[8]));
        String DEVICE_IMEI = ChappieCarApplication.DEVICE_IMEI;
        if ((ChappieCarApplication.CMD_FLAGE & 0X0001) == 0x0001 && bs[8] == (byte) 0x80) {  // 查询产品ID
            if (ChappieCarApplication.isDebug)
                Log.e("info", "查询产品ID");
            try {
                StringBuilder cmdSB = new StringBuilder();
                cmdSB.append('O');
                cmdSB.append(DEVICE_IMEI);
                cmdSB.append('\n');
                sendBroadCast2LongService(cmdSB.toString());
                ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE & 0xFFFE;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if ((ChappieCarApplication.CMD_FLAGE & 0X0002) == 0x0002 && bs[8] == (byte) 0x81) {  // 查询软件版本号
            if (ChappieCarApplication.isDebug)
                Log.e("info", "查询软件版本号");
            ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE & 0xFFFD;
            sendCMD2LongService(bs);
        }
        if ((ChappieCarApplication.CMD_FLAGE & 0X0004) == 0x0004 && bs[8] == (byte) 0x82) {  // 请求模块重启
            sendCMD2LongService(bs);
            ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE & 0xFFFB;
        }
        if ((ChappieCarApplication.CMD_FLAGE & 0X0008) == 0x0008 && bs[8] == (byte) 0x83) {  // 模块休眠唤醒指令
            sendCMD2LongService(bs);
            ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE & 0xFFF7;
        }
        if ((ChappieCarApplication.CMD_FLAGE & 0X0010) == 0x0010 && bs[8] == (byte) 0x85) {  // 模块上传通用数据流
            if (ChappieCarApplication.isDebug)
                Log.e("info", "模块上传通用数据流");
            sendCMD2LongService(bs);
            ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE & 0xFFEF;
        }
        if ((ChappieCarApplication.CMD_FLAGE & 0X0020) == 0x0020 && bs[8] == (byte) 0x86) {  // 设置是否打开关闭通用数据流
            sendCMD2LongService(bs);
            ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE & 0xFFDF;
        }
        if ((ChappieCarApplication.CMD_FLAGE & 0X0040) == 0x0040 && bs[8] == (byte) 0x87) {  // 请求上传厂家自定义OBD数据流
            sendCMD2LongService(bs);
            ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE & 0xFFBF;
        }
        if ((ChappieCarApplication.CMD_FLAGE & 0X0080) == 0x0080 && bs[8] == (byte) 0x88) {  // 设置OBD数据流时间间隔
            sendCMD2LongService(bs);
            ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE & 0xFF7F;
        }
        if ((ChappieCarApplication.CMD_FLAGE & 0x0100) == 0x0100 && bs[8] == (byte) 0x89) {  // 查询上传用户PID数据
            sendCMD2LongService(bs);
            ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE & 0xFEFF;
        }
        if ((ChappieCarApplication.CMD_FLAGE & 0X0200) == 0X0200 && bs[8] == (byte) 0x8a) {  // 车辆体检
            sendCMD2LongService(bs);
            ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE & 0xFDFF;
        }
        if ((ChappieCarApplication.CMD_FLAGE & 0X0400) == 0x0400 && bs[8] == (byte) 0x8C) {  // 查询VIN码
            sendCMD2LongService(bs);
            ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE & 0xFBFF;
        }
        if ((ChappieCarApplication.CMD_FLAGE & 0x0800) == 0x0800 && bs[8] == (byte) 0x8D) {  // 查询发动机故障码
            sendCMD2LongService(bs);
            ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE & 0xF7FF;
        }
        if ((ChappieCarApplication.CMD_FLAGE & 0x1000) == 0x1000 && bs[8] == (byte) 0x8E) {  // 清除发动机故障码
            sendCMD2LongService(bs);
            ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE & 0xEFFF;
        }
        if ((ChappieCarApplication.CMD_FLAGE & 0x2000) == 0x2000 && bs[8] == (byte) 0x8F) {  // 设置查询里程
            sendCMD2LongService(bs);
            ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE & 0xDFFF;
        }
        if ((ChappieCarApplication.CMD_FLAGE & 0X4000) == 0x4000 && bs[8] == (byte) 0x90) {  // 请求上传模块累计上传数据
            sendCMD2LongService(bs);
            ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE & 0xBFFF;
        }
        if ((ChappieCarApplication.CMD_FLAGE & 0x8000) == 0x8000 && bs[8] == (byte) 0x92) {  // 设置查询报警参数
            sendCMD2LongService(bs);
            ChappieCarApplication.CMD_FLAGE = ChappieCarApplication.CMD_FLAGE & 0x7FFF;
        }
        if (bs[8] == (byte) 0x84 || bs[8] == (byte) 0x8B || bs[8] == (byte) 0x91) {  // 点火熄火指令 // 上传行程报告 // 报警信息上传
            sendCacheData2LongServer(bs);
        }
    }

}


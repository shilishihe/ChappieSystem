package com.scblhkj.carluncher.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * 保护主后台服务
 *
 * @author he
 */
public class ProtectCoreService extends Service {

    private static final String TAG = "ProtectCoreService";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Intent service = new Intent(this, CCLuncherCoreService.class);
        // 启动服务
        this.startService(service);
        Log.i(TAG, "保护服务启动服务！!");
        super.onDestroy();
    }
}

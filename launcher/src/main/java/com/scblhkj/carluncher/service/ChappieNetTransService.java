package com.scblhkj.carluncher.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.scblhkj.carluncher.thread.WifiAPSendFileDataThread;
import com.scblhkj.carluncher.utils.ToastUtil;

import java.io.DataOutputStream;

/**远程控制拍照和摄像
 * Created by blh on 2016-08-29.
 */
public class ChappieNetTransService extends Service {
    private BroadcastReceiver broadcastReceiver;
    private DataOutputStream dos = null;


    @Override
    public void onCreate() {
        registerBroadcastReceiver();
        initBroadcast();
       // ToastUtil.showToast("已连接服务器");
        super.onCreate();

    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }
    @Override
    public void onDestroy() {
        unRegisterBroadcastReceiver();
        super.onDestroy();
    }

    private void initBroadcast(){
       broadcastReceiver=new BroadcastReceiver() {
           @Override
           public void onReceive(Context context, Intent intent) {
              String action=intent.getAction();
               Log.e("info","网络状态下，收到的Action》》"+action);
               switch (action){
                   case "com.scblhkj.chappie.hddvr.SEND_CAR_CMD_NET_PIC":
                       String filePath = intent.getStringExtra("com.scblhkj.chappie.hddvr.SEND_CAR_CMD_NET_PIC_PATH");
                       Log.e("info", "接收到了手动录制完成后发送过来的图片路径" );
                       //new WifiAPSendFileDataThread(dos, filePath, "CP").start();// 消息发送方启动线程发送消息
                       break;
                   case "com.scblhkj.chappie.FINISH_NET_VIDEO_FILE":
                       String videoFilePath = intent.getStringExtra("com.scblhkj.chappie.FINISH_VIDEO_FILE_VALUE");   // 获取到了手动录制视频的文件路径
                       Log.e("info", "接收到了手动录制完成后发送过来的视频路径" + videoFilePath);
                     //  new WifiAPSendFileDataThread(dos, videoFilePath, "CV").start();
                       break;
               }
           }
       };
   }
    private void unRegisterBroadcastReceiver(){
        if(broadcastReceiver!=null){
            unregisterReceiver(broadcastReceiver);
        }
    }
    private void registerBroadcastReceiver(){
        IntentFilter filter=new IntentFilter();
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        filter.addAction("com.scblhkj.chappie.hddvr.SEND_CAR_CMD_NET_PIC");
        filter.addAction("com.scblhkj.chappie.FINISH_NET_VIDEO_FILE");  // 手动视频录制完成之后的广播,携带的广播数据是录制完成视频的路径
        registerReceiver(broadcastReceiver, filter);
        Log.e("info", "注册了远程拍照和摄像的广播接受者");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }
}

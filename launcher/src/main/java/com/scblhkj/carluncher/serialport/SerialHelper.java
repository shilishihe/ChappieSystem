package com.scblhkj.carluncher.serialport;

import android.util.Log;

import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.utils.SerialFunUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;

import android_serialport_api.SerialPort;


public abstract class SerialHelper {
    private SerialPort mSerialPort;
    private OutputStream mOutputStream;
    private InputStream mInputStream;
    private ReadThread mReadThread;
    private SendThread mSendThread;
    private String sPort = "/dev/ttyMT2";
    private String nPort="ATRON";
    private int iBaudRate = ChappieConstant.SERIAL_BAUDRATE;
    private int newBaunRate=ChappieConstant.NEW_SERIAL_BAUDRATE;
    private boolean _isOpen = false;
    private byte[] _bLoopData = new byte[]{0x30};
    private int iDelay = 500;

    // ----------------------------------------------------
    public SerialHelper(String sPort, int iBaudRate) {
        this.sPort = sPort;
        this.iBaudRate = iBaudRate;
    }

    public SerialHelper() {
        this("/dev/ttyMT2", ChappieConstant.SERIAL_BAUDRATE);
    }

    public SerialHelper(String sPort) {
        this(sPort, ChappieConstant.SERIAL_BAUDRATE);
    }

    public SerialHelper(String sPort, String sBaudRate) {
        this(sPort, Integer.parseInt(sBaudRate));
    }

    // ----------------------------------------------------
    public void open() throws SecurityException, IOException, InvalidParameterException {
        mSerialPort = new SerialPort(new File(sPort), iBaudRate, 0);
        mOutputStream = mSerialPort.getOutputStream();
        mInputStream = mSerialPort.getInputStream();
        mReadThread = new ReadThread();
        mReadThread.start();
        mSendThread = new SendThread();
        mSendThread.setSuspendFlag();
        mSendThread.start();
        _isOpen = true;
    }
    public void openNewObd() throws SecurityException, IOException, InvalidParameterException {
        mSerialPort = new SerialPort(new File(nPort),newBaunRate , 0);
        mOutputStream = mSerialPort.getOutputStream();
        mInputStream = mSerialPort.getInputStream();
        mReadThread = new ReadThread();
        mReadThread.start();
        mSendThread = new SendThread();
        mSendThread.setSuspendFlag();
        mSendThread.start();
        _isOpen = true;
    }
    // ----------------------------------------------------
    public void close() {
        if (mReadThread != null)
            mReadThread.interrupt();
        if (mSerialPort != null) {
            mSerialPort.close();
            mSerialPort = null;
        }
        _isOpen = false;
    }

    // ----------------------------------------------------
    public void send(byte[] bOutArray) {
        try {
            mOutputStream.write(bOutArray);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // ----------------------------------------------------
    public void sendHex(String sHex) {
        byte[] bOutArray = SerialFunUtil.HexToByteArr(sHex);
        send(bOutArray);
    }

    // ----------------------------------------------------
    public void sendTxt(String sTxt) {
        byte[] bOutArray = sTxt.getBytes();
        send(bOutArray);
    }

    private static final String TAG = "SerialHelper";

    // ----------------------------------------------------
    private class ReadThread extends Thread {
        @Override
        public void run() {
            super.run();
             Log.e("info", "串口ReadThread正在接收数据");
            while (!isInterrupted()) {
                try {
                    if (mInputStream == null) {
                        Log.e("info", "mInputStream为NULL");
                        return;
                    } else {
                    }
                    byte[] buffer = new byte[511];
                    int size = mInputStream.read(buffer);
                    if (size > 0) {
                      Log.e("info", "串口广播的最原始的数据为：" + SerialFunUtil.ByteArrToHexForLog(buffer));
                        ComBean comRecData = new ComBean();
                        comRecData.setbRec(buffer);
                        onDataReceived(comRecData);
                    }
                    try {
                        Thread.sleep(50);// 延时50ms
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } catch (Throwable e) {
                    Log.e("info", "执行读取串口的线程挂掉了");
                    e.printStackTrace();
                    restartSerialHelper();
                    return;
                }
            }
        }
    }

    // ----------------------------------------------------
    private class SendThread extends Thread {
        public boolean suspendFlag = true;// 控制线程的执行

        @Override
        public void run() {
            super.run();
            while (!isInterrupted()) {
                synchronized (this) {
                    while (suspendFlag) {
                        try {
                            wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                send(getbLoopData());
                try {
                    Thread.sleep(iDelay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        // 线程暂停
        public void setSuspendFlag() {
            this.suspendFlag = true;
        }

        // 唤醒线程
        public synchronized void setResume() {
            this.suspendFlag = false;
            notify();
        }
    }

    // ----------------------------------------------------
    public int getBaudRate() {
        return iBaudRate;
    }

    public boolean setBaudRate(int iBaud) {
        if (_isOpen) {
            return false;
        } else {
            iBaudRate = iBaud;
            return true;
        }
    }

    public boolean setBaudRate(String sBaud) {
        int iBaud = Integer.parseInt(sBaud);
        return setBaudRate(iBaud);
    }

    // ----------------------------------------------------
    public String getPort() {
        return sPort;
    }

    public boolean setPort(String sPort) {
        if (_isOpen) {
            return false;
        } else {
            this.sPort = sPort;
            return true;
        }
    }

    // ----------------------------------------------------
    public boolean isOpen() {
        return _isOpen;
    }

    // ----------------------------------------------------
    public byte[] getbLoopData() {
        return _bLoopData;
    }

    // ----------------------------------------------------
    public void setbLoopData(byte[] bLoopData) {
        this._bLoopData = bLoopData;
    }

    // ----------------------------------------------------
    public void setTxtLoopData(String sTxt) {
        this._bLoopData = sTxt.getBytes();
    }

    // ----------------------------------------------------
    public void setHexLoopData(String sHex) {
        this._bLoopData = SerialFunUtil.HexToByteArr(sHex);
    }

    // ----------------------------------------------------
    public int getiDelay() {
        return iDelay;
    }

    // ----------------------------------------------------
    public void setiDelay(int iDelay) {
        this.iDelay = iDelay;
    }

    // ----------------------------------------------------
    public void startSend() {
        if (mSendThread != null) {
            mSendThread.setResume();
        }
    }

    // ----------------------------------------------------
    public void stopSend() {
        if (mSendThread != null) {
            mSendThread.setSuspendFlag();
        }
    }

    // ----------------------------------------------------
    protected abstract void onDataReceived(ComBean ComRecData);

    //
    protected abstract void restartSerialHelper();
}
package com.scblhkj.carluncher.serialport;

import java.io.Serializable;
import java.text.SimpleDateFormat;

/**
 * @author benjaminwan 串口数据
 */
public class ComBean implements Serializable {

    private static final long serialVersionUID = -7060210544600464481L;

    private byte[] bRec;
    private String sRecTime;
    private String sComPort;
    private byte[][] bytess;

    public byte[][] getBytess() {
        return bytess;
    }

    public void setBytess(byte[][] bytess) {
        this.bytess = bytess;
    }

    public byte[] getbRec() {
        return bRec;
    }

    public void setbRec(byte[] bRec) {
        this.bRec = bRec;
    }

    public String getsComPort() {
        return sComPort;
    }

    public void setsComPort(String sComPort) {
        this.sComPort = sComPort;
    }

    public String getsRecTime() {
        return sRecTime;
    }

    public void setsRecTime(String sRecTime) {
        this.sRecTime = sRecTime;
    }

    public ComBean(String sPort, byte[] buffer, int size) {

        sComPort = sPort;
        bRec = new byte[size];
        for (int i = 0; i < size; i++) {
            bRec[i] = buffer[i];
        }
        SimpleDateFormat sDateFormat = new SimpleDateFormat("hh:mm:ss");
        sRecTime = sDateFormat.format(new java.util.Date());

    }

    public ComBean() {
    }

}
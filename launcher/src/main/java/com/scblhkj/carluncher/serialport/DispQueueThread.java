package com.scblhkj.carluncher.serialport;

import com.scblhkj.carluncher.utils.SerialFunUtil;
import com.scblhkj.carluncher.utils.ToastUtil;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Leo on 2015/11/4.
 */
public class DispQueueThread extends Thread {

    private Queue<ComBean> queueList = new LinkedList<ComBean>();

    @Override
    public void run() {
        super.run();
        while (!isInterrupted()) {
            final ComBean comData;
            while ((comData = queueList.poll()) != null) {
                // 发送广播出去
                ToastUtil.showToast(SerialFunUtil.ByteArrToHexForLog(comData.getbRec()));


                try {
                    Thread.sleep(100);//显示性能高的话，可以把此数值调小。
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public synchronized void AddQueue(ComBean ComData) {
        queueList.add(ComData);
    }


}

package com.scblhkj.carluncher.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.scblhkj.carluncher.service.CCLuncherCoreService;

/**
 * 启动核心服务的广播接收者
 */
public class StartCoreServiceReceiver extends BroadcastReceiver {

    private static String TAG = "StartCoreServiceReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            // 监听开机启动
            Intent service = new Intent(context, CCLuncherCoreService.class);
            context.startService(service);
            Log.i(TAG, "开机监听启动服务！!");
        } else if (intent.getAction().equals("com.chappie.relodservice")) {
            // 服务被销毁后再次启动服务
            Intent service = new Intent(context, CCLuncherCoreService.class);
            context.startService(service);
            Log.i(TAG, "服务销毁后重新启动服务！!");
        } else if (intent.getAction().equals("com.chappie.restarservice")) {
            // 服务被销毁后再次启动服务
            Intent service = new Intent(context, CCLuncherCoreService.class);
            context.startService(service);
            Log.i(TAG, "stopService重新启动服务");
        }

    }

}

package com.scblhkj.carluncher.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.scblhkj.carluncher.service.CCLuncherCoreService;

/**
 * 重新启动核心服务的广播接收者
 */
public class RestartCoreServiceReceiver extends BroadcastReceiver {

    private static final String TAG = "RestartCoreService";

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, CCLuncherCoreService.class);
        context.startService(service);
        Log.i(TAG, "核心服务重新启动");
    }

}

package com.scblhkj.carluncher.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.scblhkj.carluncher.service.DamenService;

public class BootStartReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			//开机自动启动这个service
			context.startService(new Intent(context, DamenService.class));
			//context.s
			Log.e("info", "BootStartReceiver启动，启动了DamenService");
			//context.startService(new Intent(context,DamenService.class));
		} 
	}

}

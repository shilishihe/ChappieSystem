package com.scblhkj.carluncher.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.scblhkj.carluncher.R;

/**
 * Created by blh on 2016-06-17.
 */
public class SettingAdapter extends BaseAdapter {
    private Context mContext;
    public String[] img_text={"wifi","网络","蓝牙","ADAS","GPS","震动巡检"};
    public int[] imgs={R.drawable.ic_settingwifion,R.drawable.ic_settingneton,R.drawable.ic_settingbluetoothon,
            R.drawable.ic_settingadason, R.drawable.ic_settinggpson,R.drawable.ic_settingqueakon
    };
    private int clickTemp=-1;
    public void setSeclection(int position) {
        clickTemp = position;
    }
    public SettingAdapter(Context mContext) {
        super();
        this.mContext = mContext;
    }
    @Override
    public int getCount() {
        return img_text.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView= LayoutInflater.from(mContext).inflate(R.layout.settingfragmentgriditem,parent,false);

        }
        TextView tv = BaseViewHolder.get(convertView, R.id.tv_item);
        ImageView iv = BaseViewHolder.get(convertView, R.id.iv_item);
        iv.setBackgroundResource(imgs[position]);

        tv.setText(img_text[position]);
        return convertView;
    }
}

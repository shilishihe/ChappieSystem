package com.scblhkj.carluncher.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.common.TimeLineItemType;
import com.scblhkj.carluncher.domain.OBDFaultBean;
import com.scblhkj.carluncher.holder.TimeLineViewHolder;

import java.util.List;

/**
 * Created by Leo on 2015/12/17.
 * 车辆状态显示车辆故障的RecyleView数据适配器
 */
public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineViewHolder> {

    private List<OBDFaultBean> beans;
    private static final String TAG = "TimeLineAdapter";
    public TimeLineAdapter(List<OBDFaultBean> beans) {
        this.beans = beans;
    }

    @Override
    public int getItemViewType(int position) {
        final int size = beans.size() - 1;
        if (0 == size) {
            return TimeLineItemType.ATOM;
        } else if (0 == position) {
            return TimeLineItemType.START;
        } else if (size == position) {
            return TimeLineItemType.END;
        } else {
            return TimeLineItemType.NORMAL;
        }
    }

    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = View.inflate(parent.getContext(), R.layout.item_obd_time_line, null);
        return new TimeLineViewHolder(v, viewType);
    }

    @Override
    public int getItemCount() {
        return beans.size();
    }

    @Override
    public void onBindViewHolder(TimeLineViewHolder holder, int position) {
        holder.setData(beans.get(position));
    }

}

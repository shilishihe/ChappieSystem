package com.scblhkj.carluncher.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.manager.image.ImageLoaderFactory;
import com.scblhkj.carluncher.manager.image.ImageLoaderWrapper;

import java.util.List;

/**
 * Created by Leo on 2016/5/6.
 * 消息中心数据主界面ListView数据适配器类
 */
public class MessageCenterAdapter extends BaseAdapter {

    private Context context;
    private List<MessageCenterBean> list;
    private ImageLoaderWrapper imageLoaderWrapper;

    public MessageCenterAdapter(Context context, List<MessageCenterBean> list) {
        this.context = context;
        this.list = list;
        imageLoaderWrapper = ImageLoaderFactory.getImageLoaderWrapper();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.message_center_main_list_item, null);
            viewHolder.userHead = (ImageView) convertView.findViewById(R.id.userHead);
            viewHolder.userName = (TextView) convertView.findViewById(R.id.userName);
            viewHolder.lastMessage = (TextView) convertView.findViewById(R.id.lastMessage);
            viewHolder.lastTime = (TextView) convertView.findViewById(R.id.lastTime);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        imageLoaderWrapper.loadCircleImage(context, list.get(position).getHeadUrl(), viewHolder.userHead);
        viewHolder.userName.setText(list.get(position).getUserName());
        viewHolder.lastMessage.setText(list.get(position).getLastMessage());
        viewHolder.lastTime.setText(list.get(position).getLastMessagTime());
        return convertView;
    }

    public class ViewHolder {
        public ImageView userHead;
        public TextView userName;
        public TextView lastTime;
        public TextView lastMessage;
    }
}

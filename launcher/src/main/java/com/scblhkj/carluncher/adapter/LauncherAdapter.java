package com.scblhkj.carluncher.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.scblhkj.carluncher.R;

/**
 * Created by blh on 2016-06-17.
 */
public class LauncherAdapter extends BaseAdapter {
    private Context mContext;
    public String[] img_text={"记录仪","OBD","胎压","导航","相册","音乐","蓝牙电话","FM发射","常用设置","二维码","语音通话","系统设置"};
    public int[] imgs={R.mipmap.ic_recor1,R.mipmap.ic_obdi,R.mipmap.ic_tire1,
            R.mipmap.ic_gaodedaohang, R.mipmap.ic_docun,R.mipmap.ic_musici,
            R.mipmap.ic_bttel1,R.mipmap.ic_fm1,R.mipmap.ic_settin,R.drawable.ic_imeiqcode,R.drawable.ic_voice ,R.drawable.ic_systemsetting
    };
    public LauncherAdapter(Context mContext) {
        super();
        this.mContext = mContext;
    }
    @Override
    public int getCount() {
        return img_text.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView= LayoutInflater.from(mContext).inflate(R.layout.servicefragmentgriditem,parent,false);
        }
        TextView tv = BaseViewHolder.get(convertView, R.id.tv_item);
        ImageView iv = BaseViewHolder.get(convertView, R.id.iv_item);
        iv.setBackgroundResource(imgs[position]);
        tv.setText(img_text[position]);
        return convertView;
    }
}

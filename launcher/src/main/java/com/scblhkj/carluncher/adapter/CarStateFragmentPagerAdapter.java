package com.scblhkj.carluncher.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by Leo on 2016/1/7.
 * 车辆状态Fragment适配器
 */
public class CarStateFragmentPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> list;

    public CarStateFragmentPagerAdapter(FragmentManager fm, List<Fragment> list) {
        super(fm);
        this.list = list;
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }
}

package com.scblhkj.carluncher.adapter;

/**
 * Created by Leo on 2016/5/6.
 * 消息中心数据实体类
 */
public class MessageCenterBean {

    private String headUrl;  // 用户头像图片地址
    private String userName; // 用户名称
    private String lastMessagTime;  // 用户最后一条消息发送的时间
    private String lastMessage;  // 用户最后一条消息

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl;
    }

    public String getLastMessagTime() {
        return lastMessagTime;
    }

    public void setLastMessagTime(String lastMessagTime) {
        this.lastMessagTime = lastMessagTime;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}

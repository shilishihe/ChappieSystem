package com.scblhkj.carluncher.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by Leo on 2015/11/9.
 * ViewPager数据适配器类
 */
public class ChappieFragmentPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> list;

    public ChappieFragmentPagerAdapter(FragmentManager fm, List<Fragment> list) {
        super(fm);
        this.list = list;

    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }
}

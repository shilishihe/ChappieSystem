package com.scblhkj.carluncher.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.domain.MyCarInfoBean;
import com.scblhkj.carluncher.utils.ToastUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Leo on 2016/1/16.
 * 我的汽车ListView数据适配器类
 */
public class MyCarInfoAdapter extends BaseAdapter {

    private List<MyCarInfoBean> list;
    private Context context;

    public MyCarInfoAdapter(Context context) {
        list = new ArrayList<MyCarInfoBean>();
        this.context = context;
    }

    public void setList(List<MyCarInfoBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = View.inflate(context, R.layout.adapter_car_info_item, null);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            holder.tvDes = (TextView) convertView.findViewById(R.id.tv_des);
            holder.tvModify = (TextView) convertView.findViewById(R.id.tv_modify);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvTitle.setText(list.get(position).getTitle());
        holder.tvDes.setText(list.get(position).getDes());
        holder.tvModify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtil.showToast("修改按钮被点击");
            }
        });
        return convertView;
    }

    public class ViewHolder {
        public TextView tvTitle;
        public TextView tvDes;
        public TextView tvModify;
    }

    public void addList(List<MyCarInfoBean> list) {
        list.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(MyCarInfoBean bean) {
        list.add(bean);
        notifyDataSetChanged();
    }

    public List<MyCarInfoBean> getList() {
        return list;
    }
}

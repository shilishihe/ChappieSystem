package com.scblhkj.carluncher.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.rey.material.widget.Button;
import com.rey.material.widget.Switch;
import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.utils.SpUtil;
import com.scblhkj.carluncher.utils.TitleBuilder;
import com.scblhkj.carluncher.widget.Slider;

/**
 * 胎压设置Fragment
 */
public class TPMSSettingFragment extends BaseFragment implements View.OnClickListener
        , RadioGroup.OnCheckedChangeListener
        , Slider.OnPositionChangeListener
        , View.OnTouchListener
        , Switch.OnCheckedChangeListener {

    private TextView tv_emitter_matching;
    private TextView tv_tyre_trans;
    private RadioGroup rg_pres_unit;
    private RadioGroup rg_temp_unit;
    private TextView tv_pre_top_value;
    private TextView tv_temp_top_value;
    private TextView tv_pre_low_value;
    private Slider sl_alert_top_pre;
    private Slider sl_alert_low_pre;
    private Slider sl_aler_temp;
    private Button bt_reset_params;
    private Switch sw_pre;
    private Switch sw_alert;
    private RadioButton rb_bar;
    private RadioButton rb_kap;
    private RadioButton rb_psi;
    private RadioButton rb_dc;
    private RadioButton rb_f;
    private LinearLayout ll_content;


    public TPMSSettingFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tpmssetting, container, false);
        new TitleBuilder(view)
                .setTitleText("设置")
                .setLeftImage(R.mipmap.ic_tpms_back)
                .setLeftOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // 返回胎压监测主界面
                        try {
                            onTPMSSettingFragmentInterface = (OnTPMSSettingFragmentInterface) activity;
                            onTPMSSettingFragmentInterface.onTPMSSettingFragmentBackClick();
                        } catch (ClassCastException e) {
                            throw new ClassCastException(activity.toString() + "must implement OnTPMSSettingFragmentInterface");
                        }
                    }
                }).build();
        initView();
        initData();
        return view;
    }

    @Override
    public void initView() {

        tv_emitter_matching = (TextView) view.findViewById(R.id.tv_emitter_matching);
        tv_tyre_trans = (TextView) view.findViewById(R.id.tv_tyre_trans);
        rg_pres_unit = (RadioGroup) view.findViewById(R.id.rg_pres_unit);
        rg_temp_unit = (RadioGroup) view.findViewById(R.id.rg_temp_unit);
        tv_pre_top_value = (TextView) view.findViewById(R.id.tv_pre_top_value);
        tv_temp_top_value = (TextView) view.findViewById(R.id.tv_temp_top_value);
        tv_pre_low_value = (TextView) view.findViewById(R.id.tv_pre_low_value);
        sl_alert_top_pre = (Slider) view.findViewById(R.id.sl_alert_top_pre);
        sl_alert_low_pre = (Slider) view.findViewById(R.id.sl_alert_low_pre);
        sl_aler_temp = (Slider) view.findViewById(R.id.sl_aler_temp);
        bt_reset_params = (Button) view.findViewById(R.id.bt_reset_params);
        sw_pre = (Switch) view.findViewById(R.id.sw_pre);
        sw_alert = (Switch) view.findViewById(R.id.sw_alert);

        rb_bar = (RadioButton) view.findViewById(R.id.rb_bar);
        rb_kap = (RadioButton) view.findViewById(R.id.rb_kap);
        rb_psi = (RadioButton) view.findViewById(R.id.rb_psi);
        rb_dc = (RadioButton) view.findViewById(R.id.rb_dc);
        rb_f = (RadioButton) view.findViewById(R.id.rb_f);

        ll_content = (LinearLayout) view.findViewById(R.id.ll_content);
    }

    @Override
    public void initData() {
        tv_emitter_matching.setOnClickListener(this);
        tv_tyre_trans.setOnClickListener(this);
        rg_pres_unit.setOnCheckedChangeListener(this);
        rg_temp_unit.setOnCheckedChangeListener(this);
        sl_alert_top_pre.setOnPositionChangeListener(this);
        sl_alert_low_pre.setOnPositionChangeListener(this);
        sl_aler_temp.setOnPositionChangeListener(this);
        bt_reset_params.setOnClickListener(this);
        sw_pre.setOnCheckedChangeListener(this);
        sw_alert.setOnCheckedChangeListener(this);

        sl_alert_top_pre.setOnTouchListener(this);
        sl_alert_low_pre.setOnTouchListener(this);
        sl_aler_temp.setOnTouchListener(this);
        sw_pre.setOnTouchListener(this);
        sw_alert.setOnTouchListener(this);

        ll_content.setOnClickListener(this);
        setSpData();
    }

    private OnTPMSSettingFragmentInterface onTPMSSettingFragmentInterface;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_emitter_matching: {
                // 进入胎压配对界面
                try {
                    onTPMSSettingFragmentInterface = (OnTPMSSettingFragmentInterface) activity;
                    onTPMSSettingFragmentInterface.onTPMSSettingFragment2Matching();
                } catch (ClassCastException e) {
                    throw new ClassCastException(activity.toString() + "must implement OnTPMSSettingFragmentInterface");
                }
                break;
            }
            case R.id.tv_tyre_trans: {
                try {
                    onTPMSSettingFragmentInterface = (OnTPMSSettingFragmentInterface) activity;
                    onTPMSSettingFragmentInterface.onTPMSSettingFragment2StyreChaging();
                } catch (ClassCastException e) {
                    throw new ClassCastException(activity.toString() + "must implement OnTPMSSettingFragmentInterface");
                }
                break;
            }
            case R.id.bt_reset_params: {
                resetDefaultParams();
                break;
            }
            case R.id.ll_content: {
                // 点击内容区域的空白区域执行放大操作
                try {
                    onTPMSSettingFragmentInterface = (OnTPMSSettingFragmentInterface) activity;
                    onTPMSSettingFragmentInterface.onTPMSMaxView();
                } catch (ClassCastException e) {
                    throw new ClassCastException(activity.toString() + "must implement OnTPMSSettingFragmentInterface");
                }
                break;
            }
            default: {
                throw new RuntimeException("no such id method should invoke");
            }
        }
    }

    @Override
    public void onPositionChanged(Slider view, boolean fromUser, float oldPos, float newPos, float oldValue, float newValue) {
        switch (view.getId()) {
            case R.id.sl_alert_top_pre: {
                int presUnit = SpUtil.getInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES);
                if (1 == presUnit) {
                    tv_pre_top_value.setText(String.valueOf(newValue + "Bar"));
                } else if (2 == presUnit) {
                    tv_pre_top_value.setText(String.valueOf(newValue + "Kpa"));
                } else if (3 == presUnit) {
                    tv_pre_top_value.setText(String.valueOf(newValue + "Psi"));
                }
                SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX, newValue);
                break;
            }
            case R.id.sl_alert_low_pre: {
                int presUnit = SpUtil.getInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES);
                if (1 == presUnit) {
                    tv_pre_low_value.setText(String.valueOf(newValue) + "Bar");
                } else if (2 == presUnit) {
                    tv_pre_low_value.setText(String.valueOf(newValue) + "Kpa");
                } else if (3 == presUnit) {
                    tv_pre_low_value.setText(String.valueOf(newValue) + "Psi");
                }
                SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN, newValue);
                break;
            }
            case R.id.sl_aler_temp: {
                int tempUnit = SpUtil.getInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_TEMP);
                if (1 == tempUnit) {
                    tv_temp_top_value.setText(String.valueOf(newValue + "℃"));
                } else if (2 == tempUnit) {
                    tv_temp_top_value.setText(String.valueOf(newValue + "℉"));
                }
                SpUtil.saveInt2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_TEMP_MAX, (int) (newValue));
                break;
            }
            default: {
                throw new RuntimeException("no such id method should invoke");
            }
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (radioGroup.getId()) {
            // 压力单位模块
            case R.id.rg_pres_unit: {
                int unit = SpUtil.getInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES);// 默认是 Kpa
                int max = sl_alert_top_pre.getMaxValue();  // 胎压上限最大值
                int min = sl_alert_top_pre.getMinValue();  // 胎压下限最小值
                int lmax = sl_alert_low_pre.getMaxValue(); // 胎压下限最大值
                int lmin = sl_alert_low_pre.getMinValue(); // 胎压下限最小值

                float tempValue = SpUtil.getFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX);   // 胎压上限当前值
                float tempLValue = SpUtil.getFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN);  // 胎压下限当前值


                switch (i) {   // 1代表Bar 2代表Kpa 3代表Psi
                    case R.id.rb_bar: {
                        if (2 == unit) {   // Kpa转Bar
                            /**
                             * 胎压上限部分
                             */
                            tempValue = tempValue * 0.01f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX, tempValue);
                            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES, 1);
                            sl_alert_top_pre.setValueRange((int) Math.ceil(min * 0.01), (int) Math.ceil(max * 0.01), false);
                            sl_alert_top_pre.setValue(tempValue, true);
                            tv_pre_top_value.setText(String.format("%.1f", (float) tempValue) + "Bar");
                            /**
                             * 胎压下限部分
                             */
                            tempLValue = tempLValue * 0.01f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN, tempLValue);
                            sl_alert_low_pre.setValueRange((int) Math.ceil(lmin * 0.01), (int) Math.ceil(lmax * 0.01), false);
                            sl_alert_low_pre.setValue(tempLValue, true);
                            tv_pre_low_value.setText(String.format("%.1f", (float) tempLValue) + "Bar");
                        } else if (3 == unit) {  // Psi转Bar
                            /**
                             * 胎压上限部分
                             */
                            tempValue = tempValue * 0.0689f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX, tempValue);
                            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES, 1);
                            sl_alert_top_pre.setValueRange((int) Math.ceil(min * 0.0689f), (int) Math.ceil(max * 0.0689f), false);
                            sl_alert_top_pre.setValue(tempValue, true);
                            tv_pre_top_value.setText(String.format("%.1f", (float) tempValue) + "Bar");
                            /**
                             * 胎压下限部分
                             */
                            tempLValue = tempLValue * 0.0689f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN, tempLValue);
                            sl_alert_low_pre.setValueRange((int) Math.ceil(lmin * 0.0689f), (int) Math.ceil(lmax * 0.0689f), false);
                            sl_alert_low_pre.setValue(tempLValue, true);
                            tv_pre_low_value.setText(String.format("%.1f", (float) tempLValue) + "Bar");
                        }
                        break;
                    }
                    case R.id.rb_kap: {
                        if (1 == unit) {   // Bar转Kpa
                            /**
                             * 胎压上限部分
                             */
                            tempValue = tempValue * 100f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX, tempValue);
                            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES, 2);
                            sl_alert_top_pre.setValueRange((int) Math.ceil(min * 100f), (int) Math.ceil(max * 100f), false);
                            sl_alert_top_pre.setValue(tempValue, true);
                            tv_pre_top_value.setText(String.format("%.1f", (float) tempValue) + "Kpa");
                            /**
                             * 胎压下限部分
                             */
                            tempLValue = tempLValue * 100f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN, tempLValue);
                            sl_alert_low_pre.setValueRange((int) Math.ceil(lmin * 100f), (int) Math.ceil(lmax * 100f), false);
                            sl_alert_low_pre.setValue(tempLValue, true);
                            tv_pre_low_value.setText(String.format("%.1f", (float) tempLValue) + "Kpa");
                        } else if (3 == unit) {  // Psi转Kpa
                            /**
                             * 胎压上限部分
                             */
                            tempValue = tempValue * 6.894f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX, tempValue);
                            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES, 2);
                            sl_alert_top_pre.setValueRange((int) Math.ceil(min * 6.894), (int) Math.ceil(max * 6.894), false);
                            sl_alert_top_pre.setValue(tempValue, true);
                            tv_pre_top_value.setText(String.format("%.1f", (float) tempValue) + "Kpa");
                            /**
                             * 胎压下限部分
                             */
                            tempLValue = tempLValue * 6.894f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN, tempLValue);
                            sl_alert_low_pre.setValueRange((int) Math.ceil(lmin * 6.894), (int) Math.ceil(lmax * 6.894), false);
                            sl_alert_low_pre.setValue(tempLValue, true);
                            tv_pre_low_value.setText(String.format("%.1f", (float) tempLValue) + "Kpa");
                        }
                        break;
                    }
                    case R.id.rb_psi: {
                        if (1 == unit) {   // Bar转PSI
                            /**
                             * 胎压上限部分
                             */
                            tempValue = tempValue * 14.503f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX, tempValue);
                            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES, 3);
                            sl_alert_top_pre.setValueRange((int) Math.ceil(min * 14.503), (int) Math.ceil(max * 14.503), false);
                            sl_alert_top_pre.setValue(tempValue, true);
                            tv_pre_top_value.setText(String.format("%.1f", (float) tempValue) + "Psi");
                            /**
                             * 胎压下限部分
                             */
                            tempLValue = tempLValue * 14.503f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN, tempLValue);
                            sl_alert_low_pre.setValueRange((int) Math.ceil(lmin * 14.503), (int) Math.ceil(lmax * 14.503), false);
                            sl_alert_low_pre.setValue(tempLValue, true);
                            tv_pre_low_value.setText(String.format("%.1f", (float) tempLValue) + "Psi");
                        } else if (2 == unit) {  // Kpa转Psi
                            /**
                             * 胎压上限部分
                             */
                            tempValue = tempValue * 0.145f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX, tempValue);
                            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES, 3);
                            sl_alert_top_pre.setValueRange((int) Math.ceil(min * 0.145), (int) Math.ceil(max * 0.145), false);
                            sl_alert_top_pre.setValue(tempValue, true);
                            tv_pre_top_value.setText(String.format("%.1f", (float) tempValue) + "Psi");
                            /**
                             * 胎压下限部分
                             */
                            tempLValue = tempLValue * 0.145f;
                            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN, tempLValue);
                            sl_alert_low_pre.setValueRange((int) Math.ceil(lmin * 0.145f), (int) Math.ceil(lmax * 0.145f), false);
                            sl_alert_low_pre.setValue(tempLValue, true);
                            tv_pre_low_value.setText(String.format("%.1f", (float) tempLValue) + "Psi");
                        }
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
            }


            case R.id.rg_temp_unit: {
                int unit = SpUtil.getInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_TEMP);
                int max = sl_aler_temp.getMaxValue();
                int min = sl_aler_temp.getMinValue();
                int tempMax = SpUtil.getInt2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_TEMP_MAX);
                switch (i) {
                    case R.id.rb_f: {  // 1代表摄氏度 2代表华氏度  点击了华氏度
                        if (1 == unit) {
                            tempMax = (int) (tempMax * 1.8f + 32);
                            max = (int) (max * 1.8f + 32);
                            min = (int) (min * 1.8f + 32);
                            SpUtil.saveInt2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_TEMP_MAX, tempMax);
                            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_TEMP, 2);
                            sl_aler_temp.setValueRange(min, max, false);
                            sl_aler_temp.setValue(tempMax, true);
                            tv_temp_top_value.setText(String.valueOf(tempMax + "℉"));
                        }
                        break;
                    }

                    case R.id.rb_dc: {  // 点击了摄氏度
                        if (2 == unit) {
                            tempMax = (int) ((tempMax - 32) / 1.8f);
                            max = (int) ((max - 32) / 1.8f);
                            min = (int) ((min - 32) / 1.8f);
                            SpUtil.saveInt2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_TEMP_MAX, tempMax);
                            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_TEMP, 1);
                            sl_aler_temp.setValueRange(min, max, false);
                            sl_aler_temp.setValue(tempMax, true);
                            tv_temp_top_value.setText(String.valueOf(tempMax + "℃"));
                        }

                        break;
                    }

                    default: {
                        break;
                    }
                }
                break;
            }
            default: {
                throw new RuntimeException("no such id method should invoke");
            }
        }

    }


    public interface OnTPMSSettingFragmentInterface {
        void onTPMSSettingFragmentBackClick();

        void onTPMSSettingFragment2Matching();

        void onTPMSSettingFragment2StyreChaging();

        /**
         * 此方法用于主界面胎压Fragment中窗口处于缩小状态，点击小模块时应该执行放大模块操作
         * 但是由于此界面中用到了可滚动布局，导致点击时缩小的模块窗口放大不了
         * 在此使用在该布局的内容布局上加上了一个可回调的点击事件，在MainActivity中用代码手动的执行放大窗口的操作
         * 暂时还未找到该问题的原因
         */
        void onTPMSMaxView();
    }

    // 按下点的x坐标
    public float temp_tx = 0;
    public float diff = 2;
    // 按下点的时间
    public long temp_tt = 0;
    // 过滤的时间间隔
    public long timeout = 50;

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                ChappieCarApplication.ALLOW_SCROLL = 0;
                temp_tt = System.currentTimeMillis();
                temp_tx = event.getX();
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                if (Math.abs(event.getX() - temp_tx) > diff && (System.currentTimeMillis() - temp_tt) <= timeout) {
                    ChappieCarApplication.ALLOW_SCROLL = 1;//认为是滑动
                }
                break;
            }

            case MotionEvent.ACTION_UP: {
                ChappieCarApplication.ALLOW_SCROLL = -1;
                break;
            }

            case MotionEvent.ACTION_CANCEL: {
                if (ChappieCarApplication.ALLOW_SCROLL == 0 || ChappieCarApplication.ALLOW_SCROLL == 1) {
                    ChappieCarApplication.ALLOW_SCROLL = -1;
                }
                break;
            }
            default: {
                break;
            }
        }
        return false;
    }

    private static final String TAG = "TPMSSettingFragment";

    @Override
    public void onCheckedChanged(Switch view, boolean checked) {
        switch (view.getId()) {
            case R.id.sw_pre: {
                SpUtil.saveBoolean2SP(ChappieConstant.TMPS_FRAGMENT_SWITCH_PRES_ALARM, checked);
                if (ChappieCarApplication.isDebug)
                Log.e(TAG, checked + "");
                break;
            }
            case R.id.sw_alert: {
                SpUtil.saveBoolean2SP(ChappieConstant.TMPS_FRAGMENT_SWITCH_ALARM, checked);
                break;
            }
            default: {
                break;
            }
        }
    }

    private void setSpData() {

        boolean checked_pres = SpUtil.getBoolean2SP(ChappieConstant.TMPS_FRAGMENT_SWITCH_PRES_ALARM);  // 压力告警开关
        boolean checked_alert = SpUtil.getBoolean2SP(ChappieConstant.TMPS_FRAGMENT_SWITCH_ALARM);  // 告警开关
        float sl_pres_max = SpUtil.getFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX);  // 压力上限值
        float sl_pres_min = SpUtil.getFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN);  // 压力下限值
        int sl_temp = SpUtil.getInt2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_TEMP_MAX);  // 温度上限值
        int pres_unit = SpUtil.getInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES);  // 压力单位
        int temp_unit = SpUtil.getInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_TEMP);  // 温度单位


        // 滑动开关
        sw_pre.setChecked(checked_pres);
        sw_alert.setChecked(checked_alert);

        // 胎压单位
        if (1 == pres_unit) {
            /**
             * 胎压上限
             */
            rb_bar.setChecked(true);
            sl_alert_top_pre.setValueRange(2, 8, false);
            sl_alert_top_pre.setValue(sl_pres_max, true);
            tv_pre_top_value.setText(String.valueOf(sl_pres_max + "Bar"));
            /**
             * 胎压下限
             */
            sl_alert_low_pre.setValueRange(2, 3, false);
            sl_alert_low_pre.setValue(sl_pres_min, true);
            tv_pre_low_value.setText(String.valueOf(sl_pres_min + "Bar"));
        } else if (2 == pres_unit) {
            rb_kap.setChecked(true);
            /**
             * 胎压上限
             */
            sl_alert_top_pre.setValueRange(200, 800, false);
            sl_alert_top_pre.setValue(sl_pres_max, true);
            tv_pre_top_value.setText(String.valueOf(sl_pres_max + "Kpa"));
            /**
             * 胎压下限
             */
            sl_alert_low_pre.setValueRange(100, 300, false);
            sl_alert_low_pre.setValue(sl_pres_min, true);
            tv_pre_low_value.setText(String.valueOf(sl_pres_min + "Kpa"));
        } else if (3 == pres_unit) {
            /**
             * 胎压上限
             */
            rb_psi.setChecked(true);
            sl_alert_top_pre.setValueRange((int) (200 * 0.145), (int) (800 * 0.145), false);
            sl_alert_top_pre.setValue(sl_pres_max, true);
            tv_pre_top_value.setText(String.valueOf(sl_pres_max + "Psi"));
            /**
             * 胎压下限
             */
            sl_alert_low_pre.setValueRange((int) (100 * 0.145), (int) (300 * 0.145), false);
            sl_alert_low_pre.setValue(sl_pres_min, true);
            tv_pre_low_value.setText(String.valueOf(sl_pres_min + "Psi"));
        }
        // 温度单位
        if (1 == temp_unit) {  // 摄氏度
            rb_dc.setChecked(true);
            sl_aler_temp.setValueRange(50, 120, false);
            sl_aler_temp.setValue((float) sl_temp, true);
            tv_temp_top_value.setText(String.valueOf(sl_temp) + "℃");
        } else if (2 == temp_unit) {  // 华氏度
            rb_f.setChecked(true);
            sl_aler_temp.setValue((float) sl_temp, true);
            sl_aler_temp.setValueRange(122, 248, false);
            tv_temp_top_value.setText(String.valueOf(sl_temp) + "℉");
        }

    }

    /**
     * 重置默认参数
     */
    private void resetDefaultParams() {
        // 压力的默认单位是Kpa
        SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES, 2);
        rb_kap.setChecked(true);
        // 温度的默认单位是摄氏度
        SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_TEMP, 1);
        rb_dc.setChecked(true);
        // 胎压上限
        sl_alert_top_pre.setValueRange(200, 800, true);
        sl_alert_top_pre.setValue(300, true);
        SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX, 300f);
        // 胎压下限
        sl_alert_low_pre.setValueRange(200, 300, true);
        sl_alert_low_pre.setValue(200, true);
        SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN, 200f);
        // 温度上限
        sl_aler_temp.setValueRange(50, 120, true);
        sl_aler_temp.setValue(75, true);
        SpUtil.saveInt2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_TEMP_MAX, 75);
    }
}


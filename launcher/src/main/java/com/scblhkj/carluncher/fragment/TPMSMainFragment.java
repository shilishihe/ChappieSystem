package com.scblhkj.carluncher.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.rey.material.widget.Button;
import com.rey.material.widget.TextView;
import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.domain.TPMSSerialBean;
import com.scblhkj.carluncher.service.SerialDataCommSerivce;
import com.scblhkj.carluncher.utils.SpUtil;
import com.scblhkj.carluncher.utils.TitleBuilder;

/**
 * 胎压监测这Fragment
 */
public class TPMSMainFragment extends BaseFragment implements View.OnClickListener {

    // 左前轮胎压
    private TextView tv_left_top_pre;
    // 左前轮温度
    private TextView tv_left_top_temp;
    // 右前轮胎压
    private TextView tv_right_top_pre;
    // 右前轮温度
    private TextView tv_right_top_temp;
    // 左后轮胎压
    private TextView tv_left_bottom_pre;
    // 左后轮温度
    private TextView tv_left_bottom_temp;
    // 右后轮胎压
    private TextView tv_right_bottom_pre;
    // 右后轮温度
    private TextView tv_right_bottom_temp;
    // 接收串口数据的广播接收者
    private BroadcastReceiver serialCommReceiver;
    private static final String TAG = "TPMSMainFragment";

    // 胎压单位
    private TextView tv_left_top_pre_des;
    private TextView tv_right_top_pre_des;
    private TextView tv_left_bottom_pre_des;
    private TextView tv_right_bottom_pre_des;

    // 温度单位
    private TextView tv_left_top_temp_des;
    private TextView tv_right_top_temp_des;
    private TextView tv_left_bottom_temp_des;
    private TextView tv_right_bottom_temp_des;

    // 知道了按钮
    private Button bt_know;
    // 最高胎压
    private float tpmsPreHight;
    // 最低胎压
    private float tpmsPreLow;
    // 最高轮胎温度
    private int tpmsTempHight;
    // 胎压单位
    private int preUnit;
    // 轮胎温度单位
    private int temUnit;
    // 警告布局
    private RelativeLayout rl_warning;

    @Override
    public void onResume() {
        registerSerialCommReceiver();
        super.onResume();
    }

    @Override
    public void onPause() {
        unRegisterSerialCommReceiver();
        super.onPause();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tpmsmain, container, false);
        new TitleBuilder(view)
                .setTitleText("胎压监测")
                .setRightImage(R.mipmap.ic_tpms_setting)
                .setRightOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            tpmsFragmentInterface = (TPMSFragmentInterface) activity;
                            tpmsFragmentInterface.onTPMSFragmentSettingBtuonClick();
                        } catch (ClassCastException e) {
                            throw new ClassCastException(activity.toString() + "must implement TPMSFragmentInterface");
                        }
                    }
                }).build();
        initView();
        initData();
        return view;
    }

    @Override
    public void initView() {
        tv_left_top_pre = TextView.class.cast(view.findViewById(R.id.tv_left_top_pre));
        tv_left_top_temp = TextView.class.cast(view.findViewById(R.id.tv_left_top_temp));
        tv_right_top_pre = TextView.class.cast(view.findViewById(R.id.tv_right_top_pre));
        tv_right_top_temp = TextView.class.cast(view.findViewById(R.id.tv_right_top_temp));
        tv_left_bottom_pre = TextView.class.cast(view.findViewById(R.id.tv_left_bottom_pre));
        tv_left_bottom_temp = TextView.class.cast(view.findViewById(R.id.tv_left_bottom_temp));
        tv_right_bottom_pre = TextView.class.cast(view.findViewById(R.id.tv_right_bottom_pre));
        tv_right_bottom_temp = TextView.class.cast(view.findViewById(R.id.tv_right_bottom_temp));
        tv_left_top_pre_des = TextView.class.cast(view.findViewById(R.id.tv_left_top_pre_des));
        tv_right_top_pre_des = TextView.class.cast(view.findViewById(R.id.tv_right_top_pre_des));
        tv_left_bottom_pre_des = TextView.class.cast(view.findViewById(R.id.tv_left_bottom_pre_des));
        tv_right_bottom_pre_des = TextView.class.cast(view.findViewById(R.id.tv_right_bottom_pre_des));
        tv_left_top_temp_des = TextView.class.cast(view.findViewById(R.id.tv_left_top_temp_des));
        tv_right_top_temp_des = TextView.class.cast(view.findViewById(R.id.tv_right_top_temp_des));
        tv_left_bottom_temp_des = TextView.class.cast(view.findViewById(R.id.tv_left_bottom_temp_des));
        tv_right_bottom_temp_des = TextView.class.cast(view.findViewById(R.id.tv_right_bottom_temp_des));
        rl_warning = RelativeLayout.class.cast(view.findViewById(R.id.rl_warning));
        bt_know = Button.class.cast(view.findViewById(R.id.bt_know));
    }

    @Override
    public void initData() {
        // 初始化串口通信的接收者
        serialCommReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (SerialDataCommSerivce.SERIAL_TPMS_ACTION.equals(action)) {
                    // 处理代码
                    Bundle bundle = intent.getExtras();
                    TPMSSerialBean bean = (TPMSSerialBean) bundle.getSerializable(SerialDataCommSerivce.SerialHelperControl.SERIAL_TMPA_DATA);
                    if (bean != null) {
                        int[][] datass = bean.getDatasss();
                        for (int i = 0; i < datass.length; i++) {
                            if ((datass[i][0] == 0x01) && (datass[i][2] == 0x02)) {
                                setDatas(datass[i]);
                                checkData(datass[i]);
                            }
                        }
                    }
                }
            }
        };
        bt_know.setOnClickListener(this);
        initSpData();

    }

    /**
     * 给Sp数据附上初值
     */
    private void initSpData() {
        preUnit = SpUtil.getInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES);
        temUnit = SpUtil.getInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_TEMP);
        tpmsPreHight = SpUtil.getFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX);
        tpmsPreLow = SpUtil.getFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN);
        tpmsTempHight = SpUtil.getInt2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_TEMP_MAX);
        if (0 == preUnit) {
            preUnit = 2; // 默认的单位Kpa
            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES, preUnit);
        }
        if (0 == temUnit) { // 默认的单位摄氏度
            temUnit = 1;
            SpUtil.saveInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_TEMP, temUnit);
        }
        if (0 == tpmsPreHight) {  // 默认上限400Kpa
            tpmsPreHight = 300;
            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MAX, tpmsPreHight);
        }
        if (0 == tpmsPreLow) { // 默认下限180Kpa
            tpmsPreLow = 200;
            SpUtil.saveFloat2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_PRESSURE_MIN, tpmsPreLow);
        }
        if (0 == tpmsTempHight) {  // 默认上限70摄氏度
            tpmsTempHight = 75;
            SpUtil.saveInt2SP(ChappieConstant.TMPS_FRAGMENT_SLIDER_TEMP_MAX, tpmsTempHight);
        }
    }

    /**
     * 判断是否出现告警数据
     */
    private void checkData(int[] rd) {

    }

    private void setPreUnit(String unit) {
        tv_left_top_pre_des.setText(unit);
        tv_right_top_pre_des.setText(unit);
        tv_left_bottom_pre_des.setText(unit);
        tv_right_bottom_pre_des.setText(unit);
    }

    private void setTempUnit(String unit) {
        tv_left_top_temp_des.setText(unit);
        tv_right_top_temp_des.setText(unit);
        tv_left_bottom_temp_des.setText(unit);
        tv_right_bottom_temp_des.setText(unit);
    }

    private void setDatas(int[] rd) {
        // 解析数据
        if (rd[0] == 1 && rd[1] == 255) {  //判断包头
            if (rd[2] == 0X02) {  //判断是否是胎压数据
                if (doCheckData(rd) == rd[rd[3] + 3]) {
                    int unit = SpUtil.getInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_PRES);
                    float f = 3.44f;
                    if (0 == unit || 2 == unit) {
                        f = 3.44f;  //kpa
                    } else if (1 == unit) {
                        f = 3.44f * 0.01f;
                    } else if (3 == unit) {
                        f = 3.44f * 0.1450377f;
                    }

                    int pUnit = SpUtil.getInt2SP(ChappieConstant.TPMS_FRAGMENT_UNIT_TEMP);
                    float pf = 0f;
                    if (0 == pUnit || 2 == pUnit) {
                        pf = rd[9];
                    } else if (1 == pUnit) {
                        pf = rd[9] * 1.8f + 30;
                    }

                    switch (rd[6]) {
                        case 0X0a: {
                            switch (rd[7]) {
                                case 0X00: {
                                    tv_left_top_pre.setText(String.format("%.2f", rd[8] * f));
                                    tv_left_top_temp.setText(String.format("%.1f", pf));
                                    break;
                                }
                                case 0X01: {
                                    tv_right_top_pre.setText(String.format("%.2f", rd[8] * f));
                                    tv_right_top_temp.setText(String.format("%.1f", pf));
                                    break;
                                }
                                case 0X10: {
                                    tv_left_bottom_pre.setText(String.format("%.2f", rd[8] * f));
                                    tv_left_bottom_temp.setText(String.format("%.1f", pf));
                                    break;
                                }
                                case 0X11: {
                                    tv_right_bottom_pre.setText(String.format("%.2f", rd[8] * f));
                                    tv_right_bottom_temp.setText(String.format("%.1f", pf));
                                    break;
                                }
                                default: {
                                    break;
                                }
                            }
                            break;
                        }
                        default: {
                            break;
                        }

                    }
                }
            } else {
                if (ChappieCarApplication.isDebug)
                Log.e(TAG, "胎压数据不合法");
            }
        }
    }

    /**
     * 异或校验检查数据的准确性
     */
    private int doCheckData(int[] datas) {
        int r = datas[4];
        int l = datas[3];
        for (int i = 5; i < l + 3; i++) {
            r = r ^ datas[i];
        }
        return r;
    }

    /**
     * 注册串口通信的广播
     */
    private void registerSerialCommReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SerialDataCommSerivce.SERIAL_TPMS_ACTION);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        activity.registerReceiver(serialCommReceiver, intentFilter);
    }

    /**
     * 注销串口通信的广播
     */
    private void unRegisterSerialCommReceiver() {
        if (serialCommReceiver != null) {
            activity.unregisterReceiver(serialCommReceiver);
        }
    }

    private TPMSFragmentInterface tpmsFragmentInterface;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_know: {
                rl_warning.setVisibility(View.GONE);
                break;
            }
        }
    }

    public interface TPMSFragmentInterface {
        void onTPMSFragmentSettingBtuonClick();
    }
}

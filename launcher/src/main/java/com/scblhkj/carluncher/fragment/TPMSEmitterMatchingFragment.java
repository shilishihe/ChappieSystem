package com.scblhkj.carluncher.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.rey.material.widget.Button;
import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.activity.TMPSSettingActivity;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.domain.TPMSSerialBean;
import com.scblhkj.carluncher.service.SerialDataCommSerivce;
import com.scblhkj.carluncher.utils.TitleBuilder;
import com.scblhkj.carluncher.widget.dialog.TMPSEMatchDialog;

/**
 * 胎压发射器配对界面
 */
public class TPMSEmitterMatchingFragment extends BaseFragment implements View.OnClickListener {


    private Button bt_lt;
    private Button bt_rt;
    private Button bt_lb;
    private Button bt_rb;
    private BroadcastReceiver serialCommReceiver;
    private RelativeLayout rl_loading;
    private Button bt_cancel;
    private boolean canClick = true;

    private TMPSEmitterMatchingFragmentInterface mListener;
    private static final String TAG = "胎压发射器";

    public TPMSEmitterMatchingFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tpmsemitter_matching, container, false);
        new TitleBuilder(view)
                .setTitleText("发射器配对")
                .setLeftImage(R.mipmap.ic_tpms_back)
                .setLeftOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // 返回胎压设置界面
                        try {
                            if (canClick) {
                                mListener = (TMPSEmitterMatchingFragmentInterface) activity;
                                mListener.OnTMPSEmitterMatchingFragmentBackPressed();
                            }
                        } catch (ClassCastException e) {
                            throw new ClassCastException(activity.toString() + "must implement TMPSEmitterMatchingFragmentInterface");
                        }
                    }
                }).build();
        initView();
        initData();
        return view;
    }

    @Override
    public void initView() {
        bt_lt = (Button) view.findViewById(R.id.bt_lt);
        bt_rt = (Button) view.findViewById(R.id.bt_rt);
        bt_lb = (Button) view.findViewById(R.id.bt_lb);
        bt_rb = (Button) view.findViewById(R.id.bt_rb);
        bt_cancel = (Button) view.findViewById(R.id.bt_cancel);
        rl_loading = (RelativeLayout) view.findViewById(R.id.rl_loading);
    }

    @Override
    public void initData() {
        // 初始化串口通信的接收者
        serialCommReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (SerialDataCommSerivce.SERIAL_TPMS_ACTION.equals(action)) {
                    // 处理代码
                    Bundle bundle = intent.getExtras();
                    TPMSSerialBean bean = (TPMSSerialBean) bundle.getSerializable(SerialDataCommSerivce.SerialHelperControl.SERIAL_TMPA_DATA);
                    if (bean != null) {
                        int[][] datass = bean.getDatasss();
                        for (int i = 0; i < datass.length; i++) {
                            if ((datass[i][0] == 0x01) && (datass[i][2] == 0x02)) {
                                // 配对成功或是不成功
                                setDatas(datass[i]);
                            }
                        }
                    }
                }
            }
        };
        bt_lt.setOnClickListener(this);
        bt_rt.setOnClickListener(this);
        bt_lb.setOnClickListener(this);
        bt_rb.setOnClickListener(this);
        bt_cancel.setOnClickListener(this);
    }

    private void setDatas(int[] rd) {
        // 解析数据
        if (rd[0] == 1 && rd[1] == 255) {  //判断包头
            if (rd[2] == 0X02) {  //判断是否是胎压数据
            } else {
                if (ChappieCarApplication.isDebug)
                Log.e(TAG, "胎压数据不合法");
            }
        }
    }

    @Override
    public void onResume() {
        registerSerialCommReceiver();
        super.onResume();
    }

    @Override
    public void onPause() {
        unRegisterSerialCommReceiver();
        super.onPause();
    }

    /**
     * 注册串口通信的广播
     */
    private void registerSerialCommReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SerialDataCommSerivce.SERIAL_TPMS_ACTION);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        activity.registerReceiver(serialCommReceiver, intentFilter);
    }

    /**
     * 注销串口通信的广播
     */
    private void unRegisterSerialCommReceiver() {
        if (serialCommReceiver != null) {
            activity.unregisterReceiver(serialCommReceiver);
        }
    }

    // 显示加载布局
    private void showLoadingView() {
        canClick = false;
        rl_loading.setVisibility(View.VISIBLE);
    }

    // 隐藏加载布局
    private void hideLoadingView() {
        canClick = true;
        rl_loading.setVisibility(View.GONE);
    }

    @Override

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_lt: {
                showLoadingView();
                break;
            }
            case R.id.bt_rt: {
                showLoadingView();
                break;
            }
            case R.id.bt_lb: {
                showLoadingView();
                break;
            }
            case R.id.bt_rb: {
                showLoadingView();
                break;
            }
            case R.id.bt_cancel: {
                hideLoadingView();
                break;
            }
            default: {
                break;
            }
        }
    }

    public interface TMPSEmitterMatchingFragmentInterface {
        void OnTMPSEmitterMatchingFragmentBackPressed();
    }

    /**
     * 向串口发送数据
     */
    private void sendData2Port(byte[] bs) {
        Intent dataIntent = new Intent();
        dataIntent.setAction(SerialDataCommSerivce.SERIAL_ACTION_RECEIVE);
        dataIntent.addCategory(Intent.CATEGORY_DEFAULT);
        Bundle bundle = new Bundle();
        bundle.putByteArray(TMPSSettingActivity.CMD, bs);
        dataIntent.putExtras(bundle);
        activity.sendBroadcast(dataIntent);
    }

    /**
     * 弹出对话框
     */
    private void showDialog() {
        TMPSEMatchDialog.Builder builder = new TMPSEMatchDialog.Builder(activity);
        builder.setTitle("正在匹配").setCancelButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.dismiss();
            }
        }).create().show();
    }
}

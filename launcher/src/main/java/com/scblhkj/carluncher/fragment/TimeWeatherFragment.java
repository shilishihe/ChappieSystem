package com.scblhkj.carluncher.fragment;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.LocationSource;
import com.amap.api.services.weather.LocalDayWeatherForecast;
import com.amap.api.services.weather.LocalWeatherForecast;
import com.amap.api.services.weather.LocalWeatherForecastResult;
import com.amap.api.services.weather.LocalWeatherLive;
import com.amap.api.services.weather.LocalWeatherLiveResult;
import com.amap.api.services.weather.WeatherSearch;
import com.amap.api.services.weather.WeatherSearchQuery;
import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.utils.ToastUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by blh on 2016-06-20.
 */
public class TimeWeatherFragment extends Fragment implements LocationSource,
        AMapLocationListener,WeatherSearch.OnWeatherSearchListener {
    private View timeWeatherView;
    private TextView tv_nowTime,tv_nowMonth,tv_nowWeek,tv_nowTemp;
    private ImageView iv_weather;
    private TextView tv_nowLocation;
    private static String mYear;
    private static String mMonth;
    private static String mDay;
    private static String mWay;
    private static String mTime;
    private OnLocationChangedListener mListener;
    public AMapLocationClientOption mLocationOption = null;
    private AMapLocationClient mlocationClient;
    private String loc;
    private WeatherSearchQuery mquery;
    private WeatherSearch mweathersearch;
    private LocalWeatherLive weatherlive;
    private OnLocationChangedListener mLocationChangeListener = null;
    private LocalWeatherForecast weatherforecast;
    private List<LocalDayWeatherForecast> forecastlist = null;
    private String cityname ;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private static final int msgKey1 = 1;
    private static final int msgKey2=2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        timeWeatherView=inflater.inflate(R.layout.layout1, container, false);
        initView();
        initData();
        return  timeWeatherView;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(mlocationClient!=null){
            mlocationClient.stopLocation();//ֹͣ��λ
        }
        deactivate();
        getNowDataInfo();//获取当前日期
        getLocation();//获取定位信息
        Log.e("info", "TimeWeatherFragment在OnStart");
    }

    @Override
    public void onStop() {

        super.onStop();
        Log.e("info", "TimeWeatherFragment在OnStop");
    }

    private void initView() {
        sp=getActivity().getSharedPreferences("deskstate", Activity.MODE_PRIVATE);
        editor=sp.edit();
        tv_nowTime=(TextView)timeWeatherView.findViewById(R.id.tv_nowTime);
        tv_nowMonth=(TextView)timeWeatherView.findViewById(R.id.tv_nowMonth);
        tv_nowWeek=(TextView)timeWeatherView.findViewById(R.id.tv_nowWeek);
        tv_nowTemp=(TextView)timeWeatherView.findViewById(R.id.tv_nowTemp);
        iv_weather=(ImageView)timeWeatherView.findViewById(R.id.iv_weather);
        tv_nowLocation=(TextView)timeWeatherView.findViewById(R.id.tv_nowLocation);
    }

    private void searchliveweather() {
        if(loc!=null){
            mquery = new WeatherSearchQuery(loc, WeatherSearchQuery.WEATHER_TYPE_LIVE);//检索参数为城市和天气类型，实时天气为1、天气预报为2
            mweathersearch=new WeatherSearch(getActivity());
            mweathersearch.setOnWeatherSearchListener(this);
            mweathersearch.setQuery(mquery);
            mweathersearch.searchWeatherAsyn(); //异步搜索
        }
    }
    private void initData() {
        new ClockThread().start();
        new DateWeatherThread().start();

    }

  public class DateWeatherThread extends Thread{
      @Override
      public void run() {
          try {
              Thread.sleep(1000);
              Message message=new Message();
              message.what=msgKey2;
              mHandler.sendMessage(message);
          } catch (InterruptedException e) {
              e.printStackTrace();
          }
      }
  }
   public class ClockThread extends Thread{
       @Override
       public void run() {
           do{
               try {
                   Thread.sleep(1000);
                   Message msg=new Message();
                   msg.what=msgKey1;
                   mHandler.sendMessage(msg);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
           }while(true);
       }
   }


    private Handler mHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case msgKey1:
                    long sysTime = System.currentTimeMillis();
                    SimpleDateFormat sdf=new SimpleDateFormat("HH:mm", Locale.getDefault());
                    CharSequence sysTimeStr = sdf.format(new Date());
                    tv_nowTime.setText(sysTimeStr);
                    break;
                case msgKey2:
                    getNowDataInfo();//获取当前日期
                    getLocation();//获取定位信息
                 // getDateWeatherInfo();
                    break;
            }
        }
    };
    /****
     * 获取位置信息
     */
    private void getLocation() {
        Log.e("info","正在获取定位信息....");
        mlocationClient = new AMapLocationClient(getActivity());
        mLocationOption=new AMapLocationClientOption();
        mlocationClient.setLocationListener(this);
        //设置定位模式为高精度模式，Battery_Saving为低功耗模式，Device_Sensors是仅设备模式
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        //设置定位间隔,单位毫秒,默认为2000ms
        mLocationOption.setInterval(5000);
        //设置定位参数
        mlocationClient.setLocationOption(mLocationOption);
        // 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
        //启动定位
        mlocationClient.startLocation();
        Log.e("info", "启动定位....");
    }

    /****
     * 获取当前日期
     */
    private void getNowDataInfo() {
        final Calendar c=Calendar.getInstance();
        c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
        mYear=String.valueOf(c.get(Calendar.YEAR));//获取当前年份
        mMonth=String.valueOf(c.get(Calendar.MONTH)+1);//获取当前月份
        mDay=String.valueOf(c.get(Calendar.DAY_OF_MONTH));//获取当前月份的日期号
        mWay=String.valueOf(c.get(Calendar.DAY_OF_WEEK));
        Log.e("info", "当前日期:" + mYear + "-" + mMonth + "-" + mDay);
        if("1".equals(mWay)){
            mWay="天";
        }else if("2".equals(mWay)){
            mWay="一";
        }else if("3".equals(mWay)){
            mWay="二";
        }else if("4".equals(mWay)){
            mWay="三";
        }else if("5".equals(mWay)){
            mWay="四";
        }else if("6".equals(mWay)){
            mWay="五";
        }else if("7".equals(mWay)){
            mWay="六";
        }
        String date=mMonth + "月" + mDay + "日";
        String week="星期" + mWay;
        tv_nowMonth.setText(mMonth + "月" + mDay + "日");
        tv_nowWeek.setText("星期" + mWay);
        /****
         *   if(sp.getString("nowDateInfo","")!=null||!TextUtils.isEmpty(sp.getString("nowDateInfo",""))){ //如果日期缓存不为空
         if(!date.equals(sp.getString("nowDateInfo",""))){ //如果新获取的日期和之前缓存的日期不想同则使用新获取的日期，并做缓存
         tv_nowMonth.setText(mMonth + "月" + mDay + "日");
         editor.putString("nowDateInfo", "");
         editor.commit();
         }else {
         Log.e("info","return");
         }

         }else {
         tv_nowMonth.setText(mMonth + "月" + mDay + "日");
         editor.putString("nowDateInfo", "");
         editor.commit();
         }
         if(sp.getString("nowWeekInfo","")!=null||!TextUtils.isEmpty(sp.getString("nowWeekInfo",""))){
         if(!week.equals(sp.getString("nowWeekInfo",""))){ //如果新获取的星期几和之前缓存的星期几不想同则使用新获取的星期几，并做缓存
         tv_nowWeek.setText(week);
         editor.putString("nowWeekInfo",week);
         editor.commit();
         }else {
         // return;
         Log.e("info","return");
         }
         }else {
         tv_nowWeek.setText(week);
         editor.putString("nowWeekInfo",week);
         editor.commit();
         }
         */

      //  editor.putString("nowDateInfo", date);
      //  editor.putString("nowWeekInfo",week);

       // tv_nowMonth.setText(mMonth + "月" + mDay + "日");
       // tv_nowWeek.setText("星期" + mWay);

    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
             if(aMapLocation!=null){
                 if(aMapLocation.getErrorCode()==0){
                     aMapLocation.getLocationType();//获取当前定位结果来源，如网络定位结果，详见定位类型表
                     loc = aMapLocation.getCity().toString();
                     Log.e("info", "定位的城市是:" + loc);
                     tv_nowLocation.setText(loc);
                     //getWeatherInfo();//根据定位信息来获取该位置的天气信息
                     searchliveweather();
                 }else{
                     //显示错误信息ErrCode是错误码，errInfo是错误信息，详见错误码表。
                     Log.e("AmapError", "location Error, ErrCode:"
                             + aMapLocation.getErrorCode() + ", errInfo:"
                             + aMapLocation.getErrorInfo());
                 }
             }else{
                 Log.e("info","aMapLocation为空");
             }
    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        //Log.d(TAG, "activate");
        mLocationChangeListener = onLocationChangedListener;
        if (mlocationClient == null) {
            getLocation();
        }
    }

    @Override
    public void deactivate() {

        mLocationChangeListener = null;
        if (mlocationClient != null) {
            mlocationClient.stopLocation();
            mlocationClient.onDestroy();
            mlocationClient = null;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mlocationClient!=null){
            mlocationClient.stopLocation();//ֹͣ��λ
        }
        deactivate();
        Log.e("info", "TimeWeatherFragment在onPause");
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mlocationClient!=null){
            mlocationClient.startLocation();
        }
        Log.e("info", "TimeWeatherFragment在onResume()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("info", "TimeWeatherFragment在onDestroy()");
    }

    /**
     * 实时天气查询回调
     */
    @Override
    public void onWeatherLiveSearched(LocalWeatherLiveResult weatherLiveResult ,int rCode) {
        if (rCode == 1000) {
            if (weatherLiveResult != null && weatherLiveResult.getLiveResult() != null) {
                weatherlive = weatherLiveResult.getLiveResult();
                tv_nowTemp.setText(weatherlive.getTemperature()+"℃");
                Log.e("info", "实时天气》》》" + weatherlive.getWeather());
                String nowWeather=weatherlive.getWeather();
                if(nowWeather.equals("晴")){
                    iv_weather.setImageResource(R.mipmap.ic_day_sunny);
                }else if(nowWeather.equals("多云")){
                    iv_weather.setImageResource(R.mipmap.ic_day_cloudy);
                }else if(nowWeather.equals("阴")){
                    iv_weather.setImageResource(R.mipmap.ic_day_yin);
                } else if(nowWeather.equals("阵雨")){
                    iv_weather.setImageResource(R.mipmap.ic_day_shower);
                }else if(nowWeather.equals("雷阵雨")){
                    iv_weather.setImageResource(R.mipmap.ic_day_thunder_shower);
                }else if(nowWeather.equals("雷阵雨并伴有冰雹")){
                    iv_weather.setImageResource(R.mipmap.ic_day_thunder_shower);
                }else if(nowWeather.equals("雨夹雪")){
                    iv_weather.setImageResource(R.mipmap.ic_day_sleet);
                }else if(nowWeather.equals("小雨")){
                    iv_weather.setImageResource(R.mipmap.ic_day_light_rain);
                }else if(nowWeather.equals("中雨")){
                    iv_weather.setImageResource(R.mipmap.ic_day_moderate_rain);
                }else if(nowWeather.equals("大雨")){
                    iv_weather.setImageResource(R.mipmap.ic_day_heavy_rain);
                }else if(nowWeather.equals("暴雨")){
                    iv_weather.setImageResource(R.mipmap.ic_day_heavy_rain);
                }else if(nowWeather.equals("大暴雨")){
                    iv_weather.setImageResource(R.mipmap.ic_day_heavy_rain_heavy_rain);
                }else if(nowWeather.equals("特大暴雨")){
                    iv_weather.setImageResource(R.mipmap.ic_day_heavy_rain_heavy_rains);
                }else if(nowWeather.equals("阵雪")){
                    iv_weather.setImageResource(R.mipmap.ic_day_snow_shower);
                }else if(nowWeather.equals("小雪")){
                    iv_weather.setImageResource(R.mipmap.ic_day_light_snow);
                }else if(nowWeather.equals("中雪")){
                    iv_weather.setImageResource(R.mipmap.ic_day_moderate_snow);
                }else if(nowWeather.equals("大雪")){
                    iv_weather.setImageResource(R.mipmap.ic_day_heavy_snow);
                }else if(nowWeather.equals("暴雪")){
                    iv_weather.setImageResource(R.mipmap.ic_day_blizzard);
                }else if(nowWeather.equals("雾")){
                    iv_weather.setImageResource(R.mipmap.ic_day_fog);
                }else if(nowWeather.equals("冻雨")){
                    iv_weather.setImageResource(R.mipmap.ic_day_freezing_rain);
                }else if(nowWeather.equals("沙尘暴")){
                    iv_weather.setImageResource(R.mipmap.ic_day_strong_sandstorms);
                }else if(nowWeather.equals("小雨-中雨")){
                    iv_weather.setImageResource(R.mipmap.ic_day_light_rain_moderate_rain);
                }else if(nowWeather.equals("中雨-大雨")){
                    iv_weather.setImageResource(R.mipmap.ic_day_light_rain_moderate_rain);
                }else if(nowWeather.equals("大雨-暴雨")){
                    iv_weather.setImageResource(R.mipmap.ic_day_heavy_rain_heavy_rains);
                }else if(nowWeather.equals("暴雨-大暴雨")){
                    iv_weather.setImageResource(R.mipmap.ic_day_heavy_rains_torrential_rain);
                }else if(nowWeather.equals("大暴雨-特大暴雨")){
                    iv_weather.setImageResource(R.mipmap.ic_day_heavy_rains_torrential_rain);
                }else if(nowWeather.equals("小雪-中雪")){
                    iv_weather.setImageResource(R.mipmap.ic_day_light_snow_moderate_snow);
                }else if(nowWeather.equals("中雪-大雪")){
                    iv_weather.setImageResource(R.mipmap.ic_day_moderate_snow_heavy_snow);
                }else if(nowWeather.equals("大雪-暴雪")){
                    iv_weather.setImageResource(R.mipmap.ic_day_heavy_snow_blizzard);
                }else if(nowWeather.equals("浮尘")){
                    iv_weather.setImageResource(R.mipmap.ic_day_dust_storms);
                }else if(nowWeather.equals("扬沙")){
                    iv_weather.setImageResource(R.mipmap.ic_day_dust_storms);
                }else if(nowWeather.equals("强沙尘暴")){
                    iv_weather.setImageResource(R.mipmap.ic_day_dust_storms);
                }else if(nowWeather.equals("飑")){
                    iv_weather.setImageResource(R.mipmap.ic_day_dust_storms);
                }else if(nowWeather.equals("龙卷风")){
                    iv_weather.setImageResource(R.mipmap.ic_day_torrential_rain);
                }
                else if(nowWeather.equals("弱高吹雪")){
                    iv_weather.setImageResource(R.mipmap.ic_day_snow_shower);
                }else if(nowWeather.equals("轻霾")){
                    iv_weather.setImageResource(R.mipmap.ic_day_haze);
                }else if(nowWeather.equals("轻霾")){
                    iv_weather.setImageResource(R.mipmap.ic_day_haze);
                }

            }else {
                // ToastUtil.show(WeatherSearchActivity.this, R.string.no_result);
                Log.e("info>>>>>>>", "没有查到相关天气信息");
            }
        }else {
            Log.e("info", "rcode>>>>>"+rCode);
        }
    }

    @Override
    public void onWeatherForecastSearched(LocalWeatherForecastResult localWeatherForecastResult, int i) {

    }
}

package com.scblhkj.carluncher.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scblhkj.carluncher.activity.MainActivity;


/**
 * Created by he on 2015/8/7.
 * Fragment基类
 */
abstract class BaseFragment extends Fragment {

    protected MainActivity activity;
    /**
     * 所有子类去实现各自的View
     */
    protected View view;


   /* */

    /**
     * 回调接口
     * MainActivity中打开和关闭抽屉的回调接
     *//*
    private OnLeftButtonClick onLeftButtonClick;

    public interface OnLeftButtonClick {
        void onLeftButtonClickListener();
    }*/


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
    }

    protected void intent2Activity(Class<? extends Activity> tarActivity) {
        Intent intent = new Intent(activity, tarActivity);
        startActivity(intent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return view;
    }

    /**
     * 初始化布局
     */
    public abstract void initView();

    /**
     * 初始化数据
     */
    public abstract void initData();

}

package com.scblhkj.carluncher.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.scblhkj.carluncher.R;
import com.scblhkj.carluncher.activity.CarStateMainActivity;
import com.scblhkj.carluncher.app.ChappieCarApplication;
import com.scblhkj.carluncher.common.ChappieConstant;
import com.scblhkj.carluncher.domain.GPSInfo;
import com.scblhkj.carluncher.domain.OBDSerialBean;
import com.scblhkj.carluncher.service.SerialDataCommSerivce;
import com.scblhkj.carluncher.utils.ObdSerialUtil;
import com.scblhkj.carluncher.widget.MarqueeTextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 车辆状态Fragment
 */
public class CarStateFragment extends Fragment implements CarStateMainActivity.CarStateMainActivityCallBack {

    private BroadcastReceiver serialCommReceiver;
    protected CarStateMainActivity activity;
    protected View view;
    private static final String TAG = "CarStateFragment";
    /************************
     * 控件
     *****************************/
    private ImageView im_engine;
    private ImageView im_engine_loading;
    private TextView tv_engine;
    private ImageView im_solar_term_door;
    private ImageView im_solar_term_door_loading;
    private TextView tv_solar_term_door;
    private ImageView im_idling_system;
    private ImageView im_idling_systemt_loading;
    private TextView tv_idling_systemt;
    private ImageView im_inlet;
    private ImageView im_inlet_loading;
    private TextView tv_inlet;
    private ImageView im_cooling;
    private ImageView im_cooling_loading;
    private TextView tv_cooling;
    private ImageView im_emission;
    private ImageView im_emission_loading;
    private TextView tv_emission;
    private ImageView im_power;
    private ImageView im_power_loading;
    private TextView tv_power;
    private ImageView im_state_loading;
    private ImageView im_state_more;
    private Animation rAnim; //左转动画
    private MarqueeTextView tv_location;
    private MarqueeTextView tv_carDetail; // 汽车品牌的详细信息
    private MarqueeTextView tv_lastScanTime; // 上次扫描的时间


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (CarStateMainActivity) getActivity();
        activity.setCarStateMainActivityCallBack(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = View.inflate(activity, R.layout.fragment_car_state, null);
        return view;
    }

    /**
     * 初始化控件
     */
    private void initView() {
        im_engine = (ImageView) view.findViewById(R.id.im_engine);
        im_engine_loading = (ImageView) view.findViewById(R.id.im_engine_loading);
        tv_engine = (TextView) view.findViewById(R.id.tv_engine);
        im_solar_term_door = (ImageView) view.findViewById(R.id.im_solar_term_door);
        im_solar_term_door_loading = (ImageView) view.findViewById(R.id.im_solar_term_door_loading);
        tv_solar_term_door = (TextView) view.findViewById(R.id.tv_solar_term_door);
        im_idling_system = (ImageView) view.findViewById(R.id.im_idling_system);
        im_idling_systemt_loading = (ImageView) view.findViewById(R.id.im_idling_systemt_loading);
        tv_idling_systemt = (TextView) view.findViewById(R.id.tv_idling_systemt);
        im_inlet = (ImageView) view.findViewById(R.id.im_inlet);
        im_inlet_loading = (ImageView) view.findViewById(R.id.im_inlet_loading);
        tv_inlet = (TextView) view.findViewById(R.id.tv_inlet);
        im_cooling = (ImageView) view.findViewById(R.id.im_cooling);
        im_cooling_loading = (ImageView) view.findViewById(R.id.im_cooling_loading);
        tv_cooling = (TextView) view.findViewById(R.id.tv_cooling);
        im_emission = (ImageView) view.findViewById(R.id.im_emission);
        im_emission_loading = (ImageView) view.findViewById(R.id.im_emission_loading);
        tv_emission = (TextView) view.findViewById(R.id.tv_emission);
        im_power = (ImageView) view.findViewById(R.id.im_power);
        im_power_loading = (ImageView) view.findViewById(R.id.im_power_loading);
        tv_power = (TextView) view.findViewById(R.id.tv_power);
        im_state_loading = (ImageView) view.findViewById(R.id.im_state_loading);
        tv_location = (MarqueeTextView) view.findViewById(R.id.tv_location);
        tv_carDetail = (MarqueeTextView) view.findViewById(R.id.tv_carDetail);
        tv_lastScanTime = (MarqueeTextView) view.findViewById(R.id.tv_lastScanTime);
        im_state_more = (ImageView) view.findViewById(R.id.im_state_more);
    }

    @Override
    public void onStart() {
        super.onStart();
        initView();
        initData();
        initBroadcastReceiver();
        initAnimation();
    }

    @Override
    public void onResume() {
        super.onResume();
        registerSerialCommReceiver();
    }

    @Override
    public void onPause() {
        super.onPause();
        unRegisterSerialCommReceiver();
    }


    /**
     * 将TextView的颜色设置成警告色
     *
     * @param tv
     */
    private void setWarnningTextColor(TextView tv) {
        tv.setTextColor(activity.getResources().getColor(R.color.red));
    }

    private void initBroadcastReceiver() {
        // 初始化串口通信的接收者
        serialCommReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case SerialDataCommSerivce.SERIAL_OBD_ACTION: {
                        OBDSerialBean bean = (OBDSerialBean) intent.getExtras().getSerializable(SerialDataCommSerivce.SerialHelperControl.SERIAL_OBD_DATA);
                        int[][] datass = bean.getDatass();   // OBD数据
                        int length = datass.length;
                        for (int i = 0; i < length; i++) {
                            if (datass[i][0] == 0X28) {
                                if (datass[i][8] == 0x85) {
                                    float result = datass[i][31] / 10f;  // 在此可以显示实时电压
                                } else if (datass[i][8] == 0X8A) {  // 车辆体检
                                    if ((datass[i][12] & 0x01) == 0x01) {
                                        if (!ChappieCarApplication.isDebug)
                                            Log.e(TAG, "发动机无故障");
                                        Log.e(TAG, "datass[i][12]");
                                    } else {
                                        if (!ChappieCarApplication.isDebug)
                                            Log.e(TAG, "发动机有故障");
                                        im_engine.setBackgroundResource(R.mipmap.ic_car_state_tpms_warning);
                                        setWarnningTextColor(tv_engine);
                                    }
                                    if ((datass[i][12] & 0x02) == 0x02) {
                                        if (!ChappieCarApplication.isDebug)
                                            Log.e(TAG, "电源电路正常");
                                    } else {
                                        if (!ChappieCarApplication.isDebug)
                                            Log.e(TAG, "电源电路异常");
                                        im_power.setBackgroundResource(R.mipmap.ic_car_state_powerwarning);
                                        setWarnningTextColor(tv_power);
                                    }
                                    if ((datass[i][12] & 0x04) == 0x04) {
                                        if (!ChappieCarApplication.isDebug)
                                            Log.e(TAG, "节气门开度正常");
                                    } else {
                                        if (!ChappieCarApplication.isDebug)
                                            Log.e(TAG, "节气门开度异常");
                                        im_solar_term_door.setBackgroundResource(R.mipmap.ic_car_state_battery_oltage_warning);
                                        setWarnningTextColor(tv_solar_term_door);
                                    }
                                    if ((datass[i][12] & 0x08) == 0x08) {
                                        if (!ChappieCarApplication.isDebug)
                                            Log.e(TAG, "排放系统良好");
                                    } else {
                                        if (!ChappieCarApplication.isDebug)
                                            Log.e(TAG, "排放系统异常");
                                        im_emission.setBackgroundResource(R.mipmap.ic_car_state_emission_warning);
                                        setWarnningTextColor(tv_emission);
                                    }
                                    if ((datass[i][12] & 0x10) == 0x10) {
                                        if (!ChappieCarApplication.isDebug)
                                            Log.e(TAG, "冷却系统良好");
                                    } else {
                                        if (!ChappieCarApplication.isDebug)
                                            Log.e(TAG, "冷却系统异常");
                                        im_cooling.setBackgroundResource(R.mipmap.ic_car_state_cooling_warning);
                                        setWarnningTextColor(tv_cooling);
                                    }
                                    if ((datass[i][12] & 0x20) == 0x20) {
                                        if (!ChappieCarApplication.isDebug)
                                            Log.e(TAG, "怠速高");
                                        im_idling_system.setBackgroundResource(R.mipmap.ic_car_state_idling_system_warning);
                                        setWarnningTextColor(tv_idling_systemt);
                                    }
                                    if ((datass[i][12] & 0x40) == 0x40) {
                                        if (!ChappieCarApplication.isDebug)
                                            Log.e(TAG, "怠速低");
                                        im_idling_system.setBackgroundResource(R.mipmap.ic_car_state_idling_system_normal);
                                        setWarnningTextColor(tv_idling_systemt);
                                    }
                                    activity.stopScanning(); //停止扫描
                                }
                            } else if (datass[i][8] == 0x91) {  // 获取报警消息

                            }
                        }
                        break;
                    }
                    case ChappieConstant.GPS_INFO: {
                        GPSInfo gpsInfo = (GPSInfo) intent.getSerializableExtra(ChappieConstant.GPS_INFO);
                        tv_location.setText(gpsInfo.getAddress());
                        break;
                    }
                    case SerialDataCommSerivce.SERIAL_TPMS_ACTION: {
                        break;
                    }
                    default: {
                        break;
                    }
                }

            }
        };
    }

    /**
     * 注册串口通信的广播
     */
    private void registerSerialCommReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SerialDataCommSerivce.SERIAL_OBD_ACTION);  // OBD数据广播
        intentFilter.addAction(SerialDataCommSerivce.SERIAL_TPMS_ACTION);  // TPMS数据广播
        intentFilter.addAction(ChappieConstant.GPS_INFO); // 车辆位置广播
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        activity.registerReceiver(serialCommReceiver, intentFilter);
    }

    /**
     * 注销串口通信的广播
     */
    private void unRegisterSerialCommReceiver() {
        if (serialCommReceiver != null) {
            activity.unregisterReceiver(serialCommReceiver);
        }
    }

    /**
     * 初始化数据
     */
    private void initData() {

    }

    /**
     * 初始化动画
     */
    private void initAnimation() {
        rAnim = AnimationUtils.loadAnimation(activity, R.anim.right_image_rotate);
        LinearInterpolator lir = new LinearInterpolator();
        rAnim.setInterpolator(lir);
    }


    /**
     * 开始选择动画
     */
    @Override
    public void startLoadingAnimation() {

        ObdSerialUtil.sendOBDSerialCMD(activity, (byte) 0X0A);  // 发送车辆体检指令

        im_engine.setVisibility(View.GONE);
        im_solar_term_door.setVisibility(View.GONE);
        im_idling_system.setVisibility(View.GONE);
        im_inlet.setVisibility(View.GONE);
        im_cooling.setVisibility(View.GONE);
        im_emission.setVisibility(View.GONE);
        im_power.setVisibility(View.GONE);
        im_state_more.setVisibility(View.GONE);

        im_engine_loading.setVisibility(View.VISIBLE);
        im_solar_term_door_loading.setVisibility(View.VISIBLE);
        im_idling_systemt_loading.setVisibility(View.VISIBLE);
        im_inlet_loading.setVisibility(View.VISIBLE);
        im_cooling_loading.setVisibility(View.VISIBLE);
        im_emission_loading.setVisibility(View.VISIBLE);
        im_power_loading.setVisibility(View.VISIBLE);
        im_state_loading.setVisibility(View.VISIBLE);
        /**
         * 启动旋转动画
         */
        im_engine_loading.startAnimation(rAnim);
        im_solar_term_door_loading.startAnimation(rAnim);
        im_idling_systemt_loading.startAnimation(rAnim);
        im_inlet_loading.startAnimation(rAnim);
        im_cooling_loading.startAnimation(rAnim);
        im_emission_loading.startAnimation(rAnim);
        im_power_loading.startAnimation(rAnim);
        im_state_loading.startAnimation(rAnim);

    }

    /**
     * 清理动画
     */
    @Override
    public void cleanLoadingAnimation() {
        /**
         * 清除动画
         */
        im_engine_loading.clearAnimation();
        im_solar_term_door_loading.clearAnimation();
        im_idling_systemt_loading.clearAnimation();
        im_inlet_loading.clearAnimation();
        im_cooling_loading.clearAnimation();
        im_emission_loading.clearAnimation();
        im_power_loading.clearAnimation();
        im_state_loading.clearAnimation();

        im_engine.setVisibility(View.VISIBLE);
        im_solar_term_door.setVisibility(View.VISIBLE);
        im_idling_system.setVisibility(View.VISIBLE);
        im_inlet.setVisibility(View.VISIBLE);
        im_cooling.setVisibility(View.VISIBLE);
        im_emission.setVisibility(View.VISIBLE);
        im_power.setVisibility(View.VISIBLE);
        im_state_more.setVisibility(View.VISIBLE);

        im_engine_loading.setVisibility(View.GONE);
        im_solar_term_door_loading.setVisibility(View.GONE);
        im_idling_systemt_loading.setVisibility(View.GONE);
        im_inlet_loading.setVisibility(View.GONE);
        im_cooling_loading.setVisibility(View.GONE);
        im_emission_loading.setVisibility(View.GONE);
        im_power_loading.setVisibility(View.GONE);
        im_state_loading.setVisibility(View.GONE);

    }


    /**
     * 设置上次车辆体检的时间
     */
    @Override
    public void setLastScanTime() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        tv_lastScanTime.setText("最近检测 ：" + df.format(new Date()));
    }


}

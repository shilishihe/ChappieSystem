/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gm.ctrl_misc;

public class CtrlMisc {
    private static final String TAG = "Module";
    private static final boolean DEBUG = false;

    static {
        System.loadLibrary("ctrlmisc_jni");
    }

    public static native void CtrlMisc_camera_internal_ctrl(int onoff);

    public static native int CtrlMisc_camera_get_internal_camera();
    // return value 0 for carback camera, 1 for internal camera

}

package com.scblhkj.carluncher.carcorder.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.os.Environment;
import android.os.StatFs;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceView;
import android.view.WindowManager;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Leo on 2015/11/10.
 * 通用的操作方法
 */
public class CommonUtil {

    private static final String TAG = "CommonUtil";

    public static void captureScreen(Activity activity) {
        acquireScreenshot(activity);
    }

    public static void jiePing(SurfaceView surfaceViews) {
        Bitmap bitmap = Bitmap.createBitmap(surfaceViews.getWidth(), surfaceViews.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas bitCanvas = new Canvas(bitmap);
        surfaceViews.draw(bitCanvas);
        if (bitCanvas != null) {
            saveBitmap(bitmap);
            ToastUtil.showToast("截图成功");
        }
    }

    /**
     * 保存方法
     */
    public static void saveBitmap(Bitmap bm) {
        File f = new File(buildSavePictruePath());
        if (f.exists()) {
            f.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(f);
            bm.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
            Log.i(TAG, "已经保存");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 构建当前的日期
     *
     * @return
     */
    public static String buildNowDay() {
        SimpleDateFormat nowDayFormat = new SimpleDateFormat("yyyy-MM-dd");
        return nowDayFormat.format(new Date());
    }

    /**
     * 构建当天的时间
     *
     * @return
     */
    public static String buileNowTime() {
        SimpleDateFormat nowTimeFormat = new SimpleDateFormat("HH-mm-ss");
        return nowTimeFormat.format(new Date());
    }

    /**
     * 生成视频文件保存的地址
     */
    public static File buildSaveVideoFile() {
        File file;
        File dir = new File("/storage/sdcard1/ChappieLauncher/video", buildNowDay());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        file = new File(dir.getPath(), buileNowTime() + ".3gp");
        Log.e(TAG, "构建的视频文件路径为" + file.getPath());
        return file;
    }


    /**
     * 生成视频文件保存的地址
     */
    public static String buildSavePictruePath() {
        File file = new File("/storage/sdcard1/ChappieLauncher/picture", buildNowDay());
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getPath() + "/" + buileNowTime() + ".png";
    }


    /**
     * 获取sd卡可用空间大小
     * // 这里也需要判断是否有外置内存卡,如果有先删除外置内存卡上的视频
     *
     * @return
     */
    public static long getSDCardAvailableSpace() {
        long availableSize = 0;
        if (SDCardScannerUtil.isSecondSDcardMounted()) {  // 有内存卡,就先删内存卡上的视频
            availableSize = SDCardScannerUtil.getAvailableSize(SDCardScannerUtil.getTFDir()) / 1024 / 1024;
        } else { // 无内存卡
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                File sdcardDir = Environment.getExternalStorageDirectory();
                StatFs sf = new StatFs(sdcardDir.getPath());
                long blockSize = sf.getBlockSize();
                long availCount = sf.getAvailableBlocks();
                availableSize = availCount * blockSize / 1024 / 1024;
            }
        }
        return availableSize;
    }


    private static final String DEVICE_NAME = "/dev/graphics/fb0";

    @SuppressWarnings("deprecation")
    public static Bitmap acquireScreenshot(Context context) {
        WindowManager mWinManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        Display display = mWinManager.getDefaultDisplay();
        display.getMetrics(metrics);
        // 屏幕高
        int height = metrics.heightPixels;
        // 屏幕的宽
        int width = metrics.widthPixels;
        int pixelformat = display.getPixelFormat();
        PixelFormat localPixelFormat1 = new PixelFormat();
        PixelFormat.getPixelFormatInfo(pixelformat, localPixelFormat1);
        // 位深
        int deepth = localPixelFormat1.bytesPerPixel;
        byte[] arrayOfByte = new byte[height * width * deepth];
        try {
            // 读取设备缓存，获取屏幕图像流
            InputStream localInputStream = readAsRoot();
            DataInputStream localDataInputStream = new DataInputStream(localInputStream);
            localDataInputStream.readFully(arrayOfByte);
            localInputStream.close();
            int[] tmpColor = new int[width * height];
            int r, g, b;
            for (int j = 0; j < width * height * deepth; j += deepth) {
                b = arrayOfByte[j] & 0xff;
                g = arrayOfByte[j + 1] & 0xff;
                r = arrayOfByte[j + 2] & 0xff;
                tmpColor[j / deepth] = (r << 16) | (g << 8) | b | (0xff000000);
            }
            // 构建bitmap
            Bitmap scrBitmap = Bitmap.createBitmap(tmpColor, width, height, Bitmap.Config.ARGB_8888);
            return scrBitmap;

        } catch (Exception e) {
            Log.d(TAG, "#### 读取屏幕截图失败");
            e.printStackTrace();
        }
        return null;

    }

    /**
     * @throws Exception
     * @throws
     * @Title: readAsRoot
     * @Description: 以root权限读取屏幕截图
     */
    public static InputStream readAsRoot() throws Exception {
        File deviceFile = new File(DEVICE_NAME);
        Process localProcess = Runtime.getRuntime().exec("su");
        String str = "cat " + deviceFile.getAbsolutePath() + "\n";
        localProcess.getOutputStream().write(str.getBytes());
        return localProcess.getInputStream();
    }

    /**
     * 获取当前时间
     *
     * @return
     */
    public static String getCurrentTime() {
        SimpleDateFormat nowDayFormat = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat nowTimeFormat = new SimpleDateFormat("HHmmss");
        String nowDay = nowDayFormat.format(new Date());
        String nowTime = nowTimeFormat.format(new Date());
        return nowDay + "_" + nowTime;
    }

    /**
     * 将List集合序列化成Stirng
     *
     * @param SceneList
     * @return
     * @throws IOException
     */
    public static String SceneList2String(List SceneList) throws IOException {
        // 实例化一个ByteArrayOutputStream对象，用来装载压缩后的字节文件。
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        // 然后将得到的字符数据装载到ObjectOutputStream
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        // writeObject 方法负责写入特定类的对象的状态，以便相应的 readObject 方法可以还原它
        objectOutputStream.writeObject(SceneList);
        // 最后，用Base64.encode将字节文件转换成Base64编码保存在String中
        String SceneListString = new String(Base64.encode(byteArrayOutputStream.toByteArray(), Base64.DEFAULT));
        // 关闭objectOutputStream
        objectOutputStream.close();
        return SceneListString;
    }

    /**
     * 反序列化成List集合
     *
     * @param SceneListString
     * @return
     * @throws StreamCorruptedException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @SuppressWarnings("unchecked")
    public static List String2SceneList(String SceneListString) throws StreamCorruptedException, IOException, ClassNotFoundException {
        byte[] mobileBytes = Base64.decode(SceneListString.getBytes(), Base64.DEFAULT);
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(mobileBytes);
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        List SceneList = (List) objectInputStream.readObject();
        objectInputStream.close();
        return SceneList;
    }

    public static String timestamp2Data(long longString) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        return sdf.format(new Date(longString));
    }


    /**
     * 获取系统时间戳
     *
     * @return
     */
    public static long getTimeStamp() {
        return System.currentTimeMillis();
    }

    /**
     * 压缩编码后的时间戳
     *
     * @return
     */
    public static String getUnixTimeStamp() {
        String timeStamp = String.valueOf(CommonUtil.getTimeStamp() / 1000L);
        return timeStamp;
    }

    /**
     * 判断字符串中指定字符串出现的次数
     *
     * @param str
     * @param key
     * @return
     */
    public static int getSubCount(String str, String key) {
        int count = 0;
        count = str.split(key).length - 1;
        if (count <= 5) {
            count = 5;
        }
        return count;
    }

}

package com.scblhkj.carluncher.carcorder.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by he on 2015/8/22.
 * SharedPreferences 工具类
 */
public class SpUtil {

    private static Context context;
    private static SharedPreferences sp;
    private static SharedPreferences.Editor ed;
    public static int MODE = Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE;
    public static final String SP_CONFIG = "sp_config";

    public static void init(Context context) {
        SpUtil.context = context;
        sp = context.getSharedPreferences(SP_CONFIG, MODE);
        ed = sp.edit();
    }

    /**
     * 根据指定的key获取值
     *
     * @param key
     * @return
     */
    public static Boolean getBoolean2SP(String key) {
        return sp.getBoolean(key, false);
    }

    public static String getString2SP(String key) {
        return sp.getString(key, "0");
    }


    public static void saveInt2SP(String key, int i) {
        ed.putInt(key, i);
        ed.commit();
    }

    private static String TAG = "SpUtil";

    public static int getInt2SP(String key) {
        return sp.getInt(key, 0);
    }

    /**
     * 存储key value
     *
     * @param key
     * @param value
     */
    public static void saveString2SP(String key, String value) {
        ed.putString(key, value);
        ed.commit();
    }

    /**
     * 存储key value
     *
     * @param key
     * @param value
     */
    public static void saveBoolean2SP(String key, Boolean value) {
        ed.putBoolean(key, value);
        ed.commit();
    }

    public static void saveFloat2SP(String key, Float value) {
        ed.putFloat(key, value);
        ed.commit();
    }

    public static float getFloat2SP(String key) {
        return sp.getFloat(key, 0);
    }


}

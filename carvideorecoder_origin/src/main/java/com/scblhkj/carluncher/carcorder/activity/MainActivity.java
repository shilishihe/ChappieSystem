package com.scblhkj.carluncher.carcorder.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.scblhkj.carluncher.carcorder.R;
import com.scblhkj.carluncher.carcorder.service.DriveVideoRecordService;

public class MainActivity extends Activity {

    private static final String TAG = "行车记录仪主界面";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startDriveRecordService();
        startCarCollisionDetectionServoce();
    }

    @Override
    protected void onResume() {
        maxWindowManager();
        super.onResume();
    }

    @Override
    protected void onPause() {
        minWindowManager();
        super.onPause();
    }


    private void minWindowManager() {
        Intent intent = new Intent();
        intent.setAction(DriveVideoRecordService.MIN_WINDOW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intent);
    }

    private void maxWindowManager() {
        Intent intent = new Intent();
        intent.setAction(DriveVideoRecordService.MAX_WINDOW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intent);
    }

    /**
     * 启动行车记录服务
     */
    private void startDriveRecordService() {
        Intent intent = new Intent(this.getApplicationContext(), DriveVideoRecordService.class);
        this.startService(intent);
    }

    /**
     * 开启碰撞检测服务
     */
    private void startCarCollisionDetectionServoce() {
        Intent intent = new Intent();
        intent.setAction("com.scblhkj.carluncher.carcorder.service.CarCollisionDetectionService");
        intent.setPackage("com.scblhkj.carluncher.carcorder");  //Android5.0后service不能采用隐式启动，故此处加上包名
        startService(intent);
    }
}

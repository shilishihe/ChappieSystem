package com.scblhkj.carluncher.carcorder.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.widget.LinearLayout;

import com.gm.ctrl_misc.CtrlMisc;
import com.scblhkj.carluncher.carcorder.R;
import com.scblhkj.carluncher.carcorder.app.MyApplication;
import com.scblhkj.carluncher.carcorder.common.Constant;
import com.scblhkj.carluncher.carcorder.service.CarCollisionDetectionService;
import com.scblhkj.carluncher.carcorder.utils.CommonUtil;
import com.scblhkj.carluncher.carcorder.utils.FileUtil;
import com.scblhkj.carluncher.carcorder.utils.SDCardScannerUtil;
import com.scblhkj.carluncher.carcorder.utils.ToastUtil;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Leo on 2015/11/10.
 * 视频录制和拍照的控件
 */
public class RecordView extends LinearLayout implements MediaRecorder.OnErrorListener {


    private static SurfaceView mSurfaceView;
    private SurfaceHolder mSurfaceHolder;

    private MediaRecorder mMediaRecorder;
    private Camera mCamera;
    private Timer mTimer;// 计时器
    private OnRecordFinishListener mOnRecordFinishListener;// 录制完成回调接口
    private int mWidth;// 视频分辨率宽度
    private int mHeight;// 视频分辨率高度
    private boolean isPreviewing;// 是否是预览界面
    private int mRecordMaxTime;// 一次拍摄最长时间
    private int mTimeCount;// 时间计数
    public static final String CAR_COLLISION_ACTION = "com.scblhkj.chappie.carlauncher.car.collision";
    public int currentCamera; // 当前摄像头
    private static long tempStartRecordTime = 0;
    private static long tempEndRecordTime = 0;
    private Context context;
    private static long tempCollisionTime = 0;
    /**
     * 汽车震动的广播接收者类
     */
    private BroadcastReceiver carCollisionReceiver;
    private File mVecordFile = null;// 文件
    private File currentViewFile;

    public RecordView(Context context) {
        this(context, null);
    }

    public RecordView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    // 实例化广播接收者
    private void initCarCollisionReceiver() {
        carCollisionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (CAR_COLLISION_ACTION.equals(action)) {
                    Bundle bundle = intent.getExtras();
                    tempCollisionTime = bundle.getLong(CarCollisionDetectionService.TEMP_COLLISON_TIME);
                }
            }
        };
    }

    // 注册广播接收者
    private void registCarCollisionReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(RecordView.CAR_COLLISION_ACTION);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        context.registerReceiver(carCollisionReceiver, intentFilter);
    }

    public RecordView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RecorderView, defStyle, 0);
        mWidth = a.getInteger(R.styleable.RecorderView_video_width, 1280);// 默认320
        mHeight = a.getInteger(R.styleable.RecorderView_video_width, 720);// 默认240
        isPreviewing = a.getBoolean(R.styleable.RecorderView_is_open_camera, true);// 默认打开
        mRecordMaxTime = a.getInteger(R.styleable.RecorderView_record_max_time, 180);// 默认为一分钟时间
        LayoutInflater.from(context).inflate(R.layout.recorder_view, this);
        mSurfaceView = (SurfaceView) findViewById(R.id.surfaceview);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(new CustomCallBack());
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        initCarCollisionReceiver();
        a.recycle();
    }


    /**
     * @author liuyinjun
     * @date 2015-2-5
     */
    private class CustomCallBack implements Callback {

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                if (!isPreviewing) {
                    return;
                } else {
                    initCamera(Constant.CAMERA_MAIN); // 初始化为主摄像头
                    currentCamera = Constant.CAMERA_MAIN; // 初始化摄像头为主摄像头
                    if (onRecordViewInitSuccessListener != null) {
                        onRecordViewInitSuccessListener.OnRecordViewInitSuccess();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (MyApplication.isDebug)
                Log.e("Surface", "SurfaceView 被启动了");
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            if (MyApplication.isDebug)
                Log.e("Surface", "SurfaceView 被改变了");
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            if (!isPreviewing) {
                return;
            }
            if (MyApplication.isDebug)
                Log.e("Surface", "SurfaceView 被销毁了");
            freeCameraResource();
        }

    }

    private List<Camera.Size> sizes;

    /**
     * 初始化摄像头
     *
     * @throws IOException
     * @author liuyinjun
     * @date 2015-2-5
     */
    private void initCamera(int type) throws IOException {
        if (mCamera != null) {
            freeCameraResource();
        } else {
            try {
                if (type == Constant.CAMERA_MAIN) {  // 主摄像头
                    CtrlMisc.CtrlMisc_camera_internal_ctrl(Constant.CAMERA_MAIN);  // 打开主摄像头
                    mCamera = Camera.open(Constant.CAMERA_MAIN);
                    currentCamera = Constant.CAMERA_MAIN;
                } else if (type == Constant.CAMERA_CECOND) { // 副摄像头
                    CtrlMisc.CtrlMisc_camera_internal_ctrl(Constant.CAMERA_CECOND);  // 打开副摄像头
                    mCamera = Camera.open(Constant.CAMERA_CECOND);
                    currentCamera = Constant.CAMERA_CECOND;
                }
                sizes = mCamera.getParameters().getSupportedPreviewSizes();
                if (MyApplication.isDebug)
                    Log.e(TAG, "打开相机成功");
            } catch (Exception e) {
                if (MyApplication.isDebug)
                    Log.e(TAG, "打开相机失败");
                e.printStackTrace();
                freeCameraResource();
            }
        }
        if (mCamera == null) {
            return;
        } else {
            setCameraParams();
            mCamera.setPreviewDisplay(mSurfaceHolder);
            mCamera.startPreview();
            if (isFocus) {
                mCamera.autoFocus(null);
            }
            mCamera.unlock();
            if (MyApplication.isDebug)
                Log.e(TAG, "初始化相机对象");
        }
    }

    // 自动聚焦的标识
    private boolean isFocus = false;

    /**
     * 设置摄像头为竖屏
     *
     * @author liuyinjun
     * @date 2015-2-5
     */
    private void setCameraParams() {
        if (mCamera != null) {
            Camera.Parameters params = mCamera.getParameters();
            List<String> list = params.getSupportedFocusModes();
            if (list.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                isFocus = true;
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
            }
            params.set("orientation", "portrait");
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            mCamera.setParameters(params);
        }
    }

    /**
     * 释放摄像头资源
     *
     * @author liuyinjun
     * @date 2015-2-5
     */
    private void freeCameraResource() {
        if (mCamera != null) {
            mCamera.setPreviewCallback(null);
            // mCamera.cancelAutoFocus();
            mCamera.stopPreview();
            mCamera.lock();
            mCamera.release();
            mCamera = null;
            isPreviewing = false;
            /**
             * 关闭自动聚焦
             */
            if (MyApplication.isDebug)
                Log.e(TAG, "关闭摄像头");
        }
    }


    /**
     * 初始化
     *
     * @throws IOException
     * @author liuyinjun
     * @date 2015-2-5
     */
    private void initRecord() {

        try {
            if (SDCardScannerUtil.isSecondSDcardMounted()) { // 判断是否有外置的sd卡
                mMediaRecorder = new MediaRecorder();
                mMediaRecorder.reset();
                if (mCamera != null) {
                    mMediaRecorder.setCamera(mCamera);
                }
                mMediaRecorder.setPreviewDisplay(mSurfaceHolder.getSurface());// 设置捕获视频图像的预览界面
            //    if (RemoteSpUtil.getBoolean2SP(Constant.CAR_RECORDER_COMMON_SETTING_ACTIVITY_SW_RECORD_VOICE)) {   // 如果开启了就录制声音
                    mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);// 设置音频录入源
                mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);  // 设置视频图像的录入源
                mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);// 设置录入媒体的输出格式
                mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);// 设置音频编码
                //    }

                mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);// 设置视频编码
                mMediaRecorder.setVideoSize(Constant.VIDEO_RECORD_WIDTH_HEIGH, Constant.VIDEO_RECORD_HEIGHT_HEIGH);  // 设置录制视频的尺寸
                mMediaRecorder.setVideoEncodingBitRate(Constant.VIDEO_ENCODING_BIT_RATE_HEIGH);// 设置录制的视频编码比特率，然后就清晰了
                mMediaRecorder.setVideoFrameRate(32);//设置要捕获的视频帧速率

                currentViewFile = CommonUtil.buildSaveVideoFile();

                mMediaRecorder.setOutputFile(currentViewFile.getPath());
                // 准备录制
                mMediaRecorder.prepare();
                registCarCollisionReceiver();
                // 每段视频的开始录制时间戳
                tempStartRecordTime = System.currentTimeMillis();
                // 开始录制
                mMediaRecorder.start();
                Log.e(TAG,"开始录制视频");
            } else {
                ToastUtil.showToast("您未安装外置SD卡，暂时无法使用视频录制功能");
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static final String TAG = "RecordView";

    /**
     * 开始录制视频
     *
     * @author liuyinjun
     * @date 2015-2-5
     */
    public void record(final int type) {
        try {
            if (!isPreviewing) { // 如果未打开摄像头，则打开
                initCamera(type);
                isPreviewing = true;
                if (MyApplication.isDebug)
                    Log.e(TAG, "打开摄像头");
            }
            Log.e(TAG, "-------");
            initRecord();
            Log.e(TAG, "-------");
            mTimeCount = 0;// 时间计数器重新赋值
            mTimer = new Timer();
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    mTimeCount++;
                    if (mTimeCount == mRecordMaxTime + 1) {// 达到指定时间，停止拍摄
                        stop();
                        checkAvailbaleSpace();
                        record(type); // 重新开始录制
                    }
                }
            }, 0, 1000);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 检查内存卡容量
     */
    private void checkAvailbaleSpace() {
        File file = null;
        // 检查内存容量,是否大于某个临界值，如果大于就开始删除之前录制的视频
        if (SDCardScannerUtil.isSecondSDcardMounted()) {
            file = new File("/storage/sdcard1/ChappieLauncher/video");
            if (CommonUtil.getSDCardAvailableSpace() < 300) {  // 可用空间小于300M
                // 执行删除操作
                Log.e(TAG, "执行删除录制文件操作");
                deleteRecord(file);
            }
        }
    }

    /**
     * 停止拍摄
     *
     * @author liuyinjun
     * @date 2015-2-5
     */
    public void stop() {
        // 停止录像的时候，销毁定时聚焦定时器
        stopRecord();
        // 每一段视频的录制结束时间戳
        tempEndRecordTime = System.currentTimeMillis();
        // 碰撞的时间点是否在刚刚录制完成的这段视频里，如果是重命名该段视频的名称
        String filePath = currentViewFile.getPath();
        if (tempEndRecordTime >= tempCollisionTime && tempStartRecordTime <= tempCollisionTime) {  // 碰撞的时间在刚刚录制完成的这段视频区间里面，锁存这段视频
            if (TextUtils.isEmpty(filePath)) {
                if (MyApplication.isDebug)
                    Log.e(TAG, "要修该的视频文件的路径为空");
            } else {
                filePath = FileUtil.reNameFile(filePath);
            }
        }
        // 主动通知Android系统视频进行更新
        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + filePath)));
        releaseRecord();
        freeCameraResource();
    }

    /**
     * 停止录制
     *
     * @author liuyinjun
     * @date 2015-2-5
     */
    public void stopRecord() {
        if (mTimer != null)
            mTimer.cancel();
        if (mMediaRecorder != null) {
            // 设置后不会崩
            mMediaRecorder.setOnErrorListener(null);
            mMediaRecorder.setPreviewDisplay(null);
            try {
                mMediaRecorder.stop();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (RuntimeException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 释放资源
     *
     * @author liuyinjun
     * @date 2015-2-5
     */
    private void releaseRecord() {
        if (mMediaRecorder != null) {
            mMediaRecorder.setOnErrorListener(null);
            try {
                mMediaRecorder.release();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mMediaRecorder = null;
    }

    public int getTimeCount() {
        return mTimeCount;
    }

    /**
     * @return the mVecordFile
     */
    public File getmVecordFile() {
        return mVecordFile;
    }

    /**
     * 录制完成回调接口
     *
     * @author liuyinjun
     * @date 2015-2-5
     */
    public interface OnRecordFinishListener {
        void onRecordFinish();
    }

    @Override
    public void onError(MediaRecorder mr, int what, int extra) {
        try {
            if (mr != null)
                mr.reset();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface OnRecordViewInitSuccessListener {
        void OnRecordViewInitSuccess();
    }

    private OnRecordViewInitSuccessListener onRecordViewInitSuccessListener;

    public void setOnRecordViewInitSuccessListener(OnRecordViewInitSuccessListener onRecordViewInitSuccessListener) {
        this.onRecordViewInitSuccessListener = onRecordViewInitSuccessListener;
    }

    /**
     * 主摄像头拍照
     */
    public void takeMainPicture() {
        stop();  // 停止录制，释放资源
        isPreviewing = true;
        mCamera = Camera.open(0);
        try {
            mCamera.setPreviewDisplay(mSurfaceHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mCamera.startPreview();
        if (isPreviewing && mCamera != null) { // 界面处于预览界面
            mCamera.takePicture(shutterCallback, rawPictureCallback, jpegPictureCallback);
        }
    }


    /**
     * 副摄像头拍照
     */
    public void takeSecondPicture() {
        stop(); // 停止录像,释放资源
        isPreviewing = true;
        CtrlMisc.CtrlMisc_camera_internal_ctrl(1);  // 打开副摄像头
        mCamera = Camera.open(1);
        try {
            mCamera.setPreviewDisplay(mSurfaceHolder);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mCamera.startPreview();
        if (isPreviewing && mCamera != null) { // 界面处于预览界面
            mCamera.takePicture(shutterCallback, rawPictureCallback, jpegPictureCallback);
        }
    }

    /**
     * 倒车后视
     */
    public void switch2BackView() {
        stop();
        isPreviewing = true;
        CtrlMisc.CtrlMisc_camera_internal_ctrl(0);  // 切换到倒车模式
        mCamera = Camera.open(1);
        try {
            mCamera.setPreviewDisplay(mSurfaceHolder);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mCamera.startPreview();
    }

    /**
     * 切换到副摄像头录像
     */
    public void restartSecondRecordVideo() {
        stop(); // 停止录像,释放资源
        isPreviewing = false;
        record(Constant.CAMERA_CECOND);
    }

    /**
     * 切换到主摄像头录制服务
     */
    public void restartMainVideo() {
        stop(); // 停止录像,释放资源
        CtrlMisc.CtrlMisc_camera_internal_ctrl(0);  // 重置到倒车摄像头
        isPreviewing = false;
        record(Constant.CAMERA_MAIN);
    }

    Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {
        @Override
        public void onShutter() {
        }
    };

    Camera.PictureCallback rawPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] arg0, Camera arg1) {
        }
    };

    Camera.PictureCallback jpegPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] bytes, Camera camera) {
            if (SDCardScannerUtil.isSecondSDcardMounted()) {
                File file = new File(CommonUtil.buildSavePictruePath());
                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdir();
                }
                try {
                    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
                    bos.write(bytes);
                    bos.flush();
                    bos.close();
                    ToastUtil.showToast("[Test] Photo take and store in" + file.toString());
                    // 项目中遇到调用图库进行图片的选择，因为不能主动及时更新，遂实现代码调用实现主动及时更新
                    context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + file.getPath())));
                } catch (Exception e) {
                    ToastUtil.showToast("Picture Failed" + e.toString());
                }
                // 拍完照不管成功或是失败都要重新的开始自动录像
                isPreviewing = false;
                switch (currentCamera) {
                    case Constant.CAMERA_MAIN: {
                        restartMainVideo();
                        break;
                    }
                    case Constant.CAMERA_CECOND: {
                        restartSecondRecordVideo();
                        break;
                    }
                }
            } else {
                ToastUtil.showToast("您未安装外置SD卡，暂时无法使用视频录制功能");
            }

            // 不管拍照成功或者是失败都需要重置前置倒车摄像头
            CtrlMisc.CtrlMisc_camera_internal_ctrl(0);  // 重置到倒车摄像头
        }
    };


    /*
    * 删除之前视频记录
    */
    private synchronized void deleteRecord(File file) {
        int dSize = 0;
        if (file.exists()) {
            if (file.isDirectory()) {  //判断是否是文件夹(这里的file肯定是文件夹)  /video
                File[] dirs = file.listFiles();
                for (File subDir : dirs) {  //遍历所有的文件  /20151117
                    if (subDir.isDirectory()) { //如果是文件夹，就删除里边的50M的文件个文件(这里的文件肯定也是文件夹,每一天都会有一个文件夹)
                        if (subDir.listFiles().length > 0) {  //文件夹下有文件,需要删除文件夹下的文件，且文件的总大小要超过500M
                            File[] vfs = subDir.listFiles();
                            for (File f : vfs) {  //每一天视频文件夹下的所有视频文件
                                if (!f.getName().contains("lock")) {   // 不删除所存文件
                                    dSize += FileUtil.getDirSize(f);
                                    if (MyApplication.isDebug)
                                        Log.e(TAG, FileUtil.getDirSize(f) + "M的文件被删除");
                                    f.delete();
                                    if (dSize >= 500) {
                                        if (MyApplication.isDebug)
                                            Log.e(TAG, "500M文件删除成功");
                                        break;
                                    }
                                }
                            }
                        } else {  //文件夹为空，删除文件夹增加遍历的效率
                            subDir.delete();
                        }
                    }
                }
            }
        }
    }
}


package com.scblhkj.carluncher.carcorder.app;

import android.app.Application;

import com.scblhkj.carluncher.carcorder.utils.MeasureScreenUtil;
import com.scblhkj.carluncher.carcorder.utils.RemoteSpUtil;
import com.scblhkj.carluncher.carcorder.utils.SpUtil;
import com.scblhkj.carluncher.carcorder.utils.ToastUtil;

/**
 * Created by Leo on 2016/2/24.
 */
public class MyApplication extends Application {
    public static int SCREEN_WIDTH = 0;
    public static int SCREEN_HEIGHT = 0;
    public static boolean isDebug = false;

    @Override
    public void onCreate() {
        super.onCreate();
        ToastUtil.init(this);
        SpUtil.init(this);
        RemoteSpUtil.init(this);
        MeasureScreenUtil.init(this);
        MeasureScreenUtil.getScreenSize();
        MyApplication.SCREEN_WIDTH = MeasureScreenUtil.width;
        MyApplication.SCREEN_HEIGHT = MeasureScreenUtil.height;
    }
}

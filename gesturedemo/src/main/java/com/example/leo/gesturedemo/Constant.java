package com.example.leo.gesturedemo;

/**
 * Created by Leo on 2016/3/2.
 */
public interface Constant {
    /**
     * 手势广播
     */
    String ACTION_INTENT_GSENSON_F5_EVENT = "android.intent.action.GSENSON_F5_EVENT";  // 向右手势
    String ACTION_INTENT_GSENSON_F6_EVENT = "android.intent.action.GSENSON_F6_EVENT";  // 向左手势
    /**
     * 手势开启或者是关闭
     */
    String ACTION_ENABLE_SENSORTEK = "com.sensortek.broadcast.enable"; // 使能手势
    String ACTION_DISABLE_SENSORTEK = "com.sensortek.broadcast.disable"; // 关闭手势
}

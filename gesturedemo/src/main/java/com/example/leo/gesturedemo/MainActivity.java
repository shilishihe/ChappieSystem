package com.example.leo.gesturedemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button bt;
    private BroadcastReceiver broadcastReceiver;
    private static final String TAG = "手势";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt = (Button) findViewById(R.id.bt);
        bt.setOnClickListener(this);
        initBroadCastReceiver();
        registerBoradCastReceiver();
        sendBroacast2EnableSensortek();
    }

    /**
     * 初始化广播接收者
     */
    private void initBroadCastReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch (action) {
                    case Constant.ACTION_INTENT_GSENSON_F5_EVENT: {  // 向右手势(点亮屏幕)
                        Log.e(TAG, "右方手势");
                        Toast.makeText(MainActivity.this, "右方手势", Toast.LENGTH_LONG).show();
                        break;
                    }
                    case Constant.ACTION_INTENT_GSENSON_F6_EVENT: {  // 向左手势(熄灭屏幕)
                        Log.e(TAG, "左方手势");
                        Toast.makeText(MainActivity.this, "左方手势", Toast.LENGTH_LONG).show();
                        break;
                    }
                    default: {
                        Log.i(TAG, "没有注册此类广播");
                        break;
                    }
                }


            }
        };
    }

    @Override
    protected void onDestroy() {
        unRegisterBoradCastReceiver();
        super.onDestroy();
    }

    /**
     * 广播通知开启手势
     */
    private void sendBroacast2EnableSensortek() {
        Intent dataIntent = new Intent();
        dataIntent.setAction(Constant.ACTION_ENABLE_SENSORTEK);
        dataIntent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(dataIntent);
        Toast.makeText(this, "发送开启手势广播", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        sendBroacast2EnableSensortek();
    }

    private void registerBoradCastReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constant.ACTION_INTENT_GSENSON_F5_EVENT);  // 向右手势
        intentFilter.addAction(Constant.ACTION_INTENT_GSENSON_F6_EVENT);  // 向左手势
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    private void unRegisterBoradCastReceiver() {
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

}
